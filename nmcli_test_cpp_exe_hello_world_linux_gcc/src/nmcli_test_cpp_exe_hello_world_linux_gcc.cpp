//============================================================================
// Name        : nmcli_test_cpp_exe_hello_world_linux_gcc.cpp
// Author      : Me
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================
/*
#include <iostream>
using namespace std;

int main() {
	cout << "Hello World" << endl; // prints Hello World
	return 0;
}
*/
/** @mainpage nmcli_test_cpp - None
 *
 * @author Me <anony@mo.us>
 * @version 1.0.0
**/


#include <stdio.h>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <list>

#include "WifiAccessPointDetails.h"

using namespace std;

const string cmdScan = "nmcli device wifi rescan";
const string cmdWifiList = "nmcli -t -f SIGNAL,IN-USE,BARS,SSID,SECURITY device wifi list";

/**
 * Main class of project nmcli_test_cpp
 *
 * @param argc the number of arguments
 * @param argv the arguments from the commandline
 * @returns exit code of the application
 */
int main(int argc, char **argv) {
	// print a greeting to the console
	printf("Hello World from nmcli_test_cpp!\n");

	cout << "test text" << std::endl;

	list<WifiAccessPointDetails> wifiList;
	printf("Number in list=%d\n", wifiList.size());

	printf("Add to list.\n");
	for(int i = 0; i < 10; ++i)
	{
		wifiList.push_back(WifiAccessPointDetails().generateRandomObj());
	}

	return 0;
}


class NetworkConnections
{
	NetworkConnections() : m_populated(false) {};
	~NetworkConnections();

	void testFunction(void);

public:

private:
bool m_populated;
};
void NetworkConnections::testFunction(void)
{

}

