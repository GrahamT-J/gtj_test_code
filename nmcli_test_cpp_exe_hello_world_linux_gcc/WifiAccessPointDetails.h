/*
 * WifiList.h
 *
 *  Created on: May 26, 2017
 *      Author: cargt
 */
#pragma once
#ifndef WIFILIST_H
#define WIFILIST_H

#include <stdint.h>
#include <vector>
#include <string>

// forward declarations

using namespace std;

#define MAX_SSID_LENGTH 32
#define MAX_PASSWORD_LENGTH 64


class WifiAccessPointDetails
{
public:
	WifiAccessPointDetails();
	~WifiAccessPointDetails() {};
	WifiAccessPointDetails(const string& formattedInput);

    WifiAccessPointDetails(const WifiAccessPointDetails& inObj) { *this = inObj; }

	WifiAccessPointDetails& operator=(const WifiAccessPointDetails& inObj);

	bool isPopulated(void) { return m_populated; }

    string getFormatStringForScanCommand();
    string getObjectAsString();

    string generateRandomAPString();
    WifiAccessPointDetails generateRandomObj();

    const char separator = ':';



private:

	bool m_populated;
	int m_signalStrength;
	bool m_connectionInUse;
	uint8_t m_numberOfBars;
	string m_SSID;
	string m_security;

};




#endif /* WIFILIST_H_ */
