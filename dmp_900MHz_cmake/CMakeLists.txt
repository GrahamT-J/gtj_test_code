cmake_minimum_required (VERSION 2.8.1)

######## Project settings ########
PROJECT(dmp_900MHz_cmake)
SET(LICENSE "TBD")

######## Preprocessor flags ########
SET(GCC_PRE_PROC_DEFS "-DUSING_CHIPCON_RF_CHIP")

add_definitions(${GCC_PRE_PROC_DEFS})
SET( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${GCC_PRE_PROC_DEFS}" )

######## Build and include settings ########
include_directories(
	inc
	src
	src/dummy
	src/hardware_includes
	src/Transmitter
	src/Transmitter/9060
)

link_directories(
	${LINK_DIRECTORIES}
)


file(GLOB SOURCES
	"src/*.c"
	"src/dummy/Dummy.c"
	"src/Transmitter/*.c"
	"src/Transmitter/9060/*.c"
	"src/hardware_linux/*.c"
)

add_executable(
	dmp_900MHz_cmake

	${SOURCES}
)

TARGET_LINK_LIBRARIES(
	dmp_900MHz_cmake
)

######## Install targets ########
INSTALL(TARGETS dmp_900MHz_cmake
	RUNTIME DESTINATION usr/bin
)
