/*H*****************************************************************************
FILENAME: BitMacros.h

DESCRIPTION: Macros for manipulation status bits without worrying about the
  byte they are in

NOTES:

Copyright (c) Digital Monitoring Products Inc. 2008.  All rights reserved.

*****************************************************************************H*/
#ifndef  BITMACROS_H
#define  BITMACROS_H

/*----compilation control-----------------------------------------------------*/

/*******************************************************************************
INCLUDE FILES   Note: Minimize nesting of header files.
*******************************************************************************/
/*----system and platform files-----------------------------------------------*/

/*----program files-----------------------------------------------------------*/

/*******************************************************************************
FILE CONTEXT
*******************************************************************************/
/*----defines, typedefs, structs, unions, enums-------------------------------*/
//-------------------------------------------------

#if defined(XT50_RECEIVER) || defined(XTL_RECEIVER)

#define getFlag(A)          (A##_##BYTE & A##_MASK)
#define setFlag(A)          A##_##BYTE |= A##_MASK
#define toggleFlag(A)       A##_##BYTE ^= A##_MASK
#define clearFlag(A)        A##_##BYTE &= ~A##_MASK
#define getIO(A)            (A##_##IN & A##_MASK)
#define getIOOut(A)         (A##_##OUT & A##_MASK)
#define setIO(A)            A##_##SET = A##_MASK
#define clearIO(A)          A##_##CLR = A##_MASK
#define setIODirOut(A)      A##_##DIR |= A##_MASK
#define setIODirIn(A)       A##_##DIR &= ~A##_MASK
#define setIORisingInt(A)   A##_##IE |= A##_MASK
#define setIOEnableInt(A)   A##_##IE |= A##_MASK
#define clearIOEnableInt(A) A##_##IE &= ~A##_MASK
#define clearIOIntFlag(A)   A##_##CLRIFG |= A##_MASK
#define clearIOSel(A)       A##_##SEL &= ~A##_SELMASK

#else

#ifndef __STDC__

MoveImmediateWordMacro MACRO   value,port
            mov.w   #value,&port
            ENDM

GetPortMask MACRO   mask,port
            bit.b   #mask,&port
            ENDM

SetPortMask MACRO   mask,port
            bis.b   #mask,&port
            ENDM

ClearPortMask MACRO   mask,port
              bic.b   #mask,&port
              ENDM

TogglePortMask MACRO   mask,port
              xor.b   #mask,&port
              ENDM


#define moveImmediateWord(A,B) MoveImmediateWordMacro   A,B
#define getIO(A) GetPortMask              A##_MASK,A##_IN
#define setIO(A) SetPortMask              A##_MASK,A##_OUT
#define toggleIO(A) TogglePortMask        A##_MASK,A##_OUT
#define clearIO(A) ClearPortMask          A##_MASK,A##_OUT
#define setIODirOut(A) SetPortMask        A##_MASK,A##_DIR
#define setIODirIn(A) ClearPortMask       A##_MASK,A##_DIR
#define setIOFallingInt(A) SetPortMask    A##_MASK,A##_IES
#define setIORisingInt(A) ClearPortMask   A##_MASK,A##_IES
#define setIOEnableInt(A) SetPortMask     A##_MASK,A##_IE
#define clearIOEnableInt(A) ClearPortMask A##_MASK,A##_IE
#define getIOIntFlag(A) GetPortMask       A##_MASK,A##_IFG
#define clearIOIntFlag(A) ClearPortMask   A##_MASK,A##_IFG
#define setIOSel(A) SetPortMask           A##_MASK,A##_SEL
#define clearIOSel(A) ClearPortMask       A##_MASK,A##_SEL

#else
#define moveImmediateWord(A,B) (B)=(A)
#define getFlag(A)          (A##_##BYTE & A##_MASK)
#define setFlag(A)          A##_##BYTE |= A##_MASK
#define toggleFlag(A)       A##_##BYTE ^= A##_MASK
#define clearFlag(A)        A##_##BYTE &= ~A##_MASK
#define getIO(A)            (A##_##IN & A##_MASK)
#define getIOOut(A)         (A##_##OUT & A##_MASK)
#define setIO(A)            A##_##OUT |= A##_MASK
#define toggleIO(A)         A##_##OUT ^= A##_MASK
#define clearIO(A)          A##_##OUT &= ~A##_MASK
#define setIODirOut(A)      A##_##DIR |= A##_MASK
#define setIODirIn(A)       A##_##DIR &= ~A##_MASK
#define setIOIntEdge(A)     (A##_##IES & A##_MASK)
#define setIOFallingInt(A)  A##_##IES |= A##_MASK
#define setIORisingInt(A)   A##_##IES &= ~A##_MASK
#define setIOEnableInt(A)   A##_##IE |= A##_MASK
#define clearIOEnableInt(A) A##_##IE &= ~A##_MASK
#define getIOIntFlag(A)     A##_##IFG & A##_MASK
#define clearIOIntFlag(A)   A##_##IFG &= ~A##_MASK
#define setIOSel(A)         A##_##SEL |= A##_MASK
#define clearIOSel(A)       A##_##SEL &= ~A##_MASK
#define enableIOResistor(A) A##_##REN |= A##_MASK
#define disableIOResistor(A) A##_##REN &= ~A##_MASK
#endif

#endif

/*----data descriptions-------------------------------------------------------*/

/*----data declarations-------------------------------------------------------*/

/*----macros------------------------------------------------------------------*/

/*----function prototypes-----------------------------------------------------*/

#endif                                  /* end of file */
