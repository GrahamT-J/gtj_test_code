/*******************************************************************************
FILENAME: TransmitterMain.h

DESCRIPTION:

NOTES:

Copyright (c) Digital Monitoring Products Inc. 2010.  All rights reserved.

*******************************************************************************/
#ifndef  TRANSMITTERMAIN_H
#define  TRANSMITTERMAIN_H

/*----compilation control-----------------------------------------------------*/

/*******************************************************************************
INCLUDE FILES
*******************************************************************************/
/*----system and platform files-----------------------------------------------*/
#include "ProtocolTiming.h"

/*----program files-----------------------------------------------------------*/

/*******************************************************************************
FILE CONTEXT
*******************************************************************************/
/*----defines, typedefs, structs, unions, enums-------------------------------*/
typedef enum
{
  FS_SETUP_TRANSMITTER,
  FS_WAITING_FOR_FIRST_PATTERN,
  FS_WAITING_FOR_FIRST_ACK,
  FS_WAITING_FOR_SLOT_DEFINITION,
  FS_TRANSMITTING,
  FS_WAITING_FOR_ACK_PATTERN,
  FS_WAITING_FOR_ACK,
  FS_WAITING_FOR_EOF,
  FS_WAITING_FOR_ACK_SETUP,
  FS_SETUP_FOR_ACK,
  FS_WAITING_FOR_SECOND_HALF_OF_LONG_SLOT,
  FS_TRANSMITTING_AFTER_LONG_SLOT,
  FS_WAITING_FOR_FIRST_HALF_OF_LONG_SLOT
} FrameStatus;

typedef enum
{
  WM_OFF,
  WM_RECEIVE,
  WM_TRANSMIT,
  WM_IDLE
} WirelessMode;


typedef enum
{
  RS_GOOD_SYNC,
  RS_TEMP_SYNC,
  RS_BAD_SYNC,
  RS_FORCE_NO_SYNC
} ReceiverSyncStatus;

// SyncCounter Description
//  SyncCounter can range from -RESYNC_FAILURE to +GOOD_SYNC_COUNT with a negative number meaning the transmitter is out of sync.  A positive number will signify the transmitter
//  is in some form of sync with a receiver and a +GOOD_SYNC_COUNT means that we are in absolute sync with our programmed receiver.  When a message from a wrong receiver is
//  received the count will be taken to INITIAL_SYNC_COUNT.  If we receive a message that we didn't understand the will either decrement the count by one if the count is positive
//  take the count to BUMP_SYNC_COUNT if the current count is negative.  If a frame timeout occurs the number will be decremented with the limit of either RESYNC_FAILURE or
//  RECEIVER_FAIL_RESYNC_FAILURE based upon if the receiver is currently in failure.
typedef enum
{
  INITIAL_SYNC_COUNT = 0,
  BUMP_SYNC_COUNT = 1,
  TRANSMITTER_MODE_SYNC = 5,
  GOOD_SYNC_COUNT = 10,
  GOOD_STATUS_SYNC_COUNT = 14,
  MAX_SYNC_COUNT = 24,
  RESYNC_TRIES    = 64,  // Set to allow a transmitter to listen through all hop channels (53) but still be a power of two.  MUST BE A POWER OF TWO!!
  RESYNC_FAILURE  = -(3 * RESYNC_TRIES) - 1, // Set to allow three times of RESYNC_TRIES but minus 1 to make it not a power of RESYNC_TRIES.  MUST NOT BE A POWER OF RESYNC_TRIES
  RECEIVER_FAIL_RESYNC_FAILURE = -RESYNC_TRIES - 1 // Set to allow a single RESYNC_TRIES but minus 1 to make it not a power of RESYNC_TRIES.  MUST NOT BE A POWER OF RESYNC_TRIES
} SyncCounter;

typedef enum
{
  DEFAULT_WAKEUP_FRAME_TIMEOUT = FB_TOTAL,
  STANDARD_FRAME_TIMEOUT = FB_TOTAL,
} FrameTimeout;

// lastResortTimer is really just a "safety net" to make sure we timeout after a reasonable amount of time
typedef enum
{
  LAST_RESORT_TIMEOUT = 200 // number of frame timeouts before we give up
} LastResortTimer;

typedef enum
{
  MSS_AWAKE,
  MSS_SUCCESS,
  MSS_FAILURE,
  MSS_RETRY
} MainSleepState;

/*----data descriptions-------------------------------------------------------*/
extern FrameStatus frameStatus;
extern SyncCounter syncCounter;
extern char hopOffset;
extern FrameTimeout frameTimeout;
extern LastResortTimer lastResortTimer;
extern int BusPoll_Time;

/*----data declarations-------------------------------------------------------*/
/*----macros------------------------------------------------------------------*/

/*----function prototypes-----------------------------------------------------*/
extern void TransmitterMain(void);
extern void ProcessTransmitterEOF(void);
extern bool TransmitterInSync(void);
extern void SwitchFrequencyModes(void);
extern void ResetLastResortTimer(void);

#endif                                  /* end of file */
