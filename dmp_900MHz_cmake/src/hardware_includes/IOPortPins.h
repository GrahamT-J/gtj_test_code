/*H*****************************************************************************
FILENAME:

DESCRIPTION:

NOTES:

Copyright (c) Digital Monitoring Products Inc. 2004.  All rights reserved.

*****************************************************************************H*/
#ifndef IOPORTPINS_H
#define IOPORTPINS_H

/*----compilation control-----------------------------------------------------*/
#if !defined(__linux__)
/*******************************************************************************
INCLUDE FILES   Note: Minimize nesting of header files.
*******************************************************************************/
/*----system and platform files-----------------------------------------------*/

/*----program files-----------------------------------------------------------*/
#include "MSP430SFR.h"
#include "BitMacros.h"

/*******************************************************************************
FILE CONTEXT
*******************************************************************************/
/*----defines, typedefs, structs, unions, enums-------------------------------*/
#define EnableRFDataClkInt() MoveImmediateWord(CCIS0+CM0+CAP+CCIE,TBCCTL2)
#define DisableRFDataClkInt() MoveImmediateWord(CCIS0+CM0+CAP,TBCCTL2)

#define CSn_MASK                  BIT0
#define CSn_IN                    P1IN
#define CSn_OUT                   P1OUT
#define CSn_DIR                   P1DIR
#define CSn_SEL                   P1SEL

#define SI_MASK                   BIT2
#define SI_IN                     P1IN
#define SI_OUT                    P1OUT
#define SI_DIR                    P1DIR
#define SI_SEL                    P1SEL

#define SCLK_MASK                 BIT0
#define SCLK_IN                   P2IN
#define SCLK_OUT                  P2OUT
#define SCLK_DIR                  P2DIR
#define SCLK_SEL                  P2SEL

#define SO_GDO1_MASK              BIT3
#define SO_GDO1_IN                P1IN
#define SO_GDO1_DIR               P1DIR
#define SO_GDO1_SEL               P1SEL
#define SO_GDO1_IE                P1IE
#define SO_GDO1_IES               P1IES
#define S0_GDO1_IFG               P1IFG

#define GDO2_MASK                 BIT7
#define GDO2_IN                   P2IN
#define GDO2_DIR                  P2DIR
#define GDO2_SEL                  P2SEL
#define GDO2_IE                   P2IE
#define GDO2_IES                  P2IES
#define GDO2_IFG                  P2IFG

#define RF_DATA_CLK_MASK          SO_GDO1_MASK
#define RF_DATA_CLK_IN            SO_GDO1_IN
#define RF_DATA_CLK_DIR           SO_GDO1_DIR
#define RF_DATA_CLK_SEL           SO_GDO1_SEL
#define RF_DATA_CLK_IE            SO_GDO1_IE
#define RF_DATA_CLK_IES           SO_GDO1_IES
#define RF_DATA_CLK_IFG           S0_GDO1_IFG

#define GDO0_MASK                 BIT4
#define GDO0_IN                   P2IN
#define GDO0_DIR                  P2DIR
#define GDO0_SEL                  P2SEL
#define GDO0_OUT                  P2OUT

#define RF_DATA_IN_MASK           GDO0_MASK
#define RF_DATA_IN_IN             GDO0_IN
#define RF_DATA_IN_DIR            GDO0_DIR
#define RF_DATA_IN_SEL            GDO0_SEL

#define RF_DATA_OUT_MASK          GDO0_MASK
#define RF_DATA_OUT_IN            GDO0_IN
#define RF_DATA_OUT_DIR           GDO0_DIR
#define RF_DATA_OUT_SEL           GDO0_SEL
#define RF_DATA_OUT_OUT           GDO0_OUT


/*----data descriptions-------------------------------------------------------*/

/*----data declarations-------------------------------------------------------*/

/*----macros------------------------------------------------------------------*/

/*----function prototypes-----------------------------------------------------*/

#else // !defined(__linux__)

#define GDO0_GPIO_NUM       70 // gpio number
#define GDO1_GPIO_NUM        9 // 9 is also SPI_3 MISO
#define GDO2_GPIO_NUM       69 // gpio number

#define RF_DATA_CLK         GDO1_GPIO_NUM // gpio number
#define RF_DATA_IN          GDO0_GPIO_NUM // gpio number
#define RF_DATA_OUT         GDO0_GPIO_NUM // gpio number

// Interrupt Enable
unsigned char clearIOEnableInt(unsigned char gpioNum);
unsigned char setIOEnableInt(unsigned char gpioNum);

// Interrupt Type
unsigned char setIORisingInt(unsigned char gpioNum);


unsigned char clearIOIntFlag(unsigned char gpioNum);

// Set IO directions
unsigned char setIODirIn(unsigned char gpioNum);
unsigned char setIODirOut(unsigned char gpioNum);

// IO operations
unsigned char clearIO(unsigned char gpioNum);
unsigned char getIO(unsigned char gpioNum);
unsigned char setIO(unsigned char gpioNum);

#endif // __linux__
#endif                                  /* end of file */
