/*H*****************************************************************************
FILENAME:

DESCRIPTION:

NOTES:

Copyright (c) Digital Monitoring Products Inc. 1999.  All rights reserved.

	Last change:  TDC  27 Jan 99    8:43 am
*****************************************************************************H*/

#ifndef  CHIPCON_H
#define  CHIPCON_H

/*----compilation control-----------------------------------------------------*/

/*******************************************************************************
INCLUDE FILES   Note: Minimize nesting of header files.
*******************************************************************************/
/*----system and platform files-----------------------------------------------*/

/*----program files-----------------------------------------------------------*/
#include "Types.h"

/*******************************************************************************
FILE CONTEXT
*******************************************************************************/
/*----defines, typedefs, structs, unions, enums-------------------------------*/
typedef enum
{
  STANDARD_FREQUENCY_MODE,
  LOW_INTERFERENCE_FREQUENCY_MODE
} FrequencyMode;

/*******************************************************************************
Description:  The below table defines the chipcon configuration registers and should
                be referenced with readChipconConfigRegister() or writeChipconConfigRegister()
*******************************************************************************/
typedef enum
{
  IOCFG2 = 0x00,  //GDO2 output pin configuration
  IOCFG1,         //GDO1 output pin configuration
  IOCFG0,         //GDO0 output pin configuration
  FIFOTHR,        //RX FIFO and TX FIFO thresholds
  SYNC1,          //Sync word, high byte
  SYNC0,          //Sync word, low byte
  PKTLEN,         //Packet length
  PKTCTRL1,       //Packet automation control
  PKTCTRL0,       //Packet automation control
  ADDR,           //Device address
  CHANNR,         //Channel number
  FSCTRL1,        //Frequency synthesizer control
  FSCTRL0,        //Frequency synthesizer control
  FREQ2,          //Frequency control word, high byte
  FREQ1,          //Frequency control word, middle byte
  FREQ0,          //Frequency control word, low byte
  MDMCFG4,        //Modem configuration
  MDMCFG3,        //Modem configuration
  MDMCFG2,        //Modem configuration
  MDMCFG1,        //Modem configuration
  MDMCFG0,        //Modem configuration
  DEVIATN,        //Modem deviation setting
  MCSM2,          //Main Radio Control State Machine configuration
  MCSM1,          //Main Radio Control State Machine configuration
  MCSM0,          //Main Radio Control State Machine configuration
  FOCCFG,         //Frequency Offset Compensation configuration
  BSCFG,          //Bit Synchronization configuration
  AGCTRL2,        //AGC control
  AGCTRL1,        //AGC control
  AGCTRL0,        //AGC control
  WOREVT1,        //High byte Event 0 timeout
  WOREVT0,        // Low byte Event 0 timeout
  WORCTRL,        // Wake On Radio control
  FREND1,         // Front end RX configuration
  FREND0,         // Front end TX configuration
  FSCAL3,         // Frequency synthesizer calibration
  FSCAL2,         // Frequency synthesizer calibration
  FSCAL1,         // Frequency synthesizer calibration
  FSCAL0,         // Frequency synthesizer calibration
  RCCTRL1,        // RC oscillator configuration
  RCCTRL0,        // RC oscillator configuration
  FSTEST,         // Frequency synthesizer calibration control
  PTEST,          // Production test
  AGCTEST,        // AGC test
  TEST2,          // Various test settings
  TEST1,          // Various test settings
  TEST0,          // Various test settings
  PATABLE = 0x3E  // PA power control settings
}ConfigRegisters;

/*******************************************************************************
Description:  The below table defines the Command Strobe addresses and should be referenced with
                sendChipconCommandStrobe
********************************************************************************/
typedef enum
{
  SRES = 0x30,  // Reset chip.
  SFSTXON,      // Enable and calibrate frequency synthesizer (if MCSM0.FS_AUTOCAL=1). If in RX (with CCA):
                //   Go to a wait state where only the synthesizer is running (for quick RX / TX turnaround).
  SXOFF,        // Turn off crystal oscillator.
  SCAL,         // Calibrate frequency synthesizer and turn it off (enables quick start). SCAL can be strobed from
                //   IDLE mode without setting manual calibration mode (MCSM0.FS_AUTOCAL=0)
  SRX,          // Enable RX. Perform calibration first if coming from IDLE and MCSM0.FS_AUTOCAL=1.
  STX,          // In IDLE state: Enable TX. Perform calibration first if MCSM0.FS_AUTOCAL=1.
                //   If in RX state and CCA is enabled: Only go to TX if channel is clear.
  SIDLE,        // Exit RX / TX, turn off frequency synthesizer and exit Wake-On-Radio mode if applicable.
  SAFC,         // Perform AFC adjustment of the frequency synthesizer as outlined in section 22.1.
  SWOR,         // Start automatic RX polling sequence (Wake-on-Radio) as described in section 27.5.
  SPWD,         // Enter power down mode when CSn goes high.
  SFRX,         // Flush the RX FIFO buffer.
  SFTX,         // Flush the TX FIFO buffer.
  SWORRST,      // Reset real time clock.
  SNOP          // No operation. May be used to pad strobe commands to two bytes for simpler software.
}CommandStrobes;

/*******************************************************************************
Description:  The below table define the status register addresses and should be referenced with
                readChipconStatusRegister
********************************************************************************/
typedef enum
{
  PARTNUM = 0x30, //Part number for CC1100
  VERSION,        //Current version number
  FREQEST,        //Frequency Offset Estimate
  LQI,            //Demodulator estimate for Link Quality
  RSSI,           //Received signal strength indication
  MARCSTATE,      //Control state machine state
  WORTIME1,       //High byte of WOR timer
  WORTIME0,       //Low byte of WOR timer
  PKTSTATUS,      //Current GDOx status and packet status
  VCO_VC_DAC,     //Current setting from PLL calibration module
  TXBYTES,        //Underflow and number of bytes in the TX FIFO
  RXBYTES         //Overflow and number of bytes in the RX FIFO
} StatusRegisters;

#define PA_TABLE_SIZE 8 //CC1100 PATable is 8 bytes in length

#define RF_BITS_PER_SECOND (9600)
#if defined USING_1232_HARDWARE || defined DMP_1119_TRANSMITTER
#define RFBitTime 2
#else
#define RFBitTime (SMCLK / RF_BITS_PER_SECOND)
#endif

#define CONVERT_RSSI_TO_DBM(measuredRSSI) ((measuredRSSI / 2) - 76) // convert from 1/2dbm to 1dbm and apply offset
//The RSSI_TO_DBM offset is a measured value and is based upon the specific chipcon
//configuration.  If changes are made to the baud rate or frequency band this number will be invalid
/*----data descriptions-------------------------------------------------------*/

/*----data declarations-------------------------------------------------------*/

/*----macros------------------------------------------------------------------*/

/*----function prototypes-----------------------------------------------------*/
#if defined(XT50_RECEIVER) || defined(XTL_RECEIVER)
extern bool initChipcon(void);
#else
extern void initChipcon(void);
#endif
extern char writeChipconConfigRegister(ConfigRegisters address, char data);
extern char sendChipconCommandStrobe(CommandStrobes address);
extern char readChipconConfigRegister(ConfigRegisters address);
extern char readChipconStatusRegister(StatusRegisters address);
extern char readChipconStatus(void);
extern char initChipconPowerTable(bool highPowerReceiver);
extern void sendChannel(unsigned char channel);
extern void latchFrequencyError(void);
extern void applyFrequencyErrorOffset(void);
#if defined(XT50_RECEIVER) || defined(XTL_RECEIVER)
extern char getCurrentRSSI(void);
extern void readRSSI(void);
extern void clearCurrentRSSI(void);
#endif
extern void SetFrequencyMode(FrequencyMode mode);
extern void SetReducedTXPowerLevel(bool reducedTXPower);
#endif
                                 /* end of file */
