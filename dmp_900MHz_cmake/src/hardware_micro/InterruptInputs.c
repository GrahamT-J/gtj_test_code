/*H*****************************************************************************
FILENAME:

DESCRIPTION:

NOTES:

Copyright (c) Digital Monitoring Products Inc. 2009.  All rights reserved.

*****************************************************************************H*/
/*----compilation control-----------------------------------------------------*/

/*******************************************************************************
INCLUDE FILES
*******************************************************************************/
/*----system and platform files (external libraries) -------------------------*/
#include "types.h"
#include <string.h>

/*----program files-----------------------------------------------------------*/
#include "Sleep.h"
#include "transmitterprotocol.h"
#include "IOPortPins.h"
#include "Inputs.h"
#include "ChargerInputs.h"


/*******************************************************************************
EXTERNAL REFERENCES   Only used if not available in a header file.
*******************************************************************************/
/*----data declarations-------------------------------------------------------*/
extern ZoneFlags myZoneFlags;
extern char Flags2;
extern char TxD_Que;

/*----function prototype------------------------------------------------------*/
extern bool Chk_Key_Que(void);
extern void AckTamper(void); // ASM fcn
extern bool IsINSTMode(void); // ASM fcn

/*******************************************************************************
PRIVATE DECLARATIONS
*******************************************************************************/
/*----defines, typedefs, structs, unions, enums-------------------------------*/

// it takes this many transmits at low battery before low battery is shown
enum {LOW_BATTERY_COUNTER_THRESHHOLD = 10};

typedef struct
{
  BITFIELD lowBattery : 1;
  BITFIELD counter    : 7;
} LowBatteryState;

/*----data declarations-------------------------------------------------------*/
ContactState contactSteadyState;
LowBatteryState lowBatteryState;
static ZoneMessageByte previousZoneMessageByte;
static char savedKeyPress;
static bool validSavedKeyPress = false;

/*----function prototype------------------------------------------------------*/

/*----macros------------------------------------------------------------------*/

/*******************************************************************************
PRIVATE FUNCTION DEFINITIONS
*******************************************************************************/

/*******************************************************************************

DESCRIPTION:

NOTES: This assumes that any edge interrupts on this port are for alarm contacts

*******************************************************************************/
extern void rfDataClockEdge(void);
// Note: This was converted from an __interrupt function to a regular C function. An interrupt
// wrapper preserves the scratch registers R12-R15 because the interrupt code below
// calls regular (non-interrupt) C functions (which are free to alter R12-R15 w/out preserving them).
void PORT1_EDGE_INTERRUPT(void)
{
  if (getIOIntFlag(RF_DATA_CLK))
  {
    clearIOIntFlag(RF_DATA_CLK);
    rfDataClockEdge();
  }
  else
  {
    P1IFG = 0;        // clear all edge interrupts
    P1IE &= RF_DATA_CLK_MASK; // disable all other interrupts
  }
}

/*******************************************************************************
PUBLIC FUNCTION DEFINITIONS
*******************************************************************************/

/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
void initInputs(void)
{
 getZoneMessageByte(&previousZoneMessageByte);
 previousZoneMessageByte ^= 0xFF; // force a difference the next time getZoneMessageByte() is called
}

/*******************************************************************************

DESCRIPTION:  Starts debounce timer so it will interrupt at debounce timer plus
  the amount of time for it to start at an appropriate hop

NOTES:  If SLEEP_TICKS_PER_HOP ever becomes a value that is not a power of two, the
  modulo operator will take much longer and this will probably need to change

*******************************************************************************/
void startInputDebounce(short debounceTime)
{
  if (! (TACCTL0 & CCIE)) // if not already debouncing
  {
    unsigned short tempTar = TAR;
    TACCR0 = tempTar + debounceTime + // current time plus debounce time plus
             SLEEP_TICKS_PER_HOP -      // an exra hop minus
             WAKEUP_CLOCK_SLOP_TICKS -  // some padding for clock slop minus
             ((tempTar - sleepTime) % SLEEP_TICKS_PER_HOP); // how far off we currently are from start of hop
    TACCTL0 = CCIE;  // enable interrupt and set compare mode
  }
}

/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
void testBattery(void)
{
  if (CACTL2 & CAOUT)
  {
    if (lowBatteryState.counter < LOW_BATTERY_COUNTER_THRESHHOLD)
    {
      lowBatteryState.counter++;
    }
    else
    {
      lowBatteryState.lowBattery = true;
    }
  }
  else      // a single "good" resets everything
  {
    lowBatteryState.counter = 0;
    lowBatteryState.lowBattery = false;
  }
}

/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
ZoneMessageByte getInputState(void)
{
  ZoneMessageByte state;

  // get states from keypads
  getZoneMessageByte(&state);

  return state;
}

/*******************************************************************************

DESCRIPTION: Copies current ZoneMessageByte value to location referenced by ptr.

NOTES:

*******************************************************************************/
void getZoneMessageByte(ZoneMessageByte* ptr)
{
  ZoneMessageByte zmb = (ZoneMessageByte)0;
  if (contactSteadyState & CS_TAMPER_SW)
    zmb |= MB_TAMPER;
  AckTamper(); // tell ASM code we've examined tamper state

  if (currentAnalogState.batteryLow || currentAnalogState.batteryMissing)
    zmb |= MB_LOW_BATTERY;

  if (currentAnalogState.powerFail)
    zmb |= MB_LOW_POWER;

  if (IsINSTMode())  // acquire works only in INST mode
    zmb |= MB_ACQUIRE;

  if (ptr != NULL)
    *ptr = zmb;
}

/*******************************************************************************

DESCRIPTION: If ZoneMessageByte values have changed, return TRUE.

NOTES:

*******************************************************************************/
bool TestForZoneMessageToSend(void)
{
  ZoneMessageByte zmb = (ZoneMessageByte)0;

  getZoneMessageByte(&zmb);

  if (zmb != previousZoneMessageByte)
  {
    previousZoneMessageByte = zmb;
    return true;
  }
  else
  {
    return false;
  }
}

/*******************************************************************************

DESCRIPTION: Update TAMPER status

NOTES:

*******************************************************************************/
void SetTamperState(void)
{
  contactSteadyState |= CS_TAMPER_SW;
}

void ClrTamperState(void)
{
  contactSteadyState &= ~CS_TAMPER_SW;
}

/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
void SaveKeypress(unsigned char key)
{
  static unsigned char lastSavedKeyPress = 0;

  savedKeyPress = key;

  // Don't toggle the sequence bit if this is a card read
  if (!(savedKeyPress & CARD_READ_BIT))
  {
    // Toggle sequence bit for each new key
    savedKeyPress |= ((lastSavedKeyPress & SEQUENCE_BIT) ^ SEQUENCE_BIT);
    lastSavedKeyPress = savedKeyPress;
  }

  validSavedKeyPress = true;
}

/*******************************************************************************

DESCRIPTION: Examine any data that's in the key queue and return it's type
   (key press or card reader data).

NOTES:

*******************************************************************************/
KeyTestType TestForKeyPressOrCardData(void)
{
  KeyTestType keyType = KEYTEST_NONE;

  // if there's not a saved char already, then try to fetch char from queue
  if (!validSavedKeyPress && Chk_Key_Que())
    SaveKeypress(TxD_Que);

  if (validSavedKeyPress)
  {
    if (savedKeyPress & CARD_READ_BIT)
      keyType = KEYTEST_CARDREAD;
    else
      keyType = KEYTEST_KEY;
  }

  return keyType;
}

/*******************************************************************************

DESCRIPTION: Examine the destType and fetch that type of data.

NOTES: This fcn should only be called after first calling TestForKeyPressOrCardData
  to ensure there is data waiting to be fetched.

*******************************************************************************/
void getKeyPressOrCardData(unsigned char* dest, KeyTestType destType)
{
  char data;
  int count;
  unsigned char *tempDest = dest;
  unsigned digitCount = 0;

  if (dest == NULL)
    return;

  if (destType == KEYTEST_KEY)
  {
    *dest = savedKeyPress & KEY_MASK;
  }
  else if (destType == KEYTEST_CARDREAD)
  {
    data = savedKeyPress;

    // Fill buffer with PAD digits
    for (count = 0; count < NUM_CARD_READ_DIGITS; count += 2)
    {
      *tempDest++ = CARD_READ_PAD_BYTE;
    }

    for (count = 0; count < NUM_CARD_READ_DIGITS; count++)
    {
      if (data & CARD_READ_BIT)
      {
        // pack 2 nibbles per byte.
        if (count & 0x01)
        {
          *dest &= ~(CARD_READ_MASK << 4);         // zero out pad in MS nibble
          *dest++ |= (data & CARD_READ_MASK) << 4; // or in the data to the MS nibble
          digitCount++;
        }
        else
        {
          *dest = (CARD_READ_PAD_BYTE & 0xF0) | (data & CARD_READ_MASK); // ls nibble is written first
          digitCount++;
        }

        if (Chk_Key_Que())
          data = TxD_Que;
        else
          break; // exit for loop since no more data
      }
      else
      {
        SaveKeypress(TxD_Que); // found a normal keypress byte (not card reader data)
        break; // exit for loop
      }
    }

    if (digitCount >= 5)
    {
      // Enable enqueueing of a CMD key press (as a card read CMD, not a normal press)
      // The keypress should be sent after the card read (+ retries if any)
      // is sent and ACKed.
      EnableAutomaticCMDPress();
    }
  }
}

/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
void ClearKeyPress(void)
{
  validSavedKeyPress = false;
}


