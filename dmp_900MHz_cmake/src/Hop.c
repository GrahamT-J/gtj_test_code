/*******************************************************************************
FILENAME:

DESCRIPTION:

NOTES:

Copyright (c) Digital Monitoring Products Inc. 2007.  All rights reserved.

*******************************************************************************/
/*----compilation control-----------------------------------------------------*/

/*******************************************************************************
INCLUDE FILES
*******************************************************************************/
/*----system and platform files (external libraries) -------------------------*/

/*----program files-----------------------------------------------------------*/
#include "Types.h"
#include "Hop.h"
#include "Chipcon.h"

/*******************************************************************************
EXTERNAL REFERENCES   Only used if not available in a header file.
*******************************************************************************/
/*----data declarations-------------------------------------------------------*/

/*----function prototype------------------------------------------------------*/


/*******************************************************************************
PRIVATE DECLARATIONS
*******************************************************************************/
/*----defines, typedefs, structs, unions, enums-------------------------------*/
// note, this may need modification for values of NUM_HOP_CHANNELS other than 61 and 53
#ifdef FCC_RECEIVER_VERSION
#define ChannelScramble(A) (A)
#else
#define ChannelScramble(A) (((A) ^ ((A) * 8)) & 0x3F)
#endif

/*----data declarations-------------------------------------------------------*/
byte houseCode = INVALID_HOUSE_CODE;
char currentTransmitterChannel = 0;
char currentReceiverChannel = 0;
char currentChannel = 0;

/*----function prototype------------------------------------------------------*/

/*----macros------------------------------------------------------------------*/

/*******************************************************************************
PRIVATE FUNCTION DEFINITIONS
*******************************************************************************/
/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/


/*******************************************************************************
PUBLIC FUNCTION DEFINITIONS
*******************************************************************************/
/*******************************************************************************

DESCRIPTION: Used to scramble the channel order and set the Wireless chip to the new channel

NOTES:

*******************************************************************************/
void SetFrequency(byte Channel)
{

  currentChannel = Channel;


  Channel = ChannelScramble(Channel);
  // if too big, do it again
  if (Channel >= NUM_HOP_CHANNELS) Channel = ChannelScramble(Channel);

  sendChannel(Channel);
}

/*******************************************************************************

DESCRIPTION: Adds a channel offset to a channel and returns the result

NOTES:

*******************************************************************************/
byte addChannel(byte channel, byte channelAdd)
{
  char newChannel = channel + channelAdd;
  while (newChannel >= NUM_HOP_CHANNELS) newChannel -= NUM_HOP_CHANNELS;
  return newChannel;
}

/*******************************************************************************

DESCRIPTION: Subtracts a channel from another channel, returns the result

NOTES:

*******************************************************************************/
byte SubtractChannel(byte channel, byte channelSub)
{
  while (channel < channelSub)
    channel += NUM_HOP_CHANNELS;

  return (channel - channelSub);
}

/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
byte NextChannel(byte channel)
{
  char incVal = houseCode;

  if (incVal == 0)
  {
    incVal = 1;
  }
  return addChannel(channel, incVal);
}

/*******************************************************************************

DESCRIPTION: Function will add channel to numHops and return the result

NOTES:

*******************************************************************************/

byte AddHops(byte numHops, byte channel)
{
  if (houseCode == 0)
    channel = addChannel(channel, numHops);
  else
    channel = addChannel(channel, (numHops * houseCode));

  return (channel);
}

/*******************************************************************************

DESCRIPTION: Function will subtract a number of hops from the channel number passed
              and will return the result.

NOTES:

*******************************************************************************/
byte SubtractHops(byte numHops, byte channel)
{
  if(houseCode == 0)
    channel = SubtractChannel(channel, numHops);
  else
    channel = SubtractChannel(channel, (numHops * houseCode));

  return (channel);
}
