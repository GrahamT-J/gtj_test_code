#ifndef MSP430SFR_H
  #define MSP430SFR_H

  #ifdef MSP430x24x
    #include "msp430x24x.h"
  #else
    #ifdef USING_RECEIVER_HARDWARE
      #include "msp430x14x.h"
    #else
      #ifdef USING_2131_HARDWARE
        #include "msp430x21x1.h"
      #else
        #ifdef USING_1232_HARDWARE
          #include "msp430x12x2.h"
        #else
          #include "msp430x11x1.h"
        #endif
      #endif
    #endif
  #endif
#endif

