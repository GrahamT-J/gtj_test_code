#ifndef __DUMMY_H__
#define __DUMMY_H__

extern unsigned int TBCCR1; // Timer used for wireless slot

void PowerDownComparator(void);
void PowerUpComparator(void);

#endif // __DUMMY_H__
