/*H*****************************************************************************
FILENAME:

DESCRIPTION:

NOTES:

Copyright (c) Digital Monitoring Products Inc. 2009.  All rights reserved.

*****************************************************************************H*/
/*----compilation control-----------------------------------------------------*/

/*******************************************************************************
INCLUDE FILES
*******************************************************************************/
/*----system and platform files (external libraries) -------------------------*/
#include "Types.h"
#include <string.h>

/*----program files-----------------------------------------------------------*/


/*******************************************************************************
EXTERNAL REFERENCES   Only used if not available in a header file.
*******************************************************************************/
/*----data declarations-------------------------------------------------------*/

/*----function prototype------------------------------------------------------*/

/*******************************************************************************
PRIVATE DECLARATIONS
*******************************************************************************/
/*----defines, typedefs, structs, unions, enums-------------------------------*/

// it takes this many transmits at low battery before low battery is shown
enum {LOW_BATTERY_COUNTER_THRESHHOLD = 10};

typedef struct
{
  BITFIELD lowBattery : 1;
  BITFIELD counter    : 7;
} LowBatteryState;

/*----data declarations-------------------------------------------------------*/
LowBatteryState lowBatteryState;

/*----function prototype------------------------------------------------------*/

/*----macros------------------------------------------------------------------*/

/*******************************************************************************
PRIVATE FUNCTION DEFINITIONS
*******************************************************************************/

/*******************************************************************************

DESCRIPTION:

NOTES: This assumes that any edge interrupts on this port are for alarm contacts

*******************************************************************************/


void testBattery(void)
{
  // Start with always good
  lowBatteryState.counter = 0;
  lowBatteryState.lowBattery = false;

  // To set low battery then
  //lowBatteryState.counter = LOW_BATTERY_COUNTER_THRESHOLD;
  //lowBatteryState.lowBattery = true;
}

