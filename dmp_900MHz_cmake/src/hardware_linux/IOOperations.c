#include "IOPortPins.h"


// Interrupt Enable
unsigned char clearIOEnableInt(unsigned char gpioNum){return 0;}
unsigned char setIOEnableInt(unsigned char gpioNum){return 0;}

// Interrupt Type
unsigned char setIORisingInt(unsigned char gpioNum){return 0;}


unsigned char clearIOIntFlag(unsigned char gpioNum){return 0;}

// Set IO directions
unsigned char setIODirIn(unsigned char gpioNum){return 0;}
unsigned char setIODirOut(unsigned char gpioNum){return 0;}

// IO operations
unsigned char clearIO(unsigned char gpioNum){return 0;}
unsigned char getIO(unsigned char gpioNum){return 0;}
unsigned char setIO(unsigned char gpioNum){return 0;}

