/*
 * Copyright (C) Your copyright notice.
 *
 * Author: Me
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the PG_ORGANIZATION nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY	THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS-IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "Spi.hpp"
#include "Gpio.hpp"

using namespace std;

void readMultipleTestCommands_CC1101(Gpio& gpioCS, Spi& spi);
void testReadWrite_CC1120(Gpio& gpioCS, Spi& spi);
void usage();

void usage()
{
	printf("Parameters required:\n<spi dev number 0 or 1>\tchooses device spi0.0 or 1.0\n");
}
//10
//18

#define CS_USES_GPIO
#ifdef CS_USES_GPIO
#define CS_ACTIVE 0
#define CS_INACTIVE 1
#endif

int main(int argc, char** argv) {
    cout << "Hello World from spi_cc1101_auto_test" << endl; /* prints Hello World */

    if(argc != 2)
    {
    	usage();
    	return -1;
    }

   	int spiDevNumber = atoi(argv[1]);
    char spiDeviceName[16];

   	if ((0 == spiDevNumber) || (1 == spiDevNumber))
   	{
   		sprintf(spiDeviceName, "/dev/spi%d.0", spiDevNumber);
   	}
   	else
   	{
   		printf("Error in spi dev number:%d", spiDevNumber);
   	}

   	printf("Using %s\n", spiDeviceName);
	// Set up SPI interface
   	Spi spi("/dev/spidev0.0", 8, 2.5 * 1000 * 1000);


#ifdef CS_USES_GPIO
   	char gpioNumberText[4];
   	sprintf(gpioNumberText, "%d", ((1 == spiDevNumber) ? 10 : 18));
   	//Gpio gpioCS("18"); // spi0.0
   	//Gpio gpioCS("10"); // spi1.0
   	printf("CS gpio num=%s\n", gpioNumberText);

   	Gpio gpioCS(gpioNumberText);
   	gpioCS.unexportPin();
   	sleep(1);
   	gpioCS.exportPin();
   	gpioCS.setPinDirection(Gpio::DIRECTION_OUT);
   	gpioCS.setPinValue(CS_INACTIVE);
#endif


   	// Set up GPIO Pin 69 as input pin.
    Gpio gpio("69");
    gpio.unexportPin();
    sleep(1);
    gpio.exportPin();
    gpio.setPinDirection(Gpio::DIRECTION_IN);

    uint8_t addrVal = 0;
    uint8_t rdVal = 0;
    uint8_t errVal = 0;


    while(1)
    {
    	uint8_t value;
    	uint8_t status;
    	gpioCS.setPinValue(CS_ACTIVE);
    	status = spi.readSingleByte(0xaa,value);
    	gpioCS.setPinValue(CS_INACTIVE);
    	printf("%02x, %02x\n",value,status);
    	sleep(1);
    }

#ifdef CS_USES_GPIO
    // CC1120 test
    //testReadWrite_CC1120(gpioCS, spi);
    // CC1101 - 0x30 = Partnum; 0x31 = Version, 0x34 = RSSI
    readMultipleTestCommands_CC1101(gpioCS, spi);
#endif
/*
#ifdef CS_USES_GPIO
    gpioCS.setPinValue(CS_ACTIVE);
    uint8_t pinVal;
    //gpio.getPinValue(&pinVal, 1);
    //printf("Pin:%u\n", pinVal);
#endif
    addrVal = 0x30; errVal = spi.readSingleByte(addrVal, rdVal);
#ifdef CS_USES_GPIO
    gpioCS.setPinValue(CS_INACTIVE);
#endif
    printf("Read location %x: RdVal=0x%02x, RetVal=%d\n", addrVal, rdVal, errVal);
#ifdef CS_USES_GPIO
    gpioCS.setPinValue(CS_ACTIVE);
#endif
    addrVal = 0x31; errVal = spi.readSingleByte(addrVal, rdVal);
#ifdef CS_USES_GPIO
    gpioCS.setPinValue(CS_INACTIVE);
#endif
    printf("Read location %x: RdVal=0x%02x, RetVal=%d\n", addrVal, rdVal, errVal);
#ifdef CS_USES_GPIO
    gpioCS.setPinValue(CS_ACTIVE);
#endif
    addrVal = 0x34; errVal = spi.readSingleByte(addrVal, rdVal);
#ifdef CS_USES_GPIO
    gpioCS.setPinValue(CS_INACTIVE);
#endif
    printf("Read location %x: RdVal=0x%02x, RetVal=%d\n", addrVal, rdVal, errVal);
*/

#ifdef CS_USES_GPIO
    //gpioCS.unexportPin();
#endif
    return 0;
}

void readMultipleTestCommands_CC1101(Gpio& gpioCS, Spi& spi)
{
#define NO_OF_TEST_CMDS 15
	// 0x30 Partnum, 0x31 Version, 0x34 RSSI, 0x35 MARCSTATE state machine state
	uint8_t addrValArr[NO_OF_TEST_CMDS] = { 0x35, 0x35, 0x35, 0x30, 0x30, 0x30, 0x31, 0x31, 0x31, 0x34, 0x34, 0x34, 0x35, 0x35, 0x35, };
	uint8_t statusByteArr[NO_OF_TEST_CMDS];
	uint8_t rdValArr[NO_OF_TEST_CMDS];

	for(uint8_t i = 0; i < NO_OF_TEST_CMDS; ++i)
	{
#ifdef CS_USES_GPIO
    gpioCS.setPinValue(CS_ACTIVE);
#endif
    // Must use readBurst for status bytes
    statusByteArr[i] = spi.readBurst(addrValArr[i], &rdValArr[i], 1);
#ifdef CS_USES_GPIO
    gpioCS.setPinValue(CS_INACTIVE);
#endif
	}
	for(uint8_t i = 0; i < NO_OF_TEST_CMDS; ++i)
	{
		printf("Read location 0x%02x: Status Byte=0x%02x, RdVal=0x%02x\n", addrValArr[i], statusByteArr[i], rdValArr[i]);
	}
}

void testReadWrite_CC1120(Gpio& gpioCS, Spi& spi)
{
	// Test read / write
	uint8_t addrVal = 0x2E;
	uint8_t statusVal;
	uint8_t rdVal;

#ifdef CS_USES_GPIO
    gpioCS.setPinValue(CS_ACTIVE);
#endif
	statusVal = spi.readSingleByte(addrVal, rdVal);
#ifdef CS_USES_GPIO
    gpioCS.setPinValue(CS_INACTIVE);
#endif
	printf("Read CC1120 location 0x%02x: Status Byte=0x%02x, RdVal=0x%02x\n", addrVal, statusVal, rdVal);

	uint8_t writeVal = (rdVal == 0xA5) ? 0xA6 : 0xA5;
#ifdef CS_USES_GPIO
    gpioCS.setPinValue(CS_ACTIVE);
#endif
	statusVal = spi.writeSingleByte(addrVal, writeVal);
#ifdef CS_USES_GPIO
    gpioCS.setPinValue(CS_INACTIVE);
#endif
	printf("Write CC1120 location 0x%02x: Status Byte=0x%02x, WrVal=0x%02x\n", addrVal, statusVal, writeVal);

#ifdef CS_USES_GPIO
    gpioCS.setPinValue(CS_ACTIVE);
#endif
	statusVal = spi.readSingleByte(addrVal, rdVal);
#ifdef CS_USES_GPIO
    gpioCS.setPinValue(CS_INACTIVE);
#endif
	printf("Read CC1120 location 0x%02x: Status Byte=0x%02x, RdVal=0x%02x\n", addrVal, statusVal, rdVal);

}

