/*
 * Copyright (C) Your copyright notice.
 *
 * Author: Me
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the PG_ORGANIZATION nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY	THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS-IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdlib.h>
#include <stdio.h>

#include <signal.h>
#include <stdio.h>
#include <string.h>
#include <string>
#include <sys/time.h>

#include <errno.h>
#include <unistd.h>
#include <fcntl.h>


 /****************************************************************
 * Constants
 ****************************************************************/

#define SYSFS_GPIO_DIR "/sys/class/gpio"
#define MAX_BUF 64

/****************************************************************
 * Variables
 ****************************************************************/

/****************************************************************
 * gpio_export
 ****************************************************************/
int gpio_export(unsigned int gpio)
{
	int fd, len;
	char buf[MAX_BUF];

	fd = open(SYSFS_GPIO_DIR "/export", O_WRONLY);
	if (fd < 0) {
		perror("gpio/export");
		return fd;
	}

	len = snprintf(buf, sizeof(buf), "%d", gpio);
	write(fd, buf, len);
	close(fd);

	return 0;
}

/****************************************************************
 * gpio_unexport
 ****************************************************************/
int gpio_unexport(unsigned int gpio)
{
	int fd, len;
	char buf[MAX_BUF];

	fd = open(SYSFS_GPIO_DIR "/unexport", O_WRONLY);
	if (fd < 0) {
		perror("gpio/export");
		return fd;
	}

	len = snprintf(buf, sizeof(buf), "%d", gpio);
	write(fd, buf, len);
	close(fd);
	return 0;
}

/****************************************************************
 * gpio_set_dir
 ****************************************************************/
int gpio_set_dir(unsigned int gpio, unsigned int out_flag)
{
	int fd, len;
	char buf[MAX_BUF];

	len = snprintf(buf, sizeof(buf), SYSFS_GPIO_DIR  "/gpio%d/direction", gpio);

	fd = open(buf, O_WRONLY);
	if (fd < 0) {
		perror("gpio/direction");
		return fd;
	}

	if (out_flag)
		write(fd, "out", 4);
	else
		write(fd, "in", 3);

	close(fd);
	return 0;
}

/****************************************************************
 * gpio_set_value
 ****************************************************************/
int gpio_set_value(unsigned int gpio, unsigned int value)
{
	int fd, len;
	char buf[MAX_BUF];

	len = snprintf(buf, sizeof(buf), SYSFS_GPIO_DIR "/gpio%d/value", gpio);

	fd = open(buf, O_WRONLY);
	if (fd < 0) {
		perror("gpio/set-value");
		return fd;
	}

	if (value)
		write(fd, "1", 2);
	else
		write(fd, "0", 2);

	close(fd);
	return 0;
}

/****************************************************************
 * gpio_get_value
 ****************************************************************/
int gpio_get_value(unsigned int gpio, unsigned int *value)
{
	int fd, len;
	char buf[MAX_BUF];
	char ch;

	len = snprintf(buf, sizeof(buf), SYSFS_GPIO_DIR "/gpio%d/value", gpio);

	fd = open(buf, O_RDONLY);
	if (fd < 0) {
		perror("gpio/get-value");
		return fd;
	}

	read(fd, &ch, 1);

	if (ch != '0') {
		*value = 1;
	} else {
		*value = 0;
	}

	close(fd);
	return 0;
}


/****************************************************************
 * gpio_set_edge
 ****************************************************************/

int gpio_set_edge(unsigned int gpio, char *edge)
{
	int fd, len;
	char buf[MAX_BUF];

	len = snprintf(buf, sizeof(buf), SYSFS_GPIO_DIR "/gpio%d/edge", gpio);

	fd = open(buf, O_WRONLY);
	if (fd < 0) {
		perror("gpio/set-edge");
		return fd;
	}

	write(fd, edge, strlen(edge) + 1);
	close(fd);
	return 0;
}

/****************************************************************
 * gpio_fd_open
 ****************************************************************/

int gpio_fd_open(unsigned int gpio)
{
	int fd, len;
	char buf[MAX_BUF];

	len = snprintf(buf, sizeof(buf), SYSFS_GPIO_DIR "/gpio%d/value", gpio);

	fd = open(buf, O_RDONLY | O_NONBLOCK );
	if (fd < 0) {
		perror("gpio/fd_open");
	}
	return fd;
}

/****************************************************************
 * gpio_fd_close
 ****************************************************************/

int gpio_fd_close(int fd)
{
	return close(fd);
}

static unsigned int g_gpio = 0;
#define INPUT_CHAR_BUF_LENGTH 10
struct {
	bool i2cSlaveRequested;
	bool inputAvailable;
	char inputBuf[INPUT_CHAR_BUF_LENGTH];
	char inputChar;
} workRequired;

#if fget_method
bool checkWorkToDo()
{
	unsigned int curVal = 0;
    gpio_get_value(g_gpio, &curVal);
    workRequired.i2cSlaveRequested = curVal;

    char buf[INPUT_CHAR_BUF_LENGTH];
    char *retVal;
    retVal = fgets(buf, sizeof buf, stdin);
    // error if !retVal
    if(retVal)
    {
    	int len = strlen(buf);
    	if ((len > 0) && (len <= INPUT_CHAR_BUF_LENGTH))
            buf[len - 1] = '\0';
        workRequired.inputAvailable = true;
        strcpy(&workRequired.inputBuf[0], &buf[0]);
    }
    else
    {
    	workRequired.inputAvailable = false;
    }

    if (workRequired.i2cSlaveRequested) return true;
    if (workRequired.inputAvailable) return true;

    return false;
}
#else
bool checkWorkToDo()
{
	unsigned int curVal = 0;
    gpio_get_value(g_gpio, &curVal);
    workRequired.i2cSlaveRequested = curVal;

    char rdChar;
    ssize_t charPresent = read (0, &rdChar, 1);
    if (charPresent)
    {
    	workRequired.inputChar = rdChar;
    	workRequired.inputAvailable = true;
    }
    else
    {
    	workRequired.inputAvailable = false;
    }

    if (workRequired.i2cSlaveRequested) return true;
    if (workRequired.inputAvailable) return true;

    return false;
}
#endif

#define SLEEP_TIME_IN_MS 100

int main (int argc, char **argv, char **envp)
{
struct timespec sleepTime;

	int gpio_fd;
	unsigned int gpio;
	int len;

	if (argc < 2) {
		printf("Usage: timer_gpio <gpio-pin>\n\n");
		printf("Waits for a change in the GPIO pin voltage level or input on stdin\n");
		exit(-1);
	}

	puts("Hello World from i2c_sleep"); /* prints Hello World */

	fcntl(0, F_SETFL, O_NONBLOCK); // open stdin

	gpio = atoi(argv[1]);

	gpio_export(gpio);
	gpio_set_dir(gpio, 0);
	gpio_set_edge(gpio, "rising");
	gpio_fd = gpio_fd_open(gpio);

	g_gpio = gpio; // Set global var

	memset(&workRequired, 0, sizeof workRequired);

	sleepTime.tv_sec = (SLEEP_TIME_IN_MS / 1000); // whole number of seconds
	sleepTime.tv_nsec = (SLEEP_TIME_IN_MS % 1000) * 1000000; // remove number of seconds -> nano seconds

 /* Main Loop. */
	do
	{
		while(checkWorkToDo())
		{
			if(workRequired.i2cSlaveRequested)
			{
				 printf("Worker: doing transition work. ");
			}
			if(workRequired.inputAvailable)
			{
				//printf("in: len=%d, s=%s", strlen(workRequired.inputBuf), workRequired.inputBuf);
				printf("in: %c", workRequired.inputChar);
			}
			printf("\n");
		}
		printf("Going to sleep\n");
		nanosleep(&sleepTime, NULL);
	} while (1);


}


