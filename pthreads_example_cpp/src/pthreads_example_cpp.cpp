/*
 * Copyright (C) Your copyright notice.
 *
 * Author: Me
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the PG_ORGANIZATION nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY	THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS-IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <iostream>
#include <pthread.h>

using namespace std;
/*
 * Basic threading in CPP

int t1DataIn;

void *print_message(void *arg);
void *print_message(void *arg)
{
	int *data = (int *) arg;
	cout << "Threading. In:" << *data << endl;
}

int main(void) {

	pthread_t thread1;


	pthread_create(&thread1, NULL, &print_message, &t1DataIn);
	t1DataIn = 23;
    cout << "Hello World#######" << endl; // prints Hello World

    void *result1;
    pthread_join(thread1, &result1);

    return 0;
}
*/

// Full CPP class with pthreads
#include <time.h>
class MyThread
{
public:
	MyThread() : m_tid(0) {}

	void start()
	{
		pthread_create(&m_tid, NULL, helper, this);
	}

	void join()
	{
		pthread_join(m_tid, 0);
	}

private:
	static void *helper(void *arg)
	{
		MyThread* mt = reinterpret_cast<MyThread*>(arg);
		mt->doWork();
		return 0;
	}

	void doWork(void)
	{
		cout << "Doing work" << endl;
	}

	pthread_t m_tid;
};

int main(void)
{
	cout << "Hello world" << endl;

	MyThread mt;
	mt.start();

	timespec time;
	time.tv_sec = 2;
	time.tv_nsec = 0;
	nanosleep(&time, NULL);
	cout << "Hello world again." << endl;
	mt.join();
	return 0;
}

