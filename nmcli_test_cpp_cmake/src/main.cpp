/** @mainpage nmcli_test_cpp - None
 *
 * @author Me <anony@mo.us>
 * @version 1.0.0
**/


#include <stdio.h>
#include <unistd.h>

#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <list>
#include <iterator>
#include <algorithm>
#include <map>
#include <set>

#include <fcntl.h>

#include "WifiAccessPointDetails.h"

using namespace std;

const char separator = ':';
const string cmdScan = "nmcli device wifi rescan";
const string cmdWifiList = "nmcli -t -f SIGNAL,IN-USE,BARS,SSID,SECURITY device wifi list";
const string cmdConnList_Name_UUID = "nmcli -t -f NAME,UUID connection show";

int getWifiAccessPointsAsList(list<WifiAccessPointDetails>&wifiList);
int getDuplicateConnectionsAsUUIDs(
		set<string>&uuidsToReturn,
		const map<string,string>&conMap,
		const string& conNameSearch
		);
int getConnectionsAsMap(map<string,string>&connectionsNameUUID);

void testDuplicateConnections(void);
void testWifiObject(void);

/**
 * Main class of project nmcli_test_cpp
 *
 * @param argc the number of arguments
 * @param argv the arguments from the commandline
 * @returns exit code of the application
 */
int main(int argc, char **argv) {
	// print a greeting to the console
	printf("Hello World from nmcli_test_cpp_cmake!\n");

	//testWifiObject();
	//testDuplicateConnections();

	list<WifiAccessPointDetails>wifiList;
	getWifiAccessPointsAsList(wifiList);

	map<string,string>connectionsNameUUID;
	getConnectionsAsMap(connectionsNameUUID);

	set<string>duplicateConnectionsUUID_1;
	getDuplicateConnectionsAsUUIDs(duplicateConnectionsUUID_1, connectionsNameUUID, string("Cargt"));

	set<string>duplicateConnectionsUUID_guest;
	getDuplicateConnectionsAsUUIDs(duplicateConnectionsUUID_guest, connectionsNameUUID, string("Cargt Guest"));

	set<string>duplicateConnectionsUUID_2;
	getDuplicateConnectionsAsUUIDs(duplicateConnectionsUUID_2, connectionsNameUUID, string("Cargt2"));

	printf("Sorting by signal strength\n");
	wifiList.sort(WifiAccessPointDetails().greaterThan); // Highest Sig strength first etc
	for(list<WifiAccessPointDetails>::iterator iter = wifiList.begin(); iter != wifiList.end(); iter++)
	{
		string objString = (*iter).getObjectAsString();
		printf("ObjString=%s\n", objString.c_str());
	}

	// Try and connect to an AP
	const string connectCmd = "nmcli device wifi connect";
	const string reconnectCmd = "nmcli con up";
	const string passwordCmd = "password";

	const string ssidCargtGuest = "Cargt Guest";
	const string ssidCargt = "Cargt";
	const string ssidCargt2 = "Cargt2";
	const string pwdCargt = "31184bd9";

	uint8_t count = 1;
	do
	{
		string callString;

		switch (count)
		{
		case 1:
			callString = connectCmd + " " + "\"" + ssidCargtGuest + "\"";
			break;
		case 2:
			callString = connectCmd + " " + "\"" + ssidCargt + "\"" + " " + passwordCmd + " " + "\"" + pwdCargt + "\"";
			break;
		case 3:
			callString = connectCmd + " " + "\"" + ssidCargt2 + "\"" + " " + passwordCmd + " " + "\"" + pwdCargt + "\"";
			break;
		case 4:
			callString = reconnectCmd + " " + "\"" + ssidCargtGuest + "\"";
			break;
		case 5:
			callString = reconnectCmd + " " + "\"" + ssidCargt + "\"";
			break;
		case 6:
			callString = reconnectCmd + " " + "\"" + ssidCargt2 + "\"";
			break;
		}

		FILE *ptrnmcliResults;
		char buffer[1024];

		printf("Connect to:%s\n", callString.c_str());
		ptrnmcliResults = popen(callString.c_str(), "r");

		if(!ptrnmcliResults)
		{
			printf("Error with opening nmcli connect.\n");
			return -1;
		}

		while (NULL != fgets(buffer, sizeof(buffer), ptrnmcliResults))
		{
			printf("Ret Str=%s", buffer);
		}
		pclose(ptrnmcliResults);
		printf("Sleeping\n");
		sleep(10);
		if (count == 6) count = 1; else ++count;
	} while(1);

	return 0;
}


int getWifiAccessPointsAsList(list<WifiAccessPointDetails>&wifiList)
{
	FILE *ptrnmcliResults;
	char buffer[1024];

	ptrnmcliResults = popen(cmdWifiList.c_str(), "r");

	if(!ptrnmcliResults)
	{
		printf("Error with opening nmcli command list.\n");
		return -1;
	}

	while (NULL != fgets(buffer, sizeof(buffer), ptrnmcliResults))
	{
		WifiAccessPointDetails tmpWifi = WifiAccessPointDetails(string(buffer));
		if (tmpWifi.isPopulated())
		{
			wifiList.push_back(tmpWifi);
		}
		//printf("Str=%s", buffer);
	}
	pclose(ptrnmcliResults);
	return 0;
}

int getDuplicateConnectionsAsUUIDs(
		set<string>&uuidsToReturn,
		const map<string,string>&conMap,
		const string& conNameSearch)
{
	map<string,string>::const_iterator iter;
	for(iter = conMap.begin(); iter != conMap.end(); ++iter)
	{
		size_t pos = iter->first.find(conNameSearch, 0);
		if (0 != pos) // not found at start
		{
			continue;
		}

		// Check next character is a space or null char
		pos = conNameSearch.length();
		if((' ' != iter->first[pos]) && ('\0' != iter->first[pos]))
			continue;

		if(' ' == iter->first[pos])
			++pos; // advance over space

		// Only numbers are allowed from this point up to null character
		// Null char will jump to adding to return set
		while( (iter->first[pos] >= '0') && (iter->first[pos] <= '9') )
		{
			++pos;
		}
		if('\0' == iter->first[pos])
		{
			uuidsToReturn.insert(iter->second);
			//printf("Dup=%s,%s\n", iter->first.c_str(), iter->second.c_str());
		}
	}

	return 0;
}

int getConnectionsAsMap(map<string,string>&connectionsNameUUID)
{
	FILE *ptrnmcliResults;
	char buffer[1024];

	ptrnmcliResults = popen(cmdConnList_Name_UUID.c_str(), "r");

	if(!ptrnmcliResults)
	{
		printf("Error with opening nmcli connection list.\n");
		return -1;
	}

	while (NULL != fgets(buffer, sizeof(buffer), ptrnmcliResults))
	{
		string responseString = string(buffer);
		size_t pos1 = responseString.find(separator, 0);

		size_t newLinePos = responseString.find('\n');
		string ssidStr =  responseString.substr(0, pos1 - 0);
		string uuidStr = responseString.substr(pos1 + 1, newLinePos - (pos1 + 1));

		connectionsNameUUID.insert(pair<string,string>(ssidStr,uuidStr));
		//printf("Str=%s|%s\n", ssidStr.c_str(), uuidStr.c_str());
	}
	pclose(ptrnmcliResults);
	return 0;
}

void testDuplicateConnections(void)
{
	printf("testDuplicateConnections:\n");

	set<string>duplicateUUIDs;

	map<string,string>connectionMap;

	connectionMap.insert(pair<string,string>("xxx","xxx-end"));
	connectionMap.insert(pair<string,string>("xxx2", "xxx2-end"));
	connectionMap.insert(pair<string,string>("xxx3", "xxx3-end"));

	connectionMap.insert(pair<string,string>("xxx 1","xxx-dup1&xxx 1-end"));

	connectionMap.insert(pair<string,string>("xxx 1 1", "xxx 1-dup1"));
	connectionMap.insert(pair<string,string>("xxx 1 2", "xxx 1-dup2"));

	connectionMap.insert(pair<string,string>("xxx1", "xxx1-end"));
	connectionMap.insert(pair<string,string>("xxx1 1", "xxx1-dup1"));
	connectionMap.insert(pair<string,string>("xxx1 2", "xxx1-dup2"));
	connectionMap.insert(pair<string,string>("xxx1 3", "xxx1-dup3"));

	connectionMap.insert(pair<string,string>("xxx xxx", "xxx-xxx-end"));
	connectionMap.insert(pair<string,string>("xxx xxx 1", "xxx-xxx-dup1"));
	connectionMap.insert(pair<string,string>("xxx xxx 2", "xxx-xxx-dup2"));
	printf("Map size=%lu\n", connectionMap.size());

	string searchSSID;
	duplicateUUIDs.clear();
	searchSSID = "xxx";
	getDuplicateConnectionsAsUUIDs(duplicateUUIDs, connectionMap, searchSSID);
	printf("Duplicate size=%lu, Base_ssid=%s,Dups=", duplicateUUIDs.size(), searchSSID.c_str());
	for(set<string>::iterator iterSet = duplicateUUIDs.begin(); iterSet != duplicateUUIDs.end(); ++iterSet)
	{
		printf("%s|", iterSet->c_str());
	}
	printf("\n");

	duplicateUUIDs.clear();
	searchSSID = "xxx1";
	getDuplicateConnectionsAsUUIDs(duplicateUUIDs, connectionMap, searchSSID);
	printf("Duplicate size=%lu, Base_ssid=%s,", duplicateUUIDs.size(), searchSSID.c_str());
	for(set<string>::iterator iterSet = duplicateUUIDs.begin(); iterSet != duplicateUUIDs.end(); ++iterSet)
	{
		printf("%s|", iterSet->c_str());
	}
	printf("\n");

	duplicateUUIDs.clear();
	searchSSID = "xxx 1";
	getDuplicateConnectionsAsUUIDs(duplicateUUIDs, connectionMap, searchSSID);
	printf("Duplicate size=%lu, Base_ssid=%s,", duplicateUUIDs.size(), searchSSID.c_str());
	for(set<string>::iterator iterSet = duplicateUUIDs.begin(); iterSet != duplicateUUIDs.end(); ++iterSet)
	{
		printf("%s|", iterSet->c_str());
	}
	printf("\n");

	duplicateUUIDs.clear();
	searchSSID = "xxx xxx";
	getDuplicateConnectionsAsUUIDs(duplicateUUIDs, connectionMap, searchSSID);
	printf("Duplicate size=%lu, Base_ssid=%s,", duplicateUUIDs.size(), searchSSID.c_str());
	for(set<string>::iterator iterSet = duplicateUUIDs.begin(); iterSet != duplicateUUIDs.end(); ++iterSet)
	{
		printf("%s|", iterSet->c_str());
	}
	printf("\n");
}

void testWifiObject(void)
{
	list<WifiAccessPointDetails> wifiTestList;
	printf("Number in list=%lu\n", wifiTestList.size());

	printf("Add to list.\n");
	for(int i = 0; i < 10; ++i)
	{
		wifiTestList.push_back(WifiAccessPointDetails().generateRandomObj());
		printf("Wifi_string=%s\n", wifiTestList.back().getObjectAsString().c_str());
	}

	printf("Sorting by signal strength");
	wifiTestList.sort();
	//wifiList.sort(wifiList.begin(), wifiList.end(), greater<WifiAccessPointDetails>());
	//std::sort(wifiList.begin(), wifiList.end(), greater<WifiAccessPointDetails>());

	for(list<WifiAccessPointDetails>::iterator iter = wifiTestList.begin(); iter != wifiTestList.end(); iter++)
	{
		string objString = (*iter).getObjectAsString();
		printf("ObjString=%s\n", objString.c_str());
	}

	list<WifiAccessPointDetails>::iterator test_iter = wifiTestList.begin();
	//test_iter += 3;
	//string test_string = (*test_iter).getObjectAsString();
	//printf("JumpString=%s\n", test_string);

	for (list<WifiAccessPointDetails>::reverse_iterator riter=wifiTestList.rbegin(); riter!=wifiTestList.rend(); ++riter)
	{
	    string objString = (*riter).getObjectAsString();
	    printf("RevString=%s\n", objString.c_str());
	}

	printf("Size=%lu\n", wifiTestList.size());

	list<WifiAccessPointDetails>::iterator new_iter = wifiTestList.begin();
	//advance(new_iter, wifiList.size()-1);
	advance(new_iter, 3);
	WifiAccessPointDetails tmpDetails;
	printf("%s\n", (*new_iter).getObjectAsString().c_str());

}

class NetworkConnections
{
	NetworkConnections() : m_populated(false) {};
	~NetworkConnections();

	void testFunction(void);

public:

private:
bool m_populated;
};
void NetworkConnections::testFunction(void)
{

}

// Execute command <cmd>, put its output (stdout and stderr) in <output>,
// and return its status
int exec_command(string& cmd, string& output) {
    // Save original stdout and stderr to enable restoring
    int org_stdout = dup(1);
    int org_stderr = dup(2);

    int pd[2];
    pipe(pd);

    // Make the read-end of the pipe non blocking, so if the command being
    // executed has no output the read() call won't get stuck
    int flags = fcntl(pd[0], F_GETFL);
    flags |= O_NONBLOCK;

    if(fcntl(pd[0], F_SETFL, flags) == -1) {
        throw string("fcntl() failed");
    }

    // Redirect stdout and stderr to the write-end of the pipe
    dup2(pd[1], 1);
    dup2(pd[1], 2);
    int status = system(cmd.c_str());
    int buf_size = 1000;
    char buf[buf_size];

    // Read from read-end of the pipe
    long num_bytes = read(pd[0], buf, buf_size);

    if(num_bytes > 0) {
        output.clear();
        output.append(buf, num_bytes);
    }

    // Restore stdout and stderr and release the org* descriptors
    dup2(org_stdout, 1);
    dup2(org_stderr, 2);
    close(org_stdout);
    close(org_stderr);

    return status;
}
