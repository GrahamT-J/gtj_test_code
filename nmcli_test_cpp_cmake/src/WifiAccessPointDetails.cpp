/*
 * WifiList.cpp
 *
 *  Created on: May 26, 2017
 *      Author: cargt
 */
#include "WifiAccessPointDetails.h"

#include <stdlib.h>
#include <algorithm>
#include <vector>
#include <string>
#include <sstream>

using namespace std;

const char alphaNum[] =
	"0123456789"
	"!@#$%^&*"
	"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	"abcdefghijklmnopqrstuvwxyz";

const char *sec1 = "WPA2";
const char *sec2 = "WPA1";
const char *sec3 = "WPA1 WPA2";
const char *sec4 = "WPA2 802.1X";
const char *sec5 = "";
const char *security[] = { sec1, sec2, sec3, sec4, sec5 };

const char guiNoBarChar = 0x5f;
const char asciiBarChar = '*';

WifiAccessPointDetails::WifiAccessPointDetails() :
	m_populated(false)
	, m_signalStrength(0)
	, m_connectionInUse(false)
	, m_numberOfBars(0)
	, m_SSID()
	, m_security()
{

}

WifiAccessPointDetails::WifiAccessPointDetails(const string& formattedInput)
{
	// Note security is the last field and can be blank(null)

	// Empty object to start just in case there are errors
	m_signalStrength = 0;
	m_connectionInUse = false;
	m_numberOfBars = 0;
	m_SSID = string();
	m_security = string();
	m_populated = false;

	// Format is per the format string command
	int tmp_signalStrength;
	bool tmp_connectionInUse;
	uint8_t tmp_numberOfBars;
	vector<char> tmp_SSID;
	vector<char> tmp_security;

	// Fields= SIGNAL,IN-USE,BARS,SSID,SECURITY
	// Find the relevant separators - ':' colon character
	size_t pos1 = formattedInput.find(separator, 0);
	size_t pos2 = formattedInput.find(separator, pos1 + 1);
	size_t pos3 = formattedInput.find(separator, pos2 + 1);
	size_t pos4 = formattedInput.find(separator, pos3 + 1);

	size_t newLinePos = formattedInput.find('\n');

	// Check that none are equal to end
	// Security can be null / empty so separator could be last in buffer
	if((pos1 == string::npos)
		|| (pos2 == string::npos)
		|| (pos3 == string::npos)
		|| (pos4 == string::npos))
		return; // error so obj not populated


	string sigStr =  formattedInput.substr(0, pos1 - 0);
	string useStr =  formattedInput.substr(pos1+1, pos2 - (pos1 + 1));
	string barsStr = formattedInput.substr(pos2+1, pos3 - (pos2 + 1));
	string SSID =    formattedInput.substr(pos3+1, pos4 - (pos3 + 1));
	string security = formattedInput.substr(pos4+1, newLinePos - (pos4 + 1) );

	// Convert the necessary values
    tmp_signalStrength = strtol(sigStr.c_str(), NULL, 10);
    tmp_connectionInUse = (useStr[0] == '*') ? true : false;
    if(4 == barsStr.size())
    {
    	// Simple ascii string of * and space
        tmp_numberOfBars = count(barsStr.begin(), barsStr.end(), asciiBarChar);
    }
    else
    {
    	// Complex gui string of bar heights. No bar is single char so count those
    	uint8_t countNoBars = count(barsStr.begin(), barsStr.end(), guiNoBarChar);
    	tmp_numberOfBars = 4 - countNoBars;
    }

	// Save values to object
	m_signalStrength = tmp_signalStrength;
	m_connectionInUse = tmp_connectionInUse;
	m_numberOfBars = tmp_numberOfBars;
	m_SSID = SSID;
	m_security = security;
	m_populated = true;
}

WifiAccessPointDetails& WifiAccessPointDetails::operator=(const WifiAccessPointDetails& inObj)
{
	if(this == &inObj)
		return *this;

	m_populated = inObj.m_populated;
	m_signalStrength = inObj.m_signalStrength;
	m_connectionInUse = inObj.m_connectionInUse;
	m_numberOfBars = inObj.m_numberOfBars;
	m_SSID = inObj.m_SSID;
	m_security = inObj.m_security;

	return *this;
}


//bool operator==(const WifiAccessPointDetails& inObj);
//bool operator!=(const WifiAccessPointDetails& inObj);
bool WifiAccessPointDetails::operator<(const WifiAccessPointDetails& rhs)
{
	//printf("SS=%d|%d,", this->m_signalStrength, rhs.m_signalStrength);
	if (this->m_signalStrength < rhs.m_signalStrength)
		return true;
	else if (this->m_signalStrength > rhs.m_signalStrength)
		return false;
	// equal continue checking

	//printf("Con=%d|%d,", this->m_connectionInUse, rhs.m_connectionInUse);
	if (this->m_connectionInUse < rhs.m_connectionInUse)
		return true;
	else if (this->m_connectionInUse > rhs.m_connectionInUse)
		return false;
	// equal continue checking

	//printf("Bars=%d|%d,", this->m_numberOfBars, rhs.m_numberOfBars);
	if (this->m_numberOfBars < rhs.m_numberOfBars)
		return true;
	else if (this->m_numberOfBars > rhs.m_numberOfBars)
		return false;
	// equal continue checking

	//printf("SSID=%s|%s,", this->m_SSID.c_str(), rhs.m_SSID.c_str());
	if (this->m_SSID < rhs.m_SSID)
		return true;
	else if (this->m_SSID > rhs.m_SSID)
		return false;
	// equal continue checking

	//printf("sec=%s|%s,", this->m_security.c_str(), rhs.m_security.c_str());
	if (this->m_security < rhs.m_security)
		return true;
	else if (this->m_security > rhs.m_SSID)
		return false;
	// equal continue checking

	//printf("pop=%d|%d,", this->m_populated, rhs.m_populated);
	if (this->m_populated < rhs.m_populated)
		return true;

	printf("false\n");
	return false;
}

bool WifiAccessPointDetails::greaterThan(const WifiAccessPointDetails &lhs, const WifiAccessPointDetails& rhs)
{
	//printf("SS=%d|%d,", lhs.m_signalStrength, rhs.m_signalStrength);
	if (lhs.m_signalStrength > rhs.m_signalStrength)
		return true;
	else if (lhs.m_signalStrength < rhs.m_signalStrength)
		return false;
	// equal continue checking

	//printf("Con=%d|%d,", lhs.m_connectionInUse, rhs.m_connectionInUse);
	if (lhs.m_connectionInUse > rhs.m_connectionInUse)
		return true;
	else if (lhs.m_connectionInUse < rhs.m_connectionInUse)
		return false;
	// equal continue checking

	//printf("Bars=%d|%d,", lhs.m_numberOfBars, rhs.m_numberOfBars);
	if (lhs.m_numberOfBars > rhs.m_numberOfBars)
		return true;
	else if (lhs.m_numberOfBars < rhs.m_numberOfBars)
		return false;
	// equal continue checking

	//printf("SSID=%s|%s,", lhs.m_SSID.c_str(), rhs.m_SSID.c_str());
	if (lhs.m_SSID > rhs.m_SSID)
		return true;
	else if (lhs.m_SSID < rhs.m_SSID)
		return false;
	// equal continue checking

	//printf("sec=%s|%s,", lhs.m_security.c_str(), rhs.m_security.c_str());
	if (lhs.m_security > rhs.m_security)
		return true;
	else if (lhs.m_security > rhs.m_SSID)
		return false;
	// equal continue checking

	//printf("pop=%d|%d,", lhs.m_populated, rhs.m_populated);
	if (lhs.m_populated > rhs.m_populated)
		return true;

	//printf("false\n");
	return false;
}

string WifiAccessPointDetails::getFormatStringForScanCommand()
{
	return ("-t -f SIGNAL,IN-USE,BARS,SSID,SECURITY");
}

string WifiAccessPointDetails::getObjectAsString()
{
	string retString;

	stringstream ss;
	ss << (int) m_signalStrength;
	retString += ss.str();
	retString += separator;

	retString += (m_connectionInUse == true) ? '*' : ' ';
	retString += separator;

	for(int i = 1; i <= 4; ++i)
	{
	    retString += (i <= m_numberOfBars) ? '*' : ' ';
	}
	retString += separator;

	retString += string(m_SSID.begin(), m_SSID.end());
	retString += separator;

	retString += string(m_security.begin(), m_security.end());
	// Note no separator after last field

	return retString;
}

string WifiAccessPointDetails::generateRandomAPString()
{
	string randomFormattedString;

	stringstream ss;
	uint8_t sigStrength = (uint8_t) rand();
	sigStrength = 10;
	// Signal strength
	ss << (int) sigStrength;
	randomFormattedString += ss.str();
	randomFormattedString += WifiAccessPointDetails::separator;
	// is connected?
	randomFormattedString += ((rand() % 2) ? '*' : ' ');
	randomFormattedString += WifiAccessPointDetails::separator;

	// Random Bars
	uint8_t randomBars = (rand() % 5); // Max of 4 bars - String is 4 chars of * or space
	string barString;
	for(int i = 1; i <= 4; ++i)
	{
		barString += (i <= randomBars) ? '*' : ' ';
	}
	randomFormattedString += barString;
	randomFormattedString += WifiAccessPointDetails::separator;

	uint8_t randomSSIDLength = ((unsigned int)rand()) % (MAX_SSID_LENGTH + 1);
	for (int i = 0; i < randomSSIDLength; ++i)
	{
		unsigned int idx = ((unsigned int)rand()) % (sizeof alphaNum - 1); // -1 due to \0 termination not array [0-N]
		char randomChar = alphaNum[idx];
		randomFormattedString += randomChar;
	}
	randomFormattedString += WifiAccessPointDetails::separator;

	uint8_t idx = ((unsigned int)rand()) % (sizeof security / sizeof (char *)); // get number of entries
	randomFormattedString += security[idx];
	// Note no separator after

	return randomFormattedString;
}

WifiAccessPointDetails WifiAccessPointDetails::generateRandomObj()
{
	return WifiAccessPointDetails(generateRandomAPString());
}

