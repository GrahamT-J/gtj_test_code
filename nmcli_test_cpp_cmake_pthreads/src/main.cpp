/** @mainpage nmcli_test_cpp - None
 *
 * @author Me <anony@mo.us>
 * @version 1.0.0
**/


#include <stdio.h>
#include <unistd.h>

#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <list>
#include <iterator>
#include <algorithm>
#include <map>
#include <set>

#include <string.h>
#include <pthread.h>

#include <atomic>

#include <fcntl.h>

#include "WifiAccessPointDetails.h"

using namespace std;

const char separator = ':';
const string cmdScan = "nmcli device wifi rescan";
const string cmdWifiList = "nmcli -t -f SIGNAL,IN-USE,BARS,SSID,SECURITY device wifi list";
const string cmdConnDelete         = "nmcli connection delete";
const string cmdConnList_NAME      = "nmcli -t -f NAME connection show";
const string cmdConnList_NAME_UUID = "nmcli -t -f NAME,UUID connection show";
const string cmdConnList_NAME_TYPE = "nmcli -t -f NAME,TYPE connection show";

int getWifiAccessPointsAsList(list<WifiAccessPointDetails>&wifiList);
int getDuplicateConnectionsAsUUIDs(
		set<string>&uuidsToReturn,
		const map<string,string>&conMap,
		const string& conNameSearch
		);
int getConnectionsAsMap(map<string,string>&connectionsNameUUID);
int getConnections_SSID(set<string>&ssidsFound);
int getConnectionsByType(const string& searchTypeString, set<string>&connectionsNameSSID);

void printSetOfStrings(const set<string>& in);
void testDuplicateConnections(void);
void testWifiObject(void);

int exec_command(string& cmd, string& output);
int exec_command_with_pthread(string& cmd, string& output);

struct thread_data_t
{
	int pd[2];
	bool thread_complete;
	int status;
	string commandString;
	string resultString;
};
struct thread_data_t thread_data;

/**
 * Main class of project nmcli_test_cpp
 *
 * @param argc the number of arguments
 * @param argv the arguments from the commandline
 * @returns exit code of the application
 */
int main(int argc, char **argv) {
	// print a greeting to the console
	printf("Hello World from nmcli_test_cpp_cmake_pthreads!\n");

	//testWifiObject();
	//testDuplicateConnections();

	set<string>ssidsOfType;
	string typeString = "802-11-wireless";
	printf("Get connections by type:%s\n", typeString.c_str());
	getConnectionsByType(typeString, ssidsOfType);
	printf("Make delete command\n");
	string deleteCmdString = cmdConnDelete;
	for(set<string>::iterator iter = ssidsOfType.begin(); iter != ssidsOfType.end(); ++iter)
	{
		deleteCmdString += " \"";
		deleteCmdString += *iter;
		deleteCmdString += "\"";
	}
	printf("Del cnt=%lu,cmd=%s\n", ssidsOfType.size(), deleteCmdString.c_str());

	string responseString;
	int status = exec_command(deleteCmdString, responseString);
	printf("ret=%d,resp=%s\n", status, responseString.c_str());
return 0;


	list<WifiAccessPointDetails>wifiList;
	getWifiAccessPointsAsList(wifiList);

	map<string,string>connectionsNameUUID;
	getConnectionsAsMap(connectionsNameUUID);

	printf("Printing duplicate connections by UUIDs\n");
	set<string>duplicateConnectionsUUID_1;
	getDuplicateConnectionsAsUUIDs(duplicateConnectionsUUID_1, connectionsNameUUID, string("Cargt"));
	printSetOfStrings(duplicateConnectionsUUID_1);

	set<string>duplicateConnectionsUUID_guest;
	getDuplicateConnectionsAsUUIDs(duplicateConnectionsUUID_guest, connectionsNameUUID, string("Cargt Guest"));
	printSetOfStrings(duplicateConnectionsUUID_guest);

	set<string>duplicateConnectionsUUID_2;
	getDuplicateConnectionsAsUUIDs(duplicateConnectionsUUID_2, connectionsNameUUID, string("Cargt2"));
	printSetOfStrings(duplicateConnectionsUUID_2);

	printf("Sorting by signal strength\n");
	wifiList.sort(WifiAccessPointDetails().greaterThan); // Highest Sig strength first etc
	for(list<WifiAccessPointDetails>::iterator iter = wifiList.begin(); iter != wifiList.end(); iter++)
	{
		string objString = (*iter).getObjectAsString();
		printf("ObjString=%s\n", objString.c_str());
	}

	// Try and connect to an AP
	const string connectCmd = "nmcli device wifi connect";
	const string reconnectCmd = "nmcli con up";
	const string passwordCmd = "password";

	const string ssidCargtGuest = "Cargt Guest";
	const string ssidCargt = "Cargt";
	const string ssidCargt2 = "Cargt2";
	const string pwdCargt = "31184bd9";

	// Try and use exec_command
	//int exec_command(string& cmd, string& output);
	string cmdString = connectCmd + " " + "\"" + ssidCargt + "\"" + " " + passwordCmd + " " + "\"" + pwdCargt + "\"";
	string resultString;
	printf("Using exec_command_with_pthread for:%s\n", cmdString.c_str());
	int cmd_ret = exec_command_with_pthread(cmdString, resultString);
	//printf("exec_command1=%d,string=%s\n", thread_data.status, thread_data.resultString.c_str());
	printf("exec_command1=%d,string=%s\n", cmd_ret, resultString.c_str());

	printf("Using exec_command for:%s\n", cmdString.c_str());
	cmd_ret = exec_command(cmdString, resultString);
	printf("exec_command2=%d,string=%s\n", cmd_ret, resultString.c_str());
	sleep(5);

	uint8_t count = 1;
	do
	{
		string callString;

		switch (count)
		{
		case 1:
			callString = connectCmd + " " + "\"" + ssidCargtGuest + "\"";
			break;
		case 2:
			callString = connectCmd + " " + "\"" + ssidCargt + "\"" + " " + passwordCmd + " " + "\"" + pwdCargt + "\"";
			break;
		case 3:
			callString = connectCmd + " " + "\"" + ssidCargt2 + "\"" + " " + passwordCmd + " " + "\"" + pwdCargt + "\"";
			break;
		case 4:
			callString = reconnectCmd + " " + "\"" + ssidCargtGuest + "\"";
			break;
		case 5:
			callString = reconnectCmd + " " + "\"" + ssidCargt + "\"";
			break;
		case 6:
			callString = reconnectCmd + " " + "\"" + ssidCargt2 + "\"";
			break;
		}

		FILE *ptrnmcliResults;
		char buffer[1024];

		printf("Connect to:%s\n", callString.c_str());
		ptrnmcliResults = popen(callString.c_str(), "r");

		if(!ptrnmcliResults)
		{
			printf("Error with opening nmcli connect.\n");
			return -1;
		}

		while (NULL != fgets(buffer, sizeof(buffer), ptrnmcliResults))
		{
			printf("Ret Str=%s", buffer);
		}
		pclose(ptrnmcliResults);
		printf("Sleeping\n");
		sleep(10);
		if (count == 6) count = 1; else ++count;
	} while(1);

	return 0;
}


int getWifiAccessPointsAsList(list<WifiAccessPointDetails>&wifiList)
{
	FILE *ptrnmcliResults;
	char buffer[1024];

	ptrnmcliResults = popen(cmdWifiList.c_str(), "r");

	if(!ptrnmcliResults)
	{
		printf("Error with opening nmcli command list.\n");
		return -1;
	}

	while (NULL != fgets(buffer, sizeof(buffer), ptrnmcliResults))
	{
		WifiAccessPointDetails tmpWifi = WifiAccessPointDetails(string(buffer));
		if (tmpWifi.isPopulated())
		{
			wifiList.push_back(tmpWifi);
		}
		//printf("Str=%s", buffer);
	}
	pclose(ptrnmcliResults);
	return 0;
}

int getDuplicateConnectionsAsUUIDs(
		set<string>&uuidsToReturn,
		const map<string,string>&conMap,
		const string& conNameSearch)
{
	map<string,string>::const_iterator iter;
	for(iter = conMap.begin(); iter != conMap.end(); ++iter)
	{
		size_t pos = iter->first.find(conNameSearch, 0);
		if (0 != pos) // not found at start
		{
			continue;
		}

		// Check next character is a space or null char
		pos = conNameSearch.length();
		if((' ' != iter->first[pos]) && ('\0' != iter->first[pos]))
			continue;

		if(' ' == iter->first[pos])
			++pos; // advance over space

		// Only numbers are allowed from this point up to null character
		// Null char will jump to adding to return set
		while( (iter->first[pos] >= '0') && (iter->first[pos] <= '9') )
		{
			++pos;
		}
		if('\0' == iter->first[pos])
		{
			uuidsToReturn.insert(iter->second);
			//printf("Dup=%s,%s\n", iter->first.c_str(), iter->second.c_str());
		}
	}

	return 0;
}

int getConnections_SSID(set<string>&ssidsFound)
{
	FILE *ptrnmcliResults;
	char buffer[1024];

	ssidsFound.clear();
	ptrnmcliResults = popen(cmdConnList_NAME.c_str(), "r");

	if(!ptrnmcliResults)
	{
		printf("Error with opening nmcli connection list.\n");
		return -1;
	}

	while (NULL != fgets(buffer, sizeof(buffer), ptrnmcliResults))
	{
		string responseString = string(buffer);

		size_t newLinePos = responseString.find('\n');
		string ssidStr =  responseString.substr(0, newLinePos);

		ssidsFound.insert(ssidStr);
		printf("Str=%s\n", ssidStr.c_str());
	}
	pclose(ptrnmcliResults);
	return 0;
}

int getConnectionsByType(const string& searchTypeString, set<string>&connectionsNameSSID)
{
	FILE *ptrnmcliResults;
	char buffer[1024];

	connectionsNameSSID.clear();
	ptrnmcliResults = popen(cmdConnList_NAME_TYPE.c_str(), "r");

	if(!ptrnmcliResults)
	{
		printf("Error with opening nmcli connection list.\n");
		return -1;
	}

	while (NULL != fgets(buffer, sizeof(buffer), ptrnmcliResults))
	{
		string responseString = string(buffer);
		size_t pos1 = responseString.find(separator, 0);

		size_t newLinePos = responseString.find('\n');
		string field1Str =  responseString.substr(0, pos1 - 0);
		string field2Str = responseString.substr(pos1 + 1, newLinePos - (pos1 + 1));
		//printf("resp=%s,fld1=%s,fld2=%s\n", responseString.c_str(), field1Str.c_str(), field2Str.c_str());
		if(searchTypeString == field2Str)
		{
			connectionsNameSSID.insert(field1Str);
			//printf("Str=%s|%s\n", field1Str.c_str(), field2Str.c_str());
		}
	}
	pclose(ptrnmcliResults);
	return 0;
}

int getConnectionsAsMap(map<string,string>&connectionsNameUUID)
{
	FILE *ptrnmcliResults;
	char buffer[1024];

	ptrnmcliResults = popen(cmdConnList_NAME_UUID.c_str(), "r");

	if(!ptrnmcliResults)
	{
		printf("Error with opening nmcli connection list.\n");
		return -1;
	}

	while (NULL != fgets(buffer, sizeof(buffer), ptrnmcliResults))
	{
		string responseString = string(buffer);
		size_t pos1 = responseString.find(separator, 0);

		size_t newLinePos = responseString.find('\n');
		string ssidStr =  responseString.substr(0, pos1 - 0);
		string uuidStr = responseString.substr(pos1 + 1, newLinePos - (pos1 + 1));

		connectionsNameUUID.insert(pair<string,string>(ssidStr,uuidStr));
		//printf("Str=%s|%s\n", ssidStr.c_str(), uuidStr.c_str());
	}
	pclose(ptrnmcliResults);
	return 0;
}

void printSetOfStrings(const set<string>& in)
{
	printf("string set=");
	for(set<string>::iterator iter = in.begin(); iter != in.end(); ++iter)
	{
		printf("%s\n", (*iter).c_str());
	}
}

void testDuplicateConnections(void)
{
	printf("testDuplicateConnections:\n");

	set<string>duplicateUUIDs;

	map<string,string>connectionMap;

	connectionMap.insert(pair<string,string>("xxx","xxx-end"));
	connectionMap.insert(pair<string,string>("xxx2", "xxx2-end"));
	connectionMap.insert(pair<string,string>("xxx3", "xxx3-end"));

	connectionMap.insert(pair<string,string>("xxx 1","xxx-dup1&xxx 1-end"));

	connectionMap.insert(pair<string,string>("xxx 1 1", "xxx 1-dup1"));
	connectionMap.insert(pair<string,string>("xxx 1 2", "xxx 1-dup2"));

	connectionMap.insert(pair<string,string>("xxx1", "xxx1-end"));
	connectionMap.insert(pair<string,string>("xxx1 1", "xxx1-dup1"));
	connectionMap.insert(pair<string,string>("xxx1 2", "xxx1-dup2"));
	connectionMap.insert(pair<string,string>("xxx1 3", "xxx1-dup3"));

	connectionMap.insert(pair<string,string>("xxx xxx", "xxx-xxx-end"));
	connectionMap.insert(pair<string,string>("xxx xxx 1", "xxx-xxx-dup1"));
	connectionMap.insert(pair<string,string>("xxx xxx 2", "xxx-xxx-dup2"));
	printf("Map size=%lu\n", connectionMap.size());

	string searchSSID;
	duplicateUUIDs.clear();
	searchSSID = "xxx";
	getDuplicateConnectionsAsUUIDs(duplicateUUIDs, connectionMap, searchSSID);
	printf("Duplicate size=%lu, Base_ssid=%s,Dups=", duplicateUUIDs.size(), searchSSID.c_str());
	for(set<string>::iterator iterSet = duplicateUUIDs.begin(); iterSet != duplicateUUIDs.end(); ++iterSet)
	{
		printf("%s|", iterSet->c_str());
	}
	printf("\n");

	duplicateUUIDs.clear();
	searchSSID = "xxx1";
	getDuplicateConnectionsAsUUIDs(duplicateUUIDs, connectionMap, searchSSID);
	printf("Duplicate size=%lu, Base_ssid=%s,", duplicateUUIDs.size(), searchSSID.c_str());
	for(set<string>::iterator iterSet = duplicateUUIDs.begin(); iterSet != duplicateUUIDs.end(); ++iterSet)
	{
		printf("%s|", iterSet->c_str());
	}
	printf("\n");

	duplicateUUIDs.clear();
	searchSSID = "xxx 1";
	getDuplicateConnectionsAsUUIDs(duplicateUUIDs, connectionMap, searchSSID);
	printf("Duplicate size=%lu, Base_ssid=%s,", duplicateUUIDs.size(), searchSSID.c_str());
	for(set<string>::iterator iterSet = duplicateUUIDs.begin(); iterSet != duplicateUUIDs.end(); ++iterSet)
	{
		printf("%s|", iterSet->c_str());
	}
	printf("\n");

	duplicateUUIDs.clear();
	searchSSID = "xxx xxx";
	getDuplicateConnectionsAsUUIDs(duplicateUUIDs, connectionMap, searchSSID);
	printf("Duplicate size=%lu, Base_ssid=%s,", duplicateUUIDs.size(), searchSSID.c_str());
	for(set<string>::iterator iterSet = duplicateUUIDs.begin(); iterSet != duplicateUUIDs.end(); ++iterSet)
	{
		printf("%s|", iterSet->c_str());
	}
	printf("\n");
}

void testWifiObject(void)
{
	list<WifiAccessPointDetails> wifiTestList;
	printf("Number in list=%lu\n", wifiTestList.size());

	printf("Add to list.\n");
	for(int i = 0; i < 10; ++i)
	{
		wifiTestList.push_back(WifiAccessPointDetails().generateRandomObj());
		printf("Wifi_string=%s\n", wifiTestList.back().getObjectAsString().c_str());
	}

	printf("Sorting by signal strength");
	wifiTestList.sort();
	//wifiList.sort(wifiList.begin(), wifiList.end(), greater<WifiAccessPointDetails>());
	//std::sort(wifiList.begin(), wifiList.end(), greater<WifiAccessPointDetails>());

	for(list<WifiAccessPointDetails>::iterator iter = wifiTestList.begin(); iter != wifiTestList.end(); iter++)
	{
		string objString = (*iter).getObjectAsString();
		printf("ObjString=%s\n", objString.c_str());
	}

	list<WifiAccessPointDetails>::iterator test_iter = wifiTestList.begin();
	//test_iter += 3;
	//string test_string = (*test_iter).getObjectAsString();
	//printf("JumpString=%s\n", test_string);

	for (list<WifiAccessPointDetails>::reverse_iterator riter=wifiTestList.rbegin(); riter!=wifiTestList.rend(); ++riter)
	{
	    string objString = (*riter).getObjectAsString();
	    printf("RevString=%s\n", objString.c_str());
	}

	printf("Size=%lu\n", wifiTestList.size());

	list<WifiAccessPointDetails>::iterator new_iter = wifiTestList.begin();
	//advance(new_iter, wifiList.size()-1);
	advance(new_iter, 3);
	WifiAccessPointDetails tmpDetails;
	printf("%s\n", (*new_iter).getObjectAsString().c_str());

}

class NetworkConnections
{
	NetworkConnections() : m_populated(false) {};
	~NetworkConnections();

	void testFunction(void);

public:

private:
bool m_populated;
};
void NetworkConnections::testFunction(void)
{

}

// Execute command <cmd>, put its output (stdout and stderr) in <output>,
// and return its status
int exec_command(string& cmd, string& output)
{
    // Save original stdout and stderr to enable restoring
    int org_stdout = dup(1);
    int org_stderr = dup(2);

    int pd[2];
    pipe(pd);

    // Make the read-end of the pipe non blocking, so if the command being
    // executed has no output the read() call won't get stuck
    int flags = fcntl(pd[0], F_GETFL);
    flags |= O_NONBLOCK;

    if(fcntl(pd[0], F_SETFL, flags) == -1) {
        throw string("fcntl() failed");
    }

    // Redirect stdout and stderr to the write-end of the pipe
    dup2(pd[1], 1);
    dup2(pd[1], 2);
    int status = system(cmd.c_str());
    int buf_size = 1000;
    char buf[buf_size];

    // Read from read-end of the pipe
    long num_bytes = read(pd[0], buf, buf_size);

    if(num_bytes > 0) {
        output.clear();
        output.append(buf, num_bytes);
    }

    // Restore stdout and stderr and release the org* descriptors
    dup2(org_stdout, 1);
    dup2(org_stderr, 2);
    close(org_stdout);
    close(org_stderr);

    return status;
}

void* processWifiCommand(void* ptr)
{
	struct thread_data_t *thread_data = (thread_data_t *)ptr;

    //printf("wifiThread:executing command\n");
    //printf("pd0=%d,pd1=%d\n", thread_data->pd[0], thread_data->pd[1]);
    int sys_ret = system(thread_data->commandString.c_str());
    //printf("wifiThread:sys_ret=%d\n", sys_ret);
    thread_data->status = sys_ret;
    //printf("wifiThread:Status return=%d\n", thread_data->status);

    thread_data->thread_complete = true;
	return 0;
}

int exec_command_with_pthread(string& cmd, string& output)
{
    pthread_t pidWifiThread;
    memset(&thread_data, 0, sizeof thread_data);

    pipe(thread_data.pd);

    // Save original stdout and stderr to enable restoring
    int org_stdout = dup(1);
    int org_stderr = dup(2);

    // Make the read-end of the pipe non blocking, so if the command being
    // executed has no output the read() call won't get stuck
    int flags = fcntl(thread_data.pd[0], F_GETFL);
    flags |= O_NONBLOCK;

    if(fcntl(thread_data.pd[0], F_SETFL, flags) == -1) {
        throw string("fcntl() failed");
    }


    printf("pd0=%d,pd1=%d\n", thread_data.pd[0], thread_data.pd[1]);

    thread_data.commandString = cmd;
    // Redirect stdout and stderr to the write-end of the pipe
    dup2(thread_data.pd[1], 1); // stdout
    dup2(thread_data.pd[1], 2); // stderr

    pthread_create(&pidWifiThread, NULL, processWifiCommand, &thread_data);

    int buf_size = 1000;
    char buf[buf_size];
    unsigned int num_bytes_in_buf = 0;

    // stdout is now redirected so no printf's
    //printf("Wait for thread complete flag\n");
    while(!thread_data.thread_complete)
    {
    	//printf(".");
    	sleep(1);
    }
    //printf("\n");
    // Read from read-end of the pipe
    //printf("Reading from pipe 1st time\n");
    long num_bytes = read(thread_data.pd[0], buf, buf_size);
    num_bytes_in_buf += num_bytes;
    //printf("nbytes=%ld, bytes=", num_bytes);
    for(int i = 0; i < num_bytes; ++i)
    {
    	//printf("%02x,", buf[i]);
    	//printf("%c", buf[i]);
    }

    //printf("\n");

    if(num_bytes > 0) {
        output.clear();
        output.append(buf, num_bytes);
    }

    // Read from read-end of the pipe
    //printf("Reading from pipe 2nd time\n");
    num_bytes = read(thread_data.pd[0], &buf[num_bytes_in_buf], buf_size);
    num_bytes_in_buf += num_bytes;
    //printf("nbytes=%ld, bytes=", num_bytes);
    for(int i = 0; i < num_bytes; ++i)
    {
    	//printf("%02x,", buf[i]);
    	//printf("%c", buf[i]);
    }

    //printf("\n");


    //printf("Waiting for thread completion\n");
    pthread_join(pidWifiThread, NULL);

    // Restore stdout and stderr and release the org* descriptors
    dup2(org_stdout, 1);
    dup2(org_stderr, 2);
    close(org_stdout);
    close(org_stderr);

    printf("buffer contents=");
    for(unsigned int i = 0; i <= num_bytes_in_buf; ++i)
    {
    	printf("%02x,",buf[i]);
    }
    printf("\n");
    printf("buf ascii=");
    for(unsigned int i = 0; i <= num_bytes_in_buf; ++i)
    {
    	printf("%c", buf[i]);
    }
    printf("\n");

    printf("Reading from pipe after join\n");
    num_bytes = read(thread_data.pd[0], buf, buf_size);
    printf("nbytes=%ld, bytes=", num_bytes);
    for(int i = 0; i < num_bytes; ++i)
    	printf("%02x,", buf[i]);
    printf("\n");

    for(int i = 0; i < num_bytes; ++i)
    	printf("%c", buf[i]);
    printf("\n");


    printf("num_bytes into output string=%ld\n", num_bytes);
    if(num_bytes > 0) {
        //output.clear();
        output.append(buf, num_bytes);
    }
    printf("Output string=%s\n", output.c_str());

    return thread_data.status;
}
