/*
 * main.h
 *
 *  Created on: Aug 14, 2017
 *      Author: cargt
 */

#ifndef MAIN_H_
#define MAIN_H_

#define DB_FILENAME "/home/root/doorbell.db"
#define INOTIFY_PATH_SETTINGS DB_FILENAME

#define RX_EVNT_SYSTEM_ARMED_FILE_0    "/home/root/dmp/sysmonitor/armed"
#define RX_EVNT_SYSTEM_DISARMED_FILE_0 "/home/root/dmp/sysmonitor/disarmed"

#define RX_EVNT_ALARM_CONDITION_ON_FILE_0   "/home/root/dmp/sysmonitor/alarm_condition_on"
#define RX_EVNT_ALARM_CONDITION_OFF_FILE_0  "/home/root/dmp/sysmonitor/alarm_condition_off"

//*************************************
// Messages
#define MAX_FILE_MESSAGE_LENGTH 20

#define ARMED_MESSAGE       "armed"
#define DISARMED_MESSAGE    "disarmed"

#define ALARM_CONDITION_ON_MESSAGE  "started"
#define ALARM_CONDITION_OFF_MESSAGE "stopped"

#endif /* MAIN_H_ */
