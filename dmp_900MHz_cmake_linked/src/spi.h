/*H*****************************************************************************
FILENAME: spi.h

DESCRIPTION: spi access functions

NOTES:

Copyright (c) Cargt Inc 2017  All rights reserved.

	Last change:
*****************************************************************************H*/

#ifndef __SPI_H__
#define __SPI_H__

/*----compilation control-----------------------------------------------------*/

/*******************************************************************************
INCLUDE FILES   Note: Minimize nesting of header files.
*******************************************************************************/
/*----system and platform files-----------------------------------------------*/

/*----program files-----------------------------------------------------------*/
#include <stddef.h>
#include <stdint.h>

#include "types.h"

/*******************************************************************************
FILE CONTEXT
*******************************************************************************/
/*----defines, typedefs, structs, unions, enums-------------------------------*/

#define SPI_BITS_PER_WORD   8
#define SPI_CLOCK_SPEED_HZ (2.5 * 1000 * 1000)

/*----data descriptions-------------------------------------------------------*/

/*----data declarations-------------------------------------------------------*/

/*----macros------------------------------------------------------------------*/

/*----function prototypes-----------------------------------------------------*/
extern uint8_t readSpi(uint8_t headerByte, char* buffer, const size_t nbytes, int fd_spi);

extern char writeSpi(uint8_t headerByte, const char* buffer, const size_t nbytes, int fd_spi);

#endif
