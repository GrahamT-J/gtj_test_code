/** @mainpage dmp_900MHz_cmake_linked - None
 *
 * @author Me <anony@mo.us>
 * @version 1.0.0
**/


#include <stdio.h>
#include <stdlib.h>

#include "doorbell_inotify_inputs.h"
#include "settings.h"

#ifdef __cplusplus
extern "C" {
#endif
    #include "Main.h"
    #include "gpio_linux.h"
#ifdef __cplusplus
}
#endif

#define CS_USES_GPIO
#ifdef CS_USES_GPIO
    #define CS_ACTIVE   0
    #define CS_INACTIVE 1
#endif

void usage()
{
	printf("Parameters required:\n<spi dev number 0 or 1>\tchooses device spi0.0 or 1.0\n");
}

#ifdef CS_USES_GPIO
#define GPIO_DIR_IN  0
#define GPIO_DIR_OUT 1
int setupGpioCS(unsigned char gpio_num)
{
    if (gpio_unexport(gpio_num) < 0)                return -1; // make sure we get full control
    if (gpio_export(gpio_num) < 0)                  return -1;
    if (gpio_set_dir(gpio_num, GPIO_DIR_OUT) < 0)   return -1;
    if (gpio_set_value(gpio_num, CS_INACTIVE) < 0)  return -1;
    return 0;
} // setupGpioCS
#endif


/**
 * Main class of project dmp_900MHz_cmake_linked
 *
 * @param argc the number of arguments
 * @param argv the arguments from the commandline
 * @returns exit code of the application
 */
int main(int argc, char** argv)
{

	printf("Hello World from dmp_900MHz_cmake_linked\n");

    if(argc != 2)
    {
        usage();
        return -1;
    }

    int spiDevNumber = atoi(argv[1]);
    char spiDeviceName[16];

    if ((0 == spiDevNumber) || (1 == spiDevNumber))
    {
        sprintf(spiDeviceName, "/dev/spidev%d.0", spiDevNumber);
    }
    else
    {
        printf("Error in spi dev number:%d", spiDevNumber);
        return -1;
    }

    printf("Using %s\n", spiDeviceName);
    // Set up SPI interface

    // Setup the settings database
    settings_init();

    // Setup the inotify system for doorbell event inputs
    DRBL_INOTIFIYIN_init();

#ifdef CS_USES_GPIO
    // CS -> spi0.0 is gpio 10, spi1.0 is gpio 18
    unsigned char gpioCSNum = ((0 == spiDevNumber) ? 10 : 18);
    printf("CS gpio num=%u, use GPIO CS=%d\n", (unsigned int)gpioCSNum, true);
    //if (0 != setupGpioCS(gpioCSNum))
    //{
    //    return -1;
    //}
    // Start main 900MHz wireless system

    int retVal = main_900MHz_wireless(spiDevNumber, true, gpioCSNum);
#else
    // Start main 900MHz wireless system
    int retVal = main_900MHz_wireless(spiDevNumber, false, 0);
#endif

    // Only gets to this point in error condition
    return retVal;
}

