/*
 * log_to_file.c
 *
 *  Created on: Jun 6, 2017
 *      Author: cargt
 */
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#include <sys/stat.h>

#include "LOG_to_file.h"


void LOG_to_file(char* path, char* string)
{
	size_t length;
	if(NULL == string)
		length = 0;
	else
		length = strlen(string); // NOTE: strlen doesn't check for NULL pointers!!!
	// strlen will seg fault if tested against a NULL pointer

	LOG_to_file_data(path, (uint8_t *)string, length);
}

void LOG_to_file_data(char* path, uint8_t* data, size_t len)
{
#define PATH_SZ 1024
	ssize_t nrd;
	int fd;
	struct stat st = {0};
	char* path_ptr = path+1;
	char subpath[PATH_SZ];
	char * location;
	time_t timer;
	char time_buf[30];
	struct tm* tm_info;

    time(&timer);
    tm_info = localtime(&timer);

	/* check that each directory is present
	 * mkdir if not found
	 */
	location = strchr(path_ptr,'/');
	while( location!=NULL )
	{
		//get subpath for each directory
		memset(subpath,0,PATH_SZ);
		memcpy(subpath,path,location-path);

		if (stat(subpath, &st) == -1) {
			printf ("\nmkdir: %s", subpath);
			mkdir(subpath, 0777);
		}

		location = strchr(location+1,'/');
	}

	fd = open(path, O_CREAT | O_WRONLY | O_APPEND, S_IRUSR | S_IWUSR);

	strftime(time_buf, 30, "<%Y-%m-%d %H:%M:%S> ", tm_info);
	if( write(fd,time_buf,strlen(time_buf)) != strlen(time_buf) )
	{
		printf("write error: %s", path);
	}
	if(len > 0)
	{
		if( write(fd,data,len) != len )
		{
			printf("write error: %s", path);
		}
	}
	close(fd);

}

