/*H*****************************************************************************
FILENAME: chipcon.c

DESCRIPTION: Code file for linux spi device driver

NOTES:

Copyright (c) Cargt Inc. 2017  All rights reserved.

*****************************************************************************H*/
/*----compilation control-----------------------------------------------------*/

/*******************************************************************************
INCLUDE FILES
*******************************************************************************/
/*----system and platform files (external libraries) -------------------------*/

/*----program files-----------------------------------------------------------*/
#include <linux/types.h>
#include <linux/spi/spidev.h>
#include <sys/ioctl.h>
#include <string.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <assert.h>

#include "gpio_linux.h"
#include "spi.h"


/*******************************************************************************
EXTERNAL REFERENCES   Only used if not available in a header file.
*******************************************************************************/
/*----data declarations-------------------------------------------------------*/

/*----function prototype------------------------------------------------------*/

/*******************************************************************************
PRIVATE DECLARATIONS  Defined in this module, used only in this module.
*******************************************************************************/
/*----defines, typedefs, structs, unions, enums-------------------------------*/

/*----data declarations-------------------------------------------------------*/

/*----function prototype------------------------------------------------------*/

/*----macros------------------------------------------------------------------*/

/*******************************************************************************
PUBLIC FUNCTION DEFINITIONS  Note: Header is repeated for each function.
*******************************************************************************/

uint8_t readSpi(uint8_t headerByte, char* buffer, const size_t nbytes, int fd_spi)
{
	const int LEN = 1 + nbytes;
	uint8_t tx[LEN];
	memset(tx, 0x00, LEN);
	tx[0] = headerByte;

	uint8_t rx[LEN];
	memset(rx, 0x00, LEN);

	struct spi_ioc_transfer tr;
	memset(&tr, 0, sizeof tr);

	tr.tx_buf = (unsigned long)tx;
	tr.rx_buf = (unsigned long)rx;
	tr.len = LEN;
	tr.speed_hz = SPI_CLOCK_SPEED_HZ;
	tr.bits_per_word = SPI_BITS_PER_WORD;

	int rc = ioctl(fd_spi, SPI_IOC_MESSAGE(1), &tr);
	if (rc < 0) {
		perror("SPI read failed");
		abort();
	}

	if (nbytes > 0)
        memcpy(buffer, rx+1, nbytes);

	return rx[0];
}

char writeSpi(uint8_t headerByte, const char* buffer, const size_t nbytes, int fd_spi)
{
	const int LEN = 1 + nbytes;
	uint8_t tx[LEN];
	tx[0] = headerByte;
	if (nbytes > 0)
        memcpy(tx + 1, buffer, nbytes);

	uint8_t rx[LEN];
	memset(rx, 0x00, LEN);

	struct spi_ioc_transfer tr ;
	memset(&tr, 0, sizeof tr);

	tr.tx_buf = (unsigned long) tx;
	tr.rx_buf = (unsigned long) rx;
	tr.len = LEN;
	tr.speed_hz = SPI_CLOCK_SPEED_HZ;
	tr.bits_per_word = SPI_BITS_PER_WORD;

	int rc = ioctl(fd_spi, SPI_IOC_MESSAGE(1), &tr);
	if (rc < 0) {
		perror("SPI write failed");
		abort();
	}

	return rx[0];
}

/*******************************************************************************
PRIVATE FUNCTION DEFINITIONS  Note: Header is repeated for each function.
*******************************************************************************/


