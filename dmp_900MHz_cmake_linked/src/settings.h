/*
 * settings.h
 *
 *  Created on: Jul 31, 2017
 *      Author: mark
 */

#ifndef SETTINGS_H_
#define SETTINGS_H_

#include <stdio.h>
#include <sqlite3.h>
#include <stdbool.h>

#if defined (__cplusplus)
extern "C" {
#endif

bool settings_init(void);
bool settings_read_from_db(void);
void settings_inotify_init();

typedef struct
{

	char name[32];
	char firmware_version[16];
	char device_model[16];
	long serial_number_900MHz;

}settings_type;

extern settings_type settings;




#if defined (__cplusplus)
}
#endif

#endif /* SETTINGS_H_ */
