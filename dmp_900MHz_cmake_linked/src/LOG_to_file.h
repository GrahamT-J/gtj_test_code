/*
 * log_to_file.h
 *
 *  Created on: Jun 6, 2017
 *      Author: cargt
 */

#ifndef SRC_DMP_LOG_TO_FILE_H_
#define SRC_DMP_LOG_TO_FILE_H_

#include <stdlib.h>
#include <stdint.h>

void LOG_to_file     (char* path, char* string);
void LOG_to_file_data(char* path, uint8_t* data, size_t len);


#endif /* SRC_DMP_LOG_TO_FILE_H_ */
