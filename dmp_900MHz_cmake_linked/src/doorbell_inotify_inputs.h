/*
 * doorbell_inotify_inputs.h
 *
 *  Created on: Aug 22, 2017
 *      Author: cargt
 */

#ifndef DOORBELL_INOTIFY_INPUTS_H_
#define DOORBELL_INOTIFY_INPUTS_H_

#include <pthread.h>
#include <inttypes.h>


typedef enum {
	DOORBELL_EVENT_NONE,
	DOORBELL_EVENT_PRESS,
	DOORBELL_EVENT_PROX,
} doorbell_event_t;

#ifdef __cplusplus
extern "C" {
#endif

void                DRBL_INOTIFIYIN_init(void);
uint64_t            DRBL_INOTIFIYIN_getProxValue(void);
doorbell_event_t    DRBL_INOTIFYIN_getDoorbellEvent(void);
void                DRBL_INOTIFYIN_resetDoorbellEvent(doorbell_event_t event);

#ifdef __cplusplus
}
#endif

#endif /* DOORBELL_INOTIFY_INPUTS_H_ */
