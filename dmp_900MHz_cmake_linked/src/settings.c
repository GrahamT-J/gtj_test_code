/*
 * settings.c
 *
 *  Created on: Jul 31, 2017
 *      Author: mark
 */

#include <string.h>
#include <stdlib.h>
#include "settings.h"

#include "main.h"


settings_type settings;

static int callback(void *NotUsed, int argc, char **argv, char **azColName){
  int i;
  for(i=0; i<argc; i++){
	printf("db entry: %s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");

	// Only need to
	if(azColName[i] != NULL && argv[i] != NULL) {
		if( strcmp(azColName[i],"name")==0 ) {
			strcpy(settings.name, argv[i]);
		}
		else if( strcmp(azColName[i],"firmware_version")==0 ) {
			strcpy(settings.firmware_version, argv[i]);
		}
		else if( strcmp(azColName[i],"device_model")==0 ) {
			strcpy(settings.device_model, argv[i]);
		}
		else if( strcmp(azColName[i],"serial_number_900MHz")==0 ) {
			settings.serial_number_900MHz = strtol(argv[i], NULL, 10);
		}
		else {
			//printf("WARNING: unknown setting: %s\n", azColName[i]);
		}
	}

  }
  printf("\n");
  return 0;
}

static void set_defaults(void)
{
	strcpy(settings.name, "doorbell");
	strcpy(settings.firmware_version, "0.0.1");
	strcpy(settings.device_model, "V-1000");
	settings.serial_number_900MHz = 1450000;
}

bool settings_read_from_db(void)
{
  sqlite3 *db;
  char *zErrMsg = 0;
  int rc;
  char sql_select[] = "SELECT * FROM settings";

  rc = sqlite3_open_v2(DB_FILENAME, &db, SQLITE_OPEN_READONLY, NULL);
  if( rc ){
	fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
	sqlite3_close(db);
	return(0);
  }
  rc = sqlite3_exec(db, sql_select, callback, 0, &zErrMsg);
  if( rc!=SQLITE_OK ){
	fprintf(stderr, "SQL error: %s\n", zErrMsg);
	sqlite3_free(zErrMsg);
	return 0;
  }
  sqlite3_close(db);

  return 1;
}

bool settings_init(void)
{

  set_defaults();
  if( settings_read_from_db() == 0 )
  {
	  return 0;
  }
  // initialize inotify after init
  return 1;

}

