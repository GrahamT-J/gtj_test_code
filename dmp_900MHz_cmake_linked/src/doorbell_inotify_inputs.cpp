/*
 * doorbell_inotify_inputs.c
 *
 *  Created on: Aug 22, 2017
 *      Author: cargt
 */

#include <stdint.h>
#include <stdio.h>
#include <stddef.h>
#include <sys/inotify.h>
#include <unistd.h>
#include <limits.h>
#include <pthread.h>
#include <string.h>
#include <string>
#include <iostream>
#include <fstream>
#include <inttypes.h>


#include "doorbell_inotify_inputs.h"


/***************************************************************************************/
#define BUF_LEN (10 * (sizeof(struct inotify_event) + NAME_MAX + 1))
#define INOTIFY_PATH "/home/root/dmp/900Mhz"
#define INOTIFY_FILE_0 "doorbell"
#define INOTIFY_FILE_1 "prox"


/*******************************************************************/
struct doorbell_event_struct_t {
	// Active flags:
	//   set when received from input source;
	//   reset when successfully sent and acknowledged by panel
	volatile bool press_active;
	volatile bool prox_active;
	pthread_mutex_t prox_value_mutex;
	volatile uint64_t prox_value;
	// add timestamps for logging purposes
} ;

struct doorbell_event_struct_t doorbell_events;

/***************************************************************************************/
static uint64_t convertProxStringToUint64(std::string& proxString);
static void notify_thread(void);
static void displayInotifyEvent(struct inotify_event *i);
static void setProxValue(uint64_t proxValue);

/***************************************************************************************/
void DRBL_INOTIFIYIN_init()
{
    //start thread
	pthread_t tid = 0;

	// initialize doorbell_events structure
	pthread_mutex_init(&doorbell_events.prox_value_mutex, nullptr);
	doorbell_events.press_active = false;
	doorbell_events.prox_active = false;

	int ret = pthread_create(&tid,NULL,(void *(*)(void *))notify_thread,NULL);
	if (ret != 0)
	{
		printf("pthread_create failed, ret = %d\r\n", ret);
	}

	pthread_detach(tid);
}

uint64_t DRBL_INOTIFIYIN_getProxValue(void)
{
	pthread_mutex_lock(&doorbell_events.prox_value_mutex);
	uint64_t proxValue = doorbell_events.prox_value;
	pthread_mutex_unlock(&doorbell_events.prox_value_mutex);

	return proxValue;
}

doorbell_event_t DRBL_INOTIFYIN_getDoorbellEvent(void)
{
	// prox is considered a higher priority if both events are active
	if (doorbell_events.prox_active)
		return DOORBELL_EVENT_PROX;
	else if (doorbell_events.press_active)
		return DOORBELL_EVENT_PRESS;
	else
		return DOORBELL_EVENT_NONE;
}

void DRBL_INOTIFYIN_resetDoorbellEvent(doorbell_event_t event)
{
	if (DOORBELL_EVENT_PROX == event)
	{
		doorbell_events.prox_active = false;
	}
	else if (DOORBELL_EVENT_PRESS == event)
	{
		doorbell_events.press_active = false;
	}
}


/*** Private functions *******************************************************/
static void displayInotifyEvent(struct inotify_event *i)
{
    if (i->len > 0)
    {
        printf("        name = %s\n", i->name);
        if( strcmp(i->name,INOTIFY_FILE_0)==0 )
        {
        	doorbell_events.press_active = true;
        }
        if( strcmp(i->name,INOTIFY_FILE_1)==0 )
        {
        	// Get last line of prox file
        	std::ifstream proxFile ( INOTIFY_PATH  "/" INOTIFY_FILE_1);
        	std::string currentLine, lastLine;
        	while (getline (proxFile,currentLine))
        	{
        	    //cout << currentLine<<endl;
        	    lastLine=currentLine;

        	}
        	proxFile.close();
        	uint64_t proxValue = convertProxStringToUint64(lastLine);
        	setProxValue(proxValue);
        	doorbell_events.prox_active = true;
        	printf("900Mhz rxd prox val=\"0x%" PRIx64 "\"\n", proxValue);
        }
    }
}

static void setProxValue(uint64_t proxValue)
{
	pthread_mutex_lock(&doorbell_events.prox_value_mutex);
	doorbell_events.prox_value = proxValue;
	pthread_mutex_unlock(&doorbell_events.prox_value_mutex);
}

static uint64_t convertProxStringToUint64(std::string& proxString)
{
	// search string for ""
	int firstPos = proxString.find("\"", 0);
	if (firstPos == std::string::npos)
		return 0;

	firstPos++; // move to start of string, avoiding prev search char
	int secondPos = proxString.find("\"", firstPos);
	if (secondPos == std::string::npos)
		return 0;

	int searchLength = secondPos - firstPos;
	proxString = proxString.substr(firstPos, searchLength);

    long long ll = std::stoll(proxString,nullptr,0); // use base ",16" if no 0x at start of number
    return (uint64_t)ll;
}



static void notify_thread(void)
{
	int inotifyFd, wd, j;
    size_t numRead;
    char *p;
    struct inotify_event *event;
    char buf[BUF_LEN] __attribute__ ((aligned(8)));

	printf("Task Start\n");
    inotifyFd = inotify_init();                 /* Create inotify instance */
    if (inotifyFd == -1)
   	 printf("inotify_init");

	 wd = inotify_add_watch(inotifyFd, INOTIFY_PATH, IN_CLOSE_WRITE);


	 while(1)
	 {
		 numRead = read(inotifyFd, buf, BUF_LEN);
		 if (numRead == 0)
			 printf("read() from inotify fd returned 0!");

		 if (numRead == -1)
			 printf("read");

		 printf("Read %ld bytes from inotify fd\n", (long) numRead);

		 /* Process all of the events in buffer returned by read() */

		 for (p = buf; p < buf + numRead; ) {
			 event = (struct inotify_event *) p;
			 displayInotifyEvent(event);

			 p += sizeof(struct inotify_event) + event->len;
		 }
	 }
}


