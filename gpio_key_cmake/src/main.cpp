/** @mainpage gpio_key_cmake - None
 *
 * @author Me <anony@mo.us>
 * @version 1.0.0
**/

#include <stdio.h>
#include <fcntl.h>
#include <linux/input.h>
#include <unistd.h>
#include <signal.h>
#include <stdlib.h>

int main()
{
        //char devname[] = "/dev/input/event1";
        char devname[] = "/dev/input/by-path/platform-gpio_keys-event";
        int device = open(devname, O_RDONLY);
        struct input_event ev;

        while(1)
        {
                read(device,&ev, sizeof(ev));
                if(ev.type == 1 && ev.value == 1){
                        printf("Key: %i State: %i\n",ev.code,ev.value);
                }
        }
}
/*
 * http://www.thelinuxdaily.com/2010/05/grab-raw-keyboard-input-from-event-device-node-devinputevent/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <dirent.h>
#include <linux/input.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/select.h>
#include <sys/time.h>
#include <termios.h>
#include <signal.h>

void handler (int sig)
{
  printf ("nexiting...(%d)n", sig);
  exit (0);
}

void perror_exit (char *error)
{
  perror (error);
  handler (9);
}

int main (int argc, char *argv[])
{
  struct input_event ev[64];
  int fd, rd, value, size = sizeof (struct input_event);
  char name[256] = "Unknown";
  char *device = NULL;

  //Setup check
  if (argv[1] == NULL){
      printf("Please specify (on the command line) the path to the dev event interface devicen");
      exit (0);
    }

  if ((getuid ()) != 0)
    printf ("You are not root! This may not work...n");

  if (argc > 1)
    device = argv[1];

  printf("About to open device:%s\n", device);
  //Open Device
  if ((fd = open (device, O_RDONLY)) == -1)
    printf ("%s is not a vaild device.n", device);

  //printf("Printing device name:\n");
  //Print Device Name
  //ioctl (fd, EVIOCGNAME (sizeof (name)), name);
  //printf ("Reading From : %s (%s)n", device, name);

  printf("Starting read:\n");
  while (1){
      if ((rd = read (fd, ev, size * 64)) < size)
      {
    	  printf("Sending read error\n");
          perror_exit ("read()");
      }

      printf(".");
      value = ev[0].value;

      if (value != ' ' && ev[1].value == 1 && ev[1].type == 1){ // Only read the key press event
       printf ("Code[%d]n", (ev[1].code));
      }
  }

  return 0;
}
*/
