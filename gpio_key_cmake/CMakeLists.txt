cmake_minimum_required (VERSION 2.8.1)

######## Project settings ########
PROJECT(gpio_key_cmake)
SET(LICENSE "TBD")

######## Build and include settings ########
include_directories(
	inc
)

link_directories(
	${LINK_DIRECTORIES}
)


file(GLOB SOURCES
	"src/*.cpp"
)

add_executable(
	gpio_key_cmake

	${SOURCES}
)

TARGET_LINK_LIBRARIES(
	gpio_key_cmake
)

######## Install targets ########
INSTALL(TARGETS gpio_key_cmake
	RUNTIME DESTINATION usr/bin
)
