//------------------------------------------------------------------------------
// comm.c
//------------------------------------------------------------------------------
// Copyright 2016 by Bryce J TeBeest
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Includes
//------------------------------------------------------------------------------
#include "main.h"
#include "stm32f7xx_hal.h"
#include <stdlib.h>

#include "lis3dh_driver.h"
#include "comm.h"
#include "cmsis_os.h"

//------------------------------------------------------------------------------
// Definitions
//------------------------------------------------------------------------------
//Enumerations
enum
    {
    SIGNAL_I2C_TIMED_EVENT              = ( 1 << 0 ),
    };

//OS Definitions
osSemaphoreDef( i2c1_semaphore );
osSemaphoreDef( i2c2_semaphore );
osSemaphoreDef( i2c3_semaphore );
osSemaphoreDef( i2c4_semaphore );

osSemaphoreDef( i2c1_task_semaphore );
osSemaphoreDef( i2c2_task_semaphore );
osSemaphoreDef( i2c3_task_semaphore );
osSemaphoreDef( i2c4_task_semaphore );

//------------------------------------------------------------------------------
// Variables
//------------------------------------------------------------------------------
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wmissing-field-initializers"
i2c_port_type i2c_ports[I2C_COUNT] =
    {
        { /* I2C1 */
        /* RX Callback */   NULL,
        /* TX Callback */   NULL,
        /* Port Base */     I2C1_BASE,
        },
        { /* I2C2 */
        /* RX Callback */   NULL,
        /* TX Callback */   NULL,
        /* Port Base */     I2C2_BASE,
        },
        { /* I2C3 */
        /* RX Callback */   NULL,
        /* TX Callback */   NULL,
        /* Port Base */     I2C3_BASE,
        },
        { /* I2C4 */
        /* RX Callback */   NULL,
        /* TX Callback */   NULL,
        /* Port Base */     I2C4_BASE,
        }
    };

accel_type accels[ACCEL_COUNT] =
    {
        { /* ACCEL1 */
        &i2c_ports[I2C2_IDX],
        0x18,
        },
    };

comm_task_type comm_tasks[I2C_COUNT] =
    {
        { /* I2C1 Task */
        /* Semaphore */     NULL,
        /* Timer */         NULL,
        /* task_signals */  0x00,
        /* msec_delay */    10,
        /* Accel1 */        NULL,
        /* Accel2 */        NULL,
        },
        { /* I2C2 Task */
        /* Semaphore */     NULL,
        /* Timer ID */      NULL,
        /* task_signals */  0x00,
        /* msec_delay */    10,
        /* Accel1 */        &accels[0],
        /* Accel2 */        NULL,
        },
        { /* I2C3 Task */
        /* Semaphore */     NULL,
        /* Timer */         NULL,
        /* task_signals */  0x00,
        /* msec_delay */    10,
        /* Accel1 */        NULL,
        /* Accel2 */        NULL,
        },
        { /* I2C4 Task */
        /* Semaphore */     NULL,
        /* Timer */         NULL,
        /* task_signals */  0x00,
        /* msec_delay */    10,
        /* Accel1 */        NULL,
        /* Accel2 */        NULL,
        },
    };
#pragma GCC diagnostic pop

//------------------------------------------------------------------------------
// Prototypes
//------------------------------------------------------------------------------
static uint8_t i2c_handle_to_index( I2C_HandleTypeDef * hi2c );
static void i2c1_timer_handler( void const * argument );
static void i2c2_timer_handler( void const * argument );
static void i2c3_timer_handler( void const * argument );
static void i2c4_timer_handler( void const * argument );

//OS Definitions
osTimerDef( i2c1_task_timer, i2c1_timer_handler );
osTimerDef( i2c2_task_timer, i2c2_timer_handler );
osTimerDef( i2c3_task_timer, i2c3_timer_handler );
osTimerDef( i2c4_task_timer, i2c4_timer_handler );

//------------------------------------------------------------------------------
// Code
//------------------------------------------------------------------------------
/*
*   PROCEDURE NAME:
*       Communication_Task
*
*   DESCRIPTION:
*       This is called with unique arguments to instantiated communication
*       threads for hardware peripherals.
*
*/
void Communication_Task( void const * argument )
{
uint32_t i;
uint32_t instance = (uint32_t)argument;
uint32_t signals;

if( instance >= I2C_COUNT )
    {
    Error_Handler();
    }

switch( instance )
    {
    case I2C1_IDX:
        comm_tasks[instance].semaphore = osSemaphoreCreate( osSemaphore( i2c1_task_semaphore ), 1 );
        i2c_ports[instance].semaphore = osSemaphoreCreate( osSemaphore( i2c1_semaphore ), 1 );
        comm_tasks[instance].timer = osTimerCreate( osTimer( i2c1_task_timer ), osTimerPeriodic, NULL );
        break;

    case I2C2_IDX:
        comm_tasks[instance].semaphore = osSemaphoreCreate( osSemaphore( i2c2_task_semaphore ), 1 );
        i2c_ports[instance].semaphore = osSemaphoreCreate( osSemaphore( i2c2_semaphore ), 1 );
        comm_tasks[instance].timer = osTimerCreate( osTimer( i2c2_task_timer ), osTimerPeriodic, NULL );
        break;

    case I2C3_IDX:
        comm_tasks[instance].semaphore = osSemaphoreCreate( osSemaphore( i2c3_task_semaphore ), 1 );
        i2c_ports[instance].semaphore = osSemaphoreCreate( osSemaphore( i2c3_semaphore ), 1 );
        comm_tasks[instance].timer = osTimerCreate( osTimer( i2c3_task_timer ), osTimerPeriodic, NULL );
        break;

    case I2C4_IDX:
        comm_tasks[instance].semaphore = osSemaphoreCreate( osSemaphore( i2c4_task_semaphore ), 1 );
        i2c_ports[instance].semaphore = osSemaphoreCreate( osSemaphore( i2c4_semaphore ), 1 );
        comm_tasks[instance].timer = osTimerCreate( osTimer( i2c4_task_timer ), osTimerPeriodic, NULL );
        break;

    default:
        Error_Handler();
        break;
    }

MX_I2Cx_Init( &i2c_ports[instance], (void *)i2c_ports[instance].port_base );

//Assign out the port-specific initialization to each connected IC
for( i = 0; i < MAX_ACCEL_PER_TASK; i++ )
    {
    if( comm_tasks[instance].accels[i] )
        {
        //TODO - Need to check these returns
        LIS3DH_SetODR( comm_tasks[instance].accels[i], LIS3DH_ODR_400Hz );
        LIS3DH_SetMode( comm_tasks[instance].accels[i], LIS3DH_NORMAL );
        LIS3DH_SetFullScale( comm_tasks[instance].accels[i], LIS3DH_FULLSCALE_2 );
        LIS3DH_FIFOModeEnable( comm_tasks[instance].accels[i], LIS3DH_FIFO_STREAM_MODE );
        LIS3DH_SetAxis( comm_tasks[instance].accels[i], LIS3DH_X_ENABLE | LIS3DH_Y_ENABLE | LIS3DH_Z_ENABLE );
        }
    }

//Start grab the semaphore for the first time and then wait for the timer to release it
osSemaphoreWait( comm_tasks[instance].semaphore, osWaitForever );
osTimerStart( comm_tasks[instance].timer, comm_tasks[instance].msec_delay );

while( 1 )
    {
    osSemaphoreWait( comm_tasks[instance].semaphore, osWaitForever );
    taskENTER_CRITICAL();
    signals = comm_tasks[instance].task_signals;
    comm_tasks[instance].task_signals = 0;
    taskEXIT_CRITICAL();

    if( signals & SIGNAL_I2C_TIMED_EVENT )
        {
        if( comm_tasks[instance].accels[0] )
            {
            LIS3DH_GetAccAxesRaw( comm_tasks[instance].accels[0], &comm_tasks[instance].accels[0]->data );
            if( abs(comm_tasks[instance].accels[0]->data.AXIS_Z) > abs(comm_tasks[instance].accels[0]->data.AXIS_X) &&
                abs(comm_tasks[instance].accels[0]->data.AXIS_Z) > abs(comm_tasks[instance].accels[0]->data.AXIS_Y) )
                {
                HAL_GPIO_WritePin( LD1_GPIO_Port, LD1_Pin, GPIO_PIN_SET );
                }
            else
                {
                HAL_GPIO_WritePin( LD1_GPIO_Port, LD1_Pin, GPIO_PIN_RESET );
                }
            }
        }
    }
} //StartDefaultTask()

/*
*   PROCEDURE NAME:
*       i2c_handle_to_index
*
*   DESCRIPTION:
*       Convert ST's peripheral base address to logical index
*       for use in finding current thread's index.
*/
static uint8_t i2c_handle_to_index( I2C_HandleTypeDef * hi2c )
{
uint32_t base = (uint32_t)hi2c->Instance;

//This can be accomplished more efficiently by storing off the IDX
//value into the I2C_HandleTypeDef at initialization but was
//decided against to avoid modifying the standard library.
switch( base )
    {
    case I2C1_BASE:
        return I2C1_IDX;

    case I2C2_BASE:
        return I2C2_IDX;

    case I2C3_BASE:
        return I2C3_IDX;

    case I2C4_BASE:
        return I2C4_IDX;

    default:
        Error_Handler();
        return I2C_COUNT;
    }
} //i2c_handle_to_index()

/*
*   PROCEDURE NAME:
*       i2c1_timer_handler
*
*   DESCRIPTION:
*       Release the semaphore to cause another time-based
*       iteration through the communication code.
*/
static void i2c1_timer_handler( void const * argument )
{
taskENTER_CRITICAL();
comm_tasks[I2C1_IDX].task_signals |= SIGNAL_I2C_TIMED_EVENT;
taskEXIT_CRITICAL();

osSemaphoreRelease( comm_tasks[I2C1_IDX].semaphore );
} //i2c1_timer_handler()

/*
*   PROCEDURE NAME:
*       i2c2_timer_handler
*
*   DESCRIPTION:
*       Release the semaphore to cause another time-based
*       iteration through the communication code.
*/
static void i2c2_timer_handler( void const * argument )
{
taskENTER_CRITICAL();
comm_tasks[I2C2_IDX].task_signals |= SIGNAL_I2C_TIMED_EVENT;
taskEXIT_CRITICAL();

osSemaphoreRelease( comm_tasks[I2C2_IDX].semaphore );
} //i2c2_timer_handler()

/*
*   PROCEDURE NAME:
*       i2c3_timer_handler
*
*   DESCRIPTION:
*       Release the semaphore to cause another time-based
*       iteration through the communication code.
*/
static void i2c3_timer_handler( void const * argument )
{
taskENTER_CRITICAL();
comm_tasks[I2C3_IDX].task_signals |= SIGNAL_I2C_TIMED_EVENT;
taskEXIT_CRITICAL();

osSemaphoreRelease( comm_tasks[I2C3_IDX].semaphore );
} //i2c3_timer_handler()

/*
*   PROCEDURE NAME:
*       i2c4_timer_handler
*
*   DESCRIPTION:
*       Release the semaphore to cause another time-based
*       iteration through the communication code.
*/
static void i2c4_timer_handler( void const * argument )
{
taskENTER_CRITICAL();
comm_tasks[I2C4_IDX].task_signals |= SIGNAL_I2C_TIMED_EVENT;
taskEXIT_CRITICAL();

osSemaphoreRelease( comm_tasks[I2C4_IDX].semaphore );
} //i2c4_timer_handler()

//------------------------------------------------------------------------------
// ISRs
//------------------------------------------------------------------------------
/*
*   PROCEDURE NAME:
*       HAL_I2C_MasterTxCpltCallback
*
*   DESCRIPTION:
*       Generated at the end of any I2C transfer at about the
*       same time as the stop bit.  If a read is desired then
*       that is kicked off here as ST's default libraries
*       leave a combined address latch + read lacking.
*/
void HAL_I2C_MasterTxCpltCallback( I2C_HandleTypeDef * hi2c )
{
uint8_t read_address;
uint8_t * read_buffer;
uint8_t read_count;
uint8_t index;

index = i2c_handle_to_index( hi2c );
read_count = i2c_ports[index].read_count;

if( read_count )
    {
    read_address = i2c_ports[index].read_address;
    read_buffer = i2c_ports[index].read_buffer;

    //Reset read_count to prevent TX callbacks from causing additional reads
    i2c_ports[index].read_count = 0;
    while( HAL_I2C_Master_Receive_DMA( hi2c, (uint16_t)(read_address << 1 | 1), (uint8_t *)read_buffer, read_count ) != HAL_OK )
        {
        /* Transfer error in reception process */
        Error_Handler();
        }
    }
else
    {
    if( i2c_ports[index].tx_callback )
        {
        i2c_ports[index].tx_callback( index, i2c_ports[index].write_count );
        }

    //Only release during a true TX to allow a following read the ability hold the transfer open
    osSemaphoreRelease( i2c_ports[index].semaphore );
    }
} //HAL_I2C_MasterTxCpltCallback()

/*
*   PROCEDURE NAME:
*       HAL_I2C_MasterRxCpltCallback
*
*   DESCRIPTION:
*       Call any optional callback at the end of an I2C read.
*/
void HAL_I2C_MasterRxCpltCallback( I2C_HandleTypeDef * hi2c )
{
uint8_t index = i2c_handle_to_index( hi2c );

if( i2c_ports[index].rx_callback )
    {
    i2c_ports[index].rx_callback( index, i2c_ports[index].read_buffer, i2c_ports[index].read_count );
    }

osSemaphoreRelease( i2c_ports[index].semaphore );
} //HAL_I2C_MasterRxCpltCallback()

//------------------------------------------------------------------------------
// EOF
//------------------------------------------------------------------------------
