//------------------------------------------------------------------------------
// comm.h
//------------------------------------------------------------------------------
// Copyright 2016 by Bryce J TeBeest
//------------------------------------------------------------------------------

#ifndef __COMM_H
#define __COMM_H

#include "main.h"
#include "stm32f7xx_hal.h"

#include "cmsis_os.h"

//------------------------------------------------------------------------------
// Definitions
//------------------------------------------------------------------------------
#define ACCEL1_IDX              0
#define ACCEL_COUNT             1
#define MAX_ACCEL_PER_TASK      2

#define I2C1_IDX                0
#define I2C2_IDX                1
#define I2C3_IDX                2
#define I2C4_IDX                3
#define I2C_COUNT               4

//------------------------------------------------------------------------------
// Types
//------------------------------------------------------------------------------
typedef void (*i2c_rx_callback_type)( uint8_t port_index, uint8_t * rx_buf, uint32_t len );
typedef void (*i2c_tx_callback_type)( uint8_t port_index, uint32_t len );

typedef struct
    {
    //Callbacks
    i2c_rx_callback_type    rx_callback;
    i2c_tx_callback_type    tx_callback;

    //Port management
    uint32_t                port_base;
    DMA_HandleTypeDef       hdma_i2c_rx;
    DMA_HandleTypeDef       hdma_i2c_tx;
    I2C_HandleTypeDef       handle;
    osSemaphoreId           semaphore;

    //Transmission variables
    uint8_t                 read_address;
    volatile uint8_t        read_count;                 //Cleared in ISR
    uint8_t                 read_reg;
    uint8_t *               read_buffer;
    uint8_t                 write_buffer[2];
    uint8_t                 write_count;
    } i2c_port_type;

typedef struct
    {
    int16_t AXIS_X;
    int16_t AXIS_Y;
    int16_t AXIS_Z;
    } axes_data_type;

typedef struct
    {
    //System configuration
    i2c_port_type *     port;               //I2C Port
    uint8_t             address;            //I2C Address

    //Resulting data
    axes_data_type      data;
    } accel_type;

typedef struct
    {
    osSemaphoreId       semaphore;
    osTimerId           timer;
    volatile uint32_t   task_signals;
    uint32_t            msec_delay;
    accel_type *        accels[MAX_ACCEL_PER_TASK];
    } comm_task_type;

//------------------------------------------------------------------------------
// Prototypes
//------------------------------------------------------------------------------
void MX_I2Cx_Init( i2c_port_type * handle, void * base_address );

#endif /* __COMM_H */

//------------------------------------------------------------------------------
// EOF
//------------------------------------------------------------------------------
