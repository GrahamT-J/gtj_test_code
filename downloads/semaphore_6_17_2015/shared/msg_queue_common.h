#ifndef MSG_QUEUE_COMMON_H_
#define MSG_QUEUE_COMMON_H_

#define CHECK(x) \
    do { \
        if (!(x)) { \
            fprintf(stderr, "%s:%d: ", __func__, __LINE__); \
            perror(#x); \
            exit(-1); \
        } \
    } while (0) \

/*
 * The basic data types
 */
typedef unsigned char		uint8;  /*  8 bits */
typedef unsigned short int	uint16; /* 16 bits */
typedef unsigned long int	uint32; /* 32 bits */

/*
 *  Message Queue client IDs
 */
typedef uint8 msg_que_id_type; enum
    {
    MSG_QUEUE_CLIENT_UI             = 0x00,
    MSG_QUEUE_CLIENT_RGN_UPDATES    = 0x01,

    MSG_QUEUE_CLIENT_CNT,
    MSG_QUEUE_CLIENT_INVALID
    };

/*
 *  Message Queue Commands
 */
typedef uint16 msg_que_cmnd_type; enum
    {
    MSG_QUEUE_CMND_INIT     = 0,
    MSG_QUEUE_CMND_TX_DATA  = 1,
    MSG_QUEUE_CMND_RX_DATA  = 2,
    MSG_QUEUE_CMND_DEINIT   = 4,

    MSG_QUEUE_CMND_CNT,
    };

#define MSG_QUE_PAYLOAD_SIZE    ( 1024 )

typedef struct _msg_que_packet_type
    {
    msg_que_id_type         id;
    msg_que_cmnd_type       cmd;
    uint8                   payload[MSG_QUE_PAYLOAD_SIZE];
    } msg_que_packet_type;

//#define QUEUE_NAME              "/test_queue"
#define UART_TX_QUEUE_NAME      "/tx_queue"
#define UART_RX_QUEUE_NAME_UI   "/rx_queue_client_ui"
#define UART_RX_QUEUE_NAME_RGN  "/rx_queue_client_rgn"
#define MAX_SIZE    ( sizeof( msg_que_packet_type ) )
#define MSG_STOP    "exit"


#endif /* #ifndef MSG_QUEUE_COMMON_H_ */
