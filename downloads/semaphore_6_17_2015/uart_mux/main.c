#include <stdio.h>

#include <semaphore.h>
#include <sys/mman.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <errno.h>      	// Error number definitions
#include <termios.h>    	// POSIX terminal control definitions
#include <pthread.h>
#include <sys/resource.h>       // setpriority()

#include <mqueue.h>

#include "msg_queue_common.h"

static int         m_port = 0;                // port handle

static volatile int m_kill_worker;
static volatile int m_worker_alive;

static int         m_buf_idx;
static char        m_buf[1024];           // rx buffer

// This function just calls Puppy::worker_proc
// arg = pointer to Puppy class
static pthread_attr_t   thread_attrs;
static pthread_t        m_worker_thread;
static unsigned char    stack[16 * 1024];		// pthread needs MIN 16k



static int uart_open(const char *device_name) {
    m_port = open( device_name, O_RDWR| O_NONBLOCK | O_NDELAY );

    printf("open(): m_port = %08X\n", m_port);
    if ( m_port <= 0 ) {
        printf("    device_name = '%s'\n", device_name ? device_name : "NULL");
        printf("    Error opening device: %s\n", strerror(errno));
        m_port = 0;
        return 1;
    } // end if

    // configure
    struct termios tty ;
    memset(&tty, 0, sizeof(tty));

    if ( tcgetattr ( m_port, &tty ) != 0 ) {
        printf("    Error from tcgetattr: %s\n", strerror(errno));
        m_port = 0;
        return 2;
    } // end if

    // set all control codes to 0
    memset(&tty.c_cc, 0, sizeof(tty.c_cc));

    // set baud
    cfsetospeed (&tty, B115200);
    cfsetispeed (&tty, B115200);

    // set tty options
    tty.c_cflag     &= ~PARENB;   // 8n1
    tty.c_cflag     &= ~CSTOPB;
    tty.c_cflag     &= ~CSIZE;
    tty.c_cflag     |=  CS8;
    tty.c_cflag     &= ~CRTSCTS;  // no flow control

    //tty.c_cflag     |= CRTSCTS;  // no flow control


    tty.c_lflag      =  0;        // no signaling chars, no echo, no canonical processing
    tty.c_oflag      =  0;        // no remapping, no delays
    tty.c_cc[VMIN]   =  1;        // block until this many characters received
    //tty.c_cc[VTIME]  =  50;        // inter-character timer
    tty.c_cc[VTIME]  =  1;        // inter-character timer

    tty.c_cflag     |=  CREAD | CLOCAL;                  // turn on READ & ignore ctrl lines
    tty.c_iflag     &=  ~(IXON | IXOFF | IXANY);         // turn off s/w flow ctrl
    tty.c_lflag     &=  ~(ICANON | ECHO | ECHOE | ISIG); // make raw
    tty.c_oflag     &=  ~OPOST;                          // make raw

    // TB
    // enable parity, but ignore errors
//      tty.c_iflag     |=  IGNPAR;             // ignore parity errors
//      tty.c_cflag     |=  PARODD | PARENB;   // odd parity, parity enabled


    cfmakeraw(&tty);

    // flush and set options
    tcflush( m_port, TCIFLUSH );

    if ( tcsetattr ( m_port, TCSANOW, &tty ) != 0) {
        printf("    Error from tcsetattr: %s\n", strerror(errno));
        return 3;
    } // end if


    return 0;
} // end method

static void display_pthread_attr(pthread_attr_t *attr, char *prefix)
{
    int s, i;
    size_t v;
    void *stkaddr;
    struct sched_param sp;

    return;	// comment for debug info about thread attributes

    s = pthread_attr_getdetachstate(attr, &i);
    if (s != 0)
        handle_error_en(s, "pthread_attr_getdetachstate");
    printf("%sDetach state        = %s\n", prefix,
            (i == PTHREAD_CREATE_DETACHED) ? "PTHREAD_CREATE_DETACHED" :
            (i == PTHREAD_CREATE_JOINABLE) ? "PTHREAD_CREATE_JOINABLE" :
            "???");

    s = pthread_attr_getscope(attr, &i);
    if (s != 0)
        handle_error_en(s, "pthread_attr_getscope");
    printf("%sScope               = %s\n", prefix,
            (i == PTHREAD_SCOPE_SYSTEM)  ? "PTHREAD_SCOPE_SYSTEM" :
            (i == PTHREAD_SCOPE_PROCESS) ? "PTHREAD_SCOPE_PROCESS" :
            "???");

    s = pthread_attr_getinheritsched(attr, &i);
    if (s != 0)
        handle_error_en(s, "pthread_attr_getinheritsched");
    printf("%sInherit scheduler   = %s\n", prefix,
            (i == PTHREAD_INHERIT_SCHED)  ? "PTHREAD_INHERIT_SCHED" :
            (i == PTHREAD_EXPLICIT_SCHED) ? "PTHREAD_EXPLICIT_SCHED" :
            "???");

    s = pthread_attr_getschedpolicy(attr, &i);
    if (s != 0)
        handle_error_en(s, "pthread_attr_getschedpolicy");
    printf("%sScheduling policy   = %s\n", prefix,
            (i == SCHED_OTHER) ? "SCHED_OTHER" :
            (i == SCHED_FIFO)  ? "SCHED_FIFO" :
            (i == SCHED_RR)    ? "SCHED_RR" :
            "???");

    s = pthread_attr_getschedparam(attr, &sp);
    if (s != 0)
        handle_error_en(s, "pthread_attr_getschedparam");
    printf("%sScheduling priority = %d\n", prefix, sp.sched_priority);

    s = pthread_attr_getguardsize(attr, &v);
    if (s != 0)
        handle_error_en(s, "pthread_attr_getguardsize");
    printf("%sGuard size          = %d bytes\n", prefix, v);

    s = pthread_attr_getstack(attr, &stkaddr, &v);
    if (s != 0)
        handle_error_en(s, "pthread_attr_getstack");
    printf("%sStack address       = %p\n", prefix, stkaddr);
    printf("%sStack size          = 0x%x bytes\n", prefix, v);
}

mqd_t mq_client_ui_rx;
char buffer[MAX_SIZE];



int poll(void) {
    msg_que_packet_type *   msg_que_ptr;

    if (!m_port) {
        usleep(1);	// do avoid hogging the processor
        return 1;
    } // end if

    fd_set rfds;
    FD_ZERO(&rfds);
    FD_SET(m_port, &rfds);

    struct timeval timeout;
    timeout.tv_sec  = 0;
    timeout.tv_usec = 2500; // 2.5ms
    if (select(m_port+1, &rfds, NULL, NULL, &timeout) == 1) {
        int n = read( m_port, &m_buf[m_buf_idx], sizeof(m_buf) - m_buf_idx);

        if (n < 0) {
            printf("\n\nPuppy::poll() Error reading: %d, %s\n", errno, strerror(errno));
            return 4;
        } else {
            printf("Read %d bytes: %s\n", n, &m_buf[m_buf_idx]);

            //m_buf_idx += n;

            //if (m_buf_idx == sizeof(m_buf))
            {
                // make it obvious there was an overflow!
                //for (int i=0; i<10000; i++) printf("BUFFER_OVERFLOW\n");
//                ReceivePuppyClientData(this, m_buf, n);

                //PRC - TODO: Add RX callback here
                msg_que_ptr = buffer;
                msg_que_ptr->id     = MSG_QUEUE_CLIENT_UI;
                msg_que_ptr->cmd    = MSG_QUEUE_CMND_RX_DATA;
                memcpy( msg_que_ptr->payload, &m_buf[m_buf_idx], n );
                /* send the message */
                CHECK( 0 <= mq_send(mq_client_ui_rx, buffer, n + sizeof(msg_que_id_type) + sizeof(msg_que_cmnd_type), 0) );


                m_buf_idx = 0;
            } // end if
        } // end if
    } else {
        // Wait until we DO timeout before processing
        //ReceivePuppyClientData(this, m_buf, m_buf_idx);
        m_buf_idx = 0;
    } // end if

    return 0;
} // end method

void * worker_proc(void)
{
    m_worker_alive = 1;

    //printf("In worker thread\n");

    do {
        poll();
    } while (!m_kill_worker);

    //printf("Leaving worker thread\n");
    //if (m_kill_worker) printf(".. m_kill_worker = true\n");
    //if (!m_worker_alive) printf(".. m_worker_alive = false\n");

    fflush(stdout);

    m_worker_alive = 0;

    return NULL;
} // end method

void * thread_func(void* arg) {
//    Puppy *pclient = (Puppy*)arg;

    setpriority(PRIO_PROCESS, 0, -9);

//    if (!pclient) {
//        printf("thread_func() called with NULL arg\n");
//        return NULL;
//    } // end if

    //return pclient->worker_proc();
    return worker_proc();
} // end method

#define TEST_TX_BUF_SIZE    ( 10 )
static char tx_buf[TEST_TX_BUF_SIZE];

static void handle_msg_command
    ( void )
{

}

int main(void)
{
    mqd_t mq;
    struct mq_attr attr;
    int must_stop = 0;
    char buffer[MAX_SIZE + 1];

    int n_written = 0;

    memset( tx_buf, 0, sizeof( tx_buf ) );

    printf("UART Mux\n");
//    int fd = shm_open("/shmname", O_CREAT | O_RDWR, S_IRUSR | S_IWUSR);

//    if( fd == -1 )
//    {
//        printf( "shm_open() failed: %d\n", errno );
//        return -1;
//    }

//    ftruncate(fd, sizeof(sem_t));
//    sem_t *sem = mmap(NULL, sizeof(sem_t), PROT_READ | PROT_WRITE,
//        MAP_SHARED, fd, 0);

//    if( sem != NULL )
//    {
////        sem_init(sem, 1, 1);
//           sem_init(sem, 1, 1);
//    }
//    else
//    {
//        printf( "sem is null\n" );
//        return -1;
//    }
    #define SNAME "/mysem"
//    sem_t *sem = sem_open( SNAME, O_CREAT, 0644, 0 );
    sem_t *sem = sem_open( SNAME, O_CREAT, 0644, 1 );

    uart_open( "/dev/ttymxc3" );

    tx_buf[0] = 'H';
    tx_buf[1] = 'e';
    tx_buf[2] = 'l';
    tx_buf[3] = 'l';
    tx_buf[4] = 'o';
    tx_buf[5] = '\n';
    n_written = write(m_port, tx_buf, TEST_TX_BUF_SIZE);

    pthread_attr_init(&thread_attrs);
    pthread_attr_setstack(&thread_attrs, stack, sizeof(stack));

    display_pthread_attr(&thread_attrs, "puppy ");


//    if (pthread_create(&m_worker_thread, &thread_attrs, &thread_func, this))
    if( pthread_create(&m_worker_thread, &thread_attrs, &thread_func, NULL) )
    {
        printf("Thread create failed\n");
    }


    /* initialize the queue attributes */
    attr.mq_flags = 0;
    attr.mq_maxmsg = 10;
    attr.mq_msgsize = MAX_SIZE;
    attr.mq_curmsgs = 0;

    /* create the message queue */
    mq = mq_open(UART_TX_QUEUE_NAME, O_CREAT | O_RDONLY, 0644, &attr);
    CHECK((mqd_t)-1 != mq);

    printf("Starting Client UI\n");

//    while( 1 )
//    {
//        sleep(1);
//    }

    do {
        ssize_t                 bytes_read;
        msg_que_packet_type *   msg_que_ptr;


        /* receive the message */
        bytes_read = mq_receive(mq, buffer, MAX_SIZE, NULL);
        CHECK(bytes_read >= 0);

//        buffer[bytes_read] = '\0';
//        if (! strncmp(buffer, MSG_STOP, strlen(MSG_STOP)))
//        {
//            must_stop = 1;
//        }
//        else
//        {
//            printf("Received: %s\n", buffer);
//        }

        msg_que_ptr = buffer;
        switch( msg_que_ptr->id )
        {
            case MSG_QUEUE_CLIENT_RGN_UPDATES:
                break;

            case MSG_QUEUE_CLIENT_UI:
            default:
                break;
        }

        switch( msg_que_ptr->cmd )
        {
            case MSG_QUEUE_CMND_INIT:
                /* open the message queue */
                mq_client_ui_rx = mq_open(UART_RX_QUEUE_NAME_UI, O_WRONLY);
                CHECK((mqd_t)-1 != mq_client_ui_rx);
                break;

            case MSG_QUEUE_CMND_TX_DATA:


                break;

//            case MSG_QUEUE_CMND_RX_DATA:
//                break;

            case MSG_QUEUE_CMND_DEINIT:
                /* cleanup */
                CHECK( (mqd_t)-1 != mq_close( mq_client_ui_rx ) );
                CHECK( (mqd_t)-1 != mq_unlink(UART_RX_QUEUE_NAME_UI) );
                break;

            default:
                break;
        }

    } while (!must_stop);

    /* cleanup */
    CHECK((mqd_t)-1 != mq_close(mq));
    CHECK((mqd_t)-1 != mq_unlink(UART_TX_QUEUE_NAME));

    if (m_port)
    {
        close(m_port);
        m_port = NULL;
    }

    sem_destroy( sem );

    return 0;
}

