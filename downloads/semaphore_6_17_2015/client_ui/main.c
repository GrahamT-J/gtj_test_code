#include <stdio.h>

#include <semaphore.h>
#include <sys/mman.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <errno.h>      	// Error number definitions
#include <termios.h>    	// POSIX terminal control definitions
#include <pthread.h>
#include <sys/resource.h>       // setpriority()

#include <mqueue.h>

#include "msg_queue_common.h"

int main(void)
{
    mqd_t   mq;
    mqd_t   mq_client_ui_rx;


    char buffer[MAX_SIZE];

    printf("Starting Client UI\n");

    /* create the message queue */
    mq = mq_open(UART_RX_QUEUE_NAME_UI, O_CREAT | O_RDONLY, 0644, &attr);
    CHECK((mqd_t)-1 != mq);

    /* open the mail queue */
    mq = mq_open(QUEUE_NAME, O_WRONLY);
    CHECK((mqd_t)-1 != mq);


    #define SNAME "/mysem"
    sem_t *sem = sem_open( SNAME, 0 );

    printf("Send to server (enter \"exit\" to stop it):\n");

    sem_wait( sem );

    do {
        printf("> ");
        fflush(stdout);

        memset(buffer, 0, MAX_SIZE);
        fgets(buffer, MAX_SIZE, stdin);

        /* send the message */
        CHECK(0 <= mq_send(mq, buffer, MAX_SIZE, 0));

    } while (strncmp(buffer, MSG_STOP, strlen(MSG_STOP)));

    sem_post( sem );

    /* cleanup */
    CHECK((mqd_t)-1 != mq_close(mq));

    return 0;
}

