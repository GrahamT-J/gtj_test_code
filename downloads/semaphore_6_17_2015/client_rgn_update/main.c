#include <stdio.h>

#include <semaphore.h>
#include <sys/mman.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <errno.h>      	// Error number definitions
#include <termios.h>    	// POSIX terminal control definitions
#include <pthread.h>
#include <sys/resource.h>       // setpriority()

#include <mqueue.h>

#include "msg_queue_common.h"

#define DEFAULT_TIMEOUT ( 5 )

int main(void)
{
    struct timespec ts;

    printf("Client RGN Update\n");

    #define SNAME "/mysem"
    sem_t *sem = sem_open( SNAME, 0 );

    if( clock_gettime( CLOCK_REALTIME, &ts ) == -1)
    {
         //handle_error("clock_gettime");
        printf( "Error: clock_gettime() failed\n");
        return -1;
    }

    ts.tv_sec += DEFAULT_TIMEOUT;

//    if( sem_trywait( sem ) == 0 )
    if( sem_timedwait( sem, &ts ) == 0 )
    {
        printf( "Success\n" );

        //Do something

        sem_post( sem );
    }
    else
    {
        //printf( "Semaphore not available: %d\n", errno );
        printf( "Timed out waiting for semaphore: %d\n", errno );
    }


    return 0;
}

