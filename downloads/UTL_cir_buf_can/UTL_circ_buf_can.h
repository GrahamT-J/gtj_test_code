/*
 * UTL_circ_buf_can.h
 *
 *  Created by Peter Carlson on March 22, 2017
 *  Copyright (c) 2017 Cargt, Inc. All rights reserved.
 *
 */

#ifndef UTL_CIRC_BUF_CAN_H_
#define UTL_CIRC_BUF_CAN_H_

#include <stdint.h>
#include <stdbool.h>

#include "fsl_flexcan.h"


typedef struct
    {
//	uint32_t            rcrd_size;
	uint32_t			data_size;	//bytes
    uint32_t            read_idx;	//bytes
    uint32_t            write_idx;	//bytes
//    flexcan_frame_t *   mem;
    uint8_t *				mem;
    bool               	full;
    } UTL_circ_buf_can_t;

uint32_t UTL_circ_buf_can_free_space
    (
    UTL_circ_buf_can_t *	cb
    );

void UTL_circ_buf_can_init
    (
    UTL_circ_buf_can_t * 	cb,
	flexcan_frame_t *		buf,
	uint32_t				num_rcrds
    );

uint32_t UTL_circ_buf_can_read
    (
    UTL_circ_buf_can_t * 	cb,
	flexcan_frame_t * 		buf,
	uint32_t 				num_rcrds
    );

void UTL_circ_buf_can_reset
    (
    UTL_circ_buf_can_t *     cb
    );

uint32_t UTL_circ_buf_can_write
    (
    UTL_circ_buf_can_t *	cb,
	flexcan_frame_t * 		buf,
	uint32_t 				num_rcrds
    );

uint32_t UTL_circ_buf_can_used_space
    (
    UTL_circ_buf_can_t *	cb
    );

#endif /* UTL_CIRC_BUF_CAN_H_ */
