/*
 * utl_circ_buf_can.c
 *
 *  Created by Peter Carlson on March 22, 2017
 *  Copyright (c) 2017 Cargt, Inc. All rights reserved.
 */

#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include "UTL_circ_buf_can.h"

#include "FreeRTOS.h"
#include "task.h"

#include "fsl_flexcan.h"
#include "fsl_debug_console.h"

#define		minval(x,y)		( (x) < (y) ? (x) : (y) )


/*
*   PROCEDURE NAME:
*       UTL_circ_buf_can_free_space - CAN Circular buffer free space
*
*   DESCRIPTION:
*       Returns the number of records in the CAN packet
*       circular buffer.
*
*/
uint32_t UTL_circ_buf_can_free_space
    (
    UTL_circ_buf_can_t * cb
    )
{
//uint32_t free_bytes =

return( ( cb->data_size / sizeof(flexcan_frame_t) ) - UTL_circ_buf_can_used_space( cb ) );
} /* UTL_circ_buf_can_free_space() */


/*
*   PROCEDURE NAME:
*       UTL_circ_buf_can_init - Initialize CAN circular buffer
*
*   DESCRIPTION:
*       Initializes the CAN circular buffer by allocating memory of size
*       num_rcrds and setting indices to zero and indicating not full.
*
*/
void UTL_circ_buf_can_init
    (
	UTL_circ_buf_can_t * 	cb,
	flexcan_frame_t *		buf,
	uint32_t				num_rcrds
    )
{
cb->mem 		= (uint8_t*)buf;
cb->data_size 	= num_rcrds * sizeof(flexcan_frame_t);
cb->read_idx 	= 0;
cb->write_idx 	= 0;
cb->full 		= false;
} /* UTL_circ_buf_can_init() */


/*
*   PROCEDURE NAME:
*       UTL_circ_buf_can_read - CAN Circular buffer read
*
*   DESCRIPTION:
*       Read up to num_rcrds CAN packets from cb into buf. If buf is NULL,
*       the read_idx member of cb is updated but no memory is actually
*       copied. Returns the number of records read into buf.
*
*/
uint32_t UTL_circ_buf_can_read
    (
	UTL_circ_buf_can_t * 	cb,
	flexcan_frame_t * 		buf,
	uint32_t 				num_rcrds
    )
{
uint32_t              copy_size;
uint32_t              copy0;
uint32_t              copy1;
uint32_t              read_idx;
UBaseType_t 		uxSavedInterruptStatus;

copy_size = UTL_circ_buf_can_used_space( cb );
copy_size = minval( copy_size, num_rcrds );

copy_size = copy_size * sizeof(flexcan_frame_t);

if( !copy_size )
    {
    goto out_free;
    }

read_idx = cb->read_idx;


//If copy goes off the end of the buffer, do it in two parts.
//Otherwise do it in one big copy.
if( read_idx + copy_size > cb->data_size )
    {
    copy0 = cb->data_size - read_idx;
    copy1 = copy_size - copy0;
    if( buf )
        {
        memmove( buf, &cb->mem[read_idx], copy0 );
        buf += copy0;
        memmove( buf, cb->mem, copy1 );
        }
    read_idx = copy1;
    }
else
    {
    if( buf )
        {
        memmove( buf, &cb->mem[read_idx], copy_size );
        }
    read_idx += copy_size;
    if( read_idx == cb->data_size )
        {
        read_idx = 0;
        }
    }

//Update read index and full status. Disable interrupts
//if not running in interrupt context.
uxSavedInterruptStatus = taskENTER_CRITICAL_FROM_ISR();
cb->read_idx = read_idx;
cb->full = false;
taskEXIT_CRITICAL_FROM_ISR(uxSavedInterruptStatus);

out_free:
return( copy_size / sizeof(flexcan_frame_t) );
} /* UTL_circ_buf_can_read() */


/*
*   PROCEDURE NAME:
*       UTL_circ_buf_reset_to - Reset read and write indexes
*
*   DESCRIPTION:
*       Reset the read and write pointers to a specific offset
*       in the buffer.
*
*/
void UTL_circ_buf_can_reset
    (
    UTL_circ_buf_can_t *     cb
    )
{
cb->full 		= false;
cb->write_idx	= 0;
cb->read_idx 	= 0;
} /* UTL_circ_buf_can_reset() */

/*
*   PROCEDURE NAME:
*       UTL_circ_buf_can_write - CAN Circular buffer write
*
*   DESCRIPTION:
*       Write up to buf_size bytes into cb from buf. If buf is NULL,
*       the write_idx member of cb is updated but no memory is actually
*       copied. Returns the number of bytes written into buf.
*
*/
uint32_t UTL_circ_buf_can_write
    (
	UTL_circ_buf_can_t * 	cb,
	flexcan_frame_t * 		buf,
	uint32_t 				num_rcrds
    )
{
uint32_t              copy_size;
uint32_t              copy0;
uint32_t              copy1;
uint32_t              write_idx;
UBaseType_t 		uxSavedInterruptStatus;

copy_size = UTL_circ_buf_can_free_space( cb );
copy_size = minval( copy_size, num_rcrds );

copy_size = copy_size * sizeof(flexcan_frame_t);

if( !copy_size )
    {
    goto out_free;
    }

write_idx = cb->write_idx;

//If copy goes off the end of the buffer, do it in two parts.
if( write_idx + copy_size > cb->data_size )
    {
    copy0 = cb->data_size - write_idx;
    copy1 = copy_size - copy0;
    if( buf != NULL )
        {
        memmove( &cb->mem[write_idx], buf, copy0 );
        buf += copy0;
        memmove( cb->mem, buf, copy1 );
        }
    write_idx = copy1;
    }
else
    {
    if( buf )
        {
        memmove( &cb->mem[write_idx], buf, copy_size );
        }
    write_idx += copy_size;
    if( write_idx == cb->data_size )
        {
        write_idx = 0;
        }
    }

//Update write index and full status.
uxSavedInterruptStatus = taskENTER_CRITICAL_FROM_ISR();
cb->write_idx = write_idx;
cb->full = (cb->read_idx == cb->write_idx);
taskEXIT_CRITICAL_FROM_ISR(uxSavedInterruptStatus);

out_free:
//return copy_size;
return( copy_size / sizeof(flexcan_frame_t) );
} /* UTL_circ_buf_write() */


/*
*   PROCEDURE NAME:
*       UTL_circ_buf_can_used_space - CAN Circular buffer used space
*
*   DESCRIPTION:
*       Returns the number of bytes in cb.
*
*/
uint32_t UTL_circ_buf_can_used_space
    (
    UTL_circ_buf_can_t * cb
    )
{
uint32_t	ret = 0;
//UBaseType_t uxSavedInterruptStatus;

//uxSavedInterruptStatus = taskENTER_CRITICAL_FROM_ISR();

if( cb->full )
    {
	ret = ( cb->data_size / sizeof(flexcan_frame_t) );
    }
else if( cb->write_idx >= cb->read_idx )
    {
    ret = ( ( cb->write_idx - cb->read_idx ) / sizeof(flexcan_frame_t) );
    }
else
    {
    ret = ( ( cb->data_size - (cb->read_idx - cb->write_idx) ) / sizeof(flexcan_frame_t) );
    }

//taskEXIT_CRITICAL_FROM_ISR(uxSavedInterruptStatus);

//PRINTF( "used_space = %d\r\n", ret );

return ret;

} /* UTL_circ_buf_can_used_space() */

