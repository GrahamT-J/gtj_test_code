/*******************************************************************************
FILENAME: Protocol.c

DESCRIPTION:

NOTES:

Copyright (c) Digital Monitoring Products Inc. 2007.  All rights reserved.

*******************************************************************************/
/*----compilation control-----------------------------------------------------*/

/*******************************************************************************
INCLUDE FILES
*******************************************************************************/
/*----system and platform files (external libraries) -------------------------*/


/*----program files-----------------------------------------------------------*/
#include "types.h"
#include "ProtocolTiming.h"
/*******************************************************************************
EXTERNAL REFERENCES
*******************************************************************************/
/*----data declarations-------------------------------------------------------*/

/*----function prototype------------------------------------------------------*/


/*******************************************************************************
PRIVATE DECLARATIONS
*******************************************************************************/
/*----defines, typedefs, structs, unions, enums-------------------------------*/

/*----data declarations-------------------------------------------------------*/

/*----function prototype------------------------------------------------------*/
static char SlotNumberToOutputGroup(SlotNumber slot);

/*----macros------------------------------------------------------------------*/

/*******************************************************************************
PRIVATE FUNCTION DEFINITIONS
*******************************************************************************/
/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/


/*******************************************************************************
PUBLIC FUNCTION DEFINITIONS
*******************************************************************************/
/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
SlotNumber nextSlot(SlotNumber slot)
{
  if (slot >= MAX_SLOT_NUMBER)
  {
    return (SlotNumber)0;
  }
  else
  {
    return (SlotNumber)(slot + 1);
  }
}

/*******************************************************************************

DESCRIPTION: Function will add the two slot numbers together and return the result

NOTES:

*******************************************************************************/
SlotNumber AddSlots(SlotNumber slot, SlotNumber numToAdd)
{
  SlotNumber newSlot = slot + numToAdd;
  while (newSlot >= TOTAL_SLOTS) newSlot -= TOTAL_SLOTS;
  return newSlot;
}

/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
SlotNumber subtractSlots(SlotNumber slot1, SlotNumber slot2)
{
  if (slot1 < slot2)
  {
    slot1 += TOTAL_SLOTS;
  }
  return (SlotNumber)(slot1 - slot2);
}

/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
SlotNumber zoneToSlotNumber(SlotTransmitterNumber zone)
{
  SlotTransmitterNumber zoneCalc = zone;
  SlotNumber slotCalc = (SlotNumber)0;

  while (zoneCalc >= TOTAL_GROUPS)
  {
    slotCalc += (SLOTS_BETWEEN_CHECKIN_SLOTS + 1);
    zoneCalc -= TOTAL_GROUPS;
  }
  slotCalc += zoneCalc * SLOTS_PER_GROUP;

  return slotCalc;
}

/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
SlotTransmitterNumber slotToZoneNumber(SlotNumber slot)
{
  char slotGroup = slot / SLOTS_PER_GROUP;
  char groupOffset = slot % SLOTS_PER_GROUP;
  char zoneOffset = groupOffset / (SLOTS_BETWEEN_CHECKIN_SLOTS + 1);
  SlotTransmitterNumber zone = INVALID_TRANSMITTER_NUMBER;

  if (((groupOffset % (SLOTS_BETWEEN_CHECKIN_SLOTS + 1)) == 0) &&
      (zoneOffset < CHECKIN_SLOTS_PER_GROUP))
  {
    zone = (SlotTransmitterNumber)(slotGroup + (zoneOffset * TOTAL_GROUPS));  // NOTE: this is a non-power of 2 multiply
  }

  return zone;
}

/*******************************************************************************

DESCRIPTION: Function to convert from a valid output number to its checkin slot number

NOTES: Ouput must be greater than TOTAL_SLOT_ZONE and less than TOTAL_TRANSMITTER_SLOTS

*******************************************************************************/
SlotNumber OutputToSlotNumber(SlotTransmitterNumber Output)
{
  int groupOffset;
  int groupNumber;
  int returnSlot = INVALID_SLOT;

  Output -= TOTAL_SLOT_ZONES;
  if (Output < SLOW_OUTPUTS_PER_GROUP)
  {
    returnSlot = (Output * SLOTS_BETWEEN_OUTPUT_SLOTS) + 11;
  }
  else
  {
    groupNumber = ((Output / FAST_OUTPUTS_PER_GROUP) - FAST_OUTPUTS_PER_GROUP);
    groupOffset = ((Output % FAST_OUTPUTS_PER_GROUP) + SLOW_OUTPUTS_PER_GROUP);
    returnSlot = (groupNumber * SLOTS_PER_OUTPUT_GROUP) + (groupOffset * SLOTS_BETWEEN_OUTPUT_SLOTS) + 11;
  }
  return returnSlot;
}

/*******************************************************************************

DESCRIPTION: Wrapper function to detect and call the correct function to convert from
              Transmitter Number to Slot Number
NOTES:

*******************************************************************************/
SlotNumber TransmitterToSlotNumber(SlotTransmitterNumber transmitter)
{
  SlotNumber returnSlot = INVALID_SLOT;

  if (transmitter >= TOTAL_SLOT_ZONES)
    returnSlot = OutputToSlotNumber(transmitter);
  else
    returnSlot = zoneToSlotNumber(transmitter);

  return returnSlot;
}

/*******************************************************************************

DESCRIPTION: Finds output group for given slot number

NOTES:

*******************************************************************************/
static char SlotNumberToOutputGroup(SlotNumber slot)
{
  return ((slot - 11) / SLOTS_PER_OUTPUT_GROUP);
}

/*******************************************************************************

DESCRIPTION: Determines if a given slot is in the primary group for slow outputs

NOTES:

*******************************************************************************/
bool IsPrimarySlowOutputGroup(SlotNumber slot)
{
  return (SlotNumberToOutputGroup(slot) == 0);
}
