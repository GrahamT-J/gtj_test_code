/*H*****************************************************************************
FILENAME:

DESCRIPTION:

NOTES:

Copyright (c) Digital Monitoring Products Inc. 2009 - 2017. All rights reserved.

*****************************************************************************H*/
#ifndef  INTERRUPT_INPUTS_H
#define  INTERRUPT_INPUTS_H

/*----compilation control-----------------------------------------------------*/
#include "packet.h"
#include "sleep.h"
/*******************************************************************************
INCLUDE FILES   Note: Minimize nesting of header files.
*******************************************************************************/
/*----system and platform files-----------------------------------------------*/

/*----program files-----------------------------------------------------------*/

/*******************************************************************************
FILE CONTEXT
*******************************************************************************/
/*----defines, typedefs, structs, unions, enums-------------------------------*/
// note, for proper sync, the debounce time must equal an integral number of hops
// with no fraction.
// For example, with 32 hops per second and a debounce of 500mS, there are exactly
// 16 hops during debounce.
enum
{
  DEBOUNCE_MS = 375,
  DEBOUNCE_TIME = ((long)DEBOUNCE_MS * SLEEP_TICKS_PER_SECOND) / 1000
};

/*----data descriptions-------------------------------------------------------*/

/*----data declarations-------------------------------------------------------*/

/*----macros------------------------------------------------------------------*/

/*----function prototypes-----------------------------------------------------*/
extern void setupInputs(void);
extern void initInputs(bool hardReset);
extern void startInputDebounce(short multiplier);
extern void testBattery(void);
extern char getInputState(void);

#endif                                  /* end of file */
