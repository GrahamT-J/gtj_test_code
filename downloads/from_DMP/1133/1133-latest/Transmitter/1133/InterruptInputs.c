/*H*****************************************************************************
FILENAME:

DESCRIPTION:

NOTES:

Copyright (c) Digital Monitoring Products Inc. 2009 - 2017. All rights reserved.

*****************************************************************************H*/
/*----compilation control-----------------------------------------------------*/

/*******************************************************************************
INCLUDE FILES
*******************************************************************************/
/*----system and platform files (external libraries) -------------------------*/
#include "types.h"

/*----program files-----------------------------------------------------------*/
#include "Sleep.h"
#include "transmitterprotocol.h"
#include "IOPortPins.h"
#include "InterruptInputs.h"



/*******************************************************************************
EXTERNAL REFERENCES   Only used if not available in a header file.
*******************************************************************************/
/*----data declarations-------------------------------------------------------*/

extern ZoneFlags myZoneFlags;
/*----function prototype------------------------------------------------------*/

/*******************************************************************************
PRIVATE DECLARATIONS
*******************************************************************************/
/*----defines, typedefs, structs, unions, enums-------------------------------*/
// reuse same bits from message byte
typedef enum
{
  CS_TAMPER_SW = MB_TAMPER
} ContactState;

// it takes this many transmits at low battery before low battery is shown
enum {LOW_BATTERY_COUNTER_THRESHHOLD = 10};

typedef struct
{
  BITFIELD lowBattery : 1;
  BITFIELD counter    : 7;
} LowBatteryState;

/*----data declarations-------------------------------------------------------*/
ContactState contactSteadyState;
LowBatteryState lowBatteryState;

/*----function prototype------------------------------------------------------*/

/*----macros------------------------------------------------------------------*/

/*******************************************************************************
PRIVATE FUNCTION DEFINITIONS
*******************************************************************************/
/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
static ContactState readCurrentContactState(void)
{
  ContactState currentState = (ContactState)0;

  if (!getIO(TAMPER))
    currentState |= CS_TAMPER_SW;

  return currentState;
}

/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
static bool processInputStateChange(void)
{
  bool changed = false;
  ContactState currentState;

  currentState = readCurrentContactState();
  // if steady state different from current state
  if (contactSteadyState != currentState)
  {
    // process state change
    contactSteadyState = currentState;
    changed = true;

    if (currentState & CS_TAMPER_SW)
      ackStatus.counter = 0;
  }
  return changed;
}

/*******************************************************************************

DESCRIPTION:

NOTES: This assumes that any edge interrupts on this port are for alarm contacts

*******************************************************************************/
extern void rfDataClockEdge(void);
#pragma vector = PORT1_VECTOR
#pragma type_attribute = __interrupt
void port1EdgeInterrupt(void)
{
  if (getIOIntFlag(RF_DATA_CLK))
  {
    clearIOIntFlag(RF_DATA_CLK);
    rfDataClockEdge();
  }
  else if (getIOIntFlag(TAMPER))
  {
    clearIOIntFlag(TAMPER);
    clearIOEnableInt(TAMPER);
    startInputDebounce(DEBOUNCE_TIME);
  }
  else
  {
    P1IFG = 0;        // clear all edge interrupts
    P1IE &= RF_DATA_CLK_MASK | TAMPER_MASK; // disable all other interrupts
  }
}


/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
#pragma vector = TIMERA0_VECTOR
#pragma type_attribute = __interrupt
void debounceInterrupt(void)
{
  if (!startupDelayComplete)
  {
    __low_power_mode_off_on_exit();
  }
  else if (!getCurrentlyCommunicating()) // only process a change if not already communicating
  {
    if (processInputStateChange())
    {
      if (wakeup(true))
      {
        __low_power_mode_off_on_exit();
      }
    }
    else
    {
      setupInputs();
    }
  }
  TACCTL0 = 0; // stop debounce
}


/*******************************************************************************
PUBLIC FUNCTION DEFINITIONS
*******************************************************************************/
/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
void setupInputs(void)
{
  clearIOEnableInt(TAMPER);
  if(getIO(TAMPER))
    setIOFallingInt(TAMPER);
  else
    setIORisingInt(TAMPER);
  clearIOIntFlag(TAMPER);
  setIOEnableInt(TAMPER);

  TACCTL0 = 0;  // no timer interrupts
}

/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
void initInputs(bool hardReset)
{
  setIODirIn(TAMPER);
  setIODirIn(BATTERY_VOLTAGE);

  // Setup the voltage reference enable pin for low battery detection
  setIODirOut(BATT_VOLTAGE_ENABLE);
  clearIOSel(BATT_VOLTAGE_ENABLE);
  clearIO(BATT_VOLTAGE_ENABLE);

  // Input buffer P2.3 disabled
  CAPD = BATTERY_VOLTAGE_MASK;

  if (hardReset)
    StartupDelay();

  contactSteadyState = readCurrentContactState();
  setupInputs();
}

/*******************************************************************************

DESCRIPTION:  Starts debounce timer so it will interrupt at debounce timer plus
  the amount of time for it to start at an appropriate hop

NOTES:  If SLEEP_TICKS_PER_HOP ever becomes a value that is not a power of two, the
  modulo operator will take much longer and this will probably need to change

*******************************************************************************/
void startInputDebounce(short debounceTime)
{
  if (! (TACCTL0 & CCIE)) // if not already debouncing
  {
    unsigned short tempTar = TAR;
    TACCR0 = tempTar + debounceTime + // current time plus debounce time plus
             SLEEP_TICKS_PER_HOP -      // an exra hop minus
             WAKEUP_CLOCK_SLOP_TICKS -  // some padding for clock slop minus
             ((tempTar - sleepTime) % SLEEP_TICKS_PER_HOP); // how far off we currently are from start of hop
    TACCTL0 = CCIE;  // enable interrupt and set compare mode
  }
}

/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
void testBattery(void)
{
  if (CACTL2 & CAOUT)
  {
    if (lowBatteryState.counter < LOW_BATTERY_COUNTER_THRESHHOLD)
    {
      lowBatteryState.counter++;
    }
    else
    {
      lowBatteryState.lowBattery = true;
    }
  }
  else      // a single "good" resets everything
  {
    lowBatteryState.counter = 0;
    lowBatteryState.lowBattery = false;
  }
}

/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
char getInputState(void)
{
  char state = contactSteadyState;

  if (lowBatteryState.lowBattery)
    state |= MB_LOW_BATTERY;

  if (GetWakeupReason() == WR_CHECKIN)
    state |= MB_CHECKIN;

  return state;
}
