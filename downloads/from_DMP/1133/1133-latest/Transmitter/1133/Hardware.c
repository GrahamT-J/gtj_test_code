/*******************************************************************************
FILENAME: Hardware.c

DESCRIPTION: Miscellaneous hardware descriptions not included elsewhere

NOTES:

Copyright (c) Digital Monitoring Products Inc. 2009.  All rights reserved.

*******************************************************************************/
/*----compilation control-----------------------------------------------------*/

/*******************************************************************************
INCLUDE FILES
*******************************************************************************/
/*----system and platform files (external libraries) -------------------------*/

/*----program files-----------------------------------------------------------*/
#include "Types.h"
#include "Hardware.h"
#include "IOPortPins.h"
#include "Sleep.h"
#include "InterruptInputs.h"
#include "ChipconCommunication.h"


/*******************************************************************************
EXTERNAL REFERENCES   Only used if not available in a header file.
*******************************************************************************/
/*----data declarations-------------------------------------------------------*/
bool startupDelayComplete = false;

/*----function prototype------------------------------------------------------*/


/*******************************************************************************
PRIVATE DECLARATIONS
*******************************************************************************/
/*----defines, typedefs, structs, unions, enums-------------------------------*/
enum {Delta = ((MASTER_CLOCK / (SLEEP_MASTER_CLOCK / SLEEP_CLOCK_DIVIDER)) & 0xFFFE)};  // must be even for test
#define STARTUP_DELAY (SLEEP_TICKS_PER_SECOND * 2) // 2 Seconds

/*----data declarations-------------------------------------------------------*/
byte RF_ONState;

/*----function prototype------------------------------------------------------*/

/*----macros------------------------------------------------------------------*/

/*******************************************************************************
PRIVATE FUNCTION DEFINITIONS
*******************************************************************************/
/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/


/*******************************************************************************
PUBLIC FUNCTION DEFINITIONS
*******************************************************************************/
/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
void setDCO(void)
{
  word startCapture = 0;
  bool finished = false;
  CCTL2 = CCIS0 | CM0 | CAP;
  TACTL = TASSEL_2 + MC_2 + TACLR;          // SMCLK, cont-mode, clear
  while (! finished)
  {
    word diff;
    while (! (CCTL2 & CCIFG))  // loop until capture
      ;

    CCTL2 &= ~(CCIFG);  // clear copture

    diff = (CCR2 - startCapture) & 0xFFFE;
    startCapture = CCR2;

    if (diff == Delta)
    {
      CCTL2 = 0;
      finished = true;
    }
    else if (diff > Delta)
    {
      if (DCOCTL-- == 0) // if rollover
      {
        --BCSCTL1;
      }
    }
    else
    {
      if (++DCOCTL == 0) // if rollover
      {
        ++BCSCTL1;
      }
    }
  }
}


/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
void initSys(bool hardReset)
{
  BCSCTL1 |= DIVA0 * SLEEP_CLOCK_DIVIDER_SETTING;

  // Select the minimum internal load capacitance value (~1pF).  The board includes 11pF
  // plus PCB traces, and the crystal is specified for 12.5pF.  The default value is 6pF.
  BCSCTL3 &= ~(XCAP0 | XCAP1);

  // Configure unconnected pins as outputs (prevents floating inputs / reduces power consumption)
  P5DIR |= BIT0;

  WDTCTL = WDTPW | WDTCNTCL | WDTSSEL;  // start watchdog, reset, and use ACLK

  LED_OFF(GRN_LED);
  LED_OFF(RED_LED);
  setIODirOut(GRN_LED);
  setIODirOut(RED_LED);

  setDCO();
  initRFChip();

  initInputs(hardReset);
  wakeup(true);
}


/*******************************************************************************

DESCRIPTION: Function to power off the MSP430's onboard Comparator

NOTES:

*******************************************************************************/
void PowerDownComparator(void)
{
  CACTL1 &= ~CAON;  // turn off comparator
  clearIO(BATT_VOLTAGE_ENABLE);   //  turn off the voltage reference generator
}


/*******************************************************************************

DESCRIPTION:  Function to power on the MSP430's onboard Comparator

NOTES:

*******************************************************************************/
void PowerUpComparator(void)
{
  setIO(BATT_VOLTAGE_ENABLE); // Turn on the voltage reference generator
  CACTL1 = CARSEL | CAREF_2;  // Ref applied to the - terminal, Ref 0.50*Vcc
  CACTL2 = P2CA0 | CAF;       // CA0 applied to the + terminal, Comparator output is filtered
  CACTL1 |= CAON;             // Turn on comparator
}

/*******************************************************************************

DESCRIPTION: Uses Timer A to put the processor to sleep for STARTUP_DELAY time

NOTES:

*******************************************************************************/
void StartupDelay(void)
{
  TACTL = TASSEL_1 + // ACLK
          MC_2 +     // Continuous
          SLEEP_TIMERA_DIVIDER_SETTING * ID0;
  TACCR0 = TAR + STARTUP_DELAY;
  TACCTL0 = CCIE;
  WDTCTL = WDTPW | WDTHOLD | WDTCNTCL | WDTSSEL;  // stop watchdog
  __enable_interrupt();
  (void)LPM3;
  __disable_interrupt();
  WDTCTL = WDTPW | WDTCNTCL | WDTSSEL;  // start watchdog, reset, and use ACLK
  startupDelayComplete = true;
}
