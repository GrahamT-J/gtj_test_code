/*H*****************************************************************************
FILENAME: IndicatorLED.c

DESCRIPTION: Controls the red/green indicator LED based on keypad status.

NOTES:

Copyright (c) Digital Monitoring Products Inc. 2017.  All rights reserved.

*****************************************************************************H*/

#include "types.h"

#include <msp430.h>

#include "IndicatorLED.h"
#include "ioportpins.h"
#include "Packet.h"
#include "Sleep.h"

static bool systemIsArmed;
static bool systemInAlarm;

static uint8 userCodeSentTimer;
static bool userCodeAccepted;

static uint16 blinkPeriodTicks;
static bool enableSteady;

#define DELAY_1s   (SLEEP_TICKS_PER_SECOND * 1)
#define DELAY_2s   (SLEEP_TICKS_PER_SECOND * 2)
#define DELAY_5s   (SLEEP_TICKS_PER_SECOND * 5)
#define DELAY_10ms (SLEEP_TICKS_PER_SECOND / 100)

/*F****************************************************************************
*
* Name: ScheduleTimingInterrupt()
* Description:  Setup the the timing interrupt to fire some number of ticks in
*               the future.
* Parameters:   - number of ticks until next interrupt
* Return value: None
*
*****************************************************************************F*/
static void ScheduleTimingInterrupt(uint16 ticks)
{
  uint16 tar;

  CCTL1 = 0;

  do
  {
    tar = TAR;
  } while(tar != TAR);

  CCR1 = tar + ticks;
  CCTL1 = CCIE;
}

/*F****************************************************************************
*
* Name: IndicatorTimingInterrupt()
* Description:  Called on each timing interrupt.  Setup the LEDs and the timer
*               for the next segment of the current blinking pattern.
* Parameters:   None
* Return value: None
*
*****************************************************************************F*/
void IndicatorTimingInterrupt(void)
{
  // Check if a steady pulse has just expired
  if (enableSteady)
  {
    enableSteady = false;

    // If the blinking pattern is enabled, begin the OFF phase
    if (blinkPeriodTicks != 0)
    {
      ScheduleTimingInterrupt(blinkPeriodTicks);
      LED_OFF(RED_LED);
    }
    else
    {
      // The blinking pattern is not enabled - turn off the LEDs and do not schedule another interrupt.
      LED_OFF(RED_LED);
    }
  }
  // Check if the blinking pattern is enabled
  else if (blinkPeriodTicks != 0)
  {
    if (getIOOut(RED_LED))
    {
      ScheduleTimingInterrupt(blinkPeriodTicks);
      LED_OFF(RED_LED);
    }
    else
    {
      ScheduleTimingInterrupt(DELAY_10ms);
      LED_ON(RED_LED);
    }
  }
  else
  {
    LED_OFF(RED_LED);
  }

  // In any case, the green LED should now be off
  LED_OFF(GRN_LED);
}

/*F****************************************************************************
*
* Name: ProcessIndicatorState()
* Description:  Called every 1/2 second at the end of the keypad slot.  
* Parameters:   None
* Return value: None
*
*****************************************************************************F*/
void ProcessIndicatorState(void)
{
  if (userCodeSentTimer > 0)
  {
    userCodeSentTimer -= 1;
  }

  if (userCodeAccepted)
  {
    if (systemIsArmed)
    {
      LED_ON(RED_LED);
      LED_OFF(GRN_LED);
    }
    else
    {
      LED_OFF(RED_LED);
      LED_ON(GRN_LED);
    }
    ScheduleTimingInterrupt(DELAY_1s);
    enableSteady = true;
    userCodeAccepted = false;
  }
  else if (!enableSteady)
  {
    if (systemInAlarm)
    {
      if (blinkPeriodTicks != DELAY_2s)
      {
        blinkPeriodTicks = DELAY_2s;
        LED_ON(RED_LED);
        ScheduleTimingInterrupt(DELAY_10ms);
      }
    }
    else if (systemIsArmed)
    {
      if (blinkPeriodTicks != DELAY_5s)
      {
        blinkPeriodTicks = DELAY_5s;
        LED_ON(RED_LED);
        ScheduleTimingInterrupt(DELAY_10ms);
      }
    }
    else
    {
      blinkPeriodTicks = 0;
      LED_OFF(RED_LED);
      LED_OFF(GRN_LED);
    }
  }
}

/*F****************************************************************************
*
* Name: ProcessKeypadStatus()
* Description:  Parses the keypad status word to determine which LED pattern
*               should be active.
* Parameters:   - new keypad status word
* Return value: None
*
*****************************************************************************F*/
void ProcessKeypadStatus(KeypadStatusWord status)
{
  bool newArmedState = (status.statusColor == STATUS_COLOR_RED);
  if ((systemIsArmed != newArmedState) && (userCodeSentTimer > 0))
  {
    userCodeAccepted = true;
    userCodeSentTimer = 0;
  }

  systemIsArmed = newArmedState;
  systemInAlarm = (status.redBacklight == 1);
}

/*F****************************************************************************
*
* Name: StartUserCodeSentTimer()
* Description:  Starts the timer that determines if a user code was sent
*               recently enough to flash the LED if the armed state changes.
* Parameters:   None
* Return value: None
*
*****************************************************************************F*/
void StartUserCodeSentTimer(void)
{
  userCodeSentTimer = 12;  // decremented on each keypad slot, so 12 ticks == 6 seconds
}
