  /*******************************************************************************
FILENAME: IOPortPins.h

DESCRIPTION:

NOTES:

Copyright (c) Digital Monitoring Products Inc. 2009.  All rights reserved.

*******************************************************************************/

#ifndef IOPORTPINS_H
#define IOPORTPINS_H

#include <msp430.h>

#define getFlag(A)          (A##_##BYTE & A##_MASK)
#define setFlag(A)          A##_##BYTE |= A##_MASK
#define toggleFlag(A)       A##_##BYTE ^= A##_MASK
#define clearFlag(A)        A##_##BYTE &= ~A##_MASK
#define getIO(A)            (A##_##IN & A##_MASK)
#define getIOOut(A)         (A##_##OUT & A##_MASK)
#define setIO(A)            A##_##OUT |= A##_MASK
#define toggleIO(A)         A##_##OUT ^= A##_MASK
#define clearIO(A)          A##_##OUT &= ~A##_MASK
#define setIODirOut(A)      A##_##DIR |= A##_MASK
#define setIODirIn(A)       A##_##DIR &= ~A##_MASK
#define setIOIntEdge(A)     (A##_##IES & A##_MASK)
#define setIOFallingInt(A)  A##_##IES |= A##_MASK
#define setIORisingInt(A)   A##_##IES &= ~A##_MASK
#define setIOEnableInt(A)   A##_##IE |= A##_MASK
#define clearIOEnableInt(A) A##_##IE &= ~A##_MASK
#define getIOIntFlag(A)     A##_##IFG & A##_MASK
#define clearIOIntFlag(A)   A##_##IFG &= ~A##_MASK
#define setIOSel(A)         A##_##SEL |= A##_MASK
#define clearIOSel(A)       A##_##SEL &= ~A##_MASK
#define enableIOResistor(A) A##_##REN |= A##_MASK
#define disableIOResistor(A) A##_##REN &= ~A##_MASK

#define LED_ON(A)  setIO(A)
#define LED_OFF(A) clearIO(A)
//-------------------------------------------------

#define BATTERY_VOLTAGE_MASK      BIT3
#define BATTERY_VOLTAGE_DIR       P2DIR
#define BATTERY_VOLTAGE_IN        P2IN
#define BATTERY_VOLTAGE_SEL       P2SEL

#define BATT_VOLTAGE_ENABLE_MASK  BIT5
#define BATT_VOLTAGE_ENABLE_DIR   P2DIR
#define BATT_VOLTAGE_ENABLE_OUT   P2OUT
#define BATT_VOLTAGE_ENABLE_SEL   P2SEL

#define RED_LED_MASK	          BIT1
#define RED_LED_OUT               P2OUT
#define RED_LED_DIR               P2DIR
#define RED_LED_SEL               P2SEL
#define RED_LED_IN                P2IN

#define GRN_LED_MASK	          BIT2
#define GRN_LED_OUT               P3OUT
#define GRN_LED_DIR               P3DIR
#define GRN_LED_SEL               P3SEL
#define GRN_LED_IN                P3IN

#define CSn_MASK                  BIT0
#define CSn_IN                    P1IN
#define CSn_OUT                   P1OUT
#define CSn_DIR                   P1DIR
#define CSn_SEL                   P1SEL

#define TAMPER_MASK               BIT1
#define TAMPER_IN                 P1IN
#define TAMPER_OUT                P1OUT
#define TAMPER_DIR                P1DIR
#define TAMPER_SEL                P1SEL
#define TAMPER_IES                P1IES
#define TAMPER_IE                 P1IE
#define TAMPER_IFG                P1IFG
#define TAMPER_REN                P1REN

#define SI_MASK                   BIT2
#define SI_IN                     P1IN
#define SI_OUT                    P1OUT
#define SI_DIR                    P1DIR
#define SI_SEL                    P1SEL

#define SCLK_MASK                 BIT0
#define SCLK_IN                   P2IN
#define SCLK_OUT                  P2OUT
#define SCLK_DIR                  P2DIR
#define SCLK_SEL                  P2SEL

#define SO_GDO1_MASK              BIT3
#define SO_GDO1_IN                P1IN
#define SO_GDO1_DIR               P1DIR
#define SO_GDO1_SEL               P1SEL
#define SO_GDO1_IE                P1IE
#define SO_GDO1_IES               P1IES
#define S0_GDO1_IFG               P1IFG

#define RF_DATA_CLK_MASK          SO_GDO1_MASK
#define RF_DATA_CLK_IN            SO_GDO1_IN
#define RF_DATA_CLK_DIR           SO_GDO1_DIR
#define RF_DATA_CLK_SEL           SO_GDO1_SEL
#define RF_DATA_CLK_IE            SO_GDO1_IE
#define RF_DATA_CLK_IES           SO_GDO1_IES
#define RF_DATA_CLK_IFG           S0_GDO1_IFG

#define GDO0_MASK                 BIT4
#define GDO0_IN                   P2IN
#define GDO0_DIR                  P2DIR
#define GDO0_SEL                  P2SEL
#define GDO0_OUT                  P2OUT

#define RF_DATA_IN_MASK           GDO0_MASK
#define RF_DATA_IN_IN             GDO0_IN
#define RF_DATA_IN_DIR            GDO0_DIR
#define RF_DATA_IN_SEL            GDO0_SEL

#define RF_DATA_OUT_MASK          GDO0_MASK
#define RF_DATA_OUT_IN            GDO0_IN
#define RF_DATA_OUT_DIR           GDO0_DIR
#define RF_DATA_OUT_SEL           GDO0_SEL
#define RF_DATA_OUT_OUT           GDO0_OUT

#endif
