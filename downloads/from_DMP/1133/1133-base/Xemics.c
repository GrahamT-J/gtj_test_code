/*H*****************************************************************************
FILENAME:

DESCRIPTION:

NOTES:

Copyright (c) Digital Monitoring Products Inc. 2004-2009.  All rights reserved.

*****************************************************************************H*/
/*----compilation control-----------------------------------------------------*/

/*******************************************************************************
INCLUDE FILES
*******************************************************************************/
/*----system and platform files (external libraries) -------------------------*/

/*----program files-----------------------------------------------------------*/
#include "Xemics.h"
#include "Packet.h"
#include "ioportpins.h"
#include "hop.h"

/*******************************************************************************
EXTERNAL REFERENCES   Only used if not available in a header file.
*******************************************************************************/
/*----data declarations-------------------------------------------------------*/

/*----function prototype------------------------------------------------------*/


/*******************************************************************************
PRIVATE DECLARATIONS
*******************************************************************************/
/*----defines, typedefs, structs, unions, enums-------------------------------*/
typedef enum
{
  XR_RTPARAM_H,
  XR_RTPARAM_L,
  XR_FSPARAM_H,
  XR_FSPARAM_M,
  XR_FSPARAM_L,
  XR_DATA_OUT,
  XR_ADPARAM_H,
  XR_ADPARAM_L,
  XR_PATTERN_H,
  XR_PATTERN_MH,
  XR_PATTERN_ML,
  XR_PATTERN_L
} XemicsRegister;

enum
{
  RTP_Rmode	=	1,			// Receive mode 0 = A, 1 = B
  RTP_Bits	=	1,			// Bitsync
#ifdef PROJECT_1101
  RTP_RSSI	=	0,
  RTP_FEI		=	1,
#else
  RTP_RSSI	=	1,
  RTP_FEI		=	0,
#endif
  RTP_BW		=	2,			// BB filter bandwidth 0 = 10kHz, 1 = 20, 2 = 40, 3 = 200
  RTP_Tpow	=	3,			// Transmit power 0 = 0dBm, 1 = 5, 2 = 10, 3 = 15
  RTP_Osc		=	0,			// 0 = use on-chip oscillator (never change), 1 = external signal
  RTP_WBB		=	1,			// Receiver wakeup 0 = boost, 1 = normal
  RTP_Filter	=	1,		// Transmit filter enable
  RTP_Fsel	=	1,			// FEI mode 0 = FFD demodulator, 1 = correlators
  RTP_Stair	=	0,			// Filter rise/fall time 0 = 10%, 1 = 20%
  RTP_Modul	=	1,			// Modulation inhibit 0 = modulation, 1 = inhibit
  RTP_RSSR	=	0,			// RSSI range 0 = low, 1 = high
  RTP_Clkout	=	0,		// Enable clkout

  FSP_Band	=	3,			// Band 902-928 MHz
  #ifdef FCC_SPECIAL_VERSION
  FSP_Dev		=	4,			// Freq dev. 0 = 5kHz, 1 = 10, 2 = 20, 3 = 40, 4 = 100
  #else
  FSP_Dev		=	2,			// Freq dev. 0 = 5kHz, 1 = 10, 2 = 20, 3 = 40, 4 = 100
  #endif
  FSP_BR		=	RF_BAUDRATE,			// Bitrate 0 = 4.8kp/s, 1 = 9.6, 2 = 19.2, 3 = 38.4, 4 = 76.8

  ADP_Pattern	=	1,		// Pattern on
#ifdef PROJECT_1101
  ADP_Psize	=	PT_PATTERN_SIZE - 1,			// Pattern size 0 = 8 bit, 1 = 16, 2 = 24, 3 = 32
#else
  ADP_Psize	=	PR_PATTERN_SIZE - 1,			// Pattern size 0 = 8 bit, 1 = 16, 2 = 24, 3 = 32
#endif
  ADP_Ptol	=	0,			// Number of tolerated errors in pattern	(0-3)
  ADP_Clkfreq	=	0,		// clkout freq. 0 = 1.22Mhz, 1 = 2.44, 2 = 4.87, 3 = 9.75
  ADP_IQA		=	1,			// IQ amplifiers on
  ADP_Res1	=	0,			// reserved, must be 0
  ADP_Invert	=	1,		// inverted data from receiver
  ADP_RegBW	=	1,			// regulation of bandwidth of base-band filter 0 = on, 1 = off
  ADP_Regfreq	=	0,		// regulation periodicity 0 = at startup, 1 = every minute receiver is on
  ADP_Regcond	=	0,		// regulation changing 0 = each time it's changed, 1 = not changed each time
  ADP_WBBcond	=	0,		// bb filter boose 0 = boost at each change, 1 = no boosting
  ADP_Xsel	=	0,			// external oscillator mode 0 = CL + CO = 15pF, 1 = 11pF (lower current)
  ADP_Res2	=	0,			// reserved

  RTPH  =	(RTP_Rmode << 7) | (RTP_Bits << 6) | (RTP_RSSI << 5) | (RTP_FEI << 4) | (RTP_BW << 2) | (RTP_Tpow),
  RTPL  =	(RTP_Osc << 7) | (RTP_WBB << 6) | (RTP_Filter << 5) | (RTP_Fsel << 4) | (RTP_Stair << 3) | (RTP_Modul << 2) |  (RTP_RSSR << 1) | (RTP_Clkout),
  FSPH  =	(FSP_Band << 6) | (FSP_Dev << 3) | (FSP_BR),
  ADPH  =	(ADP_Pattern << 7) | (ADP_Psize << 5) | (ADP_Ptol << 3) | (ADP_Clkfreq << 1) | (ADP_IQA),
  ADPL  =	(ADP_Res1 << 7) | (ADP_Invert << 6) | (ADP_RegBW << 5) | (ADP_Regfreq << 4) | (ADP_Regcond << 3) | (ADP_WBBcond << 2) | (ADP_Xsel << 1) | (ADP_Res2)
};

const char xemicsInitTable[] =
{
  RTPH,
  RTPL,
  FSPH,
  0x00,
  0x00,
  0xFF,
  ADPH,
  ADPL,
#ifdef PROJECT_1101
  RPAT_HIGH,
  RPAT_MIDDLE,
  RPAT_LOW,
#else
  TPAT_HIGH,
  TPAT_MIDDLE,
  TPAT_LOW,
#endif
  0x00
};
#define FACTORY_FREQUENCY_OFFSET_ADDRESS 0x1004
#define factoryFrequencyOffsetPtr (const signed int*)(FACTORY_FREQUENCY_OFFSET_ADDRESS)
#define factoryFrequencyOffset *factoryFrequencyOffsetPtr

typedef union
{
  unsigned char asByte;
  struct
  {
    BITFIELD  Spare : 4;
    BITFIELD  FEI   : 2;
    BITFIELD  RSSI  : 2;
  };
}XemicsDataOut;

// Xemics channels are centered around 915 MHz
// The frequency synthesizer step is (XTAL/77824) or 501.131 Hz
// The low channel is (915 MHz - (26 * 501.131 Hz * ChannelSpacing))
// The high channel is (915 MHz + (26 * 501.131 Hz * ChannelSpacing))
typedef enum
{
  STANDARD_CHANNEL_SPACING          = 896,  // 449 kHz spacing, 903.326 MHz to 926.674 MHz span
  LOW_INTERFERENCE_CHANNEL_SPACING  = 722   // 362 kHz spacing, 905.593 MHz to 924.407 MHz span
} ChannelSpacing;

/*----data declarations-------------------------------------------------------*/
static char rtpLowShadow;
static char rtpHighShadow;
static char currentRSSIData = 0;
static signed char FreqBias;
static char frequencyErrorByte;
static ChannelSpacing channelSpacing = STANDARD_CHANNEL_SPACING;

/*----function prototype------------------------------------------------------*/
void setXemicsEnable(void);

/*----macros------------------------------------------------------------------*/

/*******************************************************************************
PRIVATE FUNCTION DEFINITIONS
*******************************************************************************/
/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
static byte transmitToXemics(char address, char value)
{
  unsigned short transmitWord;
  char bitCounter;
  char returnValue = 0;

  // 0    : start (0)
  // 1    : read(1)/write(0)
  // 2-6  : address
  // 7-14 : data
  // 15-17: stop bits (high) (not part of transmit word, but sent anyway, 3 for good measure)
  transmitWord = ((address << 8) + value) << 1;

  setIO(XE_TXD);
  setIO(XE_CLK);
  clearIO(XE_ENABLE);
  for (bitCounter = 0; bitCounter < 18; ++bitCounter)
  {
    setIO(XE_CLK);
    if ((transmitWord & 0x8000) || (bitCounter >= 15)) // last three bits are 1 (stop bits)
    {
      setIO(XE_TXD);
    }
    else
    {
      clearIO(XE_TXD);
    }
    transmitWord <<= 1;
    clearIO(XE_CLK);
    if ((bitCounter >= 7) && (bitCounter <= 14))
    {
      returnValue <<= 1;
      if (getIO(XE_RXD) > 0)
      {
        returnValue |= 1;
      }
    }
  }
  return returnValue;
}

/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
static void writeXemics(char address, char value)
{
  // clear read/write flag, send value, and ignore return value
  transmitToXemics(address & 0x1F, value);
}

/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
void setXemicsRegister(XemicsRegister reg, char val)
{
  if (reg == XR_RTPARAM_L)
    rtpLowShadow = val;
  else if (reg == XR_RTPARAM_H)
    rtpHighShadow = val;

  writeXemics(reg, val);
}

/*******************************************************************************
PUBLIC FUNCTION DEFINITIONS
*******************************************************************************/
/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
void initXemicsChip(void)
{
  char i;

  FreqBias = 0;


  for (i = 0; i < sizeof(xemicsInitTable); ++i)
  {
    setXemicsRegister((XemicsRegister)i, xemicsInitTable[i]);
  }
  setXemicsEnable();

}


/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
void sendChannel(unsigned char channel)
{
  signed int WorkingVal;
  signed int Frequency;

#ifdef SINGLE_CHANNEL
  channel = SINGLE_CHANNEL;
#endif
  WorkingVal = channel - (NUM_HOP_CHANNELS / 2); // adjust to +/- values around 0

  Frequency = (WorkingVal * channelSpacing);

  Frequency += FreqBias;  // add in frequency bias
  Frequency += factoryFrequencyOffset; // add in factory programmed calibration offset

  setXemicsRegister(XR_FSPARAM_M, Frequency >> 8);
  setXemicsRegister(XR_FSPARAM_L, Frequency & 0xFF);
}

/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
void setModulation(bool mod)
{
  if (mod) rtpLowShadow &= ~0x04;
  else rtpLowShadow |= 0x04;

  setXemicsRegister(XR_RTPARAM_L, rtpLowShadow);
}

/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
void latchFrequencyError(void)
{
  CCTL2 = OUTMOD_0+CM0+CCIE+OUT;  // set transmit to latch FEI
  // set read/write flag and data address, send all 1's, and return value
  frequencyErrorByte = transmitToXemics(0x25, 0xFF);
  CCTL2 = OUTMOD_0+CM0+CCIE;      // clear transmit

}

/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
void applyFrequencyErrorOffset(void)
{
  frequencyErrorByte &= 0x30;
  if ((frequencyErrorByte == 0x30) && (FreqBias < 25)) // frequency too low
  {
    ++FreqBias;
  }
  else if ((frequencyErrorByte == 0x20) && (FreqBias > -25)) // frequency too high
  {
    --FreqBias;
  }
}



/*******************************************************************************

DESCRIPTION: Function will read the RSSI value from the Xemics chip and store in currentRSSI

NOTES:

*******************************************************************************/
void readRSSI(void)
{
  XemicsDataOut xemicsData;

  xemicsData.asByte = transmitToXemics(0x25, 0xFF); // get the RSSI data from Xemics
  if (xemicsData.RSSI > currentRSSIData)  // if the new value is larger than our current value
  {
    currentRSSIData = xemicsData.RSSI;  // ratchet up the current value
  }
}

/*******************************************************************************

DESCRIPTION: Returns the current value of currentRSSI

NOTES:

*******************************************************************************/
char getCurrentRSSI(void)
{
  return (currentRSSIData != 0);
}


/*******************************************************************************

DESCRIPTION: Will clear the current RSSI value

NOTES:

*******************************************************************************/
void clearCurrentRSSI(void)
{
  currentRSSIData = 0;
}


/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
void setXemicsEnable(void)
{
#ifdef PROJECT_1101
  byte Mask = ((RF_ON_OUT ^ RF_ONState) & RF_ON_MASK);  // bit set if rf on state is different than that of the port
  RF_ON_OUT ^= Mask;  // toggle (or not)
  _NOP();
  setIO(XE_ENABLE);
  RF_ON_OUT ^= Mask;  // toggle back (or not)
#else
  setIO(XE_ENABLE);
#endif
}

/*******************************************************************************

DESCRIPTION: Sets the frequency mode to either standard or low-interference

NOTES: The channel spacing is the only thing that needs to change for Xemics frequency modes

*******************************************************************************/
void SetFrequencyMode(FrequencyMode mode)
{
  if (mode == STANDARD_FREQUENCY_MODE)
    channelSpacing = STANDARD_CHANNEL_SPACING;
  else
    channelSpacing = LOW_INTERFERENCE_CHANNEL_SPACING;
}

/*******************************************************************************

DESCRIPTION: Reduces the transmit power level if needed (also sets it back
             to normal transmit power level if needed)

NOTES: The normal and reduced TX power levels are 15dBm and 10dBm, respectively
       rtpHighShadow holds the TX power level in bits 0-1
       Transmit power -> 0 = 0dBm, 1 = 5, 2 = 10, 3 = 15

*******************************************************************************/
void SetReducedTXPowerLevel(bool reducedTXPower)
{
  // Change the TX power level if it is not currently at the correct power level
  if (reducedTXPower == ((rtpHighShadow & 0x01) > 0))
  {
    rtpHighShadow ^= 0x01;
    setXemicsRegister(XR_RTPARAM_H, rtpHighShadow);
  }
}
