/*******************************************************************************
FILENAME:

DESCRIPTION:

NOTES:

Copyright (c) Digital Monitoring Products Inc. 2009.  All rights reserved.

*******************************************************************************/
/*----compilation control-----------------------------------------------------*/

/*******************************************************************************
INCLUDE FILES
*******************************************************************************/
/*----system and platform files (external libraries) -------------------------*/

/*----program files-----------------------------------------------------------*/
#include "types.h"
#include "MSP430SFR.h"
#include "IOPortPins.h"
#include "Hop.h"
#include "Debug.h"
#include "Inputs.h"
#include "WirelessCommunication.h"
#include "FCCTest.h"
#include "Sleep.h"
#include "TransmitterProtocol.h"

/*******************************************************************************
EXTERNAL REFERENCES   Only used if not available in a header file.
*******************************************************************************/
/*----data declarations-------------------------------------------------------*/

/*----function prototype------------------------------------------------------*/

/*******************************************************************************
PRIVATE DECLARATIONS
*******************************************************************************/
/*----defines, typedefs, structs, unions, enums-------------------------------*/
typedef enum
{
  FS_JUST_WOKEUP,
  FS_WAITING_FOR_FIRST_PATTERN,
  FS_WAITING_FOR_FIRST_ACK,
  FS_WAITING_FOR_SLOT_DEFINITION,
  FS_TRANSMITTING,
  FS_WAITING_FOR_ACK_PATTERN,
  FS_WAITING_FOR_ACK
} FrameStatus;

typedef enum
{
  WM_OFF,
  WM_RECEIVE,
  WM_TRANSMIT,
  WM_IDLE
} WirelessMode;


typedef enum
{
  RS_GOOD_SYNC,
  RS_TEMP_SYNC,
  RS_BAD_SYNC,
  RS_FORCE_NO_SYNC
} ReceiverSyncStatus;

// SyncCounter Description
//  SyncCounter can range from -RESYNC_FAILURE to +GOOD_SYNC_COUNT with a negative number meaning the transmitter is out of sync.  A positive number will signify the transmitter
//  is in some form of sync with a receiver and a +GOOD_SYNC_COUNT means that we are in absolute sync with our programmed receiver.  When a message from a wrong receiver is
//  received the count will be taken to INITIAL_SYNC_COUNT.  If we receive a message that we didn't understand the will either decrement the count by one if the count is positive
//  take the count to BUMP_SYNC_COUNT if the current count is negative.  If a frame timeout occurs the number will be decremented with the limit of either RESYNC_FAILUER or
//  RECEIVER_FAIL_RESYNC_FAILURE based upon if the receiver is currently in failure.
typedef enum
{
  INITIAL_SYNC_COUNT = 0,
  BUMP_SYNC_COUNT = 1,
  GOOD_SYNC_COUNT = 5,
  RESYNC_TRIES    = 64,  // Set to allow a transmitter to listen through all hop channels (53) but still be a power of two.  MUST BE A POWER OF TWO!!
  RESYNC_FAILURE  = -(3 * RESYNC_TRIES) - 1, // Set to allow three times of RESYNC_TRIES but minus 1 to make it not a power of RESYNC_TRIES.  MUST NOT BE A POWER OF RESYNC_TRIES
  RECEIVER_FAIL_RESYNC_FAILURE = -RESYNC_TRIES - 1 // Set to allow a single RESYNC_TRIES but minus 1 to make it not a power of RESYNC_TRIES.  MUST NOT BE A POWER OF RESYNC_TRIES
} SyncCounter;

typedef enum
{
  DEFAULT_WAKEUP_FRAME_TIMEOUT = FB_TOTAL,
  STANDARD_FRAME_TIMEOUT = FB_TOTAL,
} FrameTimeout;

// lastResortTimer is really just a "safety net" to make sure we timeout after a reasonable amount of time
typedef enum
{
  LAST_RESORT_TIMEOUT = 200 // number of frame timeouts before we give up
} LastResortTimer;

typedef enum
{
  MSS_AWAKE,
  MSS_SUCCESS,
  MSS_FAILURE,
  MSS_RETRY
} MainSleepState;
/*----data declarations-------------------------------------------------------*/
SlotNumber currentSlotNumber;

static FrameStatus frameStatus = FS_JUST_WOKEUP;
static SyncCounter syncCounter;
static char hopOffset;
static FrameTimeout frameTimeout;
static LastResortTimer lastResortTimer;
static MainSleepState mainSleepState;
static FrequencyMode primaryFrequencyMode = STANDARD_FREQUENCY_MODE;
static FrequencyMode currentFrequencyMode = STANDARD_FREQUENCY_MODE;

const char versionString[] = "106";   // must be 3 digits
const char dateString[] = "11/17/09"; // must be mm/dd/yy
const char copyrightString[] = "(c)2009 DMP";
#pragma required =  copyrightString

/*----function prototype------------------------------------------------------*/

/*----macros------------------------------------------------------------------*/

/*******************************************************************************
PRIVATE FUNCTION DEFINITIONS
*******************************************************************************/


/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
static void setWirelessMode(WirelessMode mode)
{

  if (mode == WM_OFF)
  {
    setupWirelessSleep();
    PowerDownComparator();
  }
#ifdef USING_CHIPCON_RF_CHIP
  else if (mode == WM_IDLE)
  {
    setupWirelessIdle();
  }
#endif
  else
  {
#ifdef USING_ACK_LED
#if defined USING_1232_HARDWARE || defined DMP_1119_TRANSMITTER
    if( ackStatus.counter <=3 && getAlarmWakeup())
#else
    if (ackStatus.counter <= 3)
#endif
    {
      LED_ON(RED_LED);  //LED ON
    }
#endif
    PowerUpComparator();
    if (mode == WM_RECEIVE)
    {
#ifdef USING_CHIPCON_RF_CHIP
      setupWirelessIdle();
#endif
      setupWirelessReceive();
    }
    else
    {
      setupWirelessTransmit();
    }
  }

}

/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
static void followHop(void)
{
  currentChannel = addChannel(currentChannel, hopOffset);
  SetFrequency(currentChannel);
  setWirelessMode(WM_RECEIVE);
  currentSlotNumber = nextSlot(currentSlotNumber);
}

/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
static void resetState()
{
  communicationStatus.frameStartDetected = false;
  frameStatus = FS_WAITING_FOR_FIRST_PATTERN;
}

/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
static SyncCounter resyncFailCount()
{
  if (receiverFailed())
  {
    return RECEIVER_FAIL_RESYNC_FAILURE;
  }
  return RESYNC_FAILURE;
}

/*******************************************************************************

DESCRIPTION:  Sets the synchronization status.  We are either in sync with the
 receiver or not.  Once we get good sync, we are in sync several tries (GOOD_SYNC_COUNT)

NOTES:  This is somewhat tricky, so be careful with it

*******************************************************************************/
static void setReceiverSync(ReceiverSyncStatus status)
{
  switch (status)
  {
  case RS_GOOD_SYNC:
    syncCounter = GOOD_SYNC_COUNT;
    hopOffset = ((ReceiverAck*)receiverPacketBuffer)->houseCode;
    break;

  case RS_TEMP_SYNC:
    if (syncCounter < GOOD_SYNC_COUNT)  // we get at least one try
    {
      syncCounter++;
      if (syncCounter <= 0)
      {
        syncCounter = (SyncCounter)1;
        // only set hop offset if we were not previously in sync
        hopOffset = ((ReceiverAck*)receiverPacketBuffer)->houseCode;
      }
    }
    break;

  case RS_BAD_SYNC:
    if (syncCounter > resyncFailCount())
    {
      --syncCounter;
    }
    if (syncCounter <= 0)
    {
      hopOffset = 1; // Hop if transmitter fails to communicate
    }
    break;

  case RS_FORCE_NO_SYNC:
    syncCounter = INITIAL_SYNC_COUNT;
    hopOffset = 0;
    break;
  }
}

/*******************************************************************************

DESCRIPTION:  Depending upon whether or not we are in sync and how long we have
not been in sync, we determine whether to hop, stay, or timeout.

NOTES:

*******************************************************************************/
static void processHop(void)
{
  if ((syncCounter > BUMP_SYNC_COUNT) ||
      ((syncCounter & (RESYNC_TRIES - 1)) == 0) && (syncCounter < 0))
  {
#ifdef USING_CHIPCON_RF_CHIP
    setWirelessMode(WM_IDLE);
#endif
    followHop();
  }
  // if we are just to go out of sync, bump a little bit in case we got off slot somehow
  else if (syncCounter == BUMP_SYNC_COUNT)
  {
    hopOffset = (unsigned short)(hopOffset * 4) % NUM_HOP_CHANNELS;
#ifdef USING_CHIPCON_RF_CHIP
    setWirelessMode(WM_IDLE);
#endif
    followHop();
  }
#ifdef DMP_1145_TRANSMITTER
  // Key fobs need to reset learned information, so they can be picked up by
  // another repeater, if in the process of failing
  else if (syncCounter == INITIAL_SYNC_COUNT)
  {
    resetLearnedInformation();
  }
#endif
  else if (syncCounter <= resyncFailCount())
  {
    mainSleepState = MSS_FAILURE;
  }
  // otherwise, stay on channel
}

/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
static void setFrameTimeout(FrameStatus status)
{
  if (status == FS_JUST_WOKEUP)
  {
    // timeout for first sync.  Longer than a standard slot for clock slop
    frameTimeout = DEFAULT_WAKEUP_FRAME_TIMEOUT;
  }
  else if (status == FS_WAITING_FOR_ACK_PATTERN)
  {
    // time before we can expect to see sync + a little extra
    frameTimeout = (FrameTimeout)(SlotSYNC - RFFrameBitCounter + 10);
  }
  else  // standard timeout, usuall as a saftety net
  {
    frameTimeout = STANDARD_FRAME_TIMEOUT;
  }
}

/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
static void setupFrameStatus(FrameStatus status)
{
  // decrement last resort timeout every retry and every transmit
  if ((status == FS_TRANSMITTING) || (status == FS_WAITING_FOR_FIRST_PATTERN))
  {
    if (--lastResortTimer == 0)
    {
      mainSleepState = MSS_FAILURE;
    }
  }
  setFrameTimeout(status);
  frameStatus = status;
}

/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
static void processFrameTimeout(void)
{
  if (frameTimeout == 0)
  {
    if ((frameStatus == FS_WAITING_FOR_FIRST_PATTERN) || (frameStatus == FS_JUST_WOKEUP))
    {
#ifdef DMP_BUILDING
      if (diag.numSyncs < DIAG_MAX_SYNCS) diag.numSyncs++;
#endif
      processHop();
      setReceiverSync(RS_BAD_SYNC);  // we are not in sync
    }
    setupFrameStatus(FS_WAITING_FOR_FIRST_PATTERN);
  }
}

/*******************************************************************************

DESCRIPTION: Toggles between standard and low-interference frequency modes

NOTES: Reassigns the current frequency mode

*******************************************************************************/
static void SwitchFrequencyModes(void)
{
  if (currentFrequencyMode == STANDARD_FREQUENCY_MODE)
    currentFrequencyMode = LOW_INTERFERENCE_FREQUENCY_MODE;
  else
    currentFrequencyMode = STANDARD_FREQUENCY_MODE;

  SetFrequencyMode(currentFrequencyMode);
}


/*******************************************************************************
PUBLIC FUNCTION DEFINITIONS
*******************************************************************************/
/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
void decrementFrameTimeout(void)
{
  if (frameTimeout > 0)
  {
    frameTimeout--;
  }
}

/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
void processMainShutdown(void)
{
  setWirelessMode(WM_RECEIVE);
  setWirelessMode(WM_OFF);
}

/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
void processMainWakeup(bool hardSleep)
{
  resetState();
  RFFrameBitCounter = 0;
  setupFrameStatus(FS_JUST_WOKEUP);
#ifdef USING_CHIPCON_RF_CHIP
  setWirelessMode(WM_IDLE);
#endif
  if (hardSleep)  // we just wokeup from a "hard sleep", so reset fail timers
  {
    if (syncCounter < INITIAL_SYNC_COUNT) // if "way" out of sync, make only slightly out of sync
    {
      syncCounter = INITIAL_SYNC_COUNT;
    }
    lastResortTimer = LAST_RESORT_TIMEOUT;
  }
  #ifdef DMP_BUILDING
  if (diag.numSyncs < DIAG_MAX_SYNCS) diag.numSyncs++;
  #endif
  SetFrequency(currentChannel);
  initializeCommunicationStatus();
  setWirelessMode(WM_RECEIVE);
}

/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
void resetHardware(bool hardReset)
{
  initSys(hardReset);
  initDebugInterface();
  _EINT();
  processMainWakeup(true);
}

/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
__task void main(void)
{
  static char characterOffset;
  resetHardware(true);
#ifdef FCC_SPECIAL_VERSION
  fccTest();
#endif
#ifdef DMP_1119_TRANSMITTER
  InitializeTransmitterInformation();
  SetFrequencyMode(currentFrequencyMode);
#endif

  while (true)
  {
    processFrameTimeout();

    switch (mainSleepState)
    {
    case MSS_SUCCESS:
      // Keep track of the last frequency mode communication was established
      primaryFrequencyMode = currentFrequencyMode;
      goToSleep(TS_SUCCESS);
      break;

    case MSS_FAILURE:
      #ifdef DMP_1142_TRANSMITTER
        if(ackStatus.panicAckWaiting)
          ackStatus.panicAckWaiting = false;
      #endif
#ifdef DMP_1119_TRANSMITTER
      // There is no need to switch frequency modes if the transmitter is able to communicate
      if (BothTransmitterNumbersInvalid())
      {
#endif
        // If primary frequency mode toggle modes and try again before going to sleep
        if (currentFrequencyMode == primaryFrequencyMode)
        {
          WDTCTL = WDTPW | WDTCNTCL | WDTSSEL; // Kick watchdog since starting over
          SwitchFrequencyModes();
          processMainWakeup(true);
        }
        else
        {
          SwitchFrequencyModes();
          goToSleep(TS_FAILURE);
        }
#ifdef DMP_1119_TRANSMITTER
      }
      else
      {
        goToSleep(TS_FAILURE);
      }
#endif
      break;

    case MSS_RETRY:
#ifdef DMP_1119_TRANSMITTER
      // Failing right away, instead of retrying, keeps the transmiter in sync when only the zone
      // or the output have a valid serial number
      if (BothTransmitterNumbersInvalid() || CurrentTransmitterNumberValid())
        goToSleep(TS_RETRY);
      else
        goToSleep(TS_FAILURE);
#else
      goToSleep(TS_RETRY);
#endif
      break;

    // do nothing if awake or any other state
    }
    mainSleepState = MSS_AWAKE;

    switch (frameStatus)
    {
    case FS_JUST_WOKEUP:
    case FS_WAITING_FOR_FIRST_PATTERN:
      if (communicationStatus.frameStartDetected)
      {
        setupFrameStatus(FS_WAITING_FOR_FIRST_ACK);
      }
      break;

    case FS_WAITING_FOR_FIRST_ACK:
      if (processReceivedAck(&characterOffset) != RAS_NOTHING_TO_DO)
      {
        setupFrameStatus(FS_WAITING_FOR_SLOT_DEFINITION);
      }
      break;


    case FS_WAITING_FOR_SLOT_DEFINITION:
      {
        TransmitterMessageStatus slotDefinitionStatus;
        slotDefinitionStatus = setupTransmitterMessage(characterOffset);

        switch (slotDefinitionStatus)
        {
        case TMS_MESSAGE_SETUP:
          setWirelessMode(WM_TRANSMIT);
          setupFrameStatus(FS_TRANSMITTING);
          #ifdef DMP_BUILDING
          if (diag.numTransmits < DIAG_MAX_TRANSMITS) diag.numTransmits++;
          #endif
          setReceiverSync(RS_GOOD_SYNC);
          break;

        case TMS_NO_MESSAGE_NEEDED:
          setReceiverSync(RS_GOOD_SYNC);
          mainSleepState = MSS_SUCCESS;
          break;

        case TMS_STILL_WAITING:  // do nothing
          break;

        case TMS_NO_MESSAGE_SETUP:
          setReceiverSync(RS_TEMP_SYNC);
#ifdef USING_CHIPCON_RF_CHIP
          setWirelessMode(WM_IDLE);
#endif
          followHop();
          resetState();
          break;

        default:
          resetState();
          break;
        }
      }
      break;

    case FS_TRANSMITTING:
      if (communicationStatus.transmitComplete)
      {
#ifdef USING_CHIPCON_RF_CHIP
        setWirelessMode(WM_IDLE);
#endif
        communicationStatus.transmitComplete = false;
        initStateForReceive();
        followHop();
        setupFrameStatus(FS_WAITING_FOR_ACK_PATTERN);
        communicationStatus.frameStartDetected = false;
        communicationStatus.timeToTestFrequency = false;
      }
      if (communicationStatus.timeToTestBattery)
      {
        communicationStatus.timeToTestBattery = false;
        testBattery();
      }
      break;

    case FS_WAITING_FOR_ACK_PATTERN:
      if (communicationStatus.frameStartDetected)
      {
        setupFrameStatus(FS_WAITING_FOR_ACK);
      }
#ifndef USING_CHIPCON_RF_CHIP
      if (communicationStatus.timeToTestFrequency)
      {
        communicationStatus.timeToTestFrequency = false;
        latchFrequencyError();
      }
#endif
      break;

    case FS_WAITING_FOR_ACK:
      {
        ReceivedAckStatus slotAckStatus;
        slotAckStatus = processReceivedAck(&characterOffset);

        switch (slotAckStatus)
        {
        case RAS_GOOD_MESSAGE_ACK_RECEIVED:
#ifdef DMP_BUILDING
          diag.sequence++;
          diag.numTransmits = 1;
          diag.numSyncs = 0;
#endif
          applyFrequencyErrorOffset();
          mainSleepState = MSS_SUCCESS;
          break;

        case RAS_GOOD_MESSAGE_NON_ACK:
          mainSleepState = MSS_RETRY;
          break;

        case RAS_BAD_MESSAGE:
          setupFrameStatus(FS_WAITING_FOR_FIRST_ACK);  // try again immediately
          break;

        case RAS_NOTHING_TO_DO:
          break;

        // Heard from the correct receiver but there was no programming so shut down immediately
        case RAS_CORRECT_RECEIVER_NO_PROGRAMMING:
          mainSleepState = MSS_FAILURE;
          break;

        case RAS_GOOD_MESSAGE_WRONG_RECEIVER:
        default:
          setReceiverSync(RS_FORCE_NO_SYNC);
          resetState();
          break;
        }
      }
      break;
    }
  }   // loop forever
}




