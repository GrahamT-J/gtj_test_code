/*H*****************************************************************************
FILENAME:

DESCRIPTION:

NOTES:

Copyright (c) Digital Monitoring Products Inc. 2009.  All rights reserved.

*****************************************************************************H*/
#ifndef  SCANNED_INPUTS_H
#define  SCANNED_INPUTS_H

/*----compilation control-----------------------------------------------------*/
#include "packet.h"
#include "sleep.h"
/*******************************************************************************
INCLUDE FILES   Note: Minimize nesting of header files.
*******************************************************************************/
/*----system and platform files-----------------------------------------------*/

/*----program files-----------------------------------------------------------*/

/*******************************************************************************
FILE CONTEXT
*******************************************************************************/
/*----defines, typedefs, structs, unions, enums-------------------------------*/
// note, for proper sync, the debounce time must equal an integral number of hops
// with no fraction.
// For example, with 32 hops per second and a debounce of 500mS, there are exactly
// 16 hops during debounce.
enum
{
  DEBOUNCE_MS = 250,
  DEBOUNCE_TIME = ((long)DEBOUNCE_MS * SLEEP_TICKS_PER_SECOND) / 1000
};

/*----data descriptions-------------------------------------------------------*/

/*----data declarations-------------------------------------------------------*/

/*----macros------------------------------------------------------------------*/
#define setupInputs() // not used for scanned inputs

/*----function prototypes-----------------------------------------------------*/
extern void initInputs(bool hardReset);
extern void startInputDebounce(short multiplier);
extern void testBattery(void);
extern ExtendedZoneMessageByte getInputState(void);
#ifdef DMP_1100_PRODUCTION_TEST
extern void clearContactState(void);
#endif


#endif                                  /* end of file */
