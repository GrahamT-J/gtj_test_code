/*******************************************************************************
FILENAME:

DESCRIPTION:

NOTES:

Copyright (c) Digital Monitoring Products Inc. 2009.  All rights reserved.

*******************************************************************************/
/*----compilation control-----------------------------------------------------*/

/*******************************************************************************
INCLUDE FILES
*******************************************************************************/
/*----system and platform files (external libraries) -------------------------*/

/*----program files-----------------------------------------------------------*/
#include "sleep.h"
#include "TransmitterProtocol.h"
#include "WirelessCommunication.h"
#include "Inputs.h"



/*******************************************************************************
EXTERNAL REFERENCES   Only used if not available in a header file.
*******************************************************************************/
/*----data declarations-------------------------------------------------------*/
extern SlotNumber currentSlotNumber;

/*----function prototype------------------------------------------------------*/


/*******************************************************************************
PRIVATE DECLARATIONS
*******************************************************************************/
/*----defines, typedefs, structs, unions, enums-------------------------------*/
#define SOFTWARE_VERSION 1

typedef enum
{
  TMT_SERIAL_MESSAGE,
  TMT_ZONE_MESSAGE,
  TMT_CHECKIN_MESSAGE,
  TMT_NO_MESSAGE
} TransmitterMessageType;

/*----data declarations-------------------------------------------------------*/

AckStatus ackStatus = {.counter = 0};
/*----function prototype------------------------------------------------------*/
void formatTransmitterMessage(TransmitterMessageType messageType);

static SlotTransmitterNumber myZoneNumber = INVALID_TRANSMITTER_NUMBER;
static SlotNumber myCheckinSlotNumber = INVALID_SLOT;
static unsigned short transmitterCrc;

/*----macros------------------------------------------------------------------*/

/*******************************************************************************
PRIVATE FUNCTION DEFINITIONS
*******************************************************************************/
/*******************************************************************************

DESCRIPTION: Sets CRC for transmitter message, including any extra
information that is expected, and sets transmitter packet length

NOTES:

*******************************************************************************/
static void finalizeTransmitterMessage(char size, AdditionalCheckItems items)
{
  unsigned short crc = INITIAL_CRC;
  // first, start with any additional items
  if (items & ACI_HOUSE_CODE)
  {
    calcCRC16ForChar(houseCode, &crc);
  }
  if (items & ACI_ZONE_NUMBER)
  {
    calcCRC16ForChar(myZoneNumber >> 8, &crc);
    calcCRC16ForChar(myZoneNumber & 0xFF, &crc);
  }
  if (items & ACI_SERIAL_NUMBER)
  {
    calcCRC16ForChar(serialNumber & 0xFF, &crc);
    calcCRC16ForChar((serialNumber >> 8) & 0xFF, &crc);
    calcCRC16ForChar((serialNumber >> 16) & 0xFF, &crc);
  }
  encodeWirelessBuffer(transmitterPacketBuffer, size, crc);
  transmitterCrc = (transmitterPacketBuffer[size - 1] << 8) | transmitterPacketBuffer[size - 2];
  transmitterPacketSize = size;
}


/*******************************************************************************
PUBLIC FUNCTION DEFINITIONS
*******************************************************************************/
/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
ReceivedAckStatus processReceivedAck(char* characterOffset)
{
  char bufferOffset = getWirelessReceiveBufferOffset();
  ReceivedAckStatus returnValue = RAS_NOTHING_TO_DO;
  ReceiverAck * const ackPointer = (ReceiverAck*)receiverPacketBuffer;

  if (bufferOffset >= sizeof(ReceiverAck))
  {
    switch (ackPointer->ackType)
    {
    case AT_NO_ACK:
      returnValue = RAS_GOOD_MESSAGE_NON_ACK;
      *characterOffset = sizeof(ReceiverNoAck);
      break;

    case AT_STANDARD_ACK:
      *characterOffset = sizeof(ReceiverStandardAck);
      if (bufferOffset >= *characterOffset)
      {
        if (decodeWirelessBuffer(receiverPacketBuffer, sizeof(ReceiverStandardAck), transmitterCrc))
          returnValue = RAS_GOOD_MESSAGE_ACK_RECEIVED;
        else
          returnValue = RAS_BAD_MESSAGE;
      }
      break;

    case AT_ACK_WITH_ZONE_NUMBER:
      *characterOffset = sizeof(ReceiverZoneNumberAck);
      if (bufferOffset >= *characterOffset)
      {
        ReceiverZoneNumberAck * const zoneAckPointer = (ReceiverZoneNumberAck*)receiverPacketBuffer;

        if (decodeWirelessBuffer(receiverPacketBuffer, sizeof(ReceiverZoneNumberAck), transmitterCrc) &&
            ((houseCode == INVALID_HOUSE_CODE) || (myZoneNumber == INVALID_TRANSMITTER_NUMBER)))
        {
          if (zoneAckPointer->found)
          {
            if (houseCode == INVALID_HOUSE_CODE)
              houseCode = zoneAckPointer->houseCode;
            myZoneNumber = (SlotTransmitterNumber)(zoneAckPointer->zoneLow + (zoneAckPointer->zoneHigh << 8));
            myCheckinSlotNumber = zoneToSlotNumber(myZoneNumber);
            returnValue = RAS_GOOD_MESSAGE_ACK_RECEIVED;
          }
          else
          {
            returnValue = RAS_GOOD_MESSAGE_WRONG_RECEIVER;
          }
        }
        else
        {
          returnValue = RAS_BAD_MESSAGE;
        }
      }
      break;

    case AT_VARIABLE_LENGTH_ACK:
      if (bufferOffset >= sizeof(ReceiverAck) + 1)
      {
        ReceiverVariableLengthAck * const reservedAckPointer = (ReceiverVariableLengthAck*)receiverPacketBuffer;
        *characterOffset = sizeof(ReceiverVariableLengthAck) - 1 + reservedAckPointer->numBytesInData;
        if (bufferOffset >= *characterOffset)
        {
          returnValue = RAS_BAD_MESSAGE;  // we don't know what the ack is, so it's bad
        }
      }
      break;

    default:
      returnValue = RAS_BAD_MESSAGE;
      break;
    }
  }
  if( returnValue == RAS_GOOD_MESSAGE_ACK_RECEIVED )
  {         //we have received a good ack so count it
    if( ackStatus.counter <= LED_ACK_COUNT )
      ackStatus.counter++;
  }

  return returnValue;
}

/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
TransmitterMessageStatus setupTransmitterMessage(char startOffset)
{
  char bufferOffset = getWirelessReceiveBufferOffset();
  ReceiverSlotStart* slotPointer;
  TransmitterMessageStatus status = TMS_NO_MESSAGE_SETUP;
  bool completeMessageReceived = false;
  bool alarmWakeup = getAlarmWakeup();
  SlotNumber slotNumber = INVALID_SLOT;
  TransmitterMessageType  messageTypeToSend = TMT_NO_MESSAGE;

  transmitterPacketSize = 0;

  if (bufferOffset >= sizeof(ReceiverNoMessageSlot) + startOffset)
  {
    slotPointer = ((ReceiverSlotStart*)(receiverPacketBuffer + startOffset));
    slotNumber = (SlotNumber)((slotPointer->slotNumberHigh << 8) | slotPointer->slotNumberLow);

    // if waking up in my checkin slot and the slot has a slottype that doesn't need a response
    // and we arn't waking for an alarm then go back to sleep
    if ((isAlarmSlotType(slotPointer->slotType) && (slotPointer->slotType != ST_GENERAL_ALARM)) &&
        (! alarmWakeup) &&
        ((slotNumber >= myCheckinSlotNumber) &&
         (slotNumber <= myCheckinSlotNumber + SLOTS_BETWEEN_CHECKIN_SLOTS)))
    {
      if (bufferOffset >= sizeof(ReceiverNoMessageSlot) + startOffset)
      {
        completeMessageReceived = true;
        if (decodeWirelessBuffer(receiverPacketBuffer, sizeof(ReceiverNoMessageSlot) + startOffset, INITIAL_CRC))
        {
          status = TMS_NO_MESSAGE_NEEDED;
        }
      }
    }
    else if (isAlarmSlotType(slotPointer->slotType) )//if slottype is <=15
    {
      char size = sizeof(ReceiverNoMessageSlot) + startOffset;
      if (isTwoByteAlarmSlotType(slotPointer->slotType))
      {
        size += 2;
      }
      if (bufferOffset >= size)
      {
        completeMessageReceived = true;

        // setup zone number or serial number alarm message
        if (decodeWirelessBuffer(receiverPacketBuffer, size, INITIAL_CRC))
        {
          // if not assigned a zone number, send serial number, otherwise send zone number
          if (myZoneNumber == INVALID_TRANSMITTER_NUMBER)
          {
            messageTypeToSend = TMT_SERIAL_MESSAGE;
          }
          else
          {
            messageTypeToSend = TMT_ZONE_MESSAGE;
          }

          status = TMS_MESSAGE_SETUP;
        }
      }
    }
    else if (slotPointer->slotType == ST_CHECKIN_NEED_REPLY) //if slottype is 16
    {
      if (bufferOffset >= sizeof(ReceiverNoMessageSlot) + startOffset)
      {
        completeMessageReceived = true;
        if (decodeWirelessBuffer(receiverPacketBuffer, sizeof(ReceiverNoMessageSlot) + startOffset, INITIAL_CRC))
        {
          // make sure we are in the right slot
          if ((slotNumber >= myCheckinSlotNumber) &&
              (slotNumber <= myCheckinSlotNumber + SLOTS_BETWEEN_CHECKIN_SLOTS))
          {
            messageTypeToSend = TMT_CHECKIN_MESSAGE;
            status = TMS_MESSAGE_SETUP;
          }
        }
      }
    }
          //if we are in our checkin slot and the slot type is called for us to update our programming
    else if( slotPointer->slotType == ST_CHECKIN_UPDATE_PROGRAMMING && (slotNumber >= myCheckinSlotNumber) &&
         (slotNumber <= myCheckinSlotNumber + SLOTS_BETWEEN_CHECKIN_SLOTS) )
    {
      messageTypeToSend = TMT_SERIAL_MESSAGE;
      myZoneNumber = INVALID_TRANSMITTER_NUMBER;
    }
    else
    {
      // we don't know what this message is, so say we received it, status alone
      completeMessageReceived = true;
    }

    formatTransmitterMessage(messageTypeToSend);
  }

  if (completeMessageReceived == false)
  {
    status = TMS_STILL_WAITING;
  }
  else
  {
    currentSlotNumber = slotNumber;
  }
  return status;
}

/*******************************************************************************

DESCRIPTION:

NOTES:  If SLEEP_TICKS_PER_HOP is no longer a power of 2, this will take up much
more room

*******************************************************************************/
short getSleepTicks(void)
{
  SlotNumber slotDifference = (SlotNumber)(myCheckinSlotNumber - currentSlotNumber);
  if (slotDifference <= 0)
  {
    slotDifference += TOTAL_SLOTS;
  }
  return slotDifference * SLEEP_TICKS_PER_HOP;
}

/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
void resetLearnedInformation(void)
{
  myZoneNumber = INVALID_TRANSMITTER_NUMBER;
  houseCode = INVALID_HOUSE_CODE;
  currentSlotNumber = INVALID_SLOT;
  myCheckinSlotNumber = INVALID_SLOT;
}

#ifdef USE_SCANNED_INPUTS
/*******************************************************************************

DESCRIPTION: Function will format the transmitterPacketBuffer according to the passed message type

NOTES:

*******************************************************************************/
void formatTransmitterMessage(TransmitterMessageType messageType)
{
  switch (messageType)
  {
    case (TMT_SERIAL_MESSAGE):
    {
      TransmitterAlarmSerialNumExtendedMsg* const sendPointer =
        (TransmitterAlarmSerialNumExtendedMsg*)transmitterPacketBuffer;

      sendPointer->messageType = TAT_ALARM_WITH_EXTENDED_SERIAL_NUMBER;
      sendPointer->reserved = 0;
      sendPointer->serialNumberLow = serialNumber & 0xFF;
      sendPointer->serialNumberMiddle = (serialNumber >> 8) & 0xFF;
      sendPointer->serialNumberHigh = (serialNumber >> 16) & 0xFF;
      sendPointer->softwareVersion = SOFTWARE_VERSION;
      sendPointer->zoneMessage = getInputState();
      finalizeTransmitterMessage(sizeof(TransmitterAlarmSerialNumExtendedMsg), ACI_NOTHING);
      break;
    }

    case (TMT_ZONE_MESSAGE):    // Fall through
    case (TMT_CHECKIN_MESSAGE): // Checkin messages are sent as zone messages
    {
      TransmitterAlarmExtendedZoneMessage* const sendPointer =
        (TransmitterAlarmExtendedZoneMessage*)transmitterPacketBuffer;

      sendPointer->messageType = TAT_ALARM_WITH_EXTENDED_ZONE_NUMBER;
      sendPointer->zoneNumberHigh = (myZoneNumber >> 8) & 0xFF;
      sendPointer->zoneNumberLow = myZoneNumber & 0xFF;
      sendPointer->zoneMessage = getInputState();
      finalizeTransmitterMessage(sizeof(TransmitterAlarmExtendedZoneMessage),
          ACI_HOUSE_CODE | ACI_SERIAL_NUMBER);
      break;
    }

    default:
    {
      break;
    }
  }
}

#else

/*******************************************************************************

DESCRIPTION: Function will format the transmitterPacketBuffer according to the passed message type

NOTES:

*******************************************************************************/
void formatTransmitterMessage(TransmitterMessageType messageType)
{
  switch (messageType)
  {
    case (TMT_SERIAL_MESSAGE):
    {
      TransmitterAlarmSerialNumberZoneMessage* const sendPointer =
        (TransmitterAlarmSerialNumberZoneMessage*)transmitterPacketBuffer;

      sendPointer->messageType = TAT_ALARM_WITH_SERIAL_NUMBER;
      sendPointer->reserved = 0;
      sendPointer->serialNumberLow = serialNumber & 0xFF;
      sendPointer->serialNumberMiddle = (serialNumber >> 8) & 0xFF;
      sendPointer->serialNumberHigh = (serialNumber >> 16) & 0xFF;
      sendPointer->softwareVersion = SOFTWARE_VERSION;
      sendPointer->zoneMessage = (ZoneMessageByte)getInputState();
      finalizeTransmitterMessage(sizeof(TransmitterAlarmSerialNumberZoneMessage), ACI_NOTHING);
      break;
    }
    case (TMT_ZONE_MESSAGE):
    {
      TransmitterAlarmZoneNumberZoneMessage* const sendPointer =
        (TransmitterAlarmZoneNumberZoneMessage*)transmitterPacketBuffer;

      sendPointer->messageType = TAT_ALARM_WITH_ZONE_NUMBER;
      sendPointer->zoneNumberHigh = (myZoneNumber >> 8) & 0xFF;
      sendPointer->zoneNumberLow = myZoneNumber & 0xFF;
      sendPointer->zoneMessage = (ZoneMessageByte)getInputState();
      finalizeTransmitterMessage(sizeof(TransmitterAlarmZoneNumberZoneMessage),
          ACI_HOUSE_CODE | ACI_SERIAL_NUMBER);
      break;
    }
    case (TMT_CHECKIN_MESSAGE):
    {
      TransmitterCheckinMessage* const sendPointer =
          (TransmitterCheckinMessage*)transmitterPacketBuffer;

      sendPointer->zoneMessage = (ZoneMessageByte)getInputState();
      finalizeTransmitterMessage(sizeof(TransmitterCheckinMessage),
          ACI_HOUSE_CODE | ACI_ZONE_NUMBER | ACI_SERIAL_NUMBER);
      break;
    }
    default:
    {
      break;
    }
  }
}
#endif

