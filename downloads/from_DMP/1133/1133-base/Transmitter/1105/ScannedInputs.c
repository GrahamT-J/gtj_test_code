/*H*****************************************************************************
FILENAME:

DESCRIPTION:

NOTES:

Copyright (c) Digital Monitoring Products Inc. 2009.  All rights reserved.

*****************************************************************************H*/
/*----compilation control-----------------------------------------------------*/

/*******************************************************************************
INCLUDE FILES
*******************************************************************************/
/*----system and platform files (external libraries) -------------------------*/
#include "types.h"

/*----program files-----------------------------------------------------------*/
#include "Sleep.h"
#include "transmitterprotocol.h"
#include "IOPortPins.h"
#include "Inputs.h"



/*******************************************************************************
EXTERNAL REFERENCES   Only used if not available in a header file.
*******************************************************************************/
/*----data declarations-------------------------------------------------------*/

extern ZoneFlags myZoneFlags;
/*----function prototype------------------------------------------------------*/

/*******************************************************************************
PRIVATE DECLARATIONS
*******************************************************************************/
/*----defines, typedefs, structs, unions, enums-------------------------------*/
// it takes this many transmits at low battery before low battery is shown
enum {LOW_BATTERY_COUNTER_THRESHHOLD = 10};

typedef struct
{
  BITFIELD lowBattery : 1;
  BITFIELD counter    : 7;
} LowBatteryState;

/*----data declarations-------------------------------------------------------*/
LowBatteryState lowBatteryState;
static ExtendedZoneMessageByte steadyZoneState;
static ExtendedZoneMessageByte intermZoneState;

/*----function prototype------------------------------------------------------*/

/*----macros------------------------------------------------------------------*/

/*******************************************************************************
PRIVATE FUNCTION DEFINITIONS
*******************************************************************************/
/*******************************************************************************

DESCRIPTION: Will process the newly scanned zone state and look for any transitions.
              If a transition is found the funtion will return true if new zone state
NOTES:

*******************************************************************************/
static bool ProcessNewZoneState(ExtendedZoneMessageByte newZoneState)
{
  // if the new state is different from the state from the last scan.
  if ((newZoneState.zoneStateAsByte != intermZoneState.zoneStateAsByte)
      || (newZoneState.deviceState != intermZoneState.deviceState))
  {
    intermZoneState = newZoneState; // save the state if it is different
  }
  // if this scan is the same as the last scan see if it is different from the steady state
  else if ((steadyZoneState.zoneStateAsByte != intermZoneState.zoneStateAsByte)
           || (steadyZoneState.deviceState != intermZoneState.deviceState))
  {
    if (((steadyZoneState.deviceState & MB_TAMPER) > 0) || ((intermZoneState.deviceState & MB_TAMPER) > 0))
    {
      ackStatus.counter = 0;
    }
    steadyZoneState = intermZoneState; // save the new state as steady state
    return true; // return true if a new zone state
  }
  return false;
}

/*******************************************************************************

DESCRIPTION:  Function checks for a change on the tamper, reed switch, and external
              contact
NOTES:  Returns true if a zone state has changed

*******************************************************************************/
static bool ScanInputsForChange(void)
{
  static ExtendedZoneMessageByte currentZoneState;

  // Setup to read contact
  CACTL1 = CAREF_1 | CAON; // Ref applied to the + terminal, Ref 0.25*Vcc, Enable Comparator
  CACTL2 = P2CA3;          // CA4 applied to the - terminal

  currentZoneState.deviceState = (ZoneMessageByte)0;
  currentZoneState.zoneStateAsByte = 0;

  if (!getIO(TAMPER))
    currentZoneState.deviceState = MB_TAMPER;

  // Zone one is the reed switch
  if (!getIO(REED_SW))
    currentZoneState.zoneOne = ZONE_STATE_SHORT;

  // Zone two is the end-of-line resistor zone which measures open/normal/short
  if (CACTL2 & CAOUT)
  {
    currentZoneState.zoneTwo = ZONE_STATE_OPEN;
  }
  else
  {
    CACTL1 = CAREF_2 | CAON; // Ref applied to the + terminal, Ref 0.50*Vcc, Enable Comparator
    currentZoneState.zoneTwo = ZONE_STATE_SHORT;
    if (CACTL2 & CAOUT)
      currentZoneState.zoneTwo = ZONE_STATE_NORMAL;
  }

  CACTL1 &= ~CAON;  // turn off comparator

  return ProcessNewZoneState(currentZoneState);
}

/*******************************************************************************

DESCRIPTION:

NOTES: This assumes that any edge interrupts on this port are for alarm contacts

*******************************************************************************/
extern void rfDataClockEdge(void);
#ifdef __ICC430__
#pragma vector = PORT1_VECTOR
#pragma type_attribute = __interrupt
#endif
void port1EdgeInterrupt(void)
#ifndef __ICC430__
__interrupt[PORT1_VECTOR]
#endif
{
  if (getIOIntFlag(RF_DATA_CLK))
  {
    clearIOIntFlag(RF_DATA_CLK);
    rfDataClockEdge();
  }
  else
  {
    P1IFG = 0;        // clear all edge interrupts
    P1IE &= RF_DATA_CLK_MASK; // disable all other interrupts
  }
}

/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
#ifdef __ICC430__
#pragma vector = TIMERA0_VECTOR
#pragma type_attribute = __interrupt
#endif
void debounceInterrupt(void)
#ifndef __ICC430__
__interrupt[TIMERA0_VECTOR]
#endif
{
  TACCTL0 = 0;
  if (!startupDelayComplete)
  {
    __low_power_mode_off_on_exit();
  }
  else if (!getCurrentlyCommunicating()) // only process a change if not already communicating
  {
    if (ScanInputsForChange())
    {
      if (wakeup(true, false))
      {
        __low_power_mode_off_on_exit();
      }
    }
    else
    {
      startInputDebounce(DEBOUNCE_TIME);
    }
  }
}

/*******************************************************************************
PUBLIC FUNCTION DEFINITIONS
*******************************************************************************/
/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
void initInputs(bool hardReset)
{
  setIODirIn(REED_SW);
  setIODirIn(TAMPER);
  setIODirIn(BATTERY_VOLTAGE);

  // setup the voltage reference enable pin for low battery detection
  setIODirOut(BATT_VOLTAGE_ENABLE);
  clearIOSel(BATT_VOLTAGE_ENABLE);
  clearIO(BATT_VOLTAGE_ENABLE);

  CAPD = BATTERY_VOLTAGE_MASK | CONTACT_SW_MASK; // Input buffers P2.2 and P2.3 disabled

  if (hardReset)
    StartupDelay();

  startInputDebounce(DEBOUNCE_TIME);
}

/*******************************************************************************

DESCRIPTION:  Starts debounce timer so it will interrupt at debounce timer plus
  the amount of time for it to start at an appropriate hop

NOTES:  If SLEEP_TICKS_PER_HOP ever becomes a value that is not a power of two, the
  modulo operator will take much longer and this will probably need to change

*******************************************************************************/
void startInputDebounce(short debounceTime)
{
  if (! (TACCTL0 & CCIE)) // if not already debouncing
  {
    unsigned short tempTar = TAR;
    TACCR0 = tempTar + debounceTime + // current time plus debounce time plus
             SLEEP_TICKS_PER_HOP -      // an exra hop minus
             WAKEUP_CLOCK_SLOP_TICKS -  // some padding for clock slop minus
             ((tempTar - sleepTime) % SLEEP_TICKS_PER_HOP); // how far off we currently are from start of hop
    TACCTL0 = CCIE;  // enable interrupt and set compare mode
  }
}

/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
void testBattery(void)
{
  if (CACTL2 & CAOUT)
  {
    if (lowBatteryState.counter < LOW_BATTERY_COUNTER_THRESHHOLD)
    {
      lowBatteryState.counter++;
    }
    else
    {
      lowBatteryState.lowBattery = true;
    }
  }
  else      // a single "good" resets everything
  {
    lowBatteryState.counter = 0;
    lowBatteryState.lowBattery = false;
  }
}

/*******************************************************************************

DESCRIPTION: Function will return the current steady state for the zone

NOTES:

*******************************************************************************/
ExtendedZoneMessageByte getInputState(void)
{
  ExtendedZoneMessageByte state;

  state.deviceState = steadyZoneState.deviceState;
  state.zoneStateAsByte = steadyZoneState.zoneStateAsByte;
  if (lowBatteryState.lowBattery)
    state.deviceState |= MB_LOW_BATTERY;
  if (!getAlarmWakeup())
    state.deviceState |= MB_CHECKIN;

  return state;
}



