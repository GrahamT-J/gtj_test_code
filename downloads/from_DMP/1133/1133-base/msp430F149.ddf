;;/**************************************************
;; * Definitions of I/O register description file,
;; * interrupt/exception vectors, interrupt control registers,
;; * I/O register reset values. 
;; *
;; * Copyright 2001-2003 IAR Systems. All rights reserved.
;; *
;; * $Revision: 1.12 $
;; *
;; **************************************************/


;; Memory information ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; 
;; Used to define address zones within the MSP430 address space (Memory). 
;;
;;   Name      may be almost anything
;;   AdrSpace  must be Memory
;;   StartAdr  start of memory block
;;   EndAdr    end of memory block
;;   AccType   type of access, read-only (R) or read-write (RW)

[Memory]
;;         Name             AdrSpace    StartAdr    EndAdr    AccType
Memory0  = SFR              Memory      0x0000      0x01FF      RW
Memory1  = RAM              Memory      0x0200      0x09FF      RW
Memory2  = INFO             Memory      0x1000      0x10FF      RW
Memory3  = FLASH            Memory      0x1100      0xFFFF      R


;; I/O Register description file ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

[SfrInclude]
File = MSP430F149.sfr

;; Interrupt definitions ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
[InterruptList]
;               Id                 Table Adr    Prio    Enable                  Pending
Interrupt0    = PORT2_VECTOR       0x02         2       P2IE.P0                 P2IFG.P0
Interrupt1    = PORT2_VECTOR       0x02         2       P2IE.P1                 P2IFG.P1
Interrupt2    = PORT2_VECTOR       0x02         2       P2IE.P2                 P2IFG.P2
Interrupt3    = PORT2_VECTOR       0x02         2       P2IE.P3                 P2IFG.P3
Interrupt4    = PORT2_VECTOR       0x02         2       P2IE.P4                 P2IFG.P4
Interrupt5    = PORT2_VECTOR       0x02         2       P2IE.P5                 P2IFG.P5
Interrupt6    = PORT2_VECTOR       0x02         2       P2IE.P6                 P2IFG.P6
Interrupt7    = PORT2_VECTOR       0x02         2       P2IE.P7                 P2IFG.P7
Interrupt8    = USART1TX_VECTOR    0x04         2       IE2.UTXIE1              IFG2.UTXIFG1
Interrupt9    = USART1RX_VECTOR    0x06         2       IE2.URXIE1              IFG2.URXIFG1
Interrupt10   = PORT1_VECTOR       0x08         2       P1IE.P0                 P1IFG.P0
Interrupt11   = PORT1_VECTOR       0x08         2       P1IE.P1                 P1IFG.P1
Interrupt12   = PORT1_VECTOR       0x08         2       P1IE.P2                 P1IFG.P2
Interrupt13   = PORT1_VECTOR       0x08         2       P1IE.P3                 P1IFG.P3
Interrupt14   = PORT1_VECTOR       0x08         2       P1IE.P4                 P1IFG.P4
Interrupt15   = PORT1_VECTOR       0x08         2       P1IE.P5                 P1IFG.P5
Interrupt16   = PORT1_VECTOR       0x08         2       P1IE.P6                 P1IFG.P6
Interrupt17   = PORT1_VECTOR       0x08         2       P1IE.P7                 P1IFG.P7
Interrupt18   = TIMERA1_VECTOR     0x0A         2       TACCTL1.CCIE            TACCTL1.CCIFG
Interrupt19   = TIMERA1_VECTOR     0x0A         2       TACCTL2.CCIE            TACCTL2.CCIFG
Interrupt20   = TIMERA1_VECTOR     0x0A         2       TACTL.TAIE              TACTL.TAIFG
Interrupt21   = TIMERA0_VECTOR     0x0C         2       TACCTL0.CCIE            TACCTL0.CCIFG
Interrupt22   = ADC12_VECTOR       0x0E         2       ADC12IE                 ADC12IFG
Interrupt23   = USART0TX_VECTOR    0x10         2       IE1.UTXIE0              IFG1.UTXIFG0
Interrupt24   = USART0RX_VECTOR    0x12         2       IE1.URXIE0              IFG1.URXIFG0
Interrupt25   = WDT_VECTOR         0x14         2       IE1.WDTIE               IFG1.WDTIFG
Interrupt26   = COMPARATORA_VECTOR 0x16         2       CACTL1.CAIE             CACTL1.CAIFG
Interrupt27   = TIMERB1_VECTOR     0x18         2       TBCCTL1.CCIE            TBCCTL1.CCIFG
Interrupt28   = TIMERB1_VECTOR     0x18         2       TBCCTL2.CCIE            TBCCTL2.CCIFG
Interrupt29   = TIMERB1_VECTOR     0x18         2       TBCCTL3.CCIE            TBCCTL3.CCIFG
Interrupt30   = TIMERB1_VECTOR     0x18         2       TBCCTL4.CCIE            TBCCTL4.CCIFG
Interrupt31   = TIMERB1_VECTOR     0x18         2       TBCCTL5.CCIE            TBCCTL5.CCIFG
Interrupt32   = TIMERB1_VECTOR     0x18         2       TBCCTL6.CCIE            TBCCTL6.CCIFG
Interrupt33   = TIMERB1_VECTOR     0x18         2       TBCTL.TBIE              TBCTL.TBIFG
Interrupt34   = TIMERB0_VECTOR     0x1A         2       TBCCTL0.CCIE            TBCCTL0.CCIFG
Interrupt35   = NMI_VECTOR         0x1C         2       IE1.NMIIE               IFG1.NMIIFG
Interrupt36   = RESET_VECTOR       0x1E         1
      
;; End of file
