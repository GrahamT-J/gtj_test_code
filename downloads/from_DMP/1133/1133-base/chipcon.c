/*H*****************************************************************************
FILENAME: chipcon.c

DESCRIPTION: Code file for an interface to a Chipcon CC1100 wireless chip

NOTES: Requires chipcon.h, hardware.h

Copyright (c) Digital Monitoring Products Inc. 2009.  All rights reserved.

*****************************************************************************H*/
/*----compilation control-----------------------------------------------------*/

/*******************************************************************************
INCLUDE FILES
*******************************************************************************/
/*----system and platform files (external libraries) -------------------------*/


/*----program files-----------------------------------------------------------*/
#include "types.h"
#include "hardware.h"
#include "chipcon.h"
#include "Packet.h"
#include "ioportpins.h"
#include "ChipconRegisters.h"

/*******************************************************************************
EXTERNAL REFERENCES   Only used if not available in a header file.
*******************************************************************************/
/*----data declarations-------------------------------------------------------*/

/*----function prototype------------------------------------------------------*/

/*******************************************************************************
PRIVATE DECLARATIONS  Defined in this module, used only in this module.
*******************************************************************************/
/*----defines, typedefs, structs, unions, enums-------------------------------*/

// Declare an instance of the structure containing all CC1100 registers to establish
//  the defaults for system initialization
const Reg_CC1100 chipconRegisterDefaults =
{
#ifdef SWAP_CHIPCON_GPIO
  .Dflt_IOCFG2.gdo2Mode = 0x0B, // Set GD0-2 in serial CLK mode for data communication
#ifdef XT50_RECEIVER
  .Dflt_IOCFG1.gdo1Mode = 0x0E, // Set GD0-1 to Carrier Sense
#else
  .Dflt_IOCFG1.gdo1Mode = 0x2F, // Set GDO-1 to 0
#endif
#else
#if defined PROJECT_1100X || defined PROJECT_1100D
  .Dflt_IOCFG2.gdo2Mode = 0x0E, // Set GD0-2 to Carrier Sense
#else
  .Dflt_IOCFG2.gdo2Mode = 0x2F, // Set GD0-2 to 0
#endif
  .Dflt_IOCFG1.gdo1Mode = 0x0B, // Set GDO-1 in serial CLK mode for data communication
#endif

  .Dflt_IOCFG2.gdo2Inverted = 0x00,  //0 = Not-Inverted 1 = Inverted
  .Dflt_IOCFG2.reserved = 0x00,

  .Dflt_IOCFG1.gdo1Inverted = 0,  //0 = Not-Inverted 1 = Inverted
  .Dflt_IOCFG1.gdoDriveStrength = 0,  //0 = Low Drive Strength  1 = High Drive Strength

  .Dflt_IOCFG0.gdo0Mode = 0x0C,  // set GDO0 in serial data out mode for data communication
  .Dflt_IOCFG0.gdo0Inverted = 1,   //0 = Not-Inverted 1 = Inverted
  .Dflt_IOCFG0.tempSensorEnable = 0,  //0 = disabled  1 = enabled

  .Dflt_FIFOTHR.fifoThreshold = 0x07,  //set the fifo value see table page 46 of data sheet
  .Dflt_FIFOTHR.reserved = 0,

  .Dflt_SYNC1.syncWordHighByte = 0,

  .Dflt_SYNC0.syncWordLowByte = 0,

  .Dflt_PKTLEN.packetLength = 255,  // set the packet length to maximum value to decreas the likelihood of packetization

  .Dflt_PKTCTRL1.addressCheckMode = 0x00, //0 = No Checking 1 = Check, No broadcast, 2 = Check, 0 is broadcast, 3 = Check, 0,255 is broadcast
  .Dflt_PKTCTRL1.appendStatus = 0, //O = disable  1 = enable
  .Dflt_PKTCTRL1.reserved  = 0,
  .Dflt_PKTCTRL1.wakeOnRadioAutoSync  = 0,  //O = disable  1 = enable
  .Dflt_PKTCTRL1.preambleQualityThreshold = 0x00, //disable the preamble quality threshold estimator

  .Dflt_PKTCTRL0.packetLength = 2,  // set packetLength mode to infinite
  .Dflt_PKTCTRL0.enableCRC = 0, //O = disable  1 = enable
  .Dflt_PKTCTRL0.enableCC2400Support = 0, //O = disable  1 = enable
  .Dflt_PKTCTRL0.packetFormat = 1,  //set packet format to Synchronous serial mode
  .Dflt_PKTCTRL0.dataWhitening = 0, //O = disable  1 = enable
  .Dflt_PKTCTRL0.reserved = 0,

  .Dflt_ADDR.deviceAddress = 0, //set device address to zero since address checking is off

  .Dflt_CHANNR.channelNumber = 0, //set channel number to 0 since not using chipcon channels

  .Dflt_FSCTRL1.ifFrequency = 6,  //set the IF frequency to 152.34375 kHz
  .Dflt_FSCTRL1.reserved = 0,

  .Dflt_FSCTRL0.frequencyOffset = 0,  //set the freq offset to 0 for init

  .Dflt_FREQ2.frequencyHighByte = 0x22, //set frequency to 0x22BE8B or 903.352 MHz
  .Dflt_FREQ1.frequencyMidByte = 0xBE,
  .Dflt_FREQ0.frequencyLowByte = 0x8B,
#ifdef DMP_PRODUCTION_TEST
  .Dflt_MDMCFG4.dataRateExponent = 7, // set the data rate to 4797.35 bps
#else
  .Dflt_MDMCFG4.dataRateExponent = 8, // set the data rate to 9595.87 bps
#endif
  .Dflt_MDMCFG3.dataRateMantissa= 131,

  .Dflt_MDMCFG4.channelBWMantissa = 1,  // set the channelBW to 81.250 kHz
  .Dflt_MDMCFG4.channelBWExponent = 3,

  .Dflt_MDMCFG2.syncMode = 0, // disable sync mode
  .Dflt_MDMCFG2.enableManchester = 0, //O = disable  1 = enable
  .Dflt_MDMCFG2.modulationFormat = 0, //set the modulation format to 2-FSK
  .Dflt_MDMCFG2.disableDCFilter  = 0, //1 = disabled  0 = enabled

  .Dflt_MDMCFG1.reserved = 0,
  .Dflt_MDMCFG1.numPreambleBytes = 2, //set the minimum number of preamble bytes to RX/TX to 4 bytes
  .Dflt_MDMCFG1.enableFEC = 0,  //O = disable  1 = enable

  .Dflt_MDMCFG1.channelSpacingExponent = 0, // set the channel spacing to 0 since not using Chipcon
  .Dflt_MDMCFG0.channelSpacingMantissa = 0, // channels

  .Dflt_DEVIATN.deviationMantissa = 5,  //set the modulation deviation to 20.629 kHz
  .Dflt_DEVIATN.deviationExponant = 3,
  .Dflt_DEVIATN.reserved = 0,
  .Dflt_DEVIATN.reserved1 = 0,

  .Dflt_MCSM2.rxTime = 7, // disable rxTime (set to no timeout)
  .Dflt_MCSM2.rxTimeQualifier = 0,  //O = disable  1 = enable
  .Dflt_MCSM2.rxTimeRSSI = 0, //O = disable  1 = enable
  .Dflt_MCSM2.reserved = 0,

  .Dflt_MCSM1.txOffMode = 0,  //set tx mode to transition to idle after packet TX
  .Dflt_MCSM1.rxOffMode = 0,  //set rx mode to transition to idle after packet RX
  .Dflt_MCSM1.clearChannelMode = 0, // set clear channel assesment mode to 0(always asses clear channel)
  .Dflt_MCSM1.reserved = 0,

  .Dflt_MCSM0.forceXOSCOn = 0,  //O = disable  1 = enable
  .Dflt_MCSM0.enablePinControl = 0, //O = disable  1 = enable
  .Dflt_MCSM0.powerOnTimeout = 2, // set power on timeout to 64counts or approx 146us to 171us
  .Dflt_MCSM0.freqSynthAutoCal = 1, //set to calibrate automatically when going from IDLE to RX/TX
  .Dflt_MCSM0.reserved  = 0,

  .Dflt_WOREVT1.event0TimeOutHighByte = 0x87,  // set the event0 timout to 1s

  .Dflt_WOREVT0.event0TimeOutLowByte = 0x6B,

  .Dflt_WORCTRL.worTimerResolution = 0, //set WOR event0 resolution to 1 period
  .Dflt_WORCTRL.reserved = 0,
  .Dflt_WORCTRL.enableRcOscCalibration = 1, //O = disable  1 = enable
  .Dflt_WORCTRL.worEvent1 = 7,  // set event1 timeout to the range of 1.286ms to 1.5ms
  .Dflt_WORCTRL.rcOscPowerDown  = 1,  //O = disable  1 = enable

  .Dflt_FREND0.paTableIndex = 0x00, // set the PATable index to 0 which references 10db entry
  .Dflt_FREND0.reserved = 0,

// ******************************************************************************************************
// This portion of the table generated from values given the Chipcons SmartRF studio software
  .Dflt_FOCCFG.frequencyOffsetConfig = 22,  //this register is given by the SmartRF� Studio software.
  .Dflt_FOCCFG.reserved = 0,

  .Dflt_BSCFG.bitSyncConfig = 108,  //this register is given by the SmartRF� Studio software.

  .Dflt_AGCCTRL2.AGCControl2 = 67,  //this register is given by the SmartRF� Studio software.

  .Dflt_AGCCTRL1.AGCControl1 = 64,  //this register is given by the SmartRF� Studio software.
  .Dflt_AGCCTRL1.reserved = 0,

  .Dflt_AGCCTRL0.AGCControl0 = 145, //this register is given by the SmartRF� Studio software.

  .Dflt_FREND0.txLoBufferCurrent = 0x01,  //this register is given by the SmartRF� Studio software.
  .Dflt_FREND0.reserved1 = 0,

  .Dflt_FSCAL3.FSCalibrationConfig3 = 169,  //this register is given by the SmartRF� Studio software.

  .Dflt_FSCAL2.fsCalibrationResults2 = 10,  //this register is given by the SmartRF� Studio software.
  .Dflt_FSCAL2.reserved = 0,

  .Dflt_FSCAL1.fsCalibrationResults1 = 0, //this register is given by the SmartRF� Studio software.
  .Dflt_FSCAL1.reserved = 0,

  .Dflt_FSCAL0.fsCalibrationResults0 = 17,  //this register is given by the SmartRF� Studio software.
  .Dflt_FSCAL0.reserved = 0,

  .Dflt_FREND1.frontEndRXConfig = 86 //this register is given by the SmartRF� Studio software.
// *********************************************************************************************************
};

const char DefaultPowerTable[] =
{  //PATABLE settings for 915MHz band
  0xC0, // Power_8 =  10 dBm
  0xC9, // Power_7 =  7  dBm
  0x83, // Power_6 =  5  dBm
  0x8E, // Power_5 =  0  dBm
  0x57, // Power_4 = -5  dBm
  0x26, // Power_3 = -10 dBm
  0x1C, // Power_2 = -15 dBm
  0x0D, // Power_1 = -20 dBm
  0x11  // Power_0 = -30 dBm
};

const char DefaultHPPowerTable[] =
{  //PATABLE settings for 915MHz band
  0x8E, // Power_8 =  0  dBm
  0xC9, // Power_7 =  7  dBm
  0x83, // Power_6 =  5  dBm
  0x8E, // Power_5 =  0  dBm
  0x57, // Power_4 = -5  dBm
  0x26, // Power_3 = -10 dBm
  0x1C, // Power_2 = -15 dBm
  0x0D, // Power_1 = -20 dBm
  0x11  // Power_0 = -30 dBm
};

typedef struct
{
  union
  {
    struct
    {
      BITFIELD  Address         :6;
      BITFIELD  BurstAccessBit  :1;
      BITFIELD  ReadWriteBit    :1;
    };
    struct
    {
      unsigned char headerByte;
    };
  };
}ChipconHeader;

typedef struct
{
  union
  {
    struct
    {
      BITFIELD  Fifo_bytes_avail  :4;
      BITFIELD  State             :3;
      BITFIELD  Chip_Rdy          :1;
    };
    unsigned char statusByte;
  };
}ChipconStatus;

// Chipcon channels are centered around 915 MHz
// The frequency synthesizer step is (XTAL/2^16) or 396.729 Hz
// The low channel is (StartingFrequency * 396.729 Hz)
// The high channel is ((StartingFrequency + (52 * ChannelSpacing)) * 396.729 Hz)
typedef enum
{
  STANDARD_CHANNEL_SPACING          = 1132,  // 449 kHz spacing, 903.326 MHz to 926.674 MHz span
  LOW_INTERFERENCE_CHANNEL_SPACING  = 912    // 362 kHz spacing, 905.593 MHz to 924.407 MHz span
} ChannelSpacing;

typedef enum
{
  STANDARD_START_FREQUENCY         = 2276937,
  LOW_INTERFERENCE_START_FREQUENCY = 2282651
} StartingFrequency;

#define PA_TABLE_SIZE 8 //CC1100 PATable is 8 bytes in length

#ifndef XT50_RECEIVER
#define FACTORY_FREQUENCY_OFFSET_ADDRESS 0x1004
#define factoryFrequencyOffsetPtr (const signed int*)(FACTORY_FREQUENCY_OFFSET_ADDRESS)
#define factoryFrequencyOffset *factoryFrequencyOffsetPtr

#define CHIPCON_RESET_DELAY (int)((MASTER_CLOCK / 25000) + 1)
#endif

#define WRITE false
#define READ  true

#define MAX_FREQ_BIAS 32
#define MIN_FREQ_ERROR_ADJUST 1 //Each step is equal to 1.586khz so this is the minimum offset adjustment.
#define MAX_RSSI_COUNTER 5

/*----data declarations-------------------------------------------------------*/
signed char FreqBias;
static signed char frequencyErrorByte;
#ifdef XT50_RECEIVER
static char currentRSSIData = 0;
#endif
static ChannelSpacing channelSpacing = STANDARD_CHANNEL_SPACING;
static StartingFrequency startingFrequency = STANDARD_START_FREQUENCY;

/*----function prototype------------------------------------------------------*/
char sendChipconHeader(ChipconHeader header);
char writeChipconData(unsigned char data);
char readChipconData(void);
char readChipconStatus();
void setChipconDefaults(void);
/*----macros------------------------------------------------------------------*/


/*******************************************************************************
PUBLIC FUNCTION DEFINITIONS  Note: Header is repeated for each function.
*******************************************************************************/
#ifdef XT50_RECEIVER
/*F*****************************************************************************

DESCRIPTION: Function will init the IO lines used to control the chipcon chip

NOTES:  Must be called before any other chipcon function

*****************************************************************************F*/
bool initChipcon(void)
{
  // Set the IO Direction
  setIODirOut(CSn);
  setIODirOut(SCLK);
  setIODirIn(SO_GDO1);
  setIODirOut(SI);

  //Set the IO pins to IO mode
  clearIOSel(CSn);
  clearIOSel(SCLK);
  clearIOSel(SO_GDO1);
  clearIOSel(SI);

  /********* Power on Reset sequence defined in the chipcon data sheet *************/
  setIO(SCLK);
  clearIO(SI);
  setIO(CSn);
  clearIO(CSn);
  setIO(CSn);

  ChipconResetDelay();
  clearIO(CSn);
  ChipconResetDelay();
  if (getIO(SO_GDO1))   // SO should drop low meaning the chip is ready
    return false;

  sendChipconCommandStrobe(SRES); // issue the reset command
  ChipconResetDelay();
  if (getIO(SO_GDO1))   // SO should drop low meaning the reset is complete
    return false;
  /*********************************************************************************/

  setChipconDefaults();
  initChipconPowerTable(false); // Init with default PA table
  return true;
}
#else


/*F*****************************************************************************

DESCRIPTION: Function will init the IO lines used to control the chipcon chip

NOTES:  Must be called before any other chipcon function
        Function uses TimerA so all TimerA setup will be changed

*****************************************************************************F*/
void initChipcon(void)
{

  TACTL = TASSEL_2 +    // use DCO clock
          MC_2;         // continuous
  CCTL0 = 0;   // no interrupt at first,   will start later

      //Set the IO Direction
  setIODirOut(CSn);
  setIODirOut(SCLK);
  setIODirIn(SO_GDO1);
  setIODirOut(SI);
     //Set the IO pins to IO mode
  clearIOSel(CSn);
  clearIOSel(SCLK);
  clearIOSel(SO_GDO1);
  clearIOSel(SI);
    //start a reset for the chipcon chip.
  /********* Power on Reset sequence defined in the chipcon data sheet *************/
  setIO(SCLK);
  clearIO(SI);
  setIO(CSn);
  clearIO(CSn);
  setIO(CSn);
  TACCR0 = TAR + CHIPCON_RESET_DELAY;  // setup a 40us delay on timerA CCR0
  while( !(TACCTL0 & CCIFG) ); //wait until timer counts down the delay
  clearIO(CSn);
  while(getIO(SO_GDO1));  //wait for SO to drop low meaning the chip is ready
  sendChipconCommandStrobe( SRES ); //issue the reset command
  while(getIO(SO_GDO1)); //wait for SO to drop low meaning the reset is complete
  /*********************************************************************************/
  TACCTL0 = 0; // clear the pending timer interrupt
  setChipconDefaults();
  initChipconPowerTable(false); // Init with default PA table
}
#endif

/*F*****************************************************************************

DESCRIPTION: Function will set all config registers to their default values

NOTES:

*****************************************************************************F*/
void setChipconDefaults(void)
{
  char i;
  ChipconHeader header;
  char * dfltTablePtr = (char*)&chipconRegisterDefaults;

  header.ReadWriteBit = WRITE;  // setup for a write
  header.BurstAccessBit = true; // with burst access
  header.Address = 0x00; //start at begining of Config register bank
  sendChipconHeader( header );

  for(i=0; i < sizeof(chipconRegisterDefaults); i++)
  {  //write all data from the chipconDefaults table
    writeChipconData(dfltTablePtr[i]);
  }
  setIO(CSn);
}

/*F*****************************************************************************

DESCRIPTION: Function will set all enteries of the PATable to their default values

NOTES:  Function will only write the first 8 enteries from the DefaultPowerTable

*****************************************************************************F*/
char initChipconPowerTable(bool highPowerReceiver)
{
  char i;
  ChipconHeader header;
  char chipStatus;
  char const *powerTable = DefaultPowerTable;

  if (highPowerReceiver)
    powerTable = DefaultHPPowerTable;

  // Setup the header for write with burst access
  header.ReadWriteBit = WRITE;
  header.BurstAccessBit = true;
  header.Address = 0x3E;
  chipStatus = sendChipconHeader( header );

  // Fill the CC1100 PATable with the top data from the DefaultPowerTable
  for(i=0; i < PA_TABLE_SIZE; i++)
  {
    writeChipconData(powerTable[i]);
  }
  setIO(CSn);
  return(chipStatus);
}

/*F*****************************************************************************

DESCRIPTION: Function will write a data value to a Configuration Register and will
              return the chips status byte

NOTES: Must use ConfigRegisters as argument for address

*****************************************************************************F*/
char writeChipconConfigRegister(ConfigRegisters address, char data)
{
  ChipconHeader header;
  char statusByte;
    //setup the header for write with no burst access
  header.ReadWriteBit = WRITE;
  header.BurstAccessBit = false;
  header.Address = address;
    //send the header and write the data
  sendChipconHeader( header );
  statusByte = writeChipconData( data );
  setIO(CSn);  //de-activate the chip

  return(statusByte);
}


/*F*****************************************************************************

DESCRIPTION: Function will return the chips status byte

NOTES:

*****************************************************************************F*/
char readChipconStatus()
{

  char statusByte;
  statusByte = sendChipconCommandStrobe(SNOP);  // send a NOP inorder to read the status byte
  return(statusByte);
}

/*F*****************************************************************************

DESCRIPTION: Function will read the data for a config register from the chipcon chip
               and return

NOTES:  Must use ConfigRegisters as argument for address

*****************************************************************************F*/
char readChipconConfigRegister(ConfigRegisters address)
{
  ChipconHeader header;
  char returnData;
    //setup the header for write with no burst access
  header.ReadWriteBit = READ;
  header.BurstAccessBit = false;
  header.Address = address;
    //send the header and read the data
  sendChipconHeader( header );
  returnData = readChipconData();
  setIO(CSn);  //de-activate the chip

  return(returnData);
}

/*F*****************************************************************************

DESCRIPTION:  Function will send a command strobe to the chipcon chip and will
                return the status byte

NOTES:

*****************************************************************************F*/
char sendChipconCommandStrobe(CommandStrobes address)
{
  ChipconHeader header;
  char statusByte;
    //setup the header for write with no burst access
  header.ReadWriteBit = WRITE;
  header.BurstAccessBit = false;
  header.Address = address;
    //send the header
  statusByte = sendChipconHeader( header );
  setIO(CSn);  //de-activate the chip

  return(statusByte);
}


/*F*****************************************************************************

DESCRIPTION:  Function will read the data in a status register from the chipcon chip
                and return.

NOTES:

*****************************************************************************F*/
char readChipconStatusRegister(StatusRegisters address)
{
  ChipconHeader header;
  char returnData;
    //setup the header for write with no burst access
  header.ReadWriteBit = READ;
  header.BurstAccessBit = true;
  header.Address = address;
    //send the header and write the data
  sendChipconHeader( header );
  returnData = readChipconData();
  setIO(CSn);  //de-activate the chip

  return(returnData);
}

/*******************************************************************************
PRIVATE FUNCTION DEFINITIONS  Note: Header is repeated for each function.
*******************************************************************************/

/*f*****************************************************************************

DESCRIPTION: Function will send the begining header to the chipcon chip and will return
                the chips status byte.

NOTES: function call should be followed by some kind of data read or write

*****************************************************************************f*/
char sendChipconHeader(ChipconHeader header)
{
  clearIO(SCLK);  //assure sclk is low
  clearIO(CSn);   //bring chip active
  while(getIO(SO_GDO1)); //wait until chip active

  return(writeChipconData(header.headerByte));
}


/*f*****************************************************************************

DESCRIPTION:  Function will write data to the chipcon chip bus and return the status byte

NOTES:  Must be called after the header has been sent

*****************************************************************************f*/
char writeChipconData(unsigned char data)
{
  char i;
  char statusByte = 0;

  for( i=0x80; i != 0 ; i>>=1)
  { //rotate through bits from MSB to LSB while clocking the bits out and the status bits in
    clearIO(SCLK);
    if(data & i)
      setIO(SI);
    else
      clearIO(SI);
    setIO(SCLK);
    if(getIO(SO_GDO1))
      statusByte |= i;
  }
  return(statusByte);
}


/*f*****************************************************************************

DESCRIPTION:  Function will read data from the chipcon chip data bus

NOTES:  Must be called after the header has been sent

*****************************************************************************f*/
char readChipconData(void)
{
  return(writeChipconData(0));
}

/*The following ranges are represented in the data of ConversionTable and
    are used in sendChannel for roundoff error correction*/
static const char ConversionTable[] =
{
  1,    // 1<=x<5	-1
  5,    // 5<=x<10	-2
  10,   // 10<=x<15	-3
  15,   // 15<=x<20	-4
  20,   // 20<=x<24	-5
  24,   // 24<=x<29	-6
  29,   // 29<=x<34	-7
  34,   // 34<=x<39	-8
  39,   // 39<=x<43	-9
  43,   // 43<=x<48	-10
  48,   // 48<=x<53	-11
  53,   // 53<=x<57 -12
  57
};

/*f*****************************************************************************

DESCRIPTION:  Function will convert from channel number to chipcon freq register
                value and setup the RF chip to use the new channel

NOTES: Conversion from channel to freq register value is described in the following equation
          freqRegisterValue = channelSpacing * channelNumber
*****************************************************************************f*/
void sendChannel(unsigned char channel)
{
  signed long workingVal;
  unsigned char i;
#ifdef SINGLE_CHANNEL
  channel = SINGLE_CHANNEL;
#endif

  workingVal = ((unsigned long)channel * channelSpacing);
  workingVal += startingFrequency;

  // Adjust for error if standard frequency mode
  if (channelSpacing == STANDARD_CHANNEL_SPACING)
  {
    for( i=1; i<sizeof(ConversionTable); i++)
    {
      if ((channel >= ConversionTable[i-1]) && (channel < ConversionTable[i]))
      {
        workingVal -= i;
        break;
      }
    }
  }

  workingVal += factoryFrequencyOffset;
  workingVal += FreqBias;

  writeChipconConfigRegister(FREQ0, (workingVal & 0xFF));
  writeChipconConfigRegister(FREQ1, (workingVal>>8 & 0xFF));
  writeChipconConfigRegister(FREQ2, (workingVal>>16 & 0xFF));
}

/*******************************************************************************

DESCRIPTION: Will read and latch the current frequency error estimate

NOTES:

*******************************************************************************/
void latchFrequencyError(void)
{
  frequencyErrorByte = readChipconStatusRegister(FREQEST);
}

/*******************************************************************************

DESCRIPTION: Will adjust the applied frequiency bias based upon the latch frequency
              error estimate
NOTES:

*******************************************************************************/
void applyFrequencyErrorOffset(void)
{
  if ((frequencyErrorByte < -MIN_FREQ_ERROR_ADJUST) && (FreqBias < MAX_FREQ_BIAS)) // frequency too high
  {
    --FreqBias;
  }
  else if ((frequencyErrorByte > MIN_FREQ_ERROR_ADJUST) && (FreqBias > -MAX_FREQ_BIAS)) // frequency too low
  {
    ++FreqBias;
  }
}

#ifdef XT50_RECEIVER
/*******************************************************************************

DESCRIPTION: Function will count the number of times Carrier Sense is detected

NOTES:

*******************************************************************************/
void readRSSI(void)
{
  if ((getIO(RF_CARRIER_SENSE) > 0) && (currentRSSIData < MAX_RSSI_COUNTER))
    currentRSSIData++;
}

/*******************************************************************************

DESCRIPTION: Returns the current value of currentRSSIData

NOTES:

*******************************************************************************/
char getCurrentRSSI(void)
{
  return currentRSSIData;
}


/*******************************************************************************

DESCRIPTION: Will clear the current RSSI value

NOTES:

*******************************************************************************/
void clearCurrentRSSI(void)
{
  currentRSSIData = 0;
}
#endif

/*******************************************************************************

DESCRIPTION: Sets the frequency mode to either standard or low-interference

NOTES: For chipcon both the starting frequency and channel spacing are changed

*******************************************************************************/
void SetFrequencyMode(FrequencyMode mode)
{
  if (mode == STANDARD_FREQUENCY_MODE)
  {
    startingFrequency = STANDARD_START_FREQUENCY;
    channelSpacing = STANDARD_CHANNEL_SPACING;
  }
  else
  {
    startingFrequency = LOW_INTERFERENCE_START_FREQUENCY;
    channelSpacing = LOW_INTERFERENCE_CHANNEL_SPACING;
  }
}
