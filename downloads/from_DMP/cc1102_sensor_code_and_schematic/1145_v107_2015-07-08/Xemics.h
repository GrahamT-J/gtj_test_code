/*H*****************************************************************************
FILENAME:

DESCRIPTION:

NOTES:

Copyright (c) Digital Monitoring Products Inc. 2004-2009.  All rights reserved.

*****************************************************************************H*/
#ifndef  XEMICS_H
#define  XEMICS_H

/*----compilation control-----------------------------------------------------*/

/*******************************************************************************
INCLUDE FILES   Note: Minimize nesting of header files.
*******************************************************************************/
/*----system and platform files-----------------------------------------------*/

/*----program files-----------------------------------------------------------*/
#include "types.h"

/*******************************************************************************
FILE CONTEXT
*******************************************************************************/
/*----defines, typedefs, structs, unions, enums-------------------------------*/
typedef enum
{
  RFB_4800,
  RFB_9600,
  RFB_19200,
  RFB_38400,
  RFB_76800,
  RF_BAUDRATE = RFB_9600,  // desired baudrate
  RF_BAUDRATE_MULTIPLIER = (1 << RF_BAUDRATE)
} RfBaudrate;

#define XEMICS_TRANSMIT_DELAY_BITS 4 // stay in TX mode for 4 CLK cycles after the last data bit

typedef enum
{
  STANDARD_FREQUENCY_MODE,
  LOW_INTERFERENCE_FREQUENCY_MODE
} FrequencyMode;

/*----data descriptions-------------------------------------------------------*/

/*----data declarations-------------------------------------------------------*/

/*----macros------------------------------------------------------------------*/

/*----function prototypes-----------------------------------------------------*/
extern void initXemicsChip(void);
extern void sendChannel(unsigned char channel);
extern void setModulation(bool mod);
extern void SetReducedTXPowerLevel(bool reducedTXPower);
extern void latchFrequencyError(void);
extern void applyFrequencyErrorOffset(void);
extern void setXemicsEnable(void);
extern void readRSSI(void);
extern char getCurrentRSSI(void);
extern void clearCurrentRSSI(void);
extern void SetFrequencyMode(FrequencyMode mode);

#define RF_BITS_PER_SECOND (4800 * RF_BAUDRATE_MULTIPLIER)
#define RFBitTime (SMCLK / RF_BITS_PER_SECOND)


#endif                                  /* end of file */
