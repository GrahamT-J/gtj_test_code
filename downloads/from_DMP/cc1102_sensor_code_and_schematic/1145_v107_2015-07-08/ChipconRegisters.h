/*H*****************************************************************************
FILENAME: ChipconRegister.h

DESCRIPTION: This file contains struct definitions of all the config registers used
              in the CC1100 wireless transceiver chip by Chipcon.

NOTES:

Copyright (c) Digital Monitoring Products Inc. 1999.  All rights reserved.
*****************************************************************************H*/
#ifndef  ChipconRegister_H
#define  ChipconRegister_H

/*
Below are structures containing definitions for all registers contained in the
  CC1100 chipcon RF transceiver.  For more information regarding the meaning of
  each register see the CC1100 datasheet
*/

//Addresss: Register Name - Description
//0x00: IOCFG2  GDO2 output pin configuration
typedef struct
{
  BITFIELD  gdo2Mode      : 6;  //  GDO2_CFG
  BITFIELD  gdo2Inverted  : 1;  //  GDO2_INV
  BITFIELD  reserved      : 1;
} Reg_IOCFG2;

//0x01: IOCFG1  GDO1 output pin configuration
typedef struct
{
  BITFIELD  gdo1Mode            : 6;  // GDO1_CFG
  BITFIELD  gdo1Inverted        : 1;  // GDO1_INV
  BITFIELD  gdoDriveStrength    : 1;  //  GDO_DS
} Reg_IOCFG1;

//0x02: IOCFG0  GDO0 output pin configuration
typedef struct
{
  BITFIELD  gdo0Mode           : 6;  // GDO0_CFG
  BITFIELD  gdo0Inverted      : 1;  // GDO0_INV
  BITFIELD  tempSensorEnable  : 1;  // TEMP_SENSOR_ENABLE
} Reg_IOCFG0;

//0x03: FIFOTHR  RX FIFO and TX FIFO thresholds
typedef struct
{
  BITFIELD  fifoThreshold : 4;  // FIFO_THR
  BITFIELD  reserved      : 4;
} Reg_FIFOTHR;

//0x04: SYNC1  Sync word, high byte
typedef struct
{
  unsigned char syncWordHighByte;  // SYNC[15:8]
} Reg_SYNC1;

//0x05: SYNC0  Sync word, low byte
typedef struct
{
  unsigned char syncWordLowByte;  // SYNC[0:7]
} Reg_SYNC0;

//0x06: PKTLEN  Packet length
typedef struct
{
  unsigned char packetLength; // PACKET_LENGTH
} Reg_PKTLEN;

//0x07: PKTCTRL1  Packet automation control
typedef struct
{
  BITFIELD  addressCheckMode          : 2;  // ADR_CHK
  BITFIELD  appendStatus             : 1;  // APPEND_STATUS
  BITFIELD  reserved                  : 1;
  BITFIELD  wakeOnRadioAutoSync       : 1;  // WOR_AUTOSYNC
  BITFIELD  preambleQualityThreshold  : 3;  // PQT
} Reg_PKTCTRL1;

//0x08: PKTCTRL0  Packet automation control
typedef struct
{
  BITFIELD  packetLength  : 2;  // LENGTH_CONFIG
  BITFIELD  enableCRC     : 1;  // CRC_EN
  BITFIELD  enableCC2400Support :1; // CC2400_EN
  BITFIELD  packetFormat  : 2;  // PKT_FORMAT
  BITFIELD  dataWhitening : 1;  // WHITE_DATA
  BITFIELD  reserved      : 1;
} Reg_PKTCTRL0;

//0x09: ADDR  Device address
typedef struct
{
  unsigned char deviceAddress; // DEVICE_ADDR
} Reg_ADDR;

//0x0A: CHANNR  Channel number
typedef struct
{
  unsigned char channelNumber; // CHAN
} Reg_CHANNR;

//0x0B: FSCTRL1  Frequency synthesizer control
typedef struct
{
  BITFIELD ifFrequency  : 5;  // FREQ_IF
  BITFIELD reserved     : 1;
} Reg_FSCTRL1;

//0x0C: FSCTRL0  Frequency synthesizer control
typedef struct
{
  unsigned char frequencyOffset;  // FREQOFF
} Reg_FSCTRL0;

//0x0D: FREQ2  Frequency control word, high byte
typedef struct
{
  unsigned char frequencyHighByte; // FREQ[23:16]
} Reg_FREQ2;

//0x0E: FREQ1  Frequency control word, middle byte
typedef struct
{
  unsigned char frequencyMidByte; // FREQ[15:8]
} Reg_FREQ1;

//0x0F: FREQ0  Frequency control word, low byte
typedef struct
{
  unsigned char frequencyLowByte; // FREQ[7:0]
} Reg_FREQ0;

//0x10: MDMCFG4  Modem configuration
typedef struct
{
  BITFIELD dataRateExponent : 4;    // DRATE_E
  BITFIELD channelBWMantissa  : 2;  // CHANBW_M
  BITFIELD channelBWExponent  : 2;  // CHANBW_E
} Reg_MDMCFG4;

//0x11: MDMCFG3  Modem configuration
typedef struct
{
  unsigned char dataRateMantissa; // DRATE_M
} Reg_MDMCFG3;

//0x12: MDMCFG2  Modem configuration
typedef struct
{
  BITFIELD syncMode : 3; // SYNC_MODE
  BITFIELD enableManchester : 1;  // MANCHESTER_EN
  BITFIELD modulationFormat : 3;  // MOD_FORMAT
  BITFIELD disableDCFilter  : 1;  // DEM_DCFILT_OFF
} Reg_MDMCFG2;

//0x13: MDMCFG1 Modem configuration
typedef struct
{
  BITFIELD channelSpacingExponent : 2;  // CHANSPC_E
  BITFIELD reserved               : 2;
  BITFIELD numPreambleBytes       : 3;  // NUM_PREAMBLE
  BITFIELD enableFEC              : 1;  // FEC_EN
} Reg_MDMCFG1;

//0x14: MDMCFG0 Modem configuration
typedef struct
{
  unsigned char channelSpacingMantissa; // CHANSPC_M
} Reg_MDMCFG0;

//0x15: DEVIATN  Modem deviation setting
typedef struct
{
  BITFIELD deviationMantissa  : 3;  // DEVIATION_M
  BITFIELD reserved           : 1;
  BITFIELD deviationExponant  : 3;  // DEVIATION_E
  BITFIELD reserved1          : 1;
} Reg_DEVIATN;

//0x16: MCSM2  Main Radio Control State Machine configuration
typedef struct
{
  BITFIELD rxTime : 3;  // RX_TIME
  BITFIELD rxTimeQualifier  : 1;  // RX_TIME_QUAL
  BITFIELD rxTimeRSSI       : 1;  // RX_TIME_RSSI
  BITFIELD reserved         : 3;
} Reg_MCSM2;

//0x17: MCSM1 Main Radio Control State Machine configuration
typedef struct
{
  BITFIELD txOffMode  : 2;  // TXOFF_MODE
  BITFIELD rxOffMode  : 2;  // RXOFF_MODE
  BITFIELD clearChannelMode : 2;  // CCA_MODE
  BITFIELD reserved   : 2;
} Reg_MCSM1;

//0x18: MCSM0 Main Radio Control State Machine configuration
typedef struct
{
  BITFIELD forceXOSCOn : 1;  // XOSC_FORCE_ON
  BITFIELD enablePinControl  : 1;  // PIN_CTRL_EN
  BITFIELD powerOnTimeout    : 2;  // PO_TIMEOUT
  BITFIELD freqSynthAutoCal  : 2;  // FS_AUTOCAL
  BITFIELD reserved          : 2;
} Reg_MCSM0;

//0x19: FOCCFG  Frequency Offset Compensation configuration
typedef struct
{
  BITFIELD frequencyOffsetConfig  : 6;  // FOCCFG
  BITFIELD reserved         : 2;
} Reg_FOCCFG;

//0x1A: BSCFG  Bit Synchronization configuration
typedef struct
{
  unsigned char bitSyncConfig;  // BSCFG
} Reg_BSCFG;

//0x1B: AGCCTRL2  AGC control
typedef struct
{
  unsigned char AGCControl2;  //AGCCTRL2
} Reg_AGCCTRL2;

//0x1C: AGCCTRL1  AGC control
typedef struct
{
  BITFIELD AGCControl1 : 7; //AGCCTRL1
  BITFIELD reserved   : 1;
} Reg_AGCCTRL1;

//0x1D: AGCCTRL0  AGC control
typedef struct
{
  unsigned char AGCControl0;  // AGCCTRL0
} Reg_AGCCTRL0;

//0x1E: WOREVT1  High byte event0 timeout
typedef struct
{
  unsigned char event0TimeOutHighByte; // EVENT[15:8]
} Reg_WOREVT1;

//0x1F: WOREVT0 Low byte event0 timeout
typedef struct
{
  unsigned char event0TimeOutLowByte;  // EVENT[7:0]
} Reg_WOREVT0;

//0x20: WORCTRL  Wake On Radio control
typedef struct
{
  BITFIELD worTimerResolution   : 2;  // WOR_RES
  BITFIELD reserved             : 1;
  BITFIELD enableRcOscCalibration : 1;  // RC_CAL
  BITFIELD worEvent1            : 3;  //EVENT1
  BITFIELD rcOscPowerDown       : 1;  //RC_PD
} Reg_WORCTRL;

//0x21: FREND1  Front end RX configuration
typedef struct
{
  unsigned char frontEndRXConfig; // FREND1
} Reg_FREND1;

//0x22: FREND0  Front end TX configuration
typedef struct
{
  BITFIELD paTableIndex : 3;  // PA_POWER
  BITFIELD reserved     : 1;
  BITFIELD txLoBufferCurrent  : 2;  // LODIV_BUF_CURRENT_TX
  BITFIELD reserved1    : 2;
} Reg_FREND0;

//0x23: FSCAL3  Frequency synthesizer calibration
typedef struct
{
  unsigned char FSCalibrationConfig3;  // FSCAL3
} Reg_FSCAL3;

//0x24: FSCAL2  Frequency synthesizer calibration
typedef struct
{
  BITFIELD fsCalibrationResults2  : 7;  //FSCAL2
  BITFIELD reserved               : 1;
} Reg_FSCAL2;

//0x25: FSCAL1  Frequency synthesizer calibration
typedef struct
{
  BITFIELD fsCalibrationResults1  : 7;  //FSCAL1
  BITFIELD reserved               : 1;
} Reg_FSCAL1;

//0x26: FSCAL0  Frequency synthesizer calibration
typedef struct
{
  BITFIELD fsCalibrationResults0  : 7;  //FSCAL0
  BITFIELD reserved               : 1;
} Reg_FSCAL0;

// All configuration registers contained in the CC1100
typedef struct
{
  Reg_IOCFG2    Dflt_IOCFG2;
  Reg_IOCFG1    Dflt_IOCFG1;
  Reg_IOCFG0    Dflt_IOCFG0;
  Reg_FIFOTHR   Dflt_FIFOTHR;
  Reg_SYNC1     Dflt_SYNC1;
  Reg_SYNC0     Dflt_SYNC0;
  Reg_PKTLEN    Dflt_PKTLEN;
  Reg_PKTCTRL1  Dflt_PKTCTRL1;
  Reg_PKTCTRL0  Dflt_PKTCTRL0;
  Reg_ADDR      Dflt_ADDR;
  Reg_CHANNR    Dflt_CHANNR;
  Reg_FSCTRL1   Dflt_FSCTRL1;
  Reg_FSCTRL0   Dflt_FSCTRL0;
  Reg_FREQ2     Dflt_FREQ2;
  Reg_FREQ1     Dflt_FREQ1;
  Reg_FREQ0     Dflt_FREQ0;
  Reg_MDMCFG4   Dflt_MDMCFG4;
  Reg_MDMCFG3   Dflt_MDMCFG3;
  Reg_MDMCFG2   Dflt_MDMCFG2;
  Reg_MDMCFG1   Dflt_MDMCFG1;
  Reg_MDMCFG0   Dflt_MDMCFG0;
  Reg_DEVIATN   Dflt_DEVIATN;
  Reg_MCSM2     Dflt_MCSM2;
  Reg_MCSM1     Dflt_MCSM1;
  Reg_MCSM0     Dflt_MCSM0;
  Reg_FOCCFG    Dflt_FOCCFG;
  Reg_BSCFG     Dflt_BSCFG;
  Reg_AGCCTRL2  Dflt_AGCCTRL2;
  Reg_AGCCTRL1  Dflt_AGCCTRL1;
  Reg_AGCCTRL0  Dflt_AGCCTRL0;
  Reg_WOREVT1   Dflt_WOREVT1;
  Reg_WOREVT0   Dflt_WOREVT0;
  Reg_WORCTRL   Dflt_WORCTRL;
  Reg_FREND1    Dflt_FREND1;
  Reg_FREND0    Dflt_FREND0;
  Reg_FSCAL3    Dflt_FSCAL3;
  Reg_FSCAL2    Dflt_FSCAL2;
  Reg_FSCAL1    Dflt_FSCAL1;
  Reg_FSCAL0    Dflt_FSCAL0;
} Reg_CC1100;

#endif                                  /* end of file */
