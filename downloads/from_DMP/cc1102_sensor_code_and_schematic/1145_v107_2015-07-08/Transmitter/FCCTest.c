/*******************************************************************************
FILENAME:

DESCRIPTION:

NOTES:

Copyright (c) Digital Monitoring Products Inc. 2004.  All rights reserved.

*******************************************************************************/
/*----compilation control-----------------------------------------------------*/

/*******************************************************************************
INCLUDE FILES
*******************************************************************************/
/*----system and platform files (external libraries) -------------------------*/


/*----program files-----------------------------------------------------------*/

/*******************************************************************************
EXTERNAL REFERENCES   Only used if not available in a header file.
*******************************************************************************/
/*----data declarations-------------------------------------------------------*/

/*----function prototype------------------------------------------------------*/


/*******************************************************************************
PRIVATE DECLARATIONS  
*******************************************************************************/
/*----defines, typedefs, structs, unions, enums-------------------------------*/

/*----data declarations-------------------------------------------------------*/

/*----function prototype------------------------------------------------------*/

/*----macros------------------------------------------------------------------*/

/*******************************************************************************
PRIVATE FUNCTION DEFINITIONS
*******************************************************************************/
/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/


/*******************************************************************************
PUBLIC FUNCTION DEFINITIONS  
*******************************************************************************/
/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
#ifdef FCC_SPECIAL_VERSION
void fccTest(void)
{
  extern void StrobeXeEnable(void);
  extern void SendFreqency(byte, signed char);
  typedef enum
  {
    FCC_FIRST_STATE,
    FCC_LOW_OUT = FCC_FIRST_STATE,
    FCC_MED_OUT,
    FCC_HIGH_OUT,
    FCC_LISTEN,
    FCC_HOPPING_TRANSMIT,
    FCC_INVALID_STATE,
    FCC_LAST_STATE = FCC_INVALID_STATE - 1
  } FccState;
  FccState currentState = FCC_FIRST_STATE;
  unsigned int OffTest = 0;
      
  Hand_Sync();	// turn on hop following
  setWirelessMode(WM_TRANSMIT);
  sendFreq(-24000);

  // turn on modulation
  setModulation(true);
  StrobeXeEnable();

  FreqBias = 0;
  NextAcq = 0;
  while (true)
  {
    int i = 0;
    bool pressed = false;
#ifdef FCC_GLASSBREAK
    while (getIO(TAMPER) == false)               // loop while input high
#else      
    while (getIO(REED_SW))               // loop while input high
#endif
    {
      if (! pressed)
      {
        if (++i > 5000)
        {
          pressed = true;
          if (++currentState > FCC_LAST_STATE)
          {
            currentState = FCC_FIRST_STATE;
          }
          // operate based upon state
          
          switch (currentState)
          {
          case FCC_LOW_OUT:
            setWirelessMode(WM_TRANSMIT);
            sendFreq(-24000);
            break;
            
          case FCC_MED_OUT:
            setWirelessMode(WM_TRANSMIT);
            sendFreq(0);
            break;
            
          case FCC_HIGH_OUT:
            setWirelessMode(WM_TRANSMIT);
            sendFreq(24000);
            break;
            
          case FCC_LISTEN:
            setWirelessMode(WM_RECEIVE);
            break;
            
          case FCC_HOPPING_TRANSMIT:
            setWirelessMode(WM_RECEIVE);
            while (true)
            {
              if (communicationStatus.timeToHop)
              {
                communicationStatus.timeToHop = false;
                StrobeXeEnable();
              }
            }
            break;
            
          }
        }
      }
    }
    // debounce release
    if (pressed)
    {
      StrobeXeEnable();
          
      for (i = 0; i < 5000; ++i)
      {
        _NOP();
      }  
    }
    if ((currentState != FCC_LISTEN) && (++OffTest > 50000))
    {
      OffTest = 0;
      setWirelessMode(WM_RECEIVE);
      while (++OffTest < 20000)
        ;
      OffTest = 0;
      setWirelessMode(WM_TRANSMIT);
    }
  }
}


/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
void fccInterrupt(void)
{
  CCR2 += XeBitTime; // next capture time = bit timing
  if (++RFFrameBitCounter >= SlotEND)
  {
    RFFrameBitCounter = 0;
    pendchan = NextAcquisitionChannel(currentChannel);
    communicationStatus.timeToHop = true;
  }
  if (RFFrameBitCounter & 1) CCTL2 = OUTMOD_5+CM0+CCIE;
  else CCTL2 = OUTMOD_1+CM0+CCIE;
}


#endif

