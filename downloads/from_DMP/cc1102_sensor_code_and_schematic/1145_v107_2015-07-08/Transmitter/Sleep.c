/*******************************************************************************
FILENAME: Sleep.c

DESCRIPTION:

NOTES:

Copyright (c) Digital Monitoring Products Inc. 2004-2013.  All rights reserved.

*******************************************************************************/
/*----compilation control-----------------------------------------------------*/

/*******************************************************************************
INCLUDE FILES
*******************************************************************************/
/*----system and platform files (external libraries) -------------------------*/

/*----program files-----------------------------------------------------------*/
#include "Sleep.h"
#include "TransmitterProtocol.h"
#include "Main.h"
#include "Ioportpins.h"
#include "Debug.h"
#include "Inputs.h"
#include "WirelessCommunication.h"
#include "Timer.h"

/*******************************************************************************
EXTERNAL REFERENCES   Only used if not available in a header file.
*******************************************************************************/
/*----data declarations-------------------------------------------------------*/

/*----function prototype------------------------------------------------------*/


/*******************************************************************************
PRIVATE DECLARATIONS
*******************************************************************************/
/*----defines, typedefs, structs, unions, enums-------------------------------*/
typedef struct
{
  BITFIELD sleeping                 : 1;
  BITFIELD alarmWakeup              : 1;
  BITFIELD currentlyCommunicating   : 1;  // set if not done trying to send a message
  BITFIELD receiverFailed           : 1;
  BITFIELD receiveCommand           : 1;
  BITFIELD giveUpTimerActive        : 1;
  BITFIELD                          : 2;
  byte wakeupAttempt;
} SleepStatus;

enum
{
#if defined(DMP_1145_TRANSMITTER)
  MAX_WAKEUP_ATTEMPTS = 2,               // NOTE: Add 1 to the desired value (for 1 attempt, use 2 here).
#elif defined(DMP_1139_TRANSMITTER)
  MAX_WAKEUP_ATTEMPTS = 3,               // NOTE: Add 1 to the desired value (for 2 attempts, use 3 here).
#else
  MAX_WAKEUP_ATTEMPTS = 60
#endif

#if defined(DMP_1145_TRANSMITTER)
  MAX_WAIT_FOR_ACK_WAKEUP_ATTEMPTS = 12, // This is larger than 2 minutes because it's a failsafe. The FOB uses an actual 2 minute timer instead of relying on this value, since there's significant variance.
#elif defined(DMP_1139_TRANSMITTER)
  MAX_WAIT_FOR_ACK_WAKEUP_ATTEMPTS = 9,  // 8 attempts take about 5-7 minutes.
  MAX_WAKEUP_ATTEMPTS_LAST_RESORT = 31,  // When never syncing with rcvr, each attempt takes about 20 seconds. (31-1) * 20 = 600 seconds (10 min). This can be longer if sync with a rcvr occurs.
#endif
};

#define COMMAND_TIMEOUT 20

extern SlotNumber currentSlotNumber;
/*----data declarations-------------------------------------------------------*/
static SleepStatus sleepStatus;
#ifdef ENABLE_RECEIVER_COMMANDS
#ifdef ENABLE_COMMAND_CANCEL
static unsigned char commandTimeoutCounter = 0;
#endif
#endif
word sleepTime = 0;

static byte giveUpCountdown;

/*----function prototype------------------------------------------------------*/

/*----macros------------------------------------------------------------------*/

/*******************************************************************************
PRIVATE FUNCTION DEFINITIONS
*******************************************************************************/
/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
bool getCurrentlyCommunicating(void)
{
  return sleepStatus.currentlyCommunicating;
}

/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
bool getSleeping(void)
{
  return sleepStatus.sleeping;
}

/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
bool getAlarmWakeup(void)
{
  return sleepStatus.alarmWakeup;
}


/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
static void processSuccessfulWakeup(void)
{
  sleepStatus.wakeupAttempt = 0;
  sleepStatus.receiverFailed = false;
}

/*******************************************************************************

DESCRIPTION:  Returns a standard or somewhat random sleep interval, based upon
various bits of the serial number.  Starts out waiting up to 1/4 a second and
moves out to 2 seconds, if needed.

NOTES:

Try     Mask      Sleep from 1 cycle to ...
---     ----      -------------------------
1       0x07      8
2       0x07      8
3       0x07      8
4       0x07      8
5       0x0F      16
6       0x1F      32
7       0x3F      64
8       0x3F
9       0x3F
10      0x3F
11      0x3F
12      0x3F
13      0x3F
14      0x3F
15      0x3F
16      0x3F

*******************************************************************************/

static unsigned short getSleepInterval(bool normalSleep, bool *timedOut)
{
  enum
  {
    MIN_WAIT_MASK   = 0x07,
    MAX_WAIT_MASK_NUM_BITS = 6,
    SERIAL_NUM_BITS = 24
  };
  static char numTries;
  char mask;
  char interval;

  if ((normalSleep) || (numTries > SERIAL_NUM_BITS - MAX_WAIT_MASK_NUM_BITS))
  {
    unsigned int sleepTicksForCheckin = getSleepTicks();

    numTries = 0;
    *timedOut = true;
#ifdef ENABLE_RECEIVER_COMMANDS
    {
      unsigned int sleepTicksForCommand = getSleepTicksForCommand();

#ifdef ENABLE_COMMAND_CANCEL
      //if we are setup to receive a command and a command slot is going to occur before our checkin slot
      if ((sleepStatus.receiveCommand) && (sleepTicksForCommand < sleepTicksForCheckin))
        return sleepTicksForCommand;
#else
      if ((sleepTicksForCommand < sleepTicksForCheckin) && IsFastOutput())
      {
        sleepStatus.receiveCommand = true;
        return sleepTicksForCommand;
      }
      sleepStatus.receiveCommand = false;
#endif
    }
#endif
    return sleepTicksForCheckin;
  }

  *timedOut = false;

  if (numTries <= MAX_WAIT_MASK_NUM_BITS)
    mask = (1 << numTries);
  else
    mask = (1 << MAX_WAIT_MASK_NUM_BITS);

  mask--;
  mask |= MIN_WAIT_MASK;

  interval = ((serialNumber >> numTries) & mask) + 1;

  ++numTries;

  return (interval * SLEEP_TICKS_PER_HOP);
}

/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
static void FindChannel(void)
{
  char channelSkip;

  ENTER_CRITICAL_SECTION();

  // calculate the number of channels that have passed since we went to sleep
  channelSkip = (sleepTime % SLEEP_TICKS_PER_HOP_ROUND) / SLEEP_TICKS_PER_HOP;
  // add the number of skipped channels into the current channel plus one to account
  // for the fact that we are waking in the slot prior to the indented slot due to us
  // accounting for clock slop.
  currentChannel = (unsigned short)(currentChannel + ((channelSkip+1) * houseCode)) % NUM_HOP_CHANNELS;

  EXIT_CRITICAL_SECTION();

  CCR2 = TAR + RFBitTime;
  CCTL2 = OUTMOD_5+CM0+CCIE; // CC2 = Compare, Reset
}


/*******************************************************************************

DESCRIPTION: ReceiverFailed
When failure to communicate with the receiver occurs, this function is called.

NOTES:

*******************************************************************************/
static void ReceiverFailed(void)
{
  CancelGiveupTimer();
  // set wakeupAttempt to max so that when the current attempt completes (each attempt takes ~20 Sec)
  // then the FOB will give up and go to sleep (assuming the last attempt fails, that is).
  sleepStatus.wakeupAttempt = 255;  // give up retrying
  sleepStatus.receiverFailed = true;  // declare we have failed. Enter "deep sleep" (sleep until a contact changes state and passes debounce timer test).
  InitButtonHandling();
}



/*******************************************************************************

DESCRIPTION: CallBack_GiveUpTimer
This is a timer callback fcn that times out how long the FOB will attempt
to send a message before giving up.

NOTES:

*******************************************************************************/
static void CallBack_GiveUpTimer(void)
{
  if (giveUpCountdown)
  {
    --giveUpCountdown;
  }

  if (!giveUpCountdown)
  {
    ReceiverFailed();  // cancel this timer and initialize sleep vars, etc.
  }
  else
  {
    // start timing the next 6 seconds
    StartTimer(GIVE_UP_TIMER, TIME_6_Sec, CallBack_GiveUpTimer, TIMER_CONTINUOUS);
  }
}


/*******************************************************************************
PUBLIC FUNCTION DEFINITIONS
*******************************************************************************/
/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
bool receiverFailed(void)
{
  return sleepStatus.receiverFailed;
}

/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
bool getCommandStatus(void)
{
  return sleepStatus.receiveCommand;
}

/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
void CancelCommandReceive(void)
{
#if defined(ENABLE_RECEIVER_COMMANDS) && defined(ENABLE_COMMAND_CANCEL)
  sleepStatus.receiveCommand = false;
  commandTimeoutCounter = 0;
#endif
}
/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
void goToSleep(TransmitStatus status)
{
  unsigned short sleepInterval = 0;
  char slotOffset;
  bool timedOut = false;
  bool hardSleep = true;  // set if we went to sleep for more than a short retry time

  ENTER_CRITICAL_SECTION();

  switch (status)
  {
    case TS_SUCCESS:
#ifdef ENABLE_RECEIVER_COMMANDS
#ifdef ENABLE_COMMAND_CANCEL
      if (!getAlarmWakeup() && (commandTimeoutCounter > 0))
      {
        if (--commandTimeoutCounter == 0)
        {
          sleepStatus.receiveCommand = false;
        }
      }
      // if sleepStatus.receiveCommand is ever true while commandTimeoutCounter is 0,
      // the transmitter will wake up and enable the CC1100 RX every .5 Sec forever!
      if (commandTimeoutCounter == 0)
        sleepStatus.receiveCommand = false;
#endif
#endif
      processSuccessfulWakeup();
      sleepStatus.currentlyCommunicating = false;
      sleepInterval = getSleepInterval(true, &timedOut);
      timedOut = false; // override timeout
      break;

    case TS_FAILURE:
      sleepStatus.currentlyCommunicating = false;
      sleepInterval = getSleepInterval(true, &timedOut);
      timedOut = true; // override timeout
      #if defined(ENABLE_RECEIVER_COMMANDS) && defined(ENABLE_COMMAND_CANCEL)
        CancelCommandReceive();
      #endif
      break;

    case TS_RETRY:
      sleepInterval = getSleepInterval(false, &timedOut);
      if (timedOut)
      {
        sleepStatus.currentlyCommunicating = false;
      }
      hardSleep = false;
      break;
  }

  initializeCommunicationStatus();
  processMainShutdown();

  clearDebug(DM_1101_LED);
  setupInputs();

  TACTL = TASSEL_1 + // ACLK
          MC_2 + // continuous
          SLEEP_TIMERA_DIVIDER_SETTING * ID0;

#ifdef DMP_1142_TRANSMITTER
    if (ackStatus.panicAckWaiting && (status == TS_SUCCESS))
    {
      startPanicPulsing();
      ackStatus.panicAckWaiting = false;
    }
#endif

  // Using RFFrameBitCounter, calculate the number of sleep ticks are into the current slot
  slotOffset = RFFrameBitCounter * SLEEP_TICKS_PER_HOP / FB_TOTAL;
  // Since RFFrameBitCounter ranges from 150 to 300 in RX and 0 to 150 in TX we need
  // to normalize slotOffset so it is truely the number of sleep ticks into the current slot
  slotOffset = ((slotOffset + (SLEEP_TICKS_PER_HOP / 2)) % SLEEP_TICKS_PER_HOP);
  // Subtract slotOffset from our current time to get the reference for the begining
  // of the current slot, record this as the time we went to sleep.
  sleepTime = TAR - slotOffset;
  // Using the reference of the begining of the current slot, calculate how long
  // we need to sleep and subtract clock slop to assure we wake up early.
  CCR2 = sleepTime + sleepInterval - WAKEUP_CLOCK_SLOP_TICKS;
  CCTL2 = CCIE;   // compare timer with interrupt enabled
  sleepStatus.sleeping = true;
  startHopTimer(QUARTER_SECOND);  // schedule wakeup after a quarter second, in case we have changed states, or have a queued message
  STOP_WATCHDOG();  // stop watchdog

  // Use watchdog timer in interval-timer mode now, since we're sleeping.
  // After a period of time elapses, an interrupt will run and will refresh the programming
  // of the CC1100 to ensure it's in the SPWD (powerdown) state. Also, periodic battery tests are run.

  WATCHDOG_INTERVAL_TIMER_8_SEC();  // start timer interval: reset counter, and use (32768 X Aclk = 1 Second) x 8 = 8 Sec

  IE1 |= WDTIE;        // enable watchdog timer interval interrupt
  disableIOResistor(SO_GDO1); // The CC1100 is driving this pin low sometimes while the MSP430 is in low power mode
  
  // the call to LPM3 must follow this instruction for proper operation
  EXIT_CRITICAL_SECTION();
  // with the MSP430 the instruction following the EINT instruction is guaranteed to
  // execute before interrupts are enabled.
  (void)LPM3;           // Enter LPM3 until timer or contact change
  _NOP();         // for debugger
  IE1 &= ~WDTIE;        // disable watchdog timer interval interrupt
  KICK_WATCHDOG();  // start watchdog, reset, and use ACLK
  enableIOResistor(SO_GDO1);

  // start over almost fresh, since we are not communicating
  if (timedOut)
  {
    resetLearnedInformation();
#ifdef DMP_1119_TRANSMITTER
    // There is no need to reset the hardware if the transmitter is able to communicate
    if (BothTransmitterNumbersInvalid())
#endif
      resetHardware(false);
  }

  FindChannel();
  processMainWakeup(hardSleep);
}

/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
bool wakeup(bool isAlarmWakeup, bool commandReceiveRequest)
{
  bool allowWakeup = true;

#if defined USING_1232_HARDWARE || defined DMP_1119_TRANSMITTER
  if (isAlarmWakeup)
    sleepStatus.alarmWakeup = true;
#endif

  if (!sleepStatus.currentlyCommunicating)
  {
    sleepStatus.alarmWakeup = isAlarmWakeup && IsLatchedMessage(); // wakeup for checkin is not an alarm; ignore isAlarmWakeup if it's true and there's nothing latched to send
#if defined(DMP_1145_TRANSMITTER) || defined(DMP_1139_TRANSMITTER)
    byte maxAttempts;
    if (ImportantMessagesAreWaiting())
    {
      maxAttempts = MAX_WAIT_FOR_ACK_WAKEUP_ATTEMPTS;
      if (!sleepStatus.giveUpTimerActive)
      {
        sleepStatus.giveUpTimerActive = true;
        giveUpCountdown = 20;  // count 20 * 6 seconds for a 2 minute give-up timeout
        StartTimer(GIVE_UP_TIMER, TIME_6_Sec, CallBack_GiveUpTimer, TIMER_CONTINUOUS);
      }
    }
    else
    {
      maxAttempts = MAX_WAKEUP_ATTEMPTS;
    }

    if (sleepStatus.wakeupAttempt < maxAttempts) // if counter is under the max value
    {
      sleepStatus.wakeupAttempt ++; // increment
    }
    
    if (!sleepStatus.receiverFailed && (sleepStatus.wakeupAttempt >= maxAttempts)) // if we have reached the max number of attempts
    {
      ReceiverFailed();
    }
#else
    if (++sleepStatus.wakeupAttempt >= MAX_WAKEUP_ATTEMPTS)
    {
      sleepStatus.wakeupAttempt = 0;
      sleepStatus.receiverFailed = true;
    }
#endif
  }

  if (!sleepStatus.alarmWakeup && sleepStatus.receiverFailed && (sleepStatus.wakeupAttempt > 0))
  {
    allowWakeup = false;
  }

  if (allowWakeup)
  {
    sleepTime = TAR - sleepTime;

    setDebug(DM_1101_LED);
#ifndef USING_CHIPCON_RF_CHIP
    TACTL = TASSEL_2 +    // use DCO clock
            MC_2;         // continuous
    CCTL2 = 0;   // no interrupt at first, will start later
#endif
    sleepStatus.sleeping = false;
    sleepStatus.currentlyCommunicating = true;
#ifdef ENABLE_RECEIVER_COMMANDS
    if (commandReceiveRequest)
    {
#ifdef ENABLE_COMMAND_CANCEL
      commandTimeoutCounter = COMMAND_TIMEOUT;
#endif
      sleepStatus.receiveCommand = true;
    }
#endif
  }

  return allowWakeup;
}

/*******************************************************************************

DESCRIPTION: EnableFullRetries

NOTES:  This is called in order to have a fresh start at trying to send
a wireless message (such as when a contact changes state). All retry counts,
timeouts, etc. are reset.

*******************************************************************************/
void EnableFullRetries(void)
{
  sleepStatus.wakeupAttempt = 0;
  sleepStatus.receiverFailed = false;
}



/*******************************************************************************

DESCRIPTION: RestartCommandReceive will start the counter for listening 
for a command from the receiver (such as LED flashing).

NOTES:

*******************************************************************************/
#ifdef ENABLE_RECEIVER_COMMANDS
void RestartCommandReceive(void)
{
  if (houseCode == INVALID_HOUSE_CODE)
    return;
  
  sleepStatus.receiveCommand = true;
  #ifdef ENABLE_COMMAND_CANCEL
    commandTimeoutCounter = COMMAND_TIMEOUT;
  #endif
}
#endif



/*******************************************************************************

DESCRIPTION: InitButtonHandling
Initialize the queue, latch, etc.

NOTES:

*******************************************************************************/
void InitButtonHandling(void)
{
  EmptyQueue();
  InvalidateLatch();
  UpdateAckStatus(NOT_WAITING_FOR_ACK);
  CancelCommandReceive();
}

/*******************************************************************************

DESCRIPTION: CancelGiveupTimer
Cancel the 'give up' timer, and update flag

NOTES:

*******************************************************************************/
void CancelGiveupTimer(void)
{
  CancelTimer(GIVE_UP_TIMER);
  sleepStatus.giveUpTimerActive = false;
}
