/*******************************************************************************
FILENAME: Sleep.h

DESCRIPTION:

NOTES:

Copyright (c) Digital Monitoring Products Inc. 2004-2013.  All rights reserved.

*******************************************************************************/
#ifndef  SLEEP_H
#define  SLEEP_H

/*----compilation control-----------------------------------------------------*/

/*******************************************************************************
INCLUDE FILES
*******************************************************************************/
/*----system and platform files-----------------------------------------------*/
#include "types.h"

/*----program files-----------------------------------------------------------*/
#include "Hardware.h"
#include "ProtocolTiming.h"
#include "Hop.h"

/*******************************************************************************
FILE CONTEXT
*******************************************************************************/
/*----defines, typedefs, structs, unions, enums-------------------------------*/
enum
{
  SLEEP_TICKS_PER_SECOND = (int)((SLEEP_MASTER_CLOCK / SLEEP_CLOCK_DIVIDER) / SLEEP_TIMERA_DIVIDER),
  SLEEP_TICKS_PER_HOP = SLEEP_TICKS_PER_SECOND / HOPS_PER_SECOND,
  SLEEP_TICKS_PER_HOP_ROUND = SLEEP_TICKS_PER_HOP * NUM_HOP_CHANNELS,
  WAKEUP_SECONDS = 60,
  WAKEUP_RATE = ((unsigned short)(((unsigned long)WAKEUP_SECONDS * HOPS_PER_SECOND) * SLEEP_TICKS_PER_HOP))
};

enum
{
  CLOCK_TOLERANCE_PPM = 100,  // crystal tolerance of transmitter and receiver
  SLEEP_TICK_TOLERANCE = (int)(((double)CLOCK_TOLERANCE_PPM / 1000000) * SLEEP_TICKS_PER_SECOND * WAKEUP_SECONDS),
  WAKEUP_CLOCK_SLOP_TICKS = 2 * SLEEP_TICK_TOLERANCE // multiply by two in case receiver and transmitter at opposite ends
};

typedef enum
{
  TS_SUCCESS,
  TS_FAILURE,
  TS_RETRY
} TransmitStatus;

enum
{
#ifdef DMP_1145_TRANSMITTER
  ACLK_TIMER_50_mSEC     = (unsigned)((SLEEP_TICKS_PER_SECOND *50) / 1000),   // 50 ms
  LED_PULSE_OFF_TIME     = 10,                          // 1/2 second = 10 * 50 mS
  SHORT_LED_PULSE_TIME   = LED_PULSE_OFF_TIME,          // 1/2 second
  MEDIUM_LED_PULSE_TIME  = (SHORT_LED_PULSE_TIME * 2),  // 1 second
  LONG_LED_PULSE_TIME    = (SHORT_LED_PULSE_TIME * 4),  // 2 seconds
#else
  LED_PULSE_OFF_TIME = SLEEP_TICKS_PER_SECOND * 3,
  LED_PULSE_ON_TIME = (int)((SLEEP_TICKS_PER_SECOND *50) / 1000),
#endif
  LED_FLASH_TIME = 100,  // 5 minutes based on 3 sec timer
  LED_ACK_COUNT = 3   //Flash LED for three more acks after tamper
};

/*----data descriptions-------------------------------------------------------*/
extern word sleepTime;

/*----data declarations-------------------------------------------------------*/

/*----macros------------------------------------------------------------------*/

/*----function prototypes-----------------------------------------------------*/
void goToSleep(TransmitStatus status);

bool wakeup(bool isAlarmWakeup, bool isCommandWakeup);
bool getSleeping(void);
bool getCurrentlyCommunicating(void);
bool getAlarmWakeup(void);
bool receiverFailed(void);
bool getCommandStatus(void);
void EnableFullRetries(void);
void InitButtonHandling(void);
void CancelGiveupTimer(void);

#ifdef ENABLE_RECEIVER_COMMANDS
  void RestartCommandReceive(void);
  #ifdef ENABLE_COMMAND_CANCEL
    void CancelCommandReceive(void);
  #endif
#endif

#endif                                  /* end of file */
