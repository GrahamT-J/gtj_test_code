/*******************************************************************************
FILENAME: Hardware.h

DESCRIPTION: Miscellaneous hardware descriptions not included elsewhere

NOTES:

Copyright (c) Digital Monitoring Products Inc. 2004-2013.  All rights reserved.

*******************************************************************************/
#ifndef  HARDWARE_H
#define  HARDWARE_H

/*----compilation control-----------------------------------------------------*/

/*******************************************************************************
INCLUDE FILES
*******************************************************************************/
/*----system and platform files-----------------------------------------------*/

/*----program files-----------------------------------------------------------*/

/*******************************************************************************
FILE CONTEXT
*******************************************************************************/
/*----defines, typedefs, structs, unions, enums-------------------------------*/
#define MASTER_CLOCK 4300800

#define XEBitt (MASTER_CLOCK / 19200)

#define SMCLK MASTER_CLOCK

#define DIV_1 0
#define DIV_2 1
#define DIV_4 2
#define DIV_8 3

#define SLEEP_MASTER_CLOCK            32768
#define SLEEP_CLOCK_DIVIDER_SETTING   DIV_8
#define SLEEP_CLOCK_DIVIDER           (1 << SLEEP_CLOCK_DIVIDER_SETTING)
#define SLEEP_TIMERA_DIVIDER_SETTING  DIV_4
#define SLEEP_TIMERA_DIVIDER          (1 << SLEEP_TIMERA_DIVIDER_SETTING)

// Power on default values for MSP430 registers
#define DEFAULT_BCSCTL1 0x8C  //POR value is 0x84, but the XT5V bit must also be set for the setDCO algorithm to converge

// Watchdog macros
#define KICK_WATCHDOG()                  (WDTCTL = WDTPW | WDTCNTCL | WDTSSEL)
#define FORCE_WATCHDOG_TIMEOUT()         (WDTCTL = (WDTPW ^ 0xFF00) | WDTCNTCL | WDTSSEL)  // write wrong password (the upper 8 bits)
#define WATCHDOG_INTERVAL_TIMER_2_SEC()  (WDTCTL = WDTPW | WDTCNTCL | WDTSSEL | WDTTMSEL | WDTIS0) // interval timer of 2 Sec
#define WATCHDOG_INTERVAL_TIMER_8_SEC()  (WDTCTL = WDTPW | WDTCNTCL | WDTSSEL | WDTTMSEL) // interval timer of 8 Sec
#define STOP_WATCHDOG()                  (WDTCTL = WDTPW | WDTCNTCL | WDTHOLD | WDTSSEL)

typedef struct
{
  BITFIELD  greenLED    : 1;
  BITFIELD  redLED      : 1;
  BITFIELD  flashCount  : 3;
  BITFIELD  ledsOn      : 1;
  BITFIELD  reserved    : 2;
  byte      flashDuration;
} LEDFlashStruct;
/*----data descriptions-------------------------------------------------------*/

/*----data declarations-------------------------------------------------------*/
extern LEDFlashStruct ledFlashStruct;
/*----macros------------------------------------------------------------------*/

/*----function prototypes-----------------------------------------------------*/
void initSys(bool hardReset);
void startLEDPulsing(unsigned char flashCount, unsigned char flashDuration, bool redLED, bool greenLED);
void PowerUpComparator(void);
void PowerDownComparator(void);


#endif                                  /* end of file */
