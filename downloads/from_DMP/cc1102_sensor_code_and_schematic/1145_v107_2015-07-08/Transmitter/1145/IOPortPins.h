// These should all be typed with enums, bitfield, typedefs, etc.
// However, by doing it this way, we can include this file in assembler, so keep it
// this way until most assembler is gone (probably will be kept forever) ;-) Kyle

#ifndef IOPORTPINS_H
#define IOPORTPINS_H

#include "BitMacros.h"
#include "MSP430SFR.h"

#define LED_ON(A) setIO(A)
#define LED_OFF(A) clearIO(A)
//-------------------------------------------------

#define BATTERY_VOLTAGE_MASK 0x08
#define BATTERY_VOLTAGE_DIR P2DIR

#define BATT_VOLTAGE_ENABLE_MASK  0x80
#define BATT_VOLTAGE_ENABLE_OUT   P1OUT
#define BATT_VOLTAGE_ENABLE_DIR   P1DIR
#define BATT_VOLTAGE_ENABLE_SEL   P1SEL             

#define RED_LED_MASK              0x40
#define RED_LED_OUT               P1OUT
#define RED_LED_DIR               P1DIR
#define RED_LED_IN                P1IN
#define RED_LED_SEL               P1SEL

#define GREEN_LED_MASK            0x20
#define GREEN_LED_OUT             P1OUT
#define GREEN_LED_DIR             P1DIR
#define GREEN_LED_IN              P1IN
#define GREEN_LED_SEL             P1SEL

#define CSn_MASK                  0x01
#define CSn_IN                    P1IN
#define CSn_OUT                   P1OUT
#define CSn_DIR                   P1DIR
#define CSn_SEL                   P1SEL

#define SI_MASK                   0x04
#define SI_IN                     P1IN
#define SI_OUT                    P1OUT
#define SI_DIR                    P1DIR
#define SI_SEL                    P1SEL

#define SCLK_MASK                 0x01
#define SCLK_IN                   P2IN
#define SCLK_OUT                  P2OUT
#define SCLK_DIR                  P2DIR
#define SCLK_SEL                  P2SEL

#define SO_GDO1_MASK              0x08
#define SO_GDO1_IN                P1IN
#define SO_GDO1_DIR               P1DIR
#define SO_GDO1_SEL               P1SEL
#define SO_GDO1_REN               P1REN
#define SO_GDO1_IE                P1IE
#define SO_GDO1_IES               P1IES
#define S0_GDO1_IFG               P1IFG

#define RF_DATA_CLK_MASK          SO_GDO1_MASK
#define RF_DATA_CLK_IN            SO_GDO1_IN
#define RF_DATA_CLK_DIR           SO_GDO1_DIR
#define RF_DATA_CLK_SEL           SO_GDO1_SEL
#define RF_DATA_CLK_IE            SO_GDO1_IE
#define RF_DATA_CLK_IES           SO_GDO1_IES
#define RF_DATA_CLK_IFG           S0_GDO1_IFG

#define GDO0_MASK                 0x10
#define GDO0_IN                   P2IN
#define GDO0_DIR                  P2DIR
#define GDO0_SEL                  P2SEL
#define GDO0_OUT                  P2OUT

#define RF_DATA_IN_MASK           GDO0_MASK
#define RF_DATA_IN_IN             GDO0_IN
#define RF_DATA_IN_DIR            GDO0_DIR
#define RF_DATA_IN_SEL            GDO0_SEL

#define RF_DATA_OUT_MASK          GDO0_MASK
#define RF_DATA_OUT_IN            GDO0_IN
#define RF_DATA_OUT_DIR           GDO0_DIR
#define RF_DATA_OUT_SEL           GDO0_SEL
#define RF_DATA_OUT_OUT           GDO0_OUT

#define EAST_BUTTON_MASK          0x02
#define EAST_BUTTON_IN            P2IN
#define EAST_BUTTON_OUT           P2OUT
#define EAST_BUTTON_DIR           P2DIR
#define EAST_BUTTON_SEL           P2SEL
#define EAST_BUTTON_REN           P2REN
#define EAST_BUTTON_IES           P2IES
#define EAST_BUTTON_IE            P2IE
#define EAST_BUTTON_IFG           P2IFG

#define NORTH_BUTTON_MASK         0x04
#define NORTH_BUTTON_IN           P2IN
#define NORTH_BUTTON_OUT          P2OUT
#define NORTH_BUTTON_DIR          P2DIR
#define NORTH_BUTTON_SEL          P2SEL
#define NORTH_BUTTON_REN          P2REN
#define NORTH_BUTTON_IES          P2IES
#define NORTH_BUTTON_IE           P2IE
#define NORTH_BUTTON_IFG          P2IFG

#define WEST_BUTTON_MASK          0x02
#define WEST_BUTTON_IN            P1IN
#define WEST_BUTTON_OUT           P1OUT
#define WEST_BUTTON_DIR           P1DIR
#define WEST_BUTTON_SEL           P1SEL
#define WEST_BUTTON_REN           P1REN
#define WEST_BUTTON_IES           P1IES
#define WEST_BUTTON_IE            P1IE
#define WEST_BUTTON_IFG           P1IFG

#define SOUTH_BUTTON_MASK         0x20
#define SOUTH_BUTTON_IN           P2IN
#define SOUTH_BUTTON_OUT          P2OUT
#define SOUTH_BUTTON_DIR          P2DIR
#define SOUTH_BUTTON_SEL          P2SEL
#define SOUTH_BUTTON_REN          P2REN
#define SOUTH_BUTTON_IES          P2IES
#define SOUTH_BUTTON_IE           P2IE
#define SOUTH_BUTTON_IFG          P2IFG

#endif
