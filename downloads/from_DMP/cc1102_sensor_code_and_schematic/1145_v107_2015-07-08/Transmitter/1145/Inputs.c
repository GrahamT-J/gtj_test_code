/*H*****************************************************************************
FILENAME: Inputs.c

DESCRIPTION:

NOTES:

Copyright (c) Digital Monitoring Products Inc. 2004-2013.  All rights reserved.

*****************************************************************************H*/
/*----compilation control-----------------------------------------------------*/

/*******************************************************************************
INCLUDE FILES
*******************************************************************************/
/*----system and platform files (external libraries) -------------------------*/
#include "types.h"

/*----program files-----------------------------------------------------------*/
#include "IOPortPins.h"
#include "Inputs.h"
#include "Hardware.h"
#include "ProtocolTiming.h"
#include "Sleep.h"
#include "Main.h"
#include "transmitterprotocol.h"
#include "chipconCommunication.h"
#include "Timer.h"
#include "TransmitterProtocol.h"

/*******************************************************************************
PUBLIC DECLARATIONS
*******************************************************************************/
#ifdef SEND_I_AM_ALIVE_MESSAGES
  byte g_checkin4HourTimer; // WLS-476: 1145: autonomously generate an "I'm alive" message to the RCVR every 4 hours
#endif

/*******************************************************************************
EXTERNAL REFERENCES   Only used if not available in a header file.
*******************************************************************************/
/*----data declarations-------------------------------------------------------*/

extern ZoneFlags myZoneFlags;
/*----function prototype------------------------------------------------------*/

/*******************************************************************************
PRIVATE DECLARATIONS
*******************************************************************************/
/*----defines, typedefs, structs, unions, enums-------------------------------*/
// it takes this many transmits at low battery before low battery is shown
enum {LOW_BATTERY_COUNTER_THRESHHOLD = 3};

typedef enum
{
  DB_IDLE = 0,     //default phase after powerup
  DB_INITIAL,
  DB_SHORT_PRESS,
  DB_LONG_PRESS,   //for extended presses
  DB_RELEASE,
} DebouncePhase;

typedef enum
{
  FALLING_EDGE,
  RISING_EDGE
} EdgeType;

typedef struct
{
  BITFIELD lowBattery : 1;
  BITFIELD counter    : 7;
} LowBatteryState;

typedef struct
{
  BITFIELD                : 1;
  BITFIELD receiveCommand : 1;
  BITFIELD reserved       : 6;
} InputStateChangeStruct;

// button statuses
enum
{
  BUTTON_JUST_PRESSED,                // button just pressed
  BUTTON_RELEASE,
};

#define Q_ELEMENT_IN_USE   CS_CHECKIN  // reuse the checkin bit of the contact state byte to save RAM
#define MSG_IS_EMPTY       CS_CHECKIN

// The low power timer runs every 8 seconds (low power --> sleeping)
#define LPT_4_HOURS (4 * 3600 / 8)

static DebouncePhase debouncePhase;
static word lowPowerTimer;

/*----data declarations-------------------------------------------------------*/
static ContactState pressedButton; // updated in an interrupt when a North, South, East, or West button is pressed (only the first, if there are other simultaneous button presses)
static ContactState messageQueue[Q_NUM_ELEMENTS];

#ifdef SEND_I_AM_ALIVE_MESSAGES
  static ContactState aliveMessage;
#endif

LowBatteryState lowBatteryState;

/*----function prototype------------------------------------------------------*/
static void ManageButtonDebounce(int buttonStatus);
static void enableFallingEdgeInterrupt(ContactState pressedButton);
static void ProcessDebounces(void);
/*----macros------------------------------------------------------------------*/

/*******************************************************************************
PRIVATE FUNCTION DEFINITIONS
*******************************************************************************/
/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
ContactState readCurrentContactState(ContactState buttonsToProcess)
{
  ContactState currentState = (ContactState)0;
  if ((buttonsToProcess & CS_NORTH_BUTTON) && !getIO(NORTH_BUTTON))
    currentState |= CS_NORTH_BUTTON;
  if ((buttonsToProcess & CS_SOUTH_BUTTON) && !getIO(SOUTH_BUTTON))
    currentState |= CS_SOUTH_BUTTON;
#ifndef DEBUG_EAST_IS_OUTPUT
  if ((buttonsToProcess & CS_EAST_BUTTON) && !getIO(EAST_BUTTON))
    currentState |= CS_EAST_BUTTON;
#endif
  if ((buttonsToProcess & CS_WEST_BUTTON) && !getIO(WEST_BUTTON))
    currentState |= CS_WEST_BUTTON;

   return currentState;
}

/*******************************************************************************

DESCRIPTION: This fcn is called after detecting a button press or release (via an interrupt).
  It disables all 4 buttons from causing additional interrupts until the
  debounce is handled for the first button that was sensed.

NOTES:

*******************************************************************************/
static void disableButtonInterruptsStartDebounce(ContactState button, EdgeType edge)
{
  P1IE &= ~(WEST_BUTTON_MASK);
  P2IE &= ~(NORTH_BUTTON_MASK | SOUTH_BUTTON_MASK | EAST_BUTTON_MASK);
  pressedButton = button; // this is used later (after the debounce) to specify which button was debounced (ignore additional, simultaneous button presses)
  // If a button has been released (rising edge), don't set the debounce phase to DB_INITIAL phase, since this could
  // be additional bounces or spurious activity.
  if (edge == FALLING_EDGE)
  {
    ManageButtonDebounce(BUTTON_JUST_PRESSED);
  }
  else
  {
    ManageButtonDebounce(BUTTON_RELEASE);
  }
}


/*******************************************************************************

DESCRIPTION: This function enqueues messages based on their type.

NOTES:

*******************************************************************************/
static void enqueueKeyFobMessage(EnqueuedMsgType enqueuedMsgType, ContactState message)
{
  switch (enqueuedMsgType)
  {
    case ENQUEUE_SHORT_PRESS:
      if (message)
      {
        // at least one button is pressed
        messageQueue[Q_BUTTON_PRESS] = (ContactState)(message | Q_ELEMENT_IN_USE);
        messageQueue[Q_BUTTON_RELEASE] = (ContactState)0;  // remove button release if in queue
      }
      else
      {
        // button release
        messageQueue[Q_BUTTON_RELEASE] = Q_ELEMENT_IN_USE;  // all contacts are 0
      }
      break;

    case ENQUEUE_EXTENDED_PRESS:
      messageQueue[Q_BUTTON_PRESS] = (ContactState)(message | Q_ELEMENT_IN_USE);
      messageQueue[Q_BUTTON_RELEASE] = (ContactState)0;  // remove button release if in queue
      break;
      
    case ENQUEUE_AUTO_GEN_MSG:
      messageQueue[Q_AUTO_GEN] = (ContactState)(message | Q_ELEMENT_IN_USE);
      break;
  }
  
  UpdateLatch();  // if nothing is latched, cause a queue value to be latched now.
}


/*******************************************************************************

DESCRIPTION: This function dequeues messages based on their type.

NOTES:

*******************************************************************************/
static InputState_t dequeueKeyFobMessage(void)
{
  InputState_t state;
  
  for (int i = 0; i < Q_NUM_ELEMENTS; i++)  // start at index 0 (highest priority)
  {
    state.msg = messageQueue[i];
    if (state.msg & Q_ELEMENT_IN_USE)
    {
      state.msg &= ~Q_ELEMENT_IN_USE; // clear the in-use flag
      state.msgType = (Q_ElementType_t)i;
      messageQueue[i] = (ContactState)0; // not in use anymore
      return state;
    }
  }
  
  state.msg = MSG_IS_EMPTY;
  state.msgType = Q_INVALID;
  return state;
}

/*******************************************************************************

DESCRIPTION: CallBack_ShortPressTimer
This is a timer callback fcn that updates button debounce processing.

NOTES:

*******************************************************************************/
static void CallBack_ShortPressTimer(void)
{
  ProcessDebounces();  // Call debounce state machine.

  if (IsLatchedMessage())
  {
    startHopTimer(SIXTEENTH_SECOND); // a small delay; send the message after aligning to a hop.
  }
}


/*******************************************************************************

DESCRIPTION: CallBack_ExtendedPressTimer
This is a callback fcn that tests the button to see if
it's still pressed after the long press timer expires.

NOTES:

*******************************************************************************/
static void CallBack_ExtendedPressTimer(void)
{
  if (debouncePhase == DB_LONG_PRESS)
  {
    ProcessDebounces();
    if (IsLatchedMessage())
    {
      startHopTimer(SIXTEENTH_SECOND); // a small delay; send the message after aligning to a hop.
    }
  }
  setupInputs(); // enable all 4 buttons' interrupts.
}


/*******************************************************************************

DESCRIPTION: ManageButtonDebounce

NOTES:  Called when a button's GPIO changes and triggers an interrupt
to handle presses and releases.

*******************************************************************************/
static void ManageButtonDebounce(int buttonStatus)
{
  switch (buttonStatus)
  {
    case BUTTON_JUST_PRESSED:
      // newly pressed button, so go to initial phase, etc.
      debouncePhase = DB_INITIAL;

      // Start short press timer
      StartTimer(DEBOUNCE_SHORT_PRESS_TIMER, TIME_250_mSec, CallBack_ShortPressTimer, TIMER_CONTINUOUS); // keep timer running for 2nd half of initial debounce (continuous)

      // Also, start a timer for an extended button press
      StartTimer(DEBOUNCE_LONG_PRESS_TIMER, TIME_2_Sec, CallBack_ExtendedPressTimer, TIMER_ONE_SHOT);

      // NOTE: The timers above will continue to run regardless of the value of currentlyCommunicating.
      // This is a significant change from before.
      break;

    case BUTTON_RELEASE:
      // button 'releases' are expected during the initial debounce phase (they're actually 'bounces').
      if (debouncePhase == DB_INITIAL)
      {
        // 'bounce' so restart timers as if button was just pressed
        StartTimer(DEBOUNCE_SHORT_PRESS_TIMER, TIME_250_mSec, CallBack_ShortPressTimer, TIMER_CONTINUOUS);
        StartTimer(DEBOUNCE_LONG_PRESS_TIMER, TIME_2_Sec, CallBack_ExtendedPressTimer, TIMER_ONE_SHOT);
      }
      else
      {
        // button was released
        debouncePhase = DB_RELEASE;
        ProcessDebounces();
      }
      setupInputs(); // enable all 4 buttons' interrupts.
      break;
      
    default:
      break;
  }
}

/*******************************************************************************

DESCRIPTION:

NOTES: This assumes that any edge interrupts on this port are for alarm contacts

*******************************************************************************/
extern void rfDataClockEdge(void);
#ifdef __ICC430__
#pragma vector = PORT1_VECTOR
#pragma type_attribute = __interrupt
#endif
void port1EdgeInterrupt(void)
#ifndef __ICC430__
__interrupt[PORT1_VECTOR]
#endif
{  
  // first check for rf clock edge
  if (getIOIntFlag(RF_DATA_CLK))
  {
    clearIOIntFlag(RF_DATA_CLK);
    rfDataClockEdge();
  }
  else if (getIOIntFlag(WEST_BUTTON))
  {
    clearIOIntFlag(WEST_BUTTON);

    if (getIOIntEdge(WEST_BUTTON))
      disableButtonInterruptsStartDebounce(CS_WEST_BUTTON, FALLING_EDGE);
    else
      disableButtonInterruptsStartDebounce(CS_WEST_BUTTON, RISING_EDGE);
      
  }
  else
  {
    P1IFG = 0;  // disable all interrupt flags
    P1IE &= (WEST_BUTTON_MASK | RF_DATA_CLK_MASK);  // clear all other edge interrupts
  }
}

#ifdef DMP_1100_PRODUCTION_TEST
/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/

void clearContactState(void)
{
  if(contactSteadyState == (ContactState)0)
  {
    contactSteadyState |= (ContactState)CS_NORTH_BUTTON;
    contactSteadyState |= (ContactState)CS_SOUTH_BUTTON;
    contactSteadyState |= (ContactState)CS_EAST_BUTTON;
    contactSteadyState |= (ContactState)CS_WEST_BUTTON;
  }
  else
  {
    contactSteadyState = (ContactState)0;
  }
}
#endif
/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/

#ifdef __ICC430__
#pragma vector = PORT2_VECTOR
#pragma type_attribute = __interrupt
#endif
void port2EdgeInterrupt(void)
#ifndef __ICC430__
__interrupt[PORT2_VECTOR]
#endif
{
  if( getIOIntFlag(NORTH_BUTTON))
  {
    clearIOIntFlag(NORTH_BUTTON);

    if (getIOIntEdge(NORTH_BUTTON))
      disableButtonInterruptsStartDebounce(CS_NORTH_BUTTON, FALLING_EDGE);
    else
      disableButtonInterruptsStartDebounce(CS_NORTH_BUTTON, RISING_EDGE);
  }
  else if (getIOIntFlag(SOUTH_BUTTON))
  {
    clearIOIntFlag(SOUTH_BUTTON);

    if (getIOIntEdge(SOUTH_BUTTON))
      disableButtonInterruptsStartDebounce(CS_SOUTH_BUTTON, FALLING_EDGE);
    else
      disableButtonInterruptsStartDebounce(CS_SOUTH_BUTTON, RISING_EDGE);
  }
  else if (getIOIntFlag(EAST_BUTTON))
  {
    clearIOIntFlag(EAST_BUTTON);
  #ifndef DEBUG_EAST_IS_OUTPUT
    if (getIOIntEdge(EAST_BUTTON))
      disableButtonInterruptsStartDebounce(CS_EAST_BUTTON, FALLING_EDGE);
    else
      disableButtonInterruptsStartDebounce(CS_EAST_BUTTON, RISING_EDGE);
  #endif
  }
  else  // should not occur
  {
    P2IFG = 0;        // clear all edge interrupts
    P2IE &= (NORTH_BUTTON_MASK | SOUTH_BUTTON_MASK | EAST_BUTTON_MASK); // disable all other interrupts
  }
}

/*******************************************************************************

DESCRIPTION:  This timer interrupt is used in conjunction with
startHopTimer()

NOTES:

*******************************************************************************/
#ifdef __ICC430__
#pragma vector = TIMERA0_VECTOR
#pragma type_attribute = __interrupt
#endif
void hopTimerInterrupt(void)
#ifndef __ICC430__
__interrupt[TIMERA0_VECTOR]
#endif
{
  TACCTL0 = 0; // stop timer

  if (!getCurrentlyCommunicating() && IsLatchedMessage())
  {
     if (wakeup(true, GetLatchedContactReceiveCommand()))
        __low_power_mode_off_on_exit();
  }
}



/*******************************************************************************

DESCRIPTION: EnqueueButtonRelease

NOTES:
Enqueues a button release and cancels short and long press timers
*******************************************************************************/
static void EnqueueButtonRelease(void)
{
  // button was released so cancel short and long press timers
  CancelTimer(DEBOUNCE_SHORT_PRESS_TIMER);
  CancelTimer(DEBOUNCE_LONG_PRESS_TIMER);

  debouncePhase = DB_IDLE;

  // enqueue release
  enqueueKeyFobMessage(ENQUEUE_SHORT_PRESS, (ContactState)0);
}



/*******************************************************************************

DESCRIPTION: ProcessDebounces

NOTES:

*******************************************************************************/
static void ProcessDebounces(void)
{
  ContactState currentState;

  switch (debouncePhase)
  {
    // DB_IDLE:         nothing to do
    // DB_INITIAL:      a 250 mSec timer is used to allow key bounces to settle out.
    // DB_SHORT_PRESS:  process the remaining 250 mSec of the initial 500 mSec debounce
    // DB_LONG_PRESS:   process an extended press
    // DB_RELEASE:      enqueues a button release message

    case DB_IDLE:
      currentState = readCurrentContactState((ContactState)CS_ALL_BUTTONS);
      setupInputs(); // enable all 4 buttons' interrupts.
      break;

    case DB_INITIAL:
      // if the expected button is still pressed after 250 mSec
      if (readCurrentContactState(pressedButton) != 0)
      {
        // At this point, all 4 buttons still should have their edge interrupts disabled.
        // Now, since the 1 button we're debouncing is still pressed after 250 mSec, reenable
        // the falling edge interrupt for that button.  If it runs, then it indicates there was another
        // press (or bounce) during the secondary phase, therefore it will invalidate the button press.
        // This is being done, because it has been observed that spurious noise (due to plugging in a fan,
        // or a panel, for example) has created false edges on the button pins. RF energy in very close
        // proximity has also created false edges.
        enableFallingEdgeInterrupt(pressedButton);  // if falling edge detected, restart entire debounce cycle
      
        // Note: button releases are handled as before (the debounce timer interrupt processes them).
      
        debouncePhase = DB_SHORT_PRESS; // advance to the next debounce phase
        // Start short press timer for remainder of debounce.
        StartTimer(DEBOUNCE_SHORT_PRESS_TIMER, TIME_250_mSec, CallBack_ShortPressTimer, TIMER_ONE_SHOT);
      }
      else
      {
        // The expected button wasn't still pressed after the initial 250 mSec expired.
        setupInputs(); // enable all 4 buttons' interrupts.
      }
      break;
    
    case DB_SHORT_PRESS:
      // 500 mS elapsed from the first button press edge, or a button was released after a valid press occurred.
      currentState = readCurrentContactState(pressedButton); // (limited to one button at a time, to properly handle debounce interrupts and timing)

      // if the expected button is still pressed then enqueue a short press, else enqueue a release
      if (currentState & pressedButton)
      {
        // 'or' in any other buttons that are currently pressed (needed for multi-press Emerg 2, Panic 2)
        ContactState contactSteadyStateAllButtons = (ContactState)(currentState | readCurrentContactState((ContactState)CS_ALL_BUTTONS));

        // enqueue a short press
        enqueueKeyFobMessage(ENQUEUE_SHORT_PRESS, contactSteadyStateAllButtons);
        debouncePhase = DB_LONG_PRESS; // advance to the long press debounce phase
      }
      else
      {
        EnqueueButtonRelease();
      }

      setupInputs(); // enable all 4 buttons' interrupts.
      break;

    case DB_LONG_PRESS:
      currentState = readCurrentContactState(pressedButton); // (limited to one button at a time, to properly handle debounce interrupts and timing)
      
      // if the expected button is still pressed, then it's an extended press, else it's a release
      if (currentState & pressedButton)
      {
        // 'or' in any other buttons that are currently pressed (needed for multi-press Emerg 2, Panic 2)
        ContactState contactSteadyStateAllButtons = (ContactState)(CS_EXTENDED_BUTTON | pressedButton | readCurrentContactState((ContactState)CS_ALL_BUTTONS));

        // enqueue a long (extended) press
        enqueueKeyFobMessage(ENQUEUE_EXTENDED_PRESS, contactSteadyStateAllButtons);
      }
      else
      {
        EnqueueButtonRelease();
      }

      setupInputs(); // enable all 4 buttons' interrupts.
      break;

    case DB_RELEASE:
      currentState = readCurrentContactState((ContactState)CS_ALL_BUTTONS);
      EnqueueButtonRelease();
      setupInputs(); // enable all 4 buttons' interrupts.
      break;

    default:
      setupInputs(); // enable all 4 buttons' interrupts.
      break;
  }
}

/*******************************************************************************
PUBLIC FUNCTION DEFINITIONS
*******************************************************************************/

/*******************************************************************************

DESCRIPTION: This function tests the queue to see if it's empty.

NOTES:

*******************************************************************************/
bool IsQueueEmpty(void)
{
  for (int i = 0; i < Q_NUM_ELEMENTS; i++)
  {
    if (messageQueue[i] & Q_ELEMENT_IN_USE)
    {
      return false;
    }
  }
  
  return true;
}


/*******************************************************************************

DESCRIPTION: If the queue contains a long button press, remove it from
the queue and return it, otherwise return 0.

NOTES:

*******************************************************************************/
ContactState DequeueKeyFobMessageIfLongPress(void)
{
  if ((messageQueue[Q_BUTTON_PRESS] & Q_ELEMENT_IN_USE) && (messageQueue[Q_BUTTON_PRESS] & CS_EXTENDED_BUTTON))
  {
    ContactState msg = (ContactState)(messageQueue[Q_BUTTON_PRESS] & ~Q_ELEMENT_IN_USE); // clear the in-use flag;
    messageQueue[Q_BUTTON_PRESS] = (ContactState)0; // not in use anymore
    return msg;
  }
  
  return (ContactState)0;
}


/*******************************************************************************

DESCRIPTION: If the queue contains a button press, return true.
Otherwise return false.

NOTES:

*******************************************************************************/
bool ButtonPressIsInQueue(void)
{
  if (messageQueue[Q_BUTTON_PRESS] & Q_ELEMENT_IN_USE)
    return true;

  return false;
}


/*******************************************************************************

DESCRIPTION: When entering deep sleep, empty the queue.

NOTES:

*******************************************************************************/
void EmptyQueue(void)
{
  for (int i = 0; i < Q_NUM_ELEMENTS; i++)
  {
    messageQueue[i] = (ContactState)0;
  }
}


/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
void setupInputs(void)
{
  clearIOEnableInt(NORTH_BUTTON);
  clearIOEnableInt(SOUTH_BUTTON);
  clearIOEnableInt(EAST_BUTTON);
  clearIOEnableInt(WEST_BUTTON);

  if(getIO(NORTH_BUTTON))
  {
    setIOFallingInt(NORTH_BUTTON);
  }
  else
  {
    setIORisingInt(NORTH_BUTTON);
  }
  
  if(getIO(SOUTH_BUTTON))
  {
    setIOFallingInt(SOUTH_BUTTON);
  }
  else
  {
    setIORisingInt(SOUTH_BUTTON);
  }
  
  if(getIO(EAST_BUTTON))
  {
    setIOFallingInt(EAST_BUTTON);
  }
  else
  {
    setIORisingInt(EAST_BUTTON);
  }
  
  if(getIO(WEST_BUTTON))
  {
    setIOFallingInt(WEST_BUTTON);
  }
  else
  {
    setIORisingInt(WEST_BUTTON);
  }
  
  clearIOIntFlag(NORTH_BUTTON);
  clearIOIntFlag(SOUTH_BUTTON);
  clearIOIntFlag(EAST_BUTTON);
  clearIOIntFlag(WEST_BUTTON);

  setIOEnableInt(NORTH_BUTTON);
  setIOEnableInt(SOUTH_BUTTON);
  #ifndef DEBUG_EAST_IS_OUTPUT
    setIOEnableInt(EAST_BUTTON);
  #endif
  setIOEnableInt(WEST_BUTTON);
}

/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
void initInputs(void)
{
  setIODirIn(NORTH_BUTTON);
  setIODirIn(SOUTH_BUTTON);
#ifdef DEBUG_EAST_IS_OUTPUT
  setIODirOut(EAST_BUTTON);
#else
  setIODirIn(EAST_BUTTON);
#endif
  setIODirIn(WEST_BUTTON);
  setIO(NORTH_BUTTON);
  setIO(SOUTH_BUTTON);
  setIO(EAST_BUTTON);
  setIO(WEST_BUTTON);
  enableIOResistor(NORTH_BUTTON);
  enableIOResistor(SOUTH_BUTTON);
  enableIOResistor(EAST_BUTTON);
  enableIOResistor(WEST_BUTTON);
  enableIOResistor(SO_GDO1); // The CC1100 will tri-state this pin when chip select is not asserted.

  setupInputs();

  setIODirOut(BATT_VOLTAGE_ENABLE);
  clearIOSel(BATT_VOLTAGE_ENABLE);
  setIODirIn(BATTERY_VOLTAGE);
  CACTL1 = CARSEL | CAREF_2 | CAON;
  CACTL2 = P2CA0 | CAF;
  CAPD = BATTERY_VOLTAGE_MASK;
}

/*******************************************************************************

DESCRIPTION:  Starts timer so it will interrupt at delayTime plus
  the amount of time for it to start at an appropriate hop

NOTES:  If SLEEP_TICKS_PER_HOP ever becomes a value that is not a power of two, the
  modulo operator will take much longer and this will probably need to change

*******************************************************************************/
void startHopTimer(short delayTime)
{
  // If we're not sleeping, do nothing, since we're already awake and presumably already hopping.
  if (!getSleeping())
    return;
  
  if (! (TACCTL0 & CCIE))               // if timer is not already in use
  {
    unsigned short tempTar = TAR;
    TACCR0 = tempTar + delayTime +      // current time plus delayTime plus
             SLEEP_TICKS_PER_HOP -      // an extra hop minus
             WAKEUP_CLOCK_SLOP_TICKS -  // some padding for clock slop minus
             ((tempTar - sleepTime) % SLEEP_TICKS_PER_HOP); // how far off we currently are from start of hop
  }
  TACCTL0 = CCIE;                       // enable interrupt and set compare mode
}

/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
void testBattery(void)
{

  if (CACTL2 & CAOUT)
  {
    if (lowBatteryState.counter < LOW_BATTERY_COUNTER_THRESHHOLD)
      lowBatteryState.counter++;
    if (lowBatteryState.counter >= LOW_BATTERY_COUNTER_THRESHHOLD)
      lowBatteryState.lowBattery = true;
  }
  else      // a single "good" resets everything
  {
    lowBatteryState.counter = 0;
    lowBatteryState.lowBattery = false;
  }
}

/*******************************************************************************

DESCRIPTION:  This function gets the next message from the queue (if any)
and inserts low battery status. It resets retry variables for each new message.

NOTES:

*******************************************************************************/
InputState_t getInputState(void)
{
  InputState_t state;
  state = dequeueKeyFobMessage();

  if (state.msg == MSG_IS_EMPTY)
  {
    state.msg = 0;
  }
  else
  {
    EnableFullRetries(); // Needed in order to allow full wakeup attempts again, if it maxed out earlier.
  }
  
  if (lowBatteryState.lowBattery)
  {
    state.msg |= CS_LOW_BATTERY;
  }

  return state;
}


/*******************************************************************************

DESCRIPTION: During the secondary phase of a button debounce, the falling edge
  interrupt for just the button being debounced is enabled. Normally, the button
  has stopped bouncing by this time, so any new falling edges are an
  indicator that there may be spurious edges on the pin caused by noise.

  If the falling edge occurs, the ISR will cause the whole debounce process to start
  completely over (initial phase, 250 mSec timer, all buttons' interrupts
  disabled).

  Note that rising edges are normal and expected when the button is released.

NOTES:

*******************************************************************************/
static void enableFallingEdgeInterrupt(ContactState pressedButton)
{
  if (pressedButton & CS_NORTH_BUTTON)
  {
    clearIOEnableInt(NORTH_BUTTON);
    setIOFallingInt(NORTH_BUTTON);
    clearIOIntFlag(NORTH_BUTTON);
    setIOEnableInt(NORTH_BUTTON);
  }
  else if (pressedButton & CS_SOUTH_BUTTON)
  {
    clearIOEnableInt(SOUTH_BUTTON);
    setIOFallingInt(SOUTH_BUTTON);
    clearIOIntFlag(SOUTH_BUTTON);
    setIOEnableInt(SOUTH_BUTTON);
  }
  else if (pressedButton & CS_EAST_BUTTON)
  {
  #ifndef DEBUG_EAST_IS_OUTPUT
    clearIOEnableInt(EAST_BUTTON);
    setIOFallingInt(EAST_BUTTON);
    clearIOIntFlag(EAST_BUTTON);
    setIOEnableInt(EAST_BUTTON);
  #endif
  }
  else if (pressedButton & CS_WEST_BUTTON)
  {
    clearIOEnableInt(WEST_BUTTON);
    setIOFallingInt(WEST_BUTTON);  
    clearIOIntFlag(WEST_BUTTON);
    setIOEnableInt(WEST_BUTTON);
  }
}

/*******************************************************************************

DESCRIPTION:  NMI interrupt is run if there's an oscillator fault.

NOTES:

*******************************************************************************/
#ifdef __ICC430__
#pragma vector = NMI_VECTOR
#pragma type_attribute = __interrupt
#endif
void NMIInterrupt(void)
#ifndef __ICC430__
__interrupt[NMI_VECTOR]
#endif
{
  /*
   * When the WDT+ is configured to operate in watchdog mode, either writing to WDTCTL with an
   * incorrect password, or expiration of the selected time interval triggers a PUC (Power Up Clear).
   */
  KICK_WATCHDOG();  // start watchdog, reset, and use ACLK
  FORCE_WATCHDOG_TIMEOUT();

  // we shouldn't get here since the watchdog should have already triggered a PUC  
  while(1)
  {
    __no_operation();
  }
}

/*******************************************************************************

DESCRIPTION:  Watchdog timer INTERVAL interrupt is run when the MSP430 is in
  low power mode (the watchdog is disabled). It runs every 8 seconds.

NOTES:

*******************************************************************************/
#ifdef __ICC430__
#pragma vector = WDT_VECTOR
#pragma type_attribute = __interrupt
#endif
void WDTimerInterrupt(void)
#ifndef __ICC430__
__interrupt[WDT_VECTOR]
#endif
{
  // This interrupt runs every 8 seconds (ACLK with a div by 8).
  // Note: The flag (WDTIFG) is cleared automatically.

  if (++lowPowerTimer >= LPT_4_HOURS)
  {
    resetLowPowerTimer();

  #ifdef SEND_I_AM_ALIVE_MESSAGES // special release to certain customers
    // WLS-476: 1145: autonomously generate an "I'm alive" message to the RCVR every 4 hours

    // Report battery condition (low or good) every 4 hours without requiring a button press.
    // The actual message sent depends on the value of lowBatteryState.lowBattery.
  
    // Since the wireless receiver latches the last message received by a transmitter,
    // and discards messages that are the same, then a message that changes each time will be seen by the panel.
    // Don't set more than one bit at a time since this can falsely cause a 2-button panic
    // to be interpreted by panels that don't have the latest code which supports the
    // 'alive' message.
    aliveMessage <<= 1; // walk bit through the 4 bit value (these 4 bits are the ones for the 4 buttons and are the 4 LS bits)
    aliveMessage &= CS_ALL_BUTTONS; // mask if overflow
    if (!aliveMessage)
      aliveMessage = (ContactState)1;  // value is 0 so set LS bit
    aliveMessage |= CS_FOB_IS_ALIVE;  // set bit to indicate this is an 'alive' message.

    enqueueKeyFobMessage(ENQUEUE_AUTO_GEN_MSG, aliveMessage);
    g_checkin4HourTimer = MINUTES_IN_FOUR_HOURS; // reset checkin 4 hour timer, since we're now attempting to send a message from the deep sleep 4 hour timer

    if (wakeup(true, GetLatchedContactReceiveCommand()))
      __low_power_mode_off_on_exit();

    // WLS-476: 1145: autonomously generate an "I'm alive" message to the RCVR every 4 hours
    // This is the deep sleep case. Since the 4 hour timer running in deep sleep causes a message
    // with the battery condition to be sent (see above), there's no extra processing
    // to do here.

  #else // Normal code
    
    //
    // If the battery is low, report it, else good, test it.
    //
    if (lowBatteryState.lowBattery)
    {
      // report low battery without requiring a button press
      enqueueKeyFobMessage(ENQUEUE_AUTO_GEN_MSG, CS_LOW_BATTERY);  // enqueue a message to report low battery
      if (wakeup(true, GetLatchedContactReceiveCommand()))
        __low_power_mode_off_on_exit();
    }
    else
    {
      TXandTestBattery();
      // if battery is low now, don't wait another 4 hours. Report it on the next 8 second interrupt.
      if (lowBatteryState.lowBattery)
        lowPowerTimer = LPT_4_HOURS - 1;
    }
    
  #endif
    
  }
  else
  {
    // 4 hour timer did not expire. If we're sleeping when there's a message to send, wakeup and send it
    if (IsLatchedMessage())
    {
      if (wakeup(true, GetLatchedContactReceiveCommand()))
        __low_power_mode_off_on_exit();
    }
  }

  if ((lowPowerTimer & 0x000F) == 0)  // 16 * 8 = 128 Sec = 2 Min 8 Sec
  {
    sendChipconCommandStrobe(SPWD);   // send the command to place the Chipcon device in powerdown mode
  }
  
  if (debouncePhase == DB_IDLE)
  {
    ProcessDebounces();  // refresh / reinit
  }
}

/*******************************************************************************

DESCRIPTION:  The low power timer can measure many hours. It's used
  for initiating transmissions for periodic battery tests. It's also
  used for shorter duration activities, such as refreshing commands
  sent to the CC1100 to ensure it's in a powerdown state.

NOTES:

*******************************************************************************/
void resetLowPowerTimer()
{
  // choose 1, so the test for the LS 4 bits being 0000 later doesn't cause
  // the periodic refresh to artificially run faster than normal.
  lowPowerTimer = 1;
}

/*******************************************************************************

DESCRIPTION:

NOTES:
*******************************************************************************/
bool isBatteryGood()
{
  return !lowBatteryState.lowBattery;
}



/*******************************************************************************

DESCRIPTION: triggerSendingAliveMessage sets up a delayed transmission of
  the special "I'm Alive" message. When the 4 hour checkin timer (which is
  different than the 4 hour low power timer) expires, this fcn is called.
  The checkin slot can be used for normal checkin messages since this message
  is delayed to an alarm slot.

NOTES:
*******************************************************************************/
#ifdef SEND_I_AM_ALIVE_MESSAGES
void triggerSendingAliveMessage(void)
{
  lowPowerTimer = LPT_4_HOURS - 1; // set the 4 hour sleep timer to expire in 8 seconds
}
#endif
