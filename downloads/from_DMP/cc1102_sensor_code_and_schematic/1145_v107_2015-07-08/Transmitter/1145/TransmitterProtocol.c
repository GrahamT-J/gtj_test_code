/*******************************************************************************
FILENAME: TransmitterProtocol.c

DESCRIPTION:

NOTES:

Copyright (c) Digital Monitoring Products Inc. 2004-2013.  All rights reserved.

*******************************************************************************/
/*----compilation control-----------------------------------------------------*/

/*******************************************************************************
INCLUDE FILES
*******************************************************************************/
/*----system and platform files (external libraries) -------------------------*/

/*----program files-----------------------------------------------------------*/
#include "types.h"
#include "Sleep.h"
#include "TransmitterProtocol.h"
#include "WirelessCommunication.h"
#include "Inputs.h"
#include "ioportpins.h"



/*******************************************************************************
EXTERNAL REFERENCES   Only used if not available in a header file.
*******************************************************************************/
/*----data declarations-------------------------------------------------------*/
extern SlotNumber currentSlotNumber;
#ifdef SIMULATE_HEAVY_ACK_LOSS
  extern int debugAckDiscardCount;
#endif

/*----function prototype------------------------------------------------------*/


/*******************************************************************************
PRIVATE DECLARATIONS
*******************************************************************************/
/*----defines, typedefs, structs, unions, enums-------------------------------*/
#define SOFTWARE_VERSION 2

typedef enum
{
  TMT_SERIAL_MESSAGE,
  TMT_ZONE_MESSAGE,
  TMT_CHECKIN_MESSAGE,
  TMT_COMMAND_ACK_SETUP,
  TMT_NO_MESSAGE
} TransmitterMessageType;

/*----data declarations-------------------------------------------------------*/

AckStatus ackStatus = {.counter = 0};

/*----function prototype------------------------------------------------------*/
void formatTransmitterMessage(TransmitterMessageType messageType);

static SlotTransmitterNumber myZoneNumber = INVALID_TRANSMITTER_NUMBER;
static SlotNumber myCheckinSlotNumber = INVALID_SLOT;
static unsigned short transmitterCrc;
static bool latchedStatus;

// For FOB, initial latched message value of 0 is 'no buttons pressed', 
// therefore the 1st call to UpdateLatchedStatus won't latch 0 (since same value).
static InputState_t latchedState = {0, Q_INVALID};

#ifdef DMP_1142_TRANSMITTER
ZoneFlags myZoneFlags;
#endif

/*----macros------------------------------------------------------------------*/

/*******************************************************************************
PRIVATE FUNCTION DEFINITIONS
*******************************************************************************/
/*******************************************************************************

DESCRIPTION: Sets CRC for transmitter message, including any extra
information that is expected, and sets transmitter packet length

NOTES:

*******************************************************************************/
static void finalizeTransmitterMessage(char size, AdditionalCheckItems items)
{
  unsigned short crc = INITIAL_CRC;
  // first, start with any additional items
  if (items & ACI_HOUSE_CODE)
  {
    calcCRC16ForChar(houseCode, &crc);
  }
  if (items & ACI_ZONE_NUMBER)
  {
    calcCRC16ForChar(myZoneNumber >> 8, &crc);
    calcCRC16ForChar(myZoneNumber & 0xFF, &crc);
  }
  if (items & ACI_SERIAL_NUMBER)
  {
    calcCRC16ForChar(serialNumber & 0xFF, &crc);
    calcCRC16ForChar((serialNumber >> 8) & 0xFF, &crc);
    calcCRC16ForChar((serialNumber >> 16) & 0xFF, &crc);
  }
  encodeWirelessBuffer(transmitterPacketBuffer, size, crc);
  transmitterCrc = (transmitterPacketBuffer[size - 1] << 8) | transmitterPacketBuffer[size - 2];
  transmitterPacketSize = size;
}
/*******************************************************************************

DESCRIPTION:  UpdateLatchedStatus

NOTES:  Set the latched status boolean to false if the passed status is false,
and the queue is empty, but don't alter any other latch variables.
Otherwise, update the latched value. See below for more details...
*******************************************************************************/
static void UpdateLatchedStatus(bool status, InputState_t state)
{
  if (status == false)
  {
    if (IsQueueEmpty())
    {
      InvalidateLatch();
      return;
    }
    else
    {
      // ignore dummy value passed into 'state' var when status is false, and get it from the queue instead.
      state = getInputState();
    }
  }

  
  // Set latchedStatus to true only if the new state is not invalid
  // and the latched state is different than the new state.
  // For example, if a short press of the top button is latched, and another short press of the same button is
  // in the queue, only one of them will be sent. The one in the queue will be ignored.

  if ((state.msgType == Q_INVALID) || ((state.msg == latchedState.msg) && (state.msgType == latchedState.msgType)))
  {
    InvalidateLatch();
    return;
  }
  else
  {
    // latch new value
    latchedStatus = true;
    latchedState = state;
    startHopTimer(SIXTEENTH_SECOND); // a small delay; send the message after aligning to a hop.
  }
}

/*******************************************************************************

DESCRIPTION:  GetLatchedInputState

NOTES:  If there is a latched input state (contact value) then return it,
otherwise return the current value of the inputs.

*******************************************************************************/
static ZoneMessageByte GetLatchedInputState(void)
{
  // If there is a latched value, get it
  // otherwise, read the current input state, and latch it.
  if (latchedStatus == false)
  {
    UpdateLatchedStatus(true, getInputState());
  }
  
  // If the latched msg is a short press message and if there's a long press in the queue,
  // 'jam' the long press on top of the short press.
  if ((latchedState.msgType == Q_BUTTON_PRESS) && !(latchedState.msg & CS_EXTENDED_BUTTON))
  {
    ContactState msg = DequeueKeyFobMessageIfLongPress();
    if (msg)
    {
      // replace short press msg with long press msg
      latchedState.msg = msg;
    }
  }
  
  #ifdef ENABLE_RECEIVER_COMMANDS
    if (latchedState.msgType == Q_BUTTON_PRESS)
    {
      RestartCommandReceive();  // enable and restore count to full, etc.
    }
  #endif

  return (ZoneMessageByte)latchedState.msg;
}


/*******************************************************************************
PUBLIC FUNCTION DEFINITIONS
*******************************************************************************/
/*******************************************************************************

DESCRIPTION:  UpdateLatch

NOTES:  This is called when an input changes in order to update the latch
immediately, rather than waiting for setupTransmitterMessage to cause
an update. Since the keyfob uses a queue to feed the latch, and since it
relies on the latch status more than the 1139, there is a need for this
more timely update.

*******************************************************************************/
void UpdateLatch(void)
{
  GetLatchedInputState();
}


/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
ReceivedAckStatus processReceivedAck(char* characterOffset)
{
  char bufferOffset = getWirelessReceiveBufferOffset();
  ReceivedAckStatus returnValue = RAS_NOTHING_TO_DO;
  ReceiverAck * const ackPointer = (ReceiverAck*)receiverPacketBuffer;

  if (bufferOffset >= sizeof(ReceiverAck))
  {
    switch (ackPointer->ackType)
    {
    case AT_NO_ACK:
      *characterOffset = sizeof(ReceiverNoAck);
      if ((houseCode == INVALID_HOUSE_CODE) || (houseCode == ackPointer->houseCode))   // test house code too, if we have a valid one
        returnValue = RAS_GOOD_MESSAGE_NON_ACK;
      break;

    case AT_STANDARD_ACK:
      *characterOffset = sizeof(ReceiverStandardAck);
      if (bufferOffset >= *characterOffset)
      {
        if (decodeWirelessBuffer(receiverPacketBuffer, sizeof(ReceiverStandardAck), transmitterCrc))
        {
          returnValue = RAS_GOOD_MESSAGE_ACK_RECEIVED;
          #ifdef DMP_1100_PRODUCTION_TEST
            clearContactState();
            startHopTimer();
          #endif
        }
        else
        {
          returnValue = RAS_BAD_MESSAGE;
        }
      }
      break;

    case AT_ACK_WITH_ZONE_NUMBER:
      *characterOffset = sizeof(ReceiverZoneNumberAck);
      if (bufferOffset >= *characterOffset)
      {
        ReceiverZoneNumberAck * const zoneAckPointer = (ReceiverZoneNumberAck*)receiverPacketBuffer;

        if (decodeWirelessBuffer(receiverPacketBuffer, sizeof(ReceiverZoneNumberAck), transmitterCrc) &&
            ((houseCode == INVALID_HOUSE_CODE) || (myZoneNumber == INVALID_TRANSMITTER_NUMBER)))
        {
          if (zoneAckPointer->found)
          {
            if (houseCode == INVALID_HOUSE_CODE)
            {
              houseCode = zoneAckPointer->houseCode;
            }
#ifdef DMP_1142_TRANSMITTER
            myZoneFlags.transmitterFlags = zoneAckPointer->zoneFlags;
#endif
            myZoneNumber = (SlotTransmitterNumber)(zoneAckPointer->zoneLow + (zoneAckPointer->zoneHigh << 8));
            myCheckinSlotNumber = zoneToSlotNumber(myZoneNumber);
            returnValue = RAS_GOOD_MESSAGE_ACK_RECEIVED;
            #ifdef DMP_1100_PRODUCTION_TEST
            clearContactState();
            startHopTimer();
            #endif
          }
          else
          {
            returnValue = RAS_GOOD_MESSAGE_WRONG_RECEIVER;
          }
        }
        else
        {
          returnValue = RAS_BAD_MESSAGE;
        }
      }
      break;

    case AT_VARIABLE_LENGTH_ACK:
      if (bufferOffset >= sizeof(ReceiverAck) + 1)
      {
        ReceiverVariableLengthAck * const reservedAckPointer = (ReceiverVariableLengthAck*)receiverPacketBuffer;
        *characterOffset = sizeof(ReceiverVariableLengthAck) - 1 + reservedAckPointer->numBytesInData;
        if (bufferOffset >= *characterOffset)
        {
          returnValue = RAS_BAD_MESSAGE;  // we don't know what the ack is, so it's bad
        }
      }
      break;

    default:
      returnValue = RAS_BAD_MESSAGE;
      break;
    }
  }
  if( returnValue == RAS_GOOD_MESSAGE_ACK_RECEIVED )
  {         //we have received a good ack so count it
    if( ackStatus.counter <= LED_ACK_COUNT )
    {
      ackStatus.counter++;
    }
  }

  return returnValue;
}
#ifdef DMP_1100_PRODUCTION_TEST
/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
void setzonenumber(char zone)
{
  myZoneNumber = (SlotZoneNumber)zone;
  return;
}
#endif

/*******************************************************************************

DESCRIPTION: Will decode and act upon a received Command message

NOTES:

*******************************************************************************/
void processReceivedCommand(unsigned char messageFormat, unsigned char commandByte)
{
  KeyfobFeedbackCommandStruct * command = (KeyfobFeedbackCommandStruct*)&commandByte;

  if (messageFormat == KEYFOB_FEEDBACK_COMMAND)
  {
    startLEDPulsing(command->flashCount, command->flashLength, command->redLED, command->greenLED);
  }
}

/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
TransmitterMessageStatus setupTransmitterMessage(char startOffset)
{
  char bufferOffset = getWirelessReceiveBufferOffset();
  ReceiverSlotStart* slotPointer;
  TransmitterMessageStatus status = TMS_NO_MESSAGE_SETUP;
  bool completeMessageReceived = false;
  bool alarmWakeup = getAlarmWakeup();
  bool receiveCommand = getCommandStatus();
  SlotNumber slotNumber = INVALID_SLOT;
  TransmitterMessageType  messageTypeToSend = TMT_NO_MESSAGE;
  bool inCheckinSlot;

  transmitterPacketSize = 0;

  if (bufferOffset >= sizeof(ReceiverNoMessageSlot) + startOffset)
  {
    slotPointer = ((ReceiverSlotStart*)(receiverPacketBuffer + startOffset));
    slotNumber = (SlotNumber)((slotPointer->slotNumberHigh << 8) | slotPointer->slotNumberLow);

    inCheckinSlot = slotNumber >= myCheckinSlotNumber &&
      slotNumber <= myCheckinSlotNumber + SLOTS_BETWEEN_CHECKIN_SLOTS;

    /* If the check in slot has type ST_GENERAL_ALARM, we have either gone
     * missing or our zone has been removed.
     */
    if (slotPointer->slotType == ST_GENERAL_ALARM && inCheckinSlot)
      resetLearnedInformation();

#ifdef SEND_I_AM_ALIVE_MESSAGES
    // if waking up in my checkin slot, and 4 hours have elapsed, trigger sending of "I'm Alive" message.
    if (inCheckinSlot && g_checkin4HourTimer && (--g_checkin4HourTimer == 0))
    {
      triggerSendingAliveMessage();
      g_checkin4HourTimer = MINUTES_IN_FOUR_HOURS; // restart the checkin 4 hour timer
    }
#endif

    if (isGeneralDataSlotNumber(slotNumber) && receiveCommand && !alarmWakeup)
    {
      if (isGeneralDataSlotType(slotPointer->slotType)) // if this is a general data slot with a general data slot type
      {
        if ( bufferOffset >= (sizeof(ReceiverCommandSlot) + startOffset))  // if we have received all the data
        {
          completeMessageReceived = true;
          if ( decodeWirelessBuffer(receiverPacketBuffer, sizeof(ReceiverCommandSlot) + startOffset, INITIAL_CRC) )
          {   // if we have a message with a correct CRC then process the data
            status = TMS_NO_MESSAGE_NEEDED;
            ReceiverCommandSlot * dataPointer = (ReceiverCommandSlot*)(receiverPacketBuffer + startOffset);
            SlotTransmitterNumber zone = (SlotTransmitterNumber)((dataPointer->zoneNumberLowByte) | (dataPointer->zoneNumberHighBits << 8));
            if(zone == myZoneNumber)
            {
              completeMessageReceived = true;  // mark that we have received a complete valid message
              processReceivedCommand(dataPointer->dataMessageFormat, dataPointer->command);
              messageTypeToSend = TMT_COMMAND_ACK_SETUP;
              status = TMS_MESSAGE_SETUP;
#ifdef ENABLE_RECEIVER_COMMANDS
              CancelCommandReceive();
#endif
            }
          }
        }
      }
      else
      {
        status = TMS_NO_MESSAGE_NEEDED;
        completeMessageReceived = true;
      }
    }
    else if (isAlarmSlotType(slotPointer->slotType))//if is an alarm slot we must communicate
    {
      char size = sizeof(ReceiverNoMessageSlot) + startOffset;
      if (isTwoByteAlarmSlotType(slotPointer->slotType))
      {
        size += 2;
      }
      if (bufferOffset >= size)
      {
        completeMessageReceived = true;

        // setup zone number or serial number alarm message
        if (decodeWirelessBuffer(receiverPacketBuffer, size, INITIAL_CRC))
        {
          // if not assigned a zone number, send serial number, otherwise send zone number
          if (myZoneNumber == INVALID_TRANSMITTER_NUMBER)
          {
            messageTypeToSend = TMT_SERIAL_MESSAGE;
            status = TMS_MESSAGE_SETUP;
          }
          else if (IsLatchedMessage())
          {
            messageTypeToSend = TMT_ZONE_MESSAGE;
            status = TMS_MESSAGE_SETUP;
          }
          else
          {
            status = TMS_NO_MESSAGE_NEEDED;
          }
        }
      }
    }
    else if (slotPointer->slotType == ST_CHECKIN_NEED_REPLY) //if this is our checkin slot and the receiver is calling for us to checkin
    {
      if (bufferOffset >= sizeof(ReceiverNoMessageSlot) + startOffset)
      {
        completeMessageReceived = true;
        if (decodeWirelessBuffer(receiverPacketBuffer, sizeof(ReceiverNoMessageSlot) + startOffset, INITIAL_CRC))
        {
          // make sure we are in the right slot
          if (inCheckinSlot)
          {
            messageTypeToSend = TMT_CHECKIN_MESSAGE;
            status = TMS_MESSAGE_SETUP;
          }
        }
      }
    }
          //if we are in our checkin slot and the slot type is called for us to update our programming
    else if (slotPointer->slotType == ST_CHECKIN_UPDATE_PROGRAMMING && inCheckinSlot)
    {
      messageTypeToSend = TMT_SERIAL_MESSAGE;
      myZoneNumber = INVALID_TRANSMITTER_NUMBER;
    }
    else
    {
      // we don't know what this message is, so say we received it, status alone
      completeMessageReceived = true;
    }

#ifdef SIMULATE_HEAVY_ACK_LOSS
if ((slotNumber >= myCheckinSlotNumber) &&
    (slotNumber <= myCheckinSlotNumber + SLOTS_BETWEEN_CHECKIN_SLOTS))
{
  // This is a checkin slot. Use it as a trigger to enable the heavy loss mode.
  if (debugAckDiscardCount == 0)
    --debugAckDiscardCount; // can go as low as -1, which enables 'waiting for ACK' debug code to run, which also alters debugAckDiscardCount
}
#endif

    if (messageTypeToSend != TMT_NO_MESSAGE)
    {
      formatTransmitterMessage(messageTypeToSend);
    }
  }

  if (completeMessageReceived == false)
  {
    status = TMS_STILL_WAITING;
  }
  else
  {
    currentSlotNumber = slotNumber;
  }
  return status;
}

/*******************************************************************************

DESCRIPTION: Will return the number of slots between the current Slot number and our
              Checkin slot.
NOTES:

*******************************************************************************/
SlotNumber SlotsTillMySlot(void)
{
  SlotNumber slotDifference = (myCheckinSlotNumber - currentSlotNumber);

  if (slotDifference <= 0)
  {
    slotDifference += TOTAL_SLOTS;
  }
  return slotDifference;
}

/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
SlotNumber SlotsTillDataSlot(void)
{
  SlotNumber numSlots = 0;

  if (currentSlotNumber < STARTING_DATA_SLOT)
  {
    numSlots = (STARTING_DATA_SLOT - currentSlotNumber);
  }
  else
  {
    numSlots = (currentSlotNumber - STARTING_DATA_SLOT) % DATA_SLOT_PERIOD;
    numSlots = DATA_SLOT_PERIOD - numSlots;
  }
  return (numSlots);
}


/*******************************************************************************

DESCRIPTION:

NOTES:  If SLEEP_TICKS_PER_HOP is no longer a power of 2, this will take up much
more room

*******************************************************************************/
short getSleepTicks(void)
{
  SlotNumber mySlotDifference = SlotsTillMySlot();

  return (mySlotDifference * SLEEP_TICKS_PER_HOP);
}

short getSleepTicksForCommand(void)
{
  SlotNumber commandSlotDifference = SlotsTillDataSlot();

  return (commandSlotDifference * SLEEP_TICKS_PER_HOP);
}

/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
void resetLearnedInformation(void)
{
  myZoneNumber = INVALID_TRANSMITTER_NUMBER;
  houseCode = INVALID_HOUSE_CODE;
  currentSlotNumber = INVALID_SLOT;
  myCheckinSlotNumber = INVALID_SLOT;
}


/*******************************************************************************

DESCRIPTION: Function will format the transmitterPacketBuffer according to the passed message type

NOTES:

*******************************************************************************/
void formatTransmitterMessage(TransmitterMessageType messageType)
{
  ZoneMessageByte msg = (ZoneMessageByte)0; // no buttons pressed

  switch ( messageType)
  {
    case (TMT_SERIAL_MESSAGE):
    {      
      TransmitterAlarmSerialNumberZoneMessage* const sendPointer =
        (TransmitterAlarmSerialNumberZoneMessage*)transmitterPacketBuffer;

      sendPointer->messageType = TAT_ALARM_WITH_SERIAL_NUMBER;
      sendPointer->reserved = 0;
      sendPointer->serialNumberLow = serialNumber & 0xFF;
      sendPointer->serialNumberMiddle = (serialNumber >> 8) & 0xFF;
      sendPointer->serialNumberHigh = (serialNumber >> 16) & 0xFF;
      sendPointer->softwareVersion = SOFTWARE_VERSION;
      // Don't send a real button press here since there's little chance that the FOB will be able to listen for
      // an LED command. It doesn't even have a house code yet and can't hop properly.
      if (!isBatteryGood())
      {
        msg |= CS_LOW_BATTERY;
      }
      sendPointer->zoneMessage = msg;

      #ifdef DMP_BUILDING
      sendPointer->diag = diag;
      #endif
      finalizeTransmitterMessage(sizeof(TransmitterAlarmSerialNumberZoneMessage), ACI_NOTHING);
      UpdateAckStatus(WAITING_FOR_HOUSE_CODE_RESPONSE);
      break;
    }
    case (TMT_ZONE_MESSAGE):
    {
      TransmitterAlarmZoneNumberZoneMessage* const sendPointer =
        (TransmitterAlarmZoneNumberZoneMessage*)transmitterPacketBuffer;

      sendPointer->messageType = TAT_ALARM_WITH_ZONE_NUMBER;
      sendPointer->zoneNumberHigh = (myZoneNumber >> 8) & 0xFF;
      sendPointer->zoneNumberLow = myZoneNumber & 0xFF;
      sendPointer->zoneMessage = GetLatchedInputState(); // This can cause the latched status to be updated.
      #ifdef DMP_BUILDING
      sendPointer->diag = diag;
      #endif
      finalizeTransmitterMessage(sizeof(TransmitterAlarmZoneNumberZoneMessage),
          ACI_HOUSE_CODE | ACI_SERIAL_NUMBER);

      UpdateAckStatus(WAITING_FOR_ACK);
      break;
    }
    case (TMT_CHECKIN_MESSAGE):
    {
      TransmitterCheckinMessage* const sendPointer =
          (TransmitterCheckinMessage*)transmitterPacketBuffer;

      // Since checkin messages are often not transmitted (they are sent
      // only if the receiver commands the transmitter to do so),
      // and since transmitted checkin messages are not acknowledged by the
      // receiver, it is best to NOT cause the latched status to be updated (don't call GetLatchedInputState).
      // Send CS_CHECKIN only (no buttons pressed).

      if (isBatteryGood())
      {
        msg = (ZoneMessageByte)CS_CHECKIN;
      }
      else
      {
        msg = (ZoneMessageByte)(CS_CHECKIN | CS_LOW_BATTERY);
      }
      sendPointer->zoneMessage = msg;

      #ifdef DMP_BUILDING
      sendPointer->diag = diag;
      #endif
      finalizeTransmitterMessage(sizeof(TransmitterCheckinMessage),
          ACI_HOUSE_CODE | ACI_ZONE_NUMBER | ACI_SERIAL_NUMBER);
      // Note: There's no need to call UpdateAckStatus, since this checkin message must be sent in the same
      // slot as the receiver's checkin command. There is no ack / retry for this response.
      break;
    }
    case (TMT_COMMAND_ACK_SETUP):
    {
      CommandDataAckMessage* const sendPointer =
          (CommandDataAckMessage*)transmitterPacketBuffer;

      sendPointer->reserved = 0;
      sendPointer->zoneNumberHigh = (myZoneNumber >> 8) & 0xFF;
      sendPointer->zoneNumberLow = myZoneNumber & 0xFF;
      finalizeTransmitterMessage(sizeof(CommandDataAckMessage),
          ACI_HOUSE_CODE | ACI_SERIAL_NUMBER);
      // Note: There's no need to call UpdateAckStatus. This is just a msg to let the rcvr know we received the command msg.
      break;
    }
    default:
    {
      break;
    }
  }
}


/*******************************************************************************

DESCRIPTION: Function updates the ack status to indicate if waiting for an
ack or not. It also updates the latched contact status if we are not
waiting for an ack.

NOTES:

*******************************************************************************/
void UpdateAckStatus(AckWaiting_t ackWaitingVal)
{
  static bool waitingForHouseCode = false;
  
  switch (ackWaitingVal)
  {
    case WAITING_FOR_ACK:
      {
        waitingForHouseCode = false;

        #ifdef SIMULATE_HEAVY_ACK_LOSS
        if (debugAckDiscardCount < 0) // made negative by checkin slot code
          debugAckDiscardCount = 10; // start discarding
        #endif
      }
      break;

    case WAITING_FOR_HOUSE_CODE_RESPONSE:
      {
        // Set waitingForHouseCode to true so that the latched value doesn't get discarded.
        // The zone message byte sent for the TMT_SERIAL_MESSAGE case above did not come from the latch,
        // so when it's acked, don't call UpdateLatchedStatus.
        waitingForHouseCode = true;
      }
      break;

    case NOT_WAITING_FOR_ACK:
    default:
      {
        if (!waitingForHouseCode)
        {
          InputState_t dummy = {0, Q_INVALID};
          UpdateLatchedStatus(false, dummy); // allow a new value to be latched.
        }
        else
        {
          waitingForHouseCode = false;
        }
      }
      break;

  }
}


/*******************************************************************************

DESCRIPTION: Function indicates if a message has been latched and is therefore
waiting for this information to be sent to the wireless receiver.

NOTES:

*******************************************************************************/
bool IsLatchedMessage(void)
{
  return latchedStatus;
}


/*******************************************************************************

DESCRIPTION: Function indicates if an 'important' message has been latched
or is in the queue.
'Important' is defined as a key press but not a release. Auto generated messages
are not 'important'.

NOTES: This allows the fob to fail searching for a receiver sooner, saving battery.

*******************************************************************************/
bool ImportantMessagesAreWaiting(void)
{
  return (latchedStatus && (latchedState.msgType == Q_BUTTON_PRESS)) || ButtonPressIsInQueue();
}


/*******************************************************************************

DESCRIPTION: If a message has been latched and it is a button press
(but not a release) that should listen for a command (to control LEDs),
then return true.

NOTES:

*******************************************************************************/
bool GetLatchedContactReceiveCommand(void)
{
  return (latchedStatus && (latchedState.msgType == Q_BUTTON_PRESS));
}


/*******************************************************************************

DESCRIPTION:  InvalidateLatch

NOTES:

*******************************************************************************/
void InvalidateLatch(void)
{
  latchedStatus = false;
  latchedState.msgType = Q_INVALID;
  latchedState.msg = 0;
}
