/*******************************************************************************
FILENAME: Timer.c

DESCRIPTION: Functions for timers

NOTES:

Copyright (c) Digital Monitoring Products Inc. 2013.  All rights reserved.

*******************************************************************************/
/*----compilation control-----------------------------------------------------*/

/*******************************************************************************
INCLUDE FILES
*******************************************************************************/
/*----system and platform files (external libraries) -------------------------*/

/*----program files-----------------------------------------------------------*/
#include "types.h"
#include "MSP430SFR.h"
#include "Timer.h"
#include "Sleep.h"


/*******************************************************************************
EXTERNAL REFERENCES   Only used if not available in a header file.
*******************************************************************************/
/*----data declarations-------------------------------------------------------*/

/*----function prototype------------------------------------------------------*/


/*******************************************************************************
PRIVATE DECLARATIONS
*******************************************************************************/
/*----defines, typedefs, structs, unions, enums-------------------------------*/
enum
{
  // bitfield flags
  TIMER_ONE_SHOT_FLAG = 0x01,  // if set, one-shot, else continuous
  // TIMER_FUTURE_1_FLAG = 0x02,
  // TIMER_FUTURE_2_FLAG = 0x04,
  // ...
  // TIMER_FUTURE_6_FLAG = 0x40,
  // TIMER_FUTURE_7_FLAG = 0x80,
};

typedef struct
{
  TimerCallback_t functionPointer;
  byte flags;
  byte compareTime;                   // Note: an 8 bit compareTime yields a max of 255 * 50 mSec = 12.75 seconds
} TimerCompare_t;

/*----data declarations-------------------------------------------------------*/
static TimerCompare_t timerCompare[NUM_50mS_TIMERS];


// This variable is the current value of 50 mSec counter.
// It is similar to the free running TAR, and can be used as the
// time base for a software compare (similar to the CCR done in HW). Note that
// like the TAR, it wraps around to 0.  Greater or less than comparisons should
// not be done. Test only for equal or not equal.
//
static byte counter50mSec = 0;

/*----function prototype------------------------------------------------------*/

/*----macros------------------------------------------------------------------*/

/*******************************************************************************
PRIVATE FUNCTION DEFINITIONS
*******************************************************************************/
/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/


/*******************************************************************************
PUBLIC FUNCTION DEFINITIONS
*******************************************************************************/
/*******************************************************************************

DESCRIPTION: Init50mSecTimer
  setup timer A1 to fire every 50 mSec

NOTES:  This function must not be called during normal processing,
since canceling timers can cause undesired behavior.

*******************************************************************************/
void Init50mSecTimer(void)
{
  for (int i = 0; i < NUM_50mS_TIMERS; i++)
  {
    CancelTimer(i); // cancel any timers that may currently be active
  }
  
  TACTL = TASSEL_1 + MC_2 + SLEEP_TIMERA_DIVIDER_SETTING * ID0;  // use ACLK (32768), continuous, and sleep clock divider (div by 4)
  TACCR1 = TAR + ACLK_TIMER_50_mSEC; // set interrupt time
  TACCTL1 = CCIE & ~CAP; // enable interrupt and set compare mode
}


/*******************************************************************************

DESCRIPTION:  Start a timer using a free-running 50 mSec counter to compare against

NOTES: There is a 12.75 second max timer. This is due to using an
8 bit compareTime variable. It can be changed to 16 bits if needed,
however, the 1145 is almost out of RAM!

For the 'oneShot' parameter, pass TIMER_CONTINUOUS if the timer will be restarted
in the callback function. Note that the timer management SW doesn't restart the timer
automatically in order to minimize RAM usage. Pass TIMER_ONE_SHOT to cause the timer
handler to be fully canceled when the time elapses.

*******************************************************************************/
void StartTimer(int timerHandle, byte timerVal, TimerCallback_t callbackFunction, bool oneShot)
{
  timerCompare[timerHandle].compareTime = timerVal + counter50mSec;
  timerCompare[timerHandle].functionPointer = callbackFunction;
  
  timerCompare[timerHandle].flags = 0;

  if (oneShot == TIMER_ONE_SHOT)
  {
    timerCompare[timerHandle].flags |= TIMER_ONE_SHOT_FLAG;
  }
}


/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
void CancelTimer(int timerHandle)
{
  timerCompare[timerHandle].functionPointer = 0;
  timerCompare[timerHandle].flags = 0; // zero all timer flags for this timer
}


/*******************************************************************************

DESCRIPTION:

NOTES: This is called from an interrupt. Callback functions must
be interrupt safe! Also, it's possible that the processor is in a
low power mode (very slow clock) so the callback functions must not
be "lengthy".

*******************************************************************************/
void Update50msTimers(void)
{
  counter50mSec++;

  for (int i = 0; i < NUM_50mS_TIMERS; i++)
  {
    if ((timerCompare[i].functionPointer != 0) && (timerCompare[i].compareTime == counter50mSec))
    {
      // Save a copy of the flags for this timer before running the callback fcn (since it can cause the flags to change for the next timer).
      // When a timer transitions from 'continuous' to 'one-shot', we shouldn't kill
      // the one-shot timer before it runs.
      byte savedFlags = timerCompare[i].flags;

      timerCompare[i].functionPointer();
      if (savedFlags & TIMER_ONE_SHOT_FLAG)
      {
        timerCompare[i].functionPointer = 0; // one-shot callback, so cancel it now
      }
    }
  }
}
