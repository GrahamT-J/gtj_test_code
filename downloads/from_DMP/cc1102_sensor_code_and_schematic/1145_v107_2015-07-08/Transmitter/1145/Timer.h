/*******************************************************************************
FILENAME: Timer.h

DESCRIPTION: Timer header file

NOTES:

Copyright (c) Digital Monitoring Products Inc. 2013.  All rights reserved.

*******************************************************************************/
#ifndef  TIMER_H
#define  TIMER_H

/*----compilation control-----------------------------------------------------*/

/*******************************************************************************
INCLUDE FILES
*******************************************************************************/
/*----system and platform files-----------------------------------------------*/

/*----program files-----------------------------------------------------------*/

/*******************************************************************************
FILE CONTEXT
*******************************************************************************/
/*----defines, typedefs, structs, unions, enums-------------------------------*/

// Timer callback function handles
enum
{
  TIMER_1,
  TIMER_2,
  TIMER_3,
  NUM_50mS_TIMERS
};

enum
{
  TIMER_ONE_SHOT = true,
  TIMER_CONTINUOUS = false
};

typedef void(*TimerCallback_t)(void); // callback function pointer

/*----data descriptions-------------------------------------------------------*/

/*----data declarations-------------------------------------------------------*/

/*----macros------------------------------------------------------------------*/
#define DEBOUNCE_SHORT_PRESS_TIMER   TIMER_1
#define DEBOUNCE_LONG_PRESS_TIMER    TIMER_2
#define GIVE_UP_TIMER                TIMER_3

// Time values based on a 50 mSec tick.
// Do not exceed 12 seconds in order to minimize RAM usage (8-bits * 50 mSec = 12.75 Sec)
#define TIME_250_mSec                5
#define TIME_1_Sec                   (4 * TIME_250_mSec)
#define TIME_2_Sec                   (2 * TIME_1_Sec)
#define TIME_6_Sec                   (6 * TIME_1_Sec)

/*----function prototypes-----------------------------------------------------*/
void StartTimer(int timerHandle, byte timerVal, TimerCallback_t callbackFunction, bool oneShot);
void CancelTimer(int timerHandle);
void Init50mSecTimer(void);
void Update50msTimers(void);

#endif                                  /* end of file */
