/*H*****************************************************************************
FILENAME:

DESCRIPTION:

NOTES:

Copyright (c) Digital Monitoring Products Inc. 1999.  All rights reserved.

	Last change:  TDC  27 Jan 99    8:43 am
*****************************************************************************H*/
/*----compilation control-----------------------------------------------------*/

/*******************************************************************************
INCLUDE FILES
*******************************************************************************/
/*----system and platform files (external libraries) -------------------------*/

/*----program files-----------------------------------------------------------*/
#include "Hardware.h"

/*******************************************************************************
EXTERNAL REFERENCES   Only used if not available in a header file.
*******************************************************************************/
/*----data declarations-------------------------------------------------------*/

/*----function prototype------------------------------------------------------*/


/*******************************************************************************
PRIVATE DECLARATIONS  Defined in this module, used only in this module.
*******************************************************************************/
/*----defines, typedefs, structs, unions, enums-------------------------------*/

/*----data declarations-------------------------------------------------------*/

/*----function prototype------------------------------------------------------*/
void Set_DCO (void);
/*----macros------------------------------------------------------------------*/


/*******************************************************************************
PUBLIC FUNCTION DEFINITIONS  Note: Header is repeated for each function.
*******************************************************************************/
/*F*****************************************************************************

DESCRIPTION:  Function to init the hardware into the startup state

NOTES:  P3.4  Uart TX
        P3.5  Uart RX

*****************************************************************************F*/
void initHardware(void)
{
  WDTCTL = WDTPW + WDTHOLD;                 // Stop watchdog timer

  BCSCTL1 |= DIVA_0;                        // ACLK = LFXT1CLK/1
  //setIOSel(SCLK);
  //setIODirOut(SCLK);
  setIO(CSn);
  Set_DCO();                                // Set DCO
  clearIO(RED_LED);
  clearIO(GREEN_LED);
  setIODirOut(RED_LED);
  clearIOSel(RED_LED);
  setIODirOut(GREEN_LED);
  clearIOSel(GREEN_LED);
  setIODirIn(NORTH_BUTTON);
  setIODirIn(SOUTH_BUTTON);
  setIODirIn(EAST_BUTTON);
  setIODirIn(WEST_BUTTON);
  clearIOSel(NORTH_BUTTON);
  clearIOSel(SOUTH_BUTTON);
  clearIOSel(EAST_BUTTON);
  clearIOSel(WEST_BUTTON);
  setIO(NORTH_BUTTON);
  setIO(SOUTH_BUTTON);
  setIO(EAST_BUTTON);
  setIO(WEST_BUTTON);
  enableIOResistor(NORTH_BUTTON);
  enableIOResistor(SOUTH_BUTTON);
  enableIOResistor(EAST_BUTTON);
  enableIOResistor(WEST_BUTTON);
  setIOFallingInt(NORTH_BUTTON);
  setIOFallingInt(SOUTH_BUTTON);
  setIOFallingInt(EAST_BUTTON);
  setIOFallingInt(WEST_BUTTON);
  clearIOIntFlag(NORTH_BUTTON);
  clearIOIntFlag(SOUTH_BUTTON);
  clearIOIntFlag(EAST_BUTTON);
  clearIOIntFlag(WEST_BUTTON);   
  setIOEnableInt(NORTH_BUTTON);
  setIOEnableInt(SOUTH_BUTTON);
  setIOEnableInt(EAST_BUTTON);
  setIOEnableInt(WEST_BUTTON);  
  _BIS_SR(GIE);
}

/*******************************************************************************
PRIVATE FUNCTION DEFINITIONS  Note: Header is repeated for each function.
*******************************************************************************/
/*f*****************************************************************************

DESCRIPTION:

NOTES:

*****************************************************************************f*/

//------------------------------------------------------------------------------
void Set_DCO (void)                         // Set DCO to selected frequency
//------------------------------------------------------------------------------
{
  unsigned int Compare, Oldcapture = 0;

  CCTL2 = CM_1 + CCIS_1 + CAP;              // CAP, ACLK
  TACTL = TASSEL_2 + MC_2 + TACLR;          // SMCLK, cont-mode, clear

  while (1)
  {
    while (!(CCIFG & CCTL2));               // Wait until capture occured
    CCTL2 &= ~CCIFG;                        // Capture occured, clear flag
    Compare = CCR2;                         // Get current captured SMCLK
    Compare = Compare - Oldcapture;         // SMCLK difference
    Oldcapture = CCR2;                      // Save current captured SMCLK
    if (DELTA == Compare) break;            // If equal, leave "while(1)"

    else if (DELTA < Compare)
    {
      DCOCTL--;
      if (DCOCTL == 0xFF)                   // DCO is too fast, slow it down
      {
        if (!(BCSCTL1 == (XT2OFF + DIVA_3)))
        BCSCTL1--;                          // Did DCO role under?, Sel lower RSEL
      }
    }
    else
    {
      DCOCTL++;                             // DCO is too slow, speed it down
      if (DCOCTL == 0x00)
      {
        if (!(BCSCTL1 == (XT2OFF + DIVA_3 + 0x0F)))
        BCSCTL1++;                          // Did DCO role over? Sel higher RSEL
      }
    }
  }

  CCTL2 = 0;                                // Stop CCR2
  TACTL = 0;                                // Stop Timer_A
}
