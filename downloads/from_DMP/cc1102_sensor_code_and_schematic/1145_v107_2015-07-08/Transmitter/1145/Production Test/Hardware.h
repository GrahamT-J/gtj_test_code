/*H*****************************************************************************
FILENAME:

DESCRIPTION:

NOTES:

Copyright (c) Digital Monitoring Products Inc. 1999.  All rights reserved.

	Last change:  TDC  27 Jan 99    8:43 am
*****************************************************************************H*/

/*----compilation control-----------------------------------------------------*/

/*******************************************************************************
INCLUDE FILES   Note: Minimize nesting of header files.
*******************************************************************************/
/*----system and platform files-----------------------------------------------*/
#include "ioportpins.h"
#include "BitMacros.h"
#include "types.h"
/*----program files-----------------------------------------------------------*/

/*******************************************************************************
FILE CONTEXT
*******************************************************************************/
/*----defines, typedefs, structs, unions, enums-------------------------------*/
#define Bitime_5  3                      // ~ 0.5 bit length
#define Bitime    7                     // ~ 4800 baud
#define DELTA     244                     // Target DCO = DELTA*(32768) ~8MHz

#define MASTER_CLOCK 8000000
/*----data descriptions-------------------------------------------------------*/

/*----data declarations-------------------------------------------------------*/

/*----macros------------------------------------------------------------------*/

/*----function prototypes-----------------------------------------------------*/

extern void initHardware(void);


