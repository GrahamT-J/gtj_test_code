//******************************************************************************
//  MSP-FET430x110 Demo - Timer_A, Ultra-Low Pwr UART 9600 Echo, 32kHz ACLK
//
//  Description: This program demonstrates a half-duplex 9600-baud UART using
//  Timer_A3 and a 32kHz crystal.  The program will wait in LPM3, echoing back
//  a received character using 8N1 protocol.  The Set_DCO function will
//  calibrate the DCOCLK to ~2MHz which is used as the Timer_A clock.
//  ACLK = LFXT1/8 = 32768/8, MCLK = SMCLK = target DCO
//  //* External watch crystal installed on XIN XOUT is required for ACLK *//	
//
//                MSP430F2121
//            -----------------
//        /|\|              XIN|-
//         | |                 | 32kHz
//         --|RST          XOUT|-
//           |                 |
//           |   CCI0A/TXD/P1.1|-------->
//           |                 | 9600 8N1
//           |   CCI0B/RXD/P2.2|<--------
//
#define RXD       0x04                      // RXD on P2.2
#define TXD       0x02                      // TXD on P1.1


#include "Hardware.h"
#include "uart.h"
#include "chipcon.h"

char dataPattern[6];
char dataPointer = 0;
char bitPointer = 0x01;
unsigned int syncBitCounter = 0;
unsigned int patternRegister = 0;
bool TX = false;
bool BERDone = false;
byte BERCounter = 0;
char currentChannel = 0;
  
typedef struct
{
  const char command;
  void(*function)(void);
} CommandStruct;

void configRegisterPoke(void);
void commandStrobe(void);
void statusRegisterPeek(void);
void showList(void);
void TransmitData(void);
void ReadRSSI(void);
void setFrequency(void);
void setMode(void);
int hexToInt(const char* string, char length);
unsigned long hexToLong(const char* string, char length);
void intToHex(int val, char* string, char length);
void configRegisterPeek(void);
void initRFChip(void);
void sendDMPChannel(void);
void toggleLED(void);
void dataOn (void);
void dataOff (void);
void frequencySet (void);
void berTest (void);
void modeSelect (void);
void toggleClock(void);
void decrementChannel(void);
void incrementChannel(void);
void debounceDelay(void);

#define MainMenuSize 0x0B
const char* mainMenu[] =
{ "1) \t Set Frequency",
  "2) \t Set TX/RX Mode",
  "3) \t Read RSSI",
  "4) \t Data Transmit",
  "5) \t Config Register Poke",
  "6) \t Config Register Peek",
  "7) \t Command Strobe",
  "8) \t Status Register Peek",
  "9) \t Init Chipcon To Defaults",
  "A) \t Set DMP Channel",
  "I) \t Toggle LED's"
};

#define CommandListSize 21
const CommandStruct commandList[] =
{ {'1'    , &setFrequency },
  {'2'    , &setMode },
  {'3'    , &ReadRSSI },
  {'4'    , &TransmitData },
  {'5'    , &configRegisterPoke },
  {'6'    , &configRegisterPeek },
  {'7'    , &commandStrobe },
  {'8'    , &statusRegisterPeek },
  {'9'    , &initRFChip },
  {'A'    , &sendDMPChannel },
  {'D'    , &dataOn },
  {'d'    , &dataOff  },
  {'F'    , &frequencySet },
  {'B'    , berTest },
  {'I'    , toggleLED },
  {'i'    , toggleLED },
  {'M'    , modeSelect  },
  {'J'    , toggleClock },
  {'j'    , toggleClock },
  {'+'    , incrementChannel },
  {'-'    , decrementChannel }
};

void main (void)
{
#ifndef DMP_FCC_TEST
  char command;
  char lcv;
#endif

  initHardware();
  initChipcon();
  initUart();
   
#ifdef DMP_PRODUCTION_TEST
  sendString("MODE=02 RTP=6B75 FSP=D00000 ADP=A160 PAT=817E817E\n\r");
#endif
// Mainloop
  while(1)
  {
#ifndef DMP_FCC_TEST
#ifndef DMP_PRODUCTION_TEST
    showList();
#endif
    command = waitForChar();
    for( lcv = 0; lcv < CommandListSize; lcv++)
    {
      if( command == commandList[lcv].command  )
      {
        commandList[lcv].function();
        break;
      }
    }
#endif    
  }
}

void incrementChannel(void)
{
    sendChipconCommandStrobe(SIDLE);
    sendChannel(++currentChannel);
    sendChipconCommandStrobe(STX);
}

void decrementChannel(void)
{
    sendChipconCommandStrobe(SIDLE);
    sendChannel(--currentChannel);
    sendChipconCommandStrobe(STX);
}

void showList(void)
{
    char ilcv;
    int currentStatusInt;
    char currentStatus[6];

    currentStatusInt = readChipconStatus();
    intToHex(currentStatusInt, &currentStatus[0], 4);
    sendString("\f\n\r");
    sendString("Current chip status byte: ");
    sendString(&currentStatus[0]);
    sendString("\n\r");
    for( ilcv = 0; ilcv < MainMenuSize; ilcv++)
    {
      sendString(mainMenu[ilcv]);
      sendString("\n\r");
    }
    sendString("Enter a Command: ");
}


void configRegisterPoke(void)
{
  char address[6];
  char value[6];
  char stringLength = 0;
  unsigned int addressInt;
  unsigned int valueInt;

  sendString("\n\rEnter the address: ");
  stringLength = waitForString(&address[0]);
  addressInt = hexToInt(&address[0],stringLength);
  sendString("\n\rEnter the value: ");
  stringLength = waitForString(&value[0]);
  valueInt = hexToInt(&value[0],stringLength);
  if(addressInt < 0x30 && valueInt < 0xFF)
    writeChipconConfigRegister((ConfigRegisters)addressInt,valueInt);
}

void configRegisterPeek(void)
{
  char address[6];
  char stringLength = 0;
  unsigned int addressInt;
  unsigned int valueInt;

  sendString("\n\rEnter the address: ");
  stringLength = waitForString(&address[0]);
  addressInt = hexToInt(&address[0],stringLength);
  if(addressInt < 0x30)
  {
      valueInt = readChipconConfigRegister((ConfigRegisters)addressInt);
      sendString("\n\rThe register value: ");
      intToHex(valueInt, &address[0], 4);
      sendString(&address[0]);
      sendString("\n\rPress Any Key");
      waitForChar();
  }
}

void TransmitData(void)
{
  char DataString[12];
  char i;

  clearIOEnableInt(RF_DATA_CLK);
  sendChipconCommandStrobe(SIDLE); // put the RF Chip into Idle mode  
  writeChipconConfigRegister(MDMCFG2, 0x00);
  setIODirOut(RF_DATA_OUT);
  
  sendString("\n\rEnter the Data Pattern: ");
  waitForString(&DataString[0]);
  
  for( i = 0; i<6; i++)
  {
    dataPattern[i] = hexToInt(&DataString[i*2], 2);
  }
  
  bitPointer = 0x01;
  dataPointer = 0;
  TX = true;
  sendChipconCommandStrobe(STX);  // change the RF chips mode to TX    
      // Prepare for interrupts on the data clk line
  clearIOIntFlag(RF_DATA_CLK); 
  setIORisingInt(RF_DATA_CLK);
  setIOEnableInt(RF_DATA_CLK);
  
}

void toggleLED(void)
{
  if ( getIO(RED_LED) )
  {
    sendString("luminate off\n\r");
    clearIO(RED_LED);
    clearIO(GREEN_LED);
  }
  else
  {
    sendString("luminate on\n\r");
    setIO(RED_LED);
    setIO(GREEN_LED);
  }
}

void initRFChip(void)
{
  initChipcon();
  sendString("\n\rChipcon Chip Set To Defaults");
  waitForChar();
}


void sendDMPChannel(void)
{
  unsigned char channel;
  unsigned char stringLength;
  char channelString[6];
  
  sendString("\n\rPlease enter a channel");
  stringLength = waitForString(&channelString[0]);
  channel = hexToInt(&channelString[0], stringLength);
  sendChannel(channel);
}

void ReadRSSI(void)
{
  int RSSIDataInt;
  char RSSIData[6];

  RSSIDataInt = readChipconStatusRegister(RSSI);
  sendString("\n\rCurrent RSSI: ");
  intToHex(RSSIDataInt, &RSSIData[0], 4);
  sendString(&RSSIData[0]);
  sendString("\n\rPress Any Key");
  waitForChar();
  //printf("\n\rCurrentRSSI: %d", RSSIData);
  //printf("\n\rPress any key:");
  //scanf("%c");
}

void setFrequency(void)
{
  unsigned long dataLong;
  char data[6];
  char tempData;

  //printf("\n\rEnter the Frequency Register Value: ");
  sendString("\n\rEnter the Frequency Register Value: ");
  //scanf("%6Lx", &data);
  tempData = waitForString(&data[0]);
  dataLong = hexToLong(&data[0], tempData);
  if(dataLong < 0xFFFFFF)
  {
    tempData = dataLong & 0xFF;
    writeChipconConfigRegister(FREQ0, tempData);
    tempData = dataLong>>8 & 0xFF;
    writeChipconConfigRegister(FREQ1, tempData);
    tempData = dataLong>>16 & 0xFF;
    writeChipconConfigRegister(FREQ2, tempData);
  }

}

void setMode(void)
{
  char userChoice;

  do
  {
    sendString("\f1)\tTX Mode");
    sendString("\n\r2)\tRX Mode");
    sendString("\n\r3)\tSleep Mode");
    sendString("\n\r4)\tIdle Mode");
    sendString("\n\rEnter the mode:");
    userChoice = waitForChar();
  }while( userChoice > '5' || userChoice < '1');
  switch( userChoice )
  {
  case '1':
    sendChipconCommandStrobe(STX);
    TX = true;
    break;
  case '2':
    sendChipconCommandStrobe(SRX);
    TX = false;
    break;
  case '3':
    sendChipconCommandStrobe(SPWD);
    break;
  case '4':
    sendChipconCommandStrobe(SIDLE);
    break;
  }

}

void commandStrobe(void)
{
  int addressInt;
  char address[6];
  char stringLength;

  sendString("\n\rEnter the address: ");
  stringLength = waitForString(&address[0]);
  addressInt = hexToInt(&address[0], stringLength);
  if(addressInt >= 0x30 && addressInt < 0x3F)
    sendChipconCommandStrobe((CommandStrobes)addressInt);
}

void statusRegisterPeek(void)
{
    int addressInt, valueInt = 0;
    char address[6];
    char stringLength = 0;
    //printf("\n\rEnter the address: ");
    sendString("\n\rEnter the address: ");
    //scanf("%2x", &address);
    stringLength = waitForString(&address[0]);
    addressInt = hexToInt(&address[0], stringLength);
    if(addressInt >= 0x30 && addressInt < 0x3f)
    {
      valueInt = readChipconStatusRegister((StatusRegisters)addressInt);
      sendString("\n\rThe register value: ");
      intToHex(valueInt, &address[0], 4);
      sendString(&address[0]);
      sendString("\n\rPress Any Key");
      waitForChar();
      //printf("\n\rThe register value: %x", value);
      //printf("\n\rPress Any Key:");
      //scanf("%c");
    }
}

/*******************************************************************************

DESCRIPTION:  Converts integer 'val' into a hex string 'string' of length 'length'

NOTES: does not work with length > 4

*******************************************************************************/
void intToHex(int val, char* string, char length)
{
  const char hexTable[] = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
  char i;
  int workingVal = val;
  char* writeString = string + length;  // start at rightmost character
  *writeString = 0;
  writeString--;
  for (i = 0; i < length; ++i)
  {
    *writeString = hexTable[workingVal & 0x000F];
    --writeString;
    workingVal >>= 4;
  }
}

/*******************************************************************************

DESCRIPTION: converts 'length' characters of ascii hexadecimal 'string' to an integer

NOTES:

*******************************************************************************/
int hexToInt(const char* string, char length)
{
  char i;
  int returnValue = 0;
  for (i = 0; i < length; ++i)
  {
    char character = string[i];
    if ((character >= 'A') && (character <= 'F'))
    {
      character -= ('A' - 10);
    }
    else if ((character >= '0') && (character <= '9'))
    {
      character -= '0';
    }
    else
    {
      continue;  // ignore invalid character
    }
    returnValue *= 16;
    returnValue += character;
  }
  return returnValue;
}


unsigned long hexToLong(const char* string, char length)
{
  char i;
  unsigned long returnValue = 0;
  for (i = 0; i < length; ++i)
  {
    char character = string[i];
    if ((character >= 'A') && (character <= 'F'))
    {
      character -= ('A' - 10);
    }
    else if ((character >= '0') && (character <= '9'))
    {
      character -= '0';
    }
    else
    {
      continue;  // ignore invalid character
    }
    returnValue *= 16;
    returnValue += character;
  }
  return returnValue;
}

void rfDataClockEdge(void)
{
  bool dataBit = getIO(RF_DATA_IN) > 0;  // read state of data ASAP

  if( TX )
  {
    dataBit = dataPattern[dataPointer] & bitPointer;
    
    if( dataBit )
      setIO(RF_DATA_OUT);
    else
      clearIO(RF_DATA_OUT);
    
    bitPointer <<= 1;
    if( bitPointer == 0 )
    {
      bitPointer = 0x01;
    }
    dataPointer ++;
    if( dataPointer >= 6 )
    {
      dataPointer = 0;
    }
  }
  else
  {
               //shift in newest data bit
    patternRegister <<=1;
    syncBitCounter++;
    if(dataBit)
      patternRegister |= 0x01;
    
      //did we recieve the last bit of the last pattern byte?
    if( (patternRegister) == 0x817E)
    {
      patternRegister = 0;
      BERCounter++;
    }
    else if( syncBitCounter > 2000)// if we didn't detect the sync word in 8 bits then the chipcon received a false sync word
    {
      clearIOEnableInt(RF_DATA_CLK);
      BERDone = true;
    }
  }
}  

/*******************************************************************************

DESCRIPTION:

NOTES: This assumes that any edge interrupts on this port are for alarm contacts

*******************************************************************************/
#ifdef __ICC430__
#pragma vector = PORT1_VECTOR
#pragma type_attribute = __interrupt
#endif
void port1EdgeInterrupt(void)
#ifndef __ICC430__
__interrupt[PORT1_VECTOR]
#endif
{
    // first check for rf clock edge
  if (getIOIntFlag(RF_DATA_CLK))
  {
    clearIOIntFlag(RF_DATA_CLK);
    rfDataClockEdge();
  }
  else if (getIOIntFlag(WEST_BUTTON))
  {
    clearIOIntFlag(WEST_BUTTON);
    debounceDelay();
    if (!getIO(WEST_BUTTON))
    {    
      decrementChannel();   
    }
  }

}

void dataOn (void)
{
  char i;
#ifndef DMP_FCC_TEST
  sendString("ata on\n\r");
#endif
  clearIOEnableInt(RF_DATA_CLK);
  sendChipconCommandStrobe(SIDLE); // put the RF Chip into Idle mode  
  writeChipconConfigRegister(MDMCFG2, 0x00);
  writeChipconConfigRegister(DEVIATN, 0x35);
  setIODirOut(RF_DATA_OUT);

  for( i = 0; i<6; i++)
  {
    dataPattern[i] = 0x55;
  }
  
  bitPointer = 0x01;
  dataPointer = 0;
  TX = true;
  sendChipconCommandStrobe(STX);
      // Prepare for interrupts on the data clk line
  clearIOIntFlag(RF_DATA_CLK); 
  setIORisingInt(RF_DATA_CLK);
  setIOEnableInt(RF_DATA_CLK);
  
}

void dataOff (void)
{
#ifndef DMP_FCC_TEST
  sendString("ata off\n\r");
#endif
  clearIOEnableInt(RF_DATA_CLK);
  writeChipconConfigRegister(DEVIATN, 0x00);
}

void frequencySet (void)
{
  unsigned long dataLong;
  char data[6];
  char tempData;

  sendChipconCommandStrobe(SIDLE);
  sendString("SP= ");
  tempData = waitForString(&data[0]);
  dataLong = hexToLong(&data[0], tempData);
  writeChipconConfigRegister(DEVIATN, 0x00);
  if( dataLong == 0xD09AAC )
    dataLong = 0x22B13B;
  else if( dataLong == 0xD00000 )
    dataLong = 0x23313B;
  else if( dataLong == 0xD06554 )
    dataLong = 0x23B13B;
      
  if(dataLong < 0xFFFFFF)
  {
    tempData = dataLong & 0xFF;
    writeChipconConfigRegister(FREQ0, tempData);
    tempData = dataLong>>8 & 0xFF;
    writeChipconConfigRegister(FREQ1, tempData);
    tempData = dataLong>>16 & 0xFF;
    writeChipconConfigRegister(FREQ2, tempData);
  }
  if (TX)
    sendChipconCommandStrobe(STX);
  else
    sendChipconCommandStrobe(SRX);
  sendString("\n\r");
}

void berTest (void)
{
  char hexString[5];
  
  clearIOEnableInt(RF_DATA_CLK); // disable the DCLK interrupt before configureing RF Chip
  writeChipconConfigRegister(MDMCFG2, 0x00); //turn off sync word detection  
  setIODirIn(RF_DATA_IN); // set the data pin direction  
    // Prepare for interrupts on the data clk
  syncBitCounter = 0;
  BERDone = false;
  BERCounter = 0;  
  clearIOIntFlag(RF_DATA_CLK);
  setIORisingInt(RF_DATA_CLK);  
  setIOEnableInt(RF_DATA_CLK);
  while(!BERDone);
  sendString("ER= ");
  intToHex(BERCounter, &hexString[0], 2);
  sendString(&hexString[0]);
  sendString("\n\r");
}

void modeSelect (void)
{
  char modeString;
  
  sendString("= ");
  modeString = waitForChar();
  switch (modeString)
  {
  case '1':
    sendChipconCommandStrobe(SIDLE);
    writeChipconConfigRegister(DEVIATN, 0x35);
    clearIOEnableInt(RF_DATA_CLK);
    setIODirIn(RF_DATA_IN);
    TX = false;
    sendChipconCommandStrobe(SRX);
    break;
  case '2':
    sendChipconCommandStrobe(SIDLE);
    writeChipconConfigRegister(DEVIATN, 0x00);
    clearIOEnableInt(RF_DATA_CLK);
    setIODirOut(RF_DATA_OUT);
    TX = true;
    sendChipconCommandStrobe(STX);
    break;
  case '3':
    sendChipconCommandStrobe(SIDLE);
    sendChipconCommandStrobe(SPWD);
    break;
  }
  sendString("\n\r");
}


void toggleClock(void)
{
  if(TACCTL1 & CCIE)
  {
    TACCTL1 = 0;
    setIODirOut(SI);
    clearIOSel(SI);    
    sendString("CLK off\n\r");
  }
  else
  {
    TACCR1 = TAR + 4;
    TACCTL1 = OUTMOD_1 + CCIE;  
    setIODirOut(SI);
    setIOSel(SI);
    sendString("CLK on\n\r");
  }
}

#ifdef __ICC430__
#pragma vector = PORT2_VECTOR
#pragma type_attribute = __interrupt
#endif
void port2EdgeInterrupt(void)
#ifndef __ICC430__
__interrupt[PORT2_VECTOR]
#endif
{
  
  if( getIOIntFlag(NORTH_BUTTON))
  {
    clearIOIntFlag(NORTH_BUTTON);
    debounceDelay();
    if (!getIO(NORTH_BUTTON))
    {
      if (P1IE & 0x08)
        dataOff();
      else
        dataOn();
    }
  }
  else if (getIOIntFlag(SOUTH_BUTTON))
  {
    clearIOIntFlag(SOUTH_BUTTON);
    debounceDelay();
    if (!getIO(SOUTH_BUTTON))
    {    
      sendChipconCommandStrobe(SIDLE);
      sendChipconCommandStrobe(SRX);
      TX = false; 
    }
  }
  else if (getIOIntFlag(EAST_BUTTON))
  {
    clearIOIntFlag(EAST_BUTTON);
    debounceDelay();
    if (!getIO(EAST_BUTTON))
    {    
      incrementChannel();
    }
  }
}

void debounceDelay()
{
  int i;
  
  for( i=0; i<512; i++);
  
  return;
}
