/*H*****************************************************************************
FILENAME: chipconCommunication.h

DESCRIPTION:

NOTES:

Copyright (c) Digital Monitoring Products Inc. 2004-2013.  All rights reserved.

*****************************************************************************H*/
#ifndef  COMMUNICATION_H
#define  COMMUNICATION_H

/*----compilation control-----------------------------------------------------*/

/*******************************************************************************
INCLUDE FILES   Note: Minimize nesting of header files.
*******************************************************************************/
/*----system and platform files-----------------------------------------------*/

/*----program files-----------------------------------------------------------*/

/*******************************************************************************
FILE CONTEXT
*******************************************************************************/
/*----defines, typedefs, structs, unions, enums-------------------------------*/
typedef struct
{
  BITFIELD frameStartDetected   : 1;
  BITFIELD timeToTransmit       : 1;
  BITFIELD patternDetected      : 1;
  BITFIELD transmitComplete     : 1;
  BITFIELD timeToTestFrequency  : 1;
  BITFIELD timeToTestBattery    : 1;
  BITFIELD spare                : 2;
} CommunicationStatusFlags;

#define CHIPCON_TRANSMIT_DELAY_BITS 16  //transmit the recommened 2 bytes of dummy data at the end of a transmission

/*----data descriptions-------------------------------------------------------*/

/*----data declarations-------------------------------------------------------*/
extern word RFFrameBitCounter;
extern CommunicationStatusFlags communicationStatus;

/*----macros------------------------------------------------------------------*/

/*----function prototypes-----------------------------------------------------*/
byte getWirelessReceiveBufferOffset(void);
void setupWirelessTransmit(bool enableRFInterrupt);
void setupWirelessReceive(bool enableRFInterrupt);
void initializeCommunicationStatus(void);
void initStateForReceive(void);
void setupWirelessSleep(void);
void initRFChip(void);
void setupWirelessIdle(void);
void SaveFrequencyOffset(void);
void UndoFrequencyOffset(void);
void TXandTestBattery(void);
void SetLEDTimer(byte flashDuration);
#endif                                  /* end of file */
