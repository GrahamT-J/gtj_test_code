#ifndef MSP430SFR_H
  #define MSP430SFR_H

  #ifdef USING_RECEIVER_HARDWARE
    #include "msp430x14x.h"
  #else
    #ifdef USING_2131_HARDWARE
      #include "msp430x21x1.h"
    #else
      #include "msp430x11x1.h"
    #endif
  #endif

#endif

