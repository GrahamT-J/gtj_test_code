/*******************************************************************************
FILENAME: Hardware.c

DESCRIPTION: Miscellaneous hardware descriptions not included elsewhere

NOTES:

Copyright (c) Digital Monitoring Products Inc. 2004-2013.  All rights reserved.

*******************************************************************************/
/*----compilation control-----------------------------------------------------*/

/*******************************************************************************
INCLUDE FILES
*******************************************************************************/
/*----system and platform files (external libraries) -------------------------*/

/*----program files-----------------------------------------------------------*/
#include "types.h"
#include "IOPortPins.h"
#include "Hardware.h"
#include "Sleep.h"
#include "Inputs.h"
#include "WirelessCommunication.h"
#include "Timer.h"


/*******************************************************************************
EXTERNAL REFERENCES   Only used if not available in a header file.
*******************************************************************************/
/*----data declarations-------------------------------------------------------*/

/*----function prototype------------------------------------------------------*/


/*******************************************************************************
PRIVATE DECLARATIONS
*******************************************************************************/
/*----defines, typedefs, structs, unions, enums-------------------------------*/
enum {Delta = ((MASTER_CLOCK / (SLEEP_MASTER_CLOCK / SLEEP_CLOCK_DIVIDER)) & 0xFFFE)};  // must be even for test

// DCO range select parameters
#define RSEL_MASK 0x07
#define MIN_RSEL 0x00
#define MAX_RSEL 0x07

/*----data declarations-------------------------------------------------------*/
LEDFlashStruct ledFlashStruct;

#ifdef USING_RECEIVER_HARDWARE
byte DummyPort;
#endif
byte RF_ONState;

/*----function prototype------------------------------------------------------*/

/*----macros------------------------------------------------------------------*/

/*******************************************************************************
PRIVATE FUNCTION DEFINITIONS
*******************************************************************************/
/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/


/*******************************************************************************
PUBLIC FUNCTION DEFINITIONS
*******************************************************************************/
/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
void setDCO(void)
{
  word startCapture = 0;
  bool finished = false;
  CCTL2 = CCIS0 | CM0 | CAP;
  TACTL = TASSEL_2 + MC_2 + TACLR;          // SMCLK, cont-mode, clear
  while (! finished)
  {
    word diff;
    while (! (CCTL2 & CCIFG))  // loop until capture
      ;

    CCTL2 &= ~(CCIFG);  // clear capture

    diff = (CCR2 - startCapture) & 0xFFFE;
    startCapture = CCR2;

    if (diff == Delta)
    {
      CCTL2 = 0;
      finished = true;
    }
    else if (diff > Delta)
    {
      if (DCOCTL-- == 0) // if rollover
      {
        if ((BCSCTL1 & RSEL_MASK) > MIN_RSEL)
          --BCSCTL1;  // decrement RSEL
        else
          BCSCTL1 |= MAX_RSEL;  // rollover RSEL from 0 to 7
      }
    }
    else
    {
      if (++DCOCTL == 0) // if rollover
      {
        if ((BCSCTL1 & RSEL_MASK) < MAX_RSEL)
          ++BCSCTL1;  // increment RSEL
        else
          BCSCTL1 &= ~MAX_RSEL;  // rollover RSEL from 7 to 0
      }
    }
  }
}


/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
void initSys(bool hardReset)
{
  __disable_interrupt();  // setDCO, initRFChip etc assume interrupts are not enabled
  BCSCTL1 = DEFAULT_BCSCTL1 | (DIVA0 * SLEEP_CLOCK_DIVIDER_SETTING);
#ifdef USING_2131_HARDWARE
  BCSCTL3 |= XCAP_3;
#endif
  KICK_WATCHDOG();  // start watchdog, reset, and use ACLK
  setDCO();
  initRFChip();
  initRFChip();  // extra init in case 1st failed. The RF chip may have been in the middle of a data xfer, etc and the MSP 430 watchdog expired, etc.
  initRFChip();  // and one more just for good measure
  // Note: setDCO and initRFChip above cause the timer to use TASSEL_2 (DCO clock source).
  // From this point on, however, TASSEL_1 (ACLK) must be constantly selected for proper
  // 50 mSec timer operation.
  TACTL = TASSEL_1 + // ACLK
          MC_2 + // continuous
          SLEEP_TIMERA_DIVIDER_SETTING * ID0;
  wakeup(true, false);
  if (hardReset)
  {
    TACCTL1 = 0; // don't disable 50 mSec interrupt if a soft reset
    initInputs();
    #ifdef SEND_I_AM_ALIVE_MESSAGES
      g_checkin4HourTimer = MINUTES_IN_FOUR_HOURS; // WLS-476: 1145: autonomously generate an "I'm alive" message to the RCVR every 4 hours
    #endif
  }
  setIODirOut(RED_LED);
  setIODirOut(GREEN_LED);
  LED_OFF(RED_LED);
  LED_OFF(GREEN_LED);
  P1DIR |= BIT4; //setup the unused input as an output.
  
  #ifdef DEBUG_EAST_IS_OUTPUT  //1145 button is used as a debug output pin
    clearIO(EAST_BUTTON);
  #endif
}

/*******************************************************************************

DESCRIPTION: Function will setup a timer for pulsing the LED

NOTES:

*******************************************************************************/
enum
{
  SHORT_FLASH = 0,
  MEDIUM_FLASH,
  LONG_FLASH_DURATION,
  RESERVED_FLASH_DURATION
};

void startLEDPulsing(unsigned char flashCount, unsigned char flashDuration, bool redLED, bool greenLED)
{
  ledFlashStruct.greenLED = greenLED;
  if (greenLED)
    setIO(GREEN_LED);
  else
    clearIO(GREEN_LED);
  ledFlashStruct.redLED = redLED;
  if (redLED)
    setIO(RED_LED);
  else
    clearIO(RED_LED);
  ledFlashStruct.ledsOn = true;
  ledFlashStruct.flashCount = (flashCount + 1);
  switch (flashDuration)
  {
  case SHORT_FLASH:
    ledFlashStruct.flashDuration = SHORT_LED_PULSE_TIME;
    break;
  case MEDIUM_FLASH:
    ledFlashStruct.flashDuration = MEDIUM_LED_PULSE_TIME;
    break;
  case LONG_FLASH_DURATION:
    ledFlashStruct.flashDuration = LONG_LED_PULSE_TIME;
    break;
  default:
    ledFlashStruct.flashDuration = SHORT_LED_PULSE_TIME;
    break;
  }
  
  // The LED flashing may be due to a short press. Save battery by canceling the 'command receive' processing
  // if it's running due to a long press. Also, cancel detection of a long press, if it's still being
  // timed (saves battery also).
  CancelCommandReceive();
  CancelTimer(DEBOUNCE_LONG_PRESS_TIMER);

  SetLEDTimer(ledFlashStruct.flashDuration);
  #ifdef DEBUG_EAST_IS_OUTPUT  //1145 button is used as a debug output pin
    setIO(EAST_BUTTON);
  #endif
}

/*******************************************************************************

DESCRIPTION: Function to power off the MSP430's onboard Comparator

NOTES:

*******************************************************************************/
void PowerDownComparator(void)
{
  CACTL1 &= ~CAON;  // turn off comparator
  clearIO(BATT_VOLTAGE_ENABLE);   //  turn off the voltage reference generator
}


/*******************************************************************************

DESCRIPTION:  Function to power on the MSP430;s onboard Comparator

NOTES:

*******************************************************************************/
void PowerUpComparator(void)
{
  setIO(BATT_VOLTAGE_ENABLE);   // turn on the voltage reference generator
  CACTL1 |= CAON;  // turn on comparator
}
