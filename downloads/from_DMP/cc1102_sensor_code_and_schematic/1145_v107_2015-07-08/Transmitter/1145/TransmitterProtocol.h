/*******************************************************************************
FILENAME: TransmitterProtocol.h

DESCRIPTION:

NOTES:

Copyright (c) Digital Monitoring Products Inc. 2004-2013.  All rights reserved.

*******************************************************************************/
#ifndef  TRANSMITTERPROTOCOL_H
#define  TRANSMITTERPROTOCOL_H

/*----compilation control-----------------------------------------------------*/

/*******************************************************************************
INCLUDE FILES
*******************************************************************************/
/*----system and platform files-----------------------------------------------*/
#include "Packet.h"

/*----program files-----------------------------------------------------------*/

/*******************************************************************************
FILE CONTEXT
*******************************************************************************/
/*----defines, typedefs, structs, unions, enums-------------------------------*/
typedef enum
{
  RAS_GOOD_MESSAGE_ACK_RECEIVED,
  RAS_GOOD_MESSAGE_NON_ACK,
  RAS_GOOD_MESSAGE_WRONG_RECEIVER,
  RAS_BAD_MESSAGE,
  RAS_NOTHING_TO_DO,
  RAS_CORRECT_RECEIVER_NO_PROGRAMMING
} ReceivedAckStatus;

typedef enum
{
  TMS_NO_MESSAGE_SETUP,
  TMS_MESSAGE_SETUP,
  TMS_NO_MESSAGE_NEEDED,
  TMS_STILL_WAITING
} TransmitterMessageStatus;

typedef struct
{
  BITFIELD  counter                       :4; // used with ACK LED
  BITFIELD  panicAckWaiting               :1; // used by 1142 transmitter for special panic processing 
  BITFIELD                                :1;
  BITFIELD                                :1;
  BITFIELD                                :1;
}AckStatus;

typedef enum
{
  WAITING_FOR_ACK,
  NOT_WAITING_FOR_ACK,
  WAITING_FOR_HOUSE_CODE_RESPONSE
} AckWaiting_t;

#define SERIAL_NUMBER_ADDRESS 0x1000
#define serialNumberPtr (const long*)(SERIAL_NUMBER_ADDRESS)
#define serialNumber *serialNumberPtr

/*----data descriptions-------------------------------------------------------*/

/*----data declarations-------------------------------------------------------*/
extern AckStatus ackStatus;
/*----macros------------------------------------------------------------------*/

/*----function prototypes-----------------------------------------------------*/
ReceivedAckStatus processReceivedAck(char* characterOffset);
short getSleepTicks(void);
short getSleepTicksForCommand(void);
TransmitterMessageStatus setupTransmitterMessage(char startOffset);
void resetLearnedInformation(void);
void UpdateAckStatus(AckWaiting_t ackWaitingVal);
bool IsLatchedMessage(void);
bool ImportantMessagesAreWaiting(void);
bool GetLatchedContactReceiveCommand(void);
void UpdateLatch(void);
void InvalidateLatch(void);

#ifdef DMP_1100_PRODUCTION_TEST
  void setzonenumber(char);
#endif

#endif                                  /* end of file */
