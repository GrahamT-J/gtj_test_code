/*H*****************************************************************************
FILENAME: Inputs.h

DESCRIPTION:

NOTES:

Copyright (c) Digital Monitoring Products Inc. 2004-2013.  All rights reserved.

*****************************************************************************H*/
#ifndef  INPUTS_H
#define  INPUTS_H

/*----compilation control-----------------------------------------------------*/
#include "packet.h"
#include "sleep.h"

/*******************************************************************************
INCLUDE FILES   Note: Minimize nesting of header files.
*******************************************************************************/
/*----system and platform files-----------------------------------------------*/

/*----program files-----------------------------------------------------------*/

/*******************************************************************************
FILE CONTEXT
*******************************************************************************/
/*----defines, typedefs, structs, unions, enums-------------------------------*/
#define MINUTES_IN_FOUR_HOURS (4 * 60)

// note, for proper sync, the debounce time must equal an integral number of hops
// with no fraction.
// For example, with 32 hops per second and a debounce of 250mS, there are exactly
// 8 hops during debounce.
enum
{
#ifdef DMP_1100_PRODUCTION_TEST
  // Button debouncing no longer uses the timer values in this enum.
  #error Need to determine production test requirements. A quarter second debounce was changed to 1.5 Sec in the past.
#endif
  FIFTEEN_MS       = ((long)15 * SLEEP_TICKS_PER_SECOND) / 1000,
  QUARTER_SECOND   = ((long)250 * SLEEP_TICKS_PER_SECOND) / 1000,
  SIXTEENTH_SECOND = (QUARTER_SECOND / 4)
};

// reuse same bits from message byte
typedef enum
{
  CS_NORTH_BUTTON    = MB_ALARM_0,
  CS_SOUTH_BUTTON    = MB_ALARM_1,
  CS_EAST_BUTTON     = MB_ALARM_2,
  CS_WEST_BUTTON     = MB_ALARM_3,
  CS_LOW_BATTERY     = MB_LOW_BATTERY,
  CS_TAMPER          = MB_TAMPER,
  CS_EXTENDED_BUTTON = MB_EXTENDED,
  CS_CHECKIN         = MB_CHECKIN
} ContactState;

#define CS_ALL_BUTTONS (CS_NORTH_BUTTON | CS_SOUTH_BUTTON | CS_EAST_BUTTON | CS_WEST_BUTTON)

#ifdef SEND_I_AM_ALIVE_MESSAGES
  // ref RADAR WLS-476. Since a keyfob can't really be tampered, a tamper message is used to indicate "I'm Alive".
  // Also a 4-bit count is used when this bit is set.
  #define CS_FOB_IS_ALIVE (CS_TAMPER)
#else
  #define CS_FOB_IS_ALIVE 0
#endif

typedef enum
{
  ENQUEUE_SHORT_PRESS,      // for short button presses or button releases
  ENQUEUE_EXTENDED_PRESS,   // for long button presses
  ENQUEUE_AUTO_GEN_MSG      // auto gen: 4 hour battery message, or "I'm Alive" msg
} EnqueuedMsgType;

// message queue indices
typedef enum
{
  Q_BUTTON_PRESS    = 0, // highest priority
  Q_BUTTON_RELEASE  = 1,
  Q_AUTO_GEN        = 2, // lowest priority
  Q_NUM_ELEMENTS,
  Q_INVALID         = Q_NUM_ELEMENTS + 1
} Q_ElementType_t;

typedef struct
{
  byte              msg;
  Q_ElementType_t   msgType;
} InputState_t;

/*----data descriptions-------------------------------------------------------*/

/*----data declarations-------------------------------------------------------*/

#ifdef SEND_I_AM_ALIVE_MESSAGES
  // WLS-476: 1145: autonomously generate an "I'm alive" message to the RCVR every 4 hours
  extern byte g_checkin4HourTimer;
#endif
/*----macros------------------------------------------------------------------*/

/*----function prototypes-----------------------------------------------------*/
void setupInputs(void);
void initInputs(void);
void startHopTimer(short delayTime);
void testBattery(void);
InputState_t getInputState(void);
ContactState readCurrentContactState(ContactState buttonsToProcess);
#ifdef DMP_1100_PRODUCTION_TEST
  void clearContactState(void);
#endif
void resetLowPowerTimer(void);
bool isBatteryGood(void);
bool IsQueueEmpty(void);
ContactState DequeueKeyFobMessageIfLongPress(void);
bool ButtonPressIsInQueue(void);
void EmptyQueue(void);

#ifdef SEND_I_AM_ALIVE_MESSAGES
  void triggerSendingAliveMessage(void);
#endif

#endif                                  /* end of file */
