;;/**************************************************
;; * Definitions of I/O register description file,
;; * interrupt/exception vectors, interrupt control registers,
;; * I/O register reset values. 
;; *
;; * Copyright 2001-2004 IAR Systems. All rights reserved.
;; *
;; * $Revision: 1.1 $
;; *
;; **************************************************/


;; Memory information ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; 
;; Used to define address zones within the MSP430 address space (Memory). 
;;
;;   Name      may be almost anything
;;   AdrSpace  must be Memory
;;   StartAdr  start of memory block
;;   EndAdr    end of memory block
;;   AccType   type of access, read-only (R) or read-write (RW)

[Memory]
;;         Name             AdrSpace    StartAdr    EndAdr    AccType
Memory0  = SFR              Memory      0x0000      0x01FF      RW
Memory1  = RAM              Memory      0x0200      0x02FF      RW
Memory2  = INFO             Memory      0x1000      0x10FF      RW
Memory3  = FLASH            Memory      0xE000      0xFFFF      R


;; I/O Register description file ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

[SfrInclude]
File = MSP430F2131.sfr

;; Interrupt definitions ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
[InterruptList]
;              Id                 Table Adr     Prio    Enable                  Pending
Interrupt0   = PORT1_VECTOR       0x04          2       P1IE.P0                 P1IFG.P0
Interrupt1   = PORT1_VECTOR       0x04          2       P1IE.P1                 P1IFG.P1
Interrupt2   = PORT1_VECTOR       0x04          2       P1IE.P2                 P1IFG.P2
Interrupt3   = PORT1_VECTOR       0x04          2       P1IE.P3                 P1IFG.P3
Interrupt4   = PORT1_VECTOR       0x04          2       P1IE.P4                 P1IFG.P4
Interrupt5   = PORT1_VECTOR       0x04          2       P1IE.P5                 P1IFG.P5
Interrupt6   = PORT1_VECTOR       0x04          2       P1IE.P6                 P1IFG.P6
Interrupt7   = PORT1_VECTOR       0x04          2       P1IE.P7                 P1IFG.P7
Interrupt8   = PORT2_VECTOR       0x06          2       P2IE.P0                 P2IFG.P0
Interrupt9   = PORT2_VECTOR       0x06          2       P2IE.P1                 P2IFG.P1
Interrupt10  = PORT2_VECTOR       0x06          2       P2IE.P2                 P2IFG.P2
Interrupt11  = PORT2_VECTOR       0x06          2       P2IE.P3                 P2IFG.P3
Interrupt12  = PORT2_VECTOR       0x06          2       P2IE.P4                 P2IFG.P4
Interrupt13  = PORT2_VECTOR       0x06          2       P2IE.P5                 P2IFG.P5
Interrupt14  = PORT2_VECTOR       0x06          2       P2IE.P6                 P2IFG.P6
Interrupt15  = PORT2_VECTOR       0x06          2       P2IE.P7                 P2IFG.P7
Interrupt16  = TIMERA1_VECTOR     0x10          2       TACCTL1.CCIE            TACCTL1.CCIFG
Interrupt17  = TIMERA1_VECTOR     0x10          2       TACCTL2.CCIE            TACCTL2.CCIFG
Interrupt18  = TIMERA1_VECTOR     0x10          2       TACTL.TAIE              TACTL.TAIFG
Interrupt19  = TIMERA0_VECTOR     0x12          2       TACCTL0.CCIE            TACCTL0.CCIFG
Interrupt20  = WDT_VECTOR         0x14          2       IE1.WDTIE               IFG1.WDTIFG
Interrupt21  = COMPARATORA_VECTOR 0x16          2       CACTL1.CAIE             CACTL1.CAIFG
Interrupt22  = NMI_VECTOR         0x1C          2       IE1.NMIIE               IFG1.NMIIFG
Interrupt23  = RESET_VECTOR       0x1E          1

      
;; End of file