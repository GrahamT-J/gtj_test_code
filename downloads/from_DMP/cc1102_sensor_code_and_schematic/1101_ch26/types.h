#ifndef TYPES_H
#define TYPES_H

#ifdef XT50_RECEIVER
#define __disable_interrupt()
#define __enable_interrupt()
#else
#define __disable_interrupt_and_store(A) A = __get_interrupt_state(); __disable_interrupt()
// the MSP430 requires a NOP instruction after the DINT instruction to
// guarantee no interrupt is accepted before the next instruction
#define ENTER_CRITICAL_SECTION() __disable_interrupt(); \
                                 _NOP()

#define EXIT_CRITICAL_SECTION() __enable_interrupt()
#endif

typedef unsigned short word;
typedef unsigned char BITFIELD;
typedef unsigned int LONG_BITFIELD;
typedef unsigned char byte;

enum
{
  false,
  true = !false
};
typedef char bool;

//-------------------------------------------------

#endif
