/*******************************************************************************
FILENAME: Protocol.h

DESCRIPTION:

NOTES:

Copyright (c) Digital Monitoring Products Inc. 2004.  All rights reserved.

*******************************************************************************/
#ifndef  PROTOCOL_H
#define  PROTOCOL_H

/*----compilation control-----------------------------------------------------*/

/*******************************************************************************
INCLUDE FILES
*******************************************************************************/
/*----system and platform files-----------------------------------------------*/

/*----program files-----------------------------------------------------------*/
#include "WirelessChip.h"

/*******************************************************************************
FILE CONTEXT
*******************************************************************************/
/*----defines, typedefs, structs, unions, enums-------------------------------*/
#define FB_TOTAL        300
#define SlotRX	        (SlotEND / 2)		// bit window slot for RX hop frame
#define SlotEND		      (FB_TOTAL)	// bit window slot for END of frame
#define SlotTX          2		// bit window slot for TX frame

#define HOPS_PER_SECOND (RF_BITS_PER_SECOND / FB_TOTAL)  // 32 = 9600/300

#define MAX_BITS_IN_A_ROW 8

#define SlotFM	        (SlotRX + ((PR_PREAMBLE_SIZE * 8) - 4))		// bit window to sample frequency error (4 bits before end of preamble)
/***************************************************************************//**
* SlotSync is a define that is used by a transmitter to sync its RFFrameBitCounter
*  with the Receiver's counter.  So after the first byte of data the transmitter's
*  counter is set to the value of SlotSYNC.  SlotSYNC's value is derived from the
*  size of the preamble, sync words, and the first byte of data.  With the repeater
*  there is 32 extra bits which accounts for 16 bits of receive delay in addition
*  to 16 bits of shift register delay.
*******************************************************************************/
#if defined USING_1232_HARDWARE || defined DMP_1119_TRANSMITTER
#define SlotSYNC	      (SlotRX + ((PR_HEADER_SIZE + 1) * 8)) //214 = 150+((7+1)*8)		// bit window for start of sync
#else
#define SlotSYNC	      ((SlotRX + ((PR_HEADER_SIZE + 1) * 8)) + 32) //214 = 150+((7+1)*8)		// bit window for start of sync
#endif
#define SlotModOff	    (SlotRX - 3) // bit window slot for swiching mod off output only
#define SlotPROG	      (SlotEND - 1)			// bit window for start of programming
#define RECEIVER_FRAME_START_LIMIT 16

typedef signed int SlotNumber;

#define STARTING_DATA_SLOT  10 // the data slot starts at slot number 10
#define DATA_SLOT_PERIOD    16 // a data slot occurs every 16 slots (1/2s)
#define SLEEP_BIT_MULTIPLIER  (RF_BITS_PER_SECOND / SLEEP_TICKS_PER_SECOND) * 2

#define  SLOTS_PER_GROUP  16
#define  CHECKIN_SLOTS_PER_GROUP  5
#define  SLOTS_BETWEEN_CHECKIN_SLOTS  1
#define  TOTAL_SLOTS  (60 * HOPS_PER_SECOND)  //1920= 60*32
#define  TOTAL_GROUPS  TOTAL_SLOTS / SLOTS_PER_GROUP // 120=1920/16
#define  MAX_SLOT_NUMBER  TOTAL_SLOTS - 1 // 1919=1920-1
#define  INVALID_SLOT  TOTAL_SLOTS + 1// 1921 = 1920 + 1

// The invalid output and zone slots are used by the transmitter to know when to wake up when failing.  The transmitter
// will wake up approximately once a minute during this slot.  If either the zone or output is in sync with the receiver,
// the other functionaly needs to wake up in a pseudorandom slot so all transmitters in this state don't talk at the same time.
#define  INVALID_OUTPUT_SLOT (outputSerialNumber % TOTAL_SLOTS)
#define  INVALID_ZONE_SLOT (zoneSerialNumber % TOTAL_SLOTS)

#define  SLOTS_PER_OUTPUT_GROUP     480
#define  OUTPUT_SLOTS_PER_GROUP     30
#define  SLOW_OUTPUTS_PER_GROUP     25
#define  FAST_OUTPUTS_PER_GROUP     5
#define  SLOTS_BETWEEN_OUTPUT_SLOTS 16
#define  TOTAL_OUTPUT_GROUPS        4

#define  NUM_SLOTS_REPEATER_TRAILS 4 //this is the number of slots the repeater trails behind the receiver

typedef unsigned int SlotTransmitterNumber;

#define  TOTAL_SLOT_ZONES  (TOTAL_SLOTS / SLOTS_PER_GROUP) * CHECKIN_SLOTS_PER_GROUP //600 = (1920/16)*5
#define  TOTAL_SLOT_OUTPUTS ((FAST_OUTPUTS_PER_GROUP * TOTAL_OUTPUT_GROUPS) + (SLOW_OUTPUTS_PER_GROUP)) // 45 = (5*4)+25
#define  TOTAL_TRANSMITTER_SLOTS (TOTAL_SLOT_ZONES + TOTAL_SLOT_OUTPUTS) // 645 = 600 + 45
#define  INVALID_TRANSMITTER_NUMBER (TOTAL_TRANSMITTER_SLOTS + 1) // 646 = 645+1

#define  FIRST_SLOW_OUTPUT_ZONE 600
#define  FIRST_FAST_OUTPUT_ZONE 625
/*----data descriptions-------------------------------------------------------*/

/*----data declarations-------------------------------------------------------*/

/*----macros------------------------------------------------------------------*/

/*----function prototypes-----------------------------------------------------*/
extern SlotNumber subtractSlots(SlotNumber slot1, SlotNumber slot2);
extern SlotNumber nextSlot(SlotNumber slot);
extern SlotNumber zoneToSlotNumber(SlotTransmitterNumber zone);
extern SlotNumber OutputToSlotNumber(SlotTransmitterNumber output);
#ifdef DMP_1119_TRANSMITTER
extern SlotTransmitterNumber SlotToOutputNumber(SlotNumber slot);
#endif
extern SlotTransmitterNumber slotToZoneNumber(SlotNumber slot);
extern SlotNumber TransmitterToSlotNumber(SlotTransmitterNumber transmitter);
extern bool IsPrimarySlowOutputGroup(SlotNumber slot);
extern SlotNumber AddSlots(SlotNumber slot, SlotNumber numToAdd);
#endif                                  /* end of file */
