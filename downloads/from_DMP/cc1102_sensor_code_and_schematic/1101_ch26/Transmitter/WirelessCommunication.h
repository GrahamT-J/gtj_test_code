#ifndef WirelessCommunication_H
  #define WirelessCommunication_H

  #ifdef USING_CHIPCON_RF_CHIP
    #include "ChipconCommunication.h"
  #else
    #include "XemicsCommunication.h"
  #endif

#endif

