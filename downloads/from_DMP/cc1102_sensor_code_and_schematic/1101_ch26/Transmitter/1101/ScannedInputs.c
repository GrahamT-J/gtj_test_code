/*H*****************************************************************************
FILENAME:

DESCRIPTION:

NOTES:

Copyright (c) Digital Monitoring Products Inc. 2009, 2016 - 2017.  All rights reserved.

*****************************************************************************H*/
/*----compilation control-----------------------------------------------------*/

/*******************************************************************************
INCLUDE FILES
*******************************************************************************/
/*----system and platform files (external libraries) -------------------------*/
#include "types.h"

/*----program files-----------------------------------------------------------*/
#include "Sleep.h"
#include "transmitterprotocol.h"
#include "IOPortPins.h"
#include "Inputs.h"



/*******************************************************************************
EXTERNAL REFERENCES   Only used if not available in a header file.
*******************************************************************************/
/*----data declarations-------------------------------------------------------*/

extern ZoneFlags myZoneFlags;
/*----function prototype------------------------------------------------------*/

/*******************************************************************************
PRIVATE DECLARATIONS
*******************************************************************************/
/*----defines, typedefs, structs, unions, enums-------------------------------*/
// it takes this many transmits at low battery before low battery is shown
enum {LOW_BATTERY_COUNTER_THRESHHOLD = 10};

typedef struct
{
  BITFIELD lowBattery : 1;
  BITFIELD counter    : 7;
} LowBatteryState;

/*----data declarations-------------------------------------------------------*/
LowBatteryState lowBatteryState;
static ExtendedZoneMessageByte steadyZoneState;
static ExtendedZoneMessageByte intermZoneState;
static ExtendedZoneMessageByte delayZoneState;
static unsigned short disarmDelayEnableTimer = 0;

/*----function prototype------------------------------------------------------*/
static bool UpdateSteadyZoneState(bool stateChanged);

/*----macros------------------------------------------------------------------*/

/*******************************************************************************
PRIVATE FUNCTION DEFINITIONS
*******************************************************************************/
/*******************************************************************************

DESCRIPTION: Will process the newly scanned zone state and look for any transitions.
              If a transition is found the funtion will return true if new zone state
NOTES:

*******************************************************************************/
static bool ProcessNewZoneState(ExtendedZoneMessageByte newZoneState)
{
  bool changed = false;
  // if the new state is different from the state from the last scan.
  if ((newZoneState.zoneStateAsByte != intermZoneState.zoneStateAsByte)
      || (newZoneState.deviceState != intermZoneState.deviceState))
  {
    intermZoneState = newZoneState; // save the state if it is different
  }
  // if this scan is the same as the last scan see if it is different from the steady state
  else if ((delayZoneState.zoneStateAsByte != intermZoneState.zoneStateAsByte)
           || (delayZoneState.deviceState != intermZoneState.deviceState))
  {
    if (((steadyZoneState.deviceState & MB_TAMPER) > 0) || ((intermZoneState.deviceState & MB_TAMPER) > 0))
    {
      ackStatus.counter = 0;
    }
    delayZoneState = intermZoneState;
    changed = true;
  }
  return UpdateSteadyZoneState(changed);
}

/*******************************************************************************

DESCRIPTION:  Function checks for a change on the tamper, reed switch, and external
              contact
NOTES:  Returns true if a zone state has changed

*******************************************************************************/
static bool ScanInputsForChange(void)
{
  static ExtendedZoneMessageByte currentZoneState;

#ifdef USE_END_OF_LINE_RESISTOR
  // Setup to read contact
  CACTL1 = CAREF_1 | CAON; // Ref applied to the + terminal, Ref 0.25*Vcc, Enable Comparator
  CACTL2 = P2CA3;          // CA4 applied to the - terminal
#endif

  currentZoneState.deviceState = (ZoneMessageByte)0;
  currentZoneState.zoneStateAsByte = 0;

  if (!getIO(TAMPER))
    currentZoneState.deviceState = MB_TAMPER;

  // Zone one is the reed switch
  if (!getIO(REED_SW))
    currentZoneState.zoneOne = ZONE_STATE_SHORT;

#ifdef USE_END_OF_LINE_RESISTOR
  // Zone two is the end-of-line resistor zone which measures open/normal/short
  if (CACTL2 & CAOUT)
  {
    currentZoneState.zoneTwo = ZONE_STATE_OPEN;
  }
  else
  {
    CACTL1 = CAREF_2 | CAON; // Ref applied to the + terminal, Ref 0.50*Vcc, Enable Comparator
    currentZoneState.zoneTwo = ZONE_STATE_SHORT;
    if (CACTL2 & CAOUT)
      currentZoneState.zoneTwo = ZONE_STATE_NORMAL;
  }

  CACTL1 &= ~CAON;  // turn off comparator
#else
  // Zone two is the external contact

  // Disarm delay processing considers ZONE_STATE_NORMAL to be a restore, and anything else to be a
  // trip, so the internal zone state should account for normally-open / normally-closed programming.
  if (myZoneFlags.contactOneNormallyOpen)
  {
    currentZoneState.zoneTwo = (getIO(CONTACT_SW) ? ZONE_STATE_SHORT : ZONE_STATE_NORMAL);
  }
  else
  {
    currentZoneState.zoneTwo = ((!getIO(CONTACT_SW)) ? ZONE_STATE_SHORT : ZONE_STATE_NORMAL);
  }
#endif

  return ProcessNewZoneState(currentZoneState);
}

/*******************************************************************************

DESCRIPTION:

NOTES: This assumes that any edge interrupts on this port are for alarm contacts

*******************************************************************************/
extern void rfDataClockEdge(void);
#ifdef __ICC430__
#pragma vector = PORT1_VECTOR
#pragma type_attribute = __interrupt
#endif
void port1EdgeInterrupt(void)
#ifndef __ICC430__
__interrupt[PORT1_VECTOR]
#endif
{
  if (getIOIntFlag(RF_DATA_CLK))
  {
    clearIOIntFlag(RF_DATA_CLK);
    rfDataClockEdge();
  }
  else
  {
    P1IFG = 0;        // clear all edge interrupts
    P1IE &= RF_DATA_CLK_MASK; // disable all other interrupts
  }
}

/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
#ifdef __ICC430__
#pragma vector = TIMERA0_VECTOR
#pragma type_attribute = __interrupt
#endif
void debounceInterrupt(void)
#ifndef __ICC430__
__interrupt[TIMERA0_VECTOR]
#endif
{
  TACCTL0 = 0;
  if (!startupDelayComplete)
  {
    __low_power_mode_off_on_exit();
  }
  else if (!getCurrentlyCommunicating()) // only process a change if not already communicating
  {
    if (ScanInputsForChange())
    {
      if (wakeup(true, false))
      {
        __low_power_mode_off_on_exit();
      }
    }
    else
    {
      startInputDebounce(DEBOUNCE_TIME);
    }
  }
}

/*******************************************************************************
PUBLIC FUNCTION DEFINITIONS
*******************************************************************************/
/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
void initInputs(bool hardReset)
{
#ifndef USE_END_OF_LINE_RESISTOR
  setIODirIn(CONTACT_SW);
#endif
  setIODirIn(REED_SW);
  setIODirIn(TAMPER);
  setIODirIn(BATTERY_VOLTAGE);

  // setup the voltage reference enable pin for low battery detection
  setIODirOut(BATT_VOLTAGE_ENABLE);
  clearIOSel(BATT_VOLTAGE_ENABLE);
  clearIO(BATT_VOLTAGE_ENABLE);

#ifdef USE_END_OF_LINE_RESISTOR
  CAPD = BATTERY_VOLTAGE_MASK | CONTACT_SW_MASK; // Input buffers P2.2 and P2.3 disabled
#else
  CAPD = BATTERY_VOLTAGE_MASK;
#endif

  if (hardReset)
    StartupDelay();

  startInputDebounce(DEBOUNCE_TIME);
}

/*******************************************************************************

DESCRIPTION:  Starts debounce timer so it will interrupt at debounce timer plus
  the amount of time for it to start at an appropriate hop

NOTES:  If SLEEP_TICKS_PER_HOP ever becomes a value that is not a power of two, the
  modulo operator will take much longer and this will probably need to change

*******************************************************************************/
void startInputDebounce(short debounceTime)
{
  if (! (TACCTL0 & CCIE)) // if not already debouncing
  {
    unsigned short tempTar = TAR;
    TACCR0 = tempTar + debounceTime + // current time plus debounce time plus
             SLEEP_TICKS_PER_HOP -      // an exra hop minus
             WAKEUP_CLOCK_SLOP_TICKS -  // some padding for clock slop minus
             ((tempTar - sleepTime) % SLEEP_TICKS_PER_HOP); // how far off we currently are from start of hop
    TACCTL0 = CCIE;  // enable interrupt and set compare mode
  }
}

/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
void testBattery(void)
{
  if (CACTL2 & CAOUT)
  {
    if (lowBatteryState.counter < LOW_BATTERY_COUNTER_THRESHHOLD)
    {
      lowBatteryState.counter++;
    }
    else
    {
      lowBatteryState.lowBattery = true;
    }
  }
  else      // a single "good" resets everything
  {
    lowBatteryState.counter = 0;
    lowBatteryState.lowBattery = false;
  }
}

/*******************************************************************************

DESCRIPTION: Function will return the current steady state for the zone

NOTES:

*******************************************************************************/
#ifdef USE_EXTENDED_ZONE_STATE
ExtendedZoneMessageByte getInputState(void)
{
  ExtendedZoneMessageByte state;

  state.deviceState = steadyZoneState.deviceState;
  state.zoneStateAsByte = steadyZoneState.zoneStateAsByte;
  if (lowBatteryState.lowBattery)
    state.deviceState |= MB_LOW_BATTERY;
  if (!getAlarmWakeup())
    state.deviceState |= MB_CHECKIN;

  return state;
}
#else
ZoneMessageByte getInputState(void)
{
  ZoneMessageByte state = (ZoneMessageByte)0;

  if (steadyZoneState.zoneOne != ZONE_STATE_NORMAL)
  {
    state |= MB_ALARM_0;
  }

  if (steadyZoneState.zoneTwo != ZONE_STATE_NORMAL)
  {
    state |= MB_ALARM_1;
  }

  if (myZoneFlags.contactOneNormallyOpen)
  {
    // The receiver will invert the alarm bit for non-extended zone state messages if the zone is programmed
    // as normally-open.  Since we inverted the state when reading the inputs, invert again to send the state
    // that the receiver expects.
    state ^= MB_ALARM_1;
  }

  if (steadyZoneState.deviceState & MB_TAMPER)
  {
    state |= MB_TAMPER;
  }

  if (lowBatteryState.lowBattery)
  {
    state |= MB_LOW_BATTERY;
  }

  if (!getAlarmWakeup())
  {
    state |= MB_CHECKIN;
  }

  return state;
}
#endif

/*F*****************************************************************************
*
* Name: EnableDisarmDelay()
* Description:  Enable or disable disarm delay depending on given flag. If
*               disarm delay is enabled then a short on the contact is not
*               reported for 20 seconds. If the contact restores before 20
*               seconds then the short is not reported.
* Parameters:   - enable indicates if disarm delay should be enabled
* Return value: none
*
*****************************************************************************F*/
void EnableDisarmDelay(bool enable)
{
  if (enable)
    disarmDelayEnableTimer = DISARM_DELAY_180_SECONDS;
  else if (disarmDelayEnableTimer > 0)
    disarmDelayEnableTimer = DISARM_DELAY_1_SECOND;
  // Note: The timer is not disabled here because disabling the timer will 
  // cause a change to the contact state if a short is being delayed. We
  // do not want a state change at this point because the transmitter may
  // need to reply to a check-in. So delay the state change for 1 second.
}

/*F*****************************************************************************
*
* Name: UpdateSteadyZoneState()
* Description:  This function is called every 250 ms. If disarm delay is 
*               disabled then simply set steadyZoneState to the new state. 
*               Otherwise contact shorts must be delayed for 20 seconds. If a
*               contact is shorted for less than 20 seconds then steadyZoneState
*               will not be changed. This prevents the change from being sent to
*               the 1100 receiver which will extend battery life. The panel will
*               enable disarm delay while the panel is disarmed.
* Parameters:   none
* Return value: true if steadyZoneState was changed
*
*****************************************************************************F*/
static bool UpdateSteadyZoneState(bool stateChanged)
{
  static byte delayTimer = 0;
  bool changed = false;
  
  // Check if tamper state changed. Changes to tamper state are not delayed.
  if (stateChanged && (steadyZoneState.deviceState != delayZoneState.deviceState))
  {
    changed = true;
    steadyZoneState.deviceState = delayZoneState.deviceState;
  }

  // If disarm delay enable timer is running then disarm delay is enabled.
  if (disarmDelayEnableTimer > 0)
  {
    bool delayTimerWasRunning = (delayTimer > 0);
    
    if (--disarmDelayEnableTimer == 0)
      delayTimer = 0;
    else if (delayTimer > 0)
      delayTimer--;
    
    // If the delay timer expired then update steadyZoneState
    if (delayTimerWasRunning && (delayTimer == 0))
    {
      if (steadyZoneState.zoneStateAsByte != delayZoneState.zoneStateAsByte)
      {
        changed = true;
        steadyZoneState.zoneStateAsByte = delayZoneState.zoneStateAsByte;
      }
    }
  }

  // Check if disarm delay is enabled
  if (disarmDelayEnableTimer > 0)
  {
    if (stateChanged)
    {
      // If the reed switch is not shorted then update steadyZoneState
      if ((delayZoneState.zoneOne != ZONE_STATE_SHORT) && (steadyZoneState.zoneOne == ZONE_STATE_SHORT))
      {
        changed = true;
        steadyZoneState.zoneOne = delayZoneState.zoneOne;
      }

      // If the external contact is faulted and just changed state then update steadyZoneState
      if ((delayZoneState.zoneTwo != steadyZoneState.zoneTwo) && (steadyZoneState.zoneTwo != ZONE_STATE_NORMAL))
      {
        changed = true;
        steadyZoneState.zoneTwo = delayZoneState.zoneTwo;
      }

      // If internal contact was shorted or external contact was faulted then start the delay timer
      if (((delayZoneState.zoneOne == ZONE_STATE_SHORT) && (steadyZoneState.zoneOne != ZONE_STATE_SHORT)) ||
          ((delayZoneState.zoneTwo != ZONE_STATE_NORMAL) && (steadyZoneState.zoneTwo == ZONE_STATE_NORMAL)))
      {
        if (delayTimer == 0)
          delayTimer = DISARM_DELAY_20_SECONDS;
      }
      else
      {
        delayTimer = 0;
      }
    }
  }
  else  // disarm delay is disabled
  {
    if (stateChanged && (steadyZoneState.zoneStateAsByte != delayZoneState.zoneStateAsByte))
    {
      changed = true;
      steadyZoneState.zoneStateAsByte = delayZoneState.zoneStateAsByte;
    }
  }

  return changed;
}
