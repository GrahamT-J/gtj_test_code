/*H*****************************************************************************
FILENAME: Packet.c

DESCRIPTION: Info relating to the data packet transferred from transmitter to
  receiver and back

NOTES:

Copyright (c) Digital Monitoring Products Inc. 2007.  All rights reserved.

*****************************************************************************H*/
/*----compilation control-----------------------------------------------------*/

/*******************************************************************************
INCLUDE FILES
*******************************************************************************/
/*----system and platform files (external libraries) -------------------------*/

/*----program files-----------------------------------------------------------*/
#include "Packet.h"

/*******************************************************************************
EXTERNAL REFERENCES   Only used if not available in a header file.
*******************************************************************************/
/*----data declarations-------------------------------------------------------*/

/*----function prototype------------------------------------------------------*/


/*******************************************************************************
PRIVATE DECLARATIONS
*******************************************************************************/
/*----defines, typedefs, structs, unions, enums-------------------------------*/

/*----data declarations-------------------------------------------------------*/
const char receiverHeaderBuffer[PR_HEADER_SIZE] =
{
  0xAA,
  0xAA,
  0xAA,
  0xAA,
  RPAT_HIGH,
  RPAT_MIDDLE,
  RPAT_LOW
};

const char transmitterHeaderBuffer[PT_HEADER_SIZE] =
{
  0xAA,
  0xAA,
  0xAA,
  0xAA,
  TPAT_HIGH,
  TPAT_MIDDLE,
  TPAT_LOW
};

char receiverPacketBuffer[PR_SIZE];
char transmitterPacketBuffer[PT_SIZE];
char receiverPacketSize;
char transmitterPacketSize = 10; // todo: set correctly

#ifdef DMP_BUILDING
TransmitterDiagnostics diag;
#endif
#ifdef DMP_1100_PRODUCTION_TEST
ZoneMessageByte diag;
#endif
#ifdef DMP_PRODUCTION_TEST
char diag;
#endif

/*----function prototype------------------------------------------------------*/

/*----macros------------------------------------------------------------------*/

/*******************************************************************************
PRIVATE FUNCTION DEFINITIONS
*******************************************************************************/
/*******************************************************************************

DESCRIPTION: Calculates CRC based for a word buffer containing characters that is
  a fixed length

NOTES:  BufferPtr is start.

*******************************************************************************/
static word calcWirelessCRC(char *bufferPtr, char length, unsigned short crc)
{
  char i;

  for (i = 0; i < length; ++i)
  {
    calcCRC16ForChar(*bufferPtr, &crc);
    bufferPtr++;
  }
  return (crc);
}



/*******************************************************************************
PUBLIC FUNCTION DEFINITIONS
*******************************************************************************/
/*******************************************************************************

DESCRIPTION: Calculates CRC based on:

             CRC-16 , X^16 + X^15 + X^2 + 1

NOTES:

*******************************************************************************/
void calcCRC16ForChar(char value, unsigned short* crc)
{
  char i;
  unsigned short tempCrc = *crc ^ value;

  for (i = 0; i < 8; ++i)
  {
    if (tempCrc & 0x01)
    {
      (tempCrc) >>= 1;
      (tempCrc) ^= 0xa001;
    }
    else
    {
      (tempCrc) >>= 1;
    }
  }
  *crc = tempCrc;
}

/*******************************************************************************

DESCRIPTION:

NOTES:  length is the number of bytes we want including CRC

*******************************************************************************/
void encodeWirelessBuffer(char *buffer, byte length, unsigned short crc)
{
  // add CRC to end
  unsigned short crcResult = calcWirelessCRC(buffer, length - 2, crc);
  buffer[length - 2] = crcResult >> 8;
  buffer[length - 1] = (char)crcResult;
}

/*******************************************************************************

DESCRIPTION:

NOTES:  length is the number of bytes we want including CRC

*******************************************************************************/
bool decodeWirelessBuffer(char *buffer, byte length, unsigned short crc)
{
  // check CRC and return true if correct
  return calcWirelessCRC(buffer, length - 2, crc) == buffer[length - 1] + (buffer[length - 2] << 8);
}

