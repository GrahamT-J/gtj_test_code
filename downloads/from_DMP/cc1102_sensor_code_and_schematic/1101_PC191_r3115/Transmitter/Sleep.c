/*******************************************************************************
FILENAME:

DESCRIPTION:

NOTES:

Copyright (c) Digital Monitoring Products Inc. 2007.  All rights reserved.

*******************************************************************************/
/*----compilation control-----------------------------------------------------*/

/*******************************************************************************
INCLUDE FILES
*******************************************************************************/
/*----system and platform files (external libraries) -------------------------*/

/*----program files-----------------------------------------------------------*/
#include "Sleep.h"
#include "IndicatorLED.h"
#include "TransmitterProtocol.h"
#include "Main.h"
#include "ioportpins.h"
#include "Debug.h"
#include "Inputs.h"
#include "WirelessCommunication.h"

/*******************************************************************************
EXTERNAL REFERENCES   Only used if not available in a header file.
*******************************************************************************/
/*----data declarations-------------------------------------------------------*/

/*----function prototype------------------------------------------------------*/


/*******************************************************************************
PRIVATE DECLARATIONS
*******************************************************************************/
/*----defines, typedefs, structs, unions, enums-------------------------------*/
typedef struct
{
  BITFIELD sleeping                 : 1;
  BITFIELD currentlyCommunicating   : 1;  // set if not done trying to send a message
  BITFIELD receiverFailed           : 1;
  BITFIELD receiveCommand           : 1;
  BITFIELD                          : 4;
  char wakeupAttempt;
  WakeupReason wakeupReason;
} SleepStatus;

enum
{
#ifdef DMP_1145_TRANSMITTER
  MAX_WAKEUP_ATTEMPTS = 10
#else
  MAX_WAKEUP_ATTEMPTS = 60
#endif
};
#define COMMAND_TIMEOUT 20

/*----data declarations-------------------------------------------------------*/
static SleepStatus sleepStatus;
#ifdef ENABLE_RECEIVER_COMMANDS
#ifdef ENABLE_COMMAND_CANCEL
static unsigned char commandTimeoutCounter = 0;
#endif
#endif
word sleepTime = 0;

/*----function prototype------------------------------------------------------*/

/*----macros------------------------------------------------------------------*/

/*******************************************************************************
PRIVATE FUNCTION DEFINITIONS
*******************************************************************************/
/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
bool getCurrentlyCommunicating(void)
{
  return sleepStatus.currentlyCommunicating;
}

/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
bool getSleeping(void)
{
  return sleepStatus.sleeping;
}

/*F****************************************************************************
*
* Name: GetKeypadWakeup()
* Description:  Returns the reason for the current wakeup.
* Parameters:   None
* Return value: - WR_ALARM: unscheduled wakeup to send an alarm
*               - WR_CHECKIN: scheduled wakeup on our checkin slot
*               - WR_KEYPAD: scheduled wakeup to listen to the keypad slot
*
*****************************************************************************F*/
WakeupReason GetWakeupReason(void)
{
  return sleepStatus.wakeupReason;
}


/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
static void processSuccessfulWakeup(void)
{
  sleepStatus.wakeupAttempt = 0;
  sleepStatus.receiverFailed = false;
}

/*******************************************************************************

DESCRIPTION:  Returns a standard or somewhat random sleep interval, based upon
various bits of the serial number.  Starts out waiting up to 1/4 a second and
moves out to 2 seconds, if needed.

NOTES:

Try     Mask      Sleep from 1 cycle to ...
---     ----      -------------------------
1       0x07      8
2       0x07      8
3       0x07      8
4       0x07      8
5       0x0F      16
6       0x1F      32
7       0x3F      64
8       0x3F
9       0x3F
10      0x3F
11      0x3F
12      0x3F
13      0x3F
14      0x3F
15      0x3F
16      0x3F

*******************************************************************************/

static unsigned short getSleepInterval(bool normalSleep, bool *timedOut)
{
  enum
  {
    MIN_WAIT_MASK   = 0x07,
    MAX_WAIT_MASK_NUM_BITS = 6,
    SERIAL_NUM_BITS = 24
  };
  static char numTries;
  char mask;
  char interval;

  if ((normalSleep) || (numTries > SERIAL_NUM_BITS - MAX_WAIT_MASK_NUM_BITS))
  {
    unsigned int sleepTicksForCheckin = getSleepTicks();

    numTries = 0;
    *timedOut = true;
#ifdef ENABLE_RECEIVER_COMMANDS
    {
      unsigned int sleepTicksForCommand = getSleepTicksForCommand();

#ifdef ENABLE_COMMAND_CANCEL
      //if we are setup to receive a command and a command slot is going to occur before our checkin slot
      if ((sleepStatus.receiveCommand) && (sleepTicksForCommand < sleepTicksForCheckin))
        return sleepTicksForCommand;
#else
      if ((sleepTicksForCommand < sleepTicksForCheckin) && IsFastOutput())
      {
        sleepStatus.receiveCommand = true;
        return sleepTicksForCommand;
      }
      sleepStatus.receiveCommand = false;
#endif
    }
#endif
    return sleepTicksForCheckin;
  }

  *timedOut = false;

  if (numTries <= MAX_WAIT_MASK_NUM_BITS)
    mask = (1 << numTries);
  else
    mask = (1 << MAX_WAIT_MASK_NUM_BITS);

  mask--;
  mask |= MIN_WAIT_MASK;

  interval = ((serialNumber >> numTries) & mask) + 1;

  ++numTries;

  printf("getSleepInterval: ret=%u, interval=%u\n", (interval * SLEEP_TICKS_PER_HOP), interval);
  return (interval * SLEEP_TICKS_PER_HOP);
}

/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
static void FindChannel(void)
{
  char channelSkip;

  ENTER_CRITICAL_SECTION();

  // calculate the number of channels that have passed since we went to sleep
  channelSkip = (sleepTime % SLEEP_TICKS_PER_HOP_ROUND) / SLEEP_TICKS_PER_HOP;
  // add the number of skipped channels into the current channel plus one to account
  // for the fact that we are waking in the slot prior to the indented slot due to us
  // accounting for clock slop.
  currentChannel = (unsigned short)(currentChannel + ((channelSkip+1) * houseCode)) % NUM_HOP_CHANNELS;

  EXIT_CRITICAL_SECTION();

#if !defined(__linux__)
  CCR2 = TAR + RFBitTime;
  CCTL2 = OUTMOD_5+CM0+CCIE; // CC2 = Compare, Reset
#endif
}


/*******************************************************************************
PUBLIC FUNCTION DEFINITIONS
*******************************************************************************/
/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
bool receiverFailed(void)
{
  return sleepStatus.receiverFailed;
}

/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
bool getCommandStatus(void)
{
  return sleepStatus.receiveCommand;
}

/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
#ifdef ENABLE_RECEIVER_COMMANDS
#ifdef ENABLE_COMMAND_CANCEL
void CancelCommandReceive(void)
{
  sleepStatus.receiveCommand = false;
  commandTimeoutCounter = 0;
}
#endif
#endif
/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
void goToSleep(TransmitStatus status)
{
  unsigned short sleepInterval = 0;
  char slotOffset;
  bool timedOut = false;
  bool hardSleep = true;  // set if we went to sleep for more than a short retry time
  unsigned int sleepTicksUntilKeypadSlot;

  //printf("goToSleep:=%d\n", status);
  ENTER_CRITICAL_SECTION();

  switch (status)
  {
    case TS_SUCCESS:
#ifdef ENABLE_RECEIVER_COMMANDS
#ifdef ENABLE_COMMAND_CANCEL
      if (!sleepStatus.alarmWakeup && (commandTimeoutCounter > 0))
      {
        if (--commandTimeoutCounter == 0)
        {
          sleepStatus.receiveCommand = false;
        }
      }
#endif
#endif
      if (sleepStatus.wakeupReason == WR_KEYPAD)
      {
        //printf("wakeup: keypad\n");
        ProcessIndicatorState();
      }
      processSuccessfulWakeup();
      sleepStatus.currentlyCommunicating = false;
      sleepInterval = getSleepInterval(true, &timedOut);
      timedOut = false; // override timeout

      sleepTicksUntilKeypadSlot = GetSleepTicksUntilKeypadSlot();
      if (sleepTicksUntilKeypadSlot < sleepInterval)
      {
        sleepInterval = sleepTicksUntilKeypadSlot;
        sleepStatus.wakeupReason = WR_KEYPAD;
      }
      else
      {
        sleepStatus.wakeupReason = WR_CHECKIN;
      }
      break;

    case TS_FAILURE:
      sleepStatus.currentlyCommunicating = false;
      sleepStatus.wakeupReason = WR_CHECKIN;
      sleepInterval = getSleepInterval(true, &timedOut);
      timedOut = true; // override timeout
#ifdef ENABLE_RECEIVER_COMMANDS
#ifdef ENABLE_COMMAND_CANCEL
      CancelCommandReceive();
#endif
#endif
      break;

    case TS_RETRY:
      sleepInterval = getSleepInterval(false, &timedOut);
      if (timedOut)
      {
        sleepStatus.currentlyCommunicating = false;
        sleepStatus.wakeupReason = WR_CHECKIN;
      }
      hardSleep = false;
      break;
  }

  initializeCommunicationStatus();
  processMainShutdown();

  clearDebug(DM_1101_LED);
  setupInputs();

#if !defined(__linux__)
  TACTL = TASSEL_1 + // ACLK
          MC_2 + // continuous
          SLEEP_TIMERA_DIVIDER_SETTING * ID0;
#endif
#ifdef DMP_1142_TRANSMITTER
    if (ackStatus.panicAckWaiting && (status == TS_SUCCESS))
    {
      startPanicPulsing();
      ackStatus.panicAckWaiting = false;
    }
#endif

  // Using RFFrameBitCounter, calculate the number of sleep ticks are into the current slot
  slotOffset = RFFrameBitCounter * SLEEP_TICKS_PER_HOP / FB_TOTAL;
  // Since RFFrameBitCounter ranges from 150 to 300 in RX and 0 to 150 in TX we need
  // to normalize slotOffset so it is truely the number of sleep ticks into the current slot
  slotOffset = ((slotOffset + (SLEEP_TICKS_PER_HOP / 2)) % SLEEP_TICKS_PER_HOP);
  // Subtract slotOffset from our current time to get the reference for the begining
  // of the current slot, record this as the time we went to sleep.
#if !defined(__linux__)
  sleepTime = TAR - slotOffset;
  // Using the reference of the begining of the current slot, calculate how long
  // we need to sleep and subtract clock slop to assure we wake up early.
  CCR2 = sleepTime + sleepInterval - WAKEUP_CLOCK_SLOP_TICKS;
  CCTL2 = CCIE;   // compare timer with interrupt enabled
#endif
  sleepStatus.sleeping = true;
#if !defined(__linux__)
  startInputDebounce(DEBOUNCE_TIME);  // schedule wakeup after debounce time, in case we have changed states
  WDTCTL = WDTPW | WDTHOLD | WDTCNTCL | WDTSSEL;  // stop watchdog
#endif
  // the call to LPM3 must follow this intstruction for proper operation
  EXIT_CRITICAL_SECTION();
  // with the MSP430 the instruction following the EINT instruction is guaranteed to
  // execute before interrupts are enabled.
#if !defined(__linux__)
  (void)LPM3;           // Enter LPM3 until timer or contact change
  _NOP();         // for debugger
  WDTCTL = WDTPW | WDTCNTCL | WDTSSEL;  // start watchdog, reset, and use ACLK
#endif
  // start over almost fresh, since we are not communicating
  if (timedOut)
  {
    resetLearnedInformation();
#ifdef DMP_1119_TRANSMITTER
    // There is no need to reset the hardware if the transmitter is able to communicate
    if (BothTransmitterNumbersInvalid())
#endif
      resetHardware(false);
  }

  FindChannel();
  processMainWakeup(hardSleep);
}

/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
bool wakeup(bool isAlarmWakeup, bool commandReceiveRequest)
{
  bool allowWakeup = true;

#if defined USING_1232_HARDWARE || defined DMP_1119_TRANSMITTER
  if (isAlarmWakeup)
    sleepStatus.alarmWakeup = true;
#endif

  if (!sleepStatus.currentlyCommunicating)
  {
    if (isAlarmWakeup)
    {
      sleepStatus.wakeupReason = WR_ALARM;
    }

#ifdef DMP_1145_TRANSMITTER
    if (sleepStatus.wakeupAttempt < MAX_WAKEUP_ATTEMPTS) // if counter is under the max valu
      sleepStatus.wakeupAttempt ++; // increment
    if (sleepStatus.wakeupAttempt >= MAX_WAKEUP_ATTEMPTS) // if we have reached the max number of attempts
      sleepStatus.receiverFailed = true;  // declare we have failed
#else
    if (++sleepStatus.wakeupAttempt >= MAX_WAKEUP_ATTEMPTS)
    {
      sleepStatus.wakeupAttempt = 0;
      sleepStatus.receiverFailed = true;
    }
#endif
  }

  if ((sleepStatus.wakeupReason != WR_ALARM) &&
      (sleepStatus.receiverFailed) &&
      (sleepStatus.wakeupAttempt > 0))
  {
    allowWakeup = false;
  }

  if (allowWakeup)
  {
#if !defined(__linux__)
    sleepTime = TAR - sleepTime;
#endif
    setDebug(DM_1101_LED);
#ifndef USING_CHIPCON_RF_CHIP
    TACTL = TASSEL_2 +    // use DCO clock
            MC_2;         // continuous

    CCTL2 = 0;   // no interrupt at first, will start later
#endif

#if !defined(__linux__)
#if defined(USING_CHIPCON_FIFO)
    TACTL = TASSEL_2 +    // use DCO clock
            MC_2;         // continuous

    CCTL2 = 0;   // no interrupt at first, will start later
    CCTL1 = 0;
#endif
#endif
    sleepStatus.sleeping = false;
    sleepStatus.currentlyCommunicating = true;
#ifdef ENABLE_RECEIVER_COMMANDS
    if (commandReceiveRequest)
    {
      sleepStatus.receiveCommand = true;
#ifdef ENABLE_COMMAND_CANCEL
      commandTimeoutCounter = COMMAND_TIMEOUT;
#endif
    }
#endif
  }

  return allowWakeup;
}



