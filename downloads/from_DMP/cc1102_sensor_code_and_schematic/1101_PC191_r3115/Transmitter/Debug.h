/*H*****************************************************************************
FILENAME:

DESCRIPTION:

NOTES:

Copyright (c) Digital Monitoring Products Inc. 2004.  All rights reserved.

*****************************************************************************H*/
#ifndef  DEBUG_H
#define  DEBUG_H

#ifdef USING_RECEIVER_HARDWARE
  #define USE_DEBUG_PORT
#endif

/*----compilation control-----------------------------------------------------*/

/*******************************************************************************
INCLUDE FILES
*******************************************************************************/
/*----system and platform files-----------------------------------------------*/
#include "types.h"
#include "ioportpins.h"
/*----program files-----------------------------------------------------------*/

/*******************************************************************************
FILE CONTEXT
*******************************************************************************/
/*----defines, typedefs, structs, unions, enums-------------------------------*/
typedef enum
{
  DS_OFF,
  DS_ON,
  DS_TOGGLE
} DebugState;

typedef enum
{
  DM_NONE = 0,
  DM_1101_LED = 0x80,
  DM_LED1 = 0x40,
  DM_LED2 = 0x20,
  DM_LED3 = 0x10,
  DM_LED4 = 0x08,
  DM_LED5 = 0x04,
  DM_LED6 = 0x02,
  DM_LED7 = 0x01,
  DM_ALL = 0xFF
} DebugMask;

#ifdef USE_DEBUG_PORT
  #define DEBUG_OUT           P5OUT
  #define DEBUG_DIR           P5DIR
  #define setDebug(MASK) DEBUG_OUT &= ~(MASK)
  #define clearDebug(MASK) DEBUG_OUT |= (MASK)
  #define toggleDebug(MASK) DEBUG_OUT ^= (MASK)
  #define initDebugInterface()    DEBUG_DIR = DM_ALL, clearDebug(DM_ALL)

#else
  #define setDebug(MASK)
  #define clearDebug(MASK)
  #define toggleDebug(MASK)
  #define initDebugInterface()
#endif

/*----data descriptions-------------------------------------------------------*/

/*----data declarations-------------------------------------------------------*/

/*----macros------------------------------------------------------------------*/

/*----function prototypes-----------------------------------------------------*/

#endif                                  /* end of file */
