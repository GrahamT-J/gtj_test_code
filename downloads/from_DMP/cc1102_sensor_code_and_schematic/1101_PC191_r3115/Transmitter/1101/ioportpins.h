  /*******************************************************************************
FILENAME: IOPortPins.h

DESCRIPTION:

NOTES:

Copyright (c) Digital Monitoring Products Inc. 2009.  All rights reserved.

*******************************************************************************/

#ifndef IOPORTPINS_H
#define IOPORTPINS_H

#if !defined(__linux__)
#include "BitMacros.h"
#include "MSP430SFR.h"

#define LED_ON(A)  setIO(A)
#define LED_OFF(A) clearIO(A)
//-------------------------------------------------

#define DEBUG_PIN_MASK            BIT2
#define DEBUG_PIN_IN              P2IN
#define DEBUG_PIN_OUT             P2OUT
#define DEBUG_PIN_DIR             P2DIR
#define DEBUG_PIN_SEL             P2SEL

#define REED_SW_MASK              BIT6
#define REED_SW_IN                P1IN
#define REED_SW_OUT               P1OUT
#define REED_SW_DIR               P1DIR
#define REED_SW_SEL               P1SEL
#define REED_SW_IES               P1IES
#define REED_SW_IE                P1IE
#define REED_SW_IFG               P1IFG
#define REED_SW_REN               P1REN

#define BATTERY_VOLTAGE_MASK      BIT3
#define BATTERY_VOLTAGE_DIR       P2DIR
#define BATTERY_VOLTAGE_IN        P2IN
#define BATTERY_VOLTAGE_SEL       P2SEL

#define BATT_VOLTAGE_ENABLE_MASK  BIT5
#define BATT_VOLTAGE_ENABLE_DIR   P2DIR
#define BATT_VOLTAGE_ENABLE_OUT   P2OUT
#define BATT_VOLTAGE_ENABLE_SEL   P2SEL

#define RED_LED_MASK	          BIT1
#define RED_LED_OUT               P2OUT
#define RED_LED_DIR               P2DIR
#define RED_LED_SEL               P2SEL
#define RED_LED_IN                P2IN

#define CSn_MASK                  BIT0
#define CSn_IN                    P1IN
#define CSn_OUT                   P1OUT
#define CSn_DIR                   P1DIR
#define CSn_SEL                   P1SEL

#define TAMPER_MASK               BIT1
#define TAMPER_IN                 P1IN
#define TAMPER_OUT                P1OUT
#define TAMPER_DIR                P1DIR
#define TAMPER_SEL                P1SEL
#define TAMPER_IES                P1IES
#define TAMPER_IE                 P1IE
#define TAMPER_IFG                P1IFG
#define TAMPER_REN                P1REN

#define SI_MASK                   BIT2
#define SI_IN                     P1IN
#define SI_OUT                    P1OUT
#define SI_DIR                    P1DIR
#define SI_SEL                    P1SEL

#define SCLK_MASK                 BIT0
#define SCLK_IN                   P2IN
#define SCLK_OUT                  P2OUT
#define SCLK_DIR                  P2DIR
#define SCLK_SEL                  P2SEL

#define SO_GDO1_MASK              BIT3
#define SO_GDO1_IN                P1IN
#define SO_GDO1_DIR               P1DIR
#define SO_GDO1_SEL               P1SEL
#define SO_GDO1_IE                P1IE
#define SO_GDO1_IES               P1IES
#define S0_GDO1_IFG               P1IFG

#define RF_DATA_CLK_MASK          SO_GDO1_MASK
#define RF_DATA_CLK_IN            SO_GDO1_IN
#define RF_DATA_CLK_DIR           SO_GDO1_DIR
#define RF_DATA_CLK_SEL           SO_GDO1_SEL
#define RF_DATA_CLK_IE            SO_GDO1_IE
#define RF_DATA_CLK_IES           SO_GDO1_IES
#define RF_DATA_CLK_IFG           S0_GDO1_IFG

#define CONTACT_SW_MASK           BIT2
#define CONTACT_SW_IN             P2IN
#define CONTACT_SW_OUT            P2OUT
#define CONTACT_SW_DIR            P2DIR
#define CONTACT_SW_SEL            P2SEL
#define CONTACT_SW_IES            P2IES
#define CONTACT_SW_IE             P2IE
#define CONTACT_SW_IFG            P2IFG
#define CONTACT_SW_REN            P2REN

#define GDO0_MASK                 BIT4
#define GDO0_IN                   P2IN
#define GDO0_DIR                  P2DIR
#define GDO0_SEL                  P2SEL
#define GDO0_OUT                  P2OUT
//#define GDO0_IE                   P2IE
//#define GDO0_IES                  P2IES
//#define GDO0_IFG                  P2IFG

#define RF_DATA_IN_MASK           GDO0_MASK
#define RF_DATA_IN_IN             GDO0_IN
#define RF_DATA_IN_DIR            GDO0_DIR
#define RF_DATA_IN_SEL            GDO0_SEL
//#define RF_DATA_IN_IE             GDO0_IE
//#define RF_DATA_IN_IES            GDO0_IES
//#define RF_DATA_IN_IFG            GDO0_IFG

#define RF_DATA_OUT_MASK          GDO0_MASK
#define RF_DATA_OUT_IN            GDO0_IN
#define RF_DATA_OUT_DIR           GDO0_DIR
#define RF_DATA_OUT_SEL           GDO0_SEL
#define RF_DATA_OUT_OUT           GDO0_OUT

#ifdef DMP_FCC_TEST
#define FCC_CHANNEL_CHANGE_MASK   BIT6
#define FCC_CHANNEL_CHANGE_IN     P1IN
#define FCC_CHANNEL_CHANGE_DIR    P1DIR
#define FCC_CHANNEL_CHANGE_SEL    P1SEL
#define FCC_CHANNEL_CHANGE_REN    P1REN

#define FCC_STATE_CHANGE_MASK     BIT1
#define FCC_STATE_CHANGE_IN       P1IN
#define FCC_STATE_CHANGE_DIR      P1DIR
#define FCC_STATE_CHANGE_SEL      P1SEL
#define FCC_STATE_CHANGE_REN      P1REN
#endif

#else // __linux__
#include "BitMacros.h"
#include "MSP430SFR.h"

#define LED_ON(A)  setIO(A)
#define LED_OFF(A) clearIO(A)

// Unused inputs are set to gpio 0 and 0 will be returned
#define TAMPER      0
#define REED_SW     0
#define CONTACT_SW  0
#define RF_DATA_IN  0

#define GDO0        70 // GPIO num
#define GDO2        69 // GPIO num
#endif

#endif
