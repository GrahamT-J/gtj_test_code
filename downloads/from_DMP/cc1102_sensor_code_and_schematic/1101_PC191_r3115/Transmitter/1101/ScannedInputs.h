/*H*****************************************************************************
FILENAME:

DESCRIPTION:

NOTES:

Copyright (c) Digital Monitoring Products Inc. 2009, 2016 - 2017.  All rights reserved.

*****************************************************************************H*/
#ifndef  SCANNED_INPUTS_H
#define  SCANNED_INPUTS_H

/*----compilation control-----------------------------------------------------*/
#include "Packet.h"
#include "Sleep.h"
/*******************************************************************************
INCLUDE FILES   Note: Minimize nesting of header files.
*******************************************************************************/
/*----system and platform files-----------------------------------------------*/

/*----program files-----------------------------------------------------------*/

/*******************************************************************************
FILE CONTEXT
*******************************************************************************/
/*----defines, typedefs, structs, unions, enums-------------------------------*/
// note, for proper sync, the debounce time must equal an integral number of hops
// with no fraction.
// For example, with 32 hops per second and a debounce of 500mS, there are exactly
// 16 hops during debounce.
enum
{
#ifdef DMP_1100_PRODUCTION_TEST
  DEBOUNCE_MS = 1500,
#else
  DEBOUNCE_MS = 250,
#endif
  DEBOUNCE_TIME = ((long)DEBOUNCE_MS * SLEEP_TICKS_PER_SECOND) / 1000,
  DISARM_DELAY_1_SECOND = 4,
  DISARM_DELAY_20_SECONDS = (20 * DISARM_DELAY_1_SECOND),
  DISARM_DELAY_180_SECONDS = (180 * DISARM_DELAY_1_SECOND)
};

/*----data descriptions-------------------------------------------------------*/

/*----data declarations-------------------------------------------------------*/

/*----macros------------------------------------------------------------------*/
#define setupInputs() // not used for scanned inputs

/*----function prototypes-----------------------------------------------------*/
extern void initInputs(bool hardReset);
extern void startInputDebounce(short multiplier);
extern void testBattery(void);
#ifdef USE_EXTENDED_ZONE_STATE
extern ExtendedZoneMessageByte getInputState(void);
#else
extern ZoneMessageByte getInputState(void);
#endif
#ifdef DMP_1100_PRODUCTION_TEST
extern void clearContactState(void);
#endif
extern void EnableDisarmDelay(bool enable);


#endif                                  /* end of file */
