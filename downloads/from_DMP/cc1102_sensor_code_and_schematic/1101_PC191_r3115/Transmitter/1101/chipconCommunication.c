/*H*****************************************************************************
FILENAME: Communication.c

DESCRIPTION:

NOTES:

Copyright (c) Digital Monitoring Products Inc. 2009.  All rights reserved.

*****************************************************************************H*/
/*----compilation control-----------------------------------------------------*/
#if defined(USING_CHIPCON_FIFO) && !defined(USING_CHIPCON_RF_CHIP)
#error "Cannot enable chipcon FIFO option without enabling chipcon as well"
#endif

/*******************************************************************************
INCLUDE FILES
*******************************************************************************/
/*----system and platform files (external libraries) -------------------------*/

#if defined(__linux__)
#include <stdio.h>
#include <signal.h>
#include <sys/time.h>
#include <string.h>
#endif

/*----program files-----------------------------------------------------------*/
#include "types.h"
#include "chipconCommunication.h"
#include "ioportpins.h"
#include "Hop.h"
#include "Packet.h"
#include "ProtocolTiming.h"
#include "Hardware.h"
#include "Debug.h"
#include "Sleep.h"
#include "Main.h"

/*******************************************************************************
EXTERNAL REFERENCES   Only used if not available in a header file.
*******************************************************************************/
/*----data declarations-------------------------------------------------------*/

/*----function prototype------------------------------------------------------*/
CommunicationStatusFlags communicationStatus;

/*******************************************************************************
PRIVATE DECLARATIONS
*******************************************************************************/
/*----defines, typedefs, structs, unions, enums-------------------------------*/
typedef enum
{
  AWAITING_NONE = 0,
  AWAITING_RISING_EDGE,
  AWAITING_FALLING_EDGE
} EdgeDetectionEnum;

/*----data declarations-------------------------------------------------------*/
static byte receiveBitPointer;
static byte receiveBytePointer;
static byte transmitBytePointer;
static byte transmitBitPointer;
static byte bitCounter;
static byte transmitDelayCounter;
word RFFrameBitCounter;
static unsigned long patternRegister = 0;

static byte dbgNumRxBytes = 0;

#if defined(USING_CHIPCON_FIFO)
  static bool awaitingFirstFifoByte;

  static void RestartReceivePacketSearch(void);
#endif

#if defined(__linux__)
  unsigned char debug_transmitterPacketBuffer[PT_SIZE*2];
  unsigned int debug_transmitBytePointer;
#endif

/*----function prototype------------------------------------------------------*/

void initStateForTransmit(void);

#if defined(__linux__)
void enableIntervalTimer(bool start);
void registerTimerHandler(void);
void WirelessTimerHandler(int signum);
#endif

/*----macros------------------------------------------------------------------*/

/*******************************************************************************
PRIVATE FUNCTION DEFINITIONS
*******************************************************************************/
/*******************************************************************************

DESCRIPTION: ISR for Timer A1.  TACCR1 is setup as a timing for enabling the contact
              TACCR2 is setup for RF bit timming

NOTES: Timer A runs off ACLK

*******************************************************************************/
#pragma vector = TIMERA1_VECTOR
#pragma type_attribute = __interrupt
void WirelessTimingInterrupt(void)
{
#if !defined(__linux__)
  volatile unsigned short vectorVal = TAIV;
  switch (vectorVal)
  {

    case 0x02: //TACCR1 timer interrupt
    {
#if defined(USING_CHIPCON_FIFO)
      setIODirOut(DEBUG_PIN);
      clearIOSel(DEBUG_PIN);
      DEBUG_PIN_OUT ^= DEBUG_PIN_MASK; // Toggle pin
      CCR1 += RFBitTime; // next capture time = bit timing
      //CCR1 += 1; // used with slow sleep clock timing  NOTE: +1 doesn't work; +2 is 256Hz edges

      ++RFFrameBitCounter;
      decrementFrameTimeout();
#endif

#ifdef USE_PANIC_PULSING
      if( ! getCurrentlyCommunicating() ) //If not currently communicating which means
                                          //this timer is sourced by ACLK(32.768kHz)
      {
        toggleIO(RED_LED);
        if( !getIO(RED_LED) ) //if LED is currently off set timer for off delay
        {
          if( --LEDFlashCounter == 0 )
          {
            clearIO(RED_LED);  //turn off LED
            TACCTL1 &= ~CCIE;  //disable interrupt
          }
          TACCR1 = TAR + LED_PULSE_OFF_TIME;
        }
        else    //else set timer for on delay
          TACCR1 = TAR + LED_PULSE_ON_TIME;
      }
      else  // if timer sourced my MCLK just delay
        TACCR1 = TAR + LED_PULSE_OFF_TIME;
#endif
      break;
    }

    case 0x04:// TACCR2 timer interrupt
    {
      //setIODirOut(DEBUG_PIN);
      // set IO pin to IO mode
      //clearIOSel(DEBUG_PIN);
      //DEBUG_PIN_OUT ^= DEBUG_PIN_MASK; // Toggle pin

      #ifdef FCC_SPECIAL_VERSION
        fccInterrupt();
        return;
      #endif
      setDebug(DM_LED5);
      CCR2 += RFBitTime; // next capture time = bit timing

      if (getSleeping())
      {
        if (wakeup(false, false))
        {
          __low_power_mode_off_on_exit();
        }
        else
        {
          // we were not allowed to wakeup, so wakeup in one minute
          sleepTime = TAR;  // record where we are when we sleep
          CCR2 = sleepTime + WAKEUP_RATE;
        }
        return;
      }
      break;
    }

    default:  //default catch all
    {
      break;
    }
  }
#endif
}


/*******************************************************************************

DESCRIPTION: Timer handler function

NOTES:       Called by a signal (interrupt)

*******************************************************************************/
#if defined(__linux__)
void WirelessTimerHandler(int signum)
{
  //printf("WirelessTimerHandler:\n");
  // increment bit counter and decrement frame count
  ++RFFrameBitCounter;
  decrementFrameTimeout();
  // timer auto restarts
}
#endif


/*******************************************************************************

DESCRIPTION: Register the timer handler with alarm signal

NOTES:       Must be called BEFORE enabling timer

*******************************************************************************/
#if defined(__linux__)
void registerTimerHandler(void)
{
  printf("registerTimerHandler:\n");
  struct sigaction sigact;
  memset(&sigact, 0, sizeof sigact);
  sigact.sa_handler = &WirelessTimerHandler;
  sigaction(SIGALRM, &sigact, NULL);
}
#endif


/*******************************************************************************

DESCRIPTION: Uses Linux timers to set the frameTimeout and Bit Counter

NOTES:       Must have the sig handler already registered

*******************************************************************************/
#if defined(__linux__)
void enableIntervalTimer(bool start)
{
  //printf("enableIntervalTimer:\n");

  struct itimerval timer;
  memset(&timer, 0, sizeof timer);
  if (!start)
  {
    setitimer(ITIMER_REAL, &timer, NULL);
  }
  else
  {
#define INTERVAL_TIMER_DELAY_USEC 104 //* 10 // test with *10 // 1 / 9595.87bps = 104.2us
	timer.it_value.tv_sec =         INTERVAL_TIMER_DELAY_USEC / 1000000;
    timer.it_value.tv_usec =        INTERVAL_TIMER_DELAY_USEC % 1000000;
    timer.it_interval.tv_sec  =     INTERVAL_TIMER_DELAY_USEC / 1000000;
    timer.it_interval.tv_usec =     INTERVAL_TIMER_DELAY_USEC % 1000000;
    setitimer(ITIMER_REAL, &timer, NULL);
  }
}
#endif

#if defined(__linux__)

#endif
#if defined(__linux__)

#endif

/*******************************************************************************
PUBLIC FUNCTION DEFINITIONS
*******************************************************************************/

/*******************************************************************************

DESCRIPTION:  Will init the RF chip to a known startup state

NOTES:

*******************************************************************************/
void initRFChip(void)
{
  initChipcon();
#if defined(__linux__)
  registerTimerHandler();
#endif
  setupWirelessSleep();
}


/*******************************************************************************

DESCRIPTION:  Will setup the wireless chip for receiving data

NOTES:

*******************************************************************************/
void setupWirelessReceive(void)
{
  initStateForReceive();  // init the communications state for RX

#if defined(USING_CHIPCON_FIFO)
  // setup registers with sync word for rx
  //writeChipconConfigRegister(MDMCFG2, 0x01); // mod 2-FSK, enable sync mode 1=15/16bits 2=16/16bits
  //#writeChipconConfigRegister(MDMCFG2, 0x02); // mod 2-FSK, enable sync mode 1=15/16bits 2=16/16bits
  //#writeChipconConfigRegister(MDMCFG1, 0x00); // preamble min 2 bytes, disable FEC, chan spacing exponent = 0
  //#writeChipconConfigRegister(PKTCTRL0, 0x02); // fifo mode, pkt length infinite
  //#writeChipconConfigRegister(PKTCTRL1, 0x80); // no address chk, preamble quality [7:5] (value of 4 gates sync word check)
  //#writeChipconConfigRegister(SYNC1, RPAT_HIGH);
  //#writeChipconConfigRegister(SYNC0, RPAT_MIDDLE);
  //writeChipconConfigRegister(FIFOTHR, 0); // Debugging: trigger FIFO threshold interrupt at rx = 4 bytes (lowest setting)
  //#writeChipconConfigRegister(IOCFG2, 0x06); // Sync word rx or tx interrupt
  //writeChipconConfigRegister(IOCFG2, 0x08); // Preamble quality detector
  //writeChipconConfigRegister(IOCFG2, 0x01); // Debugging: Rx fifo status: at or above threshold / Empty setting
  //writeChipconConfigRegister(IOCFG1, 0x2E); // default 3-state. Can be moved to default when FIFO_RX and FIFO_TX are combined into one flag

  //***************************************
  // experiment to see if we can get a ser clock out for bit counting whilst in FIFO mode
  //writeChipconConfigRegister(IOCFG0, 0x4C);
  // doesn't work
  //***************************************

  //#writeChipconConfigRegister(IOCFG0, 0x06); // Sync word rx or tx interrupt
  // Use SPI SO pin normally not sync ser clk
  awaitingFirstFifoByte = true;
#else
  // restore original non-fifo settings.
  // Needed if FIFO_RX and FIFO_TX are separated; can be removed when using a combined flag
  writeChipconConfigRegister(MDMCFG2, 0x00); // mod 2-FSK, disable sync mode
  writeChipconConfigRegister(MDMCFG1, 0x20); // preamble min 4 bytes, disable FEC, chan spacing exponent = 0
  writeChipconConfigRegister(PKTCTRL0, 0x12); // sync ser mode, pkt length infinite
  writeChipconConfigRegister(PKTCTRL1, 0x00); // no address chk, no preamble
  writeChipconConfigRegister(SYNC1, 0);
  writeChipconConfigRegister(SYNC0, 0);
  writeChipconConfigRegister(IOCFG2, 0x2F); // Set GD0-2 to 0
  //writeChipconConfigRegister(IOCFG1, 0x0B);
  writeChipconConfigRegister(IOCFG0, 0x4C);
#endif

  //******************************
  // Experiment: see if carrier sense or clear channel is ever detected
  // Debug output on GDO-2
  //writeChipconConfigRegister(IOCFG2, 0x09); // Clear channel assessment
  //writeChipconConfigRegister(IOCFG2, 0x0E); // Carrier Sense detect
  //******************************

#if defined(USING_CHIPCON_FIFO)
  // experimenting with sync in
  setIODirIn(RF_DATA_IN); // set the data pin direction
  // NOTE: chip must be in IDLE mode for this to be effective. Sleep mode automatically flushes fifo
  sendChipconCommandStrobe(SFRX);  // flush RX fifo buffer
#else
  clearIOEnableInt(RF_DATA_CLK); // disable the DCLK interrupt before configureing RF Chip
  setIODirIn(RF_DATA_IN); // set the data pin direction
#endif

  sendChipconCommandStrobe(SRX);  // place the RF Chip into RX mode

#if defined(USING_CHIPCON_FIFO)
#if !defined(__linux__)
  CCR2 = TAR + RFBitTime;
  CCTL2 = OUTMOD_5+CM0+CCIE; // CC2 = Compare, Reset

  CCR1 = TAR + RFBitTime;
  CCTL1 = OUTMOD_5+CM0+CCIE; // CC2 = Compare, Reset
#else
  enableIntervalTimer(true);
#endif
#else

#if !defined(__linux__)
  CCR2 = TAR + RFBitTime;
  CCTL2 = OUTMOD_5+CM0+CCIE; // CC2 = Compare, Reset
#endif

  // Prepair for interrupts on the data clk
  setIORisingInt(RF_DATA_CLK);
  clearIOIntFlag(RF_DATA_CLK);
  setIOEnableInt(RF_DATA_CLK);
#endif

}

#if defined(USING_CHIPCON_FIFO)
void RestartReceivePacketSearch(void)
{
    awaitingFirstFifoByte = true;
    sendChipconCommandStrobe(SIDLE);
    // NOTE: chip must be in IDLE mode for this to be effective. Sleep mode automatically flushes fifo
    sendChipconCommandStrobe(SFRX);  // flush RX fifo buffer
    sendChipconCommandStrobe(SRX);  // restarts the rx packet search
}
#endif

#if defined(USING_CHIPCON_FIFO)
void ReceiveRFDataByte(void)
{
  // get value from fifo - assumes byte availability has been proven!
  unsigned char rxByte;
  char chipStatus = readSingleRxFifoByte(&rxByte);
  static bool lastRxBit;

  //if we are currently receiving data from a valid receiver
  if (communicationStatus.patternDetected || communicationStatus.frameStartDetected)
  {
    unsigned char fifoRxBitPointer = 0x80;
    for ( int i = 0; i < 8; i++)
    {
      bool rxDataBit = !((rxByte & fifoRxBitPointer) > 0);
      fifoRxBitPointer >>= 1;
      // we've received 8 bits of same value in a row, so remove transition
      if (bitCounter >= MAX_BITS_IN_A_ROW)
      {
        if (rxDataBit == lastRxBit) // error, should be a transition
          communicationStatus.frameStartDetected = false;  // no longer in sync
      }
      else
      {
        //record data bit
        if (rxDataBit)
          receiverPacketBuffer[receiveBytePointer] += receiveBitPointer;

        receiveBitPointer >>= 1;
        //If we have received a whole byte
        if (receiveBitPointer == 0)
        {
          receiveBitPointer = 0x80;
          // if just received first byte of a receivers transmission
          if (receiveBytePointer == 0)
          {
            //check the house code to verify its from our receiver
            if ((houseCode == INVALID_HOUSE_CODE) || (houseCode == ((ReceiverAck*)receiverPacketBuffer)->houseCode))
            {
              communicationStatus.frameStartDetected = true;
              RFFrameBitCounter = SlotSYNC;
            }
            else
            {
              // need to flag error and reset receive
              RestartReceivePacketSearch();
            }
            communicationStatus.patternDetected = false;
          }
          // If we havn't overflowed setup for next byte
          if (receiveBytePointer < PR_SIZE - 1)
          {
            receiveBytePointer++;
            receiverPacketBuffer[receiveBytePointer] = 0;
          }
        }
      }
      //Are we sending a bit transition?
      if (rxDataBit != lastRxBit)
      {
        //Reset transition counter
        lastRxBit = rxDataBit;
        bitCounter = 1;
      }
      else
      {
        //no transition so count
        bitCounter++;
      }
    } // end of for loop
  }
  else
  {
    // look for third byte (RPAT_LOW) in the pattern
    // Must be the first one in the fifo after the sync word detection
    //if(syncDetectedOrSent)
    if (awaitingFirstFifoByte)
    {
      // first byte from fifo
      awaitingFirstFifoByte = false;
      if((rxByte & 0xFF) == ((~RPAT_LOW) & 0xFF))
      {
        //we recieved the pattern from the receiver so setup receive
        initStateForReceive();
        communicationStatus.patternDetected = true;
        bitCounter = 0;
        lastRxBit = (rxByte & 0x01) > 0;
      }
    }
    else
    {
      // need to flag error and reset receive
      RestartReceivePacketSearch();
    }
  }
}
#endif

#if defined(USING_CHIPCON_FIFO)
void TransmitRFDataBytes(unsigned char maxBytes);
void TransmitRFDataBytes(unsigned char maxBytes)
{
  char tempHeaderBuffer[PT_HEADER_SIZE];
  // Initially just send the bytes without bit counting
  if (transmitBytePointer < PT_HEADER_SIZE)
  {
    unsigned char size = ((PT_HEADER_SIZE - transmitBytePointer) > maxBytes) ? (maxBytes) : (PT_HEADER_SIZE - transmitBytePointer);
    for (int i = 0; i < size; ++i)
    {
      tempHeaderBuffer[i] = ~(transmitterHeaderBuffer[transmitBytePointer + i]);
    }
    writeTxFifoBytes((unsigned char *)&tempHeaderBuffer[0], size);
    transmitBytePointer += size;
  }
  else if (transmitBytePointer < (transmitterPacketSize + PT_HEADER_SIZE))
  {
    unsigned char* ptr = &transmitterPacketBuffer[transmitBytePointer - PT_HEADER_SIZE];
    // calculate remaining bytes to send and compare with available space
    unsigned char size = (((PT_HEADER_SIZE + transmitterPacketSize) - transmitBytePointer) > maxBytes) ? (maxBytes) : ((PT_HEADER_SIZE + transmitterPacketSize) - transmitBytePointer);
    // Until bit counting then invert all bytes
    for (int i = 0; i < size; ++i)
    {
      unsigned char *invPtr = ptr + i;
      *(invPtr) = ~(*(invPtr));
    }
    writeTxFifoBytes(ptr, size);
    transmitBytePointer += size;
    // if the end of the packet is going to be sent to fifo, check battery
    if ((transmitBytePointer) >= (transmitterPacketSize + PT_HEADER_SIZE - 1))
      communicationStatus.timeToTestBattery = true;
  }
  else
  {
    communicationStatus.transmitDelay = true;
    transmitDelayCounter = 0;
    communicationStatus.timeToTransmit = false;
  }
}

void TransmitRFDataByte(void)
{
  static bool txDataBit = false;
  static bool lastTxBit;
  char data = 0;
  char fifoData = 0;

  unsigned char fifoTxBitPointer = 0x80;
  for ( int i = 0; i < 8; i++ )
  {
    if (!txDataBit) //set/reset IO pin from the last interrupt
      fifoData = fifoData | fifoTxBitPointer;
    else
      fifoData = fifoData & ~(fifoTxBitPointer);

    fifoTxBitPointer >>= 1;

    // we've sent 8 bits of same value in a row, so insert transition
    if (bitCounter >= MAX_BITS_IN_A_ROW)
    {
      txDataBit = !lastTxBit;
      lastTxBit = txDataBit;
      bitCounter = 1;
    }
    else
    {
      // either use header or buffer
      if (transmitBytePointer < PT_HEADER_SIZE)
      {
        data = transmitterHeaderBuffer[transmitBytePointer];
      }
      else if (transmitBytePointer < transmitterPacketSize + PT_HEADER_SIZE)
      {
        data = transmitterPacketBuffer[transmitBytePointer - PT_HEADER_SIZE];
        // when almost done transmitting, check the battery
        if ((transmitBytePointer == transmitterPacketSize + PT_HEADER_SIZE - 1) && (transmitBitPointer == 0x02))
          communicationStatus.timeToTestBattery = true;
      }
      else
      {
        data = 0; // any remaining bits to complete a full byte will be zero
        communicationStatus.transmitDelay = true;
        transmitDelayCounter = 0;
        communicationStatus.timeToTransmit = false;
      }

      // set or clear output
      txDataBit = (data & transmitBitPointer) > 0;

      if (txDataBit != lastTxBit)
      {
        lastTxBit = txDataBit;
        bitCounter = 1;
      }
      else
      {
        bitCounter++;
      }

      transmitBitPointer >>= 1;
      if (transmitBitPointer == 0)
      {
        transmitBitPointer = 0x80;
        ++transmitBytePointer;
        if (transmitBytePointer == PT_HEADER_SIZE)
          bitCounter = 0;  // start bit counter when header complete
      }
    }
  } // end of for loop

  // write value to fifo - assumes byte availability has been proven!
  // NOTE: This setup of the radio needs to have the tx data inverted
  unsigned char chipStatus = writeSingleTxFifoByte(fifoData);
#if defined(__linux__)
  debug_transmitterPacketBuffer[debug_transmitBytePointer] = ~fifoData;
  ++debug_transmitBytePointer;
#endif
  clearDebug(DM_LED5);
}
#endif

/*******************************************************************************

DESCRIPTION:  Function called by an ISR and will receive a bit and either search for
                pattern or record the bit in the receiver buffer
NOTES:

*******************************************************************************/
void ReceiveRFData(void)
{
  bool rxDataBit = getIO(RF_DATA_IN) > 0;  // read state of data ASAP
  static bool lastRxBit;

  //if we are currently receiving data from a valid receiver
  if (communicationStatus.patternDetected || communicationStatus.frameStartDetected)
  {
    // we've received 8 bits of same value in a row, so remove transition
    if (bitCounter >= MAX_BITS_IN_A_ROW)
    {
      if (rxDataBit == lastRxBit) // error, should be a transition
        communicationStatus.frameStartDetected = false;  // no longer in sync
    }
    else
    {
      //record data bit
      if (rxDataBit)
        receiverPacketBuffer[receiveBytePointer] += receiveBitPointer;

      receiveBitPointer >>= 1;
      //If we have received a whole byte
      if (receiveBitPointer == 0)
      {
        receiveBitPointer = 0x80;
        // if just received first byte of a receivers transmission
        if (receiveBytePointer == 0)
        {
          //check the house code to verify its from our receiver
          if ((houseCode == INVALID_HOUSE_CODE) || (houseCode == ((ReceiverAck*)receiverPacketBuffer)->houseCode))
          {
            communicationStatus.frameStartDetected = true;
            RFFrameBitCounter = SlotSYNC;
          }
          communicationStatus.patternDetected = false;
        }
        // If we havn't overflowed setup for next byte
        if (receiveBytePointer < PR_SIZE - 1)
        {
          receiveBytePointer++;
          receiverPacketBuffer[receiveBytePointer] = 0;
          dbgNumRxBytes++;
        }
      }
    }
    //Are we sending a bit transition?
    if (rxDataBit != lastRxBit)
    {
      //Reset transition counter
      lastRxBit = rxDataBit;
      bitCounter = 1;
    }
    else
    {
      //no transition so count
      bitCounter++;
    }
  }
  else
  {
    //shift in newest data bit
    patternRegister <<=1;
    if (rxDataBit)
      patternRegister |= 0x01;
    else
      patternRegister &= ~0x01;
    //did we recieve the last bit of the last pattern byte?
    if((patternRegister & 0x00FFFFFF) == RECEIVER_PATTERN)
    {
      //we recieved the pattern from the receiver so setup receive
      initStateForReceive();
      communicationStatus.patternDetected = true;
      bitCounter = 0;
      dbgNumRxBytes = 0;
    }
  }
}

/*******************************************************************************

DESCRIPTION:  Fucntion called from an ISR will transmit a single bit each time called

NOTES:

*******************************************************************************/
void TransmitRFData(void)
{
  static bool txDataBit = false;
  static bool lastTxBit;
  char data = 0;

  if (!txDataBit) //set/reset IO pin from the last interrupt
    setIO(RF_DATA_OUT);
  else
    clearIO(RF_DATA_OUT);

  // we've sent 8 bits of same value in a row, so insert transition
  if (bitCounter >= MAX_BITS_IN_A_ROW)
  {
    txDataBit = !lastTxBit;
    lastTxBit = txDataBit;
    bitCounter = 1;
  }
  else
  {
    // either use header or buffer
    if (transmitBytePointer < PT_HEADER_SIZE)
    {
      data = transmitterHeaderBuffer[transmitBytePointer];
    }
    else if (transmitBytePointer < transmitterPacketSize + PT_HEADER_SIZE)
    {
      data = transmitterPacketBuffer[transmitBytePointer - PT_HEADER_SIZE];
      // when almost done transmitting, check the battery
      if ((transmitBytePointer == transmitterPacketSize + PT_HEADER_SIZE - 1) && (transmitBitPointer == 0x02))
        communicationStatus.timeToTestBattery = true;
    }
    else
    {
      communicationStatus.transmitDelay = true;
      transmitDelayCounter = 0;
      communicationStatus.timeToTransmit = false;
    }

    // set or clear output
    txDataBit = (data & transmitBitPointer) > 0;

    if (txDataBit != lastTxBit)
    {
      lastTxBit = txDataBit;
      bitCounter = 1;
    }
    else
    {
      bitCounter++;
    }

    transmitBitPointer >>= 1;
    if (transmitBitPointer == 0)
    {
      transmitBitPointer = 0x80;
      ++transmitBytePointer;
      if (transmitBytePointer == PT_HEADER_SIZE)
        bitCounter = 0;  // start bit counter when header complete
    }
  }
  clearDebug(DM_LED5);
}

#if defined(USING_CHIPCON_FIFO)
int processFifoCondition(void)
{
  static EdgeDetectionEnum syncEdgeDetection = AWAITING_NONE;
  char statusFifoByte;
  char fifoError;
  char nFifoBytes = 0;
  int rptFunctionMoreWorkAvailable = 0;

  // get status byte - make sure this function is not called too often
  //char status = readChipconStatus();
  //char pktStatus = readChipconStatusRegister(PKTSTATUS); // debug purposes
  if( !communicationStatus.timeToTransmit && !communicationStatus.transmitDelay )
  {
    // debug check sync line
#if defined(__linux__)
    bool sync = (getIO(GDO0)) > 0;
#else
    bool sync = (getIO(RF_DATA_IN)) > 0;
#endif
    if (sync)
    {
      statusFifoByte = readChipconStatusRegister(RXBYTES);
      fifoError = (statusFifoByte & 0x80);
      nFifoBytes = (statusFifoByte & 0x7F);
      if (nFifoBytes)
      {
        ReceiveRFDataByte();
        volatile char debugChar = nFifoBytes;
        //RFFrameBitCounter += 8;
      }
      // Get rx bytes as fast as possible
      if (nFifoBytes > 1)
      {
        rptFunctionMoreWorkAvailable = 1;
      }
    }
  }
  //if we are in a transmit state
  else if ( !communicationStatus.transmitDelay )
  {
    statusFifoByte = readChipconStatusRegister(TXBYTES);
    fifoError = (statusFifoByte & 0x80);
    nFifoBytes = (statusFifoByte & 0x7F);
    if (nFifoBytes < 0x3f)  // not full
    {
      // available space
      //TransmitRFDataBytes(0x40 - nFifoBytes);
      TransmitRFDataByte();
      //RFFrameBitCounter += 8;
    }
    if (nFifoBytes < (0x3f - 1)) // more than one space available
    {
      rptFunctionMoreWorkAvailable = 1;
    }
    // Set it up to detect the rising then falling edge of sync output
    // On Tx: Rising edge signifies hardware header has been sent
    // On Tx: Falling edge signifies FIFO completely empty
    syncEdgeDetection = AWAITING_RISING_EDGE;
  }
  //if we are currently ending a transmission
  else if (communicationStatus.transmitDelay)
  {
    bool sync = (getIO(GDO0)) > 0;
    if (sync && (AWAITING_RISING_EDGE == syncEdgeDetection))
    {
      syncEdgeDetection = AWAITING_FALLING_EDGE;
#if defined(__linux__)
      printf("OrigTx:");
      for(int i = 0; i < PT_HEADER_SIZE; ++i)
      {
          printf("%02X,", transmitterHeaderBuffer[i]);
      }
      for(int i = 0; i < (transmitterPacketSize); ++i)
      {
          printf("%02X,", transmitterPacketBuffer[i]);
      }
      printf("\n");

      printf("/DbgTx:");
      for(int i = 0; i < debug_transmitBytePointer; ++i)
      {
        printf("%02X,", debug_transmitterPacketBuffer[i]);
      }
      printf("\n");
#endif
    }
    else if (!sync && (AWAITING_FALLING_EDGE == syncEdgeDetection))
    {
      // Wait for sync line to go low signifying tx complete
      communicationStatus.transmitComplete = true;
      communicationStatus.transmitDelay = false;
    }
  }

  //if (++RFFrameBitCounter >= SlotEND)
  ENTER_CRITICAL_SECTION();
  if (RFFrameBitCounter >= SlotEND)
    RFFrameBitCounter = 0;
  EXIT_CRITICAL_SECTION();

  //decrementFrameTimeout();

  return rptFunctionMoreWorkAvailable;
}
#endif

/*******************************************************************************

DESCRIPTION: Called on transititions of the Chipcon GDO-1 (clock) pin

NOTES:

*******************************************************************************/
void rfDataClockEdge(void)
{
  //if we are in receive state
  if( !communicationStatus.timeToTransmit && !communicationStatus.transmitDelay )
  {
    ReceiveRFData();
  }
  //if we are in a transmit state
  else if ( !communicationStatus.transmitDelay )
    TransmitRFData();
  //if we are currently ending a transmission
  else if (communicationStatus.transmitDelay)
  {
    //insert 16 bits of delay at the end of our transmission
    if (transmitDelayCounter < CHIPCON_TRANSMIT_DELAY_BITS)
    {
      transmitDelayCounter++;
    }
    else
    {
      communicationStatus.transmitComplete = true;
      communicationStatus.transmitDelay = false;
    }
  }

  if (++RFFrameBitCounter >= SlotEND)
    RFFrameBitCounter = 0;

  decrementFrameTimeout();
}

/*******************************************************************************

DESCRIPTION: Will return the current offset into the receive buffer

NOTES:

*******************************************************************************/
byte getWirelessReceiveBufferOffset(void)
{
  return receiveBytePointer;
}

/*******************************************************************************

DESCRIPTION:  Will setup the RF chip for transmission

NOTES:

*******************************************************************************/
void setupWirelessTransmit(void)
{
  clearIOEnableInt(RF_DATA_CLK);
#if defined(USING_CHIPCON_FIFO)
#if !defined(__linux__)
  CCTL1 = 0;   // no interrupt timer A CC1
#else
  memset(&debug_transmitterPacketBuffer[0], 0, sizeof debug_transmitterPacketBuffer);
  debug_transmitBytePointer = 0;
#endif
#endif
  sendChipconCommandStrobe(SIDLE); // put the RF Chip into Idle mode
  initStateForTransmit();   //init the communications state for transmission
  latchFrequencyError();

#if defined(USING_CHIPCON_FIFO)
  // setup registers with sync word for tx
  //writeChipconConfigRegister(MDMCFG2, 0x01); // mod 2-FSK, enable sync mode 1=15/16bits 2=16/16bits
  //#writeChipconConfigRegister(MDMCFG2, 0x02); // mod 2-FSK, enable sync mode 1=15/16bits 2=16/16bits
  //#writeChipconConfigRegister(MDMCFG1, 0x20); // preamble min 4 bytes, disable FEC, chan spacing exponent = 0
  //#writeChipconConfigRegister(PKTCTRL0, 0x02); // fifo mode, pkt length infinite
  //#writeChipconConfigRegister(PKTCTRL1, 0x00); // no address chk, no preamble
  //writeChipconConfigRegister(SYNC1, TPAT_HIGH);
  //writeChipconConfigRegister(SYNC0, TPAT_MIDDLE);
  //#writeChipconConfigRegister(SYNC1, ~TPAT_HIGH); // inverted sync byte
  //#writeChipconConfigRegister(SYNC0, ~TPAT_MIDDLE); // inverted sync byte
  //***********************
  // Experiment: try preamble of 2 and sync bytes of 0xAA then send the entire packet manually including preamble
  // sync of 0 didn't work so trying extra preable bytes 0xAA
  //#writeChipconConfigRegister(MDMCFG1, 0); // preamble min 2 bytes, disable FEC, chan spacing exponent = 0;
  //#writeChipconConfigRegister(SYNC1, 0x55); // 0 didn't work; 0x55 is inverted 0xAA
  //#writeChipconConfigRegister(SYNC0, 0x55);
  // NOTE: change txBufferPointer start position back to 0
  // try and change txBufferPointer start position back to start of sync word 4
  //***********************
  //#writeChipconConfigRegister(IOCFG2, 0x06); // Sync word rx or tx interrupt
  //writeChipconConfigRegister(IOCFG2, 0x03); // Debugging: Tx fifo status: full / below threshold
  //writeChipconConfigRegister(IOCFG1, 0x2E); // default 3-state. Can be moved to default when FIFO_RX and FIFO_TX are combined into one flag
  //writeChipconConfigRegister(IOCFG1, 0x2F); // 0x2F = 0
  //#writeChipconConfigRegister(IOCFG0, 0x06); // Sync word rx or tx interrupt

  // NOTE: chip must be in IDLE mode for this to be effective. Sleep mode automatically flushes fifo
  sendChipconCommandStrobe(SFTX);  // flush TX fifo buffer
#else
  // restore original non-fifo settings
  // Needed if FIFO_RX and FIFO_TX are separated; can be removed when using a combined flag
  writeChipconConfigRegister(MDMCFG2, 0x00); // mod 2-FSK, disable sync mode
  writeChipconConfigRegister(MDMCFG1, 0x20); // preamble min 4 bytes, disable FEC, chan spacing exponent = 0
  writeChipconConfigRegister(PKTCTRL0, 0x12); // sync ser mode, pkt length infinite
  writeChipconConfigRegister(PKTCTRL1, 0x00); // no address chk, no preamble
  writeChipconConfigRegister(SYNC1, 0);
  writeChipconConfigRegister(SYNC0, 0);
  writeChipconConfigRegister(IOCFG2, 0x2F); // Set GD0-2 to 0
  //writeChipconConfigRegister(IOCFG1, 0x0B);
  writeChipconConfigRegister(IOCFG0, 0x4C);
#endif

  //******************************
  // Experiment: see if carrier sense or clear channel is ever detected
  // Debug output on GDO-2
  //writeChipconConfigRegister(IOCFG2, 0x09); // Clear channel assessment
  //writeChipconConfigRegister(IOCFG2, 0x0E); // Carrier Sense detect
  //******************************

  sendChipconCommandStrobe(STX);  // change the RF chips mode to TX

#if defined(USING_CHIPCON_FIFO)
#if !defined(__linux__)
  CCR1 = TAR + RFBitTime;
  CCTL1 = OUTMOD_5+CM0+CCIE; // CC2 = Compare, Reset
#else
  enableIntervalTimer(true);
#endif
#else
  setIODirOut(RF_DATA_OUT); //set the data pin direction

  // Prepare for interrupts on the data clk line
  setIORisingInt(RF_DATA_CLK);
  clearIOIntFlag(RF_DATA_CLK);
  setIOEnableInt(RF_DATA_CLK);
#endif

}

/*******************************************************************************

DESCRIPTION: Will blank the current communications status

NOTES:

*******************************************************************************/
void initializeCommunicationStatus(void)
{
  communicationStatus.timeToTransmit = false;
  communicationStatus.patternDetected = false;
  communicationStatus.transmitComplete = false;
  communicationStatus.timeToTestFrequency = false;
  communicationStatus.frameStartDetected = false;
  communicationStatus.timeToTestBattery = false;
  communicationStatus.spare = 0;  // saves a little codespace
}


/*******************************************************************************

DESCRIPTION: Will init the communication state for RX

NOTES:

*******************************************************************************/
void initStateForReceive(void)
{
  receiveBitPointer = 0x80;
  receiveBytePointer = 0;
  receiverPacketBuffer[0] = 0;
  communicationStatus.patternDetected = false;
  communicationStatus.frameStartDetected = false;
}


/*******************************************************************************

DESCRIPTION:  Will init the communications state for TX

NOTES:

*******************************************************************************/
void initStateForTransmit(void)
{
#if defined(USING_CHIPCON_FIFO)
  //transmitBytePointer = 6; // fifo takes care of 4 preamble and 2 sync bytes
  //transmitBytePointer = 0; // manually send preamble and sync words
  transmitBytePointer = 0; // manually send only 2 x preamble and sync words
  //transmitBytePointer = 4; // only manually send sync word onwards. Preamble done by chip through PA & SYNC
#else
  transmitBytePointer = 0;
#endif
  transmitBitPointer = 0x40;
  communicationStatus.timeToTransmit = true;
}


/*******************************************************************************

DESCRIPTION:  Will put the RF chip into sleep mode

NOTES:

*******************************************************************************/
void setupWirelessSleep(void)
{
  clearIOEnableInt(RF_DATA_CLK);
#if defined(USING_CHIPCON_FIFO)
#if !defined(__linux__)
  CCTL1 = 0;   // no interrupt timer A CC1
#endif
#endif
  sendChipconCommandStrobe(SIDLE); // transition to idle to prepaire to sleep
#ifdef USING_ACK_LED
  LED_OFF(RED_LED);   // LED OFF
#endif
  sendChipconCommandStrobe(SPWD); // send the command to place the chip to sleep
}


/*******************************************************************************

DESCRIPTION: Changes the mode of the RF chip to Idle

NOTES:

*******************************************************************************/
void setupWirelessIdle(void)
{
  clearIOEnableInt(RF_DATA_CLK); // disable data clk interrupt
#if defined(USING_CHIPCON_FIFO)
#if !defined(__linux__)
  CCTL1 = 0;   // no interrupt timer A CC1
  sendChipconCommandStrobe(SIDLE); // place RF chip into idle mode
#else
  enableIntervalTimer(false);
  char statusByte;
  do
  {
    statusByte = sendChipconCommandStrobe(SIDLE);
    // On CS active Radio MISO will go low to signify xtal stabilized
    // wait if chip has not powered up yet - top bit should return 0
    // with linux have to check SPI return value as MISO is not dual purpose SPI/GPIO
  } while ((statusByte & 0x80) > 0);
#endif
#endif
}

