/*H*****************************************************************************
FILENAME: IndicatorLED.c

DESCRIPTION: Controls the red/green indicator LED based on keypad status.

NOTES:

Copyright (c) Digital Monitoring Products Inc. 2017.  All rights reserved.

*****************************************************************************H*/

#if defined(__linux__)
#include <stdio.h>
#include "LOG_to_file.h"
#endif

#include "IndicatorLED.h"
#include "ioportpins.h"
#include "Packet.h"
#include "Sleep.h"
#include "main.h"

static bool systemIsArmed;
static bool systemInAlarm;

/*F****************************************************************************
*
* Name: ProcessIndicatorState()
* Description:  Called every 1/2 second at the end of the keypad slot.
* Parameters:   None
* Return value: None
*
*****************************************************************************F*/
void ProcessIndicatorState(void)
{
}

/*F****************************************************************************
*
* Name: ProcessKeypadStatus()
* Description:  Parses the keypad status word to determine which LED pattern
*               should be active.
* Parameters:   - new keypad status word
* Return value: None
*
*****************************************************************************F*/
void ProcessKeypadStatus(KeypadStatusWord status)
{
  bool newArmedState = (status.statusColor == STATUS_COLOR_RED);
  bool newInAlarmState = (status.redBacklight == 1);

  if (systemIsArmed != newArmedState)
  {
    systemIsArmed = newArmedState;
    // send change to system monitor
    if(systemIsArmed)
    {
      // log to system monitor armed file
      char buffer[MAX_FILE_MESSAGE_LENGTH];

      sprintf(buffer,"%s\n",ARMED_MESSAGE);
      printf("%s",buffer);

      LOG_to_file((char*)RX_EVNT_SYSTEM_ARMED_FILE_0,buffer); // just timestamp
    }
    else
    {
      // log to system monitor disarmed file
      char buffer[MAX_FILE_MESSAGE_LENGTH];

      sprintf(buffer,"%s\n",DISARMED_MESSAGE);
      printf("%s",buffer);

      LOG_to_file((char*)RX_EVNT_SYSTEM_DISARMED_FILE_0,buffer); // just timestamp
    }
#if defined(__linux__)
    printf("PKS: system armed change = %u\n", systemIsArmed);
#endif
  }

  if (systemInAlarm != newInAlarmState)
  {
    systemInAlarm = newInAlarmState;
    // send change to system monitor
    if (systemInAlarm)
    {
#if defined(__linux__)
      // log to system monitor alarm_on file
      char buffer[MAX_FILE_MESSAGE_LENGTH];

      sprintf(buffer,"%s\n",ALARM_CONDITION_ON_MESSAGE);
      printf("%s",buffer);

      LOG_to_file((char*)RX_EVNT_ALARM_CONDITION_ON_FILE_0,buffer); // just timestamp
#endif
    }
    else
    {
#if defined(__linux__)
      // log to system monitor alarm_off file
      char buffer[MAX_FILE_MESSAGE_LENGTH];

      sprintf(buffer,"%s\n",ALARM_CONDITION_OFF_MESSAGE);
      printf("%s",buffer);

      LOG_to_file((char*)RX_EVNT_ALARM_CONDITION_OFF_FILE_0,buffer); // just timestamp
#endif
    }
#if defined(__linux__)
    printf("PKS: system alarm status change = %u\n", systemInAlarm);
#endif
  }
}
