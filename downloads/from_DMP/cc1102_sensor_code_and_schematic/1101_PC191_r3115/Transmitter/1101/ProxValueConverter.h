/*
 * ProxValueConverter.h
 *
 *  Created on: Aug 26, 2017
 *      Author: cargt
 */

#ifndef PROXVALUECONVERTER_H_
#define PROXVALUECONVERTER_H_

#include <inttypes.h>

#define CARD_READ_PAD_BYTE 0xBB
#define NUM_CARD_READ_DIGITS 10

#define USER_CODE_SHIFT_NUM     1 // shift to get user code at bottom of value
#define USER_CODE_MASK          0x1FFFF // 17 bits of user code


void PVC_convertToCardReadMessageFormat(uint64_t proxIn, uint8_t* buf, uint8_t buf_size, uint8_t digitsRequired);


#endif /* PROXVALUECONVERTER_H_ */
