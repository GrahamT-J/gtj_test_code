/*H*****************************************************************************
FILENAME: Communication.h

DESCRIPTION:

NOTES:

Copyright (c) Digital Monitoring Products Inc. 2004.  All rights reserved.

*****************************************************************************H*/
#ifndef  COMMUNICATION_H
#define  COMMUNICATION_H

/*----compilation control-----------------------------------------------------*/

/*******************************************************************************
INCLUDE FILES   Note: Minimize nesting of header files.
*******************************************************************************/
/*----system and platform files-----------------------------------------------*/

/*----program files-----------------------------------------------------------*/

/*******************************************************************************
FILE CONTEXT
*******************************************************************************/
/*----defines, typedefs, structs, unions, enums-------------------------------*/
typedef struct
{
  BITFIELD frameStartDetected   : 1;
  BITFIELD timeToTransmit       : 1;
  BITFIELD patternDetected      : 1;
  BITFIELD transmitComplete     : 1;
  BITFIELD timeToTestFrequency  : 1;
  BITFIELD timeToTestBattery    : 1;
  BITFIELD transmitDelay        : 1;
  BITFIELD spare                : 1;
} CommunicationStatusFlags;

#define CHIPCON_TRANSMIT_DELAY_BITS 16  //transmit the recommened 2 bytes of dummy data at the end of a transmission

/*----data descriptions-------------------------------------------------------*/

/*----data declarations-------------------------------------------------------*/
extern word RFFrameBitCounter;
extern CommunicationStatusFlags communicationStatus;

/*----macros------------------------------------------------------------------*/

/*----function prototypes-----------------------------------------------------*/
extern byte getWirelessReceiveBufferOffset(void);
extern void setupWirelessTransmit(void);
extern void setupWirelessReceive(void);
extern void initializeCommunicationStatus(void);
extern void initStateForReceive(void);
extern void setupWirelessSleep(void);
extern void initRFChip(void);
extern void setupWirelessIdle(void);

#if defined(USING_CHIPCON_FIFO)
extern int processFifoCondition(void);
#endif

#endif                                  /* end of file */
