/*H*****************************************************************************
FILENAME: IndicatorLED.h

DESCRIPTION: Controls the red/green indicator LED based on keypad status.

NOTES:

Copyright (c) Digital Monitoring Products Inc. 2017.  All rights reserved.

*****************************************************************************H*/
#ifndef INDICATORLED_H
#define INDICATORLED_H

#include "Packet.h"

extern void ProcessKeypadStatus(KeypadStatusWord status);
extern void ProcessIndicatorState(void);

#endif
