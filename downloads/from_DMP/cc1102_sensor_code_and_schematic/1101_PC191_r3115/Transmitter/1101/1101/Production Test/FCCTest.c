/* FCCTest.c contains the main loop for the FCC test.
 *
 * Copyright Digital Monitoring Products Inc. 2004 - 2017. All rights reserved.
 */

#include "ioportpins.h"
#include "Hardware.h"
#include "ProtocolTiming.h"
#include "chipcon.h"
#include "Hop.h"

#if !defined(__linux__)
#include "Uart.h"
#else
#include <signal.h>
#include <string.h>
#include <time.h>
//#include <sys/time.h>

#include <stdio.h>

#include "gpio_linux.h"
#endif

extern bool TX;

/* A button press is registered if FCC_BUTTON is held down longer than
 * FCC_BUTTON_THRESHOLD main loop cycles.
 */
#if !defined(__linux__)
#define FCC_BUTTON_THRESHOLD 500u
#else
// Linux processor has a much faster processor
#define FCC_BUTTON_THRESHOLD 5000u
#endif
/* PATTERN_BIT_TIME is the number of main loop cycles each bit in a flash
 * pattern lasts.  It is best if this is a power of two.
 */
#define PATTERN_BIT_TIME 8192u

/* FCC state machine states */
enum State
{
  IDLE = 0,                     /* idle */
  UNMOD_LOW,                    /* unmodulated carrier on channel 0 */
  MOD_LOW,                      /* modulated carrier on channel 0 */
  UNMOD_MID,                    /* unmodulated carrier on channel 26 */
  MOD_MID,                      /* modulated carrier on channel 26 */
  UNMOD_HIGH,                   /* unmodulated carrier on channel 52 */
  MOD_HIGH,                     /* modulated carrier on channel 52 */
  UNMOD_HOP,                    /* unmodulated carrier in hopping sequence */
  MOD_HOP,                      /* modulated carrier in hopping sequence */
  BAD,                          /* illegal state */
};

/* Mode bits for setMode() */
enum
{
  M_HOP = 0x1,
  M_MOD = 0x2,
  M_TX = 0x4,
};

/* flashPatterns contains the survey LED flash patterns for each state.  Each
 * bit represents a slice of time.  If the bit is set, the LED is lit;
 * otherwise, it is unlit.
 */
unsigned flashPatterns[] = {
  [UNMOD_LOW]  = 0x0001u,
  [MOD_LOW]    = 0x0005u,
  [UNMOD_MID]  = 0x0015u,
  [MOD_MID]    = 0x0055u,
  [UNMOD_HIGH] = 0x0155u,
  [MOD_HIGH]   = 0x0555u,
  [UNMOD_HOP]  = 0x1555u,
  [MOD_HOP]    = 0x5555u,
  [IDLE] = 0u,
  [BAD] = 0u
};

#if defined(__linux__)
#define SW_DEBUG_1 95 // gpio num
#define SW_DEBUG_2 96 // gpio num

extern void TimerFCC(int signum);
extern void hoppingOn(void);
extern void hoppingOff(void);

// private functions
static void registerTimerHandler(void);
static void enableIntervalTimer(bool start);
#endif



void dataOff(void);
void dataOn(void);

static void setMode(unsigned char channel, unsigned char mode);

void FCCMain(char startFCCState)
{
  unsigned cycle;           /* main loop cycle counter (for flash patterns) */
  unsigned bit;             /* current bit (for flash patterns) */

  enum State state;         /* current state */
  unsigned accum;           /* FCC state change switch accumulator */
  bool isp;                 /* if non-zero, switch is pressed */
  bool wasp;                /* if non-zero, switch was pressed last cycle */

  bit = 0x8000u;

  state = startFCCState;
  accum = 0;
  isp = false;
  wasp = false;

  setMode(0, 0);

#if defined(__linux__)
  // need to setup gpio for export if using on linux
  gpio_export(SW_DEBUG_1);
  gpio_set_dir(SW_DEBUG_1, true); // true = output
  gpio_export(SW_DEBUG_2);
  gpio_set_dir(SW_DEBUG_2, false); // false = input
  //registerTimerHandler();

  printf("state=%u\n", state);
#endif

  for (cycle = 0;; cycle++)
  {
    if (cycle % PATTERN_BIT_TIME == 0)
      bit >>= 1;
    if (bit == 0)
      bit = 0x8000u;

#if !defined(__linux__)
    if ((flashPatterns[state] & bit) > 0)
      LED_ON(RED_LED);
    else
      LED_OFF(RED_LED);
#else
    if ((flashPatterns[state] & bit) > 0)
      gpio_set_value(SW_DEBUG_1, true);
    else
      gpio_set_value(SW_DEBUG_1, false);
#endif
    if ((!getIO(SW_DEBUG_2)) && (accum < FCC_BUTTON_THRESHOLD))
    {
      if (++accum >= FCC_BUTTON_THRESHOLD)
        isp = true;
    }
    else if (getIO(SW_DEBUG_2) && (accum > 0))
    {
      if (--accum == 0)
        isp = false;
    }

    if (isp && !wasp)
    {
      state++;
      if (state >= BAD)
        state = UNMOD_LOW;
#if defined(__linux__)
      printf("state=%u\n", state);
#endif
      switch (state)
      {
      case UNMOD_LOW:
        setMode(0, M_TX);
        break;
      case MOD_LOW:
        setMode(0, M_MOD | M_TX);
        break;
      case UNMOD_MID:
        setMode(NUM_HOP_CHANNELS / 2, M_TX);
        break;
      case MOD_MID:
        setMode(NUM_HOP_CHANNELS / 2, M_MOD | M_TX);
        break;
      case UNMOD_HIGH:
        setMode(NUM_HOP_CHANNELS - 1, M_TX);
        break;
      case MOD_HIGH:
        setMode(NUM_HOP_CHANNELS - 1, M_MOD | M_TX);
        break;
      case UNMOD_HOP:
        setMode(0, M_HOP | M_TX);
        break;
      case MOD_HOP:
        setMode(0, M_MOD | M_HOP | M_TX);
        break;
      case IDLE:
        setMode(0, 0);
        break;
      default:
        /* UNREACHABLE */
#if !defined(__linux__)
        WDTCTL = 0;
#endif
        break;
      }
    }

    wasp = isp;
  }
}

static void setMode(unsigned char channel, unsigned char mode)
{
  sendChipconCommandStrobe(SIDLE);

  if (mode & M_HOP)
  {
    houseCode = 1;
#if !defined(__linux__)
    TACCR2 = TAR + RFBitTime * FB_TOTAL;
    TACCTL2 = CCIE;
#else
    // start hopping timer
    hoppingOn();
    //enableIntervalTimer(true);
#endif
  }
  else
  {
#if !defined(__linux__)
    TACCTL2 = 0;
#else
    // stop hopping timer
    hoppingOff();
    //enableIntervalTimer(false);
#endif
  }

  if (mode & M_MOD)
    dataOn();
  else
    dataOff();

  sendChannel(channel);

  if (mode & M_TX)
    sendChipconCommandStrobe(STX);
  else
    sendChipconCommandStrobe(SRX);
}



/*******************************************************************************

DESCRIPTION: Register the timer handler with alarm signal

NOTES:       Must be called BEFORE enabling timer

*******************************************************************************/
#if defined(__linux__)
/*static void registerTimerHandler(void)
{
  struct sigaction sigact;
  memset(&sigact, 0, sizeof sigact);
  sigact.sa_handler = &TimerFCC;
  sigaction(SIGALRM, &sigact, NULL);
}
*/
#endif


/*******************************************************************************

DESCRIPTION: Uses Linux timers to set the frameTimeout and Bit Counter

NOTES:       Must have the sig handler already registered

*******************************************************************************/
#if defined(__linux__)
static void enableIntervalTimer(bool start)
{
  struct itimerval timer;
  memset(&timer, 0, sizeof timer);
  if (!start)
  {
    setitimer(ITIMER_REAL, &timer, NULL);
    printf("timer disabled\n");
  }
  else
  {

#define MICROSECONDS_PER_SEC 1000000
#define BITS_PER_BYTE 8
// 9595.87 bits per second; 300 bits per channel hop
// () are positioned to minimize compiler division truncation
// FRAME Timing is INTERVAL_TIMER_DELAY_USEC ((MICROSECONDS_PER_SEC * FB_TOTAL) / (RF_BITS_PER_SECOND))
// BYTE  Timing is INTERVAL_TIMER_DELAY_USEC ((MICROSECONDS_PER_SEC * BITS_PER_BYTE) / (RF_BITS_PER_SECOND))

#define INTERVAL_TIMER_DELAY_USEC ((MICROSECONDS_PER_SEC * BITS_PER_BYTE) / (RF_BITS_PER_SECOND))

	timer.it_value.tv_sec =         INTERVAL_TIMER_DELAY_USEC / 1000000;
    timer.it_value.tv_usec =        INTERVAL_TIMER_DELAY_USEC % 1000000;
    timer.it_interval.tv_sec  =     INTERVAL_TIMER_DELAY_USEC / 1000000;
    timer.it_interval.tv_usec =     INTERVAL_TIMER_DELAY_USEC % 1000000;
    setitimer(ITIMER_REAL, &timer, NULL);

    printf("timer enabled; timeout = %lu\n", (unsigned long)(INTERVAL_TIMER_DELAY_USEC));
  }
}
#endif
