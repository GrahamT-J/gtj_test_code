/*H*****************************************************************************
FILENAME:

DESCRIPTION:

NOTES:

Copyright (c) Digital Monitoring Products Inc. 1999.  All rights reserved.

	Last change:  TDC  27 Jan 99    8:43 am
*****************************************************************************H*/
/*----compilation control-----------------------------------------------------*/
#if defined(__linux__)
/*******************************************************************************
INCLUDE FILES
*******************************************************************************/
/*----system and platform files (external libraries) -------------------------*/


/*----program files-----------------------------------------------------------*/
#include "Hardware.h"
#include "Uart.h"

#include <stdio.h>
#include <string.h>

/*******************************************************************************
EXTERNAL REFERENCES   Only used if not available in a header file.
*******************************************************************************/
/*----data declarations-------------------------------------------------------*/

/*----function prototype------------------------------------------------------*/


/*******************************************************************************
PRIVATE DECLARATIONS  Defined in this module, used only in this module.
*******************************************************************************/
/*----defines, typedefs, structs, unions, enums-------------------------------*/
/*----data declarations-------------------------------------------------------*/
unsigned int RXTXData;
char byteReceived;

/*----function prototype------------------------------------------------------*/
//void TX_Byte (void);
//void RX_Ready (void);
/*----macros------------------------------------------------------------------*/


/*******************************************************************************
PUBLIC FUNCTION DEFINITIONS  Note: Header is repeated for each function.
*******************************************************************************/
/*F*****************************************************************************

DESCRIPTION:

NOTES:

*****************************************************************************F*/
char waitForString (char * string, unsigned int max_length)
{
  if (fgets(string, max_length, stdin) != NULL)
  {
    // Strip new line
	size_t eolPos = strcspn(string, "\r\n"); // works for LF, CR, CRLF, LFCR
    string[eolPos] = '\0';

    return eolPos;
  }
  else
  {
    return 0;
  }
}

/*F*****************************************************************************

DESCRIPTION:

NOTES:

*****************************************************************************F*/
void sendString (const char * string)
{
  printf("%s", string);
}

/*F*****************************************************************************

DESCRIPTION:

NOTES:

*****************************************************************************F*/
char waitForChar (void)
{
  char returnValue;

  returnValue = getchar();

  return(returnValue);
}


/*F*****************************************************************************

DESCRIPTION:

NOTES:

*****************************************************************************F*/
void initUart(void)
{
  // setup stdin and stdout if necessary
}

/*******************************************************************************
PRIVATE FUNCTION DEFINITIONS  Note: Header is repeated for each function.
*******************************************************************************/
/*f*****************************************************************************

DESCRIPTION:

NOTES:

*****************************************************************************f*/
// Function Transmits Character from RXTXData Buffer
void TX_Byte (void)
{
  printf("%c", RXTXData);
}

/*f*****************************************************************************

DESCRIPTION:

NOTES:

*****************************************************************************f*/
// Function Readies UART to Receive Character into RXTXData Buffer
// Sync capture not possible as DCO=TACLK=SMCLK can be off !!
void RX_Ready (void)
{
}

/*f*****************************************************************************

DESCRIPTION:

NOTES:

*****************************************************************************f*/
// Timer A0 interrupt service routine
void Timer_A (void)
{
  //CCR0 += Bitime;                           // Add Offset to CCR0
}

extern void TimerA2ISR(void);

void TIMER_A1(void)
{
  // This function checked TAIV timer
  // if 0x02 then update TACCTL1
  // if 0x04 then call TimerA2ISR();
}

#endif // defined(__linux__)
