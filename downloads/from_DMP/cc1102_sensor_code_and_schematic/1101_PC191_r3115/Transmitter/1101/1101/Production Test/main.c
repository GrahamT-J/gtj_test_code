/* main.c contains the main loop for the production test.
 *
 * Copyright Digital Monitoring Products Inc. 2004-2015.
 */

#ifdef DMP_FCC_TEST
#ifdef DMP_PRODUCTION_TEST
#error "Cannot define both DMP_FCC_TEST and DMP_PRODUCTION_TEST."
#endif
#else
#ifndef DMP_PRODUCTION_TEST
#error "Must define one of DMP_FCC_TEST or DMP_PRODUCTION_TEST."
#endif
#endif

#include "Hardware.h"
#include "Uart.h"
#include "ProtocolTiming.h"
#include "chipcon.h"
#include "Hop.h"

#if defined(__linux__)
#include <signal.h>
#include <time.h>
//#include <sys/time.h>
#include <sys/timerfd.h>

#include <stdio.h>
#endif

#ifdef DMP_FCC_TEST
void FCCMain(char startFCCState);
#endif

#if defined(__linux__)
timer_t timerID_hopTimeout = 0;  // timer signifies a need for hopping
timer_t timerID_byteTimeout = 0; // timer signifies enough time has passed for a byte to have been transmitted
#endif


/* TXBUFLEN is the maximum number of bytes in the transmit buffer. */
#define TXBUFLEN 6

/* txBuf is the transmit buffer; when in transmit mode, the data in
 * txBuf[0..txLen-1] is sent continuously.  The bit sent next is a one if
 * txBuf[txByte] & txBit is non-zero or zero otherwise.
 */
static unsigned char txBuf[TXBUFLEN];
static unsigned char txBit;
static unsigned txByte;
static unsigned txLen;

unsigned int syncBitCounter = 0;
unsigned int patternRegister = 0;
bool TX = false;
bool MOD = false;
bool BERDone = false;
byte BERCounter = 0;

int hexToInt(const char* string, char length);
unsigned long hexToLong(const char* string, char length);
void intToHex(int val, char* string, char length);

void configRegisterPoke(void);
void commandStrobe(void);
void statusRegisterPeek(void);
void showList(void);
void TransmitData(void);
void ReadRSSI(void);
void setFrequency(void);
void setMode(void);
void configRegisterPeek(void);
void initRFChip(void);
void sendDMPChannel(void);
void toggleLED(void);
void dataOn(void);
void dataOff(void);
void frequencySet(void);
void berTest(void);
void readZones(void);
void modeSelect(void);
void toggleClock(void);
void decrementChannel(void);
void incrementChannel(void);
void debounceDelay(void);

#if defined(__linux__)
int createTimer(timer_t *timerID);
void linuxTimerHandler(int sig, siginfo_t *sigInfo, void *context);
#endif

const struct
{
  const char command;
  void (*function)(void);
} commandList[] =
{
  {'1', setFrequency},
  {'2', setMode},
  {'3', ReadRSSI},
  {'4', TransmitData},
  {'5', configRegisterPoke},
  {'6', configRegisterPeek},
  {'7', commandStrobe},
  {'8', statusRegisterPeek},
  {'9', initRFChip},
  {'A', sendDMPChannel},
  {'D', dataOn},
  {'d', dataOff },
  {'F', frequencySet},
  {'B', berTest},
  {'I', toggleLED},
  {'i', toggleLED},
  {'K', readZones},
  {'M', modeSelect},
  {'J', toggleClock},
  {'j', toggleClock},
  {'+', incrementChannel},
  {'-', decrementChannel}
};

#if defined(__linux__)
/**
 * Main class of production test / fcc test
 *
 * @param argc the number of arguments
 * @param argv the arguments from the commandline
 * @returns exit code of the application
 */
int main(int argc, char** argv)
#else
void main (void)
#endif
{
#ifdef DMP_PRODUCTION_TEST
  char command;
  char lcv;
#endif

#if defined(__linux__)
  const unsigned char spiDevNum = 0;
  const bool useGpioCS = true;
  const unsigned char gpioCSNum = 10;
  initializeSpiDriver(spiDevNum, useGpioCS, gpioCSNum);
#endif

  initHardware();

#ifdef LOW_INTERFERENCE
  SetFrequencyMode(LOW_INTERFERENCE_FREQUENCY_MODE);
#else
  SetFrequencyMode(STANDARD_FREQUENCY_MODE);
#endif

#ifdef DMP_FCC_TEST

  char startFCCState = 0;
#if defined(__linux__)
  createTimer(&timerID_hopTimeout);
  createTimer(&timerID_byteTimeout);
#endif
  FCCMain(startFCCState);

#else
  sendChannel(26);
  writeChipconConfigRegister(DEVIATN, 0x00);
  sendChipconCommandStrobe(STX);

  sendString("MODE=02 RTP=6B75 FSP=D00000 ADP=A160 PAT=817E817E\n\r");
  for (;;)
  {
    command = waitForChar();
    for (lcv = 0; lcv < sizeof(commandList)/sizeof(commandList[0]); lcv++)
    {
      if (command == commandList[lcv].command)
      {
        commandList[lcv].function();
        break;
      }
    }
  }
#endif
}

void incrementChannel(void)
{
    sendChipconCommandStrobe(SIDLE);
    if (++currentChannel >= NUM_HOP_CHANNELS)
      currentChannel = 0;
    sendChannel(currentChannel);
    sendChipconCommandStrobe(STX);
}

void decrementChannel(void)
{
    sendChipconCommandStrobe(SIDLE);
    if (currentChannel == 0)
      currentChannel = NUM_HOP_CHANNELS - 1;
    else
      currentChannel--;
    sendChannel(currentChannel);
    sendChipconCommandStrobe(STX);
}

void configRegisterPoke(void)
{
  char address[6];
  char value[6];
  char stringLength = 0;
  unsigned int addressInt;
  unsigned int valueInt;

  sendString("\n\rEnter the address: ");
  stringLength = waitForString(&address[0], sizeof address);
  addressInt = hexToInt(&address[0],stringLength);
  sendString("\n\rEnter the value: ");
  stringLength = waitForString(&value[0], sizeof value);
  valueInt = hexToInt(&value[0],stringLength);
  if(addressInt < 0x30 && valueInt < 0xFF)
    writeChipconConfigRegister((ConfigRegisters)addressInt,valueInt);
}

void configRegisterPeek(void)
{
  char address[6];
  char stringLength = 0;
  unsigned int addressInt;
  unsigned int valueInt;

  sendString("\n\rEnter the address in HEX: ");
  stringLength = waitForString(&address[0], sizeof address - 1);
  addressInt = hexToInt(&address[0],stringLength);
  if(addressInt < 0x30)
  {
      valueInt = readChipconConfigRegister((ConfigRegisters)addressInt);
      sendString("\n\rThe register value: ");
      intToHex(valueInt, &address[0], 4);
      sendString(&address[0]);
      sendString("\n\rPress Any Key");
      waitForChar();
  }
}

void TransmitData(void)
{
  unsigned i;
  char str[sizeof(txBuf)*2];

  clearIOEnableInt(RF_DATA_CLK);
  sendChipconCommandStrobe(SIDLE); // put the RF Chip into Idle mode
  writeChipconConfigRegister(MDMCFG2, 0x00);
  setIODirOut(RF_DATA_OUT);

  sendString("\n\rEnter the Data Pattern in HEX: ");
  waitForString(str, sizeof str);

  for (i = 0; i < sizeof(txBuf); i++)
    txBuf[i] = hexToInt(str + i * 2, 2);

  txByte = 0;
  txBit = 0x80;

  TX = true;
  MOD = true;
  sendChipconCommandStrobe(STX);  // change the RF chips mode to TX
      // Prepare for interrupts on the data clk line
  clearIOIntFlag(RF_DATA_CLK);
  setIORisingInt(RF_DATA_CLK);
  setIOEnableInt(RF_DATA_CLK);
}

void toggleLED(void)
{
  sendString(" LED pin in use by UART\n\r");
  /*
  if ( getIO(RED_LED) )
  {
    sendString("luminate off\n\r");
    clearIO(RED_LED);
  }
  else
  {
    sendString("luminate on\n\r");
    setIO(RED_LED);
  }
  */
}

void initRFChip(void)
{
  initChipcon();
  sendString("\n\rChipcon Chip Set To Defaults");
  waitForChar();
}


void sendDMPChannel(void)
{
  int channel;
  unsigned char stringLength;
  char channelString[6];

  sendString("\n\rPlease enter a channel");
  stringLength = waitForString(&channelString[0], sizeof channelString);
  channel = hexToInt(&channelString[0], stringLength);
  sendChannel(channel);
}

void ReadRSSI(void)
{
  int RSSIDataInt;
  char RSSIData[6];

  RSSIDataInt = readChipconStatusRegister(RSSI);
  sendString("\n\rCurrent RSSI: ");
  intToHex(RSSIDataInt, &RSSIData[0], 4);
  sendString(&RSSIData[0]);
  sendString("\n\rPress Any Key");
  waitForChar();
  //printf("\n\rCurrentRSSI: %d", RSSIData);
  //printf("\n\rPress any key:");
  //scanf("%c");
}

void setFrequency(void)
{
  unsigned long dataLong;
  char data[7];
  char tempData;

  //printf("\n\rEnter the Frequency Register Value: ");
  sendString("\n\rEnter the Frequency Register Value in HEX: ");
  //scanf("%6Lx", &data);
  tempData = waitForString(&data[0], sizeof data);
  dataLong = hexToLong(&data[0], tempData);
  if(dataLong < 0xFFFFFF)
  {
    tempData = dataLong & 0xFF;
    writeChipconConfigRegister(FREQ0, tempData);
    tempData = dataLong>>8 & 0xFF;
    writeChipconConfigRegister(FREQ1, tempData);
    tempData = dataLong>>16 & 0xFF;
    writeChipconConfigRegister(FREQ2, tempData);
  }

}

void setMode(void)
{
  char userChoice;

  do
  {
    sendString("\f1)\tTX Mode");
    sendString("\n\r2)\tRX Mode");
    sendString("\n\r3)\tSleep Mode");
    sendString("\n\r4)\tIdle Mode");
    sendString("\n\rEnter the mode:");
    userChoice = waitForChar();
  }while( userChoice > '5' || userChoice < '1');
  switch( userChoice )
  {
  case '1':
    sendChipconCommandStrobe(STX);
    TX = true;
    break;
  case '2':
    sendChipconCommandStrobe(SRX);
    TX = false;
    break;
  case '3':
    sendChipconCommandStrobe(SPWD);
    break;
  case '4':
    sendChipconCommandStrobe(SIDLE);
    break;
  }

}

void commandStrobe(void)
{
  int addressInt;
  char address[6];
  char stringLength;

  sendString("\n\rEnter the address in HEX: ");
  stringLength = waitForString(&address[0], sizeof address);
  addressInt = hexToInt(&address[0], stringLength);
  if(addressInt >= 0x30 && addressInt < 0x3F)
    sendChipconCommandStrobe((CommandStrobes)addressInt);
}

void statusRegisterPeek(void)
{
    int addressInt, valueInt = 0;
    char address[6];
    char stringLength = 0;
    //printf("\n\rEnter the address: ");
    sendString("\n\rEnter the address in HEX: ");
    //scanf("%2x", &address);
    stringLength = waitForString(&address[0], sizeof address);
    addressInt = hexToInt(&address[0], stringLength);
    if(addressInt >= 0x30 && addressInt < 0x3f)
    {
      valueInt = readChipconStatusRegister((StatusRegisters)addressInt);
      sendString("\n\rThe register value in HEX: ");
      intToHex(valueInt, &address[0], 4);
      sendString(&address[0]);
      sendString("\n\rPress Any Key");
      waitForChar();
      //printf("\n\rThe register value: %x", value);
      //printf("\n\rPress Any Key:");
      //scanf("%c");
    }
}

/*******************************************************************************

DESCRIPTION:  Converts integer 'val' into a hex string 'string' of length 'length'

NOTES: does not work with length > 4

*******************************************************************************/
void intToHex(int val, char* string, char length)
{
  const char hexTable[] = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
  char i;
  int workingVal = val;
  char* writeString = string + length;  // start at rightmost character
  *writeString = 0;
  writeString--;
  for (i = 0; i < length; ++i)
  {
    *writeString = hexTable[workingVal & 0x000F];
    --writeString;
    workingVal >>= 4;
  }
}

/*******************************************************************************

DESCRIPTION: converts 'length' characters of ascii hexadecimal 'string' to an integer

NOTES:

*******************************************************************************/
int hexToInt(const char* string, char length)
{
  int returnValue = 0;
  char i;
  bool inv = false;

  for (i = 0; i < length; ++i)
  {
    char character = string[i];
    if (character == '-' && i == 0)
    {
      inv = true;
      continue;
    }

    if (character >= 'A' && character <= 'F')
      character -= 'A' - 10;
    else if (character >= '0' && character <= '9')
      character -= '0';
    else
      continue;  // ignore invalid character

    returnValue *= 16;
    returnValue += character;
  }

  if (inv)
    return -returnValue;
  else
    return returnValue;
}


unsigned long hexToLong(const char* string, char length)
{
  char i;
  unsigned long returnValue = 0;
  for (i = 0; i < length; ++i)
  {
    char character = string[i];
    if ((character >= 'A') && (character <= 'F'))
    {
      character -= ('A' - 10);
    }
    else if ((character >= '0') && (character <= '9'))
    {
      character -= '0';
    }
    else
    {
      continue;  // ignore invalid character
    }
    returnValue *= 16;
    returnValue += character;
  }
  return returnValue;
}


void rfDataClockEdge(void)
{
  unsigned char bit = getIO(RF_DATA_IN) > 0;

  if (TX)
  {
    bit = txBuf[txByte] & txBit;
    if (bit > 0)
      setIO(RF_DATA_OUT);
    else
      clearIO(RF_DATA_OUT);

    if ((txBit >>= 1) == 0)
    {
      txBit = 0x80;

      if (++txByte >= txLen)
        txByte = 0;
    }
  }
  else
  {
    patternRegister = (patternRegister << 1) | bit;
    syncBitCounter++;

    if (patternRegister == 0x817E)
    {
      patternRegister = 0;
      BERCounter++;
    }
    else if (syncBitCounter > 2000)
    {
      clearIOEnableInt(RF_DATA_CLK);
      BERDone = true;
    }
  }
}

/*******************************************************************************

DESCRIPTION:

NOTES: This assumes that any edge interrupts on this port are for alarm contacts

*******************************************************************************/
#ifdef __ICC430__
#pragma vector = PORT1_VECTOR
#pragma type_attribute = __interrupt
#endif
void port1EdgeInterrupt(void)
#if !defined(__ICC430__)  && !defined(__linux__)
__interrupt[PORT1_VECTOR]
#endif
{
    // first check for rf clock edge
  if (getIOIntFlag(RF_DATA_CLK))
  {
    clearIOIntFlag(RF_DATA_CLK);
    rfDataClockEdge();
  }
  //else if(getIOIntFlag(TAMPER))
  //{
  //  clearIOIntFlag(TAMPER);
  //  debounceDelay();
  //  if(!getIO(TAMPER))
  //    incrementChannel();
  //}
}


#define MICRO_SECONDS_PER_SEC 1000000
#define NANO_SECONDS_PER_SEC (1000 * MICRO_SECONDS_PER_SEC)
#define BITS_PER_BYTE 8
// 9595.87 bits per second; 300 bits per channel hop
// () are positioned to minimize compiler division truncation
// FRAME Timing is INTERVAL_TIMER_DELAY_USEC ((MICROSECONDS_PER_SEC * FB_TOTAL) / (RF_BITS_PER_SECOND))
// BYTE  Timing is INTERVAL_TIMER_DELAY_USEC ((MICROSECONDS_PER_SEC * BITS_PER_BYTE) / (RF_BITS_PER_SECOND))

#define BYTE_TIMER_DELAY_MICRO_SEC ((MICRO_SECONDS_PER_SEC * BITS_PER_BYTE) / (RF_BITS_PER_SECOND))
#define FRAME_TIMER_DELAY_MICRO_SEC ((MICRO_SECONDS_PER_SEC * FB_TOTAL) / (RF_BITS_PER_SECOND))

const unsigned long BYTE_TIMER_DELAY_NANO_SEC =  1000 * BYTE_TIMER_DELAY_MICRO_SEC;
const unsigned long FRAME_TIMER_DELAY_NANO_SEC = 1000 * FRAME_TIMER_DELAY_MICRO_SEC;


void dataOn (void)
{
#ifndef DMP_FCC_TEST
  sendString("ata on\n\r");
#endif
  clearIOEnableInt(RF_DATA_CLK);
  sendChipconCommandStrobe(SIDLE); // put the RF Chip into Idle mode
  writeChipconConfigRegister(MDMCFG2, 0x00);
  writeChipconConfigRegister(DEVIATN, 0x35);
  setIODirOut(RF_DATA_OUT);

  txBuf[0] = 0xaa;
  txBit = 0x80;
  txByte = 0;
  txLen = 1;
  TX = true;

#if defined(__linux__)
  // Check timer is created
  if (timerID_byteTimeout)
  {
    struct itimerspec timerSpec;

    timerSpec.it_value.tv_sec = BYTE_TIMER_DELAY_NANO_SEC / NANO_SECONDS_PER_SEC;
    timerSpec.it_value.tv_nsec = BYTE_TIMER_DELAY_NANO_SEC % NANO_SECONDS_PER_SEC;
    timerSpec.it_interval.tv_sec = BYTE_TIMER_DELAY_NANO_SEC / NANO_SECONDS_PER_SEC;
    timerSpec.it_interval.tv_nsec = BYTE_TIMER_DELAY_NANO_SEC % NANO_SECONDS_PER_SEC;

    printf("Modulation on: timerID=(0x)%lx\n", (long)timerID_byteTimeout);
    printf("Modulation on: timeout = %lus.%lu, %lus.%lu\n",
          (unsigned long) timerSpec.it_value.tv_sec,
          (unsigned long) timerSpec.it_value.tv_nsec,
          (unsigned long) timerSpec.it_interval.tv_sec,
          (unsigned long) timerSpec.it_interval.tv_nsec
          );
    timer_settime(timerID_byteTimeout, 0, &timerSpec, NULL);
  }
#endif

  sendChipconCommandStrobe(STX);
  clearIOIntFlag(RF_DATA_CLK);
  setIORisingInt(RF_DATA_CLK);
  setIOEnableInt(RF_DATA_CLK);
}

void dataOff (void)
{
#ifndef DMP_FCC_TEST
  sendString("ata off\n\r");
#endif

#if defined(__linux__)
  // Check timer is created
  if (timerID_byteTimeout)
  {
    printf("Modulation off: timer disabled. timerID=(0x)%lx\n", (long)timerID_byteTimeout);
    struct itimerspec timerSpec;

    timerSpec.it_value.tv_sec = 0;
    timerSpec.it_value.tv_nsec = 0;
    timerSpec.it_interval.tv_sec = 0;
    timerSpec.it_interval.tv_nsec = 0;
    timer_settime(timerID_byteTimeout, 0, &timerSpec, NULL);
  }
#endif

  clearIOEnableInt(RF_DATA_CLK);
  writeChipconConfigRegister(DEVIATN, 0x00);
}

#if defined(__linux__)
void hoppingOn(void)
{
  // Check timer is created
  if (timerID_hopTimeout)
  {
    struct itimerspec timerSpec;

    timerSpec.it_value.tv_sec = FRAME_TIMER_DELAY_NANO_SEC / NANO_SECONDS_PER_SEC;
    timerSpec.it_value.tv_nsec = FRAME_TIMER_DELAY_NANO_SEC % NANO_SECONDS_PER_SEC;
    timerSpec.it_interval.tv_sec = FRAME_TIMER_DELAY_NANO_SEC / NANO_SECONDS_PER_SEC;
    timerSpec.it_interval.tv_nsec = FRAME_TIMER_DELAY_NANO_SEC % NANO_SECONDS_PER_SEC;

    printf("Hopping on: timerID=(0x)%lx\n", (long)timerID_hopTimeout);
    printf("Hopping on: timeout = %lus.%lu, %lus.%lu\n",
          (unsigned long) timerSpec.it_value.tv_sec,
          (unsigned long) timerSpec.it_value.tv_nsec,
          (unsigned long) timerSpec.it_interval.tv_sec,
          (unsigned long) timerSpec.it_interval.tv_nsec
          );
    timer_settime(timerID_hopTimeout, 0, &timerSpec, NULL);
  }
}
#endif

#if defined(__linux__)
void hoppingOff(void)
{
  // Check timer is created
  if (timerID_hopTimeout)
  {
    printf("Hopping off: timer disabled. timerID=(0x)%lx\n", (long)timerID_hopTimeout);
    struct itimerspec timerSpec;

    timerSpec.it_value.tv_sec = 0;
    timerSpec.it_value.tv_nsec = 0;
    timerSpec.it_interval.tv_sec = 0;
    timerSpec.it_interval.tv_nsec = 0;
    timer_settime(timerID_hopTimeout, 0, &timerSpec, NULL);
  }
}
#endif

void frequencySet (void)
{
  unsigned long dataLong;
  char data[7];
  char tempData;

  sendChipconCommandStrobe(SIDLE);
  sendString("SP= ");
  tempData = waitForString(&data[0], sizeof data);
  dataLong = hexToLong(&data[0], tempData);
  writeChipconConfigRegister(DEVIATN, 0x00);

  if (dataLong == 0xd09aac)
    dataLong = 0x22b13B;
  else if (dataLong == 0xd00000)
    dataLong = 0x23313b;
  else if (dataLong == 0xd06554)
    dataLong = 0x23b13b;

  if(dataLong < 0xFFFFFF)
  {
    tempData = dataLong & 0xFF;
    writeChipconConfigRegister(FREQ0, tempData);
    tempData = dataLong>>8 & 0xFF;
    writeChipconConfigRegister(FREQ1, tempData);
    tempData = dataLong>>16 & 0xFF;
    writeChipconConfigRegister(FREQ2, tempData);
  }
  if (TX)
    sendChipconCommandStrobe(STX);
  else
    sendChipconCommandStrobe(SRX);
  sendString("\n\r");
}

void berTest (void)
{
  char hexString[5];

  clearIOEnableInt(RF_DATA_CLK); // disable the DCLK interrupt before configureing RF Chip
  writeChipconConfigRegister(MDMCFG2, 0x00); //turn off sync word detection
  setIODirIn(RF_DATA_IN); // set the data pin direction
    // Prepare for interrupts on the data clk
  syncBitCounter = 0;
  BERDone = false;
  BERCounter = 0;
  clearIOIntFlag(RF_DATA_CLK);
  setIORisingInt(RF_DATA_CLK);
  setIOEnableInt(RF_DATA_CLK);
  while(!BERDone);
  sendString("ER= ");
  intToHex(BERCounter, &hexString[0], 2);
  sendString(&hexString[0]);
  sendString("\n\r");
}

void readZones (void)
{
  unsigned char zoneState = 0;
  char hexString[3];

  sendChipconCommandStrobe(SIDLE);
  sendString("=");

  initZoneIO();
#if !defined(__linux__)
  for (unsigned char i = 0; i < 8; i++)
  {
    // Set MUX_A0, MUX_A1, and MUX_A2
    P2OUT = (P2OUT & ~(BIT1 | BIT2 | BIT3)) | (i << 1);

    // The multiplexer propagation delay is 12ns at 3.3v, one processor cycle
    // is 125ns (8 MHz).
    __delay_cycles(1);

    if (getIO(ZONE))
    {
      zoneState |= (1 << i);
    }
  }
#else
// collect zone states from file notifications
#endif

  initUart();

  intToHex(zoneState, hexString, 2);
  sendString(hexString);
  sendString("\n\r");
}

void modeSelect (void)
{
  char modeString;

  sendString("= ");
  modeString = waitForChar();
  switch (modeString)
  {
  case '1':
    sendChipconCommandStrobe(SIDLE);
    writeChipconConfigRegister(DEVIATN, 0x35);
    clearIOEnableInt(RF_DATA_CLK);
    setIODirIn(RF_DATA_IN);
    TX = false;
    sendChipconCommandStrobe(SRX);
    break;
  case '2':
    sendChipconCommandStrobe(SIDLE);
    writeChipconConfigRegister(DEVIATN, 0x00);
    clearIOEnableInt(RF_DATA_CLK);
    setIODirOut(RF_DATA_OUT);
    TX = true;
    sendChipconCommandStrobe(STX);
    break;
  case '3':
    sendChipconCommandStrobe(SIDLE);
    sendChipconCommandStrobe(SPWD);
    break;
  }
  sendString("\n\r");
}


void toggleClock(void)
{
#if !defined(__linux__)
  if(TACCTL1 & CCIE)
  {
    TACCTL1 = 0;
    setIODirOut(SI);
    clearIOSel(SI);
    sendString("CLK off\n\r");
  }
  else
  {
    TACCR1 = TAR + 4;
    TACCTL1 = OUTMOD_1 + CCIE;
    setIODirOut(SI);
    setIOSel(SI);
    sendString("CLK on\n\r");
  }
#else
  sendString("ERROR: CLK is operated as part of SPI protocol not individually.");
#endif
}

#ifdef __ICC430__
#pragma vector = PORT2_VECTOR
#pragma type_attribute = __interrupt
#endif
void port2EdgeInterrupt(void)
#if !defined(__ICC430__) && !defined(__linux__)
__interrupt[PORT2_VECTOR]
#endif
{
#if !defined(__linux__)
  P2IFG = 0;
#endif
}

void debounceDelay()
{
  int i;

  for( i=0; i<8125; i++);

  return;
}

#if defined(__linux__)
void TimerA2ISR(int signum)
#else
void TimerA2ISR(void)
#endif
{
#if !defined(__linux__)
  TACCR2 = TAR + RFBitTime * FB_TOTAL;
#endif
  sendChipconCommandStrobe(SIDLE);
  currentChannel = addChannel(currentChannel, houseCode);
  SetFrequency(currentChannel);
  if (TX)
  {
    sendChipconCommandStrobe(STX);
  }
  else
  {
    sendChipconCommandStrobe(SRX);
  }
}

/*******************************************************************************

DESCRIPTION: Combines the hop and tx in one timer interrupt. The timer is byte based

NOTES:

*******************************************************************************/
#if defined(__linux__)
/*void TimerFCC(int signum)
{
  static unsigned int timerCount = 0;
  bool timeToHop = false;

#define BITS_PER_BYTE 8
  // This timer is not going to be perfect as there are not an exact number of bytes to a hop
  // if >= then it will be slightly faster than true hop speed. if > then it will be slighter slower to hop.
  ++timerCount;
  timeToHop = (timerCount >= (FB_TOTAL / BITS_PER_BYTE)) ? true : false;

  if (timeToHop)
  {
    timerCount = 0;
    sendChipconCommandStrobe(SIDLE);
    currentChannel = addChannel(currentChannel, houseCode);
    SetFrequency(currentChannel);
    if (TX)
    {
      sendChipconCommandStrobe(STX);
    }
    else
    {
      sendChipconCommandStrobe(SRX);
    }
  }
  else
  {
    if (MOD)
    {
      unsigned char chipStatus = writeSingleTxFifoByte(txBuf[txByte]);
      if (++txByte >= txLen)
        txByte = 0;
    }
  }
}
*/
#endif


/*******************************************************************************

DESCRIPTION: Creates a timer and stores the resulting id in the structure given

NOTES:       Must be called before timer set time is called on that timer id
             Timer is not started but set to zero
             Uses common timer handler for all timers

*******************************************************************************/
#if defined(__linux__)
int createTimer(timer_t *timerID)
{
  struct sigevent       timerEvent;
  struct itimerspec     iTimerSpec;
  struct sigaction      sigAction;
  int                   sigNo = SIGRTMIN;

  // Setup the signal handler
  sigAction.sa_flags = SA_SIGINFO;
  sigAction.sa_sigaction = linuxTimerHandler;
  sigemptyset(&sigAction.sa_mask);
  if(sigaction(sigNo, &sigAction, NULL) == -1)
  {
	  printf("Failed to setup signal handling for timer\n");
	  return (-1);
  }

  // Setup and disable alarm
  timerEvent.sigev_notify = SIGEV_SIGNAL;
  timerEvent.sigev_signo = sigNo;
  timerEvent.sigev_value.sival_ptr = timerID;
  int status = timer_create(CLOCK_REALTIME, &timerEvent, timerID);

  printf("Create timer: status=%d, timerID=(0x)%lx\n", status, (long) *timerID);

  // No need to set timer as it defaults to off

  return (0);
}
#endif

#if defined(__linux__)
void linuxTimerHandler(int sig, siginfo_t *sigInfo, void *context)
{
  timer_t *pTimerID = sigInfo->si_value.sival_ptr;
  long *pDebugContext = context;

  //printf("LTH: sig=%d, context=(0x)%lx\n", sig, *pDebugContext);
  //printf("LTH: timerID=(0x)%lx\n", *pTimerID);
  if (*pTimerID == timerID_hopTimeout)
  {
    // hop frequency
    //printf("Time to hop\n");
    sendChipconCommandStrobe(SIDLE);
    currentChannel = addChannel(currentChannel, houseCode);
    SetFrequency(currentChannel);
    if (TX)
    {
      sendChipconCommandStrobe(STX);
    }
    else
    {
      sendChipconCommandStrobe(SRX);
    }
  }
  else if (*pTimerID == timerID_byteTimeout)
  {
    // send byte
    //printf("Byte timeout\n");
    unsigned char chipStatus = writeSingleTxFifoByte(txBuf[txByte]);
    if (++txByte >= txLen)
      txByte = 0;
  }
}
#endif
