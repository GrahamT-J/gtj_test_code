/*H*****************************************************************************
FILENAME:

DESCRIPTION:

NOTES:

Copyright (c) Digital Monitoring Products Inc. 1999.  All rights reserved.

	Last change:  TDC  27 Jan 99    8:43 am
*****************************************************************************H*/

/*----compilation control-----------------------------------------------------*/

/*******************************************************************************
INCLUDE FILES   Note: Minimize nesting of header files.
*******************************************************************************/
/*----system and platform files-----------------------------------------------*/

/*----program files-----------------------------------------------------------*/

/*******************************************************************************
FILE CONTEXT
*******************************************************************************/
/*----defines, typedefs, structs, unions, enums-------------------------------*/

/*----data descriptions-------------------------------------------------------*/

/*----data declarations-------------------------------------------------------*/

/*----macros------------------------------------------------------------------*/

/*----function prototypes-----------------------------------------------------*/
extern char waitForString (char * string, unsigned int max_length);
extern void sendString (const char * string);
extern char waitForChar(void);
extern void RX_Ready(void);
extern void TX_Byte(void);
extern void initUart(void);

                                 /* end of file */
