/*
 * ProxValueConverter.c
 *
 *  Created on: Aug 26, 2017
 *      Author: cargt
 */

#include <string.h>

#include "ProxValueConverter.h"

static uint8_t convertKeyValueToMappedCode(uint8_t input);


void PVC_convertToCardReadMessageFormat(uint64_t proxIn, uint8_t* buf, uint8_t buf_size, uint8_t digitsRequired)
{
	// Fill buffer with pad value
	memset(buf, CARD_READ_PAD_BYTE, buf_size);

	if (buf_size < digitsRequired)
		return; // buffer is filled with pad value

	// Extract user code from prox value
	uint64_t userCode = ((proxIn >> USER_CODE_SHIFT_NUM) & USER_CODE_MASK);
	uint8_t digitNumber = 0;

	printf("PVC:prox=0x%" PRIx64 ";user code=0x%" PRIx64 ",%" PRIu64 "(dec)\n", proxIn, userCode, userCode);
	uint8_t base10Digits[NUM_CARD_READ_DIGITS];
	for(int i = 0; i < NUM_CARD_READ_DIGITS; i++)
	{
		// Use base 10 digits and store each in temp buffer
		base10Digits[i] = userCode % 10;
		userCode /= 10; // next digit
	}

	// Format base 10 user code into Nibbles
	// Start at highest digit required and work down
	// If 5 digits required start at idx 4 and go buf[4]->[0] inclusive
	// Corner case: if 4 digits are required then drop lowest base 10 digit,
	// so 4 digits gives buf[4]->[1] inclusive

	// Digits are also swapped at storage into nibbles
	// A byte sent to panel will contain 2 user digits as nibbles
	// Pairs of digits are processed where,
	// Highest digit -> lower nibble first byte
	// 2nd highest digit -> upper nibble first byte
	// ...
	// All spare nibbles must have 0xB - see prev memset

	// Corner case: if 4 digits are required then drop lowest base 10 digit
	uint8_t base10idx = (4 == digitsRequired) ? 4 : (digitsRequired - 1);

	uint8_t * tempDest = buf;
	for (int i = 0; i < digitsRequired; ++i)
	{
		uint8_t mappedValue = convertKeyValueToMappedCode(base10Digits[base10idx]);

		if (i & 1)
		{
			// (odd loop index) second value in pair into upper nibble
			buf[(i/2)] &= 0x0F; // reset upper nibble
			buf[(i/2)] |= (mappedValue << 4);
		}
		else
		{
			// (even loop index) first value in pair into lower nibble
			buf[(i/2)] &= 0xF0; // reset lower nibble
			buf[(i/2)] |= mappedValue;
		}
		--base10idx; // decrement down the base10 number towards least significant
	}
}

static uint8_t convertKeyValueToMappedCode(uint8_t input)
{
	// Only accepts digits 0->9
	switch(input)
	{
	case 1: return 4; break;
	case 2: return 5; break;
	case 3: return 6; break;
	case 4: return 8; break;
	case 5: return 9; break;
	case 6: return 10; break;
	case 7: return 12; break;
	case 8: return 13; break;
	case 9: return 14; break;
	case 0: return 15; break;

	default: return 24; // unused
	}
}

