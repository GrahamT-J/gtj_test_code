/*******************************************************************************
FILENAME:

DESCRIPTION:

NOTES:

Copyright (c) Digital Monitoring Products Inc. 2009, 2016 - 2017.  All rights reserved.

*******************************************************************************/
/*----compilation control-----------------------------------------------------*/

/*******************************************************************************
INCLUDE FILES
*******************************************************************************/
/*----system and platform files (external libraries) -------------------------*/

/*----program files-----------------------------------------------------------*/
#include <string.h>

#include "Sleep.h"
#include "TransmitterProtocol.h"
#include "WirelessCommunication.h"
#include "IndicatorLED.h"
#include "Inputs.h"
#include "ProxValueConverter.h"


#if defined(__linux__)
#include <stdio.h>

#include "doorbell_inotify_inputs.h"

#endif


/*******************************************************************************
EXTERNAL REFERENCES   Only used if not available in a header file.
*******************************************************************************/
/*----data declarations-------------------------------------------------------*/
extern SlotNumber currentSlotNumber;

/*----function prototype------------------------------------------------------*/


/*******************************************************************************
PRIVATE DECLARATIONS
*******************************************************************************/
/*----defines, typedefs, structs, unions, enums-------------------------------*/
#ifdef USE_SCANNED_INPUTS
#define SOFTWARE_VERSION 2
#else
#define SOFTWARE_VERSION 1
#endif

typedef enum
{
  TMT_SERIAL_MESSAGE,
  TMT_ZONE_MESSAGE,
  TMT_CHECKIN_MESSAGE,
  TMT_NO_MESSAGE,
  TMT_COMMAND_ACK_WITH_ZONE_MESSAGE,
  TMT_SINGLE_KEY_PRESS_MESSAGE,
  TMT_CARD_READ_MESSAGE,
  TMT_RETRY_LAST_MESSAGE_TYPE_SENT,
} TransmitterMessageType;

enum
{
  MAX_KEYPAD_RETRIES = 5
};

/*----data declarations-------------------------------------------------------*/

AckStatus ackStatus = {.counter = 0};

#if defined(__linux__)
// NOTE: TODO currently hardcoded for testing
long serial_number = 14500002; //1928872;//0x000055AA; // default value will need to be adjusted
long SERIAL_NUMBER_ADDRESS = &serial_number;
#endif

static doorbell_event_t txActiveDoorbellEvent = DOORBELL_EVENT_NONE;

/*----function prototype------------------------------------------------------*/
void formatTransmitterMessage(TransmitterMessageType messageType);

static SlotTransmitterNumber myZoneNumber = INVALID_TRANSMITTER_NUMBER;
static SlotNumber myCheckinSlotNumber = INVALID_SLOT;
static unsigned short transmitterCrc;
ZoneFlags myZoneFlags;

static unsigned char keypadRetries = 0;
static unsigned char savedCardRead[KEYPAD_CARD_READ_MESSAGE_LENGTH];

/*----macros------------------------------------------------------------------*/

/*******************************************************************************
PRIVATE FUNCTION DEFINITIONS
*******************************************************************************/
/*******************************************************************************

DESCRIPTION: Sets CRC for transmitter message, including any extra
information that is expected, and sets transmitter packet length

NOTES:

*******************************************************************************/
static void setActiveDoorbellEvent(doorbell_event_t event);
static void setActiveDoorbellEvent(doorbell_event_t event)
{
  txActiveDoorbellEvent = event;
}

static doorbell_event_t getActiveDoorbellEvent(void);
static doorbell_event_t getActiveDoorbellEvent(void)
{
  return txActiveDoorbellEvent;
}

static void clearActiveDoorbellEvent(void);
static void clearActiveDoorbellEvent(void)
{
  printf("clearActiveDoorbellEvent:event=%u\n", txActiveDoorbellEvent);
  if (DOORBELL_EVENT_NONE != txActiveDoorbellEvent)
  {
    DRBL_INOTIFYIN_resetDoorbellEvent(txActiveDoorbellEvent);
    txActiveDoorbellEvent = DOORBELL_EVENT_NONE;
  }
}

static void finalizeTransmitterMessage(char size, AdditionalCheckItems items)
{
  unsigned short crc = INITIAL_CRC;
  // first, start with any additional items
  if (items & ACI_HOUSE_CODE)
  {
    calcCRC16ForChar(houseCode, &crc);
  }
  if (items & ACI_ZONE_NUMBER)
  {
    calcCRC16ForChar(myZoneNumber >> 8, &crc);
    calcCRC16ForChar(myZoneNumber & 0xFF, &crc);
  }
  if (items & ACI_SERIAL_NUMBER)
  {
    calcCRC16ForChar(serialNumber & 0xFF, &crc);
    calcCRC16ForChar((serialNumber >> 8) & 0xFF, &crc);
    calcCRC16ForChar((serialNumber >> 16) & 0xFF, &crc);
  }
  encodeWirelessBuffer(transmitterPacketBuffer, size, crc);
  transmitterCrc = (transmitterPacketBuffer[size - 1] << 8) | transmitterPacketBuffer[size - 2];
  transmitterPacketSize = size;
}

/*******************************************************************************

DESCRIPTION:  Handles retries for messages being sent to the receiver

NOTES:

*******************************************************************************/
static void HandleRetries(TransmitterMessageType *currentMessageType)
{
  static TransmitterMessageType lastMessageTypeSent = TMT_SERIAL_MESSAGE;

  switch (*currentMessageType)
  {
    case TMT_ZONE_MESSAGE:              // These are the types of messages that
    case TMT_CHECKIN_MESSAGE:           // have retries
    case TMT_SINGLE_KEY_PRESS_MESSAGE:
    case TMT_CARD_READ_MESSAGE:
      keypadRetries = MAX_KEYPAD_RETRIES;
      // Save the type so it can be retried later
      if (*currentMessageType == TMT_CHECKIN_MESSAGE)  // check-in messages are retried
        lastMessageTypeSent = TMT_ZONE_MESSAGE;        // as zone messages
      else
        lastMessageTypeSent = *currentMessageType;
      break;

    case TMT_RETRY_LAST_MESSAGE_TYPE_SENT:
      *currentMessageType = lastMessageTypeSent;
      if ((--keypadRetries == 0) &&
          ((TMT_SINGLE_KEY_PRESS_MESSAGE == *currentMessageType) || (TMT_CARD_READ_MESSAGE == *currentMessageType)))
      {
        printf("WARNING: 900Mhz Doorbell Event max tx retries. Cancelling event. Msg Type=%u\n", *currentMessageType);
        clearActiveDoorbellEvent();  // if last retry, clear the key press flag so the next one can be sent
      }
      break;

    case TMT_SERIAL_MESSAGE:
      keypadRetries = 0;  // lost transmitter number so don't worry about retries
      break;
  }
}

/*******************************************************************************
PUBLIC FUNCTION DEFINITIONS
*******************************************************************************/
/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
// DEBUG include frameStatus
ReceivedAckStatus processReceivedAck(char* characterOffset, int frameStatus)
{
  char bufferOffset = getWirelessReceiveBufferOffset();
  ReceivedAckStatus returnValue = RAS_NOTHING_TO_DO;
  ReceiverAck * const ackPointer = (ReceiverAck*)receiverPacketBuffer;

  if (bufferOffset >= sizeof(ReceiverAck))
  {
    switch (ackPointer->ackType)
    {
    case AT_NO_ACK:
      returnValue = RAS_GOOD_MESSAGE_NON_ACK;
      *characterOffset = sizeof(ReceiverNoAck);
      break;

    case AT_STANDARD_ACK:
      *characterOffset = sizeof(ReceiverStandardAck);
      if (bufferOffset >= *characterOffset)
      {
        if (decodeWirelessBuffer(receiverPacketBuffer, sizeof(ReceiverStandardAck), transmitterCrc))
          returnValue = RAS_GOOD_MESSAGE_ACK_RECEIVED;
        else
          returnValue = RAS_BAD_MESSAGE;
        printf("ack=%d, retVal=%d,frameStatus=%d\n", ackPointer->ackType, returnValue, frameStatus);
      }
      break;

    case AT_ACK_WITH_ZONE_NUMBER:
      *characterOffset = sizeof(ReceiverZoneNumberAck);
      if (bufferOffset >= *characterOffset)
      {
        ReceiverZoneNumberAck * const zoneAckPointer = (ReceiverZoneNumberAck*)receiverPacketBuffer;

        if (decodeWirelessBuffer(receiverPacketBuffer, sizeof(ReceiverZoneNumberAck), transmitterCrc) &&
            ((houseCode == INVALID_HOUSE_CODE) || (myZoneNumber == INVALID_TRANSMITTER_NUMBER)))
        {
          if (zoneAckPointer->found)
          {
            if (houseCode == INVALID_HOUSE_CODE)
              houseCode = zoneAckPointer->houseCode;

            myZoneFlags.transmitterFlags = zoneAckPointer->zoneFlags;
            myZoneNumber = (SlotTransmitterNumber)(zoneAckPointer->zoneLow + (zoneAckPointer->zoneHigh << 8));
            myCheckinSlotNumber = zoneToSlotNumber(myZoneNumber);
            printf("myZoneNumber=%d,myCheckinSlotNumber=%d\n", myZoneNumber, myCheckinSlotNumber);
            returnValue = RAS_GOOD_MESSAGE_ACK_RECEIVED;
          }
          else
          {
            returnValue = RAS_GOOD_MESSAGE_WRONG_RECEIVER;
          }
        }
        else
        {
          returnValue = RAS_BAD_MESSAGE;
        }
      }
      break;

    case AT_VARIABLE_LENGTH_ACK:
      if (bufferOffset >= sizeof(ReceiverAck) + 1)
      {
        ReceiverVariableLengthAck * const reservedAckPointer = (ReceiverVariableLengthAck*)receiverPacketBuffer;
        *characterOffset = sizeof(ReceiverVariableLengthAck) - 1 + reservedAckPointer->numBytesInData;
        if (bufferOffset >= *characterOffset)
        {
          returnValue = RAS_BAD_MESSAGE;  // we don't know what the ack is, so it's bad
        }
      }
      break;

    default:
      returnValue = RAS_BAD_MESSAGE;
      break;
    }
  }
  if( returnValue == RAS_GOOD_MESSAGE_ACK_RECEIVED )
  {         //we have received a good ack so count it
    if( ackStatus.counter <= LED_ACK_COUNT )
      ackStatus.counter++;

    keypadRetries = 0;  // got an ack so clear retries
    if (DOORBELL_EVENT_NONE != getActiveDoorbellEvent())
  	  clearActiveDoorbellEvent(); // only clears the current event in tx
  }

  return returnValue;
}

/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
TransmitterMessageStatus setupTransmitterMessage(char startOffset)
{
  char bufferOffset = getWirelessReceiveBufferOffset();
  ReceiverSlotStart* slotPointer;
  TransmitterMessageStatus status = TMS_NO_MESSAGE_SETUP;
  bool completeMessageReceived = false;
  WakeupReason wakeupReason = GetWakeupReason();
  SlotNumber slotNumber = INVALID_SLOT;
  TransmitterMessageType  messageTypeToSend = TMT_NO_MESSAGE;

  transmitterPacketSize = 0;

  if (bufferOffset >= sizeof(ReceiverNoMessageSlot) + startOffset)
  {
    slotPointer = ((ReceiverSlotStart*)(receiverPacketBuffer + startOffset));
    slotNumber = (SlotNumber)((slotPointer->slotNumberHigh << 8) | slotPointer->slotNumberLow);
    //printf("slotNumber=%u, type=%u\n", slotNumber, slotPointer->slotType);

    // if waking up in my checkin slot and the slot has a slottype that doesn't need a response
    // and we arn't waking for an alarm then go back to sleep
    if ((isAlarmSlotType(slotPointer->slotType) && (slotPointer->slotType != ST_GENERAL_ALARM)) &&
        (wakeupReason != WR_ALARM) &&
        ((slotNumber >= myCheckinSlotNumber) &&
         (slotNumber <= myCheckinSlotNumber + SLOTS_BETWEEN_CHECKIN_SLOTS)))
    {
      if (bufferOffset >= sizeof(ReceiverNoMessageSlot) + startOffset)
      {
        completeMessageReceived = true;
        if (decodeWirelessBuffer(receiverPacketBuffer, sizeof(ReceiverNoMessageSlot) + startOffset, INITIAL_CRC))
        {
          status = TMS_NO_MESSAGE_NEEDED;
#ifdef USE_SCANNED_INPUTS
          EnableDisarmDelay(slotPointer->slotType == ST_CHECKIN_NO_REPLY_SHUTDOWN_ALARM);
#endif
        }
      }
    }
    else if (isAlarmSlotType(slotPointer->slotType) )//if slottype is <=15
    {
      char size = sizeof(ReceiverNoMessageSlot) + startOffset;
      if (isTwoByteAlarmSlotType(slotPointer->slotType))
      {
        size += 2;
      }
      if (bufferOffset >= size)
      {
        completeMessageReceived = true;

        // setup zone number or serial number alarm message
        if (decodeWirelessBuffer(receiverPacketBuffer, size, INITIAL_CRC))
        {
          // if not assigned a zone number, send serial number, otherwise send zone number
          if (myZoneNumber == INVALID_TRANSMITTER_NUMBER)
          {
            messageTypeToSend = TMT_SERIAL_MESSAGE;
            status = TMS_MESSAGE_SETUP;
          }
          else if (keypadRetries > 0)                               // retry last message if neccessary
          {
            messageTypeToSend = TMT_RETRY_LAST_MESSAGE_TYPE_SENT;
          }
          else if ((wakeupReason == WR_ALARM) || (wakeupReason == WR_CHECKIN))
          {
            messageTypeToSend = TMT_ZONE_MESSAGE;
            status = TMS_MESSAGE_SETUP;
          }
          else if (DOORBELL_EVENT_PRESS == DRBL_INOTIFYIN_getDoorbellEvent()) // send a single key press
          {
            printf("900Mhz:STM: found push event\n");
            setActiveDoorbellEvent(DOORBELL_EVENT_PRESS);
            messageTypeToSend = TMT_SINGLE_KEY_PRESS_MESSAGE;
            status = TMS_MESSAGE_SETUP;
          }
          else if (DOORBELL_EVENT_PROX == DRBL_INOTIFYIN_getDoorbellEvent()) // send a card read
          {
            printf("900Mhz:STM: found prox event\n");
            setActiveDoorbellEvent(DOORBELL_EVENT_PROX);
            messageTypeToSend = TMT_CARD_READ_MESSAGE;
            status = TMS_MESSAGE_SETUP;
          }
          else
          {
            status = TMS_NO_MESSAGE_NEEDED;
          }
        }
      }
    }
    else if ((slotPointer->slotType == ST_CHECKIN_NEED_REPLY) //if slottype is 16
#ifdef USE_SCANNED_INPUTS
             || (slotPointer->slotType == ST_CHECKIN_NEED_REPLY_SHUTDOWN)
#endif
               )
    {
      if (bufferOffset >= sizeof(ReceiverNoMessageSlot) + startOffset)
      {
        completeMessageReceived = true;
        if (decodeWirelessBuffer(receiverPacketBuffer, sizeof(ReceiverNoMessageSlot) + startOffset, INITIAL_CRC))
        {
          // make sure we are in the right slot
          if ((slotNumber >= myCheckinSlotNumber) &&
              (slotNumber <= myCheckinSlotNumber + SLOTS_BETWEEN_CHECKIN_SLOTS))
          {
            printf("checkin message: slotNumber=%u\n", slotNumber);
            messageTypeToSend = TMT_CHECKIN_MESSAGE;
            status = TMS_MESSAGE_SETUP;
#ifdef USE_SCANNED_INPUTS
            EnableDisarmDelay(slotPointer->slotType == ST_CHECKIN_NEED_REPLY_SHUTDOWN);
#endif
          }
        }
      }
    }
          //if we are in our checkin slot and the slot type is called for us to update our programming
    else if( slotPointer->slotType == ST_CHECKIN_UPDATE_PROGRAMMING && (slotNumber >= myCheckinSlotNumber) &&
         (slotNumber <= myCheckinSlotNumber + SLOTS_BETWEEN_CHECKIN_SLOTS) )
    {
      messageTypeToSend = TMT_SERIAL_MESSAGE;
      myZoneNumber = INVALID_TRANSMITTER_NUMBER;
    }
    else if (slotPointer->slotType == ST_KEYPAD_POLL)
    {
      if (bufferOffset >= sizeof(KeypadDisplaySlot) + startOffset)
      {
//        completeMessageReceived = true;
        if (decodeWirelessBuffer(receiverPacketBuffer, sizeof(KeypadDisplaySlot) + startOffset, INITIAL_CRC))
        {
          completeMessageReceived = true;
          KeypadDisplaySlot * dataPointer = (KeypadDisplaySlot*)(receiverPacketBuffer + startOffset);
          SlotTransmitterNumber zone = (SlotTransmitterNumber)((dataPointer->zoneNumberLowByte) | (dataPointer->zoneNumberHighBits << 8));

          printf("keypad display:zone=%u,myZone=%u,pos=%u,str=%s,slot=%u\n",
                  zone,
                  myZoneNumber,
                  dataPointer->countPosition,
                  &dataPointer->display[0],
                  slotNumber);

          if ((zone == myZoneNumber) && (myZoneNumber != INVALID_TRANSMITTER_NUMBER))
          {
            messageTypeToSend = TMT_COMMAND_ACK_WITH_ZONE_MESSAGE;
            status = TMS_LONG_MESSAGE_SETUP;
          }
          printf("keypad_display: status=%u,msgType=%u\n", status, messageTypeToSend);
        }
      }
    }
    else if (slotPointer->slotType == ST_KEYPAD_STATUS)
    {
      if (bufferOffset >= sizeof(KeypadStatusSlot) + startOffset)
      {
//        completeMessageReceived = true;
        if (decodeWirelessBuffer(receiverPacketBuffer, sizeof(KeypadStatusSlot) + startOffset, INITIAL_CRC))
        {
          completeMessageReceived = true;
          KeypadStatusSlot * dataPointer = (KeypadStatusSlot*)(receiverPacketBuffer + startOffset);
          SlotTransmitterNumber zone = (SlotTransmitterNumber)((dataPointer->zoneNumberLowByte) | (dataPointer->zoneNumberHighBits << 8));

          if ((zone == myZoneNumber) && (myZoneNumber != INVALID_TRANSMITTER_NUMBER))
          {
            KeypadStatusWord keypadStatus;
            keypadStatus.asWord = dataPointer->statusWord;
//            printf("keypad_status:=0x%04x;rdBkL=%01x,spr1=%01x,stCad=%01x,stClr=%01x,buz=%01x,dr=%01x,spr2=%01x,aud=%01x\n"
//                  ,keypadStatus.asWord
//                  ,keypadStatus.redBacklight
//                  ,keypadStatus.spare1
//                  ,keypadStatus.statusCadence
//                  ,keypadStatus.statusColor
//                  ,keypadStatus.buzzer
//                  ,keypadStatus.doorStrike
//                  ,keypadStatus.spare2
//                  ,keypadStatus.audibles
//                  );
            ProcessKeypadStatus(keypadStatus);

            messageTypeToSend = TMT_COMMAND_ACK_WITH_ZONE_MESSAGE;
            status = TMS_MESSAGE_SETUP;
          }
        }
      }
    }
    else
    {
      // we don't know what this message is, so say we received it, status alone
      completeMessageReceived = true;
    }

    HandleRetries(&messageTypeToSend);
    formatTransmitterMessage(messageTypeToSend);
  }

  if (completeMessageReceived == false)
  {
    status = TMS_STILL_WAITING;
  }
  else
  {
    currentSlotNumber = slotNumber;
  }

  if ((status != TMS_STILL_WAITING) && (status != TMS_NO_MESSAGE_NEEDED))
    printf("setup status:%u, msg to send=%u\n",status, messageTypeToSend);

  return status;
}

/*******************************************************************************

DESCRIPTION:

NOTES:  If SLEEP_TICKS_PER_HOP is no longer a power of 2, this will take up much
more room

*******************************************************************************/
short getSleepTicks(void)
{
  SlotNumber slotDifference = (SlotNumber)(myCheckinSlotNumber - currentSlotNumber);
  if (slotDifference <= 0)
  {
    slotDifference += TOTAL_SLOTS;
  }
  return slotDifference * SLEEP_TICKS_PER_HOP;
}

/*F****************************************************************************
*
* Name: GetSleepTicksUntilKeypadSlot()
* Description:  Calculate the number of ticks until the next keypad slot.
* Parameters:   None
* Return value: Number of ticks
*
*****************************************************************************F*/
short GetSleepTicksUntilKeypadSlot(void)
{
  SlotNumber slotDifference = (SlotNumber)(STARTING_KEYPAD_SLOT - (currentSlotNumber % SLOTS_PER_GROUP));
  if (slotDifference <= 0)
  {
    slotDifference += SLOTS_PER_GROUP;
  }
  return slotDifference * SLEEP_TICKS_PER_HOP;
}

/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
void resetLearnedInformation(void)
{
  myZoneNumber = INVALID_TRANSMITTER_NUMBER;
  houseCode = INVALID_HOUSE_CODE;
  currentSlotNumber = INVALID_SLOT;
  myCheckinSlotNumber = INVALID_SLOT;
}

#ifdef USE_EXTENDED_ZONE_STATE
/*******************************************************************************

DESCRIPTION: Function will format the transmitterPacketBuffer according to the passed message type

NOTES:

*******************************************************************************/
void formatTransmitterMessage(TransmitterMessageType messageType)
{
  switch (messageType)
  {
    case (TMT_SERIAL_MESSAGE):
    {
      TransmitterAlarmSerialNumExtendedMsg* const sendPointer =
        (TransmitterAlarmSerialNumExtendedMsg*)transmitterPacketBuffer;

      sendPointer->messageType = TAT_ALARM_WITH_EXTENDED_SERIAL_NUMBER;
      sendPointer->reserved = 0;
      sendPointer->serialNumberLow = serialNumber & 0xFF;
      sendPointer->serialNumberMiddle = (serialNumber >> 8) & 0xFF;
      sendPointer->serialNumberHigh = (serialNumber >> 16) & 0xFF;
      sendPointer->softwareVersion = SOFTWARE_VERSION;
      sendPointer->zoneMessage = getInputState();
      finalizeTransmitterMessage(sizeof(TransmitterAlarmSerialNumExtendedMsg), ACI_NOTHING);
      break;
    }

    case (TMT_ZONE_MESSAGE):    // Fall through
    case (TMT_CHECKIN_MESSAGE): // Checkin messages are sent as zone messages
    {
      TransmitterAlarmExtendedZoneMessage* const sendPointer =
        (TransmitterAlarmExtendedZoneMessage*)transmitterPacketBuffer;

      sendPointer->messageType = TAT_ALARM_WITH_EXTENDED_ZONE_NUMBER;
      sendPointer->zoneNumberHigh = (myZoneNumber >> 8) & 0xFF;
      sendPointer->zoneNumberLow = myZoneNumber & 0xFF;
      sendPointer->zoneMessage = getInputState();
      finalizeTransmitterMessage(sizeof(TransmitterAlarmExtendedZoneMessage),
          ACI_HOUSE_CODE | ACI_SERIAL_NUMBER);
      break;
    }

    case TMT_COMMAND_ACK_WITH_ZONE_MESSAGE:
    {
      CommandDataAckWithZoneMessage* const sendPointer =
          (CommandDataAckWithZoneMessage*)transmitterPacketBuffer;

      sendPointer->zoneNumberHigh = (myZoneNumber >> 8) & 0xFF;
      sendPointer->zoneNumberLow = myZoneNumber & 0xFF;
      sendPointer->reserved = 0;
      sendPointer->forwarded = false;
      sendPointer->zoneMessage = (ZoneMessageByte)getInputState();
      finalizeTransmitterMessage(sizeof(CommandDataAckWithZoneMessage),
          ACI_HOUSE_CODE | ACI_SERIAL_NUMBER);
      break;
    }

    default:
    {
      break;
    }
  }
}

#else

/*******************************************************************************

DESCRIPTION: Function will format the transmitterPacketBuffer according to the passed message type

NOTES:

*******************************************************************************/
void formatTransmitterMessage(TransmitterMessageType messageType)
{
#if defined(__linux__)
//  printf("Serial Number:num=%lu, add=%lu, addConst=%lu, addPtr=%lu, *ptr=%lu\n",
//        serial_number,
//        &serial_number,
//        SERIAL_NUMBER_ADDRESS,
//        serialNumberPtr,
//        serialNumber);
//printf("Msg type=%d,", messageType);
#endif

  switch (messageType)
  {
    case (TMT_SERIAL_MESSAGE):
    {
      TransmitterAlarmSerialNumberZoneMessage* const sendPointer =
        (TransmitterAlarmSerialNumberZoneMessage*)transmitterPacketBuffer;

      sendPointer->messageType = TAT_ALARM_WITH_SERIAL_NUMBER;
      sendPointer->reserved = 0;
      sendPointer->serialNumberLow = serialNumber & 0xFF;
      sendPointer->serialNumberMiddle = (serialNumber >> 8) & 0xFF;
      sendPointer->serialNumberHigh = (serialNumber >> 16) & 0xFF;
      sendPointer->softwareVersion = SOFTWARE_VERSION;
      sendPointer->zoneMessage = (ZoneMessageByte)getInputState();
#if defined(__linux__)
      printf("zone=%d\n", sendPointer->zoneMessage);
#endif
      finalizeTransmitterMessage(sizeof(TransmitterAlarmSerialNumberZoneMessage), ACI_NOTHING);
      break;
    }
    case (TMT_ZONE_MESSAGE):
    {
      TransmitterAlarmZoneNumberZoneMessage* const sendPointer =
        (TransmitterAlarmZoneNumberZoneMessage*)transmitterPacketBuffer;

      sendPointer->messageType = TAT_ALARM_WITH_ZONE_NUMBER;
      sendPointer->zoneNumberHigh = (myZoneNumber >> 8) & 0xFF;
      sendPointer->zoneNumberLow = myZoneNumber & 0xFF;
      sendPointer->zoneMessage = (ZoneMessageByte)getInputState();
#if defined(__linux__)
      printf("zone=%d,zone number=%u;%u|%u\n",
              sendPointer->zoneMessage,
              myZoneNumber,
              sendPointer->zoneNumberHigh,
              sendPointer->zoneNumberLow);
#endif
      finalizeTransmitterMessage(sizeof(TransmitterAlarmZoneNumberZoneMessage),
          ACI_HOUSE_CODE | ACI_SERIAL_NUMBER);
      break;
    }
    case (TMT_CHECKIN_MESSAGE):
    {
      TransmitterCheckinMessage* const sendPointer =
          (TransmitterCheckinMessage*)transmitterPacketBuffer;

      sendPointer->zoneMessage = (ZoneMessageByte)getInputState();
#if defined(__linux__)
      printf("zone=%d\n", sendPointer->zoneMessage);
#endif
      finalizeTransmitterMessage(sizeof(TransmitterCheckinMessage),
          ACI_HOUSE_CODE | ACI_ZONE_NUMBER | ACI_SERIAL_NUMBER);
      break;
    }
    case TMT_COMMAND_ACK_WITH_ZONE_MESSAGE:
    {
      CommandDataAckWithZoneMessage* const sendPointer =
          (CommandDataAckWithZoneMessage*)transmitterPacketBuffer;

      sendPointer->zoneNumberHigh = (myZoneNumber >> 8) & 0xFF;
      sendPointer->zoneNumberLow = myZoneNumber & 0xFF;
      sendPointer->reserved = 0;
      sendPointer->forwarded = false;
      sendPointer->zoneMessage = (ZoneMessageByte)getInputState();
      finalizeTransmitterMessage(sizeof(CommandDataAckWithZoneMessage),
          ACI_HOUSE_CODE | ACI_SERIAL_NUMBER);
      break;
    }
    case (TMT_SINGLE_KEY_PRESS_MESSAGE):
    {
      TransmitterKeypadSingleKeyPress* const sendPointer = (TransmitterKeypadSingleKeyPress*)transmitterPacketBuffer;

      sendPointer->messageType = TAT_KEYPAD_SINGLE_KEY_PRESS;
      sendPointer->forwarded = false;
      sendPointer->transmitterHigh = (myZoneNumber >> 8) & 0xFF;
      sendPointer->transmitterLow = myZoneNumber & 0xFF;
      sendPointer->keyByte = DOORBELL_PRESS_MAPPED_KEYPRESS_VALUE;
      sendPointer->keyByteFields.pressed = PRESSED_SHORT;

      sendPointer->zoneMessage = (ZoneMessageByte)getInputState();
      finalizeTransmitterMessage(sizeof(TransmitterKeypadSingleKeyPress), ACI_HOUSE_CODE | ACI_SERIAL_NUMBER);
      break;
    }

    case (TMT_CARD_READ_MESSAGE):
    {
      TransmitterKeypadCardRead* const sendPointer = (TransmitterKeypadCardRead*)transmitterPacketBuffer;

      sendPointer->messageType = TAT_KEYPAD_CARD_READ;
      sendPointer->forwarded = false;
      sendPointer->transmitterHigh = (myZoneNumber >> 8) & 0xFF;
      sendPointer->transmitterLow = myZoneNumber & 0xFF;
      if (keypadRetries == MAX_KEYPAD_RETRIES)  // first try to send card read
      {
        PVC_convertToCardReadMessageFormat(
                        DRBL_INOTIFIYIN_getProxValue(),
                        sendPointer->cardRead,
						KEYPAD_CARD_READ_MESSAGE_LENGTH,
                        KEYPAD_CARD_READ_NUMBER_OF_DIGITS);
        // archive for retry
        memcpy(savedCardRead, sendPointer->cardRead, KEYPAD_CARD_READ_MESSAGE_LENGTH);
      }
      else  // retry sending card read
      {
        memcpy(sendPointer->cardRead, savedCardRead, KEYPAD_CARD_READ_MESSAGE_LENGTH);
      }
      finalizeTransmitterMessage(sizeof(TransmitterKeypadCardRead), ACI_HOUSE_CODE | ACI_SERIAL_NUMBER);
      break;
    }
    default:
    {
      break;
    }
  }

}
#endif

