#if defined(__linux__)

#include "BitMacros.h"
#include "gpio_linux.h"

unsigned int getIO(unsigned int gpioNum)
{
  if (0 == gpioNum)
    return 0;

  unsigned int data;
  gpio_get_value(gpioNum, &data);
  return data;
}

#endif