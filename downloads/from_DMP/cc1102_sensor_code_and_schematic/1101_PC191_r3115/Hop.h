/*******************************************************************************
FILENAME:

DESCRIPTION:

NOTES:

Copyright (c) Digital Monitoring Products Inc. 2004.  All rights reserved.

*******************************************************************************/
#ifndef HOP_H
#define HOP_H


/*----compilation control-----------------------------------------------------*/

/*******************************************************************************
INCLUDE FILES
*******************************************************************************/
/*----system and platform files-----------------------------------------------*/

/*----program files-----------------------------------------------------------*/
#include "types.h"

/*******************************************************************************
FILE CONTEXT
*******************************************************************************/
/*----defines, typedefs, structs, unions, enums-------------------------------*/
enum {NUM_HOP_CHANNELS = 53};

#define INVALID_HOUSE_CODE  0

/*----data descriptions-------------------------------------------------------*/

/*----data declarations-------------------------------------------------------*/
extern byte houseCode;
extern char currentTransmitterChannel;
extern char currentReceiverChannel;
extern char currentChannel;

/*----macros------------------------------------------------------------------*/

/*----function prototypes-----------------------------------------------------*/
extern void SetFrequency(byte Channel);
extern byte addChannel(byte channel, byte channelAdd);
extern byte NextChannel(byte Channel);
extern byte SubtractHops(byte numHops, byte channel);
extern byte AddHops(byte numHops, byte channel);

#endif                                  /* end of file */


