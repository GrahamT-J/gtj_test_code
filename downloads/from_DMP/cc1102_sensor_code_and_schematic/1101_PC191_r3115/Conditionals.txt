COMPILER DEFINES
  _lint                         Specifies lint is the executing compiler
  __ICC430__                    Specifies the current Compiler is and IAR compiler	
  __STDC__                      Specifies the current compiler is a "C" compiler

PROJECT DEFINES
  PROJECT_1101                  Specifies the current project is the 1101 project
  PROJECT_1100X                 Specifies the current project is the 1100X project
  PROJECT_1100D                 Specifies the current project is the 1100D project

TRANSMITTER DEFINES
  DMP_1160_TRANSMITTER          Specifies the current 1101 variant as an 1160 smoke
  DMP_1103_TRANSMITTER          Specifies the current 1101 variant as an 1103 Commercial zone
  DMP_1142_TRANSMITTER          Specifies the current 1101 variant as a 1142 two button panic
  DMP_1101_TRANSMITTER          Specifies the current 1101 variant as a true 1101

DEBUG DEFINES
  DMP_BUILDING                  Used to compile the special test code for the DMP building test
                                  to talk with transmitters in building in 01500000 - 01599999 range
  DMP_1100_PRODUCTION_TEST      Used to compile the special test code for the 1100 production test
  DMP_PRODUCTION_TEST           Used to compile the special test code for the 1101 production test
  FCC_SPECIAL_VERSION           Used to compile a special version of firmware for FCC testing
  USE_DEBUG_PORT                Used to enable a special debug code were P5 is used to output diag information
  SINGLE_CHANNEL                Used to disable hopping so the channel used by the wireless device is fixed
  OVERRIDE_DEBUG_BAUDRATE       When defined the debug port is 38400bps instead of 4800pbs
  DMP_RSSI_DEBUG                Used to enable the RSSI measurements to be sent out the "prog" serial port
  Temp_Test                     ??
  DMP_WIRELESS_SNIFFER          If defined, will forward all RX and TX data to the "PROG" (USART1) port

HARDWARE DEINFES
  USING_RECEIVER_HARDWARE       Used to reconfigure the 1101 to execute on extensivly modified 1100 hardware
  USING_2131_HARDWARE           Used to configure the processor variant to a MSP430F2131
  USING_1232_HARDWARE           Used to configure the processor variant to a MSP430F1232
  USING_CHIPCON_RF_CHIP         Used to configure the firmware to interface to a chipcon CCC1100.  Also requires Xemics.c to be
                                  replaced by chipcon.c and XemicsCommunication.c or Communication.c to be replaced by
                                  ChipconCommunications.c

FEATURE DEFINES
  USE_PANIC_PULSING             Enables the RED_LED to be flashed at 1/2s on 3s off when a "ACK" is received
  ENABLE_RECEIVER_COMMANDS      Enables the transmitter to wake for each General Data Slot to receive Receiver Commands
  ENABLE_COMMAND_CANCEL         Used to enable the transmitter to cancel the current request for general data.  Used to give the
                                  transmitter to ability to listen for a data slot when an event occures.
  USING_ACK_LED                 Used to enable the onboard LED to flash for ACK confirmation
  USE_INDICATOR_AS_OUTPUT       Changes the "output" device from the onboard relay to the indicator lamp on the 1114 board
  USE_CONDITIONAL_POWER_OUTPUTS Enabes the steady output function only when external power is applied
  ENABLE_ADC_ZONE_SCAN          Used the ADC10 module to scan zones on the 1114
  USE_ONLY_PANIC_OUTPUTS        Only allows PANIC_ALARM and PANIC_TEST cadances
  USE_SCANNED_INPUTS            Zones are scanned rather than interrupt driven