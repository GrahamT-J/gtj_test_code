/*H*****************************************************************************
Filename: hop.c

Description: Calculations for channel hopping and scrambling

Copyright (c) Digital Monitoring Products Inc. 2007 - 2017. All rights reserved.
*****************************************************************************H*/

#include "Types.h"
#include "Hop.h"
#include "WirelessChip.h"

// note, this may need modification for values of NUM_HOP_CHANNELS other than 61 and 53
#define ChannelScramble(A) (((A) ^ ((A) * 8)) & 0x3F)

byte houseCode = INVALID_HOUSE_CODE;
char currentTransmitterChannel = 0;
char currentReceiverChannel = 0;
char currentChannel = 0;

/*F****************************************************************************
*
* Name: SetFrequency()
* Description:  Used to scramble the channel order and set the Wireless chip to
*               the new channel
* Parameters:   - unscrambled channel number
* Return value: None
*
*****************************************************************************F*/
void SetFrequency(byte Channel)
{
  currentChannel = Channel;

  Channel = ChannelScramble(Channel);
  // if too big, do it again
  if (Channel >= NUM_HOP_CHANNELS) Channel = ChannelScramble(Channel);

  sendChannel(Channel);
}

/*F****************************************************************************
*
* Name: addChannel()
* Description:  Adds a channel offset to a channel and returns the result
*               after accounting for wrap around.
* Parameters:   - beginning channel number
*               - number of channels to add
* Return value: resulting channel number
*
*****************************************************************************F*/
byte addChannel(byte channel, byte channelAdd)
{
  char newChannel = channel + channelAdd;
  while (newChannel >= NUM_HOP_CHANNELS) newChannel -= NUM_HOP_CHANNELS;
  return newChannel;
}

/*F****************************************************************************
*
* Name: SubtractChannel()
* Description:  Subtracts a channel from another channel, returns the result
*               after accounting for wrap around.
* Parameters:   - beginning channel number
*               - number of channels to subtract
* Return value: resulting channel number
*
*****************************************************************************F*/
byte SubtractChannel(byte channel, byte channelSub)
{
  while (channel < channelSub)
    channel += NUM_HOP_CHANNELS;

  return (channel - channelSub);
}

/*F****************************************************************************
*
* Name: NextChannel()
* Description:  Calculates the next chanel in the hop sequence, based on the
*               house code.
* Parameters:   - beginning channel number
* Return value: resulting channel number
*
*****************************************************************************F*/
byte NextChannel(byte channel)
{
  char incVal = houseCode;

  if (incVal == 0)
  {
    incVal = 1;
  }
  return addChannel(channel, incVal);
}

/*F****************************************************************************
*
* Name: AddHops()
* Description:  Function will add channel to numHops and return the result.
* Parameters:   - number of hops to add
*               - beginning channel number
* Return value: resulting channel number
*
*****************************************************************************F*/
byte AddHops(byte numHops, byte channel)
{
  if (houseCode == 0)
    channel = addChannel(channel, numHops);
  else
    channel = addChannel(channel, (numHops * houseCode));

  return (channel);
}

/*F****************************************************************************
*
* Name: SubtractHops()
* Description:  Function will subtract a number of hops from the channel number
*               passed and will return the result.
* Parameters:   - number of hops to subtract
*               - beginning channel number
* Return value: resulting channel number
*
*****************************************************************************F*/
byte SubtractHops(byte numHops, byte channel)
{
  if(houseCode == 0)
    channel = SubtractChannel(channel, numHops);
  else
    channel = SubtractChannel(channel, (numHops * houseCode));

  return (channel);
}
