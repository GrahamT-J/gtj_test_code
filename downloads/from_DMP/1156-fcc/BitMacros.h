/*H*****************************************************************************
Filename: bitmacros.h

Description: Macros for manipulation status bits without worrying about the
  byte they are in

Copyright (c) Digital Monitoring Products Inc. 2008 - 2017. All rights reserved.
*****************************************************************************H*/

#ifndef BITMACROS_H
#define BITMACROS_H

#define moveImmediateWord(A,B) (B)=(A)
#define getFlag(A)          (A##_##BYTE & A##_MASK)
#define setFlag(A)          A##_##BYTE |= A##_MASK
#define toggleFlag(A)       A##_##BYTE ^= A##_MASK
#define clearFlag(A)        A##_##BYTE &= ~A##_MASK
#define getIO(A)            (A##_##IN & A##_MASK)
#define getIOOut(A)         (A##_##OUT & A##_MASK)
#define setIO(A)            A##_##OUT |= A##_MASK
#define toggleIO(A)         A##_##OUT ^= A##_MASK
#define clearIO(A)          A##_##OUT &= ~A##_MASK
#define setIODirOut(A)      A##_##DIR |= A##_MASK
#define setIODirIn(A)       A##_##DIR &= ~A##_MASK
#define setIOIntEdge(A)     (A##_##IES & A##_MASK)
#define setIOFallingInt(A)  A##_##IES |= A##_MASK
#define setIORisingInt(A)   A##_##IES &= ~A##_MASK
#define setIOEnableInt(A)   A##_##IE |= A##_MASK
#define clearIOEnableInt(A) A##_##IE &= ~A##_MASK
#define getIOIntFlag(A)     A##_##IFG & A##_MASK
#define clearIOIntFlag(A)   A##_##IFG &= ~A##_MASK
#define setIOSel(A)         A##_##SEL |= A##_MASK
#define clearIOSel(A)       A##_##SEL &= ~A##_MASK
#define enableIOResistor(A) A##_##REN |= A##_MASK
#define disableIOResistor(A) A##_##REN &= ~A##_MASK

#endif
