/*H*****************************************************************************
Filename: packet.c

Description: Info relating to the data packet transferred from transmitter to
  receiver and back

Copyright (c) Digital Monitoring Products Inc. 2007 - 2017. All rights reserved.
*****************************************************************************H*/

#include "Packet.h"

const char receiverHeaderBuffer[PR_HEADER_SIZE] =
{
  0xAA,
  0xAA,
  0xAA,
  0xAA,
  RPAT_HIGH,
  RPAT_MIDDLE,
  RPAT_LOW
};

const char transmitterHeaderBuffer[PT_HEADER_SIZE] =
{
  0xAA,
  0xAA,
  0xAA,
  0xAA,
  TPAT_HIGH,
  TPAT_MIDDLE,
  TPAT_LOW
};

char receiverPacketBuffer[PR_SIZE];
char transmitterPacketBuffer[PT_SIZE];
char receiverPacketSize;
char transmitterPacketSize = 10; // todo: set correctly

/*F****************************************************************************
*
* Name: calcWirelessCRC()
* Description:  Calculates CRC based on a word buffer containing a fixed number
*               of characters 
* Parameters:   - pointer to data buffer
*               - number of bytes
*               - initial CRC value
* Return value: resulting CRC value
*
*****************************************************************************F*/
static word calcWirelessCRC(char *bufferPtr, char length, unsigned short crc)
{
  char i;

  for (i = 0; i < length; ++i)
  {
    calcCRC16ForChar(*bufferPtr, &crc);
    bufferPtr++;
  }
  return (crc);
}

/*F****************************************************************************
*
* Name: calcCRC16ForChar()
* Description:  Incorporates one byte into a CRC-16 checksum, based on CRC-16
*               (X^16 + X^15 + X^2 + 1)
* Parameters:   - next character to checksum
*               - initial CRC value
* Return value: resulting CRC value
*
*****************************************************************************F*/
void calcCRC16ForChar(char value, unsigned short* crc)
{
  char i;
  unsigned short tempCrc = *crc ^ value;

  for (i = 0; i < 8; ++i)
  {
    if (tempCrc & 0x01)
    {
      (tempCrc) >>= 1;
      (tempCrc) ^= 0xa001;
    }
    else
    {
      (tempCrc) >>= 1;
    }
  }
  *crc = tempCrc;
}

/*F****************************************************************************
*
* Name: encodeWirelessBuffer()
* Description:  Append the CRC to a message.
* Parameters:   - pointer to data buffer
*               - number of bytes, not including CRC
*               - initial CRC value
* Return value: None
*
*****************************************************************************F*/
void encodeWirelessBuffer(char *buffer, byte length, unsigned short crc)
{
  // add CRC to end
  unsigned short crcResult = calcWirelessCRC(buffer, length - 2, crc);
  buffer[length - 2] = crcResult >> 8;
  buffer[length - 1] = (char)crcResult;
}

/*F****************************************************************************
*
* Name: decodeWirelessBuffer()
* Description:  Verify the CRC of a message.
* Parameters:   - pointer to data buffer
*               - number of bytes, including CRC
*               - initial CRC value
* Return value: true if checksum is correct
*
*****************************************************************************F*/
bool decodeWirelessBuffer(char *buffer, byte length, unsigned short crc)
{
  // check CRC and return true if correct
  return calcWirelessCRC(buffer, length - 2, crc) == buffer[length - 1] + (buffer[length - 2] << 8);
}
