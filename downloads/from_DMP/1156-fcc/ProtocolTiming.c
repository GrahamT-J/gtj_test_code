/*H*****************************************************************************
Filename: protocoltiming.c

Description: Values and calculations related to slot timing

Copyright (c) Digital Monitoring Products Inc. 2007 - 2017. All rights reserved.
*****************************************************************************H*/

#include "types.h"
#include "ProtocolTiming.h"

/*F****************************************************************************
*
* Name: nextSlot()
* Description:  Return the next slot number, accounting for wrap around.
* Parameters:   - current slot number
* Return value: next slot number
*
*****************************************************************************F*/
SlotNumber nextSlot(SlotNumber slot)
{
  if (slot >= MAX_SLOT_NUMBER)
  {
    return (SlotNumber)0;
  }
  else
  {
    return (SlotNumber)(slot + 1);
  }
}

/*F****************************************************************************
*
* Name: AddSlots()
* Description:  Function will add the two slot numbers together and return the
*               result.
* Parameters:   - beginning slot number
*               - number of slots to add
* Return value: resulting slot number
*
*****************************************************************************F*/
SlotNumber AddSlots(SlotNumber slot, SlotNumber numToAdd)
{
  SlotNumber newSlot = slot + numToAdd;
  while (newSlot >= TOTAL_SLOTS) newSlot -= TOTAL_SLOTS;
  return newSlot;
}

/*F****************************************************************************
*
* Name: subtractSlots()
* Description:  Subtract two slot numbers, accounting for wrap around.
* Parameters:   - beginning slot number
*               - number of slots to subtract
* Return value: resulting slot number
*
*****************************************************************************F*/
SlotNumber subtractSlots(SlotNumber slot1, SlotNumber slot2)
{
  if (slot1 < slot2)
  {
    slot1 += TOTAL_SLOTS;
  }
  return (SlotNumber)(slot1 - slot2);
}

/*F****************************************************************************
*
* Name: zoneToSlotNumber()
* Description:  Convert a zone number to slot number.
* Parameters:   - zone number
* Return value: slot number
*
*****************************************************************************F*/
SlotNumber zoneToSlotNumber(SlotTransmitterNumber zone)
{
  SlotTransmitterNumber zoneCalc = zone;
  SlotNumber slotCalc = (SlotNumber)0;

  while (zoneCalc >= TOTAL_GROUPS)
  {
    slotCalc += (SLOTS_BETWEEN_CHECKIN_SLOTS + 1);
    zoneCalc -= TOTAL_GROUPS;
  }
  slotCalc += zoneCalc * SLOTS_PER_GROUP;

  return slotCalc;
}

/*F****************************************************************************
*
* Name: slotToZoneNumber()
* Description:  Convert a slot number to zone number.
* Parameters:   - slot number
* Return value: zone number
*
*****************************************************************************F*/
SlotTransmitterNumber slotToZoneNumber(SlotNumber slot)
{
  char slotGroup = slot / SLOTS_PER_GROUP;
  char groupOffset = slot % SLOTS_PER_GROUP;
  char zoneOffset = groupOffset / (SLOTS_BETWEEN_CHECKIN_SLOTS + 1);
  SlotTransmitterNumber zone = INVALID_TRANSMITTER_NUMBER;

  if (((groupOffset % (SLOTS_BETWEEN_CHECKIN_SLOTS + 1)) == 0) &&
      (zoneOffset < CHECKIN_SLOTS_PER_GROUP))
  {
    zone = (SlotTransmitterNumber)(slotGroup + (zoneOffset * TOTAL_GROUPS));  // NOTE: this is a non-power of 2 multiply
  }

  return zone;
}
