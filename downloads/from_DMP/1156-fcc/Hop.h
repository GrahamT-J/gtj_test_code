/*H*****************************************************************************
Filename: hop.h

Description: Calculations for channel hopping and scrambling

Copyright (c) Digital Monitoring Products Inc. 2004 - 2017. All rights reserved.
*****************************************************************************H*/

#ifndef HOP_H
#define HOP_H

#include "types.h"

/*----defines, typedefs, structs, unions, enums-------------------------------*/
enum {NUM_HOP_CHANNELS = 53};

#define INVALID_HOUSE_CODE  0

/*----data declarations-------------------------------------------------------*/
extern byte houseCode;
extern char currentTransmitterChannel;
extern char currentReceiverChannel;
extern char currentChannel;

/*----function prototypes-----------------------------------------------------*/
extern void SetFrequency(byte Channel);
extern byte addChannel(byte channel, byte channelAdd);
extern byte NextChannel(byte Channel);
extern byte SubtractHops(byte numHops, byte channel);
extern byte AddHops(byte numHops, byte channel);

#endif
