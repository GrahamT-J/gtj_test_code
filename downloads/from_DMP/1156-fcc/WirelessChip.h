/*H*****************************************************************************
Filename: wirelesschip.h

Description: Generic file that includes the appropriate header for the wireless
             chip (Chipcon or Xemics)

Copyright (c) Digital Monitoring Products Inc. 2009 - 2017. All rights reserved.
*****************************************************************************H*/

#ifndef WIRELESSCHIP_H
#define WIRELESSCHIP_H

#include "chipcon.h"

#endif
