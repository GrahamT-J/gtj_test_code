/*H*****************************************************************************
Filename: sleep.h

Description: Go-to-sleep routine and wakeup scheduling

Copyright (c) Digital Monitoring Products Inc. 2004 - 2017. All rights reserved.
*****************************************************************************H*/

#ifndef SLEEP_H
#define SLEEP_H

#include "types.h"

#include "DMP.h"
#include "Hardware.h"
#include "ProtocolTiming.h"
#include "Hop.h"

/*----defines, typedefs, structs, unions, enums-------------------------------*/
enum
{
  SLEEP_TICKS_PER_SECOND = (int)((SLEEP_MASTER_CLOCK / SLEEP_CLOCK_DIVIDER) / SLEEP_TIMERA_DIVIDER),
  SLEEP_TICKS_PER_HOP = SLEEP_TICKS_PER_SECOND / HOPS_PER_SECOND,
  SLEEP_TICKS_PER_HOP_ROUND = SLEEP_TICKS_PER_HOP * NUM_HOP_CHANNELS,
  WAKEUP_SECONDS = 60,
  WAKEUP_RATE = ((unsigned short)(((unsigned long)WAKEUP_SECONDS * HOPS_PER_SECOND) * SLEEP_TICKS_PER_HOP))
};

enum
{
  CLOCK_TOLERANCE_PPM = 100,  // crystal tolerance of transmitter and receiver
  SLEEP_TICK_TOLERANCE = (int)(((double)CLOCK_TOLERANCE_PPM / 1000000) * SLEEP_TICKS_PER_SECOND * WAKEUP_SECONDS),
  WAKEUP_CLOCK_SLOP_TICKS = 2 * SLEEP_TICK_TOLERANCE // multiply by two in case receiver and transmitter at opposite ends
};

typedef enum
{
  TS_SUCCESS,
  TS_FAILURE,
  TS_RETRY
} TransmitStatus;

/*----data descriptions-------------------------------------------------------*/
extern word sleepTime;

/*----function prototypes-----------------------------------------------------*/
extern void goToSleep(TransmitStatus status);
extern bool wakeup(bool isAlarmWakeup, uint8 transmitterIndex);
extern bool getSleeping(void);
extern bool getCurrentlyCommunicating(void);
extern bool getAlarmWakeup(void);
extern bool receiverFailed(void);

#endif
