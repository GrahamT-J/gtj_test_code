/*H*****************************************************************************
Filename: main.h

Description: Main transmitter control logic

Copyright (c) Digital Monitoring Products Inc. 2004 - 2017. All rights reserved.
*****************************************************************************H*/

#ifndef MAIN_H
#define MAIN_H

/*----function prototypes-----------------------------------------------------*/
extern void decrementFrameTimeout(void);
extern void processMainShutdown(void);
extern void processMainWakeup(bool hardSleep);
extern void resetHardware(bool hardReset);

#endif
