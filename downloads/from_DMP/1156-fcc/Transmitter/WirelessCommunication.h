/*H*****************************************************************************
Filename: wirelesscommunication.h

Description: Generic file that includes the appropriate header for the high
             level radio communication (Chipcon or Xemics)

Copyright (c) Digital Monitoring Products Inc. 2009 - 2017. All rights reserved.
*****************************************************************************H*/

#ifndef WIRELESSCOMMUNICATION_H
#define WIRELESSCOMMUNICATION_H

#include "ChipconCommunication.h"

#endif
