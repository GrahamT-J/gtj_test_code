/*H*****************************************************************************
Filename: transmitterprotocol.h

Description: High level processing for the 1100 wireless protocol

Copyright (c) Digital Monitoring Products Inc. 2008 - 2017. All rights reserved.
*****************************************************************************H*/

#ifndef TRANSMITTERPROTOCOL_H
#define TRANSMITTERPROTOCOL_H

/*----defines, typedefs, structs, unions, enums-------------------------------*/
typedef enum
{
  RAS_GOOD_MESSAGE_ACK_RECEIVED,
  RAS_GOOD_MESSAGE_NON_ACK,
  RAS_GOOD_MESSAGE_WRONG_RECEIVER,
  RAS_BAD_MESSAGE,
  RAS_NOTHING_TO_DO,
  RAS_CORRECT_RECEIVER_NO_PROGRAMMING
} ReceivedAckStatus;

typedef enum
{
  TMS_NO_MESSAGE_SETUP,
  TMS_MESSAGE_SETUP,
  TMS_NO_MESSAGE_NEEDED,
  TMS_STILL_WAITING
} TransmitterMessageStatus;

#define SERIAL_NUMBER_ADDRESS_1 0x1000
#define SERIAL_NUMBER_ADDRESS_2 0x1010

/*----function prototypes-----------------------------------------------------*/
extern ReceivedAckStatus processReceivedAck(char* characterOffset);
extern int16 GetSlotsUntilCheckin(uint8 index);
extern TransmitterMessageStatus setupTransmitterMessage(char startOffset);
extern void resetLearnedInformation(void);
extern bool IsTransmitterProgrammed(uint8 index);
extern void SelectCurrentTransmitter(uint8 index);
extern uint32 ReadCurrentSerialNumber(void);
extern bool BothTransmitterNumbersInvalid(void);

#endif
