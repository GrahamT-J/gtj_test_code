/*H*****************************************************************************
Filename: hardware.h

Description: Miscellaneous hardware descriptions not included elsewhere

Copyright (c) Digital Monitoring Products Inc. 2009 - 2017. All rights reserved.
*****************************************************************************H*/

#ifndef HARDWARE_H
#define HARDWARE_H

/*----defines, typedefs, structs, unions, enums-------------------------------*/
#define MASTER_CLOCK 4300800

#define SMCLK MASTER_CLOCK

#define XEBitt (MASTER_CLOCK / 19200)

#define DIV_1 0
#define DIV_2 1
#define DIV_4 2
#define DIV_8 3

#define SLEEP_MASTER_CLOCK            32768
#define SLEEP_CLOCK_DIVIDER_SETTING   DIV_8
#define SLEEP_CLOCK_DIVIDER           (1 << SLEEP_CLOCK_DIVIDER_SETTING)
#define SLEEP_TIMERA_DIVIDER_SETTING  DIV_4
#define SLEEP_TIMERA_DIVIDER          (1 << SLEEP_TIMERA_DIVIDER_SETTING)

/*----data declarations-------------------------------------------------------*/
extern bool startupDelayComplete;

/*----function prototypes-----------------------------------------------------*/
extern void initSys(bool hardReset);
extern void StartupDelay(void);

#endif
