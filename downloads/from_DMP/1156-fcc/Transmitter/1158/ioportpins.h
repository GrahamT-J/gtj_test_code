/*H*****************************************************************************
Filename: ioportpins.h

Description: I/O port pin mapping

Copyright (c) Digital Monitoring Products Inc. 2009 - 2017. All rights reserved.
*****************************************************************************H*/

#ifndef IOPORTPINS_H
#define IOPORTPINS_H

#include "BitMacros.h"
#include "MSP430SFR.h"

#define LED_ON(A)  setIO(A)
#define LED_OFF(A) clearIO(A)

//-------------------------------------------------

#define RED_LED_MASK              BIT1
#define RED_LED_OUT               P1OUT
#define RED_LED_DIR               P1DIR
#define RED_LED_SEL               P1SEL
#define RED_LED_IN                P1IN

#define CSn_MASK                  BIT0
#define CSn_IN                    P1IN
#define CSn_OUT                   P1OUT
#define CSn_DIR                   P1DIR
#define CSn_SEL                   P1SEL

#define SI_MASK                   BIT2
#define SI_IN                     P1IN
#define SI_OUT                    P1OUT
#define SI_DIR                    P1DIR
#define SI_SEL                    P1SEL

#define SCLK_MASK                 BIT0
#define SCLK_IN                   P2IN
#define SCLK_OUT                  P2OUT
#define SCLK_DIR                  P2DIR
#define SCLK_SEL                  P2SEL

#define SO_GDO1_MASK              BIT3
#define SO_GDO1_IN                P1IN
#define SO_GDO1_DIR               P1DIR
#define SO_GDO1_SEL               P1SEL
#define SO_GDO1_IE                P1IE
#define SO_GDO1_IES               P1IES
#define S0_GDO1_IFG               P1IFG

#define RF_DATA_CLK_MASK          SO_GDO1_MASK
#define RF_DATA_CLK_IN            SO_GDO1_IN
#define RF_DATA_CLK_DIR           SO_GDO1_DIR
#define RF_DATA_CLK_SEL           SO_GDO1_SEL
#define RF_DATA_CLK_IE            SO_GDO1_IE
#define RF_DATA_CLK_IES           SO_GDO1_IES
#define RF_DATA_CLK_IFG           S0_GDO1_IFG

#define GDO0_MASK                 BIT4
#define GDO0_IN                   P2IN
#define GDO0_DIR                  P2DIR
#define GDO0_SEL                  P2SEL
#define GDO0_OUT                  P2OUT

#define RF_DATA_IN_MASK           GDO0_MASK
#define RF_DATA_IN_IN             GDO0_IN
#define RF_DATA_IN_DIR            GDO0_DIR
#define RF_DATA_IN_SEL            GDO0_SEL

#define RF_DATA_OUT_MASK          GDO0_MASK
#define RF_DATA_OUT_IN            GDO0_IN
#define RF_DATA_OUT_DIR           GDO0_DIR
#define RF_DATA_OUT_SEL           GDO0_SEL
#define RF_DATA_OUT_OUT           GDO0_OUT

// The SelectZoneInput() function in ScannedInputs.c relies on the MUX_A* inputs being connected
// to P2.1, P2.2, and P2.3, and must be updated along with this file if the pinout is changed.

#define MUX_A0_MASK               BIT1
#define MUX_A0_DIR                P2DIR
#define MUX_A0_SEL                P2SEL
#define MUX_A0_OUT                P2OUT

#define MUX_A1_MASK               BIT2
#define MUX_A1_DIR                P2DIR
#define MUX_A1_SEL                P2SEL
#define MUX_A1_OUT                P2OUT

#define MUX_A2_MASK               BIT3
#define MUX_A2_DIR                P2DIR
#define MUX_A2_SEL                P2SEL
#define MUX_A2_OUT                P2OUT

#define ZONE_MASK                 BIT5
#define ZONE_IN                   P2IN
#define ZONE_DIR                  P2DIR
#define ZONE_SEL                  P2SEL

#endif
