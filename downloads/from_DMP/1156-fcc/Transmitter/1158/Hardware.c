/*H*****************************************************************************
Filename: hardware.c

Description: Miscellaneous hardware descriptions not included elsewhere

Copyright (c) Digital Monitoring Products Inc. 2009 - 2017. All rights reserved.
*****************************************************************************H*/

#include "Types.h"
#include "Hardware.h"
#include "IOPortPins.h"
#include "Sleep.h"
#include "Inputs.h"
#include "WirelessCommunication.h"

bool startupDelayComplete = false;

enum {Delta = ((MASTER_CLOCK / (SLEEP_MASTER_CLOCK / SLEEP_CLOCK_DIVIDER)) & 0xFFFE)};  // must be even for test
#define STARTUP_DELAY (SLEEP_TICKS_PER_SECOND * 2) // 2 Seconds

/*F****************************************************************************
*
* Name: setDCO()
* Description:  Configure the processor clock to the frequency defined by
*               MASTER_CLOCK (via the Delta value).  Adjusts the DCOCTL (MODx)
*               and BCSCTL1 (RSELx) values based on the number of timer ticks
*               per SMCLK.
* Parameters:   None
* Return value: None
*
*****************************************************************************F*/
void setDCO(void)
{
  word startCapture = 0;
  bool finished = false;
  CCTL2 = CCIS0 | CM0 | CAP;
  TACTL = TASSEL_2 + MC_2 + TACLR;          // SMCLK, cont-mode, clear
  while (! finished)
  {
    word diff;
    while (! (CCTL2 & CCIFG))  // loop until capture
      ;

    CCTL2 &= ~(CCIFG);  // clear copture

    diff = (CCR2 - startCapture) & 0xFFFE;
    startCapture = CCR2;

    if (diff == Delta)
    {
      CCTL2 = 0;
      finished = true;
    }
    else if (diff > Delta)
    {
      if (DCOCTL-- == 0) // if rollover
      {
        --BCSCTL1;
      }
    }
    else
    {
      if (++DCOCTL == 0) // if rollover
      {
        ++BCSCTL1;
      }
    }
  }
}

/*F****************************************************************************
*
* Name: initSys()
* Description:  Initialize processor clock, pins, and watchdog, and trigger
*               the initial wakeup.
* Parameters:   - hard reset flag, needed by initInputs
* Return value: None
*
*****************************************************************************F*/
void initSys(bool hardReset)
{
  BCSCTL1 |= DIVA0 * SLEEP_CLOCK_DIVIDER_SETTING;
  BCSCTL3 |= XCAP_3;
  WDTCTL = WDTPW | WDTCNTCL | WDTSSEL;  // start watchdog, reset, and use ACLK

  LED_OFF(RED_LED);
  setIODirOut(RED_LED);

  setDCO();
  initRFChip();

  // Default unused pins to outputs -- Prevents floating inputs/Reduces power consumption
  P1DIR |=  (BIT4 | BIT5 | BIT6 | BIT7);
  P1OUT &= ~(BIT4 | BIT5 | BIT6 | BIT7);

  initInputs(hardReset);
  wakeup(true, 0);
}

/*F****************************************************************************
*
* Name: StartupDelay()
* Description:  Uses Timer A to put the processor to sleep for STARTUP_DELAY 
*               time.
* Parameters:   None
* Return value: None
*
*****************************************************************************F*/
void StartupDelay(void)
{
  TACTL = TASSEL_1 + // ACLK
          MC_2 +     // Continuous
          SLEEP_TIMERA_DIVIDER_SETTING * ID0;
  TACCR0 = TAR + STARTUP_DELAY;
  TACCTL0 = CCIE;
  WDTCTL = WDTPW | WDTHOLD | WDTCNTCL | WDTSSEL;  // stop watchdog
  __enable_interrupt();
  (void)LPM3;
  __disable_interrupt();
  WDTCTL = WDTPW | WDTCNTCL | WDTSSEL;  // start watchdog, reset, and use ACLK
  startupDelayComplete = true;
}
