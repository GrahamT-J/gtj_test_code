/* FCCTest.c contains the main loop for the FCC test.
 *
 * Copyright Digital Monitoring Products Inc. 2004 - 2017. All rights reserved.
 */

#include "IOPortPins.h"
#include "Hardware.h"
#include "Uart.h"
#include "ProtocolTiming.h"
#include "Chipcon.h"
#include "Hop.h"

/* A button press is registered if FCC_BUTTON is held down longer than
 * FCC_BUTTON_THRESHOLD main loop cycles.
 */
#define FCC_BUTTON_THRESHOLD 500u

/* PATTERN_BIT_TIME is the number of main loop cycles each bit in a flash
 * pattern lasts.  It is best if this is a power of two.
 */
#define PATTERN_BIT_TIME 8192u

/* FCC state machine states */
enum State
{
  UNMOD_LOW,                    /* unmodulated carrier on channel 0 */
  MOD_LOW,                      /* modulated carrier on channel 0 */
  UNMOD_MID,                    /* unmodulated carrier on channel 26 */
  MOD_MID,                      /* modulated carrier on channel 26 */
  UNMOD_HIGH,                   /* unmodulated carrier on channel 52 */
  MOD_HIGH,                     /* modulated carrier on channel 52 */
  UNMOD_HOP,                    /* unmodulated carrier in hopping sequence */
  IDLE,                         /* idle */
  BAD,                          /* illegal state */
};

/* Mode bits for setMode() */
enum
{
  M_HOP = 0x1,
  M_MOD = 0x2,
  M_TX = 0x4,
};

/* flashPatterns contains the survey LED flash patterns for each state.  Each
 * bit represents a slice of time.  If the bit is set, the LED is lit;
 * otherwise, it is unlit.
 */
unsigned flashPatterns[] = {
  [UNMOD_LOW]  = 0x0001u,
  [MOD_LOW]    = 0x0005u,
  [UNMOD_MID]  = 0x0015u,
  [MOD_MID]    = 0x0055u,
  [UNMOD_HIGH] = 0x0155u,
  [MOD_HIGH]   = 0x0555u,
  [UNMOD_HOP]  = 0x1555u,
  [IDLE] = 0u,
  [BAD] = 0u
};

void dataOff(void);
void dataOn(void);

static void setMode(unsigned char channel, unsigned char mode);

void FCCMain(void)
{
  unsigned cycle;           /* main loop cycle counter (for flash patterns) */
  unsigned bit;             /* current bit (for flash patterns) */

  enum State state;         /* current state */
  unsigned accum;           /* FCC state change switch accumulator */
  bool isp;                 /* if non-zero, switch is pressed */
  bool wasp;                /* if non-zero, switch was pressed last cycle */

  bit = 0x8000u;

  state = IDLE;
  accum = 0;
  isp = false;
  wasp = false;

  setMode(0, 0);

  for (cycle = 0;; cycle++)
  {
    if (cycle % PATTERN_BIT_TIME == 0)
      bit >>= 1;
    if (bit == 0)
      bit = 0x8000u;

    if ((flashPatterns[state] & bit) > 0)
      LED_ON(RED_LED);
    else
      LED_OFF(RED_LED);

    if ((!getIO(ZONE)) && (accum < FCC_BUTTON_THRESHOLD))
    {
      if (++accum >= FCC_BUTTON_THRESHOLD)
        isp = true;
    }
    else if (getIO(ZONE) && (accum > 0))
    {
      if (--accum == 0)
        isp = false;
    }

    if (isp && !wasp)
    {
      state++;
      if (state >= BAD)
        state = UNMOD_LOW;
      switch (state)
      {
      case UNMOD_LOW:
        setMode(0, M_TX);
        break;
      case MOD_LOW:
        setMode(0, M_MOD | M_TX);
        break;
      case UNMOD_MID:
        setMode(NUM_HOP_CHANNELS / 2, M_TX);
        break;
      case MOD_MID:
        setMode(NUM_HOP_CHANNELS / 2, M_MOD | M_TX);
        break;
      case UNMOD_HIGH:
        setMode(NUM_HOP_CHANNELS - 1, M_TX);
        break;
      case MOD_HIGH:
        setMode(NUM_HOP_CHANNELS - 1, M_MOD | M_TX);
        break;
      case UNMOD_HOP:
        setMode(0, M_HOP | M_TX);
        break;
      case IDLE:
        setMode(0, 0);
        break;
      default:
        /* UNREACHABLE */
        WDTCTL = 0;
      }
    }

    wasp = isp;
  }
}

static void setMode(unsigned char channel, unsigned char mode)
{
  sendChipconCommandStrobe(SIDLE);

  if (mode & M_HOP)
  {
    houseCode = 1;
    TACCR2 = TAR + RFBitTime * FB_TOTAL;
    TACCTL2 = CCIE;
  }
  else
  {
    TACCTL2 = 0;
  }

  if (mode & M_MOD)
    dataOn();
  else
    dataOff();

  sendChannel(channel);

  if (mode & M_TX)
    sendChipconCommandStrobe(STX);
  else
    sendChipconCommandStrobe(SRX);
}
