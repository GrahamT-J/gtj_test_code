/*H*****************************************************************************
FILENAME:

DESCRIPTION:

NOTES:

Copyright (c) Digital Monitoring Products Inc. 1999.  All rights reserved.

	Last change:  TDC  27 Jan 99    8:43 am
*****************************************************************************H*/
/*----compilation control-----------------------------------------------------*/

/*******************************************************************************
INCLUDE FILES
*******************************************************************************/
/*----system and platform files (external libraries) -------------------------*/


/*----program files-----------------------------------------------------------*/
#include "Hardware.h"
#include "Uart.h"
#include <stdio.h>

/*******************************************************************************
EXTERNAL REFERENCES   Only used if not available in a header file.
*******************************************************************************/
/*----data declarations-------------------------------------------------------*/

/*----function prototype------------------------------------------------------*/


/*******************************************************************************
PRIVATE DECLARATIONS  Defined in this module, used only in this module.
*******************************************************************************/
/*----defines, typedefs, structs, unions, enums-------------------------------*/
#define RXD       0x04                      // RXD on P2.2
#define TXD       0x02                      // TXD on P1.1
#define TCK       0x10                      // SMCLk on P1.4
/*----data declarations-------------------------------------------------------*/
unsigned int RXTXData;
unsigned char BitCnt;
unsigned int Compare, Oldcapture;
char byteReceived;

/*----function prototype------------------------------------------------------*/
//void TX_Byte (void);
//void RX_Ready (void);
/*----macros------------------------------------------------------------------*/


/*******************************************************************************
PUBLIC FUNCTION DEFINITIONS  Note: Header is repeated for each function.
*******************************************************************************/
/*F*****************************************************************************

DESCRIPTION:

NOTES:

*****************************************************************************F*/
char waitForString (char * string)
{
  char data=0;
  char count=0;
  while(data != '\r' && data != '\n')
  {
    data = waitForChar();
    *string = data;
    string++;
    count++;
  }
  *--string = 0;
  return(--count);
}

/*F*****************************************************************************

DESCRIPTION:

NOTES:

*****************************************************************************F*/
void sendString (const char * string)
{
  while(*string != 0)
  {
    RXTXData = *string;
    TX_Byte();
    string++;
  }
}

/*F*****************************************************************************

DESCRIPTION:

NOTES:

*****************************************************************************F*/
char waitForChar (void)
{
  char returnValue;

  RX_Ready();
  returnValue = RXTXData;
  TX_Byte();
  return(returnValue);
}


/*F*****************************************************************************

DESCRIPTION:

NOTES:

*****************************************************************************F*/
void initUart(void)
{
  CCTL0 = OUT;                              // TXD Idle as Mark
  TACTL = TASSEL_1 + MC_2;                  // ACLK, continuous mode
#ifndef DMP_FCC_TEST
  P1SEL |= TXD | TCK;                       // P1.1/TA0 for TXD function
  P1DIR |= TXD | TCK;                       // TXD output on P1
  P2SEL |= RXD;                             // P2.2/TA0 as RXD input
  P2DIR &= ~RXD;
#endif
}

/*******************************************************************************
PRIVATE FUNCTION DEFINITIONS  Note: Header is repeated for each function.
*******************************************************************************/
/*f*****************************************************************************

DESCRIPTION:

NOTES:

*****************************************************************************f*/
// Function Transmits Character from RXTXData Buffer
void TX_Byte (void)
{

  BitCnt = 0xA;                             // Load Bit counter, 8data + ST/SP
  CCR0 = TAR;                               // Current state of TA counter
  CCR0 += Bitime;                           // Some time till first bit
  RXTXData |= 0x100;                        // Add mark stop bit to RXTXData
  RXTXData = RXTXData << 1;                 // Add space start bit
  CCTL0 = OUTMOD0 + CCIE;                   // TXD = mark = idle
  __no_operation();
  while ( CCTL0 & CCIE );                   // Wait for TX completion
}

/*f*****************************************************************************

DESCRIPTION:

NOTES:

*****************************************************************************f*/
// Function Readies UART to Receive Character into RXTXData Buffer
// Sync capture not possible as DCO=TACLK=SMCLK can be off !!
void RX_Ready (void)
{
  BitCnt = 0x8;   // Load Bit counter
  byteReceived = false;
  CCTL0 = CM1 + CCIS0 + OUTMOD0 + CAP + CCIE; // Neg Edge, Cap
  while(!byteReceived);
  //_BIS_SR(LPM3_bits + GIE);                 // Enter LPM3 w/ interr until char RXed
  __no_operation();
}

/*f*****************************************************************************

DESCRIPTION:

NOTES:

*****************************************************************************f*/
// Timer A0 interrupt service routine
#pragma vector=TIMERA0_VECTOR
__interrupt void Timer_A (void)
{
  CCR0 += Bitime;                           // Add Offset to CCR0

// RX
  if (CCTL0 & CCIS0)                        // RX on CCI0B?
  {
    if( CCTL0 & CAP )                       // Capture mode = start bit edge
    {
    CCTL0 &= ~ CAP;                         // Switch from capture to compare mode
    CCR0 += Bitime_5;
    _BIC_SR_IRQ(SCG1 + SCG0);               // DCO reamins on after reti
    }
    else
    {
    RXTXData = RXTXData >> 1;
      if (CCTL0 & SCCI)                     // Get bit waiting in receive latch
      RXTXData |= 0x80;
      BitCnt --;                            // All bits RXed?
      if ( BitCnt == 0)
//>>>>>>>>>> Decode of Received Byte Here <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
      {
      CCTL0 &= ~ CCIE;                      // All bits RXed, disable interrupt
      //_BIC_SR_IRQ(LPM3_bits);               // Clear LPM3 bits from 0(SR)
      byteReceived = true;
      }
//>>>>>>>>>> Decode of Received Byte Here <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    }
  }
// TX
  else
  {
    if ( BitCnt == 0)
    CCTL0 &= ~ CCIE;                        // All bits TXed, disable interrupt
    else
    {
      CCTL0 |=  OUTMOD2;                    // TX Space
      if (RXTXData & 0x01)
      CCTL0 &= ~ OUTMOD2;                   // TX Mark
      RXTXData = RXTXData >> 1;
      BitCnt --;
    }
  }
}

extern void TimerA2ISR(void);

#pragma vector=TIMERA1_VECTOR
__interrupt void TIMER_A1(void)
{
  switch ( TAIV )
  {
  case 0x02:
    TACCR1 += 4;
    if( (TACCTL1 & OUTMOD_7) == OUTMOD_1 )
    {
      TACCTL1 = OUTMOD_5 + CCIE;
    }
    else
    {
      TACCTL1 = OUTMOD_1 + CCIE;
    }
    break;

  case 0x04:
    TimerA2ISR();
    break;

  default:
    break;
  }
}
