/* main.c contains the main loop for the production test.
 *
 * Copyright Digital Monitoring Products Inc. 2004-2015.
 */

#ifdef DMP_FCC_TEST
#ifdef DMP_PRODUCTION_TEST
#error "Cannot define both DMP_FCC_TEST and DMP_PRODUCTION_TEST."
#endif
#else
#ifndef DMP_PRODUCTION_TEST
#error "Must define one of DMP_FCC_TEST or DMP_PRODUCTION_TEST."
#endif
#endif

#include "Hardware.h"
#include "Uart.h"
#include "ProtocolTiming.h"
#include "Chipcon.h"
#include "Hop.h"

void FCCMain(void);

/* TXBUFLEN is the maximum number of bytes in the transmit buffer. */
#define TXBUFLEN 6

/* txBuf is the transmit buffer; when in transmit mode, the data in
 * txBuf[0..txLen-1] is sent continuously.  The bit sent next is a one if
 * txBuf[txByte] & txBit is non-zero or zero otherwise.
 */
static unsigned char txBuf[TXBUFLEN];
static unsigned char txBit;
static unsigned txByte;
static unsigned txLen;

unsigned int syncBitCounter = 0;
unsigned int patternRegister = 0;
bool TX = false;
bool BERDone = false;
byte BERCounter = 0;

int hexToInt(const char* string, char length);
unsigned long hexToLong(const char* string, char length);
void intToHex(int val, char* string, char length);

void configRegisterPoke(void);
void commandStrobe(void);
void statusRegisterPeek(void);
void showList(void);
void TransmitData(void);
void ReadRSSI(void);
void setFrequency(void);
void setMode(void);
void configRegisterPeek(void);
void initRFChip(void);
void sendDMPChannel(void);
void toggleLED(void);
void dataOn(void);
void dataOff(void);
void frequencySet(void);
void berTest(void);
void readZones(void);
void modeSelect(void);
void toggleClock(void);
void decrementChannel(void);
void incrementChannel(void);
void debounceDelay(void);

const struct
{
  const char command;
  void (*function)(void);
} commandList[] =
{
  {'1', setFrequency},
  {'2', setMode},
  {'3', ReadRSSI},
  {'4', TransmitData},
  {'5', configRegisterPoke},
  {'6', configRegisterPeek},
  {'7', commandStrobe},
  {'8', statusRegisterPeek},
  {'9', initRFChip},
  {'A', sendDMPChannel},
  {'D', dataOn},
  {'d', dataOff },
  {'F', frequencySet},
  {'B', berTest},
  {'I', toggleLED},
  {'i', toggleLED},
  {'K', readZones},
  {'M', modeSelect},
  {'J', toggleClock},
  {'j', toggleClock},
  {'+', incrementChannel},
  {'-', decrementChannel}
};

void main (void)
{
#ifdef DMP_PRODUCTION_TEST
  char command;
  char lcv;
#endif

  initHardware();

#ifdef LOW_INTERFERENCE
  SetFrequencyMode(LOW_INTERFERENCE_FREQUENCY_MODE);
#else
  SetFrequencyMode(STANDARD_FREQUENCY_MODE);
#endif

#ifdef DMP_FCC_TEST
  FCCMain();
#else
  sendChannel(26);
  writeChipconConfigRegister(DEVIATN, 0x00);
  sendChipconCommandStrobe(STX);

  sendString("MODE=02 RTP=6B75 FSP=D00000 ADP=A160 PAT=817E817E\n\r");
  for (;;)
  {
    command = waitForChar();
    for (lcv = 0; lcv < sizeof(commandList)/sizeof(commandList[0]); lcv++)
    {
      if (command == commandList[lcv].command)
      {
        commandList[lcv].function();
        break;
      }
    }
  }
#endif
}

void incrementChannel(void)
{
    sendChipconCommandStrobe(SIDLE);
    if (++currentChannel >= NUM_HOP_CHANNELS)
      currentChannel = 0;
    sendChannel(currentChannel);
    sendChipconCommandStrobe(STX);
}

void decrementChannel(void)
{
    sendChipconCommandStrobe(SIDLE);
    if (currentChannel == 0)
      currentChannel = NUM_HOP_CHANNELS - 1;
    else
      currentChannel--;
    sendChannel(currentChannel);
    sendChipconCommandStrobe(STX);
}

void configRegisterPoke(void)
{
  char address[6];
  char value[6];
  char stringLength = 0;
  unsigned int addressInt;
  unsigned int valueInt;

  sendString("\n\rEnter the address: ");
  stringLength = waitForString(&address[0]);
  addressInt = hexToInt(&address[0],stringLength);
  sendString("\n\rEnter the value: ");
  stringLength = waitForString(&value[0]);
  valueInt = hexToInt(&value[0],stringLength);
  if(addressInt < 0x30 && valueInt < 0xFF)
    writeChipconConfigRegister((ConfigRegisters)addressInt,valueInt);
}

void configRegisterPeek(void)
{
  char address[6];
  char stringLength = 0;
  unsigned int addressInt;
  unsigned int valueInt;

  sendString("\n\rEnter the address: ");
  stringLength = waitForString(&address[0]);
  addressInt = hexToInt(&address[0],stringLength);
  if(addressInt < 0x30)
  {
      valueInt = readChipconConfigRegister((ConfigRegisters)addressInt);
      sendString("\n\rThe register value: ");
      intToHex(valueInt, &address[0], 4);
      sendString(&address[0]);
      sendString("\n\rPress Any Key");
      waitForChar();
  }
}

void TransmitData(void)
{
  unsigned i;
  char str[sizeof(txBuf)*2];

  clearIOEnableInt(RF_DATA_CLK);
  sendChipconCommandStrobe(SIDLE); // put the RF Chip into Idle mode
  writeChipconConfigRegister(MDMCFG2, 0x00);
  setIODirOut(RF_DATA_OUT);

  sendString("\n\rEnter the Data Pattern: ");
  waitForString(str);

  for (i = 0; i < sizeof(txBuf); i++)
    txBuf[i] = hexToInt(str + i * 2, 2);

  txByte = 0;
  txBit = 0x80;

  TX = true;
  sendChipconCommandStrobe(STX);  // change the RF chips mode to TX
      // Prepare for interrupts on the data clk line
  clearIOIntFlag(RF_DATA_CLK);
  setIORisingInt(RF_DATA_CLK);
  setIOEnableInt(RF_DATA_CLK);
}

void toggleLED(void)
{
  sendString(" LED pin in use by UART\n\r");
  /*
  if ( getIO(RED_LED) )
  {
    sendString("luminate off\n\r");
    clearIO(RED_LED);
  }
  else
  {
    sendString("luminate on\n\r");
    setIO(RED_LED);
  }
  */
}

void initRFChip(void)
{
  initChipcon();
  sendString("\n\rChipcon Chip Set To Defaults");
  waitForChar();
}


void sendDMPChannel(void)
{
  int channel;
  unsigned char stringLength;
  char channelString[6];

  sendString("\n\rPlease enter a channel");
  stringLength = waitForString(&channelString[0]);
  channel = hexToInt(&channelString[0], stringLength);
  sendChannel(channel);
}

void ReadRSSI(void)
{
  int RSSIDataInt;
  char RSSIData[6];

  RSSIDataInt = readChipconStatusRegister(RSSI);
  sendString("\n\rCurrent RSSI: ");
  intToHex(RSSIDataInt, &RSSIData[0], 4);
  sendString(&RSSIData[0]);
  sendString("\n\rPress Any Key");
  waitForChar();
  //printf("\n\rCurrentRSSI: %d", RSSIData);
  //printf("\n\rPress any key:");
  //scanf("%c");
}

void setFrequency(void)
{
  unsigned long dataLong;
  char data[6];
  char tempData;

  //printf("\n\rEnter the Frequency Register Value: ");
  sendString("\n\rEnter the Frequency Register Value: ");
  //scanf("%6Lx", &data);
  tempData = waitForString(&data[0]);
  dataLong = hexToLong(&data[0], tempData);
  if(dataLong < 0xFFFFFF)
  {
    tempData = dataLong & 0xFF;
    writeChipconConfigRegister(FREQ0, tempData);
    tempData = dataLong>>8 & 0xFF;
    writeChipconConfigRegister(FREQ1, tempData);
    tempData = dataLong>>16 & 0xFF;
    writeChipconConfigRegister(FREQ2, tempData);
  }

}

void setMode(void)
{
  char userChoice;

  do
  {
    sendString("\f1)\tTX Mode");
    sendString("\n\r2)\tRX Mode");
    sendString("\n\r3)\tSleep Mode");
    sendString("\n\r4)\tIdle Mode");
    sendString("\n\rEnter the mode:");
    userChoice = waitForChar();
  }while( userChoice > '5' || userChoice < '1');
  switch( userChoice )
  {
  case '1':
    sendChipconCommandStrobe(STX);
    TX = true;
    break;
  case '2':
    sendChipconCommandStrobe(SRX);
    TX = false;
    break;
  case '3':
    sendChipconCommandStrobe(SPWD);
    break;
  case '4':
    sendChipconCommandStrobe(SIDLE);
    break;
  }

}

void commandStrobe(void)
{
  int addressInt;
  char address[6];
  char stringLength;

  sendString("\n\rEnter the address: ");
  stringLength = waitForString(&address[0]);
  addressInt = hexToInt(&address[0], stringLength);
  if(addressInt >= 0x30 && addressInt < 0x3F)
    sendChipconCommandStrobe((CommandStrobes)addressInt);
}

void statusRegisterPeek(void)
{
    int addressInt, valueInt = 0;
    char address[6];
    char stringLength = 0;
    //printf("\n\rEnter the address: ");
    sendString("\n\rEnter the address: ");
    //scanf("%2x", &address);
    stringLength = waitForString(&address[0]);
    addressInt = hexToInt(&address[0], stringLength);
    if(addressInt >= 0x30 && addressInt < 0x3f)
    {
      valueInt = readChipconStatusRegister((StatusRegisters)addressInt);
      sendString("\n\rThe register value: ");
      intToHex(valueInt, &address[0], 4);
      sendString(&address[0]);
      sendString("\n\rPress Any Key");
      waitForChar();
      //printf("\n\rThe register value: %x", value);
      //printf("\n\rPress Any Key:");
      //scanf("%c");
    }
}

/*******************************************************************************

DESCRIPTION:  Converts integer 'val' into a hex string 'string' of length 'length'

NOTES: does not work with length > 4

*******************************************************************************/
void intToHex(int val, char* string, char length)
{
  const char hexTable[] = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
  char i;
  int workingVal = val;
  char* writeString = string + length;  // start at rightmost character
  *writeString = 0;
  writeString--;
  for (i = 0; i < length; ++i)
  {
    *writeString = hexTable[workingVal & 0x000F];
    --writeString;
    workingVal >>= 4;
  }
}

/*******************************************************************************

DESCRIPTION: converts 'length' characters of ascii hexadecimal 'string' to an integer

NOTES:

*******************************************************************************/
int hexToInt(const char* string, char length)
{
  int returnValue = 0;
  char i;
  bool inv = false;

  for (i = 0; i < length; ++i)
  {
    char character = string[i];
    if (character == '-' && i == 0)
    {
      inv = true;
      continue;
    }

    if (character >= 'A' && character <= 'F')
      character -= 'A' - 10;
    else if (character >= '0' && character <= '9')
      character -= '0';
    else
      continue;  // ignore invalid character

    returnValue *= 16;
    returnValue += character;
  }

  if (inv)
    return -returnValue;
  else
    return returnValue;
}


unsigned long hexToLong(const char* string, char length)
{
  char i;
  unsigned long returnValue = 0;
  for (i = 0; i < length; ++i)
  {
    char character = string[i];
    if ((character >= 'A') && (character <= 'F'))
    {
      character -= ('A' - 10);
    }
    else if ((character >= '0') && (character <= '9'))
    {
      character -= '0';
    }
    else
    {
      continue;  // ignore invalid character
    }
    returnValue *= 16;
    returnValue += character;
  }
  return returnValue;
}

void rfDataClockEdge(void)
{
  unsigned char bit = getIO(RF_DATA_IN) > 0;

  if (TX)
  {
    bit = txBuf[txByte] & txBit;
    if (bit > 0)
      setIO(RF_DATA_OUT);
    else
      clearIO(RF_DATA_OUT);

    if ((txBit >>= 1) == 0)
    {
      txBit = 0x80;

      if (++txByte >= txLen)
        txByte = 0;
    }
  }
  else
  {
    patternRegister = (patternRegister << 1) | bit;
    syncBitCounter++;

    if (patternRegister == 0x817E)
    {
      patternRegister = 0;
      BERCounter++;
    }
    else if (syncBitCounter > 2000)
    {
      clearIOEnableInt(RF_DATA_CLK);
      BERDone = true;
    }
  }
}

/*******************************************************************************

DESCRIPTION:

NOTES: This assumes that any edge interrupts on this port are for alarm contacts

*******************************************************************************/
#ifdef __ICC430__
#pragma vector = PORT1_VECTOR
#pragma type_attribute = __interrupt
#endif
void port1EdgeInterrupt(void)
#ifndef __ICC430__
__interrupt[PORT1_VECTOR]
#endif
{
    // first check for rf clock edge
  if (getIOIntFlag(RF_DATA_CLK))
  {
    clearIOIntFlag(RF_DATA_CLK);
    rfDataClockEdge();
  }
  //else if(getIOIntFlag(TAMPER))
  //{
  //  clearIOIntFlag(TAMPER);
  //  debounceDelay();
  //  if(!getIO(TAMPER))
  //    incrementChannel();
  //}
}

void dataOn (void)
{
#ifndef DMP_FCC_TEST
  sendString("ata on\n\r");
#endif
  clearIOEnableInt(RF_DATA_CLK);
  sendChipconCommandStrobe(SIDLE); // put the RF Chip into Idle mode
  writeChipconConfigRegister(MDMCFG2, 0x00);
  writeChipconConfigRegister(DEVIATN, 0x35);
  setIODirOut(RF_DATA_OUT);

  txBuf[0] = 0xaa;
  txBit = 0x80;
  txByte = 0;
  txLen = 1;
  TX = true;

  sendChipconCommandStrobe(STX);
  clearIOIntFlag(RF_DATA_CLK);
  setIORisingInt(RF_DATA_CLK);
  setIOEnableInt(RF_DATA_CLK);
}

void dataOff (void)
{
#ifndef DMP_FCC_TEST
  sendString("ata off\n\r");
#endif
  clearIOEnableInt(RF_DATA_CLK);
  writeChipconConfigRegister(DEVIATN, 0x00);
}

void frequencySet (void)
{
  unsigned long dataLong;
  char data[6];
  char tempData;

  sendChipconCommandStrobe(SIDLE);
  sendString("SP= ");
  tempData = waitForString(&data[0]);
  dataLong = hexToLong(&data[0], tempData);
  writeChipconConfigRegister(DEVIATN, 0x00);

  if (dataLong == 0xd09aac)
    dataLong = 0x22b13B;
  else if (dataLong == 0xd00000)
    dataLong = 0x23313b;
  else if (dataLong == 0xd06554)
    dataLong = 0x23b13b;

  if(dataLong < 0xFFFFFF)
  {
    tempData = dataLong & 0xFF;
    writeChipconConfigRegister(FREQ0, tempData);
    tempData = dataLong>>8 & 0xFF;
    writeChipconConfigRegister(FREQ1, tempData);
    tempData = dataLong>>16 & 0xFF;
    writeChipconConfigRegister(FREQ2, tempData);
  }
  if (TX)
    sendChipconCommandStrobe(STX);
  else
    sendChipconCommandStrobe(SRX);
  sendString("\n\r");
}

void berTest (void)
{
  char hexString[5];

  clearIOEnableInt(RF_DATA_CLK); // disable the DCLK interrupt before configureing RF Chip
  writeChipconConfigRegister(MDMCFG2, 0x00); //turn off sync word detection
  setIODirIn(RF_DATA_IN); // set the data pin direction
    // Prepare for interrupts on the data clk
  syncBitCounter = 0;
  BERDone = false;
  BERCounter = 0;
  clearIOIntFlag(RF_DATA_CLK);
  setIORisingInt(RF_DATA_CLK);
  setIOEnableInt(RF_DATA_CLK);
  while(!BERDone);
  sendString("ER= ");
  intToHex(BERCounter, &hexString[0], 2);
  sendString(&hexString[0]);
  sendString("\n\r");
}

void readZones (void)
{
  unsigned char zoneState = 0;
  char hexString[3];

  sendChipconCommandStrobe(SIDLE);
  sendString("=");

  initZoneIO();
  for (unsigned char i = 0; i < 8; i++)
  {
    // Set MUX_A0, MUX_A1, and MUX_A2
    P2OUT = (P2OUT & ~(BIT1 | BIT2 | BIT3)) | (i << 1);

    // The multiplexer propagation delay is 12ns at 3.3v, one processor cycle
    // is 125ns (8 MHz).
    __delay_cycles(1);

    if (getIO(ZONE))
    {
      zoneState |= (1 << i);
    }
  }
  initUart();

  intToHex(zoneState, hexString, 2);
  sendString(hexString);
  sendString("\n\r");
}

void modeSelect (void)
{
  char modeString;

  sendString("= ");
  modeString = waitForChar();
  switch (modeString)
  {
  case '1':
    sendChipconCommandStrobe(SIDLE);
    writeChipconConfigRegister(DEVIATN, 0x35);
    clearIOEnableInt(RF_DATA_CLK);
    setIODirIn(RF_DATA_IN);
    TX = false;
    sendChipconCommandStrobe(SRX);
    break;
  case '2':
    sendChipconCommandStrobe(SIDLE);
    writeChipconConfigRegister(DEVIATN, 0x00);
    clearIOEnableInt(RF_DATA_CLK);
    setIODirOut(RF_DATA_OUT);
    TX = true;
    sendChipconCommandStrobe(STX);
    break;
  case '3':
    sendChipconCommandStrobe(SIDLE);
    sendChipconCommandStrobe(SPWD);
    break;
  }
  sendString("\n\r");
}


void toggleClock(void)
{
  if(TACCTL1 & CCIE)
  {
    TACCTL1 = 0;
    setIODirOut(SI);
    clearIOSel(SI);
    sendString("CLK off\n\r");
  }
  else
  {
    TACCR1 = TAR + 4;
    TACCTL1 = OUTMOD_1 + CCIE;
    setIODirOut(SI);
    setIOSel(SI);
    sendString("CLK on\n\r");
  }
}

#ifdef __ICC430__
#pragma vector = PORT2_VECTOR
#pragma type_attribute = __interrupt
#endif
void port2EdgeInterrupt(void)
#ifndef __ICC430__
__interrupt[PORT2_VECTOR]
#endif
{
  P2IFG = 0;
}

void debounceDelay()
{
  int i;

  for( i=0; i<8125; i++);

  return;
}

void TimerA2ISR(void)
{
  TACCR2 = TAR + RFBitTime * FB_TOTAL;
  sendChipconCommandStrobe(SIDLE);
  currentChannel = addChannel(currentChannel, houseCode);
  SetFrequency(currentChannel);
  if (TX)
  {
    sendChipconCommandStrobe(STX);
  }
  else
  {
    sendChipconCommandStrobe(SRX);
  }
}
