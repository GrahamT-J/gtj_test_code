/*H*****************************************************************************
Filename: scannedinputs.h

Description: Processes inputs by periodic scanning (vs interrupt driven)

Copyright (c) Digital Monitoring Products Inc. 2009 - 2017. All rights reserved.
*****************************************************************************H*/

#ifndef SCANNEDINPUTS_H
#define SCANNEDINPUTS_H

#include "packet.h"
#include "sleep.h"
#include "DMP.h"

/*----defines, typedefs, structs, unions, enums-------------------------------*/
// note, for proper sync, the debounce time must equal an integral number of hops
// with no fraction.
// For example, with 32 hops per second and a debounce of 500mS, there are exactly
// 16 hops during debounce.
enum
{
  DEBOUNCE_MS = 250,
  DEBOUNCE_TIME = ((long)DEBOUNCE_MS * SLEEP_TICKS_PER_SECOND) / 1000,
};

#define setupInputs() // not used for scanned inputs

/*----function prototypes-----------------------------------------------------*/
extern void initInputs(bool hardReset);
extern void startInputDebounce(short multiplier);
extern ZoneMessageByte getInputState(uint8 transmitterIndex);

#endif
