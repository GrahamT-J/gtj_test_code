/*H*****************************************************************************
Filename: chipconcommunication.c

Description: High level communication routines for the Chipcon radio

Copyright (c) Digital Monitoring Products Inc. 2009 - 2017. All rights reserved.
*****************************************************************************H*/

#include "types.h"
#include "chipconCommunication.h"
#include "ioportpins.h"
#include "Hop.h"
#include "Packet.h"
#include "ProtocolTiming.h"
#include "Hardware.h"
#include "Sleep.h"
#include "Main.h"

CommunicationStatusFlags communicationStatus;

static byte receiveBitPointer;
static byte receiveBytePointer;
static byte transmitBytePointer;
static byte transmitBitPointer;
static byte bitCounter;
static byte transmitDelayCounter;
word RFFrameBitCounter;
static unsigned long patternRegister = 0;

void initStateForTransmit(void);

/*F****************************************************************************
*
* Name: WirelessTimingInterrupt()
* Description:  ISR for Timer A1.  TACCR1 is setup as a timing for enabling the
*               contact TACCR2 is setup for RF bit timing.  Note: Timer A runs
*               off ACLK.
* Parameters:   None
* Return value: None
*
*****************************************************************************F*/
#pragma vector = TIMERA1_VECTOR
#pragma type_attribute = __interrupt
void WirelessTimingInterrupt(void)
{
  switch (TAIV)
  {
    case 0x02: //TACCR1 timer interrupt
    {
      break;
    }

    case 0x04:// TACCR2 timer interrupt
    {
      CCR2 += RFBitTime; // next capture time = bit timing

      if (getSleeping())
      {
        if (wakeup(false, 0))
        {
          __low_power_mode_off_on_exit();
        }
        else
        {
          // we were not allowed to wakeup, so wakeup in one minute
          sleepTime = TAR;  // record where we are when we sleep
          CCR2 = sleepTime + WAKEUP_RATE;
        }
        return;
      }
      break;
    }

    default:  //default catch all
    {
      break;
    }
  }
}

/*F****************************************************************************
*
* Name: initRFChip()
* Description:  Will init the RF chip to a known startup state.
* Parameters:   None
* Return value: None
*
*****************************************************************************F*/
void initRFChip(void)
{
  initChipcon();
  setupWirelessSleep();
}

/*F****************************************************************************
*
* Name: setupWirelessReceive()
* Description:  Will setup the wireless chip for receiving data.
* Parameters:   None
* Return value: None
*
*****************************************************************************F*/
void setupWirelessReceive(void)
{
  initStateForReceive();  // init the communications state for RX
  clearIOEnableInt(RF_DATA_CLK); // disable the DCLK interrupt before configureing RF Chip
  setIODirIn(RF_DATA_IN); // set the data pin direction
  sendChipconCommandStrobe(SRX);  // place the RF Chip into RX mode
  CCR2 = TAR + RFBitTime;
  CCTL2 = OUTMOD_5+CM0+CCIE; // CC2 = Compare, Reset
    // Prepair for interrupts on the data clk
  setIORisingInt(RF_DATA_CLK);
  clearIOIntFlag(RF_DATA_CLK);
  setIOEnableInt(RF_DATA_CLK);
}

/*F****************************************************************************
*
* Name: ReceiveRFData()
* Description:  Function called by an ISR and will receive a bit and either
*               search for pattern or record the bit in the receiver buffer.
* Parameters:   None
* Return value: None
*
*****************************************************************************F*/
void ReceiveRFData(void)
{
  bool rxDataBit = getIO(RF_DATA_IN) > 0;  // read state of data ASAP
  static bool lastRxBit;

  // if we are currently receiving data from a valid receiver
  if (communicationStatus.patternDetected || communicationStatus.frameStartDetected)
  {
    // we've received 8 bits of same value in a row, so remove transition
    if (bitCounter >= MAX_BITS_IN_A_ROW)
    {
      if (rxDataBit == lastRxBit) // error, should be a transition
        communicationStatus.frameStartDetected = false;  // no longer in sync
    }
    else
    {
      // record data bit
      if (rxDataBit)
        receiverPacketBuffer[receiveBytePointer] += receiveBitPointer;

      receiveBitPointer >>= 1;
      // If we have received a whole byte
      if (receiveBitPointer == 0)
      {
        receiveBitPointer = 0x80;
        // if just received first byte of a receivers transmission
        if (receiveBytePointer == 0)
        {
          // check the house code to verify its from our receiver
          if ((houseCode == INVALID_HOUSE_CODE) || (houseCode == ((ReceiverAck*)receiverPacketBuffer)->houseCode))
          {
            communicationStatus.frameStartDetected = true;
            RFFrameBitCounter = SlotSYNC;
          }
          communicationStatus.patternDetected = false;
        }
        // If we havn't overflowed setup for next byte
        if (receiveBytePointer < PR_SIZE - 1)
        {
          receiveBytePointer++;
          receiverPacketBuffer[receiveBytePointer] = 0;
        }
      }
    }
    // Are we sending a bit transition?
    if (rxDataBit != lastRxBit)
    {
      // Reset transition counter
      lastRxBit = rxDataBit;
      bitCounter = 1;
    }
    else
    {
      // no transition so count
      bitCounter++;
    }
  }
  else
  {
    // shift in newest data bit
    patternRegister <<=1;
    if (rxDataBit)
      patternRegister |= 0x01;
    else
      patternRegister &= ~0x01;
    // did we recieve the last bit of the last pattern byte?
    if((patternRegister & 0x00FFFFFF) == RECEIVER_PATTERN)
    {
      // we recieved the pattern from the receiver so setup receive
      initStateForReceive();
      communicationStatus.patternDetected = true;
      bitCounter = 0;
    }
  }
}

/*F****************************************************************************
*
* Name: TransmitRFData()
* Description:  Function called from an ISR will transmit a single bit each
*               time called.
* Parameters:   None
* Return value: None
*
*****************************************************************************F*/
void TransmitRFData(void)
{
  static bool txDataBit = false;
  static bool lastTxBit;
  char data = 0;

  if (!txDataBit) // set/reset IO pin from the last interrupt
    setIO(RF_DATA_OUT);
  else
    clearIO(RF_DATA_OUT);

  // we've sent 8 bits of same value in a row, so insert transition
  if (bitCounter >= MAX_BITS_IN_A_ROW)
  {
    txDataBit = !lastTxBit;
    lastTxBit = txDataBit;
    bitCounter = 1;
  }
  else
  {
    // either use header or buffer
    if (transmitBytePointer < PT_HEADER_SIZE)
    {
      data = transmitterHeaderBuffer[transmitBytePointer];
    }
    else if (transmitBytePointer < transmitterPacketSize + PT_HEADER_SIZE)
    {
      data = transmitterPacketBuffer[transmitBytePointer - PT_HEADER_SIZE];
    }
    else
    {
      communicationStatus.transmitDelay = true;
      transmitDelayCounter = 0;
      communicationStatus.timeToTransmit = false;
    }

    // set or clear output
    txDataBit = (data & transmitBitPointer) > 0;

    if (txDataBit != lastTxBit)
    {
      lastTxBit = txDataBit;
      bitCounter = 1;
    }
    else
    {
      bitCounter++;
    }

    transmitBitPointer >>= 1;
    if (transmitBitPointer == 0)
    {
      transmitBitPointer = 0x80;
      ++transmitBytePointer;
      if (transmitBytePointer == PT_HEADER_SIZE)
        bitCounter = 0;  // start bit counter when header complete
    }
  }
}

/*F****************************************************************************
*
* Name: rfDataClockEdge()
* Description:  Called on transititions of the Chipcon GDO-1 (clock) pin
* Parameters:   None
* Return value: None
*
*****************************************************************************F*/
void rfDataClockEdge(void)
{
  //if we are in receive state
  if( !communicationStatus.timeToTransmit && !communicationStatus.transmitDelay )
    ReceiveRFData();
  //if we are in a transmit state
  else if ( !communicationStatus.transmitDelay )
    TransmitRFData();
  //if we are currently ending a transmission
  else if (communicationStatus.transmitDelay)
  {
    //insert 16 bits of delay at the end of our transmission
    if (transmitDelayCounter < CHIPCON_TRANSMIT_DELAY_BITS)
    {
      transmitDelayCounter++;
    }
    else
    {
      communicationStatus.transmitComplete = true;
      communicationStatus.transmitDelay = false;
    }
  }

  if (++RFFrameBitCounter >= SlotEND)
    RFFrameBitCounter = 0;

  decrementFrameTimeout();
}

/*F****************************************************************************
*
* Name: getWirelessReceiveBufferOffset()
* Description:  Will return the current offset into the receive buffer
* Parameters:   None
* Return value: next empty location in the buffer
*
*****************************************************************************F*/
byte getWirelessReceiveBufferOffset(void)
{
  return receiveBytePointer;
}

/*F****************************************************************************
*
* Name: setupWirelessTransmit()
* Description:  Will setup the RF chip for transmission
* Parameters:   None
* Return value: None
*
*****************************************************************************F*/
void setupWirelessTransmit(void)
{
  clearIOEnableInt(RF_DATA_CLK);
  sendChipconCommandStrobe(SIDLE); // put the RF Chip into Idle mode
  initStateForTransmit();   //init the communications state for transmission
  latchFrequencyError();
  sendChipconCommandStrobe(STX);  // change the RF chips mode to TX
  setIODirOut(RF_DATA_OUT); //set the data pin direction

    // Prepare for interrupts on the data clk line
  setIORisingInt(RF_DATA_CLK);
  clearIOIntFlag(RF_DATA_CLK);
  setIOEnableInt(RF_DATA_CLK);
}

/*F****************************************************************************
*
* Name: initializeCommunicationStatus()
* Description:  Will blank the current communications status
* Parameters:   None
* Return value: None
*
*****************************************************************************F*/
void initializeCommunicationStatus(void)
{
  communicationStatus.timeToTransmit = false;
  communicationStatus.patternDetected = false;
  communicationStatus.transmitComplete = false;
  communicationStatus.frameStartDetected = false;
  communicationStatus.spare = 0;  // saves a little codespace
}

/*F****************************************************************************
*
* Name: initStateForReceive()
* Description:  Will init the communication state for RX
* Parameters:   None
* Return value: None
*
*****************************************************************************F*/
void initStateForReceive(void)
{
  receiveBitPointer = 0x80;
  receiveBytePointer = 0;
  receiverPacketBuffer[0] = 0;
  communicationStatus.patternDetected = false;
  communicationStatus.frameStartDetected = false;
}

/*F****************************************************************************
*
* Name: initStateForTransmit()
* Description:  Will init the communications state for TX
* Parameters:   None
* Return value: None
*
*****************************************************************************F*/
void initStateForTransmit(void)
{
  transmitBytePointer = 0;
  transmitBitPointer = 0x40;
  communicationStatus.timeToTransmit = true;
}

/*F****************************************************************************
*
* Name: setupWirelessSleep()
* Description:  Will put the RF chip into sleep mode.
* Parameters:   None
* Return value: None
*
*****************************************************************************F*/
void setupWirelessSleep(void)
{
  clearIOEnableInt(RF_DATA_CLK);
  sendChipconCommandStrobe(SIDLE); // transition to idle to prepaire to sleep
  LED_OFF(RED_LED);
  sendChipconCommandStrobe(SPWD); // send the command to place the chip to sleep
}

/*F****************************************************************************
*
* Name: setupWirelessIdle()
* Description:  Changes the mode of the RF chip to Idle.
* Parameters:   None
* Return value: None
*
*****************************************************************************F*/
void setupWirelessIdle(void)
{
  clearIOEnableInt(RF_DATA_CLK); // disable data clk interrupt
  sendChipconCommandStrobe(SIDLE); // place RF chip into idle mode
}
