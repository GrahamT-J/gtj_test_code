/*H*****************************************************************************
Filename: chipconcommunication.h

Description: High level communication routines for the Chipcon radio

Copyright (c) Digital Monitoring Products Inc. 2004 - 2017. All rights reserved.
*****************************************************************************H*/

#ifndef CHIPCONCOMMUNICATION_H
#define CHIPCONCOMMUNICATION_H

/*----defines, typedefs, structs, unions, enums-------------------------------*/
typedef struct
{
  BITFIELD frameStartDetected   : 1;
  BITFIELD timeToTransmit       : 1;
  BITFIELD patternDetected      : 1;
  BITFIELD transmitComplete     : 1;
  BITFIELD transmitDelay        : 1;
  BITFIELD spare                : 3;
} CommunicationStatusFlags;

#define CHIPCON_TRANSMIT_DELAY_BITS 16  //transmit the recommened 2 bytes of dummy data at the end of a transmission

/*----data declarations-------------------------------------------------------*/
extern word RFFrameBitCounter;
extern CommunicationStatusFlags communicationStatus;

/*----function prototypes-----------------------------------------------------*/
extern byte getWirelessReceiveBufferOffset(void);
extern void setupWirelessTransmit(void);
extern void setupWirelessReceive(void);
extern void initializeCommunicationStatus(void);
extern void initStateForReceive(void);
extern void setupWirelessSleep(void);
extern void initRFChip(void);
extern void setupWirelessIdle(void);

#endif
