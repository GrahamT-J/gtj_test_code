/*H*****************************************************************************
Filename: transmitterprotocol.c

Description: High level processing for the 1100 wireless protocol

Copyright (c) Digital Monitoring Products Inc. 2009 - 2017. All rights reserved.
*****************************************************************************H*/

#include "DMP.h"
#include "sleep.h"
#include "TransmitterProtocol.h"
#include "WirelessCommunication.h"
#include "Inputs.h"

extern SlotNumber currentSlotNumber;

#define SOFTWARE_VERSION 1

typedef enum
{
  TMT_SERIAL_MESSAGE,
  TMT_ZONE_MESSAGE,
  TMT_CHECKIN_MESSAGE,
  TMT_NO_MESSAGE
} TransmitterMessageType;

uint8 currentTransmitterIndex;
bool secondTransmitterDisabled;      // we have received a NOT FOUND from the receiver

static void formatTransmitterMessage(TransmitterMessageType messageType);

static SlotTransmitterNumber myZoneNumber = INVALID_TRANSMITTER_NUMBER;
static SlotNumber myCheckinSlotNumber = INVALID_SLOT;
static unsigned short transmitterCrc;

static SlotNumber checkinSlotNumber[2];
static SlotTransmitterNumber zoneNumber[2];

/*F****************************************************************************
*
* Name: LoadCurrentTransmitterInfo()
* Description:  Load the zone number and checkin slot number for the currently
*               active transmitter into the active variables.
* Parameters:   None
* Return value: None
*
*****************************************************************************F*/
static void LoadCurrentTransmitterInfo (void)
{
  myZoneNumber = zoneNumber[currentTransmitterIndex];
  myCheckinSlotNumber = checkinSlotNumber[currentTransmitterIndex];
}

/*F****************************************************************************
*
* Name: SetZoneNumber()
* Description:  Save the specified transmitter number for the currently active
*               transmitter, and calculate the checkin slot number.
* Parameters:   transmitter number assigned by the receiver
* Return value: None
*
*****************************************************************************F*/
static void SetZoneNumber (SlotTransmitterNumber number)
{
  zoneNumber[currentTransmitterIndex] = number;
  checkinSlotNumber[currentTransmitterIndex] = zoneToSlotNumber(number);
  LoadCurrentTransmitterInfo();
}

/*F****************************************************************************
*
* Name: ClearBothZoneNumbers()
* Description:  Invalidate the zone number for both transmitters to force
*               resending the serial numbers to get updated programming.
* Parameters:   None
* Return value: None
*
*****************************************************************************F*/
static void ClearBothZoneNumbers (void)
{
  zoneNumber[0] = INVALID_TRANSMITTER_NUMBER;
  zoneNumber[1] = INVALID_TRANSMITTER_NUMBER;
  myZoneNumber  = INVALID_TRANSMITTER_NUMBER;
}

/*F****************************************************************************
*
* Name: SelectCurrentTransmitter()
* Description:  Load the zone number and checkin slot number for the specified
*               transmitter into the active variables.
* Parameters:   - transmitter index
* Return value: None
*
*****************************************************************************F*/
void SelectCurrentTransmitter (uint8 index)
{
  currentTransmitterIndex = index;
  LoadCurrentTransmitterInfo();
}

/*F****************************************************************************
*
* Name: IsTransmitterProgrammed()
* Description:  Check if the specified transmitter has a valid zone number from
*               the receiver.
* Parameters:   - transmitter index
* Return value: true if transmitter has been programmed
*
*****************************************************************************F*/
bool IsTransmitterProgrammed (uint8 index)
{
  return (zoneNumber[index] != INVALID_TRANSMITTER_NUMBER);
}

/*F****************************************************************************
*
* Name: BothTransmitterNumbersInvalid()
* Description:  Check if both transmitters have a valid zone number from
*               the receiver.
* Parameters:   None
* Return value: true if both transmitter numbers are invalid
*
*****************************************************************************F*/
bool BothTransmitterNumbersInvalid(void)
{
  return (zoneNumber[0] == INVALID_TRANSMITTER_NUMBER) && (zoneNumber[1] == INVALID_TRANSMITTER_NUMBER);
}

/*F****************************************************************************
*
* Name: finalizeTransmitterMessage()
* Description:  Sets CRC for transmitter message, including any extra
*               information that is expected, and sets transmitter packet
*               length.
* Parameters:   - length of the message
*               - set of of additional items to include in the checksum
* Return value: None
*
*****************************************************************************F*/
static void finalizeTransmitterMessage(char size, AdditionalCheckItems items)
{
  unsigned short crc = INITIAL_CRC;

  // first, start with any additional items
  if (items & ACI_HOUSE_CODE)
  {
    calcCRC16ForChar(houseCode, &crc);
  }
  if (items & ACI_ZONE_NUMBER)
  {
    calcCRC16ForChar(myZoneNumber >> 8, &crc);
    calcCRC16ForChar(myZoneNumber & 0xFF, &crc);
  }
  if (items & ACI_SERIAL_NUMBER)
  {
    uint32 serialNumber = ReadCurrentSerialNumber();
    calcCRC16ForChar(serialNumber & 0xFF, &crc);
    calcCRC16ForChar((serialNumber >> 8) & 0xFF, &crc);
    calcCRC16ForChar((serialNumber >> 16) & 0xFF, &crc);
  }
  encodeWirelessBuffer(transmitterPacketBuffer, size, crc);
  transmitterCrc = (transmitterPacketBuffer[size - 1] << 8) | transmitterPacketBuffer[size - 2];
  transmitterPacketSize = size;
}

/*F****************************************************************************
*
* Name: processReceivedAck()
* Description:  Decode the contents of the receive buffer and evaluate the
*               response sent by the receiver.  Because the ACK may include
*               additional data bytes, we return the length of the ACK to allow
*               further processing to skip over this extra data.
* Parameters:   - pointer to location to store length of the ACK
* Return value: Any of the RAS_* values representing the different ACK statuses
*
*****************************************************************************F*/
ReceivedAckStatus processReceivedAck(char* characterOffset)
{
  char bufferOffset = getWirelessReceiveBufferOffset();
  ReceivedAckStatus returnValue = RAS_NOTHING_TO_DO;
  ReceiverAck * const ackPointer = (ReceiverAck*)receiverPacketBuffer;

  if (bufferOffset >= sizeof(ReceiverAck))
  {
    switch (ackPointer->ackType)
    {
    case AT_NO_ACK:
      returnValue = RAS_GOOD_MESSAGE_NON_ACK;
      *characterOffset = sizeof(ReceiverNoAck);
      break;

    case AT_STANDARD_ACK:
      *characterOffset = sizeof(ReceiverStandardAck);
      if (bufferOffset >= *characterOffset)
      {
        if (decodeWirelessBuffer(receiverPacketBuffer, sizeof(ReceiverStandardAck), transmitterCrc))
          returnValue = RAS_GOOD_MESSAGE_ACK_RECEIVED;
        else
          returnValue = RAS_BAD_MESSAGE;
      }
      break;

    case AT_ACK_WITH_ZONE_NUMBER:
      *characterOffset = sizeof(ReceiverZoneNumberAck);
      if (bufferOffset >= *characterOffset)
      {
        ReceiverZoneNumberAck * const zoneAckPointer = (ReceiverZoneNumberAck*)receiverPacketBuffer;

        if (decodeWirelessBuffer(receiverPacketBuffer, sizeof(ReceiverZoneNumberAck), transmitterCrc) &&
            ((houseCode == INVALID_HOUSE_CODE) || (myZoneNumber == INVALID_TRANSMITTER_NUMBER)))
        {
          if (zoneAckPointer->found)
          {
            if (houseCode == INVALID_HOUSE_CODE)
              houseCode = zoneAckPointer->houseCode;

            SetZoneNumber((SlotTransmitterNumber)(zoneAckPointer->zoneLow + (zoneAckPointer->zoneHigh << 8)));

            // We sent a SN and got a zone number.  It could be a new receiver or updated programming, so we
            // should ask about the second SN again.
            secondTransmitterDisabled = false;

            returnValue = RAS_GOOD_MESSAGE_ACK_RECEIVED;
          }
          else
          {
            // Receiving a NOT FOUND for the second transmitter is not a failure if the first transmitter is in sync
            if (IsTransmitterProgrammed(0) && (currentTransmitterIndex == 1))
            {
              secondTransmitterDisabled = true;
              returnValue = RAS_CORRECT_RECEIVER_NO_PROGRAMMING;
            }
            else
            {
              returnValue = RAS_GOOD_MESSAGE_WRONG_RECEIVER;
            }
          }
        }
        else
        {
          returnValue = RAS_BAD_MESSAGE;
        }
      }
      break;

    case AT_VARIABLE_LENGTH_ACK:
      if (bufferOffset >= sizeof(ReceiverAck) + 1)
      {
        ReceiverVariableLengthAck * const reservedAckPointer = (ReceiverVariableLengthAck*)receiverPacketBuffer;
        *characterOffset = sizeof(ReceiverVariableLengthAck) - 1 + reservedAckPointer->numBytesInData;
        if (bufferOffset >= *characterOffset)
        {
          returnValue = RAS_BAD_MESSAGE;  // we don't know what the ack is, so it's bad
        }
      }
      break;

    default:
      returnValue = RAS_BAD_MESSAGE;
      break;
    }
  }

  return returnValue;
}

/*F****************************************************************************
*
* Name: setupTransmitterMessage()
* Description:  Determine what kind of message to send, based on the current
*               contents of the receive buffer.
* Parameters:   - start position of the message in the receive buffer
* Return value: Any of the TMS_* values, indicating what kind of message to send
*
*****************************************************************************F*/
TransmitterMessageStatus setupTransmitterMessage(char startOffset)
{
  char bufferOffset = getWirelessReceiveBufferOffset();
  ReceiverSlotStart* slotPointer;
  TransmitterMessageStatus status = TMS_NO_MESSAGE_SETUP;
  bool completeMessageReceived = false;
  bool alarmWakeup = getAlarmWakeup();
  SlotNumber slotNumber = INVALID_SLOT;
  TransmitterMessageType  messageTypeToSend = TMT_NO_MESSAGE;

  transmitterPacketSize = 0;

  if (bufferOffset >= sizeof(ReceiverNoMessageSlot) + startOffset)
  {
    slotPointer = ((ReceiverSlotStart*)(receiverPacketBuffer + startOffset));
    slotNumber = (SlotNumber)((slotPointer->slotNumberHigh << 8) | slotPointer->slotNumberLow);

    // if waking up in my checkin slot and the slot has a slottype that doesn't need a response
    // and we arn't waking for an alarm then go back to sleep
    if ((isAlarmSlotType(slotPointer->slotType) && (slotPointer->slotType != ST_GENERAL_ALARM)) &&
        (! alarmWakeup) &&
        ((slotNumber >= myCheckinSlotNumber) &&
         (slotNumber <= myCheckinSlotNumber + SLOTS_BETWEEN_CHECKIN_SLOTS)))
    {
      if (bufferOffset >= sizeof(ReceiverNoMessageSlot) + startOffset)
      {
        completeMessageReceived = true;
        if (decodeWirelessBuffer(receiverPacketBuffer, sizeof(ReceiverNoMessageSlot) + startOffset, INITIAL_CRC))
        {
          status = TMS_NO_MESSAGE_NEEDED;
        }
      }
    }
    else if (isAlarmSlotType(slotPointer->slotType) )//if slottype is <=15
    {
      char size = sizeof(ReceiverNoMessageSlot) + startOffset;
      if (isTwoByteAlarmSlotType(slotPointer->slotType))
      {
        size += 2;
      }
      if (bufferOffset >= size)
      {
        completeMessageReceived = true;

        // setup zone number or serial number alarm message
        if (decodeWirelessBuffer(receiverPacketBuffer, size, INITIAL_CRC))
        {
          // if not assigned a zone number, send serial number, otherwise send zone number
          if (myZoneNumber == INVALID_TRANSMITTER_NUMBER)
          {
            messageTypeToSend = TMT_SERIAL_MESSAGE;
          }
          else
          {
            messageTypeToSend = TMT_ZONE_MESSAGE;
          }

          status = TMS_MESSAGE_SETUP;
        }
      }
    }
    else if (slotPointer->slotType == ST_CHECKIN_NEED_REPLY) //if slottype is 16
    {
      if (bufferOffset >= sizeof(ReceiverNoMessageSlot) + startOffset)
      {
        completeMessageReceived = true;
        if (decodeWirelessBuffer(receiverPacketBuffer, sizeof(ReceiverNoMessageSlot) + startOffset, INITIAL_CRC))
        {
          // make sure we are in the right slot
          if ((slotNumber >= myCheckinSlotNumber) &&
              (slotNumber <= myCheckinSlotNumber + SLOTS_BETWEEN_CHECKIN_SLOTS))
          {
            messageTypeToSend = TMT_CHECKIN_MESSAGE;
            status = TMS_MESSAGE_SETUP;
          }
        }
      }
    }
          //if we are in our checkin slot and the slot type is called for us to update our programming
    else if( slotPointer->slotType == ST_CHECKIN_UPDATE_PROGRAMMING && (slotNumber >= myCheckinSlotNumber) &&
         (slotNumber <= myCheckinSlotNumber + SLOTS_BETWEEN_CHECKIN_SLOTS) )
    {
      messageTypeToSend = TMT_SERIAL_MESSAGE;
      ClearBothZoneNumbers();
      secondTransmitterDisabled = false;
    }
    else
    {
      // we don't know what this message is, so say we received it, status alone
      completeMessageReceived = true;
    }

    formatTransmitterMessage(messageTypeToSend);
  }

  if (completeMessageReceived == false)
  {
    status = TMS_STILL_WAITING;
  }
  else
  {
    currentSlotNumber = slotNumber;
  }
  return status;
}

/*F****************************************************************************
*
* Name: GetSlotsUntilCheckin()
* Description:  Calculate the number of slots from the current slot number to
*               the checkin number of the specified transmitter.  Note: If
*               SLEEP_TICKS_PER_HOP is no longer a power of 2, this will take
*               up much more code space.
* Parameters:   - Transmitter index
* Return value: Number of slots until the checkin slot
*
*****************************************************************************F*/
int16 GetSlotsUntilCheckin(uint8 index)
{
  SlotNumber slotDifference = (SlotNumber)(checkinSlotNumber[index] - currentSlotNumber);
  if (slotDifference <= 0)
  {
    slotDifference += TOTAL_SLOTS;
  }
  return slotDifference;
}

/*F****************************************************************************
*
* Name: resetLearnedInformation()
* Description:  Clear the transmitter's house code, slot number, and zone
*               numbers.  Forces the transmitter to resync with a receiver.
* Parameters:   None
* Return value: None
*
*****************************************************************************F*/
void resetLearnedInformation(void)
{
  houseCode = INVALID_HOUSE_CODE;
  currentSlotNumber = INVALID_SLOT;
  secondTransmitterDisabled = false;

  currentTransmitterIndex = 0;
  zoneNumber[0] = INVALID_TRANSMITTER_NUMBER;
  zoneNumber[1] = INVALID_TRANSMITTER_NUMBER;
  checkinSlotNumber[0] = INVALID_SLOT;
  checkinSlotNumber[1] = INVALID_SLOT;

  LoadCurrentTransmitterInfo();
}

/*F****************************************************************************
*
* Name: formatTransmitterMessage()
* Description:  Function will format the transmitterPacketBuffer according to
*               the passed message type.
* Parameters:   - type of message (serial number, zone, checkin)
* Return value: None
*
*****************************************************************************F*/
static void formatTransmitterMessage(TransmitterMessageType messageType)
{
  switch (messageType)
  {
    case (TMT_SERIAL_MESSAGE):
    {
      TransmitterAlarmSerialNumberZoneMessage* const sendPointer =
        (TransmitterAlarmSerialNumberZoneMessage*)transmitterPacketBuffer;

      uint32 serialNumber = ReadCurrentSerialNumber();
      sendPointer->messageType = TAT_ALARM_WITH_SERIAL_NUMBER;
      sendPointer->reserved = 0;
      sendPointer->serialNumberLow = serialNumber & 0xFF;
      sendPointer->serialNumberMiddle = (serialNumber >> 8) & 0xFF;
      sendPointer->serialNumberHigh = (serialNumber >> 16) & 0xFF;
      sendPointer->softwareVersion = SOFTWARE_VERSION;
      sendPointer->zoneMessage = (ZoneMessageByte)getInputState(currentTransmitterIndex);
      finalizeTransmitterMessage(sizeof(TransmitterAlarmSerialNumberZoneMessage), ACI_NOTHING);
      break;
    }
    case (TMT_ZONE_MESSAGE):
    {
      TransmitterAlarmZoneNumberZoneMessage* const sendPointer =
        (TransmitterAlarmZoneNumberZoneMessage*)transmitterPacketBuffer;

      sendPointer->messageType = TAT_ALARM_WITH_ZONE_NUMBER;
      sendPointer->zoneNumberHigh = (myZoneNumber >> 8) & 0xFF;
      sendPointer->zoneNumberLow = myZoneNumber & 0xFF;
      sendPointer->zoneMessage = (ZoneMessageByte)getInputState(currentTransmitterIndex);
      finalizeTransmitterMessage(sizeof(TransmitterAlarmZoneNumberZoneMessage),
          ACI_HOUSE_CODE | ACI_SERIAL_NUMBER);
      break;
    }
    case (TMT_CHECKIN_MESSAGE):
    {
      TransmitterCheckinMessage* const sendPointer =
          (TransmitterCheckinMessage*)transmitterPacketBuffer;

      sendPointer->zoneMessage = (ZoneMessageByte)getInputState(currentTransmitterIndex);
      finalizeTransmitterMessage(sizeof(TransmitterCheckinMessage),
          ACI_HOUSE_CODE | ACI_ZONE_NUMBER | ACI_SERIAL_NUMBER);
      break;
    }
    default:
    {
      break;
    }
  }
}

/*F****************************************************************************
*
* Name: ReadCurrentSerialNumber()
* Description:  Read the serial number for the current transmitter from flash.
* Parameters:   None
* Return value: The current serial number
*
*****************************************************************************F*/
uint32 ReadCurrentSerialNumber (void)
{
  if (currentTransmitterIndex == 0)
  {
    return *(const uint32*)(SERIAL_NUMBER_ADDRESS_1);
  }
  else
  {
    return *(const uint32*)(SERIAL_NUMBER_ADDRESS_2);
  }
}
