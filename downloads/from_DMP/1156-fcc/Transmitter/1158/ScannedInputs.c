/*H*****************************************************************************
Filename: scannedinputs.c

Description: Processes inputs by periodic scanning (vs interrupt driven)

Copyright (c) Digital Monitoring Products Inc. 2009 - 2017. All rights reserved.
*****************************************************************************H*/

#include "types.h"
#include "Sleep.h"
#include "transmitterprotocol.h"
#include "IOPortPins.h"
#include "Inputs.h"

static ZoneMessageByte steadyZoneState[2];
static ZoneMessageByte intermZoneState[2];
static bool inputHasChanged[2];

/*F****************************************************************************
*
* Name: ReadZoneInput()
* Description:  Use the multiplexer to read the current state of one zone.
* Parameters:   - zone address (0-7)
* Return value: true if zone is faulted
*
*****************************************************************************F*/
static bool ReadZoneInput(uint8 addr)
{
  // Set MUX_A0, MUX_A1, and MUX_A2
  P2OUT = (P2OUT & ~(BIT1 | BIT2 | BIT3)) | (addr << 1);

  // The multiplexer propagation delay is 12ns at 3.3v, one processor cycle
  // is 232ns (4.3008MHz).
  __delay_cycles(1);

  return getIO(ZONE);
}

/*F****************************************************************************
*
* Name: ScanInputsForChange()
* Description:  Check for changes on the 8 input zones.  Sets the
*               inputHasChanged variable if the steady state has changed.
* Parameters:   None
* Return value: None
*
*****************************************************************************F*/
static void ScanInputsForChange(void)
{
  static ZoneMessageByte newZoneState[2];

  newZoneState[0] = (ZoneMessageByte)0;
  newZoneState[1] = (ZoneMessageByte)0;

  // Read all 8 zones into the MB_ALARM_[0-3] bits of the 2 zone state bytes
  for (uint8 i = 0; i < 8; i++)
  {
    if (ReadZoneInput(i))
    {
      newZoneState[i / 4] |= (1 << (i % 4));
    }
  }

  for (uint8 i = 0; i < 2; i++)
  {
    // Wait for two consecutive readings with no change
    if (intermZoneState[i] != newZoneState[i])
    {
      intermZoneState[i] = newZoneState[i];
    }
    else if (steadyZoneState[i] != intermZoneState[i])
    {
      steadyZoneState[i] = intermZoneState[i];
      inputHasChanged[i] = true;
    }
  }
}

/*F****************************************************************************
*
* Name: rfDataClockEdge()
* Description:  Handles interrupts for the RF data clock input, which clocks
*               data to/from the radio.
* Parameters:   None
* Return value: None
*
*****************************************************************************F*/
extern void rfDataClockEdge(void);
#pragma vector = PORT1_VECTOR
#pragma type_attribute = __interrupt
void port1EdgeInterrupt(void)
{
  if (getIOIntFlag(RF_DATA_CLK))
  {
    clearIOIntFlag(RF_DATA_CLK);
    rfDataClockEdge();
  }
  else
  {
    P1IFG = 0;        // clear all edge interrupts
    P1IE &= RF_DATA_CLK_MASK; // disable all other interrupts
  }
}

/*F****************************************************************************
*
* Name: debounceInterrupt()
* Description:  Handle the debounce timer interrupt.  If not communicating
*               already, scans the zone inputs and checks if we need to wakeup.
*               If not, schedules another wakeup in another debounce time 
*               period.
* Parameters:   None
* Return value: None
*
*****************************************************************************F*/
#pragma vector = TIMERA0_VECTOR
#pragma type_attribute = __interrupt
void debounceInterrupt(void)
{
  TACCTL0 = 0;
  if (!startupDelayComplete)
  {
    __low_power_mode_off_on_exit();
  }
  else if (!getCurrentlyCommunicating()) // only process a change if not already communicating
  {
    ScanInputsForChange();
    if (inputHasChanged[0])
    {
      inputHasChanged[0] = false;
      if (wakeup(true, 0))
      {
        __low_power_mode_off_on_exit();
      }
    }
    else if (inputHasChanged[1])
    {
      inputHasChanged[1] = false;
      if (wakeup(true, 1))
      {
        __low_power_mode_off_on_exit();
      }
    }
    else
    {
      startInputDebounce(DEBOUNCE_TIME);
    }
  }
}

/*F****************************************************************************
*
* Name: initInputs()
* Description:  Initialize and start periodically reading the processor inputs.
* Parameters:   - if this is a hardReset (initial powerup or watchdog reset)
* Return value: None
*
*****************************************************************************F*/
void initInputs(bool hardReset)
{
  setIODirIn(ZONE);
  setIODirOut(MUX_A0);
  setIODirOut(MUX_A1);
  setIODirOut(MUX_A2);

  if (hardReset)
    StartupDelay();

  startInputDebounce(DEBOUNCE_TIME);
}

/*F****************************************************************************
*
* Name: startInputDebounce()
* Description:  Starts debounce timer so it will interrupt at debounce timer
*               plus the amount of time for it to start at an appropriate hop.
*
*               If SLEEP_TICKS_PER_HOP ever becomes a value that is not a power
*               of two, the modulo operator will take much longer and this will
*               probably need to change.
* Parameters:   - debounce time in timer ticks
* Return value: None
*
*****************************************************************************F*/
void startInputDebounce(short debounceTime)
{
  if (! (TACCTL0 & CCIE)) // if not already debouncing
  {
    unsigned short tempTar = TAR;
    TACCR0 = tempTar + debounceTime + // current time plus debounce time plus
             SLEEP_TICKS_PER_HOP -      // an exra hop minus
             WAKEUP_CLOCK_SLOP_TICKS -  // some padding for clock slop minus
             ((tempTar - sleepTime) % SLEEP_TICKS_PER_HOP); // how far off we currently are from start of hop
    TACCTL0 = CCIE;  // enable interrupt and set compare mode
  }
}

/*F****************************************************************************
*
* Name: getInputState()
* Description:  Function will return the current steady state for the zone.
* Parameters:   - transmitterIndex of which to return state
* Return value: zone state of the specified transmitter
*
*****************************************************************************F*/
ZoneMessageByte getInputState(uint8 transmitterIndex)
{
  ZoneMessageByte state = steadyZoneState[transmitterIndex];

  if (!getAlarmWakeup())
  {
    state |= MB_CHECKIN;
  }

  return state;
}
