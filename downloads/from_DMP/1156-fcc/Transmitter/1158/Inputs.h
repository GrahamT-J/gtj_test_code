/*H*****************************************************************************
Filename: inputs.h

Description: Miscellaneous hardware descriptions not included elsewhere

Copyright (c) Digital Monitoring Products Inc. 2009 - 2017. All rights reserved.
*****************************************************************************H*/

#ifndef INPUTS_H
#define INPUTS_H

#include "ScannedInputs.h"

#endif
