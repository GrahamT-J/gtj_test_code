/*H*****************************************************************************
Filename: msp430sfr.h

Description: Generic file that includes the appropriate header for the MSP430
             variant being used

Copyright (c) Digital Monitoring Products Inc. 2009 - 2017. All rights reserved.
*****************************************************************************H*/

#ifndef MSP430SFR_H
#define MSP430SFR_H

#include "msp430x21x1.h"

#endif
