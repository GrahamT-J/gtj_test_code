/*H*****************************************************************************
Filename: sleep.c

Description: Go-to-sleep routine and wakeup scheduling

Copyright (c) Digital Monitoring Products Inc. 2007 - 2017. All rights reserved.
*****************************************************************************H*/

#include "Sleep.h"
#include "TransmitterProtocol.h"
#include "Main.h"
#include "Ioportpins.h"
#include "Inputs.h"
#include "WirelessCommunication.h"

extern uint8 currentTransmitterIndex;
extern bool secondTransmitterDisabled;
extern SlotNumber currentSlotNumber;

typedef struct
{
  BITFIELD sleeping                 : 1;
  BITFIELD alarmWakeup              : 1;
  BITFIELD currentlyCommunicating   : 1;  // set if not done trying to send a message
  BITFIELD receiverFailed           : 1;
  BITFIELD receiveCommand           : 1;
  BITFIELD sleepingForCheckin       : 1;
  BITFIELD                          : 2;
  uint8 alarmTransmitterIndex;
  char wakeupAttempt;
} SleepStatus;

enum
{
  MAX_WAKEUP_ATTEMPTS = 60
};

static uint8 lastCheckin;
static SleepStatus sleepStatus;
static uint8 numTries;
word sleepTime = 0;

/*F****************************************************************************
*
* Name: getCurrentlyCommunicating()
* Description:  Check if we are attempting to communicate with the receiver,
*               including sleeping between retries.
* Parameters:   None
* Return value: true if we are currently communicating
*
*****************************************************************************F*/
bool getCurrentlyCommunicating(void)
{
  return sleepStatus.currentlyCommunicating;
}

/*F****************************************************************************
*
* Name: getSleeping()
* Description: Check if we are currently sleeping (used while processing timer
*              interrupts, which run while the main program flow is sleeping).
* Parameters:   None
* Return value: true if we are sleeping / waiting for a reason to transmit
*
*****************************************************************************F*/
bool getSleeping(void)
{
  return sleepStatus.sleeping;
}

/*F****************************************************************************
*
* Name: getAlarmWakeup()
* Description:  Check if this is an alarm wakeup (vs. checkin)
* Parameters:   None
* Return value: true, if the current wakeup is due to a zone state change
*
*****************************************************************************F*/
bool getAlarmWakeup(void)
{
  return sleepStatus.alarmWakeup;
}

/*F****************************************************************************
*
* Name: processSuccessfulWakeup()
* Description:  Called when we successfully communicated, this function clears
*               the receiver-failed state.
* Parameters:   None
* Return value: None
*
*****************************************************************************F*/
static void processSuccessfulWakeup(void)
{
  sleepStatus.wakeupAttempt = 0;
  sleepStatus.receiverFailed = false;
}

/*F****************************************************************************
*
* Name: PrepareToSleepUntilCheckin()
* Description:  Calculates the number of ticks until the next checkin slot (for
*               either transmitter) and configures the transmitter protocol
*               variables for the appropriate transmitter.
* Parameters:   None
* Return value: number of ticks till next checkin
*
*****************************************************************************F*/
static uint16 PrepareToSleepUntilCheckin(void)
{
  numTries = 0;

  bool isProgrammed[2];
  isProgrammed[0] = IsTransmitterProgrammed(0);
  isProgrammed[1] = IsTransmitterProgrammed(1);

  // Check if we need to send the serial number for tx1
  if ((isProgrammed[0]) && (!isProgrammed[1]) && (!secondTransmitterDisabled))
  {
    SelectCurrentTransmitter(1);
    return 3 * SLEEP_TICKS_PER_HOP;    
  }

  int16 slotsUntilCheckin[2];
  slotsUntilCheckin[0] = GetSlotsUntilCheckin(0);
  slotsUntilCheckin[1] = GetSlotsUntilCheckin(1);

  // Wake up for tx0 by default
  uint8 nextTx = 0;

  // If the checkin slot for tx1 is sooner than tx0, then plan to wake up for tx1
  if (isProgrammed[1] && (slotsUntilCheckin[1] < slotsUntilCheckin[0]))
  {
    nextTx = 1;
  }

  // If we are scheduling a checkin, but the last successful checkin was for the same transmitter,
  // we might have missed the other checkin slot, and should send in the zone state.
  if ((nextTx == 0) && (lastCheckin == 0) && isProgrammed[1])
  {
    sleepStatus.sleepingForCheckin = true;
    SelectCurrentTransmitter(1);
    return 3 * SLEEP_TICKS_PER_HOP;
  }
  if ((nextTx == 1) && (lastCheckin == 1) && isProgrammed[0])
  {
    sleepStatus.sleepingForCheckin = true;
    SelectCurrentTransmitter(0);
    return 3 * SLEEP_TICKS_PER_HOP;
  }

  sleepStatus.sleepingForCheckin = true;
  SelectCurrentTransmitter(nextTx);
  return slotsUntilCheckin[nextTx] * SLEEP_TICKS_PER_HOP;
}

/*F****************************************************************************
*
* Name: PrepareToSleepUntilRetry()
* Description:  Returns a standard or somewhat random sleep interval, based on
*               various bits of the serial number.  Starts out waiting up to
*               1/4 second and moves out to 2 seconds, if needed.
*
*               Try     Mask      Sleep from 1 cycle to ...
*               ---     ----      -------------------------
*               1       0x07      8
*               2       0x07      8
*               3       0x07      8
*               4       0x07      8
*               5       0x0F      16
*               6       0x1F      32
*               7       0x3F      64
*               8       0x3F
*               9       0x3F
*               10      0x3F
*               11      0x3F
*               12      0x3F
*               13      0x3F
*               14      0x3F
*               15      0x3F
*               16      0x3F
*
* Parameters:   Pointer to a flag indicating the retry attempts are exhausted
* Return value: Number of ticks to sleep
*
*****************************************************************************F*/
static uint16 PrepareToSleepUntilRetry(bool *timedOut)
{
  enum
  {
    MIN_WAIT_MASK   = 0x07,
    MAX_WAIT_MASK_NUM_BITS = 6,
    SERIAL_NUM_BITS = 24
  };
  char mask;
  char interval;

  if (numTries > SERIAL_NUM_BITS - MAX_WAIT_MASK_NUM_BITS)
  {
    resetLearnedInformation();
    *timedOut = true;
    return PrepareToSleepUntilCheckin();
  }

  *timedOut = false;

  if (numTries <= MAX_WAIT_MASK_NUM_BITS)
    mask = (1 << numTries);
  else
    mask = (1 << MAX_WAIT_MASK_NUM_BITS);

  mask--;
  mask |= MIN_WAIT_MASK;

  interval = ((ReadCurrentSerialNumber() >> numTries) & mask) + 1;

  ++numTries;

  return (interval * SLEEP_TICKS_PER_HOP);
}

/*F****************************************************************************
*
* Name: FindChannel()
* Description:  Calculates the channel we expect to wake up on based on the time
*               since we went to sleep.
* Parameters:   None
* Return value: None
*
*****************************************************************************F*/
static void FindChannel(void)
{
  char channelSkip;

  ENTER_CRITICAL_SECTION();

  // calculate the number of channels that have passed since we went to sleep
  channelSkip = (sleepTime % SLEEP_TICKS_PER_HOP_ROUND) / SLEEP_TICKS_PER_HOP;
  // add the number of skipped channels into the current channel plus one to account
  // for the fact that we are waking in the slot prior to the indented slot due to us
  // accounting for clock slop.
  currentChannel = (unsigned short)(currentChannel + ((channelSkip+1) * houseCode)) % NUM_HOP_CHANNELS;

  EXIT_CRITICAL_SECTION();

  CCR2 = TAR + RFBitTime;
  CCTL2 = OUTMOD_5+CM0+CCIE; // CC2 = Compare, Reset
}

/*F****************************************************************************
*
* Name: receiverFailed()
* Description:  Returns whether we have been unsuccessful communicating with
*               the receiver for an extended period of time.
* Parameters:   None
* Return value: true if we are in receiver-failed mode
*
*****************************************************************************F*/
bool receiverFailed(void)
{
  return sleepStatus.receiverFailed;
}

/*F****************************************************************************
*
* Name: goToSleep()
* Description:  Calculates the sleep interval based upon the status of the last
*               transmission, and puts the processor to sleep.  When a timer or
*               alarm causes the processor to wakeup, we setup the channel and
*               transmitter number for the next communication attempt.
* Parameters:   - The status (SUCCESS/FAILURE/RETRY) of the last transmission
* Return value: None
*
*****************************************************************************F*/
void goToSleep(TransmitStatus status)
{
  unsigned short sleepInterval = 0;
  char slotOffset;
  bool timedOut = false;
  bool hardSleep = true;  // set if we went to sleep for more than a short retry time

  ENTER_CRITICAL_SECTION();

  switch (status)
  {
    case TS_SUCCESS:
      processSuccessfulWakeup();
      sleepStatus.currentlyCommunicating = false;
      sleepInterval = PrepareToSleepUntilCheckin();
      break;

    case TS_FAILURE:
      resetLearnedInformation();
      sleepStatus.currentlyCommunicating = false;
      sleepInterval = PrepareToSleepUntilCheckin();
      timedOut = true;
      break;

    case TS_RETRY:
      sleepInterval = PrepareToSleepUntilRetry(&timedOut);
      if (timedOut)
      {
        sleepStatus.currentlyCommunicating = false;
      }
      hardSleep = false;
      break;
  }

  initializeCommunicationStatus();
  processMainShutdown();

  setupInputs();

  TACTL = TASSEL_1 + // ACLK
          MC_2 + // continuous
          SLEEP_TIMERA_DIVIDER_SETTING * ID0;

  // Using RFFrameBitCounter, calculate the number of sleep ticks are into the current slot
  slotOffset = RFFrameBitCounter * SLEEP_TICKS_PER_HOP / FB_TOTAL;
  // Since RFFrameBitCounter ranges from 150 to 300 in RX and 0 to 150 in TX we need
  // to normalize slotOffset so it is truely the number of sleep ticks into the current slot
  slotOffset = ((slotOffset + (SLEEP_TICKS_PER_HOP / 2)) % SLEEP_TICKS_PER_HOP);
  // Subtract slotOffset from our current time to get the reference for the begining
  // of the current slot, record this as the time we went to sleep.
  sleepTime = TAR - slotOffset;
  // Using the reference of the begining of the current slot, calculate how long
  // we need to sleep and subtract clock slop to assure we wake up early.
  CCR2 = sleepTime + sleepInterval - WAKEUP_CLOCK_SLOP_TICKS;
  CCTL2 = CCIE;   // compare timer with interrupt enabled
  sleepStatus.sleeping = true;
  startInputDebounce(DEBOUNCE_TIME);  // schedule wakeup after debounce time, in case we have changed states
  WDTCTL = WDTPW | WDTHOLD | WDTCNTCL | WDTSSEL;  // stop watchdog
  // the call to LPM3 must follow this intstruction for proper operation
  EXIT_CRITICAL_SECTION();
  // with the MSP430 the instruction following the EINT instruction is guaranteed to
  // execute before interrupts are enabled.
  (void)LPM3;           // Enter LPM3 until timer or contact change
  _NOP();         // for debugger
  WDTCTL = WDTPW | WDTCNTCL | WDTSSEL;  // start watchdog, reset, and use ACLK

  // start over almost fresh, since we are not communicating
  if (timedOut)
  {
    resetHardware(false);
  }

  if (sleepStatus.alarmWakeup)
  {
    SelectCurrentTransmitter(sleepStatus.alarmTransmitterIndex);
  }
  else if (sleepStatus.sleepingForCheckin)
  {
    lastCheckin = currentTransmitterIndex;
  }
  sleepStatus.sleepingForCheckin = false;

  FindChannel();
  processMainWakeup(hardSleep);
}

/*F****************************************************************************
*
* Name: wakeup()
* Description:  Checks if we are allowed to wakeup, based on the type of wakeup
*               (checkin or alarm) and whether we are in receiver-failed mode.
*               If we have failed to communicate with the receiver for an hour,
*               only allow non-alarm wakeups once every hour thereafter.
* Parameters:   - whether there is an alarm (zone state change) to report
*               - the transmitter index [0..1] that needs to report an alarm
* Return value: Whether the wakeup is allowed
*
*****************************************************************************F*/
bool wakeup(bool isAlarmWakeup, uint8 transmitterIndex)
{
  bool allowWakeup = true;

  if (!sleepStatus.currentlyCommunicating)
  {
    sleepStatus.alarmWakeup = isAlarmWakeup;
    sleepStatus.alarmTransmitterIndex = transmitterIndex;
    if (++sleepStatus.wakeupAttempt >= MAX_WAKEUP_ATTEMPTS)
    {
      sleepStatus.wakeupAttempt = 0;
      sleepStatus.receiverFailed = true;
    }
  }

  if ((! sleepStatus.alarmWakeup) &&
      (sleepStatus.receiverFailed) &&
      (sleepStatus.wakeupAttempt > 0))
  {
    allowWakeup = false;
  }

  if (allowWakeup)
  {
    sleepTime = TAR - sleepTime;

    sleepStatus.sleeping = false;
    sleepStatus.currentlyCommunicating = true;
  }

  return allowWakeup;
}
