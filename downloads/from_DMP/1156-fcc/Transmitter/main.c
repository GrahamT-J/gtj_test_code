/*H*****************************************************************************
Filename: main.c

Description: Main transmitter control logic

Copyright (c) Digital Monitoring Products Inc. 2009 - 2017. All rights reserved.
*****************************************************************************H*/

#include "types.h"
#include "MSP430SFR.h"
#include "IOPortPins.h"
#include "Hop.h"
#include "Inputs.h"
#include "WirelessCommunication.h"
#include "Sleep.h"
#include "TransmitterProtocol.h"

typedef enum
{
  FS_JUST_WOKEUP,
  FS_WAITING_FOR_FIRST_PATTERN,
  FS_WAITING_FOR_FIRST_ACK,
  FS_WAITING_FOR_SLOT_DEFINITION,
  FS_TRANSMITTING,
  FS_WAITING_FOR_ACK_PATTERN,
  FS_WAITING_FOR_ACK
} FrameStatus;

typedef enum
{
  WM_OFF,
  WM_RECEIVE,
  WM_TRANSMIT,
  WM_IDLE
} WirelessMode;

typedef enum
{
  RS_GOOD_SYNC,
  RS_TEMP_SYNC,
  RS_BAD_SYNC,
  RS_FORCE_NO_SYNC
} ReceiverSyncStatus;

// SyncCounter Description
//  SyncCounter can range from -RESYNC_FAILURE to +GOOD_SYNC_COUNT with a negative number meaning the transmitter is out of sync.  A positive number will signify the transmitter
//  is in some form of sync with a receiver and a +GOOD_SYNC_COUNT means that we are in absolute sync with our programmed receiver.  When a message from a wrong receiver is
//  received the count will be taken to INITIAL_SYNC_COUNT.  If we receive a message that we didn't understand the will either decrement the count by one if the count is positive
//  take the count to BUMP_SYNC_COUNT if the current count is negative.  If a frame timeout occurs the number will be decremented with the limit of either RESYNC_FAILUER or
//  RECEIVER_FAIL_RESYNC_FAILURE based upon if the receiver is currently in failure.
typedef enum
{
  INITIAL_SYNC_COUNT = 0,
  BUMP_SYNC_COUNT = 1,
  GOOD_SYNC_COUNT = 5,
  RESYNC_TRIES    = 64,  // Set to allow a transmitter to listen through all hop channels (53) but still be a power of two.  MUST BE A POWER OF TWO!!
  RESYNC_FAILURE  = -(3 * RESYNC_TRIES) - 1, // Set to allow three times of RESYNC_TRIES but minus 1 to make it not a power of RESYNC_TRIES.  MUST NOT BE A POWER OF RESYNC_TRIES
  RECEIVER_FAIL_RESYNC_FAILURE = -RESYNC_TRIES - 1 // Set to allow a single RESYNC_TRIES but minus 1 to make it not a power of RESYNC_TRIES.  MUST NOT BE A POWER OF RESYNC_TRIES
} SyncCounter;

typedef enum
{
  DEFAULT_WAKEUP_FRAME_TIMEOUT = FB_TOTAL,
  STANDARD_FRAME_TIMEOUT = FB_TOTAL,
} FrameTimeout;

// lastResortTimer is really just a "safety net" to make sure we timeout after a reasonable amount of time
typedef enum
{
  LAST_RESORT_TIMEOUT = 200 // number of frame timeouts before we give up
} LastResortTimer;

typedef enum
{
  MSS_AWAKE,
  MSS_SUCCESS,
  MSS_FAILURE,
  MSS_RETRY
} MainSleepState;

SlotNumber currentSlotNumber;

static FrameStatus frameStatus = FS_JUST_WOKEUP;
static SyncCounter syncCounter;
static char hopOffset;
static FrameTimeout frameTimeout;
static LastResortTimer lastResortTimer;
static MainSleepState mainSleepState;
static FrequencyMode primaryFrequencyMode = STANDARD_FREQUENCY_MODE;
static FrequencyMode currentFrequencyMode = STANDARD_FREQUENCY_MODE;

const char copyrightString[] = "(c) 2017 DMP";
#pragma required = copyrightString

/*F****************************************************************************
*
* Name: setWirelessMode()
* Description:  Switch the radio between Off, Idle, Transmit, and Receive modes.
* Parameters:   - wireless operating mode
* Return value: None
*
*****************************************************************************F*/
static void setWirelessMode(WirelessMode mode)
{
  if (mode == WM_OFF)
  {
    setupWirelessSleep();
  }
  else if (mode == WM_IDLE)
  {
    setupWirelessIdle();
  }
  else
  {
    LED_ON(RED_LED);  //LED ON
    if (mode == WM_RECEIVE)
    {
      setupWirelessIdle();
      setupWirelessReceive();
    }
    else
    {
      setupWirelessTransmit();
    }
  }
}

/*F****************************************************************************
*
* Name: followHop()
* Description:  Follow along with the receiver by hopping to the next channel
*               and switching to receive mode.
* Parameters:   None
* Return value: None
*
*****************************************************************************F*/
static void followHop(void)
{
  currentChannel = addChannel(currentChannel, hopOffset);
  SetFrequency(currentChannel);
  setWirelessMode(WM_RECEIVE);
  currentSlotNumber = nextSlot(currentSlotNumber);
}

/*F****************************************************************************
*
* Name: resetState()
* Description:  Reset the receive data processing state to prepare for the
*               next slot.
* Parameters:   None
* Return value: None
*
*****************************************************************************F*/
static void resetState(void)
{
  communicationStatus.frameStartDetected = false;
  frameStatus = FS_WAITING_FOR_FIRST_PATTERN;
}

/*F****************************************************************************
*
* Name: resyncFailCount()
* Description:  Return the sync counter value at which to go to MSS_FAILURE.
* Parameters:   None
* Return value: a sync counter value
*
*****************************************************************************F*/
static SyncCounter resyncFailCount()
{
  if (receiverFailed())
  {
    return RECEIVER_FAIL_RESYNC_FAILURE;
  }
  return RESYNC_FAILURE;
}

/*F****************************************************************************
*
* Name: setReceiverSync()
* Description:  Sets the synchronization status.  We are either in sync with
*               the receiver or not.  Once we get good sync, we are in sync
*               several tries (GOOD_SYNC_COUNT).  Note:  This is somewhat
*               tricky, so be careful with it.
* Parameters:   - sync status
* Return value: None
*
*****************************************************************************F*/
static void setReceiverSync(ReceiverSyncStatus status)
{
  switch (status)
  {
  case RS_GOOD_SYNC:
    syncCounter = GOOD_SYNC_COUNT;
    hopOffset = ((ReceiverAck*)receiverPacketBuffer)->houseCode;
    break;

  case RS_TEMP_SYNC:
    if (syncCounter < GOOD_SYNC_COUNT)  // we get at least one try
    {
      syncCounter++;
      if (syncCounter <= 0)
      {
        syncCounter = (SyncCounter)1;
        // only set hop offset if we were not previously in sync
        hopOffset = ((ReceiverAck*)receiverPacketBuffer)->houseCode;
      }
    }
    break;

  case RS_BAD_SYNC:
    if (syncCounter > resyncFailCount())
    {
      --syncCounter;
    }
    if (syncCounter <= 0)
    {
      hopOffset = 1; // Hop if transmitter fails to communicate
    }
    break;

  case RS_FORCE_NO_SYNC:
    syncCounter = INITIAL_SYNC_COUNT;
    hopOffset = 0;
    break;
  }
}

/*F****************************************************************************
*
* Name: processHop()
* Description:  Depending upon whether or not we are in sync and how long we
*               have not been in sync, we determine whether to hop, stay, or
*               timeout.
* Parameters:   None
* Return value: None
*
*****************************************************************************F*/
static void processHop(void)
{
  if ((syncCounter > BUMP_SYNC_COUNT) ||
      ((syncCounter & (RESYNC_TRIES - 1)) == 0) && (syncCounter < 0))
  {
    setWirelessMode(WM_IDLE);
    followHop();
  }
  // if we are just to go out of sync, bump a little bit in case we got off slot somehow
  else if (syncCounter == BUMP_SYNC_COUNT)
  {
    hopOffset = (unsigned short)(hopOffset * 4) % NUM_HOP_CHANNELS;
    setWirelessMode(WM_IDLE);
    followHop();
  }
  else if (syncCounter <= resyncFailCount())
  {
    mainSleepState = MSS_FAILURE;
  }
  // otherwise, stay on channel
}

/*F****************************************************************************
*
* Name: setFrameTimeout()
* Description:  Set the frame timeout based on where we are in the frame.
* Parameters:   - current frame status
* Return value: None
*
*****************************************************************************F*/
static void setFrameTimeout(FrameStatus status)
{
  if (status == FS_JUST_WOKEUP)
  {
    // timeout for first sync.  Longer than a standard slot for clock slop
    frameTimeout = DEFAULT_WAKEUP_FRAME_TIMEOUT;
  }
  else if (status == FS_WAITING_FOR_ACK_PATTERN)
  {
    // time before we can expect to see sync + a little extra
    frameTimeout = (FrameTimeout)(SlotSYNC - RFFrameBitCounter + 10);
  }
  else  // standard timeout, usuall as a saftety net
  {
    frameTimeout = STANDARD_FRAME_TIMEOUT;
  }
}

/*F****************************************************************************
*
* Name: setupFrameStatus()
* Description:  Transition to the specified frame status.
* Parameters:   - next frame status
* Return value: None
*
*****************************************************************************F*/
static void setupFrameStatus(FrameStatus status)
{
  // decrement last resort timeout every retry and every transmit
  if ((status == FS_TRANSMITTING) || (status == FS_WAITING_FOR_FIRST_PATTERN))
  {
    if (--lastResortTimer == 0)
    {
      mainSleepState = MSS_FAILURE;
    }
  }
  setFrameTimeout(status);
  frameStatus = status;
}

/*F****************************************************************************
*
* Name: processFrameTimeout()
* Description:  Check if the frame timeout has expired, and restart listening
*               for the receiver if so.
* Parameters:   None
* Return value: None
*
*****************************************************************************F*/
static void processFrameTimeout(void)
{
  if (frameTimeout == 0)
  {
    if ((frameStatus == FS_WAITING_FOR_FIRST_PATTERN) || (frameStatus == FS_JUST_WOKEUP))
    {
      processHop();
      setReceiverSync(RS_BAD_SYNC);  // we are not in sync
    }
    setupFrameStatus(FS_WAITING_FOR_FIRST_PATTERN);
  }
}

/*F****************************************************************************
*
* Name: SwitchFrequencyModes()
* Description:  Toggles between standard and low-interference frequency modes.
*               Note: Reassigns the current frequency mode.
* Parameters:   None
* Return value: None
*
*****************************************************************************F*/
static void SwitchFrequencyModes(void)
{
  if (currentFrequencyMode == STANDARD_FREQUENCY_MODE)
    currentFrequencyMode = LOW_INTERFERENCE_FREQUENCY_MODE;
  else
    currentFrequencyMode = STANDARD_FREQUENCY_MODE;

  SetFrequencyMode(currentFrequencyMode);
}

/*F****************************************************************************
*
* Name: decrementFrameTimeout()
* Description:  Called every RF bit time to decrement the frame timeout.
* Parameters:   None
* Return value: None
*
*****************************************************************************F*/
void decrementFrameTimeout(void)
{
  if (frameTimeout > 0)
  {
    frameTimeout--;
  }
}

/*F****************************************************************************
*
* Name: processMainShutdown()
* Description:  Turn off the radio in preparation for going to sleep.
* Parameters:   None
* Return value: None
*
*****************************************************************************F*/
void processMainShutdown(void)
{
  setWirelessMode(WM_RECEIVE);
  setWirelessMode(WM_OFF);
}

/*F****************************************************************************
*
* Name: processMainWakeup()
* Description:  Initialize communication state after waking up from sleep.  If
*               we are waking up from a hard sleep (anything other than a
*               retry), also reset counters used to implement retries.
* Parameters:   - hard-sleep flag
* Return value: None
*
*****************************************************************************F*/
void processMainWakeup(bool hardSleep)
{
  resetState();
  RFFrameBitCounter = 0;
  setupFrameStatus(FS_JUST_WOKEUP);
  setWirelessMode(WM_IDLE);
  if (hardSleep)  // we just wokeup from a "hard sleep", so reset fail timers
  {
    if (syncCounter < INITIAL_SYNC_COUNT) // if "way" out of sync, make only slightly out of sync
    {
      syncCounter = INITIAL_SYNC_COUNT;
    }
    lastResortTimer = LAST_RESORT_TIMEOUT;
  }
  SetFrequency(currentChannel);
  initializeCommunicationStatus();
  setWirelessMode(WM_RECEIVE);
}

/*F****************************************************************************
*
* Name: resetHardware()
* Description:  Reset the hardware.
* Parameters:   - hard-reset flag (power on or watchdog reset)
* Return value: None
*
*****************************************************************************F*/
void resetHardware(bool hardReset)
{
  initSys(hardReset);
  _EINT();
  processMainWakeup(true);
}

/*F****************************************************************************
*
* Name: main()
* Description:  The main loop.
* Parameters:   None
* Return value: None
*
*****************************************************************************F*/
__task void main(void)
{
  static char characterOffset;
  resetLearnedInformation();
  resetHardware(true);

  while (true)
  {
    processFrameTimeout();

    switch (mainSleepState)
    {
    case MSS_SUCCESS:
      // Keep track of the last frequency mode communication was established
      primaryFrequencyMode = currentFrequencyMode;
      goToSleep(TS_SUCCESS);
      break;

    case MSS_FAILURE:
      if (BothTransmitterNumbersInvalid())
      {
        // If primary frequency mode toggle modes and try again before going to sleep
        if (currentFrequencyMode == primaryFrequencyMode)
        {
          WDTCTL = WDTPW | WDTCNTCL | WDTSSEL; // Kick watchdog since starting over
          SwitchFrequencyModes();
          processMainWakeup(true);
        }
        else
        {
          SwitchFrequencyModes();
          goToSleep(TS_FAILURE);
        }
      }
      else
      {
        goToSleep(TS_FAILURE);
      }
      break;

    case MSS_RETRY:
      goToSleep(TS_RETRY);
      break;

    // do nothing if awake or any other state
    }
    mainSleepState = MSS_AWAKE;

    switch (frameStatus)
    {
    case FS_JUST_WOKEUP:
    case FS_WAITING_FOR_FIRST_PATTERN:
      if (communicationStatus.frameStartDetected)
      {
        setupFrameStatus(FS_WAITING_FOR_FIRST_ACK);
      }
      break;

    case FS_WAITING_FOR_FIRST_ACK:
      if (processReceivedAck(&characterOffset) != RAS_NOTHING_TO_DO)
      {
        setupFrameStatus(FS_WAITING_FOR_SLOT_DEFINITION);
      }
      break;


    case FS_WAITING_FOR_SLOT_DEFINITION:
      {
        TransmitterMessageStatus slotDefinitionStatus;
        slotDefinitionStatus = setupTransmitterMessage(characterOffset);

        switch (slotDefinitionStatus)
        {
        case TMS_MESSAGE_SETUP:
          setWirelessMode(WM_TRANSMIT);
          setupFrameStatus(FS_TRANSMITTING);
          setReceiverSync(RS_GOOD_SYNC);
          break;

        case TMS_NO_MESSAGE_NEEDED:
          setReceiverSync(RS_GOOD_SYNC);
          mainSleepState = MSS_SUCCESS;
          break;

        case TMS_STILL_WAITING:  // do nothing
          break;

        case TMS_NO_MESSAGE_SETUP:
          setReceiverSync(RS_TEMP_SYNC);
          setWirelessMode(WM_IDLE);
          followHop();
          resetState();
          break;

        default:
          resetState();
          break;
        }
      }
      break;

    case FS_TRANSMITTING:
      if (communicationStatus.transmitComplete)
      {
        setWirelessMode(WM_IDLE);
        communicationStatus.transmitComplete = false;
        initStateForReceive();
        followHop();
        setupFrameStatus(FS_WAITING_FOR_ACK_PATTERN);
        communicationStatus.frameStartDetected = false;
      }
      break;

    case FS_WAITING_FOR_ACK_PATTERN:
      if (communicationStatus.frameStartDetected)
      {
        setupFrameStatus(FS_WAITING_FOR_ACK);
      }
      break;

    case FS_WAITING_FOR_ACK:
      {
        ReceivedAckStatus slotAckStatus;
        slotAckStatus = processReceivedAck(&characterOffset);

        switch (slotAckStatus)
        {
        case RAS_GOOD_MESSAGE_ACK_RECEIVED:
          applyFrequencyErrorOffset();
          mainSleepState = MSS_SUCCESS;
          break;

        case RAS_GOOD_MESSAGE_NON_ACK:
          mainSleepState = MSS_RETRY;
          break;

        case RAS_BAD_MESSAGE:
          setupFrameStatus(FS_WAITING_FOR_FIRST_ACK);  // try again immediately
          break;

        case RAS_NOTHING_TO_DO:
          break;

        // Heard from the correct receiver but there was no programming so shut down immediately
        case RAS_CORRECT_RECEIVER_NO_PROGRAMMING:
          mainSleepState = MSS_SUCCESS;
          break;

        case RAS_GOOD_MESSAGE_WRONG_RECEIVER:
        default:
          setReceiverSync(RS_FORCE_NO_SYNC);
          resetState();
          break;
        }
      }
      break;
    }
  }   // loop forever
}
