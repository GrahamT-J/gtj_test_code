/*H*****************************************************************************
FILENAME: FCCMode.c

DESCRIPTION:

NOTES:

Copyright (c) Digital Monitoring Products Inc. 2009.  All rights reserved.

*****************************************************************************H*/

#include "FCCMode.h"
#include "chipcon.h"
#include "hop.h"
#include "BitMacros.h"
#include "ioportpins.h"

static void FCCButton1(void);
static void FCCButton2(void);
static void FCCButton3(void);
static void FCCButton4(void);
static void FCCButton5(void);
static void FCCButton6(void);
static void FCCButton7(void);
static void FCCButton8(void);
static void FCCButton9(void);
static void FCCButton0(void);

static void incrementChannel(void);
static void decrementChannel(void);
static void dataOn(void);
static void dataOff(void);
static void rfDataClockEdgeFCC(void);
static void SwitchFrequencyModes(void);

static FrequencyMode currentFrequencyMode = STANDARD_FREQUENCY_MODE;
static char dataPattern[6];
static char dataPointer = 0;
static char bitPointer = 0x01;
static unsigned int syncBitCounter = 0;
static unsigned int patternRegister = 0;
static bool TX = false;
//static bool BERDone = false;
static byte BERCounter = 0;

/*******************************************************************************

DESCRIPTION:


NOTES:

*******************************************************************************/
void PORT1_FCC_EDGE_INTERRUPT(void)
{
  if (getIOIntFlag(RF_DATA_CLK))
  {
    clearIOIntFlag(RF_DATA_CLK);
    rfDataClockEdgeFCC();
  }
}

/*******************************************************************************

DESCRIPTION: Use key presses to alter keypad operation in order to provide
  functionality needed for FCC tests.

NOTES: If the fcn doesn't use the key, it returns it to the caller
   so it can be processed normally. Otherwise, it returns an altered key value.

*******************************************************************************/
unsigned char ProcessFCCKeypress(unsigned char keyPressed)
{
  switch (keyPressed)
  {
    case 0x04:      // '1'
      FCCButton1();
      keyPressed++;
      break;
    case 0x05:      // '2'
      FCCButton2();
      keyPressed++;
      break;
    case 0x06:      // '3'
      FCCButton3();
      keyPressed++;
      break;
    case 0x08:      // '4'
      FCCButton4();
      keyPressed++;
      break;
    case 0x09:      // '5'
      FCCButton5();
      keyPressed++;
      break;
    case 0x0A:      // '6'
      FCCButton6();
      keyPressed++;
      break;
    case 0x0C:      // '7'
      FCCButton7();
      keyPressed++;
      break;
    case 0x0D:      // '8'
      FCCButton8();
      keyPressed++;
      break;
    case 0x0E:      // '9'
      FCCButton9();
      keyPressed++;
      break;
    case 0x0F:      // '0'
      FCCButton0();
      keyPressed++;
      break;
    default:
      break;
  }
  return keyPressed;
}

/*******************************************************************************

DESCRIPTION:


NOTES:

*******************************************************************************/
static void FCCButton1()  // IDLE
{
  dataOff();
  sendChipconCommandStrobe(SIDLE);
}

static void FCCButton2()  // TX
{
  dataOff();
  sendChipconCommandStrobe(SIDLE);
  sendChipconCommandStrobe(STX);
}

static void FCCButton3()  // RX
{
  dataOff();
  sendChipconCommandStrobe(SIDLE);
  sendChipconCommandStrobe(SRX);
  TX = false;
}

static void FCCButton4()
{
}

static void FCCButton5()  // Modulation on
{
  dataOn();
}

static void FCCButton6()  // Modulation off
{
  dataOff();
}

static void FCCButton7()
{
}

static void FCCButton8()  // Switch frequency modes
{
  SwitchFrequencyModes();
}

static void FCCButton9()  // Decrement channel
{
  decrementChannel();
}

static void FCCButton0()  // Increment channel
{
  incrementChannel();
}

/*******************************************************************************

DESCRIPTION:


NOTES:

*******************************************************************************/
static void incrementChannel(void)
{
  sendChipconCommandStrobe(SIDLE);
  if (++currentChannel > 53)
    currentChannel = 0;
  sendChannel(currentChannel);
  sendChipconCommandStrobe(STX);
}

/*******************************************************************************

DESCRIPTION:


NOTES:

*******************************************************************************/
static void decrementChannel(void)
{
  sendChipconCommandStrobe(SIDLE);
  sendChannel(--currentChannel);
  sendChipconCommandStrobe(STX);
}

/*******************************************************************************

DESCRIPTION:


NOTES:

*******************************************************************************/
static void dataOn (void)
{
  char i;
//#ifndef DMP_FCC_TEST
//  sendString("ata on\n\r");
//#endif
  clearIOEnableInt(RF_DATA_CLK);
  sendChipconCommandStrobe(SIDLE); // put the RF Chip into Idle mode
  writeChipconConfigRegister(MDMCFG2, 0x00);
  writeChipconConfigRegister(DEVIATN, 0x35);
  setIODirOut(RF_DATA_OUT);

  for( i = 0; i<6; i++)
  {
    dataPattern[i] = 0x55;
  }

  bitPointer = 0x01;
  dataPointer = 0;
  TX = true;
  sendChipconCommandStrobe(STX);
  // Prepare for interrupts on the data clk line
  clearIOIntFlag(RF_DATA_CLK);
  setIORisingInt(RF_DATA_CLK);
  setIOEnableInt(RF_DATA_CLK);
}

/*******************************************************************************

DESCRIPTION:


NOTES:

*******************************************************************************/
static void dataOff(void)
{
//#ifndef DMP_FCC_TEST
//  sendString("ata off\n\r");
//#endif
  clearIOEnableInt(RF_DATA_CLK);
  writeChipconConfigRegister(MDMCFG2, 0x30);
  writeChipconConfigRegister(DEVIATN, 0x00);
}

/*******************************************************************************

DESCRIPTION:


NOTES:

*******************************************************************************/
static void rfDataClockEdgeFCC(void)
{
  bool dataBit = getIO(RF_DATA_IN) > 0;  // read state of data ASAP

  if (TX)
  {
    dataBit = dataPattern[dataPointer] & bitPointer;

    if( dataBit )
      setIO(RF_DATA_OUT);
    else
      clearIO(RF_DATA_OUT);

    bitPointer <<= 1;
    if( bitPointer == 0 )
    {
      bitPointer = 0x01;
    }
    dataPointer ++;
    if( dataPointer >= 6 )
    {
      dataPointer = 0;
    }
  }
  else
  {
               //shift in newest data bit
    patternRegister <<=1;
    syncBitCounter++;
    if(dataBit)
      patternRegister |= 0x01;

      //did we recieve the last bit of the last pattern byte?
    if( (patternRegister) == 0x817E)
    {
      patternRegister = 0;
      BERCounter++;
    }
    else if( syncBitCounter > 2000)// if we didn't detect the sync word in 8 bits then the chipcon received a false sync word
    {
      clearIOEnableInt(RF_DATA_CLK);
      //BERDone = true;
    }
  }
}

/*******************************************************************************

DESCRIPTION: Toggles between standard and low-interference frequency modes

NOTES: Reassigns the current frequency mode

*******************************************************************************/
static void SwitchFrequencyModes(void)
{
  if (currentFrequencyMode == STANDARD_FREQUENCY_MODE)
    currentFrequencyMode = LOW_INTERFERENCE_FREQUENCY_MODE;
  else
    currentFrequencyMode = STANDARD_FREQUENCY_MODE;

  SetFrequencyMode(currentFrequencyMode);
}

