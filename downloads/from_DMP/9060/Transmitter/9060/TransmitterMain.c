/*******************************************************************************
FILENAME: TransmitterMain.c

DESCRIPTION:

NOTES:

Copyright (c) Digital Monitoring Products Inc. 2010-2016.  All rights reserved.

*******************************************************************************/
/*----compilation control-----------------------------------------------------*/

/*******************************************************************************
INCLUDE FILES
*******************************************************************************/
/*----system and platform files (external libraries) -------------------------*/

/*----program files-----------------------------------------------------------*/
#include "types.h"
#include "TransmitterMain.h"
#include "chipconCommunication.h"
#include "Hardware.h"
#include "Packet.h"
#include "TransmitterProtocol.h"
#include "Hop.h"
#include "in430.h"
#include "ResourceManager.h"

/*******************************************************************************
EXTERNAL REFERENCES   Only used if not available in a header file.
*******************************************************************************/
/*----data declarations-------------------------------------------------------*/

/*----function prototype------------------------------------------------------*/

/*******************************************************************************
PRIVATE DECLARATIONS
*******************************************************************************/
/*----defines, typedefs, structs, unions, enums-------------------------------*/
enum {INITIAL_HOP_OFFSET = (NUM_HOP_CHANNELS + 1)};

/*----data declarations-------------------------------------------------------*/

SlotNumber currentSlotNumber;

static FrameStatus frameStatus = FS_SETUP_TRANSMITTER;
static SyncCounter syncCounter;
static char hopOffset;
static FrameTimeout frameTimeout;
static LastResortTimer lastResortTimer;
static FrequencyMode currentFrequencyMode = STANDARD_FREQUENCY_MODE;

/*----function prototype------------------------------------------------------*/

/*----macros------------------------------------------------------------------*/

/*******************************************************************************
PRIVATE FUNCTION DEFINITIONS
*******************************************************************************/

/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
static void setWirelessMode(WirelessMode mode)
{
  static WirelessMode lastMode = WM_OFF;

  if (mode == lastMode)
    return;

  if (mode == WM_OFF)
  {
    setupWirelessSleep();
  }
#ifdef USING_CHIPCON_RF_CHIP
  else if (mode == WM_IDLE)
  {
    setupWirelessIdle();
  }
#endif
  else
  {
#ifdef USING_ACK_LED
#if defined USING_1232_HARDWARE || defined DMP_1119_TRANSMITTER
    if( ackStatus.counter <=3 && getAlarmWakeup())
#else
    if (ackStatus.counter <= 3)
#endif
    {
      LED_ON(RED_LED);  //LED ON
    }
#endif
    if (mode == WM_RECEIVE)
    {
#ifdef USING_CHIPCON_RF_CHIP
      setupWirelessIdle();
#endif
      setupWirelessReceive();
    }
    else
    {
      setupWirelessTransmit();
    }
  }

  lastMode = mode;
}

/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
static void resetState()
{
  communicationStatus.frameStartDetected = false;
  frameStatus = FS_SETUP_TRANSMITTER;
}

/*******************************************************************************

DESCRIPTION:  Sets the synchronization status.  We are either in sync with the
 receiver or not.  Once we get good sync, we are in sync several tries (GOOD_SYNC_COUNT)

NOTES:  This is somewhat tricky, so be careful with it

*******************************************************************************/
static void setReceiverSync(ReceiverSyncStatus status)
{
  switch (status)
  {
  case RS_GOOD_SYNC:
    // If we are out of sync, bring into sync
    if ((syncCounter < GOOD_SYNC_COUNT) &&
        (hopOffset != ((ReceiverAck*)receiverPacketBuffer)->houseCode))
    {
      syncCounter = GOOD_SYNC_COUNT;
      hopOffset = ((ReceiverAck*)receiverPacketBuffer)->houseCode;
    }
    else
    {
      if (syncCounter < MAX_SYNC_COUNT) //keep the syncCounter from overflowing
        syncCounter++;
    }
    break;

  case RS_TEMP_SYNC:
    if (syncCounter < GOOD_SYNC_COUNT)  // we get at least one try
    {
      syncCounter++;
      if (syncCounter <= 0)
      {
        syncCounter = (SyncCounter)1;
        // only set hop offset if we were not previously in sync
        hopOffset = ((ReceiverAck*)receiverPacketBuffer)->houseCode;
      }
    }
    break;

  case RS_BAD_SYNC:
    if (syncCounter > GOOD_SYNC_COUNT)      // In sync with receiver but just started to fail
    {
      syncCounter -= (SyncCounter)2;        // First several bad syncs decrement syncCounter faster
    }
    else if (syncCounter > RESYNC_FAILURE)  // Decrement syncCounter until totally out of sync
    {
      --syncCounter;
    }
    else  // Out of sync, start over
    {
      syncCounter = INITIAL_SYNC_COUNT;
      resetHardware(WIRELESS_HW_RESET);  // reset wireless HW
    }

    if (syncCounter <= 0)
    {
      resetLearnedInformation();
      hopOffset = INITIAL_HOP_OFFSET;
    }
    break;

  case RS_FORCE_NO_SYNC:
    syncCounter = INITIAL_SYNC_COUNT;
    hopOffset = 0;
    break;
  }

  if (syncCounter >= GOOD_STATUS_SYNC_COUNT) //if we are in good enough sync
  {
    BusPoll_Time = 0;
    SetSurveyInSync(true);
  }
  else
  {
    SetSurveyInSync(false);
  }
}

/*******************************************************************************

DESCRIPTION:  Depending upon whether or not we are in sync and how long we have
not been in sync, we determine whether to hop, stay, or timeout.

NOTES:

*******************************************************************************/
static void processHop(void)
{
  if ((syncCounter > BUMP_SYNC_COUNT) ||
      ((syncCounter & (RESYNC_TRIES - 1)) == 0) && (syncCounter < 0))
  {
    currentTransmitterChannel = addChannel(currentTransmitterChannel, hopOffset);
  }
  // if we are just to go out of sync, bump a little bit in case we got off slot somehow
  else if (syncCounter == BUMP_SYNC_COUNT)
  {
    hopOffset = (unsigned short)(hopOffset * 4) % NUM_HOP_CHANNELS;
    currentTransmitterChannel = addChannel(currentTransmitterChannel, hopOffset);
  }// else stay on current frequency
}

/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
static void setupFrameStatus(FrameStatus status)
{
  frameStatus = status;
}

/*******************************************************************************

DESCRIPTION: Toggles between standard and low-interference frequency modes

NOTES: Reassigns the current frequency mode

*******************************************************************************/
void SwitchFrequencyModes(void)
{
  if (currentFrequencyMode == STANDARD_FREQUENCY_MODE)
    currentFrequencyMode = LOW_INTERFERENCE_FREQUENCY_MODE;
  else
    currentFrequencyMode = STANDARD_FREQUENCY_MODE;

  SetFrequencyMode(currentFrequencyMode);
}

/***************************************************************************//**

DESCRIPTION:  Start the last resort timer over.

NOTES:

*******************************************************************************/
void ResetLastResortTimer(void)
{
  lastResortTimer = LAST_RESORT_TIMEOUT;
}

/***************************************************************************//**

DESCRIPTION:  Decrement and check the last resort timer.

NOTES:

*******************************************************************************/
static void UpdateLastResortTimer(void)
{
  if (--lastResortTimer == 0)
  {
    resetLearnedInformation();
    setReceiverSync(RS_FORCE_NO_SYNC);
    ResetLastResortTimer();
  }
}

/*******************************************************************************
PUBLIC FUNCTION DEFINITIONS
*******************************************************************************/
/***************************************************************************//**

DESCRIPTION: Returns true if the keypad is in sync with the receiver.

NOTES:

*******************************************************************************/
bool TransmitterInSync(void)
{
  return (houseCode != INVALID_HOUSE_CODE) && (syncCounter >= TRANSMITTER_MODE_SYNC);
}

/***************************************************************************//**

DESCRIPTION: Function will return true if a hop is needed while searching for sync

NOTES:

*******************************************************************************/
bool HopSearchForSync(void)
{
  return ((syncCounter & (RESYNC_TRIES - 1)) == 0);
}

/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
void decrementFrameTimeout(void)
{
  if (frameTimeout > 0)
  {
    frameTimeout--;
  }
}

/***************************************************************************//**

DESCRIPTION: Will process the end of the transmitter frame which is timed from the
              RFFrameBitcounter.  If currently in receiver mode the transmitter will
              simply hop frequencies and/or switch to transmitter mode, If in
              transmitter mode the transmitterCommState will be changed appropriately
              based upon the current state.
NOTES:

*******************************************************************************/
void ProcessTransmitterEOF(void)
{
  if (frameStatus == FS_WAITING_FOR_FIRST_HALF_OF_LONG_SLOT)
  {
    // waiting on next part of long slot so don't hop and just continue to listen
    frameStatus = FS_WAITING_FOR_SECOND_HALF_OF_LONG_SLOT;
    return;
  }

  processHop();

  //if the transmitter is waiting to setup receive mode to receive an ack
  if (frameStatus == FS_WAITING_FOR_ACK_SETUP)
  {
    frameStatus = FS_SETUP_FOR_ACK; //change the mode to setup for ack
  }
  //if the transmitter is waiting for the end of the current frame
  else if (frameStatus == FS_WAITING_FOR_EOF)
  {
    frameStatus = FS_SETUP_TRANSMITTER;//setup to receive the next frame
  }
  else if (frameStatus == FS_WAITING_FOR_SECOND_HALF_OF_LONG_SLOT)
  {
    processHop(); // extra hop because of not hopping while waiting on long slot
    setReceiverSync(RS_BAD_SYNC);  // timed out while waiting on the long slot
    frameStatus = FS_SETUP_TRANSMITTER;
  }
  //if the transmitter has timed out on a state which is not handled
  else
  {
    UpdateLastResortTimer();
    setReceiverSync(RS_BAD_SYNC);
    frameStatus = FS_SETUP_TRANSMITTER;
  }
}

/***************************************************************************//**

DESCRIPTION:

NOTES:

*******************************************************************************/
void TransmitterMain(void)
{
  static char characterOffset;

  switch (frameStatus)
  {
    case FS_SETUP_TRANSMITTER: //setup the radio to receive the slot start
    {
      initializeCommunicationStatus();
      setWirelessMode(WM_IDLE);
      SetFrequency(currentTransmitterChannel);
      setWirelessMode(WM_RECEIVE);
      setupFrameStatus(FS_WAITING_FOR_FIRST_PATTERN);
      break;
    }

    case FS_WAITING_FOR_FIRST_PATTERN:
      if (communicationStatus.frameStartDetected)
      {
        setupFrameStatus(FS_WAITING_FOR_FIRST_ACK);
      }
      break;

    case FS_WAITING_FOR_FIRST_ACK:
      if (processReceivedAck(&characterOffset) != RAS_NOTHING_TO_DO)
      {
        setupFrameStatus(FS_WAITING_FOR_SLOT_DEFINITION);
      }
      break;


    case FS_WAITING_FOR_SLOT_DEFINITION:
      {
        TransmitterMessageStatus slotDefinitionStatus;
        slotDefinitionStatus = setupTransmitterMessage(characterOffset);

        switch (slotDefinitionStatus)
        {
        case TMS_MESSAGE_SETUP:
          setWirelessMode(WM_TRANSMIT);
          setReceiverSync(RS_GOOD_SYNC);
          setupFrameStatus(FS_TRANSMITTING);
          break;

        case TMS_NO_MESSAGE_NEEDED:
          setReceiverSync(RS_GOOD_SYNC);
          ResetLastResortTimer();
          setupFrameStatus(FS_WAITING_FOR_EOF);
          break;

        case TMS_STILL_WAITING:  // do nothing
          _NOP();
          break;

        case TMS_NO_MESSAGE_SETUP:
          setReceiverSync(RS_TEMP_SYNC);
          setupFrameStatus(FS_WAITING_FOR_EOF);
          break;

        case TMS_RECEIVING_LONG_MESSAGE:
          setupFrameStatus(FS_WAITING_FOR_FIRST_HALF_OF_LONG_SLOT);
          break;

        default:
         _NOP();
         break;
        }
      }
      break;

    case FS_WAITING_FOR_SECOND_HALF_OF_LONG_SLOT:
      {
        TransmitterMessageStatus slotDefinitionStatus;
        slotDefinitionStatus = setupTransmitterMessage(characterOffset);

        switch (slotDefinitionStatus)
        {
        case TMS_LONG_MESSAGE_RECEIVED:
          setWirelessMode(WM_TRANSMIT);
          setupFrameStatus(FS_TRANSMITTING_AFTER_LONG_SLOT);
          setReceiverSync(RS_GOOD_SYNC);
          break;

        case TMS_RECEIVING_LONG_MESSAGE:
          _NOP();
          break;

        case TMS_NO_MESSAGE_NEEDED:
          setReceiverSync(RS_GOOD_SYNC);
          ResetLastResortTimer();
          processHop();  // hop an extra time because of not hoping while waiting for long slot
          setupFrameStatus(FS_WAITING_FOR_EOF);
          break;
        }
      }
      break;

    case FS_TRANSMITTING:
    case FS_TRANSMITTING_AFTER_LONG_SLOT:
      if (communicationStatus.transmitComplete)
      {
        // hop an extra time because of not hoping while waiting for long slot
        if (frameStatus == FS_TRANSMITTING_AFTER_LONG_SLOT)
          processHop();
        setWirelessMode(WM_IDLE);
        communicationStatus.transmitComplete = false;
        communicationStatus.frameStartDetected = false;
        communicationStatus.timeToTestFrequency = false;
        setupFrameStatus(FS_WAITING_FOR_ACK_SETUP);
      }
      break;

    case FS_SETUP_FOR_ACK: //setup the radio to receive the ack from the message we TXd
    {
      SetFrequency(currentTransmitterChannel);
      initStateForReceive();
      setWirelessMode(WM_RECEIVE);
      setupFrameStatus(FS_WAITING_FOR_ACK_PATTERN);
      break;
    }

    case FS_WAITING_FOR_ACK_PATTERN:
      if (communicationStatus.frameStartDetected)
      {
        setupFrameStatus(FS_WAITING_FOR_ACK);
      }
      break;

    case FS_WAITING_FOR_ACK:
      {
        ReceivedAckStatus slotAckStatus;
        slotAckStatus = processReceivedAck(&characterOffset);

        switch (slotAckStatus)
        {
        case RAS_GOOD_MESSAGE_ACK_RECEIVED:
          setReceiverSync(RS_GOOD_SYNC);
          ResetLastResortTimer(); //reset the timeout since successful communication
          applyFrequencyErrorOffset();
          setupFrameStatus(FS_WAITING_FOR_SLOT_DEFINITION);
          break;

        case RAS_GOOD_MESSAGE_NON_ACK:
          setReceiverSync(RS_BAD_SYNC);
          UpdateLastResortTimer();
          setupFrameStatus(FS_WAITING_FOR_SLOT_DEFINITION);
          break;

        case RAS_BAD_MESSAGE:
          setReceiverSync(RS_BAD_SYNC);
          setupFrameStatus(FS_WAITING_FOR_EOF);
          UpdateLastResortTimer();
          break;

        case RAS_NOTHING_TO_DO:
          break;

        case RAS_GOOD_MESSAGE_WRONG_RECEIVER:
        default:
          setReceiverSync(RS_FORCE_NO_SYNC);
          resetState();
          break;
        }
      }
      break;
    case FS_WAITING_FOR_EOF:
      setWirelessMode(WM_OFF);
      break;
    case FS_WAITING_FOR_FIRST_HALF_OF_LONG_SLOT:
      break;
    case FS_WAITING_FOR_ACK_SETUP:
      break;
    }
}