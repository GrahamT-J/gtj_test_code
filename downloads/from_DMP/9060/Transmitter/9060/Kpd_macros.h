/**

    Kpd_macros.h

    Copyright(c) Digital Monitoring Products 2004

    Author: DIGITAL MONITORING PRODUCTS
    Created: HP  3/29/04 2:25:01 PM
	Last change: HP 8/18/2004 12:15:25 PM
*/

DJNZ	MACRO	  Operand,JumpAddress
	DEC.B	  Operand
	JNZ	  JumpAddress
	ENDM

JBIT0	MACRO	  Bits,MemAddress,JumpAddress
	BIT.B	  #Bits,&MemAddress
	JZ        JumpAddress
        ENDM
// Macro for jump if bit reset for word flags
JBITW0	MACRO	  Bits,MemAddress,JumpAddress
	BIT.W	  #Bits,&MemAddress
	JZ        JumpAddress
        ENDM

JBIT1	MACRO	  Bits,MemAddress,JumpAddress
	BIT.B	  #Bits,&MemAddress
	JNZ	  JumpAddress
	ENDM
// Macro for jump if bit set for word flags	
JBITW1	MACRO	  Bits,MemAddress,JumpAddress
	BIT.W	  #Bits,&MemAddress
	JNZ	  JumpAddress
	ENDM	

RL_B    MACRO     Operand
        BIT.B     #0x80,Operand           ;Put High bit in Carry
        RLC.B     Operand
        BIT.B     #0x01,Operand           ;Put Low bit in Carry
        ENDM

RR      MACRO     Operand
        BIT.B     #0x01,Operand           ;Put Low bit in Carry
        RRC.B     Operand
        BIT.B     #0x80,Operand
        ENDM

SBIT0	MACRO     Bits,MemAddress
	BIC.B	  #Bits,&MemAddress
	ENDM

SBIT1   MACRO	  Bits,MemAddress
	BIS.B	  #Bits,&MemAddress
	ENDM
// Macro for reset bit for word flags	
SBITW0	MACRO     Bits,MemAddress
	BIC.W	  #Bits,&MemAddress
	ENDM
// Macro for set bit for word flags
SBITW1  MACRO	  Bits,MemAddress
	BIS.W	  #Bits,&MemAddress
	ENDM	

SWAP	MACRO	  Operand
        CALL      #SWAPNIBBLES
	ENDM

XCHB	MACRO	  Operand
	PUSH.B    Operand			;Save byte on Stack
	MOV.B     R5,Operand			;Move B (Reg5) to Byte
	POP.B	  R5				;Move original byte to R5
	CMP.B	  #0,R5
	ENDM				

CompToDispReg     MACRO     StartPos,MsgPtr
        MOV.B     #StartPos,R5
        MOV.W     #MsgPtr,R6
        CALL      #COMPARE_DISP_BUFF
        ENDM

FillDisplay       MACRO     Val,StartPos,EndPos
        MOV.B     #Val,R4
        MOV.B     #StartPos,R5
        MOV.B     #EndPos,R6
        CALL      #FILL_DISPLAY
        ENDM

DisplayMessage    MACRO     MsgPtr,StartPos
        MOV.W     #MsgPtr,R6
        MOV.B     #StartPos,R5
        CALL      #DISPLAY_MSG
        ENDM

InputString       MACRO     StrPos,StrLen,StrType
        MOV.B     #StrPos,Input_Pos
        MOV.B     #StrLen,Input_Len
        MOV.B     #StrType,Input_Type
        CALL      #INPUT_STRING
        ENDM

WaitForKey        MACRO     WaitTime
        MOV.B     #WaitTime,R4
        CALL      #WAIT_FOR_KEY
        ENDM

WaitForKeyRelease MACRO
        CALL      #WAIT_FOR_KEY_RELEASE
        ENDM

ClearDisplay      MACRO
        CALL      #CLEAR_DISPLAY
        ENDM
	
GoToRFIDDataState   MACRO   _State
        MOV.B       #_State*2,&RFIDDataStateCntr             ;Go to next state
        ENDM

RAM_EVEN            MACRO
        ORG         $ + ($ % 2)
        ENDM


COUNT_DOWN_B        MACRO   TIMER
        LOCAL       CD_END
        TST.B       &TIMER
        JZ          CD_END
        DEC.B       &TIMER
CD_END:
        ENDM

COUNT_DOWN_JNZ_B    MACRO   TIMER,_ADDR
        TST.B       &TIMER
        JZ          _ADDR
        DEC.B       &TIMER
        JNZ         _ADDR
        ENDM

RotateNibbleLeft    MACRO  acc
        RLA         acc
        RLA         acc
        RLA         acc
        RLA         acc
        ENDM

RotateNibbleRight MACRO acc
        RRA         acc
        RRA         acc
        RRA         acc
        RRA         acc
        ENDM

// Macro to define State Table for Keypad Programming Menu
DEFINE_STATE        MACRO       ST_NAME,ST_MSG,ST_INIT_ADDR,ST_DATA_MOD,ST_NUMBER,ST_TRANSITION
ST_NAME:
        DW          ST_MSG
        DW          ST_INIT_ADDR
        DW          ST_DATA_MOD
        DW          ST_NUMBER
        DW          ST_TRANSITION
        ENDM
// Macro to define transition table for keypad programming menu
DEFINE_TRANSITION   MACRO       TR_NAME,TR_BK_DATA,TR_BK_FLAG,TR_BK_FALSE,TR_BK_TRUE,TR_CMD_DATA,TR_CMD_FLAG,TR_CMD_FALSE,TR_CMD_TRUE
TR_NAME:
        DW          TR_BK_DATA
        DW          TR_BK_FLAG
        DW          TR_BK_FALSE
        DW          TR_BK_TRUE
        DW          TR_CMD_DATA
        DW          TR_CMD_FLAG
        DW          TR_CMD_FALSE
        DW          TR_CMD_TRUE
        ENDM
// Macro to define number table for keypad programming menu
DEFINE_NUMBER        MACRO       _NAME,_Data,_Flag,_POS,_LOW,_HIGH,_DFLT,_MODE
_NAME:
        DW          _Data
        DW          _Flag
        DW          _POS
        DW          _LOW
        DW          _HIGH
        DW          _DFLT
        DW          _MODE
        ENDM
// Macro to define a byte in flash, ram, and const memory for keypad programming
DEFINE_BYTE          MACRO        NAME,NAME_EE, DFLT_NAME_EE, DFLT
        RSEG        UDATA0
NAME
        DS          1
        RSEG        INFO
NAME_EE
        DB          DFLT
        RSEG        CONST
DFLT_NAME_EE
        DB          DFLT
        ENDM
// Macro to define a word in flash, ram, and const memory for keypad programming
DEFINE_WORD         MACRO         NAME, NAME_EE, DFLT_NAME_EE, DFLT
        RSEG        UDATA0
NAME
        DS          2
        RSEG        INFO
NAME_EE
        DW          DFLT
        RSEG        CONST
DFLT_NAME_EE
        DW          DFLT
        ENDM
// Macro to define a veriable length msg in flash, ram, and const memory for keypad programming
DEFINE_MSG          MACRO         NAME, NAME_EE, DFLT_NAME_EE, DFLT, SIZE
        RSEG        UDATA0
NAME
        DS          SIZE

        RSEG        INFO
NAME_EE
        DB          DFLT

        RSEG        CONST
DFLT_NAME_EE
        DB          DFLT
        ENDM
//Macro to call memory copy function
MemCopy             MACRO         SRC,DST,SIZE
        MOV.W       #SRC, R9
        MOV.W       #DST, R10
        MOV.W       #SIZE, R8
        CALL        #MemoryCopy
        ENDM

PUSH_SCRATCH        MACRO
        PUSH        R12
        PUSH        R13
        PUSH        R14
        PUSH        R15
        ENDM

POP_SCRATCH         MACRO
        POP         R15
        POP         R14
        POP         R13
        POP         R12
        ENDM

PUSH_ALL            MACRO
        PUSH        R4
        PUSH        R5
        PUSH        R6
        PUSH        R7
        PUSH        R8
        PUSH        R9
        PUSH        R10
        PUSH        R11
        PUSH        R12
        PUSH        R13
        PUSH        R14
        PUSH        R15
        ENDM

POP_ALL             MACRO
        POP         R15
        POP         R14
        POP         R13
        POP         R12
        POP         R11
        POP         R10
        POP         R9
        POP         R8
        POP         R7
        POP         R6
        POP         R5
        POP         R4
        ENDM

PUSH_NON_SCRATCH    MACRO
        PUSH        R4
        PUSH        R5
        PUSH        R6
        PUSH        R7
        PUSH        R8
        PUSH        R9
        PUSH        R10
        PUSH        R11
        ENDM

POP_NON_SCRATCH     MACRO
        POP         R11
        POP         R10
        POP         R9
        POP         R8
        POP         R7
        POP         R6
        POP         R5
        POP         R4
        ENDM

KICK_DOG            MACRO
        ;WDTCTL = WDTPW | WDTCNTCL | WDTSSEL;  // start watchdog, reset, and use ACLK
        MOV.W       #0x5A0C,&WDTCTL            // RELEASE_TODO: make sure watchdog is enabled
        ENDM

DISABLE_DOG            MACRO
        ;WDTCTL = WDTPW | WDTHOLD | WDTCNTCL | WDTSSEL;  // stop watchdog
        MOV.W       #0x5A8C,&WDTCTL
        ENDM

////////////////////////////////////
// Macros to set or clear pins    //
////////////////////////////////////
AudioOutPinON       MACRO
        SBIT1       AudioOutPin,AudioOutPinPORT
        ENDM

AudioOutPinOFF      MACRO
        SBIT0       AudioOutPin,AudioOutPinPORT
        ENDM

AutoTestACKOutON    MACRO
        SBIT1       AutoTestACKOut,AutoTestACKOutPORT
        ENDM

AutoTestACKOutOFF   MACRO
        SBIT0       AutoTestACKOut,AutoTestACKOutPORT
        ENDM

; Turn on green backlight but only if red backlight is off
GrnBackLite_ON      MACRO
        LOCAL       GBL_ON_END
        JBIT1       RedBackLite,RedBackLitePORT,GBL_ON_END
        SBIT1       GrnBackLite,GrnBackLitePORT
GBL_ON_END:
        ENDM

GrnBackLite_OFF     MACRO
        SBIT0       GrnBackLite,GrnBackLitePORT
        ENDM

GreenBLLogoON       MACRO
        SBIT0       GreenBLLogo,GreenBLLogoPORT
        ENDM

GreenBLLogoOFF      MACRO
        SBIT1       GreenBLLogo,GreenBLLogoPORT
        ENDM

GreenBLLogoTOGGLE   MACRO
        XOR.B       #GreenBLLogo,&GreenBLLogoPORT    ; Toggle Green Background Logo LED
        ENDM

GreenBLLogoTEST     MACRO
        BIT.B       #GreenBLLogo,&GreenBLLogoPORT    ; Z flag = 1 if bit is 0 (LED is On)
        ENDM

LCDBackLiteON       MACRO
        SBIT1       LCDBackLite,LCDBackLitePORT
        ENDM

LCDBackLiteOFF      MACRO
        SBIT0       LCDBackLite,LCDBackLitePORT
        ENDM

LCD_EON             MACRO
        SBIT1       LCD_E,LCD_EPORT  ;220 nS min pulse width, 500 nS min cycle time
        NOP
        NOP
        NOP
        NOP
        NOP
        ENDM

LCD_EOFF            MACRO
        NOP
        NOP
        NOP
        NOP
        NOP
        SBIT0       LCD_E,LCD_EPORT
        ENDM

LCD_RSON            MACRO
        SBIT1       LCD_RS,LCD_RSPORT
        ENDM

LCD_RSOFF           MACRO
        SBIT0       LCD_RS,LCD_RSPORT
        ENDM

LCD_RWON            MACRO
        SBIT1       LCD_RW,LCD_RWPORT
        ENDM

LCD_RWOFF           MACRO
        SBIT0       LCD_RW,LCD_RWPORT
        NOP
        NOP
        NOP
        NOP
        ENDM

RedBackLite_ON      MACRO
        SBIT1       RedBackLite,RedBackLitePORT
        ENDM

RedBackLite_OFF     MACRO
        SBIT0       RedBackLite,RedBackLitePORT
        ENDM

RedBLLogoON         MACRO
        SBIT0       RedBLLogo,RedBLLogoPORT
        ENDM

RedBLLogoOFF        MACRO
        SBIT1       RedBLLogo,RedBLLogoPORT
        ENDM

RedBLLogoTOGGLE     MACRO
        XOR.B       #RedBLLogo,&RedBLLogoPORT    ; Toggle Red Background Logo LED
        ENDM

RedBLLogoTEST       MACRO
        BIT.B       #RedBLLogo,&RedBLLogoPORT    ; Z flag = 1 if bit is 0 (LED is On)
        ENDM

TamperSwitchTEST    MACRO
        BIT.B       #TamperSwitch,&TamperSwitchPORT    ; Z flag = 0 if tamper is true
        ENDM

VolumeOutPinON      MACRO
        SBIT1       VolumeOutPin,VolumeOutPinPORT
        ENDM

VolumeOutPinOFF     MACRO
        SBIT0       VolumeOutPin,VolumeOutPinPORT
        ENDM

; Z=1 if in functional test mode (pin is low)
TEST_FUNCTEST_MODE_PIN  MACRO
        BIT.B       #AUTO_TEST_MODE_FUNC_BIT,&AUTO_TEST_MODE_FUNC_BIT_PORT
        ENDM
