/**

    OTAMsg.h

    Copyright(c) Digital Monitoring Products 2009

    Author: DIGITAL MONITORING PRODUCTS
*/

/******************

NOTE: This is used by C and ASM (hence no structs, etc.)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Keypad Status Params
;     Spec: http://prism.dmp.com/index.php/Wireless_Over_the_Air_Protocol#Keypad_Status_Bytes
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; Name                   Description
;
; --- The following params are stored in the KEYPAD_STATUS_1 byte ---
; RESERVED               Reserved, always 0
; AUDIBLES               0     Off
;                        1     Steady
;                        2     Fire
;                        3     Monitor
;                        4     Burglary
;                        5     Prewarn
;                        6-7   Undefined
; DOOR                   Door strike on/off
; BUZZ                   0     Off
;                        1     Momentary 0.1 second
;                        2     Momentary 1 second
;                        3     On
;
; --- The following params are stored in the KEYPAD_STATUS_2 byte ---
; STATUS COLOR           0     Red
;                        1     Green
;                        2-3   Undefined
; STATUS CADENCE         0     Off
;                        1     Steady
;                        2     Flashing on/off
;                        3     Alternating colors
;                        4-7   Undefined
; RESERVED               0-3   Reserved, always 0
; RED BL                 Red keypad backlight
;
***********************/
//Note ASM works with 0x__ or ________B format but C works better with 0x__

#define    AUDIBLES_MASK              0xE0 //11100000B  // use with KEYPAD_STATUS_1
#define    AUDIBLES_OFF               0x00 //00000000B
#define    AUDIBLES_STEADY            0x20 //00100000B
#define    AUDIBLES_FIRE              0x40 //01000000B
#define    AUDIBLES_MONITOR           0x60 //01100000B
#define    AUDIBLES_BURGLARY          0x80 //10000000B
#define    AUDIBLES_PREWARN           0xA0 //10100000B
#define    AUDIBLES_UNDEFINED1        0xC0 //11000000B
#define    AUDIBLES_UNDEFINED2        0xE0 //11100000B

#define    DOOR_MASK                  0x04 //00000100B  // use with KEYPAD_STATUS_1
#define    DOOR_ON                    0x04 //00000100B

#define    BUZZ_MASK                  0x03 //00000011B  // use with KEYPAD_STATUS_1
#define    BUZZ_OFF                   0x00 //00000000B
#define    BUZZ_TENTH_SEC             0x01 //00000001B
#define    BUZZ_ONE_SEC               0x02 //00000010B
#define    BUZZ_ON                    0x03 //00000011B

#define    STATUS_COLOR_MASK          0xC0 //11000000B  // use with KEYPAD_STATUS_2
#define    STATUS_COLOR_RED           0x00 //00000000B  // Red Logo LED
#define    STATUS_COLOR_GREEN         0x40 //01000000B  // Green Logo LED
#define    STATUS_COLOR_UNDEF2        0x80 //10000000B
#define    STATUS_COLOR_UNDEF3        0xC0 //11000000B

#define    STATUS_CADENCE_MASK        0x38 //00111000B  // use with KEYPAD_STATUS_2
#define    STATUS_CADENCE_OFF         0x00 //00000000B
#define    STATUS_CADENCE_STEADY      0x08 //00001000B
#define    STATUS_CADENCE_FLASHING    0x10 //00010000B
#define    STATUS_CADENCE_ALT_COLORS  0x18 //00011000B
#define    STATUS_CADENCE_UNDEF4      0x20 //00100000B
#define    STATUS_CADENCE_UNDEF5      0x28 //00101000B  // etc. for UNDEF 6,7

#define    RESERVED_TYPE_MASK         0x06 //00000110B  // RESERVED bits in KEYPAD_STATUS_2

#define    RED_BACKLIGHT_MASK         0x01 //00000001B  // use with KEYPAD_STATUS_2
#define    RED_BACKLIGHT_OFF          0x00 //00000000B
#define    RED_BACKLIGHT_ON           0x01 //00000001B

// powerup defaults -- used until panel sends values to the keypad
#define    KEYPAD_STATUS_1_DFLT       (AUDIBLES_OFF | BUZZ_OFF)
#define    KEYPAD_STATUS_2_DFLT       (STATUS_COLOR_GREEN | STATUS_CADENCE_STEADY | RED_BACKLIGHT_OFF)

/********************************************
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Exit Delay Params
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; --- The following params are stored in the first byte that follows a display message from the panel ---
; COUNT                  0-240 exit delay count value
;
; --- The following param is stored in the second byte that follows a display message from the panel ---
; DISPLAY_POSN           0-15  Position on 2nd line of display that selects where the count is displayed
;                              The valid range for the display offset is 0-13 (there are 3 digits to
;                              display, and 16 chars on row 2 of the LCD).

********************************************/

#define    MAX_DISPLAY_POSITION       13
