/*******************************************************************************
FILENAME:     ResourceManager.h

DESCRIPTION:

NOTES:

Copyright (c) Digital Monitoring Products Inc. 2010.  All rights reserved.

*******************************************************************************/
#ifndef  RESOURCE_MANAGER_H
#define  RESOURCE_MANAGER_H

/*----compilation control-----------------------------------------------------*/

/*******************************************************************************
INCLUDE FILES
*******************************************************************************/
/*----system and platform files-----------------------------------------------*/
#include "types.h"

/*----program files-----------------------------------------------------------*/

/*******************************************************************************
FILE CONTEXT
*******************************************************************************/
/*----defines, typedefs, structs, unions, enums-------------------------------*/

typedef enum
{
  LOWEST_PRIORITY,
  PANEL_PRIORITY = LOWEST_PRIORITY,
  LOCAL_MODE_PRIORITY,
  SURVEY_PRIORITY,
  HIGHEST_PRIORITY = SURVEY_PRIORITY,
  TOTAL_NUM_PRIORITIES  // insert new priorities above this line
} KeypadOpModePriority;

enum
{
  YIELD_MODE = true,
  NO_YIELD_MODE = false
};

/*----data descriptions-------------------------------------------------------*/

/*----data declarations-------------------------------------------------------*/
extern unsigned char KEYPAD_STATUS_1;
extern unsigned char KEYPAD_STATUS_2;
extern unsigned char RM_AudibleArray[];
extern bool RM_AudibleOneShotArray[];
extern unsigned char RM_LEDArray[];
extern KeypadOpModePriority currentKeypadOpModePriority;
extern char RM_DisplayRegisterArray[TOTAL_NUM_PRIORITIES][32]; // associated with "DisplayRegister"
extern char RM_DisplayInputBuffer[TOTAL_NUM_PRIORITIES][16];   // associated with "Input_Buffer"

/*----macros------------------------------------------------------------------*/

/*----function prototypes-----------------------------------------------------*/

extern void ResMgrModeRqst(KeypadOpModePriority priority, bool yield);
extern void UpdateSharedResources();
extern void InitResourceManager();

extern void SetSurveyPriority(void);
extern void ClearSurveyPriority(void);
extern void SetSurveyInSync(bool inSync);

extern void ResMgrUpdateValues(KeypadOpModePriority priority, byte audibleVal, byte LEDVal);

#endif                                  /* end of file */
