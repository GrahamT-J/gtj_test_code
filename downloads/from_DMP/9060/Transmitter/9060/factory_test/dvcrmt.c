/*H*****************************************************************************
FILENAME: DVCRMT.C

DESCRIPTION: Remote Device Programming Support

Copyright (c) Digital Monitoring Products Inc. 2004 - 2010.  All rights reserved.
*****************************************************************************H*/

/*----compilation control-----------------------------------------------------*/

/*******************************************************************************
INCLUDE FILES
*******************************************************************************/
/*----system and platform files (external libraries) -------------------------*/

/*----program files-----------------------------------------------------------*/
#include "dvcrmt.h"

/*******************************************************************************
PRIVATE DECLARATIONS  Defined in this module, used only in this module.
*******************************************************************************/

/*----data declarations-------------------------------------------------------*/
const byte hex[]                          = "0123456789ABCDEF";

/*----function prototype------------------------------------------------------*/

/*******************************************************************************
PUBLIC FUNCTION DEFINITIONS
*******************************************************************************/

/*f*****************************************************************************

DESCRIPTION: Converts a word value to four HEX ASCII characters

*****************************************************************************f*/
void WordToAsciiHex(byte *s, word val)
{
    *(s++) = hex[(val & 0xF000) >> 12];
    *(s++) = hex[(val & 0x0F00) >> 8];
    *(s++) = hex[(val & 0x00F0) >> 4];
    *s = hex[val & 0x000F];
}

/*f*****************************************************************************

DESCRIPTION: Converts a byte value to two HEX ASCII characters

*****************************************************************************f*/
void ByteToAsciiHex(byte *s, byte val)
{
    *(s++) = hex[(val & 0xF0) >> 4];
    *s = hex[val & 0x0F];
}

/*f*****************************************************************************

DESCRIPTION: Converts four upper case HEX ASCII characters to a word value

*****************************************************************************f*/
word AsciiHexToWord(byte *s)
{
    return ((((word)AsciiHexToByte(s)) << 8) + AsciiHexToByte(s + 2));
}

/*f*****************************************************************************

DESCRIPTION: Converts two upper case HEX ASCII characters to a byte value

*****************************************************************************f*/
byte AsciiHexToByte(byte *s)
{
    return ((HexCharToBin(*s) << 4) + HexCharToBin(*(s + 1)));
}

/*F*****************************************************************************

DESCRIPTION: Converts one upper case HEX ASCII character to a nibble value

*****************************************************************************F*/
byte HexCharToBin(byte testChar)
{
    // If char is above 9 it must be in the A - F range
    return (testChar - ((testChar > '9') ? ('A' - 10) : '0'));
}

