/*H*****************************************************************************
FILENAME:

DESCRIPTION:

NOTES:

Copyright (c) Digital Monitoring Products Inc. 2008-2010.  All rights reserved.

*****************************************************************************H*/
/*----compilation control-----------------------------------------------------*/

/*******************************************************************************
INCLUDE FILES
*******************************************************************************/
/*----system and platform files (external libraries) -------------------------*/


/*----program files-----------------------------------------------------------*/
#include "types.h"
#include "BitMacros.h"
#include "ioportpins.h"
#include "chipcon.h"
#include "chipconCommunication.h"

/*******************************************************************************
EXTERNAL REFERENCES   Only used if not available in a header file.
*******************************************************************************/
/*----data declarations-------------------------------------------------------*/

/*----function prototype------------------------------------------------------*/

/*******************************************************************************
PRIVATE DECLARATIONS  Defined in this module, used only in this module.
*******************************************************************************/
/*----defines, typedefs, structs, unions, enums-------------------------------*/

/*----data declarations-------------------------------------------------------*/
static bool TX = false;
static char dataPattern[6];
static char dataPointer = 0;
static char bitPointer = 0x01;
static unsigned int syncBitCounter = 0;
static unsigned int patternRegister = 0;
static bool BERDone = true;
static char BERCounter = 0;
static bool initialized = false;

/*----function prototype------------------------------------------------------*/
static void setTransmit(void);
static void setReceive(void);
static bool berTest(void);
static void modulationOn(void);
static void modulationOff(void);
static void frequencySet(char command);
static void RFProductionDataClockEdge(void);
void Port1ProductionTestEdgeInterrupt(void);

/*----macros------------------------------------------------------------------*/

/*******************************************************************************
PUBLIC FUNCTION DEFINITIONS
*******************************************************************************/
/*F*****************************************************************************

DESCRIPTION:  Wireless Production Test

NOTES:

*****************************************************************************F*/
char wirelessProductionTest(char command)
{
  char returnData = command;

  if (!initialized)
  {
    initialized = true;
    initRFChip();
  }

  switch (command)
  {
    case 'T':
      setTransmit();
    break;

    case 'R':
      setReceive();
    break;

    case 'B':
      returnData = berTest();
    break;

    case 'D':
      modulationOn();;
    break;

    case 'd':
      modulationOff();
    break;

    case 'L':  // 902 MHz
    case 'M':  // 915 MHz
    case 'H':  // 928 MHz
      frequencySet(command);
    break;
  }

  return returnData;
}

/*******************************************************************************
PRIVATE FUNCTION DEFINITIONS
*******************************************************************************/
/*F*****************************************************************************

DESCRIPTION:  Sets the CC1101 to transmit mode

NOTES:

*****************************************************************************F*/
static void setTransmit(void)
{
  sendChipconCommandStrobe(SIDLE);
  writeChipconConfigRegister(DEVIATN, 0x00);
  clearIOEnableInt(RF_DATA_CLK);
  setIODirOut(RF_DATA_OUT);
  TX = true;
  sendChipconCommandStrobe(STX);
}

/*F*****************************************************************************

DESCRIPTION:  Sets the CC1101 to receive mode

NOTES:

*****************************************************************************F*/
static void setReceive(void)
{
  sendChipconCommandStrobe(SIDLE);
  writeChipconConfigRegister(DEVIATN, 0x35);
  clearIOEnableInt(RF_DATA_CLK);
  setIODirIn(RF_DATA_IN);
  TX = false;
  sendChipconCommandStrobe(SRX);
}

/*F*****************************************************************************

DESCRIPTION:  Starts the BER Test and returns the number of matching patterns

NOTES:

*****************************************************************************F*/
static char berTest(void)
{
  syncBitCounter = 0;
  patternRegister = 0;
  BERDone = false;
  BERCounter = 0;

  clearIOEnableInt(RF_DATA_CLK);             // Disable the DCLK interrupt before configuring RF Chip
  writeChipconConfigRegister(MDMCFG2, 0x00); // Turn off sync word detection
  setIODirIn(RF_DATA_IN);                    // Set the data pin direction

  // Prepare for interrupts on the data clk
  clearIOIntFlag(RF_DATA_CLK);
  setIOEnableInt(RF_DATA_CLK);

  while (!BERDone);

  return BERCounter;
}

/*F*****************************************************************************

DESCRIPTION:  Turns CC1101 modulation on

NOTES:

*****************************************************************************F*/
static void modulationOn(void)
{
  char i;

  clearIOEnableInt(RF_DATA_CLK);
  sendChipconCommandStrobe(SIDLE); // put the RF Chip into Idle mode
  writeChipconConfigRegister(MDMCFG2, 0x00);
  writeChipconConfigRegister(DEVIATN, 0x35);
  setIODirOut(RF_DATA_OUT);

  for (i = 0; i < 6; i++)
  {
    dataPattern[i] = 0x55;
  }

  bitPointer = 0x01;
  dataPointer = 0;
  TX = true;
  sendChipconCommandStrobe(STX);

  // Prepare for interrupts on the data clk line
  clearIOIntFlag(RF_DATA_CLK);
  setIORisingInt(RF_DATA_CLK);
  setIOEnableInt(RF_DATA_CLK);
}

/*F*****************************************************************************

DESCRIPTION:  Turns CC1101 modulation off

NOTES:

*****************************************************************************F*/
static void modulationOff(void)
{
  clearIOEnableInt(RF_DATA_CLK);
  sendChipconCommandStrobe(SIDLE);
  writeChipconConfigRegister(DEVIATN, 0x00);
}

/*F*****************************************************************************

DESCRIPTION: Sets the CC1101 frequency to 902MHz, 915MHz, or 928MHz

NOTES:

*****************************************************************************F*/
static void frequencySet(char command)
{
  unsigned long dataLong;
  char tempData;

  sendChipconCommandStrobe(SIDLE);

  switch (command)
  {
    case 'L': // 902 MHz
      dataLong = 0x22B13B;
      break;

    case 'H': // 928 MHz
      dataLong = 0x23B13B;
      break;

    case 'M': // 915 MHz
      dataLong = 0x23313B;
      break;
  }

  tempData = dataLong & 0xFF;
  writeChipconConfigRegister(FREQ0, tempData);
  tempData = dataLong>>8 & 0xFF;
  writeChipconConfigRegister(FREQ1, tempData);
  tempData = dataLong>>16 & 0xFF;
  writeChipconConfigRegister(FREQ2, tempData);

  if (TX)
    sendChipconCommandStrobe(STX);
  else
    sendChipconCommandStrobe(SRX);
}

/*F*****************************************************************************

DESCRIPTION:  Production test RF in/out interrupt function

NOTES:

*****************************************************************************F*/
static void RFProductionDataClockEdge(void)
{
  bool dataBit = (getIO(RF_DATA_IN) > 0);  // read state of data ASAP

  if (TX)
  {
    dataBit = (dataPattern[dataPointer] & bitPointer);

    if (dataBit)
      setIO(RF_DATA_OUT);
    else
      clearIO(RF_DATA_OUT);

    bitPointer <<= 1;
    if (bitPointer == 0)
    {
      bitPointer = 0x01;

      dataPointer ++;
      if (dataPointer >= 6)
        dataPointer = 0;
    }
  }
  else
  {
    // Shift in newest data bit
    patternRegister <<= 1;
    syncBitCounter++;
    if (dataBit)
      patternRegister |= 0x01;

    // Did we recieve the last bit of the last pattern byte?
    if ((patternRegister & 0x0000FFFF) == 0x817E)
    {
      patternRegister = 0;
      BERCounter++;
    }
    // look for pattern for 2000 bits -- patern is transmitted every 128 bits
    else if (syncBitCounter > 2000)
    {
      clearIOEnableInt(RF_DATA_CLK);
      BERDone = true;
    }
  }
}

/*******************************************************************************

DESCRIPTION:


NOTES:

*******************************************************************************/
void Port1ProductionTestEdgeInterrupt(void)
{
  if (getIOIntFlag(RF_DATA_CLK))
  {
    clearIOIntFlag(RF_DATA_CLK);
    RFProductionDataClockEdge();
  }
}