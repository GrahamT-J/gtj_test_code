/*H*****************************************************************************
FILENAME: DVCRMT.H

DESCRIPTION: Remote Device Programming Support (copied from XT30)

Copyright (c) Digital Monitoring Products Inc. 2004 - 2010.  All rights reserved.
*****************************************************************************H*/
#ifndef  DVCRMT_H
#define  DVCRMT_H

#include "types.h"

/*******************************************************************************
FILE CONTEXT
*******************************************************************************/

/*----function prototypes-----------------------------------------------------*/

extern void WordToAsciiHex(byte*, word);
extern void ByteToAsciiHex(byte*, byte);
extern word AsciiHexToWord(byte*);
extern byte AsciiHexToByte(byte*);
extern byte HexCharToBin(byte);
#endif
