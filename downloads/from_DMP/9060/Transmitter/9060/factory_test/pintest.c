/*******************************************************************************
FILENAME: pintest.c

DESCRIPTION: Production Test Support

Copyright (c) Digital Monitoring Products Inc. 2008-2010.  All rights reserved.
*******************************************************************************/


/*******************************************************************************
"Spec"

M2!     Transmit        T
M1!     Receive         R

W1!     Antenna 1
W2!     Antenna 2

B       BER query       B
D       Modulation on   D
d       Modulation off  d


FD09AAC! 902MHz         L
FD00000! 915MHz         M
FD06554! 928MHz         H



All production test code is in the software that ships.
"Functional Test" code runs on powerup if the special functional test pin is low.


At reset, if the test mode pins select the functional test mode,
then the PinTest function below is executed. PinTest is a misnomer since
ICT pin tests are processed in a different test mode.

  Test code listens for asynch data on UART1 and sends commands to CC1101.
  If wireless command is accepted, received character will be echoed. Otherwise, 'F' will be returned.
  If an offset or serial number is set the full value will be returned all characters are received.

    T - Transmit
    R - Receive
    B - BER query
    D - Modulation on
    d - Modulation off
    L - Set frequency to 902 MHz
    M - Set frequency to 915 MHz
    H - Set frequency to 928 MHz

    Oxxxx - Set offset value (and read back)
    Sxxxxxxxx - Set serial number (and read back)
    Y - Set Model Number (and read back)
    Z - Set Model Option (and read back)

    o - read offset value
    s - read serial number
    y - read Model Number
    z - read Model Option
    x - read Version and Date
*******************************************************************************/

/*******************************************************************************
INCLUDE FILES
*******************************************************************************/
#include "msp430x24x.h"
#include "types.h"
#include <string.h>
#include "WirelessKeypadEE.h"
#include "dvcrmt.h"

extern void WriteFlashInfoA(byte * dest, byte val); // ASM fcn
extern void WriteFlashInfoBCD(byte * dest, byte val); // ASM fcn
extern unsigned char ModelOptionsEE;
extern unsigned char DfltModelMsgEE[];
extern unsigned char CodeVersion[];
extern unsigned char CodeDate[];

/*******************************************************************************
PRIVATE DECLARATIONS
*******************************************************************************/
#define NULL_CHAR '\0'

void PinTest(void);
static char waitForChar(void);
static void waitForDataString(byte *string, byte dataLength);
static void setSerialNumber(void);
static void setFrequencyOffset(void);
static void setModelOption(void);
static void setModelNumber(void);
static void returnSerialNumber(void);
static void returnFrequencyOffset(void);
static void returnModelOption(void);
static void returnModelNumber(void);
static void returnVersionAndDate(void);
static void sendDataString(byte *dataString, byte dataStringLength);
static void SetProgInfoBCD(byte *, byte);
static void SetProgInfoA(byte *, byte);

extern char wirelessProductionTest(char command);
extern void I2CInit(void);

/*******************************************************************************
PRIVATE FUNCTIONS
*******************************************************************************/

/*f*****************************************************************************
DESCRIPTION: Waits for a UART1 character

NOTES: Will return NULL_CHAR value if mode changes while waiting

*****************************************************************************f*/
static char waitForChar(void)
{
  byte RecvData = NULL_CHAR;
  volatile byte temp;

  // Check for character
  while (RecvData == NULL_CHAR)
  {
    if (UCA1STAT & UCRXERR)
      temp = UCA1RXBUF;            // Read received character to clear error flags
    else
      if (UC1IFG & UCA1RXIFG)
        RecvData = UCA1RXBUF;
  }
  return RecvData;
}

/*f*****************************************************************************
DESCRIPTION: Waits for a string of UART1 characters

NOTES:

*****************************************************************************f*/
static void waitForDataString(byte *string, byte dataLength)
{
  byte data = 0;

  for (char count = 0; count < dataLength; count++)
  {
    data = waitForChar();
    *string = data;
    string++;
  }
}

/*f*****************************************************************************
DESCRIPTION: Sets Panel Serial Number

NOTES:

*****************************************************************************f*/
static void setSerialNumber(void)
{
  byte serialNumberString[8];

  waitForDataString(serialNumberString, 8);

  for (char i = 0; i < 4; i++) // little endian 32-bit value
  {
    SetProgInfoA((byte *)(SERIAL_NUMBER_ADDRESS + 3 - i), AsciiHexToByte(&serialNumberString[i*2]));
  }
}

/*f*****************************************************************************
DESCRIPTION: Sets On-Board Wireless Frequency Offset

NOTES:

*****************************************************************************f*/
static void setFrequencyOffset(void)
{
  byte frequencyOffsetString[4];
  word val;
  
  waitForDataString(frequencyOffsetString, 4);
  val = AsciiHexToWord(frequencyOffsetString);
  // little endian 16-bit value
  SetProgInfoA((byte *)(FREQUENCY_OFFSET_ADDRESS + 1), val >> 8);
  SetProgInfoA((byte *)FREQUENCY_OFFSET_ADDRESS, val & 0xFF);
}

/*f*****************************************************************************
DESCRIPTION: Sets Model Option byte

NOTES:

*****************************************************************************f*/
static void setModelOption(void)
{
  byte modelOptionString[2];
  byte val;
  
  waitForDataString(modelOptionString, 2);
  val = AsciiHexToByte(modelOptionString);
  SetProgInfoBCD(&ModelOptionsEE, val);
}

/*f*****************************************************************************
DESCRIPTION: Sets Model Number string

NOTES:

*****************************************************************************f*/
static void setModelNumber(void)
{
  byte modelNumberString[4];
  byte i;
  
  waitForDataString(modelNumberString, 4);
  for (i = 0; i < 4; i++)
    SetProgInfoBCD(&DfltModelMsgEE[i], modelNumberString[i]);
}

/*f*****************************************************************************
DESCRIPTION: Returns Serial Number as a string of characters on UART1

NOTES:

*****************************************************************************f*/
static void returnSerialNumber(void)
{
  byte serialNumberString[8];

  for (char i = 0; i < 4; i++) // little endian 32-bit value
  {
    ByteToAsciiHex(&serialNumberString[i*2], *((byte *)SERIAL_NUMBER_ADDRESS + 3 - i));
  }
  sendDataString(serialNumberString, 8);
}

/*f*****************************************************************************
DESCRIPTION: Returns On-Board Wireless Frequency Offset as a string of
             characters on UART1
NOTES:

*****************************************************************************f*/
static void returnFrequencyOffset(void)
{
  byte frequencyOffsetString[4];

  // little endian 16-bit value
  WordToAsciiHex(frequencyOffsetString, (*((byte *)(FREQUENCY_OFFSET_ADDRESS + 1)) << 8) + *((byte *)FREQUENCY_OFFSET_ADDRESS));
  sendDataString(frequencyOffsetString, 4);
}

/*f*****************************************************************************
DESCRIPTION: Returns model option byte as a string of characters on UART1
NOTES:

*****************************************************************************f*/
static void returnModelOption(void)
{
  byte modelOptionString[2];

  ByteToAsciiHex(modelOptionString, ModelOptionsEE);
  sendDataString(modelOptionString, 2);
}

/*f*****************************************************************************
DESCRIPTION: Returns model number as a string of characters on UART1
NOTES:

*****************************************************************************f*/
static void returnModelNumber(void)
{
  sendDataString(DfltModelMsgEE, 4);
}

/*f*****************************************************************************
DESCRIPTION: Returns software version number and date as a string of
             characters on UART1
NOTES:

*****************************************************************************f*/
static void returnVersionAndDate(void)
{
  byte versionAndDateString[13];
  memcpy(&versionAndDateString[0], CodeVersion, 4);
  memcpy(&versionAndDateString[4], CodeDate, 9);
  sendDataString(versionAndDateString, 13);
}


#if PRODUCT_NUMBER != 30
/*f*****************************************************************************
DESCRIPTION: Returns the result of the BERT

NOTES:

*****************************************************************************f*/
static void returnBERTResult(char result)
{
  byte BERTResultString[2];

  ByteToAsciiHex(BERTResultString, result);
  sendDataString(BERTResultString, 2);
}
#endif

/*f*****************************************************************************
DESCRIPTION: Sends dataString of dataStringLength out UART1

NOTES:

*****************************************************************************f*/
static void sendDataString(byte *dataString, byte dataStringLength)
{
  for (byte i = 0; i < dataStringLength; i++)
  {
    while (UCA1STAT & UCBUSY) // wait for UART to not be busy
      ;
    UCA1TXBUF = dataString[i];
  }
}

/*******************************************************************************
PUBLIC FUNCTIONS
*******************************************************************************/
/*f*****************************************************************************
DESCRIPTION: Functional Test

NOTES:

*****************************************************************************f*/
void PinTest(void)
{
  byte RecvData;

#define MAX_TEST_FREQ   4000
#define TEST_DIVISOR    (int)(P_CLK / MAX_TEST_FREQ)
  while (1)
  {
      // Do frequecies and wireless tests (similar to MODE2 of the XT panel)
      RecvData = waitForChar();  // Wait for received character

      switch (RecvData)
      {
        case 'T':  // Transmit
        case 'R':  // Receive
        case 'D':  // Modulation On
        case 'd':  // Modulation Off
        case 'L':  // Set Frequency to 902 MHz
        case 'M':  // Set Frequency to 915 MHz
        case 'H':  // Set Frequency to 928 Mhz
          UCA1TXBUF = wirelessProductionTest(RecvData);  // Echos RecvData if successful, 'F' if failed
        break;

        case 'B':  // BERT
          returnBERTResult(wirelessProductionTest(RecvData));
        break;
        
        case 'O':
          setFrequencyOffset();
          returnFrequencyOffset();
        break;

        case 'S':
          setSerialNumber();
          returnSerialNumber();
        break;

        case 'Y':
          setModelNumber();
          returnModelNumber();
        break;

        case 'Z':
          setModelOption();
          returnModelOption();
        break;

        // read back w/out modifying
        case 'o':
          returnFrequencyOffset();
        break;

        case 's':
          returnSerialNumber();
        break;

        case 'y':
          returnModelNumber();
        break;

        case 'z':
          returnModelOption();
        break;

        case 'x':
          returnVersionAndDate();
        break;

        default:
          UCA1TXBUF = 'F';  // Unknown command
        break;
      }

  }  // end of do while (1)
}


/*f*****************************************************************************
DESCRIPTION: 

NOTES:

*****************************************************************************f*/
static void SetProgInfoA(byte * dest, byte val)
{
  WriteFlashInfoA(dest, val); // don't enable the watchdog when writing to FLASH
}

/*f*****************************************************************************
DESCRIPTION: 

NOTES:

*****************************************************************************f*/
static void SetProgInfoBCD(byte * dest, byte val)
{
  WriteFlashInfoBCD(dest, val); // don't enable the watchdog when writing to FLASH
}
