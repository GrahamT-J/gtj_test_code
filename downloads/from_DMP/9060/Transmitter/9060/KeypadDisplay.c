/*******************************************************************************
FILENAME:

DESCRIPTION:

NOTES:

Copyright (c) Digital Monitoring Products Inc. 2010.  All rights reserved.

*******************************************************************************/
/*----compilation control-----------------------------------------------------*/

/*******************************************************************************
INCLUDE FILES
*******************************************************************************/
/*----system and platform files (external libraries) -------------------------*/
#include <string.h>

/*----program files-----------------------------------------------------------*/
#include "KeypadDisplay.h"
#include "ResourceManager.h"
#include "OTAMsg.h"
#include "StringFunctions.h"
#include "TransmitterProtocol.h"
#include "ProtocolTiming.h"

byte exitDelayCount = EXIT_COUNT_DISABLED;

/*******************************************************************************
EXTERNAL REFERENCES   Only used if not available in a header file.
*******************************************************************************/
/*----data declarations-------------------------------------------------------*/
extern char DisplayRegister;
extern char Input_Buffer;

unsigned char exitDelayCountPanel;    // count sent by panel (0-254, where 0 means disabled)
unsigned char exitDelayCountPosition; // position in the display to put the count
unsigned int timer1000ms;

/*----function prototype------------------------------------------------------*/
static void WriteToDisplayRAM(char * dest, char * src, unsigned char len);
static void ReadFromDisplayRAM(char * dest, char * src, unsigned char len);

/*******************************************************************************
PRIVATE DECLARATIONS
*******************************************************************************/
/*----defines, typedefs, structs, unions, enums-------------------------------*/
#define ONE_SEC_HALF_MS_BASED  2000  // 1 second timer is decr every 1/2 mS

/*----data declarations-------------------------------------------------------*/

/*----function prototype------------------------------------------------------*/

/*----macros------------------------------------------------------------------*/

/*******************************************************************************
PRIVATE FUNCTION DEFINITIONS
*******************************************************************************/
/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/


/*******************************************************************************
PUBLIC FUNCTION DEFINITIONS
*******************************************************************************/

/*******************************************************************************

DESCRIPTION: Copies display data directly to the LCD if
   normal (panel) mode, else copies to the resource manager
   display buffers.

NOTES:

*******************************************************************************/
void processKeypadDisplay(char * displayData, char exitCount, char exitPosition)
{
  if (currentKeypadOpModePriority == PANEL_PRIORITY)
  {
    WriteToDisplayRAM(&Input_Buffer, displayData, 16);
    WriteToDisplayRAM(&DisplayRegister+16, displayData+16, 16);
  }
  else
  {
    ReadFromDisplayRAM(RM_DisplayInputBuffer[PANEL_PRIORITY], displayData, 16);
    ReadFromDisplayRAM(&RM_DisplayRegisterArray[PANEL_PRIORITY][0], "                ", 16);
    ReadFromDisplayRAM(&RM_DisplayRegisterArray[PANEL_PRIORITY][16], displayData+16, 16);
  }
  exitDelayCountPanel = exitCount;
  exitDelayCountPosition = exitPosition;
  
  if (exitDelayCountPanel)
  {
    exitDelayCount = exitDelayCountPanel;
    timer1000ms = ONE_SEC_HALF_MS_BASED;
  }
  else  // disable exit delay processing
  {
    exitDelayCount = EXIT_COUNT_DISABLED;
  }
}

/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
void CopyFromKeypadDisplay(char * inputBuf, char * dispReg)
{
  ReadFromDisplayRAM(inputBuf, &Input_Buffer, 16);
  ReadFromDisplayRAM(dispReg, &DisplayRegister, 32);
}

/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
void CopyToKeypadDisplay(char * inputBuf, char * dispReg)
{
  WriteToDisplayRAM(&Input_Buffer, inputBuf, 16);
  WriteToDisplayRAM(&DisplayRegister, dispReg, 32);
}


/*******************************************************************************

DESCRIPTION:

NOTES:
*******************************************************************************/
void ProcessExitDelay()
{
  char * ptr;

  // If display of exit delay is enabled by the panel, and the count is valid
  if (exitDelayCountPanel && (exitDelayCount != EXIT_COUNT_DISABLED))
  {
    timer1000ms = ONE_SEC_HALF_MS_BASED;

    if (currentKeypadOpModePriority == PANEL_PRIORITY) // don't write to the display if panel doesn't own that resource
    {
      // The panel initiates display of an exit delay count by sending a start count and a display position for the count.
      // These 2 values are appended to the "EXIT" display message that the panel sends.
      // The panel can disable the count by sending count = 0.

      ptr = &DisplayRegister + 16;
      byte offset = exitDelayCountPosition;
      if (offset > MAX_DISPLAY_POSITION)
        offset = MAX_DISPLAY_POSITION;
      
      ptr[offset]     = '0' + exitDelayCount / 100;
      ptr[offset + 1] = '0' + exitDelayCount % 100 / 10;
      ptr[offset + 2] = '0' + exitDelayCount % 10;

      //remove leading 0s
      if (ptr[offset] == '0')
      {
        ptr[offset] = ' ';
        if (ptr[offset + 1] == '0')
          ptr[offset + 1] = ' ';
      }
    }
    
    if (exitDelayCount)
      exitDelayCount--;
  }
}
/*******************************************************************************

DESCRIPTION:  Display serial #

NOTES:  Converts 32-bit serial number to ASCII and displays it.
    This fcn is called only when the panel is in local mode, and writes directly
    to the display.
	
*******************************************************************************/
void DisplaySerialNum()
{
  char * ptr;
  
  ptr = &DisplayRegister; //&Input_Buffer;
  strncpy(ptr, "                SERIAL#:", 24);
  ptr += 24;
  longToAscii(*(long *)SERIAL_NUMBER_ADDRESS, ptr, MAX_SERIAL_NUMBER_LEN);
}

/*******************************************************************************

DESCRIPTION:  Process survey menu

NOTES:
	
*******************************************************************************/
void ProcessSurveyMenu()
{
  SetSurveyPriority();
  strncpy(&DisplayRegister, "                RF SURVEY       ", 32); // write to the display
}

/*******************************************************************************

DESCRIPTION: Selectively write only changed chars to the display RAM.
  This is needed when running from battery to prevent the backlights
  from turning on each time the panel refreshes the keypad's display
  with the same information.

NOTES:  Bit 7 is set by the low level display SW when it writes the
  char to the LCD.

*******************************************************************************/
static void WriteToDisplayRAM(char * dest, char * src, unsigned char len)
{
  int i;

  for (i = 0; i < len; i++, dest++, src++)
  {
      if ((*dest & 0x7F) != *src)  // mask off bit 7 "displayed" flag
        *dest = *src;
  }
}

/*******************************************************************************

DESCRIPTION:  Read current display but mask off the "displayed" flag

NOTES:

*******************************************************************************/
static void ReadFromDisplayRAM(char * dest, char * src, unsigned char len)
{
  int i;

  for (i = 0; i < len; i++, dest++, src++)
    *dest = *src & 0x7F;
}

