/*H*****************************************************************************
FILENAME:

DESCRIPTION:

NOTES:

Copyright (c) Digital Monitoring Products Inc. 2009.  All rights reserved.

*****************************************************************************H*/
#ifndef  INTERRUPT_INPUTS_H
#define  INTERRUPT_INPUTS_H

/*----compilation control-----------------------------------------------------*/
#include "packet.h"
#include "sleep.h"
/*******************************************************************************
INCLUDE FILES   Note: Minimize nesting of header files.
*******************************************************************************/
/*----system and platform files-----------------------------------------------*/

/*----program files-----------------------------------------------------------*/

/*******************************************************************************
FILE CONTEXT
*******************************************************************************/
/*----defines, typedefs, structs, unions, enums-------------------------------*/
// note, for proper sync, the debounce time must equal an integral number of hops
// with no fraction.
// For example, with 32 hops per second and a debounce of 500mS, there are exactly
// 16 hops during debounce.
enum
{
  DEBOUNCE_MS = 500,
  DEBOUNCE_TIME = ((long)DEBOUNCE_MS * SLEEP_TICKS_PER_SECOND) / 1000
};

// reuse same bits from message byte
typedef enum
{
  CS_REED_SW    = MB_ALARM_0,
  CS_CONTACT_SW = MB_ALARM_1,
  CS_TAMPER_SW  = MB_TAMPER
} ContactState;

typedef enum
{
  KEYTEST_NONE,
  KEYTEST_KEY,
  KEYTEST_CARDREAD
} KeyTestType;

// These defines must match the ASM equates in kpd_equates.h for
//   ShortcutKeyBit          EQU     40h     ; indicate key pressed for 2 seconds.
//   CardReadBit             EQU     20h
#define SEQUENCE_BIT         0x80
#define SHORTCUT_KEY_BIT     0x40
#define CARD_READ_BIT        0x20

#define KEY_MASK             (SEQUENCE_BIT | SHORTCUT_KEY_BIT | 0x1F)    // shortcut key bit and the 5-bit key code
#define CARD_READ_MASK       0x0F       // each card read value is stored in the lower 4 bits
#define NUM_CARD_READ_DIGITS 10
#define CARD_READ_PAD_BYTE   0xBB       // 2 nibbles of 0xB

/*----data descriptions-------------------------------------------------------*/

/*----data declarations-------------------------------------------------------*/
extern ContactState contactSteadyState;

/*----macros------------------------------------------------------------------*/

/*----function prototypes-----------------------------------------------------*/
void initInputs(void);
void startInputDebounce(short multiplier);
void testBattery(void);
ZoneMessageByte getInputState(void);
#ifdef DMP_1100_PRODUCTION_TEST
  void clearContactState(void);
#endif
void getZoneMessageByte(ZoneMessageByte* ptr);
bool TestForZoneMessageToSend(void);
KeyTestType TestForKeyPressOrCardData(void);
void getKeyPressOrCardData(unsigned char* dest, KeyTestType destType);
void ClearKeyPress(void);
void SaveKeypress(unsigned char key);
#endif                                  /* end of file */
