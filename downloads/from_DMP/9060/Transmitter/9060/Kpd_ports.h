/** @package

    Kpd_ports.h

    Copyright(c) Digital Monitoring Products 2003

    Author: DIGITAL MONITORING PRODUCTS
    Created: HP  2/27/04 11:27:55 AM
	Last change: HP 8/27/2004 8:20:10 PM
*/

#include "msp430x24x.h"

/* The 1190 has these unused GPIO pins:
 * P2.1
 * P6.3
 * P6.4
 * P6.5
 * P6.6
 * P6.7
 *
 * These pins are used by wireless and are defined elsewhere:
 * P1.0 (output)
 * P1.2 (output)
 * P1.3 (input)
 * P2.0 (output)
 * P2.4 (bi-directional, reset as input)
 * P2.7 (input)
 *
 * These pins are dedicated to bootstrap loading (by TI)
 * P1.1
 * P2.2
 *
 * These pins are used by factory test
 * P1.5
 * P1.7
 * P3.0
 * P3.1
 * P4.0
 * P4.1
*/

/************************************************************
* DIGITAL I/O Port1/2
************************************************************/

#define  TamperSwitch        0x10        //Tamper switch input
#define  TamperSwitchPORT    P1IN
#define  KHZ_125_SIGNAL      0x40        //Timer Compare drives this pin

#define  RESET_P1DIR         01100111B   //bit 6 (125 KHz), bit 5 (test mode ack), bits 2,0 (wireless) and bit 1 (bootload) are outputs. Others are inputs
#define  RESET_P1SEL         01000000B   //drive P1.6 with TA1 output compare timer
#define  RESET_P1REN         10100010B   //enable pull-downs on unused pins and bootstrap pin

#define  LCD_RS              0x08        //LCD module RS pin output
#define  LCD_RSPORT          P2OUT
#define  LCDBackLite         0x20        //LCD backlight control output
#define  LCDBackLitePORT     P2OUT
#define  LCD_RW              0x40        //LCD module R/W pin output
#define  LCD_RWPORT          P2OUT
#define  RESET_P2DIR         01101001B   //bit 2 (bootload) and others are inputs
#define  RESET_P2SEL         00000000B
#define  RESET_P2REN         00000110B   //enable pull-downs on unused pins and bootstrap pin

/************************************************************
* DIGITAL I/O Port3/4
************************************************************/

#define  VolumeOutPin        0x04        //Speaker volume control output
#define  VolumeOutPinPORT    P3OUT
#define  AudioOutPin         0x08        //Speaker frequency control output
#define  AudioOutPinPORT     P3OUT
#define  RedBLLogo           0x10
#define  RedBLLogoPORT       P3OUT
#define  GreenBLLogo         0x20
#define  GreenBLLogoPORT     P3OUT
#define  RedBackLite         0x40        //Red keypad backlight control output
#define  RedBackLitePORT     P3OUT
#define  GrnBackLite         0x80        //Green keypad backlight control output
#define  GrnBackLitePORT     P3OUT

#define  RESET_P3DIR         11111100B
#define  RESET_P3SEL         00000000B
#define  RESET_P3REN         00000011B   //enable pull-downs on unused pins

#define  RFID_DATA_INPUT     0x04        //P4.2 Timer capture pin
#define  LCD_E               0x08        //LCD module E pin output
#define  LCD_EPORT           P4OUT
#define  Column1In           0x10        //Keypad column 1 scan input
#define  Column2In           0x20        //Keypad column 2 scan input
#define  Column3In           0x40        //Keypad column 3 scan input
#define  Column4In           0x80        //Keypad column 4 scan input
#define  ColumnMask          0xF0        //mask for all 4 columns
#define  KeypadColumnPort    P4IN

#define  RESET_P4DIR         00001000B
#define  RESET_P4SEL         00000100B   //TB2 input capture for RFID
#define  RESET_P4REN         00000011B   //enable pull-downs on unused pins

/************************************************************
* DIGITAL I/O Port5/6
************************************************************/
#define  DsplyPort           P5OUT
#define  DsplyPortIn         P5IN
#define  DsplyPortDir        P5DIR

#define  KeypadRowPort       P5OUT
#define  KeypadRowPortDir    P5DIR

#define  RESET_P5DIR         11111111B
#define  RESET_P5SEL         00000000B
#define  RESET_P5REN         00000000B

// The C fcn SetupChargerIO() initializes registers for P6.0, P6.1, P6.2
// and is called after ASM code initializes.
#define  ChrgControl         0x04
#define  ChrgControlPORT     P6OUT

#define  RESET_P6DIR         00000100B
#define  RESET_P6SEL         00000000B   //ADC doesn't depend on SEL bits
#define  RESET_P6REN         11111000B   //enable pull-downs on unused pins


// ICT and Functional test pins

#define  AUTO_MODEL_LOAD_CLK          BIT0           // P4.0
#define  AUTO_MODEL_READ_CLK          BIT1           // P4.1
#define  AUTO_MODEL_LOAD_CLK_PORT     P4IN           // Write model #, etc.
#define  AUTO_MODEL_READ_CLK_PORT     P4IN           // Read model #, etc.

#define  AutoTestACKOut               0x20   // P1.5 ACK
#define  AutoTestACKOutPORT           P1OUT

#define  AutoTestModelInPort          P5IN
#define  AutoTestModelOutPort         P5OUT
#define  AutoTestModelOutDir          P5DIR

#define  AUTO_TEST_MODE_BIT1          BIT0  // P3.0
#define  AUTO_TEST_MODE_BIT1_PORT     P3IN

#define  AUTO_TEST_MODE_BIT2          BIT1  // P3.1
#define  AUTO_TEST_MODE_BIT2_PORT     P3IN

#define  AUTO_TEST_SEL_MASK           (BIT0 | BIT1) // P3.0, P3.1 determine which ICT tests to run
#define  AUTO_TEST_SEL_PORT           P3IN

#define  AUTO_TEST_PT1                0   // Pass-Thru test Mode #1
#define  AUTO_TEST_PT2                1   // Pass-Thru test Mode #2
#define  AUTO_TEST_MODEL_NUM          2   // Model No. read/write
#define  AUTO_TEST_EXIT               3   // exit test mode

#define  AUTO_TEST_MODE_FUNC_BIT      BIT7  // P1.7 is low to select Functional Testing
#define  AUTO_TEST_MODE_FUNC_BIT_PORT P1IN
