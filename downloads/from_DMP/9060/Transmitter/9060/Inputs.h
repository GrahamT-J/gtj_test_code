/*H*****************************************************************************
FILENAME:

DESCRIPTION:

NOTES:

Copyright (c) Digital Monitoring Products Inc. 2009.  All rights reserved.

*****************************************************************************H*/
#ifndef INPUTS_H
  #define INPUTS_H

  #ifdef USE_SCANNED_INPUTS
    #include "ScannedInputs.h"
  #else
    #include "InterruptInputs.h"
  #endif

#endif

