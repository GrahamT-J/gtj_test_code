/*******************************************************************************
FILENAME: WirelessKeypadEE.h

DESCRIPTION: INFO EEPROM data definitions

NOTES:

Copyright (c) Digital Monitoring Products Inc. 2010.  All rights reserved.

*******************************************************************************/
#ifndef  WIRELESSKEYPADEE_H
#define  WIRELESSKEYPADEE_H

// INFO A FLASH segment data
#define SERIAL_NUMBER_ADDRESS    0x10C0  // 4 bytes
#define FREQUENCY_OFFSET_ADDRESS 0x10C4  // 2 bytes

#define START_INFO_A_FLASH       0x10C0
#define END_INFO_A_FLASH         0x10C5
#define SIZE_INFO_A_FLASH        (END_INFO_A_FLASH - START_INFO_A_FLASH + 1)
#endif
