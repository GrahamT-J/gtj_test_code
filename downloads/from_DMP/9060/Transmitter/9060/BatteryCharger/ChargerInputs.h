/***********************************************************
*
* ChargerInputs.h
*
* External linkage for ChargerInputs.c
*
* "Copyright (c) Digital Monitoring Products, Inc. 2009"
*
***********************************************************/

#ifndef CHARGERINPUTS_H
#define CHARGERINPUTS_H

typedef struct
{
  BITFIELD powerFail        : 1;
  BITFIELD batteryLow       : 1;
  BITFIELD batteryMissing   : 1;
  BITFIELD reserved         : 5;
} AnalogInputStatus;

// PUBLIC MACROS ///////////////////////////////////////////

// PUBLIC VARIABLES ////////////////////////////////////////
extern AnalogInputStatus currentAnalogState;

// PUBLIC FUNCTION PROTOTYPES //////////////////////////////
extern void SetupChargerIO(void);
extern void ProcessAnalogTestStep(void);
extern void StartAnalogTests(void);
extern void ProcessDebounceInterrupt(void);
extern bool BatteryIsLow(void);
extern bool PowerHasFailed(void);
extern void ClearLatchedStatus(void);
extern bool ChargerStateLatched(void);
#endif

