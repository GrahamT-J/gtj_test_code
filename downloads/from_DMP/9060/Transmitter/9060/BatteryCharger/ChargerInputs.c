/***********************************************************
*
* ChargerInputs.c
*
* Description
*
* "Copyright (c) Digital Monitoring Products, Inc. 2009"
*
***********************************************************/

// INCLUDES ////////////////////////////////////////////////

#include "types.h"
#include "ChargerInputs.h"
#include "ChargerIOPortPins.h"
#include "BitMacros.h"
#include "io430x24x.h"

// PRIVATE MACROS //////////////////////////////////////////

#define SET_ADC_SHORT_TIMER_MS(time) (BattChargerShortTimer = ((time + 24)/ 48)) // 48 mS units, round to closest 48 mS
#define BACKLIGHT_TIMEOUT (10000/48) // 10 Sec / 48 mS -- 8-bit value, don't exceed 255

enum
{
  INPUT_POWER_THRESHOLD = 1541, //(mv) Value multiplied by 4.736 plus 700 gives the actual input voltage at J22
  LOW_BAT_VOLTAGE_THRESHOLD = 1800, //(mv) Value multiplied by 2 gives the actual Bat voltage
  BAT_MISSING_VOLTAGE_THRESHOLD = 500, //(mv) Value multiplied by 2 give the actual input voltage
  ADC_VOLTAGE_REFERENCE = 3300, //The ADC is configured to use this voltage and GND as the reference for Input Power
  ADC_BAT_VOLTAGE_REFERENCE = 2500, //The ADC is configured to use this voltage and GND as the reference for Batt measurments
  ADC_POWER_FAILED_THRESHOLD = (short)((long)4095 * INPUT_POWER_THRESHOLD         / ADC_VOLTAGE_REFERENCE),
  ADC_LOW_BAT_THRESHOLD =      (short)((long)4095 * LOW_BAT_VOLTAGE_THRESHOLD     / ADC_BAT_VOLTAGE_REFERENCE),
  ADC_BAT_MISSING_THRESHOLD =  (short)((long)4095 * BAT_MISSING_VOLTAGE_THRESHOLD / ADC_BAT_VOLTAGE_REFERENCE),
  INPUT_POWER_THRESHOLD_COUNTER = 2,
  BAT_THRESHOLD_COUNTER = 2,
  BAT_MISSING_COUNTER = 2,
  TAMPER_DEBOUNCE_COUNTER = 2
};

typedef enum
{
  AT_INITIAL_STEP = 0,
  AT_SAMPLE_BAT_LOW = AT_INITIAL_STEP,
  AT_SET_BAT_LOW,
  AT_CHARGER_OFF,
  AT_SAMPLE_BAT_MISSING,
  AT_SET_BAT_MISSING,
  AT_CHARGER_ON,
  AT_SAMPLE_POWER_FAIL,
  AT_SET_POWER_FAIL,
  AT_LAST_STEP = AT_SET_POWER_FAIL,
  AT_NUM_STEPS
} AnalogTestStep;

typedef struct
{
  BITFIELD failCount    : 4;
  BITFIELD restoreCount : 4;
} InputDebounce;

typedef union
{
  struct
  { //The order of these bitfields establish the priority of the ChargerState
    BITFIELD powerFail      : 1;
    BITFIELD batteryLow     : 1;
    BITFIELD batteryMissing : 1;
    BITFIELD reserved       : 5;
  };
  unsigned char asByte;
} ChargerState;

// PUBLIC VARIABLES ///////////////////////////////////////
bool ADCTimerEnable = false;
unsigned char BattChargerShortTimer;
unsigned char BacklightTimer = 0xFF;
AnalogInputStatus currentAnalogState = {.powerFail = false, .batteryLow = false, .batteryMissing = false};

// PRIVATE VARIABLES ///////////////////////////////////////

static AnalogTestStep currentAnalogTestStep = AT_INITIAL_STEP;
static ChargerState currentChargerState = {.asByte = 0};
static ChargerState latchedChargerState = {.asByte = 0};
static InputDebounce lowBatteryDebounce = {.restoreCount = 0, .failCount = 0};
static InputDebounce batMissingDebounce = {.restoreCount = 0, .failCount = 0};
static InputDebounce inputPowerDebounce = {.restoreCount = 0, .failCount = 0};

// PRIVATE FUNCTION PROTOTYPES /////////////////////////////

/*
*  Description: This function will either update the currentContactState or latchedChargerState.
*                 This function should be called if the state of any contact has changed or if
*                 the latched state has been cleared
*/
void UpdateChargerStatus(void)
{
  ChargerState tempChargerState;

  tempChargerState.batteryMissing = currentAnalogState.batteryMissing;
  tempChargerState.batteryLow = currentAnalogState.batteryLow;
  tempChargerState.powerFail = currentAnalogState.powerFail;

  // if we have a state latched and the new state is a higher priority, or the new state is higher
  // than the current state, update the latched state
  if (((latchedChargerState.asByte != 0) && (tempChargerState.asByte > latchedChargerState.asByte)) ||
      (tempChargerState.asByte > currentChargerState.asByte))
  {
    latchedChargerState.asByte = tempChargerState.asByte; // latch the new status
  }
  else // latching not required, update the current state
  {
    currentChargerState.asByte = tempChargerState.asByte;
  }
}

// PUBLIC FUNCTIONS ////////////////////////////////////////

/*
*  Description: Function will intialize and setup GPIO for the battery charger
*
*/
void SetupChargerIO()
{
  //setup IO pin direction
  setIODirIn(AC_IN_VOLTAGE);
  setIODirIn(BAT_VOLTAGE);
  setIODirOut(CHARGE_DISABLE);

  //setup IO pins mode
  clearIOSel(CHARGE_DISABLE);
  setIOSel(AC_IN_VOLTAGE); //ADC input
  setIOSel(BAT_VOLTAGE);

  //setup ADC to sample AC & BAT voltage
  ADC12MCTL0 = INCH_1 + EOS;           //AD1 input to sample AC_IN_VOLTAGE
  ADC12MCTL1 = INCH_0 + SREF_1 + EOS;  //AD0 input to sample BAT_VOLTAGE with 2.5v ref
}

/*
*  Description: Will clear any latched state and will latch any new states if needed.
*
*/
void ClearLatchedStatus(void)
{
  if (latchedChargerState.asByte != 0)
  {
    currentChargerState.asByte = latchedChargerState.asByte;
    latchedChargerState.asByte = 0; // clear the currently latched state
    UpdateChargerStatus(); // look for a new, lower priority state to latch
  }
}

/*
*  Description: Will return true if a device state is currently latched and needs
*                to be communicated to the panel
*/
bool ChargerStateLatched(void)
{
  return (latchedChargerState.asByte > currentChargerState.asByte);
}

/*
*  Description: Function will start the analog test state machine
*
*/
void StartAnalogTests(void)
{
  if (!ADCTimerEnable)
  {
    ADC12CTL0 = SHT0_15 + REFON + REF2_5V + ADC12ON; //Turn on ADC and setup 1024 clock cycles/sample
    ADC12CTL1 = SHP + ADC12DIV_7 + ADC12SSEL_3; //Clock source = SMCLK div 8, clock drives sample time
    currentAnalogTestStep = AT_INITIAL_STEP;
    SET_ADC_SHORT_TIMER_MS(250);
    ADCTimerEnable = true;
  }
}

/*
*  Description: This function will process the states from the analog test state machine
*
*/
void ProcessAnalogTestStep(void)
{

  switch(currentAnalogTestStep)
  {
    case AT_SAMPLE_BAT_MISSING:
    case AT_SAMPLE_BAT_LOW:
    {
      //Kick off a ADC sample of batt voltage
      ADC12CTL0 &= ~ENC;
      ADC12CTL1_bit.CSTARTADD = 1;
      ADC12CTL0 |= (ENC + ADC12SC);
      break;
    }

    case AT_SET_BAT_LOW:
    {
      if (ADC12MEM1 < ADC_LOW_BAT_THRESHOLD)
      {
      //Bat Voltage is below threshold (when the charger is on)
        if (lowBatteryDebounce.failCount < BAT_THRESHOLD_COUNTER)
        {
          if (++lowBatteryDebounce.failCount >= BAT_THRESHOLD_COUNTER)
          {
            currentAnalogState.batteryLow = true;
            UpdateChargerStatus();
          }
        }
        lowBatteryDebounce.restoreCount = 0;
      }
      else
      {
      //Bat Voltage is above or equal to threshold (when the charger is on)
        if (lowBatteryDebounce.restoreCount < BAT_THRESHOLD_COUNTER)
        {
          if (++lowBatteryDebounce.restoreCount >= BAT_THRESHOLD_COUNTER)
          {
            currentAnalogState.batteryLow = false;
            UpdateChargerStatus();
          }
        }
        lowBatteryDebounce.failCount = 0;
      }
      break;
    }

    case AT_CHARGER_OFF:
    {
      //Turn off the battery charger and wait for the voltage to settle if the
      //battery is not currently being charged
      //if (!currentContactState.batteryCharging) (1190 has no input pin to sense if battery is currently being charged)
        setIO(CHARGE_DISABLE);

      break;
    }

    case AT_SET_BAT_MISSING:
    {
      if (ADC12MEM1 < ADC_BAT_MISSING_THRESHOLD)
      {
      //Bat Voltage is below threshold (when the charger is off)
        if (batMissingDebounce.failCount < BAT_MISSING_COUNTER)
        {
          if (++batMissingDebounce.failCount >= BAT_MISSING_COUNTER)
          {
            currentAnalogState.batteryMissing = true;
            UpdateChargerStatus();
          }
        }
        batMissingDebounce.restoreCount = 0;
      }
      else
      {
      //Bat Voltage is above or equal to threshold (when the charger is off)
        if (batMissingDebounce.restoreCount < BAT_MISSING_COUNTER)
        {
          if (++batMissingDebounce.restoreCount >= BAT_MISSING_COUNTER)
          {
            currentAnalogState.batteryMissing = false;
            UpdateChargerStatus();
          }
        }
        batMissingDebounce.failCount = 0;
      }
      break;
    }

    case AT_CHARGER_ON:
    {
      clearIO(CHARGE_DISABLE);
      break;
    }

    case AT_SAMPLE_POWER_FAIL:
    {
      ADC12CTL0 &= ~ENC;
      ADC12CTL1_bit.CSTARTADD = 0;
      ADC12CTL0 |= (ENC + ADC12SC);
      break;
    }

    case AT_SET_POWER_FAIL:
    {
      if (ADC12MEM0 < ADC_POWER_FAILED_THRESHOLD)
      {
        if (inputPowerDebounce.failCount < INPUT_POWER_THRESHOLD_COUNTER)
        {
          if (++inputPowerDebounce.failCount >= INPUT_POWER_THRESHOLD_COUNTER)
          {
            currentAnalogState.powerFail = true;
            UpdateChargerStatus();
          }
        }
        inputPowerDebounce.restoreCount = 0;
      }
      else
      {
        if (inputPowerDebounce.restoreCount < INPUT_POWER_THRESHOLD_COUNTER)
        {
          if (++inputPowerDebounce.restoreCount >= INPUT_POWER_THRESHOLD_COUNTER)
          {
            currentAnalogState.powerFail = false;
            UpdateChargerStatus();
          }
        }
        inputPowerDebounce.failCount = 0;
      }
      break;
    }

    default:
      break;
  }

  switch(currentAnalogTestStep)
  {
    case AT_CHARGER_OFF:
      SET_ADC_SHORT_TIMER_MS(2000);
      break;

    case AT_SAMPLE_BAT_LOW:
    case AT_SAMPLE_BAT_MISSING:
    case AT_SAMPLE_POWER_FAIL:
      SET_ADC_SHORT_TIMER_MS(1000);
      break;

    default:
      SET_ADC_SHORT_TIMER_MS(250);
      break;
  }

  if (currentAnalogTestStep < AT_LAST_STEP)
  {
    currentAnalogTestStep++;
  }
  else
  {
    ADCTimerEnable = false; //disable the timer
    ADC12CTL0 = 0; //disable the ADC12 module
    ADC12CTL1 = 0;
  }
}

void SavePower(void)
{
  // This fcn is called every 48 mS (by ASM code).
  // If running from battery, force the green backlight LEDs and LCD backlight off
  // after a timer expires. If the backlight is red, however, this timer has
  // no effect. When BacklightTimer reaches 0, it enables turning off backlights.
  if (currentAnalogState.powerFail)
  {
    if (BacklightTimer)
      BacklightTimer--;
  }
  else
  {
    BacklightTimer = BACKLIGHT_TIMEOUT;
  }
}

// called by ASM code
void SetDefaultBacklightTimer(void)
{
  BacklightTimer = BACKLIGHT_TIMEOUT;
}
