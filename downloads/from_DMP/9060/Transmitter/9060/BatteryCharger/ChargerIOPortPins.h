/*******************************************************************************
FILENAME: ChargerIOPortPins.h

DESCRIPTION:

NOTES:

Copyright (c) Digital Monitoring Products Inc. 2009.  All rights reserved.

*******************************************************************************/

#ifndef CHARGERIOPORTPINS_H
#define CHARGERIOPORTPINS_H

//-------------------------------------------------

#define CHARGE_DISABLE_MASK       BIT2
#define CHARGE_DISABLE_IN         P6IN
#define CHARGE_DISABLE_DIR        P6DIR
#define CHARGE_DISABLE_SEL        P6SEL
#define CHARGE_DISABLE_OUT        P6OUT

/*************** Not used on the 1190 keypad
#define CHARGING_MASK             BIT3
#define CHARGING_IN               P2IN
#define CHARGING_OUT              P2OUT
#define CHARGING_DIR              P2DIR
#define CHARGING_SEL              P2SEL
#define CHARGING_IES              P2IES
#define CHARGING_IE               P2IE
#define CHARGING_IFG              P2IFG
*************/

#define AC_IN_VOLTAGE_MASK        BIT1
#define AC_IN_VOLTAGE_DIR         P6DIR
#define AC_IN_VOLTAGE_SEL         P6SEL

#define BAT_VOLTAGE_MASK          BIT0
#define BAT_VOLTAGE_DIR           P6DIR
#define BAT_VOLTAGE_SEL           P6SEL

#endif
