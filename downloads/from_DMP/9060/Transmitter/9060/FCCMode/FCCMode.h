/*******************************************************************************
FILENAME:     FCCMode.h

DESCRIPTION:

NOTES:

Copyright (c) Digital Monitoring Products Inc. 2010.  All rights reserved.

*******************************************************************************/
#ifndef  FCC_MODE_H
#define  FCC_MODE_H

extern unsigned char ProcessFCCKeypress(unsigned char keyPressed);

#endif
