/*******************************************************************************
FILENAME:

DESCRIPTION:

NOTES:

Copyright (c) Digital Monitoring Products Inc. 2004-2016.  All rights reserved.

*******************************************************************************/
/*----compilation control-----------------------------------------------------*/

/*******************************************************************************
INCLUDE FILES
*******************************************************************************/
/*----system and platform files (external libraries) -------------------------*/
#include <string.h>

/*----program files-----------------------------------------------------------*/
#include "types.h"
#include "Sleep.h"
#include "TransmitterProtocol.h"
#include "TransmitterMain.h"
#include "WirelessCommunication.h"
#include "Inputs.h"
#include "ioportpins.h"
#include "InterruptInputs.h"
#include "ResourceManager.h"
#include "KeypadDisplay.h"
#include "OTAMsg.h"
#include "version.h"

/*******************************************************************************
EXTERNAL REFERENCES   Only used if not available in a header file.
*******************************************************************************/
/*----data declarations-------------------------------------------------------*/
extern SlotNumber currentSlotNumber;

/*----function prototype------------------------------------------------------*/

/*******************************************************************************
PRIVATE DECLARATIONS
*******************************************************************************/
/*----defines, typedefs, structs, unions, enums-------------------------------*/
typedef enum
{
  TMT_SERIAL_MESSAGE,
  TMT_ZONE_MESSAGE,
  TMT_CHECKIN_MESSAGE,
  TMT_COMMAND_ACK_SETUP,
  TMT_NO_MESSAGE,
  TMT_SINGLE_KEY_PRESS_MESSAGE,
  TMT_CARD_READ_MESSAGE,
  TMT_RETRY_LAST_MESSAGE_TYPE_SENT
} TransmitterMessageType;

enum
{
  MAX_KEYPAD_RETRIES = 5
};

#define CMD_KEY_CODE         0x0B       // CMD key, not a shortcut, not a card read

enum
{
  AUTO_GEN_CMD_DISABLED = 0,
  AUTO_GEN_CMD_ENABLED = 1,
  AUTO_GEN_CMD_ENQUEUED = 2,
};

typedef struct
{
  BITFIELD autoCmdGenState :2;  // There are 3 states of AUTO_GEN_CMD_...
  BITFIELD reserved        :6;
} AutoGenerateType;


/*----data declarations-------------------------------------------------------*/
AckStatus ackStatus;
static AutoGenerateType autoGenerate;

/*----function prototype------------------------------------------------------*/
void formatTransmitterMessage(TransmitterMessageType messageType, ZoneMessageByte msgToSend);
SlotTransmitterNumber myZoneNumber = INVALID_TRANSMITTER_NUMBER;
static SlotNumber myCheckinSlotNumber = INVALID_SLOT;
static unsigned short transmitterCrc;
#ifdef DMP_1142_TRANSMITTER
ZoneFlags myZoneFlags;
#endif
static unsigned char keypadRetries = 0;
static unsigned char savedCardRead[KEYPAD_CARD_READ_MESSAGE_LENGTH];

/*----macros------------------------------------------------------------------*/

/*******************************************************************************
PRIVATE FUNCTION DEFINITIONS
*******************************************************************************/
/*******************************************************************************

DESCRIPTION: Sets CRC for transmitter message, including any extra
information that is expected, and sets transmitter packet length

NOTES:

*******************************************************************************/
static void finalizeTransmitterMessage(char size, AdditionalCheckItems items)
{
  unsigned short crc = INITIAL_CRC;
  // first, start with any additional items
  if (items & ACI_HOUSE_CODE)
  {
    calcCRC16ForChar(houseCode, &crc);
  }
  if (items & ACI_ZONE_NUMBER)
  {
    calcCRC16ForChar(myZoneNumber >> 8, &crc);
    calcCRC16ForChar(myZoneNumber & 0xFF, &crc);
  }
  if (items & ACI_SERIAL_NUMBER)
  {
    calcCRC16ForChar(serialNumber & 0xFF, &crc);
    calcCRC16ForChar((serialNumber >> 8) & 0xFF, &crc);
    calcCRC16ForChar((serialNumber >> 16) & 0xFF, &crc);
  }
  encodeWirelessBuffer(transmitterPacketBuffer, size, crc);
  transmitterCrc = (transmitterPacketBuffer[size - 1] << 8) | transmitterPacketBuffer[size - 2];
  transmitterPacketSize = size;
}

/*******************************************************************************

DESCRIPTION:  Handles retries for messages being sent to the receiver

NOTES:

*******************************************************************************/
static void HandleRetries(TransmitterMessageType *currentMessageType)
{
  static TransmitterMessageType lastMessageTypeSent = TMT_SERIAL_MESSAGE;

  switch (*currentMessageType)
  {
    case TMT_ZONE_MESSAGE:              // These are the types of messages that
    case TMT_CHECKIN_MESSAGE:           // have retries
    case TMT_SINGLE_KEY_PRESS_MESSAGE:
    case TMT_CARD_READ_MESSAGE:
      keypadRetries = MAX_KEYPAD_RETRIES;
      // Save the type so it can be retried later
      if (*currentMessageType == TMT_CHECKIN_MESSAGE)  // check-in messages are retried
        lastMessageTypeSent = TMT_ZONE_MESSAGE;        // as zone messages
      else
        lastMessageTypeSent = *currentMessageType;
      break;

    case TMT_RETRY_LAST_MESSAGE_TYPE_SENT:
      *currentMessageType = lastMessageTypeSent;
      if (--keypadRetries == 0)
        ClearKeyPress();  // if last retry, clear the key press flag so the next one can be sent
      break;

    case TMT_SERIAL_MESSAGE:
      keypadRetries = 0;  // lost transmitter number so don't worry about retries
      break;
  }
}

/*******************************************************************************
PUBLIC FUNCTION DEFINITIONS
*******************************************************************************/
/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
ReceivedAckStatus processReceivedAck(char* characterOffset)
{
  char bufferOffset = getWirelessReceiveBufferOffset();
  ReceivedAckStatus returnValue = RAS_NOTHING_TO_DO;
  ReceiverAck * const ackPointer = (ReceiverAck*)receiverPacketBuffer;

  if (bufferOffset >= sizeof(ReceiverAck))
  {
    switch (ackPointer->ackType)
    {
    case AT_NO_ACK:
      returnValue = RAS_GOOD_MESSAGE_NON_ACK;
      *characterOffset = sizeof(ReceiverNoAck);
      break;

    case AT_STANDARD_ACK:
      *characterOffset = sizeof(ReceiverStandardAck);
      if (bufferOffset >= *characterOffset)
      {
        if (decodeWirelessBuffer(receiverPacketBuffer, sizeof(ReceiverStandardAck), transmitterCrc))
          returnValue = RAS_GOOD_MESSAGE_ACK_RECEIVED;
        else
          returnValue = RAS_BAD_MESSAGE;
      }
      break;

    case AT_ACK_WITH_ZONE_NUMBER:
      *characterOffset = sizeof(ReceiverZoneNumberAck);
      if (bufferOffset >= *characterOffset)
      {
        ReceiverZoneNumberAck * const zoneAckPointer = (ReceiverZoneNumberAck*)receiverPacketBuffer;

        if (decodeWirelessBuffer(receiverPacketBuffer, sizeof(ReceiverZoneNumberAck), transmitterCrc) &&
            ((houseCode == INVALID_HOUSE_CODE) || (myZoneNumber == INVALID_TRANSMITTER_NUMBER)))
        {
          if (zoneAckPointer->found)
          {
            if (houseCode == INVALID_HOUSE_CODE)
              houseCode = zoneAckPointer->houseCode;

            myZoneNumber = (SlotTransmitterNumber)(zoneAckPointer->zoneLow + (zoneAckPointer->zoneHigh << 8));
            myCheckinSlotNumber = TransmitterToSlotNumber(myZoneNumber);
            returnValue = RAS_GOOD_MESSAGE_ACK_RECEIVED;
          }
          else
          {
            returnValue = RAS_GOOD_MESSAGE_WRONG_RECEIVER;
          }
        }
        else
        {
          returnValue = RAS_BAD_MESSAGE;
        }
      }
      break;

    default:
      returnValue = RAS_BAD_MESSAGE;
      break;
    }
  }
  if (returnValue == RAS_GOOD_MESSAGE_ACK_RECEIVED)
  {
    if (ackStatus.counter <= LED_ACK_COUNT)
      ackStatus.counter++;  // good ack so count it

    keypadRetries = 0;  // got an ack so clear retries
    ClearKeyPress();

    if (autoGenerate.autoCmdGenState == AUTO_GEN_CMD_ENABLED)
    {
      autoGenerate.autoCmdGenState = AUTO_GEN_CMD_ENQUEUED;
      SaveKeypress(CMD_KEY_CODE);
    }
  }

  return returnValue;
}
#ifdef DMP_1100_PRODUCTION_TEST
/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
void setzonenumber(char zone)
{
  myZoneNumber = (SlotZoneNumber)zone;
  return;
}
#endif

/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
TransmitterMessageStatus setupTransmitterMessage(char startOffset)
{
  char bufferOffset = getWirelessReceiveBufferOffset();
  ReceiverSlotStart* slotPointer;
  TransmitterMessageStatus status = TMS_NO_MESSAGE_SETUP;
  bool completeMessageReceived = false;
  ZoneMessageByte msgToSend;
  SlotNumber slotNumber = INVALID_SLOT;
  TransmitterMessageType  messageTypeToSend = TMT_NO_MESSAGE;

  transmitterPacketSize = 0;

  if (bufferOffset >= sizeof(ReceiverNoMessageSlot) + startOffset)
  {
    getZoneMessageByte(&msgToSend);

    slotPointer = ((ReceiverSlotStart*)(receiverPacketBuffer + startOffset));
    slotNumber = (SlotNumber)((slotPointer->slotNumberHigh << 8) | slotPointer->slotNumberLow);

    // if waking up in my checkin slot and the slot has a slottype that doesn't need a response
    // and we aren't waking for an alarm then go back to sleep
    if ((isAlarmSlotType(slotPointer->slotType) && (slotPointer->slotType != ST_GENERAL_ALARM)) &&
        ((slotNumber >= myCheckinSlotNumber) &&
         (slotNumber <= myCheckinSlotNumber + SLOTS_BETWEEN_CHECKIN_SLOTS)))
    {
      if (bufferOffset >= sizeof(ReceiverNoMessageSlot) + startOffset)
      {
        completeMessageReceived = true;
        if (decodeWirelessBuffer(receiverPacketBuffer, sizeof(ReceiverNoMessageSlot) + startOffset, INITIAL_CRC))
          status = TMS_NO_MESSAGE_NEEDED;
      }
    }
    else if (isKeypadDisplaySlotType(slotPointer->slotType))
    {
      status = TMS_RECEIVING_LONG_MESSAGE;
      if (bufferOffset >= (sizeof(KeypadDisplaySlot) + startOffset))  // if we have received all the data
      {
        status = TMS_NO_MESSAGE_NEEDED;
        completeMessageReceived = true;  // mark that we have received a complete valid message
        if (decodeWirelessBuffer(receiverPacketBuffer, sizeof(KeypadDisplaySlot) + startOffset, INITIAL_CRC))
        {   // if we have a message with a correct CRC then process the data
          KeypadDisplaySlot * dataPointer = (KeypadDisplaySlot*)(receiverPacketBuffer + startOffset);
          SlotTransmitterNumber zone = (SlotTransmitterNumber)((dataPointer->zoneNumberLowByte) | (dataPointer->zoneNumberHighBits << 8));
          if (zone == myZoneNumber)
          {
            processKeypadDisplay((char *)dataPointer->display, dataPointer->countdown, dataPointer->countPosition);
            messageTypeToSend = TMT_COMMAND_ACK_SETUP;
            status = TMS_LONG_MESSAGE_RECEIVED;
          }
        }
      }
    }
    else if (isKeypadStatusSlotType(slotPointer->slotType))
    {
      if (bufferOffset >= (sizeof(KeypadStatusSlot) + startOffset))  // if we have received all the data
      {
        status = TMS_NO_MESSAGE_NEEDED;
        completeMessageReceived = true;  // mark that we have received a complete valid message
        if (decodeWirelessBuffer(receiverPacketBuffer, sizeof(KeypadStatusSlot) + startOffset, INITIAL_CRC))
        {   // if we have a message with a correct CRC then process the data
          KeypadStatusSlot * dataPointer = (KeypadStatusSlot*)(receiverPacketBuffer + startOffset);
          SlotTransmitterNumber zone = (SlotTransmitterNumber)((dataPointer->zoneNumberLowByte) | (dataPointer->zoneNumberHighBits << 8));
          if (zone == myZoneNumber)
          {
            // extract "Keypad Status" bytes from dataPointer and send to resource manager
            ResMgrUpdateValues(PANEL_PRIORITY, dataPointer->statusWord >> 8, dataPointer->statusWord & 0xFF);
            messageTypeToSend = TMT_COMMAND_ACK_SETUP;
            status = TMS_MESSAGE_SETUP;
          }
        }
      }
    }
    else if (isKeypadDataSlotNumber(slotNumber)) //this is a keypad data slot but there is no data contained in the slot
    {
      // say we got the message and don't need to reply as long as we are communicating with our receiver
      if (myZoneNumber != INVALID_TRANSMITTER_NUMBER)
        status = TMS_NO_MESSAGE_NEEDED;
      completeMessageReceived = true;
    }
    else if (isAlarmSlotType(slotPointer->slotType))
    {
      char size = sizeof(ReceiverNoMessageSlot) + startOffset;
      if (isTwoByteAlarmSlotType(slotPointer->slotType))
      {
        size += 2;
      }
      if (bufferOffset >= size)
      {
        completeMessageReceived = true;

        // Setup zone number, serial number, key press, or card read message
        if (decodeWirelessBuffer(receiverPacketBuffer, size, INITIAL_CRC))
        {
          status = TMS_MESSAGE_SETUP;                               // assume there is going to be something to send

          if (myZoneNumber == INVALID_TRANSMITTER_NUMBER)           // if not assigned a zone number, send serial number message
            messageTypeToSend = TMT_SERIAL_MESSAGE;
          else if (keypadRetries > 0)                               // retry last message if neccessary
            messageTypeToSend = TMT_RETRY_LAST_MESSAGE_TYPE_SENT;
          else if (TestForZoneMessageToSend())                      // send a zone message (tamper, low battery, low power)
            messageTypeToSend = TMT_ZONE_MESSAGE;
          else if (TestForKeyPressOrCardData() == KEYTEST_KEY)      // send a single key press
            messageTypeToSend = TMT_SINGLE_KEY_PRESS_MESSAGE;
          else if (TestForKeyPressOrCardData() == KEYTEST_CARDREAD) // send a card read
            messageTypeToSend = TMT_CARD_READ_MESSAGE;
          else
            status = TMS_NO_MESSAGE_NEEDED;                         // assumed wrong, there is nothing to send
        }
      }
    }
    else if (slotPointer->slotType == ST_CHECKIN_NEED_REPLY || slotPointer->slotType == ST_CHECKIN_NEED_REPLY_SHUTDOWN) //if this is our checkin slot and the receiver is calling for us to checkin
    {
      if (bufferOffset >= sizeof(ReceiverNoMessageSlot) + startOffset)
      {
        completeMessageReceived = true;
        if (decodeWirelessBuffer(receiverPacketBuffer, sizeof(ReceiverNoMessageSlot) + startOffset, INITIAL_CRC))
        {
          // make sure we are in the right slot
          if ((slotNumber >= myCheckinSlotNumber) &&
              (slotNumber <= myCheckinSlotNumber + SLOTS_BETWEEN_CHECKIN_SLOTS))
          {
            msgToSend |= MB_CHECKIN;
            messageTypeToSend = TMT_CHECKIN_MESSAGE;
            status = TMS_MESSAGE_SETUP;
          }
        }
      }
    }
          //if we are in our checkin slot and the slot type is called for us to update our programming
    else if( slotPointer->slotType == ST_CHECKIN_UPDATE_PROGRAMMING && (slotNumber >= myCheckinSlotNumber) &&
         (slotNumber <= myCheckinSlotNumber + SLOTS_BETWEEN_CHECKIN_SLOTS) )
    {
      messageTypeToSend = TMT_SERIAL_MESSAGE;
      myZoneNumber = INVALID_TRANSMITTER_NUMBER;
    }
    else
    {
      // we don't know what this message is, so say we received it, status alone
      completeMessageReceived = true;
      if (myZoneNumber != INVALID_TRANSMITTER_NUMBER)
        status = TMS_NO_MESSAGE_NEEDED;
    }

    HandleRetries(&messageTypeToSend);
    formatTransmitterMessage(messageTypeToSend, msgToSend);
  }

  if (completeMessageReceived == false)
  {
    if (status != TMS_RECEIVING_LONG_MESSAGE)
      status = TMS_STILL_WAITING;
  }

  currentSlotNumber = slotNumber;

  return status;
}

/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
void resetLearnedInformation(void)
{
  myZoneNumber = INVALID_TRANSMITTER_NUMBER;
  houseCode = INVALID_HOUSE_CODE;
  myCheckinSlotNumber = INVALID_SLOT;
  currentSlotNumber = INVALID_SLOT;
  SwitchFrequencyModes();
  ResetLastResortTimer();
}

/*******************************************************************************

DESCRIPTION: Function will format the transmitterPacketBuffer according to the passed message type

NOTES:

*******************************************************************************/
void formatTransmitterMessage(TransmitterMessageType messageType, ZoneMessageByte msgToSend)
{
  ZeroTransmitterPacketBuffer();

  switch (messageType)
  {
    case (TMT_SERIAL_MESSAGE):
    {
      TransmitterAlarmSerialNumberZoneMessage* const sendPointer =
        (TransmitterAlarmSerialNumberZoneMessage*)transmitterPacketBuffer;

      sendPointer->messageType = TAT_ALARM_WITH_SERIAL_NUMBER;
      sendPointer->forwarded = false;
      sendPointer->transmitterType = TDT_UNIVERSAL_DEVICE;
      sendPointer->reserved = 0;
      sendPointer->serialNumberLow = serialNumber & 0xFF;
      sendPointer->serialNumberMiddle = (serialNumber >> 8) & 0xFF;
      sendPointer->serialNumberHigh = (serialNumber >> 16) & 0xFF;
      sendPointer->softwareVersion = SOFTWARE_VERSION;
      sendPointer->zoneMessage = msgToSend;
      #ifdef DMP_BUILDING
      sendPointer->diag = diag;
      #endif
      finalizeTransmitterMessage(sizeof(TransmitterAlarmSerialNumberZoneMessage), ACI_NOTHING);
      break;
    }

    case (TMT_ZONE_MESSAGE):
    {
      TransmitterAlarmZoneNumberZoneMessage* const sendPointer =
        (TransmitterAlarmZoneNumberZoneMessage*)transmitterPacketBuffer;

      sendPointer->messageType = TAT_ALARM_WITH_ZONE_NUMBER;
      sendPointer->zoneNumberHigh = (myZoneNumber >> 8) & 0xFF;
      sendPointer->zoneNumberLow = myZoneNumber & 0xFF;
      sendPointer->zoneMessage = msgToSend;
      #ifdef DMP_BUILDING
      sendPointer->diag = diag;
      #endif
      finalizeTransmitterMessage(sizeof(TransmitterAlarmZoneNumberZoneMessage),
          ACI_HOUSE_CODE | ACI_SERIAL_NUMBER);
      break;
    }

    case (TMT_CHECKIN_MESSAGE):
    {
      TransmitterCheckinMessage* const sendPointer =
          (TransmitterCheckinMessage*)transmitterPacketBuffer;

      sendPointer->zoneMessage = msgToSend;
      finalizeTransmitterMessage(sizeof(TransmitterCheckinMessage),
          ACI_HOUSE_CODE | ACI_ZONE_NUMBER | ACI_SERIAL_NUMBER);
      break;
    }

    case (TMT_COMMAND_ACK_SETUP):
    {
      CommandDataAckWithZoneMessage* const sendPointer =
          (CommandDataAckWithZoneMessage*)transmitterPacketBuffer;

      sendPointer->zoneNumberHigh = (myZoneNumber >> 8) & 0xFF;
      sendPointer->zoneNumberLow = myZoneNumber & 0xFF;
      sendPointer->reserved = 0;
      sendPointer->forwarded = false;
      sendPointer->zoneMessage = msgToSend;
      finalizeTransmitterMessage(sizeof(CommandDataAckWithZoneMessage),
          ACI_HOUSE_CODE | ACI_SERIAL_NUMBER);
      break;
    }

    case (TMT_SINGLE_KEY_PRESS_MESSAGE):
    {
      TransmitterKeypadSingleKeyPress* const sendPointer = (TransmitterKeypadSingleKeyPress*)transmitterPacketBuffer;

      sendPointer->messageType = TAT_KEYPAD_SINGLE_KEY_PRESS;
      sendPointer->forwarded = false;
      sendPointer->transmitterHigh = (myZoneNumber >> 8) & 0xFF;
      sendPointer->transmitterLow = myZoneNumber & 0xFF;
      getKeyPressOrCardData(&sendPointer->keyByte, KEYTEST_KEY);
      if (sendPointer->keyByte & SHORTCUT_KEY_BIT)
      {
        sendPointer->keyByte &= ~SHORTCUT_KEY_BIT;
        sendPointer->keyByteFields.pressed = PRESSED_2SEC;
      }
      else if (autoGenerate.autoCmdGenState == AUTO_GEN_CMD_ENQUEUED)
      {
        autoGenerate.autoCmdGenState       = AUTO_GEN_CMD_DISABLED;
        sendPointer->keyByteFields.key     = CMD_KEY_CODE; // should already be CMD, but force it to be
        sendPointer->keyByteFields.pressed = PRESSED_NONE; // send auto generated CMD keys as if they were a card read
      }
      else
      {
        sendPointer->keyByteFields.pressed = PRESSED_SHORT;
      }

      sendPointer->zoneMessage = msgToSend;
      finalizeTransmitterMessage(sizeof(TransmitterKeypadSingleKeyPress), ACI_HOUSE_CODE | ACI_SERIAL_NUMBER);
      break;
    }

    case (TMT_CARD_READ_MESSAGE):
    {
      TransmitterKeypadCardRead* const sendPointer = (TransmitterKeypadCardRead*)transmitterPacketBuffer;

      sendPointer->messageType = TAT_KEYPAD_CARD_READ;
      sendPointer->forwarded = false;
      sendPointer->transmitterHigh = (myZoneNumber >> 8) & 0xFF;
      sendPointer->transmitterLow = myZoneNumber & 0xFF;
      if (keypadRetries == MAX_KEYPAD_RETRIES)  // first try to send card read
      {
        getKeyPressOrCardData(sendPointer->cardRead, KEYTEST_CARDREAD);
        memcpy(savedCardRead, sendPointer->cardRead, KEYPAD_CARD_READ_MESSAGE_LENGTH);
      }
      else  // retry sending card read
      {
        memcpy(sendPointer->cardRead, savedCardRead, KEYPAD_CARD_READ_MESSAGE_LENGTH);
      }
      finalizeTransmitterMessage(sizeof(TransmitterKeypadCardRead), ACI_HOUSE_CODE | ACI_SERIAL_NUMBER);
      break;
    }

    default:
    {
      break;
    }
  }
}


void EnableAutomaticCMDPress(void)
{
  autoGenerate.autoCmdGenState= AUTO_GEN_CMD_ENABLED;
}
