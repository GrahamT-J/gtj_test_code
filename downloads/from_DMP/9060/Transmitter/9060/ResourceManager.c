/*******************************************************************************
FILENAME:    ResourceManager.c

DESCRIPTION: Keypad Resource Management

NOTES:       This manages control of shared resources (LEDs, Audibles,
             and LCD display)

Copyright (c) Digital Monitoring Products Inc. 2010.  All rights reserved.

*******************************************************************************/
/*----compilation control-----------------------------------------------------*/

/*******************************************************************************
INCLUDE FILES
*******************************************************************************/
/*----system and platform files (external libraries) -------------------------*/

/*----program files-----------------------------------------------------------*/
#include "types.h"
#include "OTAMsg.h"
#include "ResourceManager.h"
#include "KeypadDisplay.h"

/*******************************************************************************
EXTERNAL REFERENCES   Only used if not available in a header file.
*******************************************************************************/
/*----data declarations-------------------------------------------------------*/

/*----function prototype------------------------------------------------------*/
extern void IndicateNewKeypadStatus1Rcvd(void); // ASM subroutine

/*******************************************************************************
PRIVATE DECLARATIONS
*******************************************************************************/
/*----defines, typedefs, structs, unions, enums-------------------------------*/

/*----data declarations-------------------------------------------------------*/

/*----function prototype------------------------------------------------------*/

/*----macros------------------------------------------------------------------*/

/*******************************************************************************
PRIVATE FUNCTION DEFINITIONS
*******************************************************************************/


/*******************************************************************************
PUBLIC DECLARATIONS
*******************************************************************************/

/*----data declarations-------------------------------------------------------*/

unsigned char KEYPAD_STATUS_1 = KEYPAD_STATUS_1_DFLT; // setup default audible/buzzer state
unsigned char KEYPAD_STATUS_1_Prev = KEYPAD_STATUS_1_DFLT; // setup default audible/buzzer state
unsigned char KEYPAD_STATUS_2 = KEYPAD_STATUS_2_DFLT; // setup default LED state and count type

KeypadOpModePriority currentKeypadOpModePriority;

static unsigned char RM_AudibleArray[TOTAL_NUM_PRIORITIES];
static bool RM_AudibleOneShotArray[TOTAL_NUM_PRIORITIES]; // this is needed for temporal buzzer one-shot
static unsigned char RM_LEDArray[TOTAL_NUM_PRIORITIES];
static bool keypadOperModeRqst[TOTAL_NUM_PRIORITIES];

// display buffers
char RM_DisplayRegisterArray[TOTAL_NUM_PRIORITIES][32]; // associated with "DisplayRegister"
char RM_DisplayInputBuffer[TOTAL_NUM_PRIORITIES][16];   // associated with "Input_Buffer"

/*******************************************************************************
PUBLIC FUNCTION DEFINITIONS
*******************************************************************************/

/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
void ResMgrModeRqst(KeypadOpModePriority priority, bool yield)
{
  KeypadOpModePriority i;

  if (yield)
  {
    keypadOperModeRqst[priority] = false;
    // find next active priority
    for (i = HIGHEST_PRIORITY; i; i--) // i >= LOWEST_PRIORITY causes a pointless comparison warning
    {
      if (keypadOperModeRqst[i])
        break;
    }
    currentKeypadOpModePriority = i;
  }
  else // not a yield
  {
    keypadOperModeRqst[priority] = true;
    if (priority > currentKeypadOpModePriority)
      currentKeypadOpModePriority = priority;
  }
}

/*******************************************************************************

DESCRIPTION: Init each state's display buffer

NOTES:

*******************************************************************************/
void InitResourceManager()
{
  int i, j;

  // Default all states' displays to spaces
  for (i = LOWEST_PRIORITY; i <= HIGHEST_PRIORITY; i++)
  {
    // Default all modes to have green LED on steady
    RM_LEDArray[i] = (STATUS_COLOR_GREEN | STATUS_CADENCE_STEADY);
    for (j = 0; j < 32; j++)
      RM_DisplayRegisterArray[i][j] = ' ';
    for (j = 0; j < 16; j++)
      RM_DisplayInputBuffer[i][j] = ' ';
  }
}

/*******************************************************************************

DESCRIPTION:

NOTES: This fcn is called by ASM code frequently (every 12 mS)

*******************************************************************************/
void UpdateSharedResources()
{
  KEYPAD_STATUS_1_Prev = KEYPAD_STATUS_1;
  KEYPAD_STATUS_1 = RM_AudibleArray[currentKeypadOpModePriority];
  KEYPAD_STATUS_2 = RM_LEDArray[currentKeypadOpModePriority];
  if (RM_AudibleOneShotArray[currentKeypadOpModePriority])
  {
    IndicateNewKeypadStatus1Rcvd(); // tell lower layer code these values have never been processed before
    RM_AudibleOneShotArray[currentKeypadOpModePriority] = false;
  }
}

/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
// called by ASM
void EnterLocalMode()
{
  // since the legacy ASM code has normal and local modes already implemented,
  // the simplest management of the displays is to just "push" and "pop"
  // when leaving / reentering normal mode.

  // "push" display data
  CopyFromKeypadDisplay(RM_DisplayInputBuffer[PANEL_PRIORITY],  RM_DisplayRegisterArray[PANEL_PRIORITY]);
  ResMgrModeRqst(LOCAL_MODE_PRIORITY, NO_YIELD_MODE);
}


/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
// called by ASM
void ExitLocalMode()
{
  // "pop" display data
  CopyToKeypadDisplay(RM_DisplayInputBuffer[PANEL_PRIORITY],  RM_DisplayRegisterArray[PANEL_PRIORITY]);
  ResMgrModeRqst(LOCAL_MODE_PRIORITY, YIELD_MODE);
}

/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
void SetSurveyPriority(void)
{
  RM_AudibleArray[SURVEY_PRIORITY] = (AUDIBLES_OFF | BUZZ_OFF);
  ResMgrModeRqst(SURVEY_PRIORITY, NO_YIELD_MODE);
}

/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
void ClearSurveyPriority(void)
{
  ResMgrModeRqst(SURVEY_PRIORITY, YIELD_MODE);
}

/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
void SetSurveyInSync(bool inSync)
{
  if (inSync)
    RM_LEDArray[SURVEY_PRIORITY] = (STATUS_COLOR_GREEN | STATUS_CADENCE_STEADY);
  else
    RM_LEDArray[SURVEY_PRIORITY] = (STATUS_COLOR_RED | STATUS_CADENCE_STEADY);
}


/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
void ResMgrUpdateValues(KeypadOpModePriority priority, byte audibleVal, byte LEDVal)
{
  bool val;
  
  // Only set the one-shot if previous value of buzzer was ON. This way, all timed sounds
  // will complete their full duration, even if an OFF message is received.
  if ((RM_AudibleArray[priority] & BUZZ_MASK) == BUZZ_ON)
    val = true;
  else
    val = false;
  
  RM_AudibleArray[priority] = audibleVal;
  RM_AudibleOneShotArray[priority] = val; // needed for timed sounds to work
  RM_LEDArray[priority] = LEDVal;
  UpdateSharedResources();
}
