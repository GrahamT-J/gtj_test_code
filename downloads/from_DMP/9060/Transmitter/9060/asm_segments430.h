/**************************************************
 *
 * Backwards compatibility segment definitions for
 * the IAR Systems MSP430 assembler..
 *
 * This file can be used to make the version 2.x
 * assembler accept the segment names available
 * A430 version 1.x.
 *
 *
 * Copyright 1996 - 2003 IAR Systems. All rights reserved.
 *
 * $Revision: 1.1 $
 *
 **************************************************/

#ifndef __ASM_SEGMENTS430_H
#define __ASM_SEGMENTS430_H

//#define CDATA0		DATA16_ID
//#define CONST		DATA16_C
//#define CSTR		DATA16_C
//#define IDATA0		DATA16_I
//#define NO_INIT		DATA16_N
#define UDATA0		DATA16_Z
#define GLOBAL          DATA16_C


#endif /* __ASM_SEGMENTS430_H */
