/**

    kpd_equates.h

    Copyright(c) Digital Monitoring Products 2010

    Author: DIGITAL MONITORING PRODUCTS
*/
      ; Filename: msp430_790equ.h

RamStart                EQU     0x0200  ;512 bytes of RAM in MSP430F135
StackTop                EQU     0x0400  ;Stack grows towards lower memory address.
RamEnd                  EQU     0x03fe
;
;//The following is for the msp430f149
;StackTop               EQU     0x0500  ;Stack grows towards lower memory address.
;RamEnd                 EQU     0x09fe

InfoStart               EQU     0x1000  ;Information memory is FLASH used as EEPROM

One_Second              EQU     84      ;84 12ms ticks = 1 second
Two_Seconds             EQU     168     ;168 12ms ticks = 2 seconds
Three_Seconds           EQU     252     ;252 12ms ticks = 3 seconds
FireAlarmCmd            EQU     1       ;extended protocol fire alarm command code
DoorChimeCmd            EQU     2       ;extended protocol door chime command code
BurgAlarmCmd            EQU     3       ;extended protocol burglary alarm command code
EntryAlarmCmd           EQU     4       ;extended protocol prewarn alarm (entry) command code

AlarmDisplayTime        EQU     40      ;
AlphaInput              EQU     1       ;
NumericInput            EQU     0       ;

BkArrow                 EQU     7       ;back arrow key mapped scan code
BkArrowCmd              EQU     011h    ;back arrow and command keys pressed (row4,column3 and column4)
BrightMin               EQU     0       ;
BrightMax               EQU     8       ;audio levels range from 0 to 8
HalfLight               EQU     4       ;brightness setting when audio is on
BrightTimeOut           EQU     10      ;10 second elapsed time for no key presses nor any alert commands
                                        ;for backlight brightness to return to user setting
MinimumPowerLED         EQU     1

CLS                     EQU     001h    ;LCD instruction,CLEAR display and return HOME
ClickInterval           EQU     3       ;3*12ms=36ms key click time interval

DispRow1                EQU     0       ;offset for display row 1 in shadow display register
DispRow2                EQU     16      ;offset for display row 2 in shadow display register

KeyMask                 EQU     02Fh    ;mask for key code bits in TxD_Que response buffer

CURSOR                  EQU     02Dh    ;ASCII negative symbol used as an LCD 'cursor' display

*    Digital Osc Control 71 -- ~2.78MHz     65 -- ~2.65  can modulated up to 2.7648MHz
RSEL_DATA               EQU     0x0F    ;Rsel = 7 (15 is max)
DCO_DATA                EQU     0x00    ;DCO = 0xE0 is Max freq (top 3 bits all set)
FLASH_DIV               EQU     0x000C  ;FLASH Divider - MUST be adjusted if DCO or RSEL are changed

DefKypdMsgLen           EQU     16      ;default keypad message length (excluding end-of-message marker)

DiagDispTimeOut         EQU     1       ;count for 1 seconds (1 second time intervals) for LCD diagnostic display
Digit4Idx               EQU     3       ;index value for 4th digit in key que
Digit5Idx               EQU     4       ;index value for 5th digit in key que
DISPLAYON               EQU     00Ch    ;LCD display ON (cursor OFF,blinking OFF)
DISPLAYOFF              EQU     8       ;LCD display OFF (cursor OFF,blinking OFF)

EmergencyKeys           EQU     01Eh    ;emergency keys extended key code sent to panel (000KKKKK format)
EmergencyKeysEnablePos  EQU     4       ;LCD display position index for '*' denoting emergency keys enabled
EOC                     EQU     01Fh    ;credit card end of characters hexF but with LSB first and a parity bit (11111)
EOM                     EQU     '~'     ;end-of-message marker for strings sent to LCD_MSG_OUT for display
                                    ;the LCD cannot display '~' so it was chosen as EOM.

FireKeys                EQU     01Fh    ;fire keys extended key code sent to panel (000KKKKK format)
FireKeysEnablePos       EQU     8       ;LCD display position index for '*' denoting fire keys enabled

BurgAlarmInc            EQU     13

FireAlarmInc            EQU     -1

FS                      EQU     016h    ;credit card seperator character hexD but with LSB first and a parity bit (10110)

HOME                    EQU     002h    ;LCD instruction,return cursor to HOME position(addr 80h)

INSTTimeOut             EQU     255     ;local installation/programming mode time out = 255 seconds (RADAR WLS-344)

LCDPosEnd               EQU     16      ;end of LCD display window size (16 characters)
LCDAlarmPosIdx          EQU     10      ;expected display position index for '-ALARM'
LCDFunction             EQU     038h    ;8-bit data, 2-lines, 5x7 dot font
LCDEntryMode            EQU     006h    ;inc DDRAM counter and cursor shift on data in
LCDDsplyCntrl           EQU     00Ch    ;display ON, cursor OFF, blinking OFF
LocalWinRow1            EQU     090h    ;row1 local msg window (LCD) address
LocalWinRow2            EQU     0D0h    ;row2 local msg window (LCD) address
ZONENumMax              EQU     3       ;max number of loops(4),also used as a mask
ZoneLowThreshold        EQU     1966    ;1.2v measurement ((1.2v / 2.5v) * 4095)
ZoneHighThreshold       EQU     3276    ;2.0v measurement ((2.0v / 2.5v) * 4095)
;ZONETimeMax             EQU     4       ;timeout for loops state transition(debounce)
                                        ;10*4loops*12ms=480ms software timeout
                                        ;(25-50ms hardware delay)

MCUIKeyPressTimeOut     EQU     2       ;mode control and user interface key press time out to enter
                                        ;local mode (2 seconds)

;New crystal is 4 mhz, time interval= 2 ms
mSec2                   EQU     8000
mSec1                   EQU     4000
mSecHalf                EQU     mSec1/2

NonPollCntMax           EQU     3       ;MSB value for 768 polls without my address ever encountered
                                        ;(MSB value of 16-bit BusPoll_Cnt thus 256 count resolution)
NORMAL                  EQU     0       ;normal loop state value (EOL resistor=1K)
NOTUSED                 EQU     3       ;loop is not used

OPEN                    EQU     2       ;open loop state value (EOL resistor missing)

PanicKeys               EQU     01Dh    ;panic keys extended key code sent to panel (000KKKKK format)
PanicKeysEnablePos      EQU     0       ;LCD display position index for '*' denoting panic keys enabled

DisplayRow1             EQU     080h    ;row1 remote msg window (LCD) address
DisplayRow2             EQU     0C0h    ;row2 remote msg window (LCD) address

RedTimeMax              EQU     40      ;40 second time out for red keypad backlight to be ON after
                                        ;detecting '-ALARM' in the display in standard protocol.
Row1                    EQU     080h    ;row 1 (portA) output select value
Row2                    EQU     040h    ;row 2 (portA) output select value
Row3                    EQU     020h    ;row 3 (portA) output select value
Row4                    EQU     010h    ;row 4 (portA) output select value
Row5                    EQU     008h    ;row 5 (portA) output select value
AllRows                 EQU       0F8h    ;turns on all rows

RxDByteCnttoRespond     EQU     3       ;received byte count value at which the keypad transmits a response
RxDByteCntMax           EQU     7       ;maximum number of bytes allowed to be received from panel

Tamper                  EQU     02Bh    ;tamper switch extended protocol key code for short-to-open condition
TamperRestored          EQU     02Ch    ;tamper switch extended protocol key code for open-to-short condition
TamperTimeOut           EQU     42      ;504ms software delay time before asserting tamper (42*12ms=504ms)
TOP1                    EQU     0       ;top row select key 1
TOP2                    EQU     1       ;top row select key 2
TOP3                    EQU     2       ;top row select key 3
TOP4                    EQU     3       ;top row select key 4
KEY1                    EQU     4
KEY2                    EQU     5
KEY3                    EQU     6
BACK                    EQU     7
KEY4                    EQU     8
KEY5                    EQU     9
KEY6                    EQU     0AH
CMD                     EQU     00Bh    ;CMD (command) key mapped scan code
KEY7                    EQU     0CH
KEY8                    EQU     0DH
KEY9                    EQU     0EH
KEY0                    EQU     0FH
SetLower4               EQU     00Fh    ;mask for setting lower 4 bits

SHIFTL                  EQU     018h    ;LCD display & cursor shift left instruction
SHORT                   EQU     1       ;short loop state value (EOL resistor shorted)
SPACE                   EQU     020h    ;ASCII space ' ' (or blank)

PollResponseLen         EQU     2       ;number of bytes to send to panel in STANDARD transmit protocol

KeyClickTime            EQU     3       ;key click time intervals (12ms) for about 24-36ms duration
                                        ;depending on whether or not the time interval starts just before
                                        ;or just after a 12ms poll.
SystemBusyTimeOut       EQU     5       ;5 second
SystemTroubleTimeOut    EQU     60      ;60 second

ONE_SECOND_COUNT        EQU       21

Top1Top2                EQU     084h    ;top 1 and top 2 keys pressed (row1,column1 and column2)
Top2Top3                EQU     082h    ;top 2 and top 3 keys pressed (row1,column2 and column3)
Top3Top4                EQU     081h    ;top 3 and top 4 keys pressed (row1,column3 and column4)

UserTimeOut             EQU     20      ;local user mode time out = 20 seconds

FrequencyMin            EQU     0       ;
FrequencyMax            EQU     7       ;tone graph value which makes the maximum tone available(2KHz)
ToneInit                EQU     4       ;factory default tone number
FreqInit                EQU     02E4h   ;1864.66 Hz A#6  -- factory default tone frequency

VolumeMin               EQU     1       ;duty cycle which makes the speaker volume minimum (quiet)
VolumeMax               EQU     8       ;duty cycle which makes the speaker volume maximum (loud)

WiegandCntMax           EQU     115     ;max # of Wiegand bits accepted

Zone2Normal             EQU     0F3h    ;mask for ZoneStates to make Zone 2  bits normal value
                                        ;for door access 'soft shunt' (hold off).
Zone2Mask               EQU     0Ch     ;

;(keypress sequence bit) EQU     80h    ; C code uses bit 7 for sequence / retransmissions in the over the air protocol with the panel
ShortcutKeyBit          EQU     40h     ; indicate key pressed for 2 seconds.
CardReadBit             EQU     20h     ; this key was from a RFID card read
;ExtendedBit             EQU     40h     ;
;KeyPressedBit           EQU     10h     ;

Zone3WaitTime           EQU     2       ;

YES_DELAY_DEFAULT       EQU     2       ;Default of delay (after wiegand input) before yes key is sent to panel.

Sixteenth_Note          EQU     8        ;based on 12 ms time
Eighth_Note             EQU     18
Quarter_Note            EQU     34
Half_Note               EQU     68
Full_Note               EQU     136
SONG_HALF_SEC           EQU     47
SONG_ONE_SEC            EQU     94

SongEnd                 EQU     010h
SongRepeat              EQU     011H

SongStepVal             EQU     2
;Frequencies will be based on 4 mhz clock
CLOCK_FREQ              EQU     4000000

WIEGAND_ZERO            EQU     00000001B
WIEGAND_ONE             EQU     00000100B

WIEG_BIT_DELAY_TIME     EQU     10      ;120ms between wiegand bits

DMP_BITCNT_HIGH         EQU     40d
DMP_BITCNT_LOW          EQU     26d

;DegradedModeAction
DM_RELAY_OFF            EQU     0
DM_SITE_CODE            EQU     1
DM_ANY_READ             EQU     2
DM_RELAY_ON             EQU     3
DM_USE_LAST             EQU     4

Zone2RelockTime         EQU     10    ; time in s for relock to overide panel strike

Zone2RelockTimeDelay    EQU     2     ; time in s after zn2 fault till relock

DEGRADED_MODE_TIMEOUT   EQU     104   ;based on 48ms timer so ~ 5sec

BUZZ_ONE_SHOT           EQU     00000001B  ; bit-field of BuzzerOneShot variable
