;----------------------------------------------------------------
;
;   kpd_backlight.s43 -
;
;   Copyright(c) Digital Monitoring Products 2004
;
;   Author: DIGITAL MONITORING PRODUCTS
;----------------------------------------------------------------
*******************************************************************************
*     Backlight Manager                                                       *
*******************************************************************************


;-------------------------------------------------------------------------------
; *******************************************************************************
; *     Include files                                                           *
; *******************************************************************************


; *******************************************************************************
; *      Equates                                                                *
; *******************************************************************************


; *******************************************************************************
; *     Public declarations                                                     *
; *******************************************************************************
EXTERN KEYPAD_STATUS_1
EXTERN KEYPAD_STATUS_1_Prev
EXTERN KEYPAD_STATUS_2

; *******************************************************************************
; *     Public RAM variables                                                    *
; *******************************************************************************
        RSEG    UDATA0


; *******************************************************************************
; *     local RAM variables                                                     *
; *******************************************************************************
        RSEG        UDATA0


; *******************************************************************************
; *     Public ROM Constants                                                    *
; *******************************************************************************
        RSEG    CONST

        EVEN



; *******************************************************************************
; *     Local ROM Constants                                                     *
; *******************************************************************************
        RSEG    CONST

        EVEN



; *******************************************************************************
; *     Start of program code                                                   *
; *******************************************************************************
        RSEG    CODE

BACKLIGHT_MGR:
      JBIT1	LocalMode,Flags1,BACKLIGHT_LOCAL  ;special processing if local mode

BACKLIGHT_CHK_RED:
        SBIT0	    RedBackLiteOn,Flags2
        JBIT0	    RxDExtendedCom,Flags2,BL_CHK_RED_TMR
        JBIT1	    RedBackLiteCmd,RemoteCmd7,BACKLIGHT_RED_ON
        JMP         BACKLIGHT_RED_1
BL_CHK_RED_TMR:
        CMP.B	    #0,RedBackTimer
        JEQ         BACKLIGHT_RED_1 ; do not turn on
BACKLIGHT_RED_ON:
        SBIT1	    RedBackLiteOn,Flags2
BACKLIGHT_RED_1:
        ; Test for override from a panel message which forces the red backlight on
        MOV.B       &KEYPAD_STATUS_2,R4 //Note: This is ignored if local mode
        AND.B       #RED_BACKLIGHT_MASK,R4
        CMP.B       #RED_BACKLIGHT_ON,R4
        JNZ         BACKLIGHT_RED_END
        SBIT1	    RedBackLiteOn,Flags2 ; turn on red backlight
BACKLIGHT_RED_END:
;
;
BACKLIGHT_CHK_HALF:
        JBIT0	    BuzzerCmd,RemoteCmd2,BACKLIGHT_CHK_MAX
        MOV.B	    #HalfLight,BrightnessLevel    ;Set briteness level
        JMP	        BACKLIGHT_SET_LITE
;
;
BACKLIGHT_CHK_MAX:
      JBIT1	RedBackLiteOn,Flags2,BACKLIGHT_SET_MAX
      CMP.B	#0,KeyLiteTimer
      JEQ	      BACKLIGHT_SET_USER
BACKLIGHT_SET_MAX:
        MOV.B     #BrightMax,BrightnessLevel    ;Set briteness level
      JMP	      BACKLIGHT_SET_LITE
;
BACKLIGHT_LOCAL:
        JBIT1     INSTMode,Flags3,BACKLIGHT_SET_MAX
        JBIT1	    BuzzerCmd,RemoteCmd2,BACKLIGHT_SET_USER_L1
;
BACKLIGHT_SET_USER:
        JBIT0	    DisplayBlank,Flags4,BACKLITE_LEVEL_FROM_EE
        MOV.B	    #BrightMin,BrightnessLevel    ;Set briteness level
        JMP	      BACKLIGHT_SET_USER_L1
;
BACKLITE_LEVEL_FROM_EE:
        MOV.B     &BrightnessLevelEE,R4         ;get user backlite level out of EEPROM
        CMP.B     #BrightMax+1,R4               ;compare to max
        JL        BACKLITE_LEVEL_FROM_EE_X      ;if out of range
        MOV.B     #BrightMax,R4                 ;set level to max and save
BACKLITE_LEVEL_FROM_EE_X:
        MOV.B     R4,BrightnessLevel            ;Set new briteness level
BACKLIGHT_SET_USER_L1:
        SBIT0     RedBackLiteOn,Flags2
;
BACKLIGHT_SET_LITE:
        MOV.B     BrightnessLevel,R4            ;Get new briteness level
        MOV.B     R4,R5                         ;move to index bar graph
        MOV.B     BrightnessLevelGraph(R5),R4   ;get duty cycle bar graph to play
        CMP.B     R4,CurrentLightsDutyCycle     ;if same as current then exit
        JEQ       BACKLIGHT_SET_RED_LITE        ;else
        MOV.B     R4,CurrentLightsDutyCycle     ;update to new bar graph
        MOV.B     R4,LightsDutyCycle            ;
        MOV.B     R4,PowerLEDDutyCycle          ;
        CMP.B     #0,R4
        JNZ       BACKLIGHT_SET_RED_LITE        ;
        MOV.B     #MinimumPowerLED,PowerLEDDutyCycle
BACKLIGHT_SET_RED_LITE:
        JBIT0     RedBackLiteOn,Flags2,BACKLIGHT_CLR_RED_LITE
        GrnBackLite_OFF                         ;turn OFF green keypad backlight
        ;GreenBLLogoOFF                         ;Note: 'Power LED' is controlled by panel (see KEYPAD_STATUS_2 variable)
        MOV.B     PowerWink_Timer,R4            ;
        CMP.B     #0,R4
        JNZ       BACKLIGHT_MGR_EXIT            ;don't set Power LED if Winking for Wiegand
        MOV.B     #0FFh,PowerLEDDutyCycle       ;set power LED to max brightness
BACKLIGHT_MGR_EXIT:
        RET

BACKLIGHT_CLR_RED_LITE:
        RedBackLite_OFF                         ;
        RET                                     ;
