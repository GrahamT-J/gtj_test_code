#ifndef VERSION_H
#define VERSION_H

// 1 - Initial release version
// 2 - Listens for keypad data on all keypad slots 6/13/2016
#define SOFTWARE_VERSION 2       // RELEASE_TODO:  If needed, increment this
#define DIAG_SOFTWARE_VERSION 6  // RELEASE_TODO:  Increment each time code is given to the factory

#endif
