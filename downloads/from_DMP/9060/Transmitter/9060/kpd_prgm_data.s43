;----------------------------------------------------------------
;
;   kpd_prgm_data.s43 -
;
;   Copyright(c) Digital Monitoring Products 2004
;
;   Author: DIGITAL MONITORING PRODUCTS
;----------------------------------------------------------------

    ; '================= FLASH Information Memory Definitions ===================='

;-------------------------------------------------------------------------------
; *******************************************************************************
; *     Include files                                                           *
; *******************************************************************************


; *******************************************************************************
; *     Public declarations                                                     *
; *******************************************************************************
PUBLIC  ModelOptionsEE
PUBLIC  DfltModelMsgEE

; *******************************************************************************
; *     Start of program code                                                   *
; *******************************************************************************

/********************************************************************************
*********************************************************************************
This file contains the keypad flash programming definition, defaults, and ram
  programming definition.
*********************************************************************************
********************************************************************************/
    RSEG INFO
FLASHStart:
    RSEG UDATA0
FLASHSave:
    RSEG  CONST
FLASHDfltSave:
            ; Ram Name            Flash Name            Default Name                Default Value
  DEFINE_WORD UserEntryFlags,     UserEntryFlagsEE,     Dflt_UserEntryFlagsEE,      0
  DEFINE_WORD AudibleTable,       AudibleTableEE,       Dflt_AudibleTableEE,        SteadyTone
  DEFINE_WORD AudibleTable1,      AudibleTable1EE,      Dflt_AudibleTable1EE,       FireAlert
  DEFINE_WORD AudibleTable2,      AudibleTable2EE,      Dflt_AudibleTable2EE,       MonitorSong
  DEFINE_WORD AudibleTable3,      AudibleTable3EE,      Dflt_AudibleTable3EE,       BurgAlert
  DEFINE_WORD AudibleTable4,      AudibleTable4EE,      Dflt_AudibleTable4EE,       EntrySong
  DEFINE_WORD AudibleTable5,      AudibleTable5EE,      Dflt_AudibleTable5EE,       SteadyTone
  DEFINE_WORD AudibleTable6,      AudibleTable6EE,      Dflt_AudibleTable6EE,       SteadyTone
  DEFINE_WORD AudibleTable7,      AudibleTable7EE,      Dflt_AudibleTable7EE,       SteadyTone
  DEFINE_WORD AudioFreq,          AudioFreqEE,          Dflt_AudioFreqEE,           FreqInit
  DEFINE_WORD AudioTone,          AudioToneEE,          Dflt_AudioToneEE,           ToneInit
  DEFINE_BYTE SendYesDelay,       SendYesDelayEE,       Dflt_SendYesDelayEE,        YES_DELAY_DEFAULT
  DEFINE_BYTE BrightnessLevel,    BrightnessLevelEE,    Dflt_BrightnessLevelEE,     BrightMax
  DEFINE_BYTE KpdAddrBin,         KpdAddrBinEE,         Dflt_KpdAddrBinEE,          1
  DEFINE_BYTE VolumeLevel,        VolumeLevelEE,        Dflt_VolumeLevelEE,         VolumeMax
  DEFINE_MSG  DfltKypdMsg,        DfltKypdMsgEE,        Dflt_DfltKypdMsgEE,         '                ', 16
  DEFINE_BYTE DfltKypdMsgEnd,     DfltKypdMsgEndEE,     Dflt_DfltKypdMsgEndEE,      '~'
  DEFINE_BYTE MiscFlags,          MiscFlagsEE,          Dflt_MiscFlagsEE,           0xFF
  DEFINE_BYTE UserDigitLength,    UserDigitLengthEE,    Dflt_UserDigitLengthEE,     5
  DEFINE_WORD SoftShuntTime,      SoftShuntTimeEE,      Dflt_SoftShuntTimeEE,       40
  DEFINE_BYTE Zone3REXTime,       Zone3REXTimeEE,       Dflt_Zone3REXTimeEE,        5
  DEFINE_BYTE WiegandCodeLen,     WiegandCodeLenEE,     Dflt_WiegandCodeLenEE,      45
  DEFINE_BYTE SiteCodePos,        SiteCodePosEE,        Dflt_SiteCodePosEE,         1
  DEFINE_BYTE SiteCodeLen,        SiteCodeLenEE,        Dflt_SiteCodeLengEE,        1
  DEFINE_BYTE UserCodePos,        UserCodePosEE,        Dflt_UserCodePosEE,         1
  DEFINE_BYTE UserCodeLen,        UserCodeLenEE,        Dflt_UserCodeLenEE,         45
  DEFINE_BYTE DegradedModeAction, DegradedModeActionEE, Dflt_DegradedModeActionEE,  0
  DEFINE_WORD CardReadOptions,    CardReadOptionsEE,    Dflt_CardReadOptionsEE,     0
  DEFINE_WORD SiteCode1,          SiteCode1EE,          Dflt_SiteCode1EE,           0xFFFF
  DEFINE_WORD SiteCode2,          SiteCode2EE,          Dflt_SiteCode2EE,           0xFFFF
  DEFINE_WORD SiteCode3,          SiteCode3EE,          Dflt_SiteCode3EE,           0xFFFF
  DEFINE_WORD SiteCode4,          SiteCode4EE,          Dflt_SiteCode4EE,           0xFFFF
  DEFINE_WORD SiteCode5,          SiteCode5EE,          Dflt_SiteCode5EE,           0xFFFF
  DEFINE_WORD SiteCode6,          SiteCode6EE,          Dflt_SiteCode6EE,           0xFFFF
  DEFINE_WORD SiteCode7,          SiteCode7EE,          Dflt_SiteCode7EE,           0xFFFF
  DEFINE_WORD SiteCode8,          SiteCode8EE,          Dflt_SiteCode8EE,           0xFFFF
    RSEG CONST
FLASHDfltSaveEnd:
  DEFINE_BYTE ModelOptions,       ModelOptionsEE,       Dflt_ModelOptionsEE,      0 ;RELEASE_TODO -- make this 0 when releasing code
                                                                                  ; ENABLE_EXT_READ|NO_YELLOW_LED|INVERT_PWR_LED|RED_TO_GREEN      ;THIN KEYPAD DEBUG Rev 1 bd
                                                                                  ; NO_YELLOW_LED|RED_TO_GREEN                                     ;THIN KEYPAD DEBUG Rev 0 bd, NO HID
                                                                                  ; ENABLE_EXT_READ|NO_YELLOW_LED|RED_TO_GREEN                     ;tHIN KEYPAD DEBUG Rev 0 bd
                                                                                  ; ENABLE_EXT_READ                                                ;693/793 DEBUG Rev 4 bd
                                                                                  ; 0                                                              ;Production setting all versions
  DEFINE_MSG  DfltModelMsg,       DfltModelMsgEE,       Dflt_DfltModelMsgEE,        '     ~',  6
  DEFINE_BYTE DfltModelMsgEnd,    DfltModelMsgEndEE,    Dflt_DfltModelMsgEndEE,     '~'
    RSEG INFO
FLASHEnd:
    RSEG UDATA0
FLASHSaveEnd:


PROG_DATA_LENGTH            EQU     FLASHEnd - FLASHStart
DFLT_PROG_DATA_LENGTH       EQU     FLASHDfltSaveEnd - FLASHDfltSave
FLASHLen                    EQU     PROG_DATA_LENGTH

NO_YELLOW_LED               EQU     00000001B  ;Whether yellow-led should follow red-led.
INVERT_PWR_LED              EQU     00000010B  ;Invert the power-led because inverter is inline.
RED_TO_GREEN                EQU     00000100B  ;Ties red backlite brightness to green backlite
ENABLE_EXT_READ             EQU     00001000B  ;Whether external HID reader is enabled.
SMITH_AND_WESSON            EQU     00010000B  ;Smith & Wesson keypad - incompatible with DMP panels
ADT_KEYPAD                  EQU     00100000B  ;ADT keypad - incompatible with DMP panels
;                           EQU     01000000B
;                           EQU     10000000B

