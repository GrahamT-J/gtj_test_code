/*******************************************************************************
FILENAME:  TransmitterProtocol.h

DESCRIPTION:

NOTES:

Copyright (c) Digital Monitoring Products Inc. 2004.  All rights reserved.

*******************************************************************************/
#ifndef  TRANSMITTERPROTOCOL_H
#define  TRANSMITTERPROTOCOL_H

/*----compilation control-----------------------------------------------------*/

/*******************************************************************************
INCLUDE FILES
*******************************************************************************/
/*----system and platform files-----------------------------------------------*/

/*----program files-----------------------------------------------------------*/
#include "ProtocolTiming.h"
#include "WirelessKeypadEE.h"

/*******************************************************************************
FILE CONTEXT
*******************************************************************************/
/*----defines, typedefs, structs, unions, enums-------------------------------*/
typedef enum
{
  RAS_GOOD_MESSAGE_ACK_RECEIVED,
  RAS_GOOD_MESSAGE_NON_ACK,
  RAS_GOOD_MESSAGE_WRONG_RECEIVER,
  RAS_BAD_MESSAGE,
  RAS_NOTHING_TO_DO,
  RAS_CORRECT_RECEIVER_NO_PROGRAMMING
} ReceivedAckStatus;

typedef enum
{
  TMS_NO_MESSAGE_SETUP,
  TMS_MESSAGE_SETUP,
  TMS_NO_MESSAGE_NEEDED,
  TMS_STILL_WAITING,
  TMS_RECEIVING_LONG_MESSAGE,
  TMS_LONG_MESSAGE_RECEIVED,
} TransmitterMessageStatus;

typedef struct
{
  BITFIELD  counter           :4;
  BITFIELD  panicAckWaiting   :1;
  BITFIELD                    :1;
  BITFIELD                    :1;
  BITFIELD                    :1;
}AckStatus;

#define serialNumberPtr (const long*)(SERIAL_NUMBER_ADDRESS)
#define serialNumber *serialNumberPtr

/*----data descriptions-------------------------------------------------------*/

/*----data declarations-------------------------------------------------------*/
extern AckStatus ackStatus;

/*----macros------------------------------------------------------------------*/

/*----function prototypes-----------------------------------------------------*/
extern ReceivedAckStatus processReceivedAck(char* characterOffset);
extern short getSleepTicks();
extern short getSleepTicksForCommand();
extern TransmitterMessageStatus setupTransmitterMessage(char startOffset);
extern void resetLearnedInformation(void);
extern bool IsFastOutput(void);
void EnableAutomaticCMDPress(void);
#ifdef DMP_1100_PRODUCTION_TEST
void setzonenumber(char);
#endif

#endif                                  /* end of file */
