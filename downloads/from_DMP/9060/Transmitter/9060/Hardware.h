/*******************************************************************************
FILENAME: Hardware.c

DESCRIPTION: Miscellaneous hardware descriptions not included elsewhere

NOTES:

Copyright (c) Digital Monitoring Products Inc. 2009.  All rights reserved.

*******************************************************************************/
#ifndef  HARDWARE_H
#define  HARDWARE_H

/*----compilation control-----------------------------------------------------*/

/*******************************************************************************
INCLUDE FILES
*******************************************************************************/
/*----system and platform files-----------------------------------------------*/

/*----program files-----------------------------------------------------------*/

/*******************************************************************************
FILE CONTEXT
*******************************************************************************/
/*----defines, typedefs, structs, unions, enums-------------------------------*/
#define MASTER_CLOCK 4000000

#define SMCLK MASTER_CLOCK

#define XEBitt (MASTER_CLOCK / 19200)

#define DIV_1 0
#define DIV_2 1
#define DIV_4 2
#define DIV_8 3

#define SLEEP_MASTER_CLOCK            32768
#define SLEEP_CLOCK_DIVIDER_SETTING   DIV_8
#define SLEEP_CLOCK_DIVIDER           (1 << SLEEP_CLOCK_DIVIDER_SETTING)
#define SLEEP_TIMERA_DIVIDER_SETTING  DIV_4
#define SLEEP_TIMERA_DIVIDER          (1 << SLEEP_TIMERA_DIVIDER_SETTING)

typedef enum
{
  SOFT_RESET,
  HARD_RESET,
  WIRELESS_HW_RESET
} ResetType;

/*----data descriptions-------------------------------------------------------*/

/*----data declarations-------------------------------------------------------*/
#ifdef USE_PANIC_PULSING
extern char LEDFlashCounter;
#endif
/*----macros------------------------------------------------------------------*/


/*----function prototypes-----------------------------------------------------*/

extern void initSys(ResetType resetType);
#ifdef USE_PANIC_PULSING
extern void startPanicPulsing(void);
#endif
extern void resetHardware(ResetType resetType);
#endif                                  /* end of file */
