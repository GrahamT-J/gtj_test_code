;----------------------------------------------------------------
;
;   Kpd_utils.s43 -
;
;   Copyright(c) Digital Monitoring Products 2010
;
;   Author: DIGITAL MONITORING PRODUCTS
;----------------------------------------------------------------

********************************************************************************
*                               Copyright 2002 - 2010                          *
*                         Digital Monitoring Products                          *
********************************************************************************


PUBLIC INIT_PORTS
PUBLIC CHECK_FOR_DIAG
EXTERN PinTest

; *******************************************************************************
; *     local RAM variables                                                     *
; *******************************************************************************
        RSEG        UDATA0
        RAM_EVEN

*******************************************************************************
*     CODE                                                                    *
*******************************************************************************

        RSEG        CODE

INITIALIZE_RAM_CONSTANTS:
        CLR.B       CurrentLightsDutyCycle          ;
        SBIT0       BuzzerCmd,RemoteCmd2            ;
        MemCopy     FLASHStart, FLASHSave, FLASHLen ; copy programming from flash to ram
        CLR.B       R4
        CLR.B       R5
Init_A  MOV.B       R4,ZONE_Timer(R5)               ;clear loop state transition timers
        INC.B       R5
        CMP.B       #ZONENumMax,R5
        JHS         Init_A

        MOV.B       #Row1,RowSel                    ;initialize row select register

        SBIT0       PanelKeyPressBit,TxD_Que
        MOV.B       #0FH,CurrentAudio
        CLR.B       TamperType
        CLR.B       TamperTypeOld
        CALL        #SET_BEEP_FROM_EE
        RET

; Called by C. Push any non-scratch registers that may be used.
CHECK_FOR_DIAG:
        PUSH_ALL
        CLR.B       TxDQueIdxR
        SBIT0       SystemBusy,Flags2               ;
        SBIT0       LocalDiag,Flags3

        ; Check if Functional Test mode requested (special test pin will be low if so).
        TEST_FUNCTEST_MODE_PIN
        JNZ         Test_for_ICT

        BIT.B       #AUTO_TEST_MODE_BIT2,&AUTO_TEST_MODE_BIT2_PORT
        JNZ         FuncTestModes3_4
        BIT.B       #AUTO_TEST_MODE_BIT1,&AUTO_TEST_MODE_BIT1_PORT
        JNZ         FuncTestMode2

        ;Functional Test Mode is selected.
        ;If mode #1, set up i/o ports and UART #1
FuncTestMode1:
        CALL        #INIT_PORTS
        CALL        #CLEAR_IE_SEL
        CALL        #INIT_UART_1
        EINT
        SBIT1       ProductionTestMode, Flags3
        CALL        #PinTest             ; does not return.

FuncTestMode2:
        ;else if mode #2, enable diag menu
        SBIT1       LocalDiag,Flags3
        BR          #RunRegular

Test_for_ICT:
        ; Check if ICT Automated diagnostics requested (1 or more special test pins will be low if so).
        BIT.B       #AUTO_TEST_MODE_BIT1,&AUTO_TEST_MODE_BIT1_PORT
        JZ          EnterTestModeICT                ; in-circuit test mode
        BIT.B       #AUTO_TEST_MODE_BIT2,&AUTO_TEST_MODE_BIT2_PORT
        JNZ         RunRegular
EnterTestModeICT:
        BR          #RUN_AUTO_TEST_MODE  ; does not return. Exits by jumping to the reset vector

FuncTestModes3_4:
        ; Future: If functional test modes 3 or 4 are ever used, they will go here.

RunRegular:
;No test mode, or the local diag mode was selected...
;Check to see if the initialize sequence has been activated
        CALL        #ScanHardware
        CALL        #ScanHardware
        CALL        #ProcessKey
        BIT.B       #INIT_KEYS_SET,&KeyFlags
        JZ          INIT_EEPROM_X                   ;are all keys in row pushed? NO -- skip
        CALL        #SET_FLASH_DEFAULTS
        JMP         CHECK_FOR_DIAG_END
INIT_EEPROM_X:
;Now verify that the messages in Data flash are terminated properly
;Without proper termination, the display routine will not stop .
        MOV.B       #EOM,R6                         ;Get terminator character
        CMP.B       &DfltKypdMsgEndEE,R6            ;Check Default keypad top line message.
        JEQ         INITEE_CHK_MODEL
        MOV.W       #DfltKypdMsgEndEE,R7
        CALL        #WriteFlash                     ;Add terminator if required
INITEE_CHK_MODEL:
        CMP.B       &DfltModelMsgEndEE,R6           ;Check model number message.
        JEQ         CHECK_FOR_DIAG_END
        MOV.W       #DfltModelMsgEndEE,R7
        CALL        #WriteFlash                     ;Add terminator if required
CHECK_FOR_DIAG_END:
        POP_ALL
        RET



;
;------------------------------------------------------------------------------------
;                   Defaults
;------------------------------------------------------------------------------------
;
SET_FLASH_DEFAULTS:
        MOV.W       #DFLT_PROG_DATA_LENGTH, R4      ;Get length of programming
        MOV.W       #FLASHDfltSave, R5              ;Get begining address of dflt programming
        MOV.W       #FLASHStart, R7                 ;Get begining address of flash programming
SFD_Loop:
        MOV.B       @R5, R6                         ;copy byte from dflt to flash programming
        CALL        #WriteFlash                     ;Write to flash
        INC         R7                              ;inc Flash pntr
        INC         R5                              ;inc Dflt pntr
        DJNZ        R4, SFD_Loop                    ;loop until done
        RET


; Called by C. Push any non-scratch registers that may be used.
INIT_PORTS:
        MOV.B       #10000000B,&P1OUT    ;P1.7 -- set high to enable pull-up for this production test pin
        MOV.B       #0,&P2OUT
        MOV.B       #00000011B,&P3OUT    ;P3.0, P3.1 -- set high to enable pull-ups for these 2 production test pins
        MOV.B       #00000011B,&P4OUT    ;P4.0, P4.1 -- set high to enable pull-ups for these 2 production test pins
        MOV.B       #0,&P5OUT
        MOV.B       #0,&P6OUT

        MOV.B       #RESET_P1DIR,&P1DIR
        MOV.B       #RESET_P1SEL,&P1SEL
        MOV.B       #RESET_P1REN,&P1REN

        MOV.B       #RESET_P2DIR,&P2DIR
        MOV.B       #RESET_P2SEL,&P2SEL
        MOV.B       #RESET_P2REN,&P2REN

        MOV.B       #RESET_P3DIR,&P3DIR
        MOV.B       #RESET_P3SEL,&P3SEL
        MOV.B       #RESET_P3REN,&P3REN

        MOV.B       #RESET_P4DIR,&P4DIR
        MOV.B       #RESET_P4SEL,&P4SEL
        MOV.B       #RESET_P4REN,&P4REN

        MOV.B       #RESET_P5DIR,&P5DIR
        MOV.B       #RESET_P5SEL,&P5SEL
        MOV.B       #RESET_P5REN,&P5REN

        MOV.B       #RESET_P6DIR,&P6DIR
        MOV.B       #RESET_P6SEL,&P6SEL
        MOV.B       #RESET_P6REN,&P6REN

//        MOV.B       #0,&P1IFG                       ;Port1 - Reset Port1 Interrupt Flags
        MOV.B       #0,&P2IFG                       ;Port2 - Reset Port2 Interrupt Flags
//        MOV.B       #00001101B,&P1IES               ;Port1 - Wiegand and Tamper Interrupt on High to Low Transistion
//        JBIT1       TamperSwitch,TamperSwitchPORT,InitTS
//        SBIT0       TamperSwitch,P1IES              ;If TamperSwitch is Low - Interrupt on Low to High Transistion
InitTS: //MOV.B       #00001101B,&P1IE                ;Port1 - Enable Interrupts
        RET
