
::NAME: 	1105lint.bat
::DESCRIPTION:	This batch file will execute lint on the 1105 wireless project
::ARGUMENTS:	This batch file accepts up to 9 arguments, the first being the home directory
::		for the project.  The remainding arguments are up to 8 switches for lints excecution
::EXAMPLE:	lint1105 C:\projects\wireless\trunk\transmitters\1105 -dDMP_1131_TRANSMITTER


::@ECHO OFF

:: Defines all the source files needed for the 1105 with relitive paths
SET SOURCEPATH=*.c ".\..\*.c" ".\..\..\Chipcon.c" ".\..\..\Hop.c" ".\..\..\Packet.c" ".\..\..\ProtocolTiming.c"

:: Defines the absolute path for the lint exe and indirect files
SET LINTPATH=C:\LINT80

:: Change directory to the home directory for the project
cd "%1"

:: Execute lint on the source files defined in SOURCEPATH
Lint-nt  +v  -i"%LINTPATH%"  std.lnt %2 %3 %4 %5 %6 %7 %8 %9 -os(LINT.OUT) %SOURCEPATH%

:: Execute lint the same as above but with output formated for ALOA
Lint-nt  +v  -i"%LINTPATH%"  std.lnt aloa.lnt %2 %3 %4 %5 %6 %7 %8 %9 -os(aloa.xml) %SOURCEPATH%

:: Run ALOA on the lint output generated above
aloa .\aloa.xml > aloa.txt

ECHO LINT is done