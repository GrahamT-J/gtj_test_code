/*H*****************************************************************************
FILENAME: KeypadDisplay.h

DESCRIPTION:

NOTES:

Copyright (c) Digital Monitoring Products Inc. 2010.  All rights reserved.

*****************************************************************************H*/
#ifndef  KEYPADDISPLAY_H
#define  KEYPADDISPLAY_H

#include "types.h"

#define MAX_SERIAL_NUMBER_LEN 8
#define EXIT_COUNT_DISABLED 0xFF // 0-254 are valid exit delay counts

extern byte exitDelayCount;
extern unsigned char exitDelayCountPanel;
extern unsigned char exitDelayCountPosition;

// fcn prototypes
extern void processKeypadDisplay(char * displayData, char count, char position);
extern void CopyFromKeypadDisplay(char * inputBuf, char * dispReg);
extern void CopyToKeypadDisplay(char * inputBuf, char * dispReg);
extern void ProcessExitDelay();
#endif
