/*******************************************************************************
FILENAME:

DESCRIPTION:

NOTES:

Copyright (c) Digital Monitoring Products Inc. 2010.  All rights reserved.

*******************************************************************************/

//////////////////////////////////////////////////////////////////////////////
// NOTE:
//
//     All references to model "1190" keypad should now be interpreted as
//     being model "9060" or "9063" due to a renumbering of the model.
//     All the 1190 comments will not be changed, as they are too numerous.
//////////////////////////////////////////////////////////////////////////////

/*----compilation control-----------------------------------------------------*/

/*******************************************************************************
INCLUDE FILES
*******************************************************************************/
/*----system and platform files (external libraries) -------------------------*/

/*----program files-----------------------------------------------------------*/
#include "types.h"
#include "Hardware.h"
#include "IOPortPins.h"
#include "TransmitterMain.h"

/*******************************************************************************
EXTERNAL REFERENCES   Only used if not available in a header file.
*******************************************************************************/
/*----data declarations-------------------------------------------------------*/
/*----function prototype------------------------------------------------------*/
extern void KEYPAD_MAIN_LOOP(void);

/*******************************************************************************
PRIVATE DECLARATIONS
*******************************************************************************/
/*----defines, typedefs, structs, unions, enums-------------------------------*/
/*----data declarations-------------------------------------------------------*/

/*----function prototype------------------------------------------------------*/

/*----macros------------------------------------------------------------------*/

/*******************************************************************************
PRIVATE FUNCTION DEFINITIONS
*******************************************************************************/

/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
__task void main(void)
{
  WDTCTL = WDTPW | WDTHOLD | WDTCNTCL | WDTSSEL;  // stop watchdog

  resetHardware(HARD_RESET);

  while (true)
  {
    KEYPAD_MAIN_LOOP();
    TransmitterMain();
  }   // loop forever
}




