/*******************************************************************************
FILENAME: Hardware.c

DESCRIPTION: Miscellaneous hardware descriptions not included elsewhere

NOTES:

Copyright (c) Digital Monitoring Products Inc. 2009.  All rights reserved.

*******************************************************************************/
/*----compilation control-----------------------------------------------------*/

/*******************************************************************************
INCLUDE FILES
*******************************************************************************/
/*----system and platform files (external libraries) -------------------------*/

/*----program files-----------------------------------------------------------*/
#include "Types.h"
#include "Hardware.h"
#include "IOPortPins.h"
#include "Sleep.h"
#include "Inputs.h"
#include "WirelessCommunication.h"
#include "KeypadDisplay.h"
#include "ChargerInputs.h"
#include "ResourceManager.h"

/*******************************************************************************
EXTERNAL REFERENCES   Only used if not available in a header file.
*******************************************************************************/
/*----data declarations-------------------------------------------------------*/
#ifdef USE_PANIC_PULSING
char LEDFlashCounter;
#endif

/*----function prototype------------------------------------------------------*/
extern void KEYPAD_INIT(void);
extern void INIT_TIMER_A(void);
extern void INIT_PORTS(void);
extern void CHECK_FOR_DIAG(void);
extern void ResetDegradedModeTimer(void);

/*******************************************************************************
PRIVATE DECLARATIONS
*******************************************************************************/
/*----defines, typedefs, structs, unions, enums-------------------------------*/
enum {Delta = ((MASTER_CLOCK / (SLEEP_MASTER_CLOCK / SLEEP_CLOCK_DIVIDER)) & 0xFFFE)};  // must be even for test
#define STARTUP_DELAY (SLEEP_TICKS_PER_SECOND * 2) // 2 Seconds

/*----data declarations-------------------------------------------------------*/
#ifdef USING_RECEIVER_HARDWARE
byte DummyPort;
#endif
byte RF_ONState;

/*----function prototype------------------------------------------------------*/

/*----macros------------------------------------------------------------------*/

/*******************************************************************************
PRIVATE FUNCTION DEFINITIONS
*******************************************************************************/
/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/


/*******************************************************************************
PUBLIC FUNCTION DEFINITIONS
*******************************************************************************/
/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
void initSys(ResetType resetType)
{
  int i;
  if (resetType == HARD_RESET)
  {
    BCSCTL1 &= ~XT2OFF;  // XT2= HF XTAL
    BCSCTL2 = SELM_0+DIVM_0+SELS+DIVS_0; // SMCLK = XT2 = 4 Mhz

    INIT_PORTS();

    // Set DCO clock to ~12 MHz
    DCOCTL = 0;
    BCSCTL1 |= 0x0F;               // Set Rsel to 15 (max)
    DCOCTL = 0x00;
    BCSCTL3 = XT2S_2;

    __bis_SR_register( __SR_OSC_OFF); // Turn off XT1 osc.

    // check for operational crystal
    do                                      // Wait for XT2 to start
    {
      IFG1 &= ~OFIFG;                       // Clear OSCFault flag
      for (i = 0xFF; i > 0; i--) __no_operation();           // Time for flag to set
    }
    while ((IFG1 & OFIFG));                 // OSCFault flag still set?

    BCSCTL1 |= (DIVA0 | DIVA1);  // Divide ACLK by 8 for the watchdog
  }

  if ((resetType == HARD_RESET) || (resetType == SOFT_RESET))
    InitResourceManager(); // setup default display and LEDs

  if ((resetType == HARD_RESET) || (resetType == WIRELESS_HW_RESET))
  {
    initRFChip();
    INIT_TIMER_A();  // Initializing the chipcon radio modifies TIMERA settings
  }

  if ((resetType == HARD_RESET) || (resetType == SOFT_RESET))
  {
    KEYPAD_INIT();
    SetupChargerIO();
    initInputs();

    // Degraded mode is not used in the wireless keypad. Make timer non-zero and never decrement it.
    ResetDegradedModeTimer();
  }

  if (resetType == HARD_RESET)
  {
    // NOTE: If diag (production test mode) is entered, this fcn doesn't return!
    //       CHECK_FOR_DIAG enables the watchdog if it writes to flash.
    CHECK_FOR_DIAG();
  }

  // Don't enable watchdog before calling CHECK_FOR_DIAG(). Functional test mode doesn't kick the dog.
  WDTCTL = WDTPW | WDTCNTCL | WDTSSEL;  // start watchdog, reset, and use ACLK

  if ((resetType == HARD_RESET) || (resetType == WIRELESS_HW_RESET))
  {
    TBCCR1 = TBR + RFBitTime;    // start RF slot timer
    TBCCTL1 = CCIE;
  }
}


/*******************************************************************************

DESCRIPTION: Function will setup the Timer A CCR1 for pulsing the LED for 50ms every 3sec for a duration of 5min

NOTES:

*******************************************************************************/
#ifdef USE_PANIC_PULSING
void startPanicPulsing(void)
{
  TACCR1 = TAR + LED_PULSE_OFF_TIME;  // set projected interrupt time value
  TACCTL1 = CCIE & ~CAP; // enable interrupt and set compare mode
  LEDFlashCounter = LED_FLASH_TIME;
}
#endif


/*******************************************************************************

DESCRIPTION:

NOTES:

*******************************************************************************/
void resetHardware(ResetType resetType)
{
  initSys(resetType);
  _EINT();
}
