;----------------------------------------------------------------
;
;   kpd_sound.s43 -
;
;   Copyright(c) Digital Monitoring Products 2004
;
;   Author: DIGITAL MONITORING PRODUCTS
;----------------------------------------------------------------

*******************************************************************************
*     Sound Manager                                                           *
*******************************************************************************


;-------------------------------------------------------------------------------
; *******************************************************************************
; *     Include files                                                           *
; *******************************************************************************


; *******************************************************************************
; *      Equates                                                                *
; *******************************************************************************


; *******************************************************************************
; *     Public declarations                                                     *
; *******************************************************************************
EXTERN BacklightTimer

; *******************************************************************************
; *     Public RAM variables                                                    *
; *******************************************************************************
        RSEG    UDATA0
                    RAM_EVEN                            ;align for 16 Bit Register usage
SysInt_Time_Base    DS      2                           ;clock ticks for system time interval
SndFlags            DS      1
CountUp             EQU     0x80                        ;
KeyClick            EQU     0x40                        ;
PwrUpBeepOn         EQU     0x20                        ;
KeyBeepOn           EQU     0x10                        ;
LclBeepOn           EQU     0x08                        ;
AudioOn             EQU     0x04                        ;
TimedStep           EQU     0x02                        ;
SongPlaying         EQU     0x01                        ;

AnyBeepOn           EQU     00110000B
ClrBeepFlgs         EQU     11001111B
ClrAudioFlgs        EQU     11111000B

SndFlags1           DS      1
WasNote             EQU     0x80                        ;
ClrWasNote          EQU     01111111B


AudioFreqNumber     DS      1                           ;bit position value for number of bars to graph according to
                                                        ;      the selected tone (used also to determine the system interrupt time interval)
VolumeDutyCycle     DS      1                           ;value for volume

CurrentVolumeDutyCycle DS   1                           ;
UpDownCntr          DS      1                           ;3 Sec. Up/Down Counter 1-8 for Burg Alert Volume
BeepTimer           DS      1

CurrentAudio        DS      1                           ;When Buzzer bit goes off:
FULL_CYCLE          EQU     0x80                        ;  Play complete song (Door monitor only)
NOTE_CYCLE          EQU     0x40                        ;  Play note only (Entry, Fire Alert, Burg Alert)
SoundTimer          DS      1                           ;sound timer
DebounceTimer       DS      1

; *******************************************************************************
; *     local RAM variables                                                     *
; *******************************************************************************
        RSEG        UDATA0
                    RAM_EVEN                            ;align for 16 Bit Register usage
SongPointer         DS      2
AudioIncrement      DS      2                           ;resets ramp rate (added to audio period each interrupt)
AudioIncNeg         EQU     0x80                        ;  .dbit 7,AudioIncrement+1
EndingFreq          DS      2
VolumePointer       DS      2                           ;pointer to current song volume
AudioSave1          DS      2                           ;buffer to save audio values during beep
AudioSave2          DS      2
AudioSave3          DS      2
FreqTemp            DS      2



TmrBIncr            DS      1                           ;TimerB Increment Divider
PrevOTAAudibles     DS      1


SongIdx             DS      1                           ;index into song

CurrentElement      DS      1
PLAY_TONE           EQU     0x80                        ;
PLAY_NOTE           EQU     0x40                        ;
PLAY_PAUSE          EQU     0x20                        ;


; *******************************************************************************
; *     Public ROM Constants                                                    *
; *******************************************************************************
        RSEG    CONST

        EVEN
AudioFrequencies:
NOTE_1_PER              DW       SOUND_BASE_FREQ/1244           ; 1244.51 Hz D#6
NOTE_2_PER              DW       SOUND_BASE_FREQ/1397           ; 1396.91 Hz F6
NOTE_3_PER              DW       SOUND_BASE_FREQ/1568           ; 1567.98 Hz G6
NOTE_4_PER              DW       SOUND_BASE_FREQ/1661           ; 1661.22 Hz G#6
NOTE_5_PER              DW       SOUND_BASE_FREQ/1865           ; 1864.66 Hz A#6
NOTE_6_PER              DW       SOUND_BASE_FREQ/2093           ; 2093.00 Hz C6
NOTE_7_PER              DW       SOUND_BASE_FREQ/2349           ; 2349.32 Hz D6
NOTE_8_PER              DW       SOUND_BASE_FREQ/2489           ; 2489.02 Hz D#7
NOTE_1                  EQU     (NOTE_1_PER-AudioFrequencies)/2
NOTE_2                  EQU     (NOTE_2_PER-AudioFrequencies)/2
NOTE_3                  EQU     (NOTE_3_PER-AudioFrequencies)/2
NOTE_4                  EQU     (NOTE_4_PER-AudioFrequencies)/2
NOTE_5                  EQU     (NOTE_5_PER-AudioFrequencies)/2
NOTE_6                  EQU     (NOTE_6_PER-AudioFrequencies)/2
NOTE_7                  EQU     (NOTE_7_PER-AudioFrequencies)/2
NOTE_8                  EQU     (NOTE_8_PER-AudioFrequencies)/2


EntrySong_AltAddr       DW      EntrySong_Alt           ; 14 (prewarn w/alarm)

                        EVEN
                        DB      0
SteadyTone              DB      0
                        DW      FULL_VOL
                        DB      PLAY_TONE + TONE_3,0
                        DB      SongEnd

                        EVEN
                        DB      0
MonitorSong             DB      FULL_CYCLE
                        DW      VolumeLevelEE
                        DB      PLAY_NOTE + NOTE_3,SONG_HALF_SEC
                        DB      PLAY_NOTE + NOTE_1,SONG_HALF_SEC
                        DB      SongEnd


                        EVEN
                        DB      0
EntrySong               DB      NOTE_CYCLE
                        DW      VolumeLevelEE
ESONG_1                 DB      PLAY_NOTE + NOTE_1,Eighth_Note
                        DB      PLAY_NOTE + NOTE_2,Eighth_Note
                        DB      PLAY_NOTE + NOTE_3,Eighth_Note
                        DB      PLAY_NOTE + NOTE_2,Eighth_Note
                        DB      PLAY_NOTE + NOTE_2,Eighth_Note
                        DB      PLAY_PAUSE + 04H,Full_Note
                        DB      SongRepeat
                        DB      ($-ESONG_1) - 1

                        EVEN
                        DB      0
EntrySong_Alt           DB      NOTE_CYCLE
                        DW      VolumeLevelEE
ESONGA_1                DB      PLAY_NOTE + NOTE_1,Eighth_Note
                        DB      PLAY_NOTE + NOTE_5,Eighth_Note
                        DB      PLAY_NOTE + NOTE_1,Eighth_Note
                        DB      PLAY_NOTE + NOTE_5,Eighth_Note
                        DB      PLAY_NOTE + NOTE_1,Eighth_Note
                        DB      PLAY_PAUSE + 04H,Full_Note
                        DB      SongRepeat
                        DB      ($-ESONGA_1) - 1

                        EVEN
                        DB      0
FireAlert               DB      NOTE_CYCLE
                        DW      FULL_VOL
FA_1                    DB      PLAY_TONE + TONE_1,SONG_ONE_SEC
                        DB      PLAY_PAUSE + 04H,SONG_ONE_SEC
                        DB      SongRepeat
                        DB      ($-FA_1) - 1

                        EVEN
                        DB      0
BurgAlert               DB      NOTE_CYCLE
                        DW      UpDownCntr
BA_1                    DB      PLAY_TONE + TONE_2,0
                        DB      SongRepeat
                        DB      ($-BA_1) - 1



; *******************************************************************************
; *     Local ROM Constants                                                     *
; *******************************************************************************
        RSEG    CONST

        EVEN


FULL_VOL                DB        7

                        EVEN
;
SOUND_BASE_FREQ         EQU     CLOCK_FREQ/2

FireSFrq                DW       SOUND_BASE_FREQ/1244           ; 1244.51 Hz D#6
FireEFrq                DW       SOUND_BASE_FREQ/2489           ; 2489.02 Hz D#7
BurgSFrq                DW       SOUND_BASE_FREQ/2489           ; 2489.02 Hz D#7
BurgEFrq                DW       SOUND_BASE_FREQ/912
NoEndFrq                DW       0
MaxEndFrq               DW       0FFFH
MinFreqTime             EQU     0200H

TONE_TABLE:
TONE_1_VEC              DW      FIRE_TONE
TONE_2_VEC              DW      BURG_TONE
TONE_3_VEC              DW      STDY_TONE
TONE_4_VEC              DW      NO_TONE
TONE_5_VEC              DW      NO_TONE
TONE_6_VEC              DW      NO_TONE
TONE_7_VEC              DW      NO_TONE
TONE_8_VEC              DW      NO_TONE
TONE_1                  EQU     (TONE_1_VEC-TONE_TABLE)/2
TONE_2                  EQU     (TONE_2_VEC-TONE_TABLE)/2
TONE_3                  EQU     (TONE_3_VEC-TONE_TABLE)/2
TONE_4                  EQU     (TONE_4_VEC-TONE_TABLE)/2
TONE_5                  EQU     (TONE_5_VEC-TONE_TABLE)/2
TONE_6                  EQU     (TONE_6_VEC-TONE_TABLE)/2
TONE_7                  EQU     (TONE_7_VEC-TONE_TABLE)/2
TONE_8                  EQU     (TONE_8_VEC-TONE_TABLE)/2

FIRE_TONE               DW      FireAlarmInc
                        DW      FireSFrq
                        DW      FireEFrq

BURG_TONE               DW      BurgAlarmInc
                        DW      BurgSFrq
                        DW      BurgEFrq

STDY_TONE               DW      0
                        DW      AudioFreqEE
                        DW      MaxEndFrq

NO_TONE                 DW      0
                        DW      AudioFreqEE
                        DW      NoEndFrq

VolumeLevelGraph:
AudioFrequencyGraph:
BrightnessLevelGraph:
                        DB      000h
                        DB      001h
                        DB      003h
                        DB      007h
                        DB      00Fh
                        DB      01Fh
                        DB      03Fh
                        DB      07Fh
                        DB      0FFh


; *******************************************************************************
; *     Start of program code                                                   *
; *******************************************************************************
        RSEG    CODE


SOUND_MGR:
        ;
        ; Process Keypad Status Params "AUDIBLE" field
        ; by simulating reception of remote command #7
        ;
        ; First, test to see if the over-the-air message AUDIBLES field has changed.
        ; If not, then don't change anything.
        MOV.B       &KEYPAD_STATUS_1,R4
        AND.B       #AUDIBLES_MASK,R4
        CMP.B       R4,&PrevOTAAudibles
        JZ          ??OTA_AUDIBLES_END
        MOV.B       R4,&PrevOTAAudibles
        
        ; It changed, so process
        RRC.B       R4
        RRC.B       R4
        RRC.B       R4
        RRC.B       R4
        RRC.B       R4
        AND.B       #0x07,R4
        JZ          ??OTA_AUDIBLES_OFF
        MOV.B       #0FH,CurrentAudio                       ;Clear current audio selection
        DEC.B       R4
        AND.B       #0xF8,&RemoteCmd7
        ADD.B       R4,&RemoteCmd7
        
        CMP.B       #2,R4                                  ;monitor tone?
        JNE         ??OTA_AUDIBLES_1
        MOV.B       #25,&BuzzTimer                         ;set timer to be at least 2*SONG_HALF_SEC*12 mS (1.128 Sec) --> (48 mS units)
??OTA_AUDIBLES_1:

        SBIT1       BuzzerCmd,RemoteCmd2
        SBIT1       RxDExtendedCom,Flags2
        SBIT1       AudioOn,SndFlags
        JMP         ??OTA_AUDIBLES_END

??OTA_AUDIBLES_OFF:
        ; If previous was a non-momentary value, then process OFF, else ignore
        MOV         &KEYPAD_STATUS_1_Prev,R5
        AND.B       #AUDIBLES_MASK,R5
        
        CMP.B       #AUDIBLES_STEADY,R5
        JEQ         ??OTA_AUDIBLES_OFF_2  ; turn off
        
        CMP.B       #AUDIBLES_FIRE,R5
        JEQ         ??OTA_AUDIBLES_OFF_2  ; turn off
        
        CMP.B       #AUDIBLES_BURGLARY,R5
        JEQ         ??OTA_AUDIBLES_OFF_2  ; turn off
        
        CMP.B       #AUDIBLES_PREWARN,R5
        JNE         ??OTA_AUDIBLES_END    ; momentary so ignore
??OTA_AUDIBLES_OFF_2:
        SBIT0       BuzzerCmd,RemoteCmd2
        AND.B       #0xF8,&RemoteCmd7                       ;clear audible alerts

??OTA_AUDIBLES_END:

        JBIT1       PwrUpBeepOn,SndFlags,SMGR_CHK_BTMR      ;If Initial Power Up, check timer (Set to 1 sec. in INIT routines)
        JBIT1       KeyBeepOn,SndFlags,SMGR_CHK_BTMR        ;If Key beep on, check timer (Set to KeyClickTime in BeepOn)
        JBIT1       KeyClick,SndFlags,SMGR_SET_BEEP         ;If New Key pressed, turn on beep and set timer
        JBIT1       BuzzerCmd,RemoteCmd2,SMGR_REMOTE_ON     ;If Control Panel sending ON, turn on
        JBIT1       LclBeepOn,SndFlags,SMGR_CHK_LCL_BEEP    ;If in local beep, check it
        JBIT0       AudioOn,SndFlags,SMGR_CLR_SOUNDS        ;Else --  Turn off neatly
        JBIT1       FULL_CYCLE,CurrentAudio,SMGR_OFF_CYCLE  ;Wait for end of song
        JBIT1       NOTE_CYCLE,CurrentAudio,SMGR_OFF_CYCLE  ;Wait for end of note
SMGR_CLR_SOUNDS
        AND.B       #ClrAudioFlgs,SndFlags                  ;Turn off all audio
        AND.B       #ClrWasNote,SndFlags1                   ;Clear WasNote
        MOV.B       #0FH,CurrentAudio                       ;Clear current audio selection
        CALL        #SET_BEEP_FROM_EE                       ;Set frequency to default setting
        RET

SMGR_CHK_BTMR
        CMP.B       #0,BeepTimer                            ;Beep timer done?
        JZ          SMGR_BEEP_OFF                           ;NO - Set audio on & return
        SBIT1       AudioOn,SndFlags
        RET
SMGR_BEEP_OFF
        AND.B       #ClrBeepFlgs,SndFlags                   ;YES - Clear PwrUpBeep & KeyBeepOn
        AND.B       #ClrWasNote,SndFlags1                   ;      Clear WasNote
        SBIT0       AudioOn,SndFlags                        ;      turn off audio
        JBIT0       SongPlaying,SndFlags,SMGR_X             ;Was a song playing?
        CALL        #RESTORE_CUR_SONG                       ;YES - Restore song
SMGR_X  RET

SMGR_SET_BEEP
        SBIT0       KeyClick,SndFlags                       ;Clear Keyclick
        JBIT0       SongPlaying,SndFlags,SM_SET1            ;Song playing now?
        CALL        #SAVE_CUR_SONG                          ;Yes -- save current song
SM_SET1 CALL        #SET_BEEP_FROM_EE                       ;Get Beep frequency and volume
        MOV.B       #KeyClickTime,BeepTimer                 ;If Key Pressed -- set beep time
        SBIT1       KeyBeepOn,SndFlags                      ;Set Flag to decrement BeepTimer and quit
        RET

SMGR_CHK_LCL_BEEP
        JBIT1       AudioOn,SndFlags,SMGR_X                 ;if audio already on - return
        CALL        #SET_BEEP_FROM_EE
        SBIT1       AudioOn,SndFlags
SM_MAX  MOV.W       #FULL_VOL,VolumePointer                 ;get audio volume
        RET

SMGR_OFF_CYCLE:
SMGR_REMOTE_ON:
        CLR.B       R5                                      ;Load Steady Tone
        ;(always extended) JBIT0       RxDExtendedCom,Flags2,SMGR_CHK_CUR      ;If not extended, use Steady
        MOV.B       RemoteCmd7,R5                           ;Else get type from panel
        AND.B       #07H,R5                                 ;Trim unused bits
        CMP.B       #4,R5                                   ;Prewarn?
        JNE         SMGR_CHK_CUR                            ;No -- don't check for previous alarm
        JBIT0       RedBackLiteOn,Flags2,SMGR_CHK_CUR       ;If previous alarm (Red Backlight On) --
        MOV.B       #0EH,R5                                 ;Change Prewarn to Alternate Entry
SMGR_CHK_CUR
        MOV.B       CurrentAudio,R4                         ;Get currently active selection
        AND.B       #0FH,R4                                 ;Trim unused bits
        CMP.B       R4,R5                                   ;Are they the same?
        JEQ         SMGR_SAME_SOUND                         ;Yes, continue same sound

; ************** New Sound **************

        AND.B       #ClrAudioFlgs,SndFlags                  ;Else, turn off sound
        AND.B       #ClrWasNote,SndFlags1                   ;      Clear WasNote
        MOV.B       R5,CurrentAudio                         ;And save new sound selection
        CALL        #GET_SONG_POINTER
        MOV.W       SongPointer,R12
        MOV.B       @R12,R4                                 ;Get flags (FULL_CYCLE, NOTE_CYCLE)
        BIS.B       R4,CurrentAudio                         ;Merge with song selection
        INC.W       SongPointer
        CALL        #GET_CUR_AUDIO_VOL_PTR
        MOV.B       #3,SongIdx                              ;Set song index to point to first song element
        RET                                                 ;And stop until next cycle

SMGR_SAME_SOUND
        CALL        #SET_CUR_AUDIO_VOL
        JBIT0       SongPlaying,SndFlags,SMGR_NEXT_STEP     ;if not currently playing, get first step
        JBIT1       TimedStep,SndFlags,SMGR_TIMED_STEP      ;if not a timed step, check for freq
        ;check for going past ending frequency
        MOV.W       SysInt_Time_Base,R12
        MOV.W       EndingFreq,FreqTemp
        JBIT1       AudioIncNeg,AudioIncrement+1,SMGR_NEG   ;if audio increment is neg, leave as is
        MOV.W       EndingFreq,R12                          ;else reverse
        MOV.W       SysInt_Time_Base,FreqTemp               ;values
SMGR_NEG
        SUB.W       FreqTemp,R12                            ;now see if frequency has been reached
        JLO         SMGR_NEXT_STEP                          ;go to next step if freq reached
SMGR_WAIT
        JBIT1       PLAY_PAUSE,CurrentElement,SMGR_X        ;if a pause, do not turn on sound
        SBIT1       AudioOn,SndFlags
        RET

SMGR_TIMED_STEP
        CMP.B       #0,SoundTimer
        JNZ         SMGR_WAIT                               ;if timer not done, wait

SMGR_NEXT_STEP
        SBIT1       SongPlaying,SndFlags
        CALL        #GET_CUR_SONG_POINTER
        MOV.B       SongIdx,R4                              ;
        ADD.W       R4,SongPointer                          ;go to next step
        MOV.W       SongPointer,R12
        MOV.B       @R12,R4                                 ;Get next element from song in R4
        INC.W       SongPointer                             ;Point to second byte of song element
        MOV.B       R4,CurrentElement
        CMP.B       #SongEnd,R4                             ;End of Song?
        JEQ         SMGR_SONG_END
        CMP.B       #SongRepeat,R4                          ;Repeat Song?
        JEQ         SMGR_REPEAT

SMGR_SONG_GO                                                ;Else -- continue Song
        JBIT1       BuzzerCmd,RemoteCmd2,SMGR_SONG_GO_L1    ;If Panel has turned off buzzer
        JBIT0       FULL_CYCLE,CurrentAudio,SMGR_CLR_SOUNDS ;  and not a FULL_CYCLE song -- stop song
SMGR_SONG_GO_L1                                             ;
        JBIT1       WasNote,SndFlags1,SMGR_NOTE_PAUSE       ;Insert a pause if last sound was a note
        MOV.W       SongPointer,R12                         ;
        MOV.B       @R12,R4                                 ;get song element time value
        MOV.B       R4,SoundTimer                           ;
        ADD.B       #SongStepVal,SongIdx                    ;point song index to next song element
        MOV.B       CurrentElement,R5                       ;get current song element
        JBIT1       PLAY_PAUSE,CurrentElement,SMGR_SOUND_PAUSE   ;if a pause, turn off sound
        JBIT1       PLAY_NOTE,CurrentElement,SMGR_NOTE_GO   ;is this song element a note?

SMGR_TONE_GO                                                ;no, must be a tone
        SBIT0       WasNote,SndFlags1                       ;
        AND.B       #07H,R5                                 ;mask unused bits
        RLA.B       R5                                      ;*2
        MOV.W       TONE_TABLE(R5),R12                      ;get the pointer to the tone definition
        MOV.W       R12,SongPointer                         ;
        MOV.W       @R12+,AudioIncrement                    ;Get AudioIncrement from Tone definition
        MOV.W       @R12+,R13                               ;
;      BIC.W        #CCIE,&TBCCTL0                          ;Disable TimerB Compare Interrupt during change
        MOV.W        @R13,SysInt_Time_Base                  ;Get Start frequency
        MOV.W       @R12,R13                                ;
        MOV.W       @R13,EndingFreq                         ;Get Ending freqency
;      BIS.W        #CCIE,&TBCCTL0                          ;Enable TimerB Compare Interrupts
        CMP.B       #0,SoundTimer                           ;is the sound timer 0?
        JNE         SMGR_SET_TIMER                          ;no, set flag
        SBIT0       TimedStep,SndFlags                      ;
        SBIT1       AudioOn,SndFlags                        ;
        RET

SMGR_NOTE_GO
        SBIT1       WasNote,SndFlags1                       ;
        AND.B       #FrequencyMax,R5                        ;make sure note is playable
        MOV.B       R5,AudioFreqNumber                      ;
        CALL        #SET_FREQUENCY                          ;set the frequency number
        CLR.W       AudioIncrement                          ;notes do not increment
SMGR_SET_TIMER                                              ;
        SBIT1       TimedStep,SndFlags                      ;notes are always timed
        SBIT1       AudioOn,SndFlags                        ;
        RET                                                 ;

SMGR_NOTE_PAUSE                                             ;Put a 1 cycle pause after each note
        MOV.B       #1,SoundTimer                           ;
        MOV.B       #PLAY_PAUSE,CurrentElement              ;
SMGR_SOUND_PAUSE                                            ;
        SBIT0       WasNote,SndFlags1                       ;
        SBIT1       TimedStep,SndFlags                      ;pauses are always timed
        SBIT0       AudioOn,SndFlags                        ;
        RET

SMGR_SONG_END                                               ;
        JBIT0       BuzzerCmd,RemoteCmd2,SMGR_CLR_SOUNDS
        SBIT0       AudioOn,SndFlags                        ;
        RET

SMGR_REPEAT
        MOV.W       SongPointer,R12                         ;
        MOV.B       @R12,R4                                 ;get next step OFFSET value from song
        SUB.B       R4,SongIdx                              ;backup index pointer to beginning of song
        JBIT1       BuzzerCmd,RemoteCmd2,SMGR_NEXT_STEP     ;if Buzzer on -- repeat
        JMP         SMGR_CLR_SOUNDS                         ;Else -- clear


GET_CUR_SONG_POINTER
        MOV.B       CurrentAudio,R5                         ;get current Audio selection
        AND.B       #0FH,R5                                 ;
GET_SONG_POINTER
        CMP.B       #0EH,R5                                 ;if CurrentAudio (now in R5) is 0EH, point to EntrySong_Alt
        JNE         GET_SONG_POINTER_NORMAL                 ;
        MOV.W       EntrySong_AltAddr,SongPointer           ;
        RET
GET_SONG_POINTER_NORMAL
        AND.B       #0x07,R5                                ;
        RLA.B       R5                                      ;type *2
        MOV.W       AudibleTableEE(R5),SongPointer;
        RET

GET_CUR_AUDIO_VOL
        CALL        #GET_CUR_SONG_POINTER                   ;
        INC.W       SongPointer                             ;
GET_CUR_AUDIO_VOL_PTR
        MOV.W       SongPointer,R13                         ;
        MOV.W       @R13,VolumePointer                      ;Load volume pointer from song
SET_CUR_AUDIO_VOL
        MOV.W       VolumePointer,R13                       ;
        MOV.B       @R13,R4                                 ;get volume
        CMP.B       CurrentVolumeDutyCycle,R4               ;
        JNE         CUR_VOL_CHANGED                         ;
        RET
CUR_VOL_CHANGED
        MOV.B       R4,CurrentVolumeDutyCycle               ;
        MOV.B       R4,R5                                   ;
        AND.B       #0FH,R5                                 ;
        MOV.B       VolumeLevelGraph(R5),R4                 ;
        MOV.B       R4,VolumeDutyCycle                      ;
        RET

SAVE_CUR_SONG
        MOV.W       AudioIncrement,AudioSave1               ;
        MOV.W       SysInt_Time_Base,AudioSave2             ;
        MOV.W       EndingFreq,AudioSave3                   ;
        RET

RESTORE_CUR_SONG
        CALL        #GET_CUR_AUDIO_VOL                  ;
        MOV.W       AudioSave1,AudioIncrement           ;
        MOV.W       AudioSave2,SysInt_Time_Base         ;
        MOV.W       AudioSave3,EndingFreq               ;
        RET

SET_BEEP_FROM_EE
        CLR.W       AudioIncrement                      ;
        MOV.W       #VolumeLevelEE,VolumePointer        ;Set VolumePointer to FLASH volume value
        CALL        #SET_CUR_AUDIO_VOL                  ;
        MOV.B       &AudioToneEE,AudioFreqNumber        ;use audio frequency number from FLASH
        CALL        #SET_FREQUENCY                      ;restore frequency number
        RET

SET_FREQUENCY
        MOV.B       AudioFreqNumber,R5                  ;get present audio frequency number (0-7)
        AND.B       #FrequencyMax,R5                    ;mask frequency number
        RLA.B       R5                                  ;multiply by 2
        MOV.W       AudioFrequencies(R5),SysInt_Time_Base ;put audio period in interrupt timer
        RET

*******************************************************************************
*     Sound Interrupt Routine                                               *
*******************************************************************************


SOUND_INTERUPT:
        JBIT0       AudioOn,SndFlags,TMB_AudioOff       ;is audio on?
        XOR.B       #AudioOutPin,AudioOutPinPORT        ;YES, toggle audio output pin
        CMP         #MinFreqTime,SysInt_Time_Base
        JLO         TMB_Audx
        INC.B       TmrBIncr
        AND.B       #0x07,TmrBIncr                      ;Only increment every 8th interrupt
        JNZ         TMB_Audx
        ADD.W       AudioIncrement,SysInt_Time_Base
        JMP         TMB_Audx
TMB_AudioOff:
        AudioOutPinOFF
TMB_Audx
        EINT
        RL_B        PowerLEDDutyCycle                   ;rotate power LED intensity ring counter
        JBIT1       RedBackLiteOn,Flags2,TMB_Red

        RL_B        LightsDutyCycle                     ;rotate backlight intensity ring counter
        JBIT0       LightsEnable,LightsDutyCycle,TMB_Grnx ;LSB set?

        ; if running from battery, and backlight timer expired, force green backlight and LCD backlight off
        BIT.B       #0xFF,&BacklightTimer
        JEQ         TMB_Grnx
        GrnBackLite_ON                    ;YES, turn ON backlight and LCD.
        LCDBackLiteON
        JMP         TMB_Vol
TMB_Grnx
        GrnBackLite_OFF                   ;NO, turn them OFF
        LCDBackLiteOFF
        JMP         TMB_Vol

TMB_Red RL_B        LightsDutyCycle                     ;rotate backlight intensity ring counter
        JBIT0       LightsEnable,LightsDutyCycle,TMB_Redx ;LSB set?
        RedBackLite_ON                                  ;Yes, Turn ON
        LCDBackLiteON
        JMP         TMB_Vol
TMB_Redx
        RedBackLite_OFF                                 ;No, Turn OFF
        LCDBackLiteOFF

TMB_Vol RL_B        VolumeDutyCycle                     ;rotate volume ring counter
        JNC         TMB_VolOn
        VolumeOutPinON
        JMP         TMB_Volx
TMB_VolOn
        VolumeOutPinOFF
TMB_Volx
FRQ_TM0:
        CMP.B       #0,&DebounceTimer
        JEQ         DBT_TM0
        DEC.B       &DebounceTimer
DBT_TM0:
        BIS.W     #CCIE,&TBCCTL0                        ;Enable Compare interrupts

        RET
