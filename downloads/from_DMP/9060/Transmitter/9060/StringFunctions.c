/*H*****************************************************************************
FILENAME:

DESCRIPTION:

NOTES:

Copyright (c) Digital Monitoring Products Inc. 2004.  All rights reserved.

*****************************************************************************H*/
/*----compilation control-----------------------------------------------------*/

/*******************************************************************************
INCLUDE FILES
*******************************************************************************/
/*----system and platform files (external libraries) -------------------------*/
#include <string.h>
#include <stdlib.h>

/*----program files-----------------------------------------------------------*/
#include "types.h"      /* Company standards. */
#include "StringFunctions.h"     /* Module specific definitions. */

/*******************************************************************************
EXTERNAL REFERENCES
*******************************************************************************/
/*----data declarations-------------------------------------------------------*/

/*----function prototype------------------------------------------------------*/

/*******************************************************************************
PRIVATE DECLARATIONS
*******************************************************************************/
/*----defines, typedefs, structs, unions, enums-------------------------------*/

/*----data declarations-------------------------------------------------------*/

/*----function prototype------------------------------------------------------*/

/*----macros------------------------------------------------------------------*/

/*******************************************************************************
PRIVATE FUNCTION DEFINITIONS
*******************************************************************************/

/*******************************************************************************

DESCRIPTION:  Converts long 'val' into a string 'string' of length 'length'

NOTES:

*******************************************************************************/
void longToAscii(long val, char* string, char length)
{
  ldiv_t result;
  char i;
  char* writeString = string + length - 1;  // start at rightmost character

  result.quot = val;     // start with value in quotient

  for (i = 0; i < length; ++i)
  {
    result = ldiv(result.quot, 10);
    *writeString = result.rem + '0';
    --writeString;
  }
}
