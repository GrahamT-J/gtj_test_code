/*******************************************************************************
FILENAME:

DESCRIPTION:

NOTES:

Copyright (c) Digital Monitoring Products Inc. 2004.  All rights reserved.

*******************************************************************************/
#ifndef  SLEEP_H
#define  SLEEP_H

/*----compilation control-----------------------------------------------------*/

/*******************************************************************************
INCLUDE FILES
*******************************************************************************/
/*----system and platform files-----------------------------------------------*/
#include "types.h"

/*----program files-----------------------------------------------------------*/
#include "Hardware.h"
#include "ProtocolTiming.h"
#include "Hop.h"

/*******************************************************************************
FILE CONTEXT
*******************************************************************************/
/*----defines, typedefs, structs, unions, enums-------------------------------*/
enum
{
  SLEEP_TICKS_PER_SECOND = (int)((SLEEP_MASTER_CLOCK / SLEEP_CLOCK_DIVIDER) / SLEEP_TIMERA_DIVIDER),
  SLEEP_TICKS_PER_HOP = SLEEP_TICKS_PER_SECOND / HOPS_PER_SECOND,
  SLEEP_TICKS_PER_HOP_ROUND = SLEEP_TICKS_PER_HOP * NUM_HOP_CHANNELS,
#ifdef THREE_SECOND_WAKEUP_OUTPUT
  WAKEUP_SECONDS = 3,
  PREVENT_COMPILER_WARNINGS = 60000,  // without this, compiler warns about LED_PULSE_ON_TIME calc being out of range
#else
  WAKEUP_SECONDS = 60,
#endif
  WAKEUP_RATE = ((unsigned short)(((unsigned long)WAKEUP_SECONDS * HOPS_PER_SECOND) * SLEEP_TICKS_PER_HOP))
};

enum
{
  CLOCK_TOLERANCE_PPM = 100,  // crystal tolerance of transmitter and receiver
  SLEEP_TICK_TOLERANCE = (int)(((double)CLOCK_TOLERANCE_PPM / 1000000) * SLEEP_TICKS_PER_SECOND * WAKEUP_SECONDS),
#ifdef THREE_SECOND_WAKEUP_OUTPUT
  // TODO: calculate from clock speed and tolerance values
  //  using previous SLEEP_TICK_TOLERANCE calc value is 0, testing has shown 3 to be 
  //  the lowest consistently good value
  WAKEUP_CLOCK_SLOP_TICKS = 3
#else
  WAKEUP_CLOCK_SLOP_TICKS = 2 * SLEEP_TICK_TOLERANCE // multiply by two in case receiver and transmitter at opposite ends
#endif
};

typedef enum
{
  TS_SUCCESS,
  TS_FAILURE,
  TS_RETRY
} TransmitStatus;

enum
{
#ifdef DMP_1145_TRANSMITTER
  LED_PULSE_TIME_BASE = (int)((SLEEP_TICKS_PER_SECOND *50) / 1000),   // 50 ms
  LED_PULSE_OFF_TIME = SLEEP_TICKS_PER_SECOND / 2,
  SHORT_LED_PULSE_TIME = LED_PULSE_OFF_TIME,  // 1/2 second
  MEDIUM_LED_PULSE_TIME = SLEEP_TICKS_PER_SECOND, // 1 second
  LONG_LED_PULSE_TIME = SLEEP_TICKS_PER_SECOND * 2, // 2 second
#else
  LED_PULSE_OFF_TIME = SLEEP_TICKS_PER_SECOND * 3,
  LED_PULSE_ON_TIME = (int)((SLEEP_TICKS_PER_SECOND *50) / 1000),
#endif
  LED_FLASH_TIME = 100,  // 5 minutes based on 3 sec timer
  LED_ACK_COUNT = 3   //Flash LED for three more acks after tamper
};

/*----data descriptions-------------------------------------------------------*/
extern word sleepTime;

/*----data declarations-------------------------------------------------------*/

/*----macros------------------------------------------------------------------*/

/*----function prototypes-----------------------------------------------------*/
void goToSleep(TransmitStatus status);

extern bool wakeup(bool isAlarmWakeup, bool isCommandWakeup);
extern bool getSleeping(void);
extern bool getCurrentlyCommunicating(void);
extern bool getAlarmWakeup(void);
extern bool receiverFailed(void);
extern bool getCommandStatus(void);
#ifdef ENABLE_RECEIVER_COMMANDS
#ifdef ENABLE_COMMAND_CANCEL
extern void CancelCommandReceive(void);
#endif
#endif
extern SlotNumber FindCurrentSlotNumber(SlotNumber);

#endif                                  /* end of file */
