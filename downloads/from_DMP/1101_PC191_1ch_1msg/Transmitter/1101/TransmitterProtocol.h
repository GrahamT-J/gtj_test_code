/*******************************************************************************
FILENAME:

DESCRIPTION:

NOTES:

Copyright (c) Digital Monitoring Products Inc. 2008.  All rights reserved.

*******************************************************************************/
#ifndef  TRANSMITTERPROTOCOL_H
#define  TRANSMITTERPROTOCOL_H

/*----compilation control-----------------------------------------------------*/

/*******************************************************************************
INCLUDE FILES
*******************************************************************************/
/*----system and platform files-----------------------------------------------*/

/*----program files-----------------------------------------------------------*/

/*******************************************************************************
FILE CONTEXT
*******************************************************************************/
/*----defines, typedefs, structs, unions, enums-------------------------------*/
typedef enum
{
  RAS_GOOD_MESSAGE_ACK_RECEIVED,
  RAS_GOOD_MESSAGE_NON_ACK,
  RAS_GOOD_MESSAGE_WRONG_RECEIVER,
  RAS_BAD_MESSAGE,
  RAS_NOTHING_TO_DO,
  RAS_CORRECT_RECEIVER_NO_PROGRAMMING
} ReceivedAckStatus;

typedef enum
{
  TMS_NO_MESSAGE_SETUP,
  TMS_MESSAGE_SETUP,
  TMS_NO_MESSAGE_NEEDED,
  TMS_STILL_WAITING
} TransmitterMessageStatus;

typedef enum
{
  TMT_SERIAL_MESSAGE,
  TMT_ZONE_MESSAGE,
  TMT_CHECKIN_MESSAGE,
  TMT_NO_MESSAGE
} TransmitterMessageType;

typedef struct
{
  BITFIELD  counter           :4;
  BITFIELD  panicAckWaiting   :1;
  BITFIELD                    :1;
  BITFIELD                    :1;
  BITFIELD                    :1;
} AckStatus;


#define SERIAL_NUMBER_ADDRESS 0x1000
#define serialNumberPtr (const long*)(SERIAL_NUMBER_ADDRESS)
#define serialNumber *serialNumberPtr

/*----data descriptions-------------------------------------------------------*/

/*----data declarations-------------------------------------------------------*/
extern AckStatus ackStatus;
/*----macros------------------------------------------------------------------*/

/*----function prototypes-----------------------------------------------------*/
extern ReceivedAckStatus processReceivedAck(char* characterOffset);
extern short getSleepTicks(void);
extern TransmitterMessageStatus setupTransmitterMessage(char startOffset);
extern void resetLearnedInformation(void);
#ifdef DMP_1100_PRODUCTION_TEST
void setzonenumber(char);
#endif

#endif                                  /* end of file */
