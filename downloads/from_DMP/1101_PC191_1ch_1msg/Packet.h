/******************************************************************************
FILENAME: Packet.h

DESCRIPTION: Info relating to the data packet transferred from transmitter to
  receiver

NOTES:

Copyright (c) Digital Monitoring Products Inc. 2008.  All rights reserved.

******************************************************************************/
#ifndef  PACKET_H
#define  PACKET_H

/*----compilation control-----------------------------------------------------*/

/*******************************************************************************
INCLUDE FILES   Note: Minimize nesting of header files.
*******************************************************************************/
/*----system and platform files-----------------------------------------------*/

/*----program files-----------------------------------------------------------*/
#include "types.h"

/*******************************************************************************
FILE CONTEXT
*******************************************************************************/
/*----defines, typedefs, structs, unions, enums-------------------------------*/
// zone message byte
typedef enum
{
  MB_ALARM_0      = 0x01,
  MB_ALARM_1      = 0x02,
  MB_ALARM_2      = 0x04,
  MB_ALARM_3      = 0x08,
  MB_EXTENDED     = 0x10,
  MB_LOW_BATTERY  = 0x20,
  MB_TAMPER       = 0x40,
  MB_CHECKIN      = 0x80
} ZoneMessageByte;

typedef enum
{
  OS_OFF,
  OS_PULSE,
  OS_STEADY,
  OS_MOMENTARY,
  OS_TEMPORAL,
  OS_WINK,
  OS_PANIC_ALARM,
  OS_PANIC_TEST
} OutputMessageByte;

typedef struct
{
  ZoneMessageByte deviceState;
  union
  {
    struct
    {
      BITFIELD  zoneFour  : 2;
      BITFIELD  zoneThree : 2;
      BITFIELD  zoneTwo   : 2;
      BITFIELD  zoneOne   : 2;
    };
    unsigned char zoneStateAsByte;
  };
} ExtendedZoneMessageByte;

enum
{
  ZONE_STATE_NORMAL = 0,
  ZONE_STATE_SHORT,
  ZONE_STATE_OPEN,
  ZONE_STATE_FAULT
};

enum {INITIAL_CRC = 0xAAAA};

#ifdef DMP_BUILDING
enum
{
  DIAG_SYNC_BITS = 10,
  DIAG_TRANSMIT_BITS = 5,
  DIAG_SEQUENCE_BITS = 1,
  DIAG_MAX_SYNCS = (1 << DIAG_SYNC_BITS) - 1,
  DIAG_MAX_TRANSMITS = (1 << DIAG_TRANSMIT_BITS) - 1,
  DIAG_MAX_SEQUENCE = (1 << DIAG_SEQUENCE_BITS) - 1
};

typedef struct
{
  unsigned short  numSyncs        : DIAG_SYNC_BITS;   // number of sync attempts before received ack
  unsigned short  numTransmits    : DIAG_TRANSMIT_BITS;    // number of transmit attempts before received ack
  unsigned short  sequence        : DIAG_SEQUENCE_BITS;    // sequence number, increments every time ack received
} TransmitterDiagnostics;
#endif

#pragma pack(1) // do not allow padding in these structures

//----------------------------------------------------------------------------
// Receiver Acknowledge
//----------------------------------------------------------------------------
typedef struct
{
  BITFIELD        houseCode       : 6;
  BITFIELD        ackType         : 2;
} ReceiverAck;

typedef ReceiverAck ReceiverNoAck;

typedef enum
{
  AT_NO_ACK,
  AT_STANDARD_ACK,
  AT_ACK_WITH_ZONE_NUMBER,
  AT_VARIABLE_LENGTH_ACK
} AckType;

typedef enum
{
  VLAT_NO_ACK = 0,
  VLAT_OUTPUT_ACK,
  VLAT_TRANSMITTER_PROGRAMMING,
  VLAT_NON_RESERVED_ACK = 0xFF
} VariableLengthAckType;

typedef struct
{
  BITFIELD        houseCode       : 6;
  BITFIELD        ackType         : 2;
  unsigned short  crc;
} ReceiverStandardAck;

typedef struct
{
  BITFIELD        houseCode       : 6;
  BITFIELD        ackType         : 2;
  BITFIELD        zoneHigh        : 2;
  BITFIELD        found           : 1;
  BITFIELD        zoneFlags       : 5;
  char            zoneLow;
  unsigned short  crc;
} ReceiverZoneNumberAck;

typedef struct
{
  BITFIELD        houseCode       : 6;
  BITFIELD        ackType         : 2;
  BITFIELD        numBytesInData  : 3;
  BITFIELD        ackTypeExtended : 5;
  char            data[1];            // 0 - 7 bytes of data go here, depending upon numBytes
  unsigned short  crc;
} ReceiverVariableLengthAck;

typedef struct
{
  BITFIELD        houseCode       : 6;
  BITFIELD        ackType         : 2;
  BITFIELD        numBytesInData  : 3;
  BITFIELD        ackTypeExtended : 5;
  OutputMessageByte outputMessage;
  unsigned short crc;
} ReceiverOutputExtendedAck;

typedef struct
{
  BITFIELD      houseCode         : 6;
  BITFIELD      ackType           : 2;
  BITFIELD      numBytesInData    : 3;
  BITFIELD      ackTypeExtended   : 5;
  unsigned char serialNumLow;
  unsigned char serialNumMid;
  unsigned char serialNumHigh;
  BITFIELD      missingTimeUnits  : 1;
  BITFIELD      missingWindow     : 7;
  BITFIELD      zoneNumHigh       : 2;
  BITFIELD      zoneFlags         : 5;
  BITFIELD      reserved          : 1;
  unsigned char zoneNumLow;
  OutputMessageByte outputMessage;
  unsigned short crc;
} RepeaterProgrammingExtendedAck;

typedef union
{
  struct
  {
    BITFIELD                          :4;
    BITFIELD  sensitivity             :1;
    BITFIELD  contactOneNormallyOpen  :1;
    BITFIELD  led                     :1;
    BITFIELD  pulseCount              :1;
  };
  struct
  {
    BITFIELD                          :3;
    BITFIELD  transmitterFlags        :5;
  };
  char asByte;
} ZoneFlags;

//----------------------------------------------------------------------------
// Receiver Message Slot
//----------------------------------------------------------------------------
typedef enum
{
  ST_GENERAL_ALARM,               // warning! when changing this table, make sure the defines below still work
  ST_CHECKIN_NO_REPLY_ALARM,
  ST_CHECKIN_NO_REPLY_SHUTDOWN_ALARM,
  ST_STAY_AWAKE_ALARM,
  ST_RESERVED4_ALARM,
  ST_RESERVED5_ALARM,
  ST_RESERVED6_ALARM,
  ST_RESERVED7_ALARM,
  ST_RESERVED8_ALARM,
  ST_RESERVED9_ALARM,
  ST_RESERVED10_ALARM,
  ST_RESERVED11_ALARM,
  ST_RESERVED12_ALARM,
  ST_RESERVED13_ALARM,
  ST_RESERVED14_ALARM,
  ST_RESERVED15_ALARM,
  ST_CHECKIN_NEED_REPLY,
  ST_CHECKIN_UPDATE_PROGRAMMING,   //Will be sent to all transmitters after programming has changed to trigger a programming update
  ST_CHECKIN_NEED_REPLY_SHUTDOWN,
  ST_RESERVED19,
  ST_RESERVED20,
  ST_RESERVED21,
  ST_RESERVED22,
  ST_RESERVED23,
  ST_KEPYPAD_POLL,
  ST_GENERAL_DATA,
  ST_REPEATER_DATA,
  ST_RESERVED27,
  ST_RESERVED28,
  ST_RESERVED29,
  ST_RESERVED30,
  ST_RESERVED31,
  ST_INVALID,
  ST_FIRST_ALARM_NO_MESSAGE = ST_GENERAL_ALARM,
  ST_LAST_ALARM_NO_MESSAGE = ST_RESERVED11_ALARM,
  ST_FIRST_ALARM_TWO_BYTE_MESSAGE = ST_RESERVED12_ALARM,
  ST_LAST_ALARM_TWO_BYTE_MESSAGE = ST_RESERVED15_ALARM,
  ST_FIRST_ALARM = ST_FIRST_ALARM_NO_MESSAGE,
  ST_LAST_ALARM = ST_LAST_ALARM_TWO_BYTE_MESSAGE,
  ST_FIRST_NEED_REPLY = ST_CHECKIN_NEED_REPLY,
  ST_LAST_NEED_REPLY = ST_RESERVED23

} SlotType;

// NOTE: these assume that first alarm is first message type
#define isAlarmSlotType(TYPE) ((TYPE) <= ST_LAST_ALARM)  //<=slot15
#define isNoMessageSlotType(TYPE) (((TYPE) <= ST_LAST_ALARM_NO_MESSAGE) )
#define isNeedReplySlotType(TYPE) ( ((TYPE) <= ST_LAST_NEED_REPLY) && ((TYPE) >=ST_FIRST_NEED_REPLY) )//  <= slottype23 && >=slottype16
#define isTwoByteAlarmSlotType(TYPE) ((TYPE) >= ST_FIRST_ALARM_TWO_BYTE_MESSAGE) && ((TYPE) <= ST_LAST_ALARM_TWO_BYTE_MESSAGE)  // >= slottype12 && <= slottype 15
#define isGeneralDataSlotType(TYPE) ((TYPE == ST_GENERAL_DATA))
#define isGeneralDataSlotNumber(SLOT) ( ((SLOT) - STARTING_DATA_SLOT) % DATA_SLOT_PERIOD == 0 )

typedef struct
{
  BITFIELD        slotNumberHigh  : 3;
  BITFIELD        slotType        : 5;
  char            slotNumberLow;
} ReceiverSlotStart;

typedef struct
{
  BITFIELD        slotNumberHigh  : 3;
  BITFIELD        slotType        : 5;
  char            slotNumberLow;
  unsigned short  crc;
} ReceiverNoMessageSlot;

//----------------------------------------------------------------------------
// Receiver Message Commands/General Data Message Formats
//----------------------------------------------------------------------------
typedef enum
{
  KEYFOB_FEEDBACK_COMMAND = 1,
  OUTPUT_STATE_COMMAND,
  REPEATER_DATA_COMMAND,  // todo: this is never used and can be reused.  Do not, however, remove or the following commands with change.
  ZONE_DISABLE_COMMAND,
  SYSTEM_STATUS_COMMAND
} ReceiverMessageCommandType;

/*******************NOTE***********************
* The bit order of KeyfobFeedbackCommandStruct is critical.
* It must match the bit order of KeyfobFeedbackRequestMessage
* in XBusMessageSupport.c
**********************************************/
typedef union
{
  struct
  {
    BITFIELD  reserved    : 2;
    BITFIELD  flashLength : 2;
    BITFIELD  flashCount  : 2;
    BITFIELD  greenLED    : 1;
    BITFIELD  redLED      : 1;
  };
  unsigned char asByte;
} KeyfobFeedbackCommandStruct;

//This structure must equal a byte or less inorder to fit into the general data slot
typedef union
{
  struct
  {
    BITFIELD checkinTestMode  : 1;
    BITFIELD spare            : 7;
  };
  unsigned char dataAsByte;
} SystemStatusStruct;

typedef struct
{
  BITFIELD        slotNumberHigh      : 3;
  BITFIELD        slotType            : 5;
  char            slotNumberLow;
  BITFIELD        dataMessageFormat   : 6;
  BITFIELD        zoneNumberHighBits  : 2;
  unsigned char   zoneNumberLowByte;
  unsigned char   command;
  unsigned short  crc;
} ReceiverCommandSlot;

typedef enum
{
  RDT_KEYFOB_FEEDBACK = 1,
  RDT_OUTPUT_UPDATE,
  RDT_ZONE_DISABLE,
  RDT_SYSTEM_STATUS
} RepeaterDataType;

typedef struct
{
  BITFIELD        slotNumberHigh          : 3;
  BITFIELD        slotType                : 5;
  char            slotNumberLow;
  BITFIELD        dataMessageFormat       : 4;
  BITFIELD        repeaterNumberHighBits  : 2;
  BITFIELD        zoneNumberHighBits      : 2;
  unsigned char   repeaterNumberLowByte;
  unsigned char   zoneNumberLowByte;
  unsigned char   data;
  unsigned short  crc;
} RepeaterDataSlot;

//Packet From Receiver
#define  PR_SIZE  32
#define  PR_PREAMBLE_SIZE  4
#define  PR_PATTERN_SIZE  3
#define  PR_HEADER_SIZE  (PR_PREAMBLE_SIZE + PR_PATTERN_SIZE)

#ifdef DMP_1100_PRODUCTION_TEST
enum
{
  DIAG_SYNC_BITS = 10,
  DIAG_TRANSMIT_BITS = 5,
  DIAG_SEQUENCE_BITS = 1,
  DIAG_MAX_SYNCS = (1 << DIAG_SYNC_BITS) - 1,
  DIAG_MAX_TRANSMITS = (1 << DIAG_TRANSMIT_BITS) - 1,
  DIAG_MAX_SEQUENCE = (1 << DIAG_SEQUENCE_BITS) - 1
};

typedef struct
{
  unsigned short  numSyncs        : DIAG_SYNC_BITS;   // number of sync attempts before received ack
  unsigned short  numTransmits    : DIAG_TRANSMIT_BITS;    // number of transmit attempts before received ack
  unsigned short  sequence        : DIAG_SEQUENCE_BITS;    // sequence number, increments every time ack received
} TransmitterDiagnostics;
#endif

// TransmitterAlarmType must stay under 32 inorder to fit into the 5 bit
// message types listed below.
typedef enum
{
  TAT_ALARM_WITH_ZONE_NUMBER,
  TAT_ALARM_WITH_SERIAL_NUMBER,
  TAT_ALARM_WITH_EXTENDED_ZONE_NUMBER,
  TAT_ALARM_WITH_EXTENDED_SERIAL_NUMBER,
  TAT_PROGRAMMING_REQUEST_MESSAGE,
  TAT_FORWARD_STANDARD_ZONE_MESSAGE,
  TAT_FORWARD_EXTENDED_ZONE_MESSAGE
} TransmitterAlarmType;

// TransmitterDeviceType must stay under 3 inorder to fit into the 2 bit transmitterType
// field in the serial number messages below.
typedef enum
{
  TDT_UNIVERSAL_DEVICE = 0,
  TDT_REPEATER_DEVICE
} TransmitterDeviceType;

typedef struct
{
  BITFIELD        messageBits     : 3;
  BITFIELD        messageType     : 5;
} TransmitterAlarmMessageHeader;

typedef struct
{
  BITFIELD        zoneNumberHigh  : 3;
  BITFIELD        messageType     : 5;
  char            zoneNumberLow;
  ZoneMessageByte zoneMessage;
#ifdef DMP_BUILDING
  TransmitterDiagnostics diag;
#endif
  unsigned short  crc;
} TransmitterAlarmZoneNumberZoneMessage;

typedef struct
{
  BITFIELD                  zoneNumberHigh  : 3;
  BITFIELD                  messageType     : 5;
  char                      zoneNumberLow;
  ExtendedZoneMessageByte   zoneMessage;
  unsigned short            crc;
} TransmitterAlarmExtendedZoneMessage;

typedef struct
{
  BITFIELD        reserved        : 2;
  BITFIELD        transmitterType : 1;
  BITFIELD        messageType     : 5;
  char            serialNumberLow;
  char            serialNumberMiddle;
  char            serialNumberHigh;
  char            softwareVersion;
  ZoneMessageByte zoneMessage;
#ifdef DMP_BUILDING
  TransmitterDiagnostics diag;
#endif
  unsigned short  crc;
} TransmitterAlarmSerialNumberZoneMessage;

typedef struct
{
  BITFIELD        reserved        : 1;
  BITFIELD        transmitterType : 2;
  BITFIELD        messageType     : 5;
  char            serialNumberLow;
  char            serialNumberMiddle;
  char            serialNumberHigh;
  char            softwareVersion;
  ExtendedZoneMessageByte   zoneMessage;
  unsigned short  crc;
} TransmitterAlarmSerialNumExtendedMsg;

typedef struct
{
  ZoneMessageByte zoneMessage;
#ifdef DMP_BUILDING
  TransmitterDiagnostics diag;
#endif
  unsigned short  crc;
} TransmitterCheckinMessage;

typedef struct
{
  BITFIELD zoneNumberHigh : 3;
  BITFIELD reserved       : 5;
  char            zoneNumberLow;
  unsigned short  crc;
} CommandDataAckMessage;

typedef struct
{
  BITFIELD zoneNumberHigh : 3;
  BITFIELD messageType    : 5;
  char     zoneNumberLow;
  unsigned short crc;
} ProgrammingRequestMessage;

typedef struct
{
  BITFIELD zoneNumberHigh : 3;
  BITFIELD messageType    : 5;
  unsigned char zoneNumberLow;
  ZoneMessageByte zoneMessage;
  unsigned short crc;
} ForwardStandardZoneMessage;

typedef struct
{
  BITFIELD zoneNumberHigh : 3;
  BITFIELD messageType    : 5;
  unsigned char zoneNumberLow;
  ExtendedZoneMessageByte zoneMessage;
  unsigned short crc;
} ForwardExtendedZoneMessage;

enum
{
  ACI_NOTHING = 0,
  ACI_HOUSE_CODE =    0x01,
  ACI_ZONE_NUMBER =   0x02,
  ACI_SERIAL_NUMBER = 0x04
};
typedef char AdditionalCheckItems;

#pragma pack()


// PacketFromTransmitter
#define  PT_SIZE          32
#define  PT_PREAMBLE_SIZE 4
#define  PT_PATTERN_SIZE  3
#define  PT_HEADER_SIZE   7

// PatternFromTransmitter
#define  TPAT_HIGH    0x89
#define  TPAT_MIDDLE  0xFC
#define  TPAT_LOW     0x45  // always end with a 0 to 1 transition


// PatternFromReceiver
#define  RPAT_HIGH    0x82
#define  RPAT_MIDDLE  0x46
#define  RPAT_LOW     0xA5  // always end with a 0 to 1 transition

#define RECEIVER_PATTERN (((unsigned long)RPAT_HIGH<<16) | (RPAT_MIDDLE<<8) | RPAT_LOW)
#define TRANSMITTER_PATTERN (((unsigned long)TPAT_HIGH<<16) | ((unsigned long)TPAT_MIDDLE<<8) | TPAT_LOW)

/*----data descriptions-------------------------------------------------------*/

/*----data declarations-------------------------------------------------------*/
extern const char receiverHeaderBuffer[PR_HEADER_SIZE];
extern const char transmitterHeaderBuffer[PT_HEADER_SIZE];
extern char receiverPacketBuffer[PR_SIZE];
extern char transmitterPacketBuffer[PT_SIZE];
extern char receiverPacketSize;
extern char transmitterPacketSize;

#ifdef DMP_BUILDING
extern TransmitterDiagnostics diag;
#endif

#ifdef DMP_1100_PRODUCTION_TEST
extern ZoneMessageByte diag;
#endif

#ifdef DMP_PRODUCTION_TEST
extern char diag;
#endif


/*----macros------------------------------------------------------------------*/

/*----function prototypes-----------------------------------------------------*/
extern void calcCRC16ForChar(char value, unsigned short* crc);
extern void encodeWirelessBuffer(char *Buffer, byte Length, unsigned short crc);
extern bool decodeWirelessBuffer(char *Buffer, byte Length, unsigned short crc);

#endif                                  /* end of file */
