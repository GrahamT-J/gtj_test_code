http://www.cs.kent.edu/~ruttan/sysprog/lectures/multi-thread/multi-thread.html
Please refer to the source code for the full details. Reading the header files first will make it easier to understand the design. To compile the program, just switch to the thread-pool-server-changes directory, and type 'gmake'.
