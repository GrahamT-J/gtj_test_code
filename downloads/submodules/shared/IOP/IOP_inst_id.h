/*
 * IOP_inst_id.h
 *
 *  Created by Peter Carlson on Oct 2, 2013
 *  Copyright (c) 2013 Cargt, LLC. All rights reserved.
 */

#ifndef IOP_INST_ID_H_
#define IOP_INST_ID_H_

#include "CARGT.h"

/* Shared Definitions */
enum
    {
    IOP_INST_ID_START           = 0,

    IOP_ESN_DATA                = 0,
    IOP_PROD_RQST               = 1,
    IOP_PROD_RQST_DATA          = 2,
    IOP_BUILD_INFO_RQST         = 3,
    IOP_BUILD_INFO_RQST_DATA    = 4,
    IOP_PROD_ID_RQST            = 5,
    IOP_PROD_ID_RQST_DATA       = 6,

    IOP_CMND_ID                 = 10,

    IOP_SET_RTC                 = 11,
    IOP_RTC_DATA                = 12,
    IOP_ERROR_DATA              = 13,

    IOP_MSG_SLOT              	= 14,
    IOP_MSG_SLOT_RSP            = 15,

    /*
     * Skip
     * IOP_DLE_CHAR             = 16 = 0x10
     */

	IOP_ATE_BRD_CONFIG			= 17,
	IOP_ATE_BRD_CONFIG_RSP		= 18,

    /*----------------------------------------------
    ADC / DMM IOP instrument IDs
    ----------------------------------------------*/
	IOP_ADC_READ				= 30,
	IOP_ADC_READ_RSP			= 31,
	IOP_ADC_VRIPPLE_READ		= 32,
	IOP_ADC_VRIPPLE_READ_RSP	= 33,
	IOP_ADC_FREQ_CNTR_READ		= 34,
	IOP_ADC_FREQ_CNTR_READ_RSP	= 35,
	IOP_ADC_FFT_READ			= 36,
	IOP_ADC_FFT_READ_RSP		= 37,

    /*----------------------------------------------
    Display IOP instrument IDs
    ----------------------------------------------*/
	IOP_DSPL_SET_BKLT			= 40,
	IOP_DSPL_SET_BKLT_RSP		= 41,
	IOP_DSPL_GET_BKLT			= 42,
	IOP_DSPL_GET_BKLT_RSP		= 43,
	IOP_DSPL_SET_TST_PTRN		= 44,
	IOP_DSPL_SET_TST_PTRN_RSP	= 45,
	IOP_DSPL_TSC_TEST			= 46,
	IOP_DSPL_TSC_TEST_RSP		= 47,
	IOP_DSPL_TSC_TEST_DATA		= 48,

    /*----------------------------------------------
    Networking IOP instrument IDs
    ----------------------------------------------*/
	IOP_NTWK_IFCONFIG			= 50,
	IOP_NTWK_IFCONFIG_RSP		= 51,
	IOP_NTWK_PING				= 52,
	IOP_NTWK_PING_RSP			= 53,

    /*----------------------------------------------
    SD Card IOP instrument IDs
    ----------------------------------------------*/
	IOP_SDCARD_DETECT			= 80,
	IOP_SDCARD_DETECT_RSP		= 81,
	IOP_SDCARD_RW_TEST			= 82,
	IOP_SDCARD_RW_TEST_RSP		= 83,

    /*----------------------------------------------
    UART IOP instrument IDs
    ----------------------------------------------*/
	IOP_UART_CONFIG				= 90,
	IOP_UART_CONFIG_RSP			= 91,
	IOP_UART_LOOPBACK_TEST		= 92,
	IOP_UART_LOOPBACK_TEST_RSP	= 93,

    /*----------------------------------------------
    Audio IOP instrument IDs
    ----------------------------------------------*/
	IOP_AUDIO_PLAY_TONE			= 100,
	IOP_AUDIO_PLAY_TONE_RSP		= 101,

    /*----------------------------------------------
    Bluetooth IOP instrument IDs
    ----------------------------------------------*/
    IOP_BT_LAP_DATA             = 200,
    IOP_BT_SET_CARRIER_FREQ     = 201,
    IOP_BT_START_CNCT           = 202,
    IOP_BT_STOP_CNCT            = 203,

    /*----------------------------------------------
    GPIO IOP instrument IDs
    ----------------------------------------------*/
	IOP_GPIO_READ_PIN			= 300,
	IOP_GPIO_READ_PIN_RSP		= 301,
	IOP_GPIO_SET_PIN			= 302,
	IOP_GPIO_SET_PIN_RSP		= 303,
	IOP_GPIO_CLEAR_PIN			= 304,
	IOP_GPIO_CLEAR_PIN_RSP		= 305,
	IOP_GPIO_TOGGLE_PIN         = 306,
	IOP_GPIO_TOGGLE_PIN_RSP     = 307,
	IOP_GPIO_SET_ATTRIBUTES_PIN = 308,
	IOP_GPIO_SET_ATTRIBUTES_RSP = 309,
	IOP_GPIO_GET_INTERRUPT_PIN  = 310,
	IOP_GPIO_GET_INTERRUPT_RSP  = 311,

    /*----------------------------------------------
    STATUS IOP instrument IDs
    ----------------------------------------------*/
	IOP_DUT_STATUS				= 400,
	IOP_LID_STATUS				= 401,

	/*----------------------------------------------
    Flash Programming IOP instrument IDs
    ----------------------------------------------*/
    IOP_PROGRAM_EZPORT_ERASE_BULK   = 500,
    IOP_PROGRAM_EZPORT_PROGRAM      = 501,
	IOP_PROGRAM_EZPORT_PERCENT      = 502,
    IOP_PROGRAM_EZPORT_RSP          = 503,
    IOP_PROGRAM_EZPORT_SETTINGS     = 504,

    IOP_PROGRAM_JTAG_ERASE_BULK     = 525,
    IOP_PROGRAM_JTAG_PROGRAM        = 526,
	IOP_PROGRAM_JTAG_COMPLETE		= 527,
    IOP_PROGRAM_JTAG_RSP            = 528,
    IOP_PROGRAM_JTAG_SETTINGS       = 529,

    IOP_PROGRAM_SWD_ERASE_BULK      = 550,
    IOP_PROGRAM_SWD_PROGRAM         = 551,
	IOP_PROGRAM_SWD_COMPLETE        = 552,
    IOP_PROGRAM_SWD_RSP             = 553,
    IOP_PROGRAM_SWD_SETTINGS        = 554,

	/*----------------------------------------------
    FAT FS ID's
    ----------------------------------------------*/
    IOP_FS_RSP      				= 600,
	IOP_FS_F_OPEN_WRITE				= 601,
	IOP_FS_F_CLOSE  				= 602,
	IOP_FS_F_WRITE  				= 603,

	/*----------------------------------------------
    Peterson Manufacturing ID's
    ----------------------------------------------*/
    IOP_PULSE_CNTRL_STATUS      	= 800,
	IOP_PULSE_CNTRL_STATUS_RSP		= 801,
	IOP_PULSE_CHNL_STATUS			= 802,
	IOP_PULSE_CHNL_STATUS_RPS		= 803,

    /*----------------------------------------------
    Customer-specific definitions
    ----------------------------------------------*/
    #ifndef IOP_USE_PRJ
        /*----------------------------------------------
        If there are no project-specific settings then
        define the end of the acceptable range here.
        ----------------------------------------------*/
        IOP_INST_ID_LAST,       //Must be last
        IOP_INST_ID_CNT         = IOP_INST_ID_LAST
    #endif
    };

#define IOP_PCBA_PART_NUM_SIZE			( 12 )
#define IOP_BRD_DESCRIPTION_MAX_LENGTH	( 30 )

#pragma pack(1)
typedef struct
	{
	char	pcba_part_num[IOP_PCBA_PART_NUM_SIZE];
	uint8	pcb_rev;
	uint16	sw_prod_num;
	uint32	sw_ver_num;
	char	brd_dscpt[IOP_BRD_DESCRIPTION_MAX_LENGTH];
	} IOP_prod_rqst_data_type;
#pragma pack()

#define IOP_ATE_DHTRBRD_NUM		( 5 )

#pragma pack(1)
typedef struct
	{
	uint8	dhtr_brd_flag;
	char	pogo_brd_pcba[IOP_PCBA_PART_NUM_SIZE];
	char	dhtr_brd_pcba[IOP_ATE_DHTRBRD_NUM][IOP_PCBA_PART_NUM_SIZE];
	} IOP_ate_brd_config_type;
#pragma pack()

#pragma pack(1)
typedef struct
	{
	uint8	src;
	uint8	dest;
	uint16	id;
	uint16	rsp_id;
	uint32	size;
    uint8   data[IOP_BUF_SIZE];
	} IOP_slot_msg_hdr_type;
#pragma pack()

#pragma pack(1)
typedef struct
	{
	uint8	src;
	uint8	dest;
	uint16	id;
	uint32	size;
    uint8   data[IOP_BUF_SIZE];
	} IOP_slot_msg_rsp_hdr_type;
#pragma pack()

#define IOP_MSG_SLOT_RAW		( 0 )
#define IOP_MSG_SLOT_IOP		( 1 )

typedef uint8 IOP_uart_idx_type; enum
	{
	IOP_UART_DUT	= 0,
	IOP_UART_J1		= 1,
	IOP_UART_J2		= 2,
	IOP_UART_J3		= 3,
	IOP_UART_J4		= 4,
	IOP_UART_J5		= 5,

	IOP_UART_CNT                   /* # of IOP UART Connections */
	};

//IOP Slot Message callback function pointer
typedef void (* IOP_msg_slot_clbk_func_type)( uint8 src, uint16 id, uint8 const *  data, uint32 size );

#pragma pack(1)
typedef struct
	{
	uint16  					id;
	IOP_msg_slot_clbk_func_type	clbk;
	} IOP_msg_slot_clbk_type;
#pragma pack()

#pragma pack(1)
typedef struct
	{
	uint8					src;
	uint8					dest;
	uint8					msg_type;
	IOP_msg_slot_clbk_type	rx_cblk;

	union
		{
		struct
			{
			uint8	payload[256];
			} raw;

		struct
			{
			uint16  id;
			uint32  length;
			uint8	payload[256];
			} iop;

		} type;

	} IOP_msg_slot_type;
#pragma pack()

#pragma pack(1)
typedef struct
	{
	uint8	chnl;
	uint32	num_samples;
	uint32	sample_rate;
	} IOP_adc_read_type;
#pragma pack()

#pragma pack(1)
typedef struct
	{
	uint8	status;
	int32	results;
	} IOP_adc_read_rsp_type;
#pragma pack()

#endif /* IOP_INST_ID_H_ */
