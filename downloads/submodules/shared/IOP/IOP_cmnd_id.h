/*
 * IOP_cmnd_id.h
 *
 *  Created by Peter Carlson on Oct 2, 2013
 *  Copyright (c) 2013 Cargt, LLC. All rights reserved.
 */

#ifndef IOP_CMND_ID_H_
#define IOP_CMND_ID_H_

#include "CARGT.h"

//IOP Command IDs
typedef uint16 iop_cmnd_id_type; enum
    {
    IOP_DOWNLOAD_ESN                = 0,    //Download Electronic Serial Number (ESN)
    IOP_GET_RTC                     = 1,

    IOP_DUT_PWR_ON                  = 2,
    IOP_DUT_PWR_OFF                 = 3,

	//Start of next command ID group.
    IOP_START_OF_NEXT_CMND          = 2,
    IOP_CMND_ID_LAST
    };

#endif /* IOP_CMND_ID_H_ */
