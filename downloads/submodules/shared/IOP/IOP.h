/*
 * IOP.h
 *
 *  Created by Peter Carlson on Oct 2, 2013
 *  Copyright (c) 2013 Cargt, LLC. All rights reserved.
 */

#ifndef IOP_H_
#define IOP_H_

#include "CARGT.h"
#include "UTL_circ_buf.h"

#define IOP_DLE_BYTE                    ( 0x10 )
#define IOP_ETX_BYTE                    ( 0x03 )

//#ifndef IOP_BUF_SIZE
#define IOP_BUF_SIZE		( 512 )

//IOP TX function pointer
typedef void (* IOP_tx_func_type)( UTL_circ_buf_t * circ_buf, uint32 size );

//IOP RX message handler function pointer
typedef void (* IOP_rx_msg_func_type)( void * ctrl, uint16 id, uint8 const * data, uint32 size );

//MISC TYPE DEFINITIONS
typedef uint8 iop_msg_descriptor_type; enum    /* AI2 Header Message Descriptor Field */
    {
    IOP_MSG_DESCRIPTOR_NO_ACK_REQ = 0x00,      /* No ACK requested                 */
    IOP_MSG_DESCRIPTOR_ACK_REQ    = 0x01,      /* ACK requested                    */
    IOP_MSG_DESCRIPTOR_ACK_RESP   = 0x02,      /* ACK response                     */
    IOP_MSG_DESCRIPTOR_INVLD      = 0x03,      /* Invalid descriptor               */

    IOP_MSG_DESCRIPTOR_NMBR                    /* Number of Message Descriptors    */
    };

typedef uint8 IOP_rx_state_type; enum
    {
    IOP_GET_HEADER_SYNC = 0,        /* Header Start Byte                */
    IOP_GET_HEADER_ACK,             /* Header ACK Byte                  */
    IOP_GET_DATA,                   /* Generic Data Byte                */
    IOP_GET_DATA_DLE,               /* Check for escaped DLE byte       */
    IOP_STATE_NMBR                  /* Number of States                 */
    };


//IOP Command Message IDs
typedef uint16 iop_cmnd_msg_type; enum
    {
	IOP_CMND_MSG_START	= 0,

	IOP_CMND_MSG_LAST,			//Must be last
	IOP_CMDN_MSG_CNT = IOP_CMND_MSG_LAST
    };

//IOP Event IDs
typedef uint16 iop_evnt_id_type; enum
    {
	IOP_EVNT_ID_START	= 0,

	IOP_EVNT_ID_LAST,			//Must be last
	IOP_EVNT_ID_CNT 	= IOP_EVNT_ID_LAST
    };

//IOP routes
typedef uint32 iop_route_hndl_type; enum
	{
	IOP_ROUTE_RS232_HNDL 	= 0, // UART
	IOP_ROUTE_BT_HNDL		= 1, // SPP connection - TODO: Add support for multiple devices
    IOP_ROUTE_BLE_HNDL		= 2, // BLE connection - TODO: Add support for multiple devices

	IOP_ROUTE_HNDL_CNT
	};

typedef struct
	{
	iop_route_hndl_type		hndl;
	//boolean					buf_busy; //PRC - Is this needed??
	} IOP_route_info_type;

#pragma pack(1)
typedef struct
    {
    uint8                       header_dle;
    uint8     					descriptor;
    uint16        				id;
    uint32                      data_length;
    } iop_hdr_type;
#pragma pack()

#pragma pack(1)
typedef struct
    {
    uint16                      chksm;
    uint8                       tail_dle;
    uint8                       tail_etx;
    } iop_tail_type;
#pragma pack()

typedef struct
    {
    iop_hdr_type         		header;
    uint8                       data[IOP_BUF_SIZE];
    iop_tail_type        		tail;
    uint16                      calc_chksm;
    uint16                      total_length;
    uint8                       num_esc_bytes;
    } IOP_msg_type;

typedef struct
	{
	IOP_route_info_type	*	route;
	boolean					inited;
	IOP_rx_state_type       rx_state;
	UTL_circ_buf_t *		tx_cb;
	uint8 *					tx_buf;
	UTL_circ_buf_t *		rx_cb;
	uint8 *					rx_buf;
	IOP_tx_func_type		tx_func;
	void_func_type          reset_func;
	IOP_msg_type *     		iop_rx_msg;
	IOP_rx_msg_func_type 	rx_msg_hndlr;
	} IOP_ctrl_type;

//IOP Instrument IDs
typedef uint16 iop_inst_id_type;

#include "IOP_inst_id.h"
#ifdef IOP_USE_PRJ
    #include "IOP_inst_id_prj.h"
#endif
#include "IOP_cmnd_id.h"

// iop_parser.c
void IOP_init
    ( void );

void IOP_parse_rx_data
    (
    IOP_ctrl_type *				ctrl,
    const uint32        		num_bytes
    );

void IOP_pwrp
    ( void );

void IOP_register
	(
	IOP_ctrl_type *			ctrl,
	uint32					tx_buf_size,
	uint32					rx_buf_size
	);

void IOP_tx_msg
    (
    IOP_ctrl_type *				ctrl,
    iop_inst_id_type    		id,
    iop_msg_descriptor_type		dscptr,
    uint8 const *               data,
    uint32                      size
    );


// iop_msg_handler.c
void IOP_msg_hndlr_init
	( void );

void IOP_rgstr_msg_hnldr
	(
	IOP_ctrl_type *			ctrl,
	IOP_rx_msg_func_type 	msg_hndlr
	);

void IOP_rx_msg_hndlr
	(
	IOP_ctrl_type *			ctrl,
	uint16                  id,
	uint8 const *           data,
	uint32                  size
	);

void IOP_msg_hndlr_pwrp
	( void );

#endif /* IOP_H_ */
