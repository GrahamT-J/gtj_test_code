/*
 * iop_msg_handler.c
 *
 *  Created by Peter Carlson on Oct 2, 2013
 *  Copyright (c) 2013 Cargt, LLC. All rights reserved.
 */

#include "CARGT.h"
#include "IOP.h"

#include "assert.h"

static void dflt_rx_msg_hndlr
	(
	IOP_ctrl_type *			ctrl,
	uint16                  id,
	uint8 const *           data,
	uint32                  size
	);

static void proc_cmnd
	(
	IOP_ctrl_type *			ctrl,
	uint16	    			cmnd_id
	);

static void send_ack
	(
	IOP_ctrl_type *			ctrl,
	iop_inst_id_type    	id
	);

#pragma GCC diagnostic ignored "-Wunused-function"

/*
*   PROCEDURE NAME:
*       IOP_msg_hndlr_init - Initialize IOP message handler
*
*   DESCRIPTION:
*
*/
void IOP_msg_hndlr_init
	( void )
{

}	/* IOP_msg_hndlr_init() */


/*
*   PROCEDURE NAME:
*       IOP_rgstr_msg_hnldr - Register new IOP RX message handler
*
*   DESCRIPTION:
*
*/
void IOP_rgstr_msg_hnldr
	(
	IOP_ctrl_type *			ctrl,
	IOP_rx_msg_func_type 	msg_hndlr
	)
{
assert( ctrl != NULL );
assert( msg_hndlr != NULL );

ctrl->rx_msg_hndlr = msg_hndlr;

}	/* IOP_rgstr_msg_hnldr() */


/*
*   PROCEDURE NAME:
*       IOP_rx_msg_hndlr - IOP RX message handler
*
*   DESCRIPTION:
*
*/
void IOP_rx_msg_hndlr
	(
	IOP_ctrl_type *			ctrl,
	uint16                  id,
	uint8 const *           data,
	uint32                  size
	)
{
//Local Variables
uint8 *          payload;
IOP_msg_type *   iop_msg;

iop_msg = (IOP_msg_type *)data;

//Set the data pointer past the header
payload = iop_msg->data + sizeof( iop_hdr_type );

//Acknowledge message if requested
if( iop_msg->header.descriptor == IOP_MSG_DESCRIPTOR_ACK_REQ )
	{
	send_ack( ctrl, iop_msg->header.id );
	}

if( ctrl->rx_msg_hndlr != NULL && iop_msg->header.descriptor != IOP_MSG_DESCRIPTOR_ACK_RESP )
	{
	ctrl->rx_msg_hndlr( ctrl, id, payload, size );
	}

}	/* IOP_rx_msg_hndlr() */


/*
*   PROCEDURE NAME:
*       IOP_msg_hndlr_pwrp - Power-up IOP message handler
*
*   DESCRIPTION:
*
*/
void IOP_msg_hndlr_pwrp
	( void )
{
//rx_msg_hander = dflt_rx_msg_hndlr;
}	/* IOP_msg_hndlr_pwrp() */


static void dflt_rx_msg_hndlr
	(
	IOP_ctrl_type *			ctrl,
	uint16                   id,
	uint8 const *           data,
	uint32                  size
	)
{
//Process Instrument ID
switch( id )
	{
	case IOP_ESN_DATA:
		{
		//uint32	esn = (uint32) *( (uint32*)( data ) );

		//PRC - TODO: Add function to set ESN
		//btmgr_setEsn( &esn );
		}
		break;

	case IOP_CMND_ID:
		{
		uint16 cmnd = (uint16) *( (uint16*)( data ) );

		proc_cmnd( ctrl, cmnd );
		}
		break;

	default:
		break;
	}
}

/*
*   PROCEDURE NAME:
*       proc_cmnd - Process IOP command
*
*   DESCRIPTION:
*
*/
static void proc_cmnd
	(
	IOP_ctrl_type *			ctrl,
	uint16	    			cmnd_id
	)
{
switch( cmnd_id )
	{
	case IOP_DOWNLOAD_ESN:
		{
		uint32	esn = 0x12345678;

		//PRC - TODO: Add function to read
		//btmgr_getEsn( &esn );

		IOP_tx_msg( ctrl, IOP_ESN_DATA, IOP_MSG_DESCRIPTOR_NO_ACK_REQ, (uint8*)&esn, sizeof( esn ) );
		}
		break;

	default:
		break;
	}
}	/* proc_cmnd() */


/*
*   PROCEDURE NAME:
*       send_ack - Transmit ACK
*
*   DESCRIPTION:
*
*/
static void send_ack
	(
	IOP_ctrl_type *			ctrl,
	iop_inst_id_type    	id
	)
{
IOP_tx_msg( ctrl, id, IOP_MSG_DESCRIPTOR_ACK_RESP, NULL, 0 );
}	/* send_ack() */
