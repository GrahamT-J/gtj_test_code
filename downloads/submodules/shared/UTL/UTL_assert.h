/*
 * UTL_assert.h
 *
 *  Created by Peter Carlson on Nov 6, 2014
 *  Copyright (c) 2013 Cargt, LLC. All rights reserved.
 *
 */

#ifndef UTL_ASSERT_H_
#define UTL_ASSERT_H_

#ifdef USE_FULL_ASSERT
    #define UTL_assert( expr )  ( assert_param ( expr ) )
#else
    //NDEBUG define is used to enable/disable this assert
    //If NDEBUG is defined, this assert is ignored
    #include "assert.h"
    #define UTL_assert( expr )  ( assert( expr ) )
#endif


#endif /* UTL_ASSERT_H_ */
