/*
 * UTL_crc.h
 *
 *  Created by Peter Carlson on Feb 4, 2013
 *  Copyright (c) 2013 Cargt, LLC. All rights reserved.
 *
 */

#ifndef UTL_CRC_H_
#define UTL_CRC_H_

uint16 UTL_calc_crc16
    (
    uint8   const * const data,
    uint32          const size,
    uint16          const initial_value
    );

uint32 UTL_calc_crc32
    (
    uint8   const * const data,
    uint32          const size,
    uint32          const initial_value
    );

#endif /* UTL_CRC_H_ */
