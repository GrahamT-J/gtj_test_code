/*
 * utl_assert.c
 *
 *  Created by Peter Carlson on Nov 6, 2014
 *  Copyright (c) 2013 Cargt, LLC. All rights reserved.
 *
 */

#include "CARGT.h"

#if defined(__ICCARM__)
	#include "cmsis_os.h"
#endif

#ifdef CARGT_USE_FULL_ASSERT

/**
   * @brief Reports the name of the source file and the source line number
   * where the assert_param error has occurred.
   * @param file: pointer to the source file name
   * @param line: assert_param error line source number
   * @retval None
   */
void assert_failed
  (
  uint8 *       file,
  uint32        line
  )
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
    HAL_NVIC_SystemReset();

  /* USER CODE END 6 */

}

#endif
