/*
 * UTL_chksm.h
 *
 *  Created by Peter Carlson on Feb 4, 2013
 *  Copyright (c) 2013 Cargt, LLC. All rights reserved.
 *
 */

#ifndef UTL_CHKSM_H_
#define UTL_CHKSM_H_

uint8 UTL_calc_chksum8
    (
    uint8   const * const data,
    uint32          const size
    );

uint16 UTL_calc_chksum16
    (
    uint16  const * const data,
    uint32          const size
    );

uint32 UTL_calc_chksum32
    (
    uint32  const * const data,
    uint32          const size
    );

#endif /* UTL_CHKSM_H_ */
