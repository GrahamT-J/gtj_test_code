/*
 * UTL_circ_buf.h
 *
 *  Created by Peter Carlson on Feb 4, 2013
 *  Copyright (c) 2013 Cargt, LLC. All rights reserved.
 *
 */

#ifndef UTL_CIRC_BUF_H_
#define UTL_CIRC_BUF_H_

#include "CARGT.h"

typedef struct
    {
    uint32                data_size;
    uint32                read_idx;
    uint32                write_idx;
    uint8 *               mem;
    boolean               full;
    } UTL_circ_buf_t;

uint32 UTL_circ_buf_free_space
    (
    UTL_circ_buf_t * cb
    );

void UTL_circ_buf_init
    (
    UTL_circ_buf_t * 	cb,
    uint8 *				buf,
    uint32 				size
    );

uint32 UTL_circ_buf_read
    (
    UTL_circ_buf_t * 	cb,
    uint8 * 			buf,
    uint32 				buf_size
    );

void UTL_circ_buf_reset
    (
    UTL_circ_buf_t *     cb
    );

uint32 UTL_circ_buf_write
    (
    UTL_circ_buf_t * 	cb,
    uint8 * 			buf,
    uint32 				buf_size
    );

uint32 UTL_circ_buf_used_space
    (
    UTL_circ_buf_t * cb
    );

#endif /* UTL_CIRC_BUF_H_ */
