/*
 * Cargt.h
 *
 *  Created by Peter Carlson on September 26, 2013
 *  Copyright (c) 2013 Cargt, LLC. All rights reserved.
 *
 */

#ifndef CARGT_H_
#define CARGT_H_

#include "CARGT_types.h"

/*
 * Common #defines
 */
#define	_KB		( 1024 )
#define _MB		( 1024 * 1024 )

#define setbit(_x_)	(1<<_x_)
#define clrbit(_x_) (~(1<<_x_))

#define		maxval(x,y)		( (x) > (y) ? (x) : (y) )
#define		minval(x,y)		( (x) < (y) ? (x) : (y) )

/*----------------------------------------------------------
Concatenate two tokens into a single C token

Use tokcat() and not _tokcat().  _tokcat() only exists to
allow tokcat() to get around preprocessor substitution
rules.  tokcat() returns a single C token that is the
result of concatenating two tokens.  For example, if
"tokcat( _, __LINE__)" appears at line 123, it will be
replaced with "_123" by the preprocessor.  tokcat() is
analogous to strcat().
----------------------------------------------------------*/
#define _tokcat( x, y) x ## y
#define tokcat( x, y) _tokcat( x, y)

#define _compiler_assert( e, m) \
	struct tokcat( __, tokcat( m, __LINE__)) \
		{ unsigned long int _ : ( (e) ? 1 : 0);}

#define cnt_of_array( _a )		( sizeof( _a ) / sizeof( _a[0]) )

//Void function pointer
typedef void (* void_func_type)( void );

#endif /* CARGT_H_ */
