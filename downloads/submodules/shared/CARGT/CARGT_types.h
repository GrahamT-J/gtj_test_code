/*
 * CARGT_types.h
 *
 *  Created by Peter Carlson Oct 2, 2013
 *  Copyright (c) 2013 Cargt, LLC. All rights reserved.
 *
 */

#ifndef CARGT_TYPES_H_
#define CARGT_TYPES_H_

/*
 * Misc. Defines
 */
#ifdef  FALSE
#undef  FALSE
#endif
#define FALSE   (0)

#ifdef  TRUE
#undef  TRUE
#endif
#define TRUE    (1)

#ifdef  NULL
#undef  NULL
#endif
#define NULL    (0)

#ifdef  ON
#undef  ON
#endif
#define ON      (1)

#ifdef  OFF
#undef  OFF
#endif
#define OFF     (0)

/*
 * The basic data types
 */
typedef unsigned char           uint8;  /*  8 bits */
typedef unsigned short int      uint16; /* 16 bits */
typedef unsigned long int       uint32; /* 32 bits */
typedef unsigned long long int  uint64; /* 64 bits*/

typedef char                    int8;   /*  8 bits */
typedef short int               int16;  /* 16 bits */
typedef int                     int32;  /* 32 bits */
typedef signed long long int    int64;  /* 64 bits */

typedef volatile int8           vint8;  /*  8 bits */
typedef volatile int16          vint16; /* 16 bits */
typedef volatile int32          vint32; /* 32 bits */
typedef volatile int64          vint64; /* 64 bits */

typedef volatile uint8          vuint8;  /*  8 bits */
typedef volatile uint16         vuint16; /* 16 bits */
typedef volatile uint32         vuint32; /* 32 bits */
typedef volatile uint64         vuint64; /* 64 bits */

typedef uint8   U08;    /*  8 bits */
typedef uint16  U16;    /* 16 bits */
typedef uint32  U32;    /* 32 bits */
typedef uint64  U64;    /* 64 bits */
typedef uint8   boolean;

#endif /* CARGT_TYPES_H_ */
