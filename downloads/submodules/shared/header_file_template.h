/*
 * Header_File_Name.h
 *
 *  Created by AUTHOR on DATE
 *  Copyright (c) YEAR Cargt, LLC. All rights reserved.
 *
 */

#ifndef Header_File_Name_H_
#define Header_File_Name_H_

#include "CARGT.h"

void function_a();

uint16 add_u8
    (
    uint8	x,
    uitn8	z
    );

#endif /* Header_File_Name_H_ */
