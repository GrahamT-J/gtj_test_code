/* Copyright (c) 2012 Nordic Semiconductor. All Rights Reserved.
 *
 * The information contained herein is property of Nordic Semiconductor ASA.
 * Terms and conditions of usage are described in detail in NORDIC
 * SEMICONDUCTOR STANDARD SOFTWARE LICENSE AGREEMENT.
 *
 * Licensees are granted free, non-transferable use of the information. NO
 * WARRANTY of ANY KIND is provided. This heading must NOT be removed from
 * the file.
 *
 */
#include "sdk_config.h"
#if BLE_DMP_ENABLED
#include "ble_dmp.h"
#include "ble_srv_common.h"
#include "sdk_common.h"

#include <stdio.h>

#include "CARGT.h"
#include "IOP.h"

#define BLE_UUID_DMP_TX_CHARACTERISTIC 0xc030                      /**< The UUID of the TX Characteristic. */
#define BLE_UUID_DMP_RX_CHARACTERISTIC 0xc031                      /**< The UUID of the RX Characteristic. */


#define BLE_DMP_MAX_RX_CHAR_LEN        BLE_DMP_MAX_DATA_LEN        /**< Maximum length of the RX Characteristic (in bytes). */
#define BLE_DMP_MAX_TX_CHAR_LEN        BLE_DMP_MAX_DATA_LEN        /**< Maximum length of the TX Characteristic (in bytes). */

//f231c03X-d5b9-4528-ba09-f65de018892f
//f231xxxx-d5b9-4528-ba09-f65de018892f
#define DMP_BASE_UUID             {{0x2f, 0x89, 0x18, 0xe0, 0x5d, 0xf6, 0x09, 0xba, 0x28, 0x45, 0xb9, 0xd5, 0x00, 0x00, 0x31, 0xf2}} /**< Used vendor specific UUID. */


static UTL_circ_buf_t 	ble_rx_crc;
static uint8_t			ble_rx_buf[IOP_BUF_SIZE*2];
static uint8_t			tmp_rx_buf[BLE_DMP_MAX_RX_CHAR_LEN];
static bool 			tmp_buf_dirty = false;


static uint32_t send_data( ble_dmp_t * p_db )
{
	//Local variables
    ble_gatts_hvx_params_t 	hvx_params;
    uint32_t      			err_code = NRF_SUCCESS;
    uint32_t 				bytes_remaining = UTL_circ_buf_used_space( &ble_rx_crc );
    uint16_t				xfer_size = minval( bytes_remaining, BLE_DMP_MAX_RX_CHAR_LEN );

    memset(&hvx_params, 0, sizeof(hvx_params));

    if( tmp_buf_dirty )
    {
    	tmp_buf_dirty = false;

    	xfer_size = BLE_DMP_MAX_RX_CHAR_LEN;

    	hvx_params.handle = p_db->rx_handles.value_handle;
    	hvx_params.p_data = tmp_rx_buf;
		hvx_params.p_len  = &xfer_size;
		hvx_params.type   = BLE_GATT_HVX_NOTIFICATION;

		err_code = sd_ble_gatts_hvx(p_db->conn_handle, &hvx_params);
    }

    while( bytes_remaining > 0 && err_code == NRF_SUCCESS )
    {
    	UTL_circ_buf_read( &ble_rx_crc, tmp_rx_buf, xfer_size );

    	hvx_params.handle = p_db->rx_handles.value_handle;
    	hvx_params.p_data = tmp_rx_buf;
		hvx_params.p_len  = &xfer_size;
		hvx_params.type   = BLE_GATT_HVX_NOTIFICATION;

		err_code = sd_ble_gatts_hvx(p_db->conn_handle, &hvx_params);

		bytes_remaining	-= xfer_size;
		xfer_size = minval( bytes_remaining, BLE_DMP_MAX_RX_CHAR_LEN );
    }

    // Set flag to indicate there is still data to be sent before reading
    // from circular buffer
    if( err_code != NRF_SUCCESS )
    {
    	tmp_buf_dirty = true;
    }

    return err_code;
}

/**@brief Function for handling the @ref BLE_GAP_EVT_CONNECTED event from the S110 SoftDevice.
 *
 * @param[in] p_nus     Nordic UART Service structure.
 * @param[in] p_ble_evt Pointer to the event received from BLE stack.
 */
static void on_connect(ble_dmp_t * p_db, ble_evt_t * p_ble_evt)
{
	p_db->conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
}


/**@brief Function for handling the @ref BLE_GAP_EVT_DISCONNECTED event from the S110 SoftDevice.
 *
 * @param[in] p_nus     Nordic UART Service structure.
 * @param[in] p_ble_evt Pointer to the event received from BLE stack.
 */
static void on_disconnect(ble_dmp_t * p_db, ble_evt_t * p_ble_evt)
{
    UNUSED_PARAMETER(p_ble_evt);
    p_db->conn_handle = BLE_CONN_HANDLE_INVALID;
}


/**@brief Function for handling the @ref BLE_GATTS_EVT_WRITE event from the S110 SoftDevice.
 *
 * @param[in] p_nus     Nordic UART Service structure.
 * @param[in] p_ble_evt Pointer to the event received from BLE stack.
 */
static void on_write(ble_dmp_t * p_db, ble_evt_t * p_ble_evt)
{
    ble_gatts_evt_write_t * p_evt_write = &p_ble_evt->evt.gatts_evt.params.write;

    if (
        (p_evt_write->handle == p_db->rx_handles.cccd_handle)
        &&
        (p_evt_write->len == 2)
       )
    {
        if (ble_srv_is_notification_enabled(p_evt_write->data))
        {
        	p_db->is_notification_enabled = true;
        }
        else
        {
        	p_db->is_notification_enabled = false;
        }
    }
    else if (
             (p_evt_write->handle == p_db->tx_handles.value_handle)
             &&
             (p_db->data_handler != NULL)
            )
    {
    	p_db->data_handler(p_db, p_evt_write->data, p_evt_write->len);
    }
    else
    {
        // Do Nothing. This event is not relevant for this service.
    }
}


/**@brief Function for adding RX characteristic.
 *
 * @param[in] p_db       Databurn IOP RX Service structure.
 * @param[in] p_db_init  Information needed to initialize the service.
 *
 * @return NRF_SUCCESS on success, otherwise an error code.
 */
static uint32_t rx_char_add(ble_dmp_t * p_db, const ble_dmp_init_t * p_db_init)
{
    /**@snippet [Adding proprietary characteristic to S110 SoftDevice] */
    ble_gatts_char_md_t char_md;
    ble_gatts_attr_md_t cccd_md;
    ble_gatts_attr_t    attr_char_value;
    ble_uuid_t          ble_uuid;
    ble_gatts_attr_md_t attr_md;

    memset(&cccd_md, 0, sizeof(cccd_md));

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.write_perm);

    cccd_md.vloc = BLE_GATTS_VLOC_STACK;

    memset(&char_md, 0, sizeof(char_md));

    char_md.char_props.notify = 1;
    char_md.p_char_user_desc  = NULL;
    char_md.p_char_pf         = NULL;
    char_md.p_user_desc_md    = NULL;
    char_md.p_cccd_md         = &cccd_md;
    char_md.p_sccd_md         = NULL;

    ble_uuid.type = p_db->uuid_type;
    ble_uuid.uuid = BLE_UUID_DMP_RX_CHARACTERISTIC;

    memset(&attr_md, 0, sizeof(attr_md));

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.write_perm);

    attr_md.vloc    = BLE_GATTS_VLOC_STACK;
    attr_md.rd_auth = 0;
    attr_md.wr_auth = 0;
    attr_md.vlen    = 1;

    memset(&attr_char_value, 0, sizeof(attr_char_value));

    attr_char_value.p_uuid    = &ble_uuid;
    attr_char_value.p_attr_md = &attr_md;
    attr_char_value.init_len  = sizeof(uint8_t);
    attr_char_value.init_offs = 0;
    attr_char_value.max_len   = BLE_DMP_MAX_RX_CHAR_LEN;

    return sd_ble_gatts_characteristic_add(p_db->service_handle,
                                           &char_md,
                                           &attr_char_value,
                                           &p_db->rx_handles);
    /**@snippet [Adding proprietary characteristic to S110 SoftDevice] */
}


/**@brief Function for adding TX characteristic.
 *
 * @param[in] p_db       Databurn IOP TX Service structure.
 * @param[in] p_db_init  Information needed to initialize the service.
 *
 * @return NRF_SUCCESS on success, otherwise an error code.
 */
static uint32_t tx_char_add(ble_dmp_t * p_db, const ble_dmp_init_t * p_db_init)
{
    ble_gatts_char_md_t char_md;
    ble_gatts_attr_t    attr_char_value;
    ble_uuid_t          ble_uuid;
    ble_gatts_attr_md_t attr_md;

    memset(&char_md, 0, sizeof(char_md));

//    char_md.char_props.write         = 1;
    char_md.char_props.write_wo_resp = 1;
    char_md.p_char_user_desc         = NULL;
    char_md.p_char_pf                = NULL;
    char_md.p_user_desc_md           = NULL;
    char_md.p_cccd_md                = NULL;
    char_md.p_sccd_md                = NULL;

    ble_uuid.type = p_db->uuid_type;
    ble_uuid.uuid = BLE_UUID_DMP_TX_CHARACTERISTIC;

    memset(&attr_md, 0, sizeof(attr_md));

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.write_perm);

    attr_md.vloc    = BLE_GATTS_VLOC_STACK;
    attr_md.rd_auth = 0;
    attr_md.wr_auth = 0;
    attr_md.vlen    = 1;

    memset(&attr_char_value, 0, sizeof(attr_char_value));

    attr_char_value.p_uuid    = &ble_uuid;
    attr_char_value.p_attr_md = &attr_md;
    attr_char_value.init_len  = 1;
    attr_char_value.init_offs = 0;
    attr_char_value.max_len   = BLE_DMP_MAX_TX_CHAR_LEN;

    return sd_ble_gatts_characteristic_add(p_db->service_handle,
                                           &char_md,
                                           &attr_char_value,
                                           &p_db->tx_handles);
}


void ble_dmp_on_ble_evt(ble_dmp_t * p_db, ble_evt_t * p_ble_evt)
{
    if ((p_db == NULL) || (p_ble_evt == NULL))
    {
        return;
    }

    switch (p_ble_evt->header.evt_id)
    {
        case BLE_GAP_EVT_CONNECTED:
            on_connect(p_db, p_ble_evt);
            break;

        case BLE_GAP_EVT_DISCONNECTED:
            on_disconnect(p_db, p_ble_evt);
            break;

        case BLE_GATTS_EVT_WRITE:
            on_write(p_db, p_ble_evt);
            break;

        case BLE_EVT_TX_COMPLETE:
        	// Check to see if there is more data to be sent
        	if( tmp_buf_dirty )
        	{
        		send_data( p_db );
        	}
        	break;

        default:
            // No implementation needed.
            break;
    }
}


uint32_t ble_dmp_init(ble_dmp_t * p_db, const ble_dmp_init_t * p_db_init)
{
    uint32_t      err_code;
    ble_uuid_t    ble_uuid;
//    ble_uuid128_t nus_base_uuid = NUS_BASE_UUID;
    ble_uuid128_t db_base_uuid = DMP_BASE_UUID;


    VERIFY_PARAM_NOT_NULL(p_db);
    VERIFY_PARAM_NOT_NULL(p_db_init);

    UTL_circ_buf_init( &ble_rx_crc, ble_rx_buf, sizeof(ble_rx_buf) );


    // Initialize the service structure.
    p_db->conn_handle             = BLE_CONN_HANDLE_INVALID;
    p_db->data_handler            = p_db_init->data_handler;
    p_db->is_notification_enabled = false;

    /**@snippet [Adding proprietary Service to S110 SoftDevice] */
    // Add a custom base UUID.
    err_code = sd_ble_uuid_vs_add(&db_base_uuid, &p_db->uuid_type);
    VERIFY_SUCCESS(err_code);

    ble_uuid.type = p_db->uuid_type;
    ble_uuid.uuid = BLE_UUID_DMP_SERVICE;

    // Add the service.
    err_code = sd_ble_gatts_service_add(BLE_GATTS_SRVC_TYPE_PRIMARY,
                                        &ble_uuid,
                                        &p_db->service_handle);
    /**@snippet [Adding proprietary Service to S110 SoftDevice] */
    VERIFY_SUCCESS(err_code);

    // Add the IOP RX Characteristic.
    err_code = rx_char_add(p_db, p_db_init);
    VERIFY_SUCCESS(err_code);

    // Add the IOP TX Characteristic.
    err_code = tx_char_add(p_db, p_db_init);
    VERIFY_SUCCESS(err_code);

    return NRF_SUCCESS;
}


uint32_t ble_dmp_send_data(ble_dmp_t * p_db, uint8_t * p_data, uint16_t length)
{
//    ble_gatts_hvx_params_t 	hvx_params;
//    uint32_t      			err_code = NRF_SUCCESS;
//    uint32_t 				bytes_remaining = length;
//    uint32_t 				buffer_offset = 0;
//    uint16_t				xfer_size = minval( length, BLE_DMP_MAX_RX_CHAR_LEN );
//
//    VERIFY_PARAM_NOT_NULL(p_db);
//
//    if ((p_db->conn_handle == BLE_CONN_HANDLE_INVALID) || (!p_db->is_notification_enabled))
//    {
//        return NRF_ERROR_INVALID_STATE;
//    }
//
//
//    if (length > UTL_circ_buf_free_space( &ble_rx_crc ))
//    {
//        return NRF_ERROR_INVALID_PARAM;
//    }
//
//    //Fill circular buffer with data to be sent
//    UTL_circ_buf_write( &ble_rx_crc, p_data, length );

    return send_data( p_db );
}

#endif //BLE_DMP_ENABLED
