/*
 * I2cStateMachine.h
 *
 *  Created on: Apr 18, 2017
 *      Author: cargt
 */

#ifndef I2CSTATEMACHINE_H_
#define I2CSTATEMACHINE_H_

#include <vector>

class I2cStateMachine
{
public:
	I2cStateMachine();
	~I2cStateMachine() {;}

	int getIOPPayload();

	int sendReadLengthRequest();
	int sendIOP_ProductRequest();
	int sendLedPatternRequest(uint8_t patternIdx);

	int requestAndGetNextIOP();

	void processMessage();

protected:

	enum { I2C_STATE_START = 0
			, I2C_STATE_AWAITING_READ_LENGTH
			, I2C_STATE_AWAITING_IOP_PACKET
	} internalStates;

private:
    char writeBuff[1024];
    char readBuff[1024];

    uint32_t m_nextMessageLength;

    std::vector<char> writeBuf;
    std::vector<char> readBuf;

};


#endif /* I2CSTATEMACHINE_H_ */
