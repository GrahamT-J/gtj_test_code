/*
 * i2c_iop.h
 *
 *  Created on: Apr 18, 2017
 *      Author: cargt
 */

#ifndef I2C_IOP_H_
#define I2C_IOP_H_

#include <stdint.h>
#include <vector>

#define MAX_LED_PATTERNS 3
#define LED_PATTERN_MSG_SIZE 13


void I2C_initialize(void);

int I2C_read_length_request(std::vector<uint8_t>& read_length);
int I2C_read_IOP_message(uint32_t message_length);

int I2C_write_prod_rqst_packet();
int I2C_write_led_pattern(uint8_t reqPattern);

int Linux_I2C_readByte
	(
	signed   int 	adapter_nr,
	unsigned char 	address,
	unsigned char 	indexSize,
	unsigned char * pucAddressBuffer,
	signed   int 	iNbBytes,
	unsigned char * pucDataBuffer
	);


int Linux_I2C_writeByte
	(
	signed   int 	adapter_nr,
	unsigned char 	address,
	signed   int 	iNbBytes,
	unsigned char * pucBuffer
	);


#endif /* I2C_IOP_H_ */
