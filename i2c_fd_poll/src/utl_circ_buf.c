/*
 * utl_circ_buf.c
 *
 *  Created by Peter Carlson on Oct 2, 2013
 *  Copyright (c) 2013 Cargt, LLC. All rights reserved.
 */

#include <string.h>


#include "CARGT.h"
#include "UTL_circ_buf.h"



#if defined(__ICCARM__)
	#include "cmsis_os.h"
#elif defined(linux)
	typedef unsigned long UBaseType_t;
	// TODO change to use linux functions
    #define taskENTER_CRITICAL_FROM_ISR(x) 0 // linux function needed to save interrupt mask
    #define taskEXIT_CRITICAL_FROM_ISR(x) 0  // linux function needed to save interrupt mask
#else
	#include "FreeRTOS.h"
	#include "task.h"
#endif

/*
*   PROCEDURE NAME:
*       UTL_circ_buf_free_space - Circular buffer free space
*
*   DESCRIPTION:
*       Returns the number of bytes of free space in the circular
*       buffer.
*
*/
uint32 UTL_circ_buf_free_space
    (
    UTL_circ_buf_t * cb
    )
{
return cb->data_size - UTL_circ_buf_used_space( cb );
} /* UTL_circ_buf_free_space() */


/*
*   PROCEDURE NAME:
*       UTL_circ_buf_init - Initialize circular buffer
*
*   DESCRIPTION:
*       Initializes the circular buffer by allocating memory of size
*       bytes and setting indices to zero and indicating not full.
*
*/
void UTL_circ_buf_init
    (
    UTL_circ_buf_t * 	cb,
    uint8 *				buf,
    uint32 				size
    )
{
cb->mem = buf;
cb->data_size = size;
cb->read_idx = 0;
cb->write_idx = 0;
cb->full = FALSE;
} /* UTL_circ_buf_init() */


/*
*   PROCEDURE NAME:
*       UTL_circ_buf_read - Circular buffer read
*
*   DESCRIPTION:
*       Read up to buf_size bytes from cb into buf. If buf is NULL,
*       the read_idx member of cb is updated but no memory is actually
*       copied. Returns the number of bytes read into buf.
*
*/
uint32 UTL_circ_buf_read
    (
    UTL_circ_buf_t * 	cb,
    uint8 * 			buf,
    uint32 				buf_size
    )
{
uint32              copy_size;
uint32              copy0;
uint32              copy1;
uint32              read_idx;
UBaseType_t 		uxSavedInterruptStatus;

copy_size = UTL_circ_buf_used_space( cb );
copy_size = minval( copy_size, buf_size );

if( !copy_size )
    {
    goto out_free;
    }

read_idx = cb->read_idx;


//If copy goes off the end of the buffer, do it in two parts.
//Otherwise do it in one big copy.
if( read_idx + copy_size > cb->data_size )
    {
    copy0 = cb->data_size - read_idx;
    copy1 = copy_size - copy0;
    if( buf )
        {
        memmove( buf, &cb->mem[read_idx], copy0 );
        buf += copy0;
        memmove( buf, cb->mem, copy1 );
        }
    read_idx = copy1;
    }
else
    {
    if( buf )
        {
        memmove( buf, &cb->mem[read_idx], copy_size );
        }
    read_idx += copy_size;
    if( read_idx == cb->data_size )
        {
        read_idx = 0;
        }
    }

//Update read index and full status. Disable interrupts
//if not running in interrupt context.
uxSavedInterruptStatus = taskENTER_CRITICAL_FROM_ISR();
cb->read_idx = read_idx;
cb->full = FALSE;
taskEXIT_CRITICAL_FROM_ISR(uxSavedInterruptStatus);

out_free:
return copy_size;
} /* UTL_circ_buf_read() */


/*
*   PROCEDURE NAME:
*       UTL_circ_buf_reset_to - Reset read and write indexes
*
*   DESCRIPTION:
*       Reset the read and write pointers to a specific offset
*       in the buffer.
*
*/
void UTL_circ_buf_reset
    (
    UTL_circ_buf_t *     cb
    )
{
cb->full 		= FALSE;
cb->write_idx	= 0;
cb->read_idx 	= 0;
} /* UTL_circ_buf_reset() */

/*
*   PROCEDURE NAME:
*       UTL_circ_buf_write - Circular buffer write
*
*   DESCRIPTION:
*       Write up to buf_size bytes into cb from buf. If buf is NULL,
*       the write_idx member of cb is updated but no memory is actually
*       copied. Returns the number of bytes written into buf.
*
*/
uint32 UTL_circ_buf_write
    (
    UTL_circ_buf_t * 	cb,
    uint8 * 			buf,
    uint32 				buf_size
    )
{
uint32              copy_size;
uint32              copy0;
uint32              copy1;
uint32              write_idx;
UBaseType_t 		uxSavedInterruptStatus;

copy_size = UTL_circ_buf_free_space( cb );
copy_size = minval( copy_size, buf_size );

if( !copy_size )
    {
    goto out_free;
    }

write_idx = cb->write_idx;

//If copy goes off the end of the buffer, do it in two parts.
if( write_idx + copy_size > cb->data_size )
    {
    copy0 = cb->data_size - write_idx;
    copy1 = copy_size - copy0;
    if( buf != NULL )
        {
        memmove( &cb->mem[write_idx], buf, copy0 );
        buf += copy0;
        memmove( cb->mem, buf, copy1 );
        }
    write_idx = copy1;
    }
else
    {
    if( buf )
        {
        memmove( &cb->mem[write_idx], buf, copy_size );
        }
    write_idx += copy_size;
    if( write_idx == cb->data_size )
        {
        write_idx = 0;
        }
    }

//Update write index and full status.
uxSavedInterruptStatus = taskENTER_CRITICAL_FROM_ISR();
cb->write_idx = write_idx;
cb->full = (cb->read_idx == cb->write_idx);
taskEXIT_CRITICAL_FROM_ISR(uxSavedInterruptStatus);

out_free:
return copy_size;
} /* UTL_circ_buf_write() */


/*
*   PROCEDURE NAME:
*       UTL_circ_buf_used_space - Circular buffer used space
*
*   DESCRIPTION:
*       Returns the number of bytes in cb.
*
*/
uint32 UTL_circ_buf_used_space
    (
    UTL_circ_buf_t * cb
    )
{
if( TRUE == cb->full )
    {
    return cb->data_size;
    }
else if( cb->write_idx >= cb->read_idx )
    {
    return cb->write_idx - cb->read_idx;
    }
else
    {
    return cb->data_size - (cb->read_idx - cb->write_idx);
    }
} /* UTL_circ_buf_used_space() */

