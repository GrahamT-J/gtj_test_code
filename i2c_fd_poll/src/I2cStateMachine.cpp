/*
 * I2cStateMachine.cpp
 *
 *  Created on: Apr 18, 2017
 *      Author: cargt
 */

#include <stdio.h>
#include <stdint.h>
#include <iostream>

#include "I2cStateMachine.h"
#include "i2c_iop.h"

using namespace std;

I2cStateMachine::I2cStateMachine()
{
	internalStates = I2C_STATE_START;
	writeBuf.reserve(1024);
	readBuf.reserve(1024);
	m_nextMessageLength = 0;
	I2C_initialize();
}

int I2cStateMachine::sendReadLengthRequest()
{
	std::vector<uint8_t>buffer;
	int retVal = I2C_read_length_request(buffer);
	if(retVal < 0)
	{
		cout << "Error: ";
		m_nextMessageLength = 0;
	}
	else
	{
		m_nextMessageLength =
				(((uint32_t)(buffer[0])) << 24)
				+ (((uint32_t)(buffer[1])) << 16)
				+ (((uint32_t)(buffer[2])) << 8)
				+ ((uint32_t)(buffer[3]));

	}

	cout << "Read length request: ret len=" << buffer.size();
	cout << " Next Msg size=" << std::dec << m_nextMessageLength;
	cout << " Raw Vals=";

	for(int i = 0; i < buffer.size(); ++i)
		{
		cout << std::hex << buffer[i] << ",";
		}

	cout << endl;
	return retVal;
}

int I2cStateMachine::sendIOP_ProductRequest()
{
	return I2C_write_prod_rqst_packet();
}

int I2cStateMachine::sendLedPatternRequest(uint8_t patternIdx)
{
	return I2C_write_led_pattern(patternIdx);
}

int I2cStateMachine::getIOPPayload()
{
	return I2C_read_IOP_message(m_nextMessageLength);
}

int I2cStateMachine::requestAndGetNextIOP()
{
	int errVal;

	errVal = sendReadLengthRequest();
	if (errVal < 0)
	{
		return errVal;
	}

	if(0 == m_nextMessageLength)
	{
		return -1;
	}

	errVal = getIOPPayload();
	return errVal;
}
