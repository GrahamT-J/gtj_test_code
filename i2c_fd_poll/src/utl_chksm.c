/*
 * utl_chksm.c
 *
 *  Created by Peter Carlson on Oct 2, 2013
 *  Copyright (c) 2013 Cargt, LLC. All rights reserved.
 */

#include "CARGT.h"


/*
*   PROCEDURE NAME:
*       UTL_calc_chksum8 - Calculate 8-bit checksum
*
*   DESCRIPTION:
*
*/
uint8 UTL_calc_chksum8
    (
    uint8   const * const data,
    uint32          const size
    )
{
//Local Variables
uint8			chksum 	= 0;
uint8 const *	ptr 	= data;
uint32			i 		= 0;

while( i < size )
	{
	chksum += *ptr;
	ptr++;
	i++;
	}

return chksum;
}   /* UTL_calc_chksum8 */


/*
*   PROCEDURE NAME:
*       UTL_calc_chksum16 - Calculate 16-bit checksum
*
*   DESCRIPTION:
*
*/
uint16 UTL_calc_chksum16
    (
    uint16  const * const data,
    uint32          const size
    )
{
//Local Variables
uint16			chksum 	= 0;
uint16 const *	ptr 	= data;
uint32			i 		= 0;

while( i < ( size / sizeof( ptr ) ) )
	{
	chksum += *ptr;
	ptr++;
	i++;
	}

return chksum;
}   /* UTL_calc_chksum16 */


/*
*   PROCEDURE NAME:
*       UTL_calc_chksum32 - Calculate 32-bit checksum
*
*   DESCRIPTION:
*
*/
uint32 UTL_calc_chksum32
    (
    uint32  const * const data,
    uint32          const size
    )
{
//Local Variables
uint32			chksum 	= 0;
uint32 const *	ptr 	= data;
uint32			i 		= 0;

while( i < ( size / sizeof( ptr ) ) )
	{
	chksum += *ptr;
	ptr++;
	i++;
	}

return chksum;
}   /* UTL_calc_chksum32 */
