/*
 * Source_File_Name.c
 *
 *  Created by AUTHOR on DATE
 *  Copyright (c) YEAR Cargt, LLC. All rights reserved.
 *
 */

#include "CARGT.h"

/*
*   PROCEDURE NAME:
*       function_a
*
*   DESCRIPTION:
*		Expected input / output
*/
void function_a
    ( void )
{
//Add code here
}   /* function_a() */


/*
*   PROCEDURE NAME:
*       add_u8
*
*   DESCRIPTION:
*		Add two bytes
*/
uint16 add_u8
    (
    uint8	x,
	uint8	z
    )
{
//Add code here
return x + z;
}   /* add_u8() */
