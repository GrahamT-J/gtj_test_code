#ifndef __GPIO_EXPORT_H__
#define __GPIO_EXPORT_H__

int gpio_export(unsigned int gpio);
int gpio_unexport(unsigned int gpio);

int gpio_set_active_low(unsigned int gpio, unsigned int active_low_flag);
int gpio_set_dir(unsigned int gpio, unsigned int out_flag);
int gpio_set_pullup(unsigned int gpio, unsigned int pullup_flag);
int gpio_set_value(unsigned int gpio, unsigned int value);
int gpio_get_value(unsigned int gpio, unsigned int *value);
int gpio_set_edge(unsigned int gpio, char *edge);
int gpio_fd_open(unsigned int gpio);
int gpio_fd_close(int fd);

#endif //__GPIO_EXPORT_H__
