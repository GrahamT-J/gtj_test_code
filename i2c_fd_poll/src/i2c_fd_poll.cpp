/*
 * Copyright (C) Your copyright notice.
 *
 * Author: Me
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the PG_ORGANIZATION nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY	THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS-IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <poll.h>
#include <unistd.h>

#include "i2c_iop.h"
#include "I2cStateMachine.h"

#ifdef __cplusplus
extern "C" {
#endif
    #include "gpio_export.h"
#ifdef __cplusplus
}
#endif

using namespace std;

/****************************************************************
* Constants
****************************************************************/

#define SYSFS_GPIO_DIR "/sys/class/gpio"
#define POLL_TIMEOUT (3 * 1000) /* n seconds */
#define MAX_BUF 64

struct {
	bool gpioTriggered;
	bool stdInCharReceived;
	char charValue;
} taskList;

uint8_t ledPatternIdx = 0;

/****************************************************************
 * Main
 ****************************************************************/
int main(int argc, char **argv, char **envp)
{
	I2cStateMachine i2cProcessor;

	cout << "Hello World from i2c_fd_poll" << endl; /* prints Hello World */

	cout << "Usage: gpio-int <gpio-pin>\n\n";
	cout << "Waits for a change in the GPIO pin voltage level or input on stdin\n";
	cout << "x or q to exit" << endl;
	cout << "N - get next IOP message" << endl;
	cout << "P - product request" << endl;
	cout << "R - read length request" << endl;
	cout << "I - read IOP message" << endl;
	cout << "L - set next LED pattern" << endl;

	if (argc < 2) {
		exit(-1);
	}

	struct pollfd fdset[2];
	int nfds = 2;
	int rc;
	char buf[MAX_BUF];
	int len;

	unsigned int gpio;
	int gpio_fd;

	gpio = atoi(argv[1]);

	if (gpio_unexport(gpio) < 0) return -1; // make sure we get full control
	if (gpio_export(gpio) < 0) return -1;
	if (gpio_set_dir(gpio, 0) < 0) return -1;
	//if (gpio_set_pullup(gpio, 1) < 0) return -1;
	//if (gpio_set_active_low(gpio, 1) < 0) return -1;

	std::string edgeString = "falling";
	if (gpio_set_edge(gpio, const_cast<char*>(edgeString.c_str())) < 0) return -1;

	gpio_fd = gpio_fd_open(gpio);

	int timeout = POLL_TIMEOUT;

	memset((void*)fdset, 0, sizeof(fdset));
	memset(&taskList, 0, sizeof taskList);

	fdset[0].fd = STDIN_FILENO;
	fdset[0].events = POLLIN;

	fdset[1].fd = gpio_fd;
	//fdset[1].events = POLLPRI;
	fdset[1].events = POLLPRI | POLLERR;

	// poll gpio always generates an initial interrupt on first call.
	// don't wish to remove the first gpio interrupt as could be already be active
	// there would be no edge to detect

	bool exitRequested = false;
	while (false == exitRequested)
	{

		// wait for input / gpio change
		rc = poll(fdset, nfds, timeout);

		if (rc < 0) {
			printf("\npoll() failed!\n");
			return -1;
		}

		// poll timed out
		if (rc == 0) {
			printf(".");
		}

		if (fdset[1].revents & POLLPRI) {
			lseek(fdset[1].fd, 0, SEEK_SET);
			len = read(fdset[1].fd, buf, MAX_BUF);
			printf("poll() GPIO %d interrupt occurred, len=%d\n", gpio, len);
			taskList.gpioTriggered = true;
		}

		if (fdset[0].revents & POLLIN) {
			(void)read(fdset[0].fd, buf, 1);
			if('\n' == buf[0])
			{
				printf("poll() stdin read \"newline\", 0x%2.2X\n", (unsigned int) buf[0]);
			}
			else
			{
				taskList.stdInCharReceived = true;
				taskList.charValue = buf[0];
				printf("poll() stdin read c=%c, 0x%2.2X\n", buf[0], (unsigned int) buf[0]);
			}
		}

		/********************
		 * Task List area
		 ********************/

		if(taskList.gpioTriggered)
		{
			unsigned int curVal = 0;
			int errVal = gpio_get_value(gpio, &curVal);
			if((errVal < 0) || (0 != curVal))
			{
				cout << "GPIO Action - NOTHING. Current gpio value = " << curVal << ". errVal=" << errVal << endl;
				taskList.gpioTriggered = false;
			}
			else
			{
				cout << "GPIO action - executing req & get next msg\n";
				i2cProcessor.requestAndGetNextIOP();
			}
		}
		if(taskList.stdInCharReceived)
		{
			cout << "Processing input char" << endl;
			switch(taskList.charValue)
			{
			case 'x':
			case 'q':
			{
				exitRequested = true;
			}
				break;
			case 'P':
			{
				cout << "Writing product request message" << endl;
				int errVal = i2cProcessor.sendIOP_ProductRequest();
				cout << "Prod Req ret value: %d\n" << errVal;
			}
				break;
			case 'R':
			{
				cout << "Sending read length request" << endl;
				int errVal = i2cProcessor.sendReadLengthRequest();
				cout << "Read length req ret value: %d\n" << errVal;
			}
				break;
			case 'I':
			{
				cout << "Reading IOP message of previously stored length" << endl;
				int errVal = i2cProcessor.getIOPPayload();
				cout << "Read IOP ret val=" << errVal;
			}
				break;
			case 'N':
			{
				cout << "Requesting and reading next IOP message" << endl;
				int errVal = i2cProcessor.requestAndGetNextIOP();
				cout << "Read next message ret val=" << errVal;
			}
			    break;
			case 'L':
			{
				++ledPatternIdx;
				if (ledPatternIdx >= MAX_LED_PATTERNS)
				    ledPatternIdx = 0;
				cout << "Set next LED pattern = " << ledPatternIdx << endl;
				int errVal = i2cProcessor.sendLedPatternRequest(ledPatternIdx);
				cout << "Set Led pattern ret val =" << errVal;
			}
			    break;
			}  // end of switch

			taskList.stdInCharReceived = false;
		}

		fflush(stdout);
	} // while not exit;

	return 0;
} // main

