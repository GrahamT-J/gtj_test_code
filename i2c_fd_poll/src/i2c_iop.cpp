/*
 * i2c_iop.cpp
 *
 *  Created on: Apr 18, 2017
 *      Author: cargt
 */

#include <stdio.h>
#include <stdint.h>
#include <iostream>
#include <fcntl.h>
#include <unistd.h>
#include <linux/i2c-dev.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <string.h>

#include "i2c_iop.h"

#ifdef __cplusplus
extern "C" {
#endif
    #include "IOP.h"
    #include "UTL_circ_buf.h"
#ifdef __cplusplus
}
#endif

//#include "android/log.h"
//static const char *TAG="wp_i2c";
//#define LOGI(fmt, args...) __android_log_print(ANDROID_LOG_INFO,  TAG, fmt, ##args)
//#define LOGD(fmt, args...) __android_log_print(ANDROID_LOG_DEBUG, TAG, fmt, ##args)
//#define LOGE(fmt, args...) __android_log_print(ANDROID_LOG_ERROR, TAG, fmt, ##args)
#define LOGI(fmt, args...) printf(fmt, ##args)
#define LOGE(fmt, args...) printf(fmt, ##args)

typedef unsigned short	__u16;
typedef unsigned char	__u8;

struct i2c_msg {
	__u16 addr;	/* slave address			*/
	__u16 flags;
#define I2C_M_TEN		0x0010	/* this is a ten bit chip address */
#define I2C_M_RD		0x0001	/* read data, from slave to master */
#define I2C_M_STOP		0x8000	/* if I2C_FUNC_PROTOCOL_MANGLING */
#define I2C_M_NOSTART		0x4000	/* if I2C_FUNC_NOSTART */
#define I2C_M_REV_DIR_ADDR	0x2000	/* if I2C_FUNC_PROTOCOL_MANGLING */
#define I2C_M_IGNORE_NAK	0x1000	/* if I2C_FUNC_PROTOCOL_MANGLING */
#define I2C_M_NO_RD_ACK		0x0800	/* if I2C_FUNC_PROTOCOL_MANGLING */
#define I2C_M_RECV_LEN		0x0400	/* length will be first received byte */
	__u16 len;		/* msg length				*/
	__u8 *buf;		/* pointer to msg data			*/
};


#define I2C_CMD_WRITE_IOP_PACKET    0x24
#define I2C_CMD_READ_LENGTH_RQST    0x25
#define I2C_CMD_READ_IOP_PACKET     0x26

#define I2C_SIZE_READ_LENGTH_RESPONSE 4

unsigned char iopMsg_ProdRqst[] = {
		/* DLE, No ack */ 0x10, 0x00,
		/* cmd */ 0x00,0x01,
		/* size */ 0x00, 0x00, 0x00, 0x00,
		/* data section */
		/* crc */ 0x00, 0x11,
		/* DLE Ending */ 0x10, 0x03
		};

// led pattern id = 1020d == 0x03FC
char iopMsg_LedPattern[MAX_LED_PATTERNS][LED_PATTERN_MSG_SIZE] = {
		 {0x10, 0x00, /*cmd*/ 0x03, 0xFC, /*size*/ 0x00,0x00,0x00,0x01, /*data*/0x03, /*crc*/ 0x01, 0x13, /*end*/0x10,0x03} // Led Pattern - Green CW
		,{0x10, 0x00, /*cmd*/ 0x03, 0xFC, /*size*/ 0x00,0x00,0x00,0x01, /*data*/0x04, /*crc*/ 0x01, 0x14, /*end*/0x10,0x03} // Led Pattern - Green CCW
		,{0x10, 0x00, /*cmd*/ 0x03, 0xFC, /*size*/ 0x00,0x00,0x00,0x01, /*data*/0x06, /*crc*/ 0x01, 0x16, /*end*/0x10,0x03} // Led Pattern - Red CW
};

const unsigned char slave_address = 126;
#define I2C_RX_BUF_SIZE 1024
#define I2C_TX_BUF_SIZE 1024 // IOP stuffed buf max size

unsigned char rx_buffer[1 + I2C_RX_BUF_SIZE]; // 1 cmd byte + IOP stuffed buf max size
unsigned char tx_buffer[1 + I2C_TX_BUF_SIZE]; // 1 cmd byte + IOP stuffed buf max size

static IOP_ctrl_type    iop_i2c;
static IOP_msg_type     i2c_iop_msg;
static uint8_t          i2c_rx_data[I2C_RX_BUF_SIZE];
static uint8_t          i2c_tx_data[I2C_TX_BUF_SIZE];
static UTL_circ_buf_t   i2c_rx_crc_buf;
static UTL_circ_buf_t   i2c_tx_crc_buf;



/***********************************
 * Function prototypes
 ***********************************/
static void rx_msg_hndlr
    (
    void *              ctrl,
	uint16              id,
	uint8 const *       data,
	uint32              size
	);


/***********************************
 * Main code
 ***********************************/
void I2C_initialize(void)
{
	IOP_init();
	memset(&iop_i2c, 0, sizeof iop_i2c);
	iop_i2c.rx_cb       = &i2c_rx_crc_buf;
	iop_i2c.rx_buf      = i2c_rx_data;
	iop_i2c.tx_cb       = &i2c_tx_crc_buf;
	iop_i2c.tx_buf      = i2c_tx_data;
	iop_i2c.iop_rx_msg  = &i2c_iop_msg;
	iop_i2c.tx_func     = NULL; // put tx function here
	iop_i2c.reset_func  = NULL;
	IOP_register(&iop_i2c, sizeof i2c_tx_data, sizeof i2c_rx_data);
	IOP_rgstr_msg_hnldr(&iop_i2c, rx_msg_hndlr);
}

void rx_msg_hndlr
    (
    void *              ctrl,
	uint16              id,
	uint8 const *       data,
	uint32              size
	)
{
	printf("calling rx_msg_hndlr\n");
	switch(id)
	{
	//case xxx:
	//	break;
	default:
		printf("Unknown id %u. Length=%lu.", id, size);
		if(size > 0)
		{
			printf(" Data:");
		}
		for(uint32_t i = 0; i < size; ++i)
		{
			printf(" 0x%02x", data[i]);
		}
		printf("\n");
		break;
	}

}

int I2C_read_length_request(std::vector<uint8_t>& read_length )
{
	tx_buffer[0] = I2C_CMD_READ_LENGTH_RQST;
	int errVal = Linux_I2C_writeByte( 0, slave_address, 1, &tx_buffer[0]);
	if( errVal < 0)
	{
		printf("Error: read length request: write portion. err=%d", errVal);
		return errVal;
	}
	int len = I2C_SIZE_READ_LENGTH_RESPONSE;
	errVal = Linux_I2C_readByte
			(
			0, // adapter_nr,
			slave_address,
			0, // indexSize,
			NULL, // pucAddressBuffer,
			len, // iNbBytes,
			&rx_buffer[0]
			);
	if( errVal >= 0)
	{
		for(unsigned int i = 0; i < len; ++i)
		{
			read_length.push_back(rx_buffer[i]);
		}
	}
	return errVal;

}

int I2C_read_IOP_message(uint32_t message_length)
{
	if((0 == message_length) || (message_length > sizeof rx_buffer))
	{
		printf ("Error: read IOP message. next message length = %lu\n", message_length);
		return -1;
	}

	tx_buffer[0] = I2C_CMD_READ_IOP_PACKET;
	int errVal = Linux_I2C_writeByte( 0, slave_address, 1, &tx_buffer[0]);
	if( errVal < 0)
	{
		printf("Error: read IOP message: write portion. err=%d\n", errVal);
		return errVal;
	}
	errVal = Linux_I2C_readByte
			(
			0, // adapter_nr,
			slave_address,
			0, // indexSize,
			NULL, // pucAddressBuffer,
			message_length, // iNbBytes,
			&rx_buffer[0]
			);
	if( errVal >= 0)
	{
		printf("Message Rx=(0x)");
		for(unsigned int i = 0; i < message_length; ++i)
		{
			printf("%02X,", rx_buffer[i]);
		}
		printf("\n");
	    printf("Sending to IOP parser -\n");
	    // copy to circular buffer and parse
	    UTL_circ_buf_write(iop_i2c.rx_cb, &rx_buffer[0], message_length);
	    IOP_parse_rx_data(&iop_i2c, UTL_circ_buf_used_space(iop_i2c.rx_cb));
	}
	return errVal;
}

int I2C_write_led_pattern(uint8_t reqPattern)
{
	uint8_t actualPattern = 0;
	if(reqPattern <= MAX_LED_PATTERNS)
		actualPattern = reqPattern;

	tx_buffer[0] = I2C_CMD_WRITE_IOP_PACKET;
	int len = 1 + LED_PATTERN_MSG_SIZE;
	memcpy(&tx_buffer[1], &iopMsg_LedPattern[reqPattern][0], LED_PATTERN_MSG_SIZE);

	int retVal = Linux_I2C_writeByte
		(
		0, // adapter_nr,
		slave_address,
		len,
		&tx_buffer[0] );

	return retVal;
}

int I2C_write_prod_rqst_packet()
{
	tx_buffer[0] = I2C_CMD_WRITE_IOP_PACKET;
	int len = 1 + sizeof iopMsg_ProdRqst;
	memcpy(&tx_buffer[1], &iopMsg_ProdRqst[0], sizeof iopMsg_ProdRqst);

	int retVal = Linux_I2C_writeByte
		(
		0, // adapter_nr,
		slave_address,
		len,
		&tx_buffer[0] );

	return retVal;
}

int Linux_I2C_readByte
	(
	signed   int 	adapter_nr,
	unsigned char 	address,
	unsigned char 	indexSize,
	unsigned char * pucAddressBuffer,
	signed   int 	iNbBytes,
	unsigned char * pucDataBuffer
	)
{
	//LOGI("Linux_I2C_readByte: 0x%x, 0x%x, 0x%x, 0x%x", adapter_nr, address, indexSize, iNbBytes);

	int fd;
    struct i2c_msg messages[2];
    struct i2c_rdwr_ioctl_data packets;

	fd = open("/dev/i2c-0", O_RDWR);

//    if( ioctl( fd, I2C_SLAVE, address ) < 0 )
//    {
//    	LOGE("Failed to acquire bus access and/or talk to slave.\n");
//        /* ERROR HANDLING; you can check errno to see what went wrong */
//        return -1;
//    }

////    outbuf = 0; //reg
//    messages[0].addr  = address;
//    messages[0].flags = 0;
//    messages[0].len   = 1;
//    messages[0].buf   = pucAddressBuffer;
//
//    /* The data will get returned in this structure */
//    messages[1].addr  = address;
//    //messages[1].flags = I2C_M_RD/* | I2C_M_NOSTART*/;
//    messages[1].flags = I2C_M_RD | I2C_M_NOSTART;
//    messages[1].len   = iNbBytes;
//    messages[1].buf   = pucDataBuffer;

    /* The data will get returned in this structure */
    messages[0].addr  = address;
    messages[0].flags = I2C_M_RD | I2C_M_NOSTART;
    messages[0].len   = iNbBytes;
    messages[0].buf   = pucDataBuffer;

    /* Send the request to the kernel and get the result back */
    packets.msgs      = messages;
//    packets.nmsgs     = 2;
    packets.nmsgs     = 1;
    if(ioctl(fd, I2C_RDWR, &packets) < 0) {
        //perror("Unable to send data");
    	LOGE("Linux_I2C_readByte: ioctl error = %d\n", errno);
    	close(fd);
        return -1;
    }

	//LOGI("Data Received: 0x%x, ",pucDataBuffer[0]);

	close(fd);
	return 0; //RET_OK
}

int Linux_I2C_writeByte
	(
	signed   int 	adapter_nr,
	unsigned char 	address,
	signed   int 	iNbBytes,
	unsigned char * pucBuffer
	)
{
	//LOGI("Linux_I2C_writeByte: 0x%x, 0x%x, 0x%x", adapter_nr, address, iNbBytes);
	//LOGI("Data: 0x%x, 0x%x, 0x%x", pucBuffer[0], pucBuffer[1], pucBuffer[2]);

	int fd;
    struct i2c_msg messages[1];
    struct i2c_rdwr_ioctl_data packets;

	fd = open("/dev/i2c-0", O_RDWR);

    messages[0].addr  = address;
    messages[0].flags = 0;
    messages[0].len   = iNbBytes;
    messages[0].buf   = pucBuffer;
    /* Send the request to the kernel and get the result back */
    packets.msgs      = messages;
    packets.nmsgs     = 1;
    if(ioctl(fd, I2C_RDWR, &packets) < 0) {
        //perror("Unable to send data");
    	LOGE("Linux_I2C_writeByte: ioctl error = %d\n", errno);
    	close(fd);
        return -1;
    }

	close(fd);
	return iNbBytes;
}



