/*
 * Copyright (C) Your copyright notice.
 *
 * Author: Me
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the PG_ORGANIZATION nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY	THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS-IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdlib.h>
#include <stdio.h>

#include </opt/dragon-sdk/sysroots/aarch64-linaro-linux/usr/src/kernel/include/linux/init.h>
#include </opt/dragon-sdk/sysroots/aarch64-linaro-linux/usr/src/kernel/include/linux/module.h>
#include </opt/dragon-sdk/sysroots/aarch64-linaro-linux/usr/src/kernel/include/linux/spi/spi.h>

#define MY_BUS_NUM 0
static struct spi_device *spi_device;

static int __init spi_init(void)
{
    int ret;
    unsigned char ch = 0x00;
    struct spi_master *master;

    //Register information about your slave device:
    struct spi_board_info spi_device_info = {
        .modalias = "my-device-driver-name",
        .max_speed_hz = 1, //speed your device (slave) can handle
        .bus_num = MY_BUS_NUM,
        .chip_select = 0,
        .mode = 3,
    };

    /*To send data we have to know what spi port/pins should be used. This information
      can be found in the device-tree. */
    master = spi_busnum_to_master( spi_device_info.bus_num );
    if( !master ){
        printk("MASTER not found.\n");
            return -ENODEV;
    }

    // create a new slave device, given the master and device info
    spi_device = spi_new_device( master, &spi_device_info );

    if( !spi_device ) {
        printk("FAILED to create slave.\n");
        return -ENODEV;
    }

    spi_device->bits_per_word = 8;

    ret = spi_setup( spi_device );

    if( ret ){
        printk("FAILED to setup slave.\n");
        spi_unregister_device( spi_device );
        return -ENODEV;
    }

    spi_write(spi_device, &ch, sizeof(ch));

    return 0;
}


static void __exit spi_exit(void)
{
    unsigned char ch = 0Xff;

    if( spi_device ){
        spi_write(spi_device, &ch, sizeof(ch));
        spi_unregister_device( spi_device );
    }
}

module_init(spi_init);
module_exit(spi_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Piktas Zuikis <[email protected]>");
MODULE_DESCRIPTION("SPI module example");

int main(void) {
        puts("Hello World"); /* prints Hello World */
        return 0;
}
