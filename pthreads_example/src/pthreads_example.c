/*
 * Copyright (C) Your copyright notice.
 *
 * Author: Me
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the PG_ORGANIZATION nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY	THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS-IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdlib.h>
#include <stdio.h>

#include <pthread.h>

//****************************************
// Experiment with OpenMP
// Doesn't seem to be working!!
// http://bisqwit.iki.fi/story/howto/openmp/
//****************************************
#include <unistd.h>

void openMPExample(void);
void openMPExample(void)
{
	int array[8];
	int b = 10;

	#pragma omp simd linear(b:2)
	for(int n=0; n<8; ++n) array[n] = b;

	for(int n=0; n<8; ++n) printf("%d\n", array[n]);

//#pragma omp parallel
	int n;
//#pragma omp parallel for
//#pragma omp for private(n)
#pragma omp parallel for private(n)
	//#pragma omp target teams
	for(n = 0; n < 10; ++n)
	{
        printf("Hello World from pthreads_example - openMP section: %d\n", n); /* prints Hello World */
        if (3 == n)
        	sleep(3);
	}

#pragma omp parallel num_threads(2)
	{
		// should print this twice
		printf("threads\n");
	}

}


struct arg {
    int a, b;
    volatile int *rst;
};
typedef struct arg arg;

void* sum(void *);
void* sum(void *ptr)
{
    int i, temp = 0;
    arg *x = ptr;

    for(i = x->a; i <= x->b; ++i)
        temp += i;
    *(x->rst) = temp;
    return 0;
}
int main(void) {
	//openMPExample();

	printf("Hello World from pthreads_example.\n");

	printf("Creating Threads:\n");
	pthread_t sum1, sum2;
    int s1, s2;
    //pthread_create(&sum1, NULL, sum, &(arg){1, 500000000, &s1});
    //pthread_create(&sum2, NULL, sum, &(arg){500000001, 1000000000, &s2});
    pthread_create(&sum1, NULL, sum, &(arg){1, 100, &s1});
    pthread_create(&sum2, NULL, sum, &(arg){101, 200, &s2});
    // Result should be 20100 for (1-101, 101->200)
    pthread_join(sum1, NULL);
    pthread_join(sum2, NULL);
    printf("Result=%d\n", s1 + s2);

    return 0;
}
