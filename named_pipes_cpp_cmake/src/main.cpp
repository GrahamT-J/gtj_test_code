/** @mainpage named_pipes_cpp_cmake - None
 *
 * @author Me <anony@mo.us>
 * @version 1.0.0
**/


/**
 * Main class of project named_pipes_cpp_cmake
 *
 * @param argc the number of arguments
 * @param argv the arguments from the commandline
 * @returns exit code of the application
 */
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <string>
#include <iostream>

//#include <sys/types.h>
#include <sys/stat.h>

#include <time.h>
#include <stdlib.h>
#include "errno.h"

using namespace std;

string fifoServerName = "fifo_server";
string fifoClientName = "fifo_client";

int fileServerFD;
int fileClientFD;

int closeFifoPipes();
int createFifoPipes();
int openFifoPipes();
int removeFifoPipes();

int closeFifoPipes()
{
	close(fileServerFD);
	close(fileClientFD);
	return 0;
}

int createFifoPipes()
{
	int file1,file2;

	printf("createFifoPipes:");
	file1 = mkfifo(fifoServerName.c_str(),0666);
	int errNum = errno;
	if(file1 < 0)
	{
		if(EEXIST == errNum)
		{
			printf("%s: already exists. Ok\n", fifoServerName.c_str());
		}
		else
		{
			printf("Unable to create a FIFO");
			return(-1);
		}
	}

	file2 = mkfifo(fifoClientName.c_str(),0666);
	errNum = errno;
	if(file2<0)
	{
		if(EEXIST == errNum)
		{
			printf("%s: already exists. Ok\n", fifoClientName.c_str());
		}
		else
		{
			printf("Unable to create a FIFO");
			return(-1);
		}
	}

	printf("fifos server and child created successfully or already exists\n");

	return 0;
}

int openFifoPipes()
{
	int file1,file2;

	printf("openFifoPipes:");
	file1 = open(fifoServerName.c_str(), O_RDWR);
	if(file1 < 0)
	{
		printf("Unable to open FIFO: %s", fifoServerName.c_str());
		return file1;
	}

	file2 = open(fifoClientName.c_str(), O_RDWR);
	if(file2<0)
	{
		printf("Unable to open FIFO: %s", fifoClientName.c_str());
		return file2;
	}

	printf("fifos server and child opened successfully\n");
	// Set main file handles
	fileServerFD = file1;
	fileClientFD = file2;

	return 0;
}

int removeFifoPipes()
{
	printf("removeFifoPipes:\n");
	remove(fifoServerName.c_str());
	remove(fifoClientName.c_str());
	return 0;
}

int main(int argc, char **argv) {
	// print a greeting to the console
	printf("Hello World from named_pipes_cpp_cmake!\n");

	int fd;
	char str[256];
	char temp[4]="how";
	char temp1[4];

	fileServerFD = 0;
	fileClientFD = 0;

	int errVal = createFifoPipes();
	if(errVal < 0)
	{
		cout << "Removing Fifo Pipes" << endl;
		removeFifoPipes();
		return errVal;
	}

	errVal = openFifoPipes();
	if(errVal < 0)
	{
		closeFifoPipes();
		removeFifoPipes();
		return errVal;
	}

	printf("File Handles: %s=%d, %s=%d\n", fifoServerName.c_str(), fileServerFD, fifoClientName.c_str(), fileClientFD);

	struct timespec ts;
	ts.tv_sec = 10;
	ts.tv_nsec = 0;
	printf("Sleeping...\n");
	nanosleep(&ts, NULL);

	printf("Exiting...\n");

	closeFifoPipes();
	removeFifoPipes();

	return 0;
}
